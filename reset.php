<?php
//loading classes
require_once('inc/bootstrap_public.php');

//start code
if (App::getAuth()->reset($_GET['id'], $_GET['token'])){
    Session::getInstance()->setFlash('success', "Votre nouveau mot de passe vient de vous être envoyé");
}else{
    Session::getInstance()->setErr();
}
App::redirect('/login');
