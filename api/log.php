<?php
require_once ('../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
    isset($_POST['function']) && isset($_POST['error']) && isset($_POST['file']) ) {
    $return_case = [
        false => ['code' => 400, 'message' => 'An error system occured, call an admin'],
        true => ['code' => 200, 'message' => 'An admin has been noticed about the error']
    ];
    $user_id = App::getAuth()->user()->id;

    $ret = App::addAjaxLog($_POST['function'], $_POST['error'], $user_id, $_POST['file']);
    $json = new parseJson($return_case[$ret]['code'], $return_case[$ret]['message'], null);
    $json->printJson();
}