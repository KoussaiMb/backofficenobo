<?php
require_once ('../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_array('names', 'Les noms de la configuration sont invalides', 'Les noms de la configuration sont introuvables');
    if (!$validator->is_valid())
        parseJson::error($validator->getError('name'))->printJson();

    $names = json_decode($_GET['names']);
    $configs = [];

    foreach ($names as $key => $value) {
        $c = App::getConf()->getConfByName($value);
        if (!$c)
            parseJson::error("Erreur lors de la récupération de la configuration", $value)->printJson();
        $configs[$value] = $c->value;
    }

    parseJson::success("La configuration a correctement été récupéré", (object) $configs)->printJson();
}