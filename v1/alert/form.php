<?php
include_once('../../inc/bootstrap.php');

$db = App::getDB();

if (!isset($_POST['action']))
    App::redirect('view');

Session::getInstance()->update('formData', $_POST);

if($_POST['action'] == 'add') {
    if (empty($_POST['user_0']) && empty($_POST['group_0']))
        App::setFlashAndRedirect('danger', 'Selectionner un utiliseur ou un groupe', 'add');
    $users = [];
    $groups = [];
    $validator = new Validator($_POST);

    for ($i = 0; $i < 10; $i++) {
        if (empty($_POST['user_' . $i]))
            break;
        $validator->isToken('user_' . $i, 'L\'utilisateur ' . $_POST['user_name_' . $i] . ' est invalide');
        $users[] = $_POST['user_' . $i];
    }
    for ($i = 0; $i < 10; $i++) {
        if (empty($_POST['group_' . $i]))
            break;
        $validator->is_id('group_' . $i, 'Le groupe ' . $_POST['group_name_' . $i] . '  est invalide');
        $groups[] = $_POST['group_' . $i];
    }
    $validator->no_symbol('title', 'Le titre de l\'alerte est invalide', 'Veuillez indiquer un titre');
    $validator->no_symbol('description', 'La description de l\'alerte est invalide', 'Veuillez indiquer une description');
    $validator->is_url('url', 'l\'url est invalide');
    $validator->is_date_bo('finished_at', 'La date semble invaide');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('add');
    }
    $_POST['users'] = $users;
    $_POST['groups'] = $groups;
    $_POST['finished_at'] = (!empty($_POST['finished_at']) ? $_POST['finished_at'] : null);

    $ret = App::getAlert()->addAlertFromBo($_POST);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu les alertes n\'ont pu être créées', 'add');
    Session::getInstance()->delete('formData');
    App::setFlashAndRedirect('success', 'L\'alerte a été créée', 'add');
}
elseif ($_POST['action'] == 'finish') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'L\'alerte est invalide', 'L\'alerte est introuvable');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getErrorString('id'), 'view');
    $ret = App::getAlert()->setAlertsAsFinished($_POST);
     if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la suspension de l\'alerte', 'view');
    App::setFlashAndRedirect('success', 'L\'alerte a bien été terminée', 'view');
}
elseif ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'L\'alerte est invalide', 'L\'alerte est introuvable');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getErrorString('id'), 'view');
    $ret = App::getAlert()->deleteAlerts($_POST);
     if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la suppression de l\'alerte', 'view');
    App::setFlashAndRedirect('success', 'L\'alerte a bien été supprimée', 'view');
}
App::redirect('view');
