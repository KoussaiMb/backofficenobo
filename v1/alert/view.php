<?php
require('../../inc/bootstrap.php');

//$otherAlert = App::getAlert()->getAlertByPriority('critic', 0, 20);
$otherAlert = App::getAlert()->getLastAlertsNotFromUser(App::getAuth()->user()->id, 0, 20);
$ownerAlert = App::getAlert()->getLastAlertsFromUser(App::getAuth()->user()->id, 0, 20);

if ($otherAlert === false || $ownerAlert === false)
    Session::getInstance()->setFlash('danger danger-criticAlert', 'Un problème est survenu lors de la récupération des alertes');

require('../../inc/header_bo.php');
?>
<div class="panel panel-primary">
    <div class="panel-heading panel-heading-xs">
        Gérer les alertes
    </div>
    <div class="panel-body">
        <div class="col-xs-12">
            <?php require('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-12">
            <a href="add"><button class="btn btn-success btn-xs">Créer une alerte</button></a>
        </div>
        <div class="col-xs-12">
            <div class="col-sm-6">
                <h3>Vos dernières alertes</h3>
                <div class="lastAlert view-alert">
                    <!--//TODO: To rebuild it-->
                    <?php
                    if (!empty ($ownerAlert)) {
                        foreach ($ownerAlert as $k => $v)
                            if ($v->finished == 0)
                                alert::createAlertDiv($v);
                        foreach ($ownerAlert as $k => $v)
                            if ($v->finished != 0)
                                alert::createAlertDiv($v);
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <h3>Les autres alertes</h3>
                <div class="criticAlert view-alert">
                    <?php
                    if (!empty ($otherAlert)) {
                        foreach ($otherAlert as $k => $v)
                            if ($v->finished == 0)
                                alert::createAlertDiv($v);
                        foreach ($otherAlert as $k => $v)
                            if ($v->finished != 0)
                                alert::createAlertDiv($v);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .view-alert {
        border: 2px solid rgba(0, 0, 0, 0.2);
        padding: 0 10px 0 10px;
        overflow-y: scroll;
        overflow-x: hidden;
    }
    .alert-nobo:nth-child(even) {
        background-color: rgba(242, 242, 242, 0.8);
    }
    @media screen and (min-width: 768px) {
        .alert-nobo {
            height: 65px;
        }
    }
    .alert-nobo {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-content: center;
        -ms-flex-line-pack: center;
        align-content: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .alert-nobo.alert-clickable {
        cursor: pointer;
    }
    .alert-nobo.border-priority-low:hover {
        background-color: rgba(230, 230, 0, 0.3);
    }
    .alert-nobo.border-priority-high:hover {
        background-color: rgba(255, 153, 51, 0.3);
    }
    .alert-nobo.border-priority-critic:hover {
        background-color: rgba(244, 67, 54, 0.3);
    }
    .alert-nobo>div:nth-of-type(2) {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: space-around;
        justify-content: space-around;
        -webkit-align-content: center;
        -ms-flex-line-pack: center;
        align-content: center;
        -webkit-align-items: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
    }
    .alert-date {
        font-size: 10px;
        color: grey;
    }
    .alert-title {
        font-size: 1.4em;
        margin-bottom: 5px;
    }
</style>
<script>
    function constructAlertButtons() {
            return "<div class='btn-group btn-group-justified' role='group'>"
                + "<div class='btn-group' role='group'>"
                + "<button type='button' class='btn btn-default' id='cancel'>Annuler</button>"
                + "</div>"
                + "<div class='btn-group' role='group'>"
                + "<button type='submit' class='btn btn-warning' name='action' value='finish'>"
                + "Mettre l'alerte comme finie"
                + "</button>"
                + "</div>"
                + "<div class='btn-group' role='group'>"
                + "<button type='submit' class='btn btn-danger' name='action' value='delete'>Supprimer l'alerte</button>"
                + "</div>"
                + "</div>";
    }
    function extractInfos(alert_div) {
        return {
            'id': alert_div.find('.alert_id').val(),
            'title': alert_div.find('.alert-title').text(),
            'description': alert_div.find('.alert-description').text(),
            'priority': alert_div.find('.alert_priority').val(),
            'date': alert_div.find('.alert-date').text()
        };
    }
    function constructFormAlert(alert) {
        var buttons = constructAlertButtons();
        return "<form action='form' method='post'>"
            + "<input type='hidden' name='id' value='" + alert.id + "'>"
            + "<div class='text-center' style='width:100%'>"
            + "<p class='alert-title'>"
            + alert.title
            + "<span class='label label-default bg-priority-" + alert.priority + "' style='margin-left:10px'>"
            + alert.priority
            + "</span>"
            + "</p>"
            + "<p class='alert-description'>" + alert.description + "</p>"
            + "<p class='alert-date'>" + alert.date + "</p>"
            + "</div>"
            + buttons
            +"</form>";
    }
    $(document).ready(function(){
        $(".view-alert").css('max-height', $(window).height() * 0.7);

        $('.alert-clickable').on('click', function () {
            var alert = extractInfos($(this));
            var AlertForm = constructFormAlert(alert);
            var modal = bootbox.dialog({
                backdrop: true,
                size: "medium",
                title: "Que voulez-vous faire ?",
                message: AlertForm
            });
            modal.init(function () {
                $('#cancel').on('click', function () {
                    modal.modal('hide');
                })
            });
        })
    });
</script>
<?php
require('../../inc/footer_bo.php');