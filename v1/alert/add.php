<?php
require('../../inc/bootstrap.php');

$data = (array)Session::getInstance()->read('formData');
$priority = App::getListManagement()->getList('priority');
$users = App::getUser()->autocompleteAll();
$groups = App::getManageGroup()->groupsAsOption();

require('../../inc/header_bo.php');
?>
    <div class="panel panel-primary">
        <div class="panel-heading panel-heading-xs">
            <a href="view" style="color: white">Gérer les alertes</a> / Créer une alerte
        </div>
        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <form method="post" action="form">
                <div class="col-xs-6">
                    <div class="form-group col-xs-8">
                        <label for="title">Titre de l'alerte</label>
                        <input type="text" class="form-control input-sm" name="title" value="<?= !empty($data['title']) ? htmlspecialchars($data['title']) : null; ?>" id="title" maxlength="40" required/>
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="priority">Priorité de l'annonce</label>
                        <select class="form-control input-sm priority-<?= !empty($data['priority_id']) ? $data['priority_id'] : 'low'; ?>" name="priority_id" id="priority">
                            <?php foreach($priority as $k => $v): ?>
                                <option class="priority-<?= $v->field; ?>" value="<?= $v->id; ?>" <?= !empty($data['priority_id']) && $data['priority_id'] == $v->id ? 'selected' : ''; ?>><?= $v->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="description">Description</label>
                        <input type="text" class="form-control input-sm" name="description" value="<?= !empty($data['description'])? htmlspecialchars($data['description']) : null; ?>" maxlength="80" id="description"/>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="url">url</label>
                        <input type="text" class="form-control input-sm" name="url" value="<?= !empty($data['url'])? htmlspecialchars($data['url']) : null; ?>" maxlength="100" id="url"/>
                    </div>
                    <div class="form-group col-xs-6">
                        <button type="button" class="btn btn-default" onclick="$('#finished_at').prop('disabled', 0).prop('placeholder', 'fin de validité')">Mettre une date de fin d'alerte</button>
                    </div>
                    <div class="form-group col-xs-6" style="margin-top: 2px">
                        <label for="finished_at" class="sr-only">Date de fin de validité</label>
                        <input type="text" class="form-control input-sm pickadate" name="finished_at" value="<?= !empty($data['finished_at']) ? $data['finished_at'] : null; ?>" id="finished_at" <?= !empty($data['finished_at']) ? '' : 'disabled'; ?>/>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-primary btn-sm" name="action" value="add">Créer l'alerte</button>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="col-xs-6 userCol">
                        <label>
                            Utilisateurs <span id="userNumber"></span>
                        </label>
                        <div class="input-group">
                            <input type="hidden" value="" id="hidden_user">
                            <input type='text' class='form-control input-sm' id='userSearch' placeholder='prénom, nom'>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-sm" id="addUser"><span class="glyphicon glyphicon-plus"></span></button>
                            </span>
                        </div>
                        <br>
                         <div class="addBody addBodyUser">
                             <?php
                             $j = -1;

                             while (!empty($data['user_' . ++$j])) {
                                 echo "<div>";
                                 echo "<input type=\"hidden\" class=\"user_token\" name=\"user_" . $j;
                                 echo "\" value=\"";
                                 echo $data['user_' . $j];
                                 echo "\">";
                                 echo "<input type=\"hidden\" class=\"user_name\" name=\"user_name_" . $j;
                                 echo "\" value=\"";
                                 echo $data['user_name_' . $j];
                                 echo "\">";
                                 echo $data['user_name_' . $j];
                                 echo "<span class='pull-right glyphicon glyphicon-remove'></span>";
                                 echo "</div>";
                             }
                             ?>
                         </div>
                    </div>
                    <div class="col-xs-6 groupCol">
                        <label for="group">
                            Groupes <span id="groupNumber"></span>
                        </label>
                        <div class="input-group">
                            <input type="hidden" value="" id="hidden_group">
                            <select class="form-control input-sm groupSelect" name="group" id="group">
                                <?php echo $groups ?>
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default btn-sm" id="addGroup"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                        </div>
                        <br>
                        <div class="addBody addBodyGroup">
                            <?php
                            $j = -1;

                            while (!empty($data['group_' . ++$j])) {
                                echo "<div>";
                                echo "<input type=\"hidden\" class=\"group_token\" name=\"group_" . $j;
                                echo "\" value=\"";
                                echo $data['group_' . $j];
                                echo "\">";
                                echo "<input type=\"hidden\" class=\"group_name\" name=\"group_name_" . $j;
                                echo "\" value=\"";
                                echo $data['group_name_' . $j];
                                echo "\">";
                                echo $data['group_name_' . $j];
                                echo "<span class='pull-right glyphicon glyphicon-remove'></span>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <style>
        .addBody span {
            padding-top: 2px
        }
        .addBody {
            padding: 3px;
        }
        .addBody > div:nth-child(odd) {
            background-color: lightgrey;
        }
    </style>
    <script type="text/javascript" src='/js/app.js'></script>
    <script>
        function constructGroupRow(group_n, group_name, group_id) {
            return "<div>"
                    + "<input type=\"hidden\" class=\"group_id\" name=\"group_"
                    + group_n
                    + "\" value=\""
                    + group_id
                    + "\">"
                    + "<input type=\"hidden\" class=\"group_name\" name=\"group_name_"
                    + group_n
                    + "\" value=\""
                    + group_name
                    + "\">"
                    + group_name
                    + "<span class='pull-right glyphicon glyphicon-remove'></span>"
                    + "</div>";
        }
        function constructUserRow(user_n, name, user_token) {
            return "<div>"
                    + "<input type=\"hidden\" class=\"user_token\" name=\"user_"
                    + user_n
                    +"\" value=\""
                    + user_token
                    + "\">"
                    + "<input type=\"hidden\" class=\"user_name\" name=\"user_name_"
                    + user_n
                    +"\" value=\""
                    + name
                    + "\">"
                    + name
                    + "<span class='pull-right glyphicon glyphicon-remove'></span>"
                    + "</div>";
        }
        function updateGroupNumber(number, limit) {
            $('#groupNumber').text('(' + number + '/' + limit + ')');
        }
        function updateUserNumber(number, limit) {
            $('#userNumber').text('(' + number + '/' + limit + ')');
        }
        function cleanGroupName(group_name) {
            var start = group_name.match(/[a-z]/i).index;
            return group_name.substr(start);
        }
        function in_array(needle, haystack) {
            return haystack.indexOf(needle);
        }
        function arrayFromVal(inputs) {
            var array = [];
            var len = inputs.length;
            var i = -1;

            while (++i < len) {
                array.push(inputs[i].defaultValue)
            }
            return array;
        }
        function updateInputs(inputs) {
            var len = inputs.length;

            if (len < 1)
                return ;

            var baseName = inputs[0].name.substr(0, inputs[0].name.lastIndexOf('_'));
            var i = -1;

            while (++i < len) {
                inputs[i].name = baseName + '_' + i;
            }
        }

        jQuery(document).ready(function() {
            var bodyGroup = $('.addBodyGroup');
            var bodyUser = $('.addBodyUser');
            var hiddenUser = $('#hidden_user');
            var userSearch = $('#userSearch');
            var group = $('#group');
            var max_user = 10;
            var max_group = 5;

            /*INIT*/
            updateGroupNumber(bodyGroup.find('div').length, max_group);
            updateUserNumber(bodyUser.find('div').length, max_user);

            /*LISTENERS*/
            $('#priority').on('change', function () {
                $(this).removeClass();
                $(this).addClass('form-control input-sm priority-' + $(this).val());
            });
            userSearch.devbridgeAutocomplete({
                lookup: <?php echo !empty($users) ? $users : '[]'; ?>,
                maxHeight: 200,
                onSelect: function (s) {
                    userSearch.val(s.data.firstname + ' ' + s.data.lastname[0] + '.');
                    hiddenUser.val(s.data.user_token);
                },
                onSearchStart: function () {
                    hiddenUser.val(null);
                },
                onSearchError: function () {
                    hiddenUser.val(null);
                }
            });
            $('#addUser').on('click', function () {
                var len = bodyUser.find('div').length;

                if (!hiddenUser.val().length)
                    bo_notify('Choisissez un utilisateur via l\'autocompletion', 'warning');
                else if (len < max_user) {
                    var user_token = hiddenUser.val();
                    var array = arrayFromVal($('.user_token'));
                    var ret = in_array(user_token, array);
                    var toInsert;

                    if (ret == -1) {
                        toInsert = constructUserRow(len, userSearch.val(), user_token);
                        bodyUser.append(toInsert);
                        updateUserNumber(len + 1, max_user);
                    }
                    else
                        bo_notify('Cet utilisateur a déjà été ajouté', 'warning');
                }
                else
                    bo_notify('Nombre maximum d\'utilisateur atteint', 'danger');
            });
            $('#addGroup').on('click', function () {
                var len = bodyGroup.find('div').length;
                var group_id = group.val();

                if (group_id == 0)
                    bo_notify('Choisissez un groupe via l\'autocompletion', 'warning');
                else if (len < max_group) {
                    var group_name = cleanGroupName(group.find('option:selected').text());
                    var array = arrayFromVal($('.group_id'));
                    var ret = in_array(group_id, array);
                    var toInsert;

                    if (ret == -1) {
                        toInsert = constructGroupRow(len, group_name, group_id);
                        bodyGroup.append(toInsert);
                        updateGroupNumber(len + 1, max_group);
                    }
                    else
                        bo_notify('Ce groupe a déjà été ajouté', 'warning');
                }
                else
                    bo_notify('Nombre maximum de groupe atteint', 'danger');
            });
            document.querySelector('.userCol').addEventListener('click', function (event) {
                var classes = event.target.classList;

                if (classes.contains('glyphicon-remove')) {
                    event.target.parentElement.remove();
                    updateUserNumber(bodyUser.find('div').length, max_user);
                    updateInputs($('.user_token'));
                    updateInputs($('.user_name'));
                }
            });
            document.querySelector('.groupCol').addEventListener('click', function (event) {
                var classes = event.target.classList;

                if (classes.contains('glyphicon-remove')) {
                    event.target.parentElement.remove();
                    updateGroupNumber(bodyGroup.find('div').length, max_group);
                    updateInputs($('.group_id'));
                    updateInputs($('.group_name'));
                }
            });
        })
    </script>
<?php
require_once("../../inc/footer_bo.php");