<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 15/06/2018
 * Time: 12:17
 */
require('../../inc/bootstrap.php');
require('../../inc/header_bo.php');
?>

<div class="panel panel-primary">
    <div class="panel-heading panel-heading-xs">
        Exports de datas top secret
    </div>
    <div class="panel-body">
        <?php include('../../inc/print_flash_helper.php'); ?>
        <form class="form-inline">
            <label for="start">Début</label>
            <input type="date" class="form-control" name="start" id="start">
            <label for="end">Fin</label>
            <input type="date" class="form-control" name="end" id="end">
            <button type="button" class="btn btn-success" id="customer_full">Client (FULL)</button>
            <button type="button" class="btn btn-primary" id="missions">Missions</button>
            <button type="button" class="btn btn-warning" id="customer_light">Client (light)</button>
            <button type="button" class="btn btn-warning" id="provider_light">Prestataire (light)</button>
        </form>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        $('#customer_full').on('click', function () {
            let url = "/inc/exports/customer_data?";
            let start = $('#start').val();
            let end = $('#end').val();

            if (start.length !== 0)
                url += 'start=' + start;
            if (end.length !== 0)
                url += '&end=' + end;
            console.log(url);
            window.location = url;
        });
        $('#missions').on('click', function () {
            let url = "/inc/exports/mission_data?";
            let start = $('#start').val();
            let end = $('#end').val();

            if (start.length !== 0)
                url += 'start=' + start;
            if (end.length !== 0)
                url += '&end=' + end;
            console.log(url);
            window.location = url;
        });
        $('#customer_light').on('click', function () {
            let url = "/inc/exports/customer_light_data?";
            let start = $('#start').val();
            let end = $('#end').val();

            if (start.length !== 0)
                url += 'start=' + start;
            if (end.length !== 0)
                url += '&end=' + end;
            console.log(url);
            window.location = url;
        });
        $('#provider_light').on('click', function () {
            let url = "/inc/exports/provider_light_data?";
            let start = $('#start').val();
            let end = $('#end').val();

            if (start.length !== 0)
                url += 'start=' + start;
            if (end.length !== 0)
                url += '&end=' + end;
            console.log(url);
            window.location = url;
        });
    });
</script>