<?php
require('../../inc/bootstrap.php');
require('../../inc/header_bo.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');

if ($validator->is_valid()) {
    $w_list_id = $_POST['w_id'];
    $w_step = $_POST['step'];
    $workflow = App::getWorkflow();
    $steps = $workflow->getStepsByName('reception');

    if ($w_step > end($steps)->step) {
        $workflow->updateWaitingListDateListOut($w_list_id);
        $workflow->updateWaitingListAccepted($w_list_id, 1);
        App::redirect_js("/v1/workflow-macro/view");
    }

    $customer_info = $workflow->getCustomerByWaitingId($w_list_id);

    require('../../inc/workflow/workflow_header.php');
?>
    <div class="row col-sm-12">
        <div class="col-sm-9" id="step-content">

        </div>

        <div class="col-sm-3" style="padding-right: 0">
            <?php require_once('../../inc/customer/customer_action_infos.php'); ?>
        </div>
    </div>
    <div class="row col-sm-12">
        <form id="workflow-form" action="" method="post">
            <input type="hidden" id="step" name="step" value="<?= $w_step ?>">
            <input type="hidden" id="w_id" name="w_id" value="<?= $w_list_id ?>">
            <input type="hidden" id="w_url" name="w_url" value="<?= $workflow->getStepByStepAndName($w_step, 'reception')->url ?>">
        </form>
    </div>
    <?php
} else {
    require('dashboard.php');
}
?>
<!-- Modal -->
<div id="modal_refuse" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                <h4 class="modal-title">Refuser le client</h4>
            </div>
            <div style='padding: 10px' id="refuse_raison">
                <h4>Sélectionnez une raison:</h4>
                <?php
                    $raisons = App::getListManagement()->getList('refuse_raison');
                ?>
                <?php foreach ($raisons as $k => $v): ?>
                    <input type="checkbox" name="raison_<?= $v->id ?>" id="raison_<?= $v->id ?>" data-val="<?= $v->id ?>">
                    <label for="raison_<?= $v->id ?>"><?= $v->field ?></label>
                    <textarea name="name" rows="3" cols="78" style="resize:none;" id="comment_<?= $v->id ?>"></textarea>
                    <br>
                <?php endforeach; ?>
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-danger" data-href="#" onclick="refuseClient()">Refuser le client</button>
            </div>
        </div>
    </div>
</div>
<style>
    .btn{
        margin: 5px;
        width: 100%;
    }
</style>
<script src="/js/app.js"></script>
<script src="/js/workflow.js"></script>
<script src="/js/reception.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    var step_url = $('#w_url').val();
    var step = Number($('#step').val());
    var w_id = Number($('#w_id').val());

    if (step_url && step && w_id) {
        loadStep(step_url, step, w_id);
    }
});
</script>
<?php
require('../../inc/footer_bo.php');
