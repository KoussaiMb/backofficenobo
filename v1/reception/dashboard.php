<?php
/* Building a column element depending on the step */
function buildCustomerColumn($type){

    $newC = false;

    if ($type == 'new-client'){
        $step_customers = App::getWorkflow()->getNewCustomersInWaitingList();
        $newC = true;
    } else if ($type == 'waiting-decision-client'){
        $step_customers = App::getWorkflow()->getAlerteCustomersInWaitingList();
        $newC = true;
    } else if ($type == 'waiting-client') {
        $step_customers= App::getWorkflow()->getWaitingCustomersInWaitingList();
        $newC = true;
    } else {
        $step_customers = App::getWorkflow()->getProgressCustomersInWaitingList();
    }

    $c_nb = sizeof($step_customers);
    for($i = 0; $i < $c_nb; $i++){

        $customer = $step_customers[$i];
        ?>

        <div class="thumbnail <?= $type ?>" data-phone="<?= $customer->phone ?>" data-val="<?= $customer->id ?>" data-class="<?= $type ?>">

            <div class="caption row">
                <div class="col-xs-12">
                    <div class="surface">
                        <?=$customer->surface?>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <span class="around-margin glyphicon glyphicon-user"></span>
                            <?= $customer->lastname ?> <?= $customer->firstname ?>
                        </div>
                        <div>
                            <span class="around-margin glyphicon glyphicon-calendar"></span>
                            <?=parseStr::DatetimeToDate($customer->date_list_in)?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <span class="around-margin glyphicon glyphicon-map-marker"></span>
                            <?=$customer->zipcode?>
                        </div>
                        <div>
                            <span class="around-margin glyphicon glyphicon-earphone"></span>

                            <?php
                                $lastCall = App::getCustomerActions()->getLastActionByUserIdAndType($customer->user_id, 1);
                                if ($lastCall) {
                                    echo parseStr::DatetimeToDate($lastCall->action_date);
                                } else {
                                    echo "Aucun appel";
                                }
                            ?>

                        </div>
                    </div>
                    <div class="col-sm-12">
                            <?php
                                if ($customer->recurrence_id != 0) {
                                    ?>
                                    <span class="around-margin glyphicon glyphicon-list"></span>
                                    <?= App::getListManagement()->getFieldById($customer->recurrence_id); ?>
                                    <?php
                                }
                            ?>
                    </div>
                </div>
            </div>
            <?php

                if($customer->workflow_id != 0){
                    $workflow = App::getWorkflow();
                    $step_info = $workflow->getStepById($customer->workflow_id);
                    $step = 1;
                    if ($step_info)
                        $step = max($step, $step_info->step);
                    $allSteps = $workflow->getStepsByName('reception');
                    $lastStep = end($allSteps);
            ?>
                    <div class="step-completion" style="width:<?=(($step-1)/$lastStep->step)*100?>%">

                    </div>
            <?php } ?>

        </div>

        <?php

    }
}


?>
<div class="panel panel-success">
    <div class="panel-heading">
        Gestion des clients en attente
    </div>
    <div class="panel-body">
        <div id="hidden_form_container" style="display:none;"></div>

        <div class="col-sm-12 flex-container">
            <div class="col-sm-3 column-title padding-0">
                <p>Nouveaux clients</p>
                <?php buildCustomerColumn('new-client'); ?>
            </div>
            <div class="col-sm-3 column-title padding-0">
                <p>Clients à contacter</p>
                <?php buildCustomerColumn('waiting-client'); ?>
            </div>
            <div class="col-sm-3 column-title padding-0">
                <p>Clients en traitement</p>
                <?php
                buildCustomerColumn('');
                ?>
            </div>
            <div class="col-sm-3 column-title padding-0">
                <p>Alerte client</p>
                <?php buildCustomerColumn('waiting-decision-client'); ?>
            </div>
        </div>
    </div>
</div>


<style>

.step-completion{
    width: 100%;
    height: 5px;
    background-color: rgb(60, 118, 61);
}

.surface {
    position: absolute;
    right: 3px;
    top: -18px;
    height: 30px;
    width: 30px;
    border-radius: 50%;
    border: 1px solid lightgrey;
    background-color: white;
    text-align: center;
    padding-top: 4px!important;
    z-index: 2;
}

.workflow-step-active{
    background-color: royalblue;
    border-radius: 10px;
    color : white;
    padding: 10px;
    text-align: center;
}

.workflow-step-inactive{
    background-color: whitesmoke;
    color : grey;
    padding: 10px;
    text-align: center;
}

.caption>div>div {
    padding: 0 2px;
}

.caption h4 {
    margin: 0;
    padding: 3px;
}

.caption>div:last-child {
    margin: 0;
}

.flex-container {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-justify-content: flex-start;
    -ms-flex-pack: start;
    justify-content: center;
    -webkit-align-content: stretch;
    -ms-flex-line-pack: stretch;
    align-content: stretch;
    -webkit-align-items: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
}

.padding-0{
    padding-right: 2px;
    padding-left: 2px;
}

.column-title{
    text-align: center;
    font-weight: bold;
    font-size: 14px;
}

.well {
    min-height: 20px;
    padding: 0;
    margin-bottom: 2px;
    border-radius: 0;
    border: 0;
}
.thumbnail .caption {
    padding: 0;
    color: #333;
}

.thumbnail {
    text-align: left;
    display: block;
    padding: 0;
    margin-bottom: 10px;
    background-color: #fff;
    border-radius: 0;
    cursor: pointer;
    border: lightgrey solid 1px;
    padding-top: 5px;
    line-height: 17px;
    font-weight: normal;
    font-size: 0.8em;
}

.card-justify{
    margin-right:1px;
}

.around-margin{
    margin-right: 5px;
    margin-left: 5px;
}

.bg-new-client {
    background : rgba(100, 125, 150, 0.2);
    border-radius: 10px;
}

.border-step-0{
    background : rgba(100, 125, 150, 0.2);
    border-radius: 10px;
}

.bg-waiting-client{
    background : rgba(60, 150, 20, 0.2);
    border-radius: 10px;
}

.border-step-1{
    border : solid 3px rgba(60, 150, 20, 0.2);
    border-radius: 10px;
}


.bg-info-client{
    background : rgba(60, 190, 60, 0.2);
    border-radius: 10px;
}

.border-step-2{
    border : solid 3px rgba(60, 190, 60, 0.2);
    border-radius: 10px;
}

.bg-quest-client{
    background : rgba(190, 125, 60, 0.2);
    border-radius: 10px;
}

.bg-waiting-decision-client{
    background : rgba(190, 125, 60, 0.2);
    border-radius: 10px;
}

.border-step-3{
    border : solid 3px rgba(190, 125, 60, 0.2);
    border-radius: 10px;
}

.bg-access-client{
    background : rgba(60, 125, 190, 0.2);
    border-radius: 10px;
}

.border-step-4{
    border : solid 3px rgba(60, 125, 190, 0.2);
    border-radius: 10px;
}

.bg-dispo-client{
    background : rgba(190, 90, 90, 0.2);
    border-radius: 10px;
}

.border-step-5{
    border : solid 3px rgba(190, 90, 90, 0.2);
    border-radius: 10px;
}

.bg-comm-client{
    background : rgba(50, 200, 200, 0.2);
    border-radius: 10px;
}

.border-step-6{
    border : solid 3px rgba(50, 200, 200, 0.2);
    border-radius: 10px;
}

</style>
