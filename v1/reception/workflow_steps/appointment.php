<?php

require('../../../inc/bootstrap.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
if (!$validator->is_valid())
    App::setFlashAndRedirect('danger', 'Informations erronées lors de la génération de la page', '/v1/reception');


$w_list_id = $_POST['w_id'];
$w_step = $_POST['step']; 
$workflow = App::getWorkflow();
$customer_info = $workflow->getCustomerByWaitingId($w_list_id);
$weekdayNames= Array("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
$fullCalendar = App::getFullCalendar();

if(!$customer_info)
    App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des informations du client', '/v1/reception');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Premier rendez-vous
    </div>
    <div class="panel-body">
        <?php include('../../../inc/print_flash_helper.php'); ?>

        <div id="hidden_form_container" style="display:none;"></div>
        <input type="hidden" id="w_list_id" value="<?= $w_list_id ?>">
        <input type="hidden" id="w_step_id" value="<?= $w_step ?>">
        <input type="hidden" id="address_dispo" value="<?= $customer_info->address_token ?>">
        <input type="hidden" id="user_token" value="<?= $customer_info->user_token ?>">
        <div class="appointment container">
            <div class="row">
                <div class="col-xs-6 row">
                    <?php
                    $event = $fullCalendar->getFirstEventByAddressByCategory($customer_info->address_id, 1);

                    if ($event) {
                        $user_info = $fullCalendar->getFirstUsersByEventId($event->event_id, "guestrelation");
                        if (!$user_info) {
                            $user_info = (object) array("firstname" => "inconnue", "lastname" => "");
                        }
                        ?>
                        <div class="col-xs-8 col-xs-offset-2 dispo-item" data-val="<?= $event->id ?>" data-date="<?= date('Y-m-d', strtotime('+'.($event->day).' days', strtotime('last sunday' , strtotime($event->start_rec)))) ?>" id="current_appointment">
                            <div class="dispo-item-corner"></div>
                            <h5><u><?= $user_info->firstname ?> <?= $user_info->lastname ?></u></h5>
                            <p>
                                <?= $weekdayNames[$event->day] ?> 
                                <?= parseStr::date_to_str(date('Y-m-d', strtotime('+'.($event->day).' days', strtotime('last sunday' , strtotime($event->start_rec))))) ?>
                                 de <?= parseStr::timeToTime($event->start) ?>
                                 à 
                                 </span><?= parseStr::timeToTime($event->end) ?></p>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-xs-8 col-xs-offset-2 dispo-item empty" data-val="0" id="current_appointment">
                            <div class="dispo-item-corner"></div>
                            <h5>Aucun rendez-vous</h5>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-xs-2">
                    <button onclick="exportAppointment()"><span class="glyphicon glyphicon-share-alt"></span><p>Exporter</p></button>
                </div>
                <div class="col-xs-4">
                    <h4>Rechercher à partir du:</h4>
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" value="<?= date("Y-m-d") ?>" id="search_date" data-real="<?= date("Y-m-d") ?>"/>
                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 guest-dispo">

        </div>
        <div class="col-sm-12 space">
            <div class="col-sm-6">
                <button type="button" name="last" class="btn btn-warning" onclick="changeStep(-1)">Précédent</button>
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-info" onclick="changeStep(1)">Valider le premier rendez-vous</button>
            </div>
        </div>
    </div>
</div>

<style>

.space{
    margin-top: 20px;
    margin-bottom: 20px;
}

.cell{
    border: 1px solid black;
    padding: 5px;
}

.toggle-cell{
    background-color: #e60000;
}

.selected{
    background-color: green;
}

.fc-day.fc-today{
    background: #FFF !important;
    border: none !important;
    border-top: 1px solid #ddd !important;
    font-weight: bold;
}

.fc-day-header.fc-today{
    background: white !important;
}

.invalid_event {
    background: repeating-linear-gradient(60deg, red, red 5px, #ff9999 5px, #ff9999 10px);
}

.dispo-item {
    background-image: linear-gradient(#fff2e6 0px, #ffe6cc 100%);
    border-color: #ffd9b3;
    border-width: 1px;
    border-style: solid;
    cursor: pointer;
}

.dispo-item-corner {
    position: absolute;
    top: -1px;
    right: -1px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 20px 20px 0;
    border-color: transparent #ffffff #ffe6cc transparent;
}

.dispo-item h5 {
    width: 100%;
    text-align: center;
}

.dispo-item p {
    width: 100%;
    text-align: center;
}

.dispo-item.empty {
    background-image: linear-gradient(rgb(213, 213, 213) 0px, rgb(191, 191, 191) 100%);
}

.appointment {
    margin-bottom: 10px;
    border-color: rgb(214, 233, 198);
    border-width: 1px;
    border-style: solid;
    padding: 5px;
    width: 100%;
}

.appointment .dispo-item {
    background-image: linear-gradient(#e6f2ff 0px, #cce6ff 100%);
    border-color: #b3d9ff;
}

.appointment .dispo-item-corner {
    border-color: transparent #ffffff #cce6ff transparent;
}

.guest-dispo hr {
    margin-top: 5px;
    margin-bottom: 5px;
}
</style>
<script src="/js/reception.js"></script>
<script src="/js/appointment.js"></script>