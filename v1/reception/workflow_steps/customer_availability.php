<?php

require('../../../inc/bootstrap.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
if (!$validator->is_valid())
    App::setFlashAndRedirect('danger', 'Informations erronées lors de la génération de la page', '/v1/reception');

$w_list_id = $_POST['w_id'];
$w_step = $_POST['step'];
$workflow = App::getWorkflow();
$customer_info = $workflow->getCustomerByWaitingId($w_list_id);

if(!$customer_info)
    App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des informations du client', '/v1/reception');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        Disponibilités clients
    </div>
    <div class="panel-body">
        <?php include('../../../inc/print_flash_helper.php'); ?>

        <div id="hidden_form_container" style="display:none;"></div>

        <input type="hidden" id="w_list_id" value="<?= $w_list_id ?>">
        <input type="hidden" id="w_step_id" value="<?= $w_step ?>">
        <input type="hidden" id="address_dispo" value="<?= $customer_info->address_token ?>">
        <input type="hidden" id="user_token" value="<?= $customer_info->user_token ?>">

        <div class="col-sm-12">
            <div id="calendar" class="no-today"></div>
        </div>


        <div class="col-sm-12 space">
            <div class="col-sm-6">
                <button type="button" name="last" class="btn btn-warning" onclick="changeStep(-1)">Précédent</button>
            </div>
            <div class="col-sm-6">
                <button type="button" id="btn_validate_dispo" class="btn btn-info">Valider dispo client</button>
            </div>
        </div>

        <!-- Modal delete dispo -->
        <div id="modal_dispo" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                        <h4 id="modal_dispo_Title" class="modal-title"></h4>
                    </div>
                    <div id="modal_dispo_Body" class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-delete-dispo">Supprimer le créneau de disponibilité</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<style>

.space{
    margin-top: 20px;
    margin-bottom: 20px;
}

.cell{
    border: 1px solid black;
    padding: 5px;
}

.toggle-cell{
    background-color: #e60000;
}

.selected{
    background-color: green;
}

.no-today .fc-today {
    background-color: #FFF !important;
}

.invalid_event {
    background: repeating-linear-gradient(60deg, red, red 5px, #ff9999 5px, #ff9999 10px);
}
</style>

<script src="/js/edit_client.js"></script>
<script>
jQuery(document).ready(function() {
    var user_token = $('#user_token').val();
    var calendar_dispo = $('#calendar');

    $("#btn_validate_dispo").click(function () {
        var w_id = document.getElementById("w_list_id").value;
        changeStep(1);
    });

    calendar_dispo.fullCalendar('destroy');
    calendar_dispo_customer(user_token);
    calendar_dispo.fullCalendar('render');

});
</script>
