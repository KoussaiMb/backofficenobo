<?php

require('../../../inc/bootstrap.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
if (!$validator->is_valid())
    App::setFlashAndRedirect('danger', 'Informations erronées lors de la génération de la page', '/v1/reception');

$w_list_id = $_POST['w_id'];
$w_step = $_POST['step'];
$workflow = App::getWorkflow();
$customer_info = $workflow->getCustomerByWaitingId($w_list_id);

if(!$customer_info)
    App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des informations du client', '/v1/reception');

$m_duration = App::getReception()->getMissionDurationByAddressId($w_list_id);
$md_length = sizeof($m_duration);

$rec_selected_type = '';
if($md_length > 0){
    $rec_selected_type = $m_duration[0]->recurrence_id;
}

$moment = App::getConf()->getAll();
$list_recurrence = App::getListManagement()->getList('recurrence');
$list_acquisition = App::getListManagement()->getList('source_acquisition');

?>

<div class="panel panel-success">
    <div class="panel-heading">
        Informations du client
    </div>
    <div class="panel-body">

        <div id="hidden_form_container" style="display:none;"></div>


        <form method="POST" action="" id="customer-info">

            <input type="hidden" id="w_list_id" name="w_list_id" value="<?= $w_list_id ?>">
            <input type="hidden" id="w_step_id" name="w_step_id" value="<?= $w_step ?>">
            <input type="hidden" id="md_length" name="md_length" value="<?= $md_length ?>">

            <div class="col-xs-12">
                <div class="form-group col-xs-6 no-padding">
                    <label for="gender">Genre</label>
                    <select class="form-control" name="gender_id" id="gender">
                        <?php $list = App::getListManagement()->getList('gender'); ?>
                        <?php foreach ($list as $k => $v): ?>
                            <option value="<?= $v->id;?>" <?= $v->id == $customer_info->gender_id ? 'selected' : '';?>><?= $v->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="maritalStatus">Etat civil</label>
                    <select class="form-control" name="maritalStatus" id="maritalStatus">
                        <?php $list = App::getListManagement()->getList('maritalStatus'); ?>
                        <?php foreach ($list as $k => $v): ?>
                            <option value="<?= $v->id;?>" <?= $v->id == $customer_info->maritalStatus_id ? 'selected' : '';?>><?= $v->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input type="hidden" name="street_number" id="street_number_home" readonly>
                <input type="hidden" name="route" id="route_home" readonly>
                <input type="hidden" name="country" id="country_home" readonly>
                <input type="hidden" name="lat" id="lat_home" readonly>
                <input type="hidden" name="lng" id="lng_home" readonly>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                    <input type="text" class="form-control" id="autocomplete_home" required>
                </div>
                <br/>
                <div class="form-group col-xs-12 no-padding">
                    <label for="address">Adresse</label>
                    <input type="text" class="form-control" name="address" id="address_home" value="<?= empty($customer_info->address) ? NULL : $customer_info->address ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="zipcode">Code postal</label>
                    <input type="text" class="form-control" name="zipcode" id="postal_code_home" value="<?= empty($customer_info->zipcode) ? NULL : $customer_info->zipcode ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="city">Ville</label>
                    <input type="text" class="form-control" name="city" id="locality_home" value="<?= empty($customer_info->city) ? NULL : $customer_info->city ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="lastname">Nom</label>
                    <input type="text" class="form-control" name="lastname" id="lastname" value="<?= empty($customer_info->lastname) ? NULL : $customer_info->lastname ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="firstname">Prénom</label>
                    <input type="text" class="form-control" name="firstname" id="firstname" value="<?= empty($customer_info->firstname) ? NULL : $customer_info->firstname ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" id="email" value="<?= empty($customer_info->email) ? NULL : $customer_info->email ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="phone">Téléphone</label>
                    <input type="text" class="form-control" name="phone" id="phone" value="<?= empty($customer_info->phone) ? NULL : $customer_info->phone ?>" required>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="select_frequency">Type de prestation voulue</label>
                    <select class="form-control frequency" id="select_frequency" name="select_frequency">
                        <option value="">Pas de choix</option>
                        <?php foreach ($list_recurrence as $rec_type): ?>
                            <option value="<?= $rec_type->id ?>"
                            <?= $rec_type->id == $customer_info->recurrence_id ? 'selected' : ''; ?>
                            ><?= $rec_type->field ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-xs-6 no-padding">
                    <label for="nbHours">Nombre d'heure par prestation</label>
                    <input type="number" class="form-control" name="nbHours" id="nbHours" value="<?= empty($customer_info->nbHours) ? NULL : $customer_info->nbHours ?>" required>
                </div>
                <div class="form-group">
                    <label for="select_acquisition">Source d'acquisition</label>
                    <select class="form-control frequency" id="select_acquisition" name="select_acquisition">
                        <option value="">Pas de choix</option>
                        <?php foreach ($list_acquisition as $rec_type): ?>
                            <option value="<?= $rec_type->id ?>"
                                <?= $rec_type->id == $customer_info->l_acquisition_id ? 'selected' : ''; ?>
                            ><?= $rec_type->field ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-12">
                    <label for="have_ironing">Besoin de repassage ?
                        <input type="checkbox" name="have_ironing" id="have_ironing" value="1" <?= isset($customer_info->have_ironing) && $customer_info->have_ironing == 1 ? 'checked' : '' ?>>
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
                <button type="button" name="last" class="btn btn-warning" onclick="changeStep(-1)">Précédent</button>
            </div>
            <div class="col-xs-4">
                <button type="button" name="refuse" class="btn btn-danger" onclick="openRefuseModal()">Refuser le client</button>
            </div>
            <div class="col-xs-4">
                <input type="hidden" name="user_token" value="<?= $customer_info->user_token ?>">
                <input type="hidden" name="address_id" value="<?= $customer_info->address_id ?>">
                <input type="hidden" id="hidden_nb_duration" name="nb_duration" value="<?= $md_length ?>">
                <button type="button" class="btn btn-success form-control" name="action" value="validate" onclick="updateCustomer()">Valider le questionnaire</button>
            </div>
        </form>
    </div>
</div>

<style>

.no-padding{
    padding: 1px;
}

.space{
    margin-top: 10px;
    margin-bottom: 20px;
}
</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBeMne5-vmGGRYsdcCxDb-PYs3McbjVmCg&libraries=places&callback=initialize" async defer></script>
<script type="text/javascript">
    function updateCustomer(){
        var customerInfos = {};

        $("#customer-info :input").each(function() {
            var attr = $(this).attr("name");
            if (typeof attr !== typeof undefined && attr !== false) {
                customerInfos[attr] = $(this).val();
            }
        });

        ajax_call("api/customer_info", "POST", customerInfos, function(result){
            console.log(result);
            if(result.code == 200 && result.data){
                changeStep(1);
            } else if (result.code == 200 && !result.data) {
                bo_notify(result.message, "success");
            } else {
                bo_notify(result.message, 'danger');
            }
        });
    }

    var placeSearch, autocomplete_home;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function fillInAddr_home() {
        var id = 'home';
        var place = autocomplete_home.getPlace();
        for (var component in componentForm) {
            document.getElementById(component+ '_' + id).value = '';
        }
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType + '_' + id).value = val;
            }
        }
        $('#lat_' + id).val(place.geometry.location.lat());
        $('#lng_' + id).val(place.geometry.location.lng());
        $('#address_' + id).val($('#street_number_' + id).val() + ' ' + $('#route_' + id).val());
    }

    function initialize() {
        initGoogleAutocomplete_home('autocomplete_home');
    }

    $(document).on("change", "#have_ironing", function(){
        $(this).val(($(this).prop("checked") ? 1 : 0));
    })
</script>
