<?php

require('../../../inc/bootstrap.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
if (!$validator->is_valid())
    App::setFlashAndRedirect('danger', 'Informations erronées lors de la génération de la page', '/v1/reception');

$w_list_id = $_POST['w_id'];
$w_step = $_POST['step'];
$workflow = App::getWorkflow();
$customer_info = $workflow->getCustomerByWaitingId($w_list_id);

if(!$customer_info)
    App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des informations du client', '/v1/reception');

$survey = new SurveyBuilder('reception', $customer_info->user_id);
$survey_name = $survey->getSurveyName();

?>
<div class="panel panel-success">
    <div class="panel-heading">
        Questionnaire
    </div>
    <div class="panel-body">
        <?php include('../../../inc/print_flash_helper.php'); ?>

        <div id="hidden_form_container" style="display:none;"></div>

        <input type="hidden" id="w_list_id" name="w_list_id" value="<?= $w_list_id ?>">
        <input type="hidden" id="w_step_id" name="w_step_id" value="<?= $w_step ?>">
        <div class="col-sm-12">
            <h2>Questionnaire</h2>
        </div>
        <form method="POST" id="survey_form_id">
            <div class="col-sm-12 survey-container">
                <?php
                if(isset($survey)){
                    echo $survey->getCompleteSurveyHtml();
                }
                ?>
                <div class="col-sm-6">
                    <button type="button" name="last" class="btn btn-warning" onclick="changeStep(-1)">Précédent</button>
                </div>
                <div class="col-sm-6">
                    <input type="hidden" name="w_step_id" id="w_step" value="<?= $w_step; ?>">
                    <input type="hidden" name="w_list_id" id="w_id" value="<?= $w_list_id; ?>">
                    <input type="hidden" name="user_id" id="user_id" value="<?= $customer_info->user_id; ?>">
                    <input type="hidden" name="survey_name" id="survey_name_id" value="<?= $survey_name; ?>">
                    <button type="button" id="validate_survey_btn" class="btn btn-success" name="action" value="validate"><span class="glyphicon glyphicon-validate"></span>Valider le questionnaire</button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
.survey-container{
    margin-bottom : 10px;
    padding:5px;
}

#validate_survey_btn{
    width: 100%;
}
</style>

<script src="/js/reception.js"></script>
<script src="/js/app.js"></script>
<script>
var w_id = document.getElementById("w_list_id").value;

$('#validate_survey_btn').click(function(){
    var form_data = $('#survey_form_id').serialize();

    ajax_call('api/survey', "POST", {form_data: form_data}, function(result){

        if (result.code == 200 && result.data){
            var score = result.data.total_score;
            var color_scores = result.data.color_score;
            var thresholds = result.data.thresholds;
            buildScoreModal(score, color_scores, thresholds, w_id);
            // TODO: Change the modal
        } else {
            bo_notify(result.message, 'danger');
            bo_notify('Impossible de calculer le score', 'danger');
        }
    });
});

/* Handler for questions of type select */
$('.select_answer').on('change' ,function(){
    var src_txt = $(this).find('option:selected').text();
    var src_id = $(this).attr('data-val');

    if(src_txt == 'Autre'){
        var src_other = $(this).attr('data-other');
        var parent = $(this).closest('div');

        var input = '<div class="col-sm-12" id="other_answer_' + src_id + '">';
        input += '<label for="other_answer_' + src_id + '"></label>';
        input += '<input type="text" class="form-control" id="other_answer_' + src_id + '" name="other_answer_' + src_id + '" value="'+ src_other  +'">';
        input += '</div>';

        parent.append(input);
    }else{
        if(document.getElementById("other_answer_" + src_id)){
            $('#other_answer_' + src_id).remove();
        }
    }
}).trigger('change');
</script>
