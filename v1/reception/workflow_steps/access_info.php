<?php

require('../../../inc/bootstrap.php');

$validator = new Validator($_POST);

$validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
$validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
if (!$validator->is_valid())
    App::setFlashAndRedirect('danger', 'Informations erronées lors de la génération de la page', '/v1/reception');

$w_list_id = $_POST['w_id'];
$w_step = $_POST['step'];
$customer_info = App::getWorkflow()->getCustomerByWaitingId($w_list_id);

if(!$customer_info)
    App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des informations du client', '/v1/reception');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        Questionnaire
    </div>
    <div class="panel-body">
        <?php include('../../../inc/print_flash_helper.php'); ?>

        <div id="hidden_form_container" style="display:none;"></div>
        <form method="POST" action="" id="access-info">
            <div class="col-xs-12">
                <h3>Informations sur l'appartement</h3>
                <div class="form-group col-xs-6">
                    <label for="surface">Surface</label>
                    <input type="number" class="form-control" name="surface" id="surface" value="<?= empty($customer_info->surface) ? NULL : $customer_info->surface ?>" required>
                </div>
                <div class="form-group col-xs-6">
                    <label for="roomNb">Nombre de chambre(s)</label>
                    <input type="number" class="form-control" name="roomNb" id="roomNb" value="<?= empty($customer_info->roomNb) ? NULL : $customer_info->roomNb ?>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="waterNb">Nombre de salle(s) d'eau</label>
                    <input type="number" class="form-control" name="waterNb" id="waterNb" value="<?= empty($customer_info->waterNb) ? NULL : $customer_info->waterNb ?>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="wcNb">Nombre de WC</label>
                    <input type="number" class="form-control" name="wcNb" id="wcNb" value="<?= empty($customer_info->wcNb) ? NULL : $customer_info->wcNb ?>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="peopleHome">Personnes dans le domicile</label>
                    <input type="number" class="form-control" name="peopleHome" id="peopleHome" value="<?= empty($customer_info->peopleHome) ? NULL : $customer_info->peopleHome ?>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="l_pet_id">Animaux de compagnie</label>
                    <select class="form-control" name="l_pet_id" id="l_pet_id">
                        <?php $pets = App::getListManagement()->getList('pet'); ?>
                        <?php foreach($pets as $pet): ?>
                            <option value="<?= $pet->id; ?>" <?= isset($customer_info->l_pet_id) && $customer_info->l_pet_id == $pet->id ? 'selected' : ''; ?>><?= $pet->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-xs-6">
                    <label for="childNb">Nombre d'enfants</label>
                    <input type="number" class="form-control" name="childNb" id="childNb" value="<?= empty($customer_info->childNb) ? NULL : $customer_info->childNb ?>">
                </div>
                <div class="form-group col-xs-6">
                    <label for="have_key">Double des clés ?
                        <input type="checkbox" name="have_key" id="have_key" value="1" <?= isset($customer_info->have_key) && $customer_info->have_key == 1 ? 'checked' : '' ?>>
                    </label>
                </div>
                <br>
            </div>
            <div class="col-xs-12">
                <h3>Informations d'accès</h3>
                <div class="col-xs-4 form-group">
                    <label for="batiment">Bâtiment</label>
                    <input type="text" class="form-control" name="batiment" value="<?= empty($customer_info->batiment) ? NULL : $customer_info->batiment ?>" id="batiment" maxlength="100">
                </div>
                <div class="col-xs-4 form-group">
                    <label for="digicode">Digicode 1</label>
                    <input type="text" class="form-control" name="digicode" value="<?= empty($customer_info->digicode) ? NULL : $customer_info->digicode ?>" id="digicode" maxlength="20">
                </div>
                <div class="col-xs-4 form-group">
                    <label for="digicode_2">Digicode 2</label>
                    <input type="text" class="form-control" name="digicode_2" value="<?= empty($customer_info->digicode_2) ? NULL : $customer_info->digicode_2 ?>" id="digicode_2" maxlength="20">
                </div>
                <div class="col-xs-4 form-group">
                    <label for="doorBell">Interphone</label>
                    <input type="text" class="form-control" name="doorBell" value="<?= empty($customer_info->doorBell) ? NULL : $customer_info->doorBell ?>" id="doorBell" maxlength="20">
                </div>
                <div class="col-xs-4 form-group">
                    <label for="door">Porte</label>
                    <input type="text" class="form-control" name="door" value="<?= empty($customer_info->door) ? NULL : $customer_info->door ?>" id="door" maxlength="60">
                </div>
                <div class="col-xs-4 form-group">
                    <label for="floor">Etage</label>
                    <input type="text" class="form-control" name="floor" value="<?= empty($customer_info->floor) ? NULL : $customer_info->floor ?>" id="floor" maxlength="100">
                </div>
            </div>

            <div class="col-xs-4">
                <button type="button" name="last" class="btn btn-warning" onclick="changeStep(-1)">Précédent</button>
            </div>
            <div class="col-xs-4">
                <button type="button" name="refuse" class="btn btn-danger" onclick="openRefuseModal()">Refuser le client</button>
            </div>
            <div class="col-xs-4">
                <input type="hidden" id="w_list_id" name="w_list_id" value="<?= $w_list_id ?>">
                <input type="hidden" id="w_step_id" name="w_step_id" value="<?= $w_step ?>">
                <input type="hidden" name="user_token" value="<?= $customer_info->user_token ?>">
                <input type="hidden" name="address_id" value="<?= $customer_info->address_id ?>">
                <button type="button" class="btn btn-success form-control" id="btn_validate_info" name="action" value="validate" onclick="updateAccessInfo()">Valider les informations</button>
            </div>
        </form>
    </div>
</div>

<style>
#btn_validate_info{
    width: 100%;
}
</style>

<script type="text/javascript">
function updateAccessInfo(){
    let accessInfos = {};

    $("#access-info :input").each(function() {
        let attr = $(this).attr("name");
        if (typeof attr !== typeof undefined && attr !== false) {
            accessInfos[attr] = $(this).val();
        }
    });

    ajax_call("api/access_info", "POST", accessInfos, function(result){
        if(result.code === '200'){
            changeStep(1);
        }else{
            console.log(e=result.data);
            bo_notify(result.message, 'danger');
        }
    });
}

    $(document).on("change", "#have_key", function(){
        $(this).val(($(this).prop("checked") ? 1 : 0));
        console.log($(this).val());
    });
</script>
