<?php
require_once ('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $validator = new Validator($_GET);
    $validator->is_id('w_list_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('w_list_id'));
        $json->printJson();
    }
    $w_id = $_GET['w_list_id'];
    $workflow = App::getWorkflow();

    $client_info = $workflow->getCustomerByWaitingId($w_id);
    if (!$client_info) {
        $json = parseJson::error('Un problème est survenu lors de la récupération des informations client');
        $json->printJson();
    }

    $workflow_step = $workflow->getStepById($client_info->workflow_id);
    if (!$workflow_step) {
        $client_info->step = 0;
    } else {
        $client_info->step = $workflow_step->step;
    }

    $json = parseJson::success(null,$client_info);
    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();

    $validator = new Validator($_PUT);
    $validator->is_id('w_list_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid()) {
        $json =  parseJson::error('Impossible de récupérer les informations du client');
        $json->printJson();
    }

    $workflow = App::getWorkflow();

    $customer = $workflow->getCustomerByWaitingId($_PUT['w_list_id']);
    if (!$customer) {
        $json =  parseJson::error('Impossible de récupérer le client');
        $json->printJson();
    }

    $step = $customer->workflow_id;
    if(isset($_PUT['action']) && !empty($_PUT['action'])){
        $action = $_PUT['action'];
        if($action === 'previous'){
            $step = $customer->workflow_id - 1;
        }else if($action === 'next'){
            $step = $customer->workflow_id + 1;
        }
    }

    $ret = $workflow->updateWaitingListStep($_PUT['w_list_id'], $step);
    if(!$ret){
        $json =  parseJson::error('Un problème est survenu lors de la modification de l\'étape du workflow');
        $json->printJson();
    }

    $ret = $workflow->updateWaitingListPending($_PUT['w_list_id'], 0);
    if(!$ret){
        parseJson::error('Impossible d\'enlever le client de l\'attente')->printJson();
    }

    $ret = $workflow->updateWaitingListDateListIn($_PUT['w_list_id']);
    if(!$ret){
        parseJson::error('Impossible de mettre la date d\'entré du client dans la liste d\'attente')->printJson();
    }

    $json = parseJson::success();
    $json->printJson();
}
