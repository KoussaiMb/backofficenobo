<?php
/**
* Created by PhpStorm.
* User: bienvenue
* Date: 13/09/2017
* Time: 17:00
*/

require_once ('../../../inc/bootstrap.php');
$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);
    $data = [];
    $dayCounter = 0;


    $validator->is_date_bo('date', "Mauvaise date", "pas de date");

    if (!$validator->is_valid()) {
        parseJson::error($validator->getError('date'))->printJson();
    }

    $dispos = app::getFullCalendar()->getEventsByCategoryFromDay(1, $_GET['date'], 1);
    $len = count($dispos);


    while (count($data) < 30 && $len > 0) {
        $dayDate = date("Y-m-d", strtotime("+$dayCounter days", strtotime($_GET['date'])));
        $tmpEvents = [];
        $possible = false;

        //delete Impossible elements
        $len = count($dispos = array_filter($dispos, function($e) use($dayDate) {
            return $e->end_rec >= $dayDate || is_null($e->end_rec);
        }));

        //If every dispo are impossible
        if ($len == 0) {
            break;
        }
        
        foreach ($dispos as $k => $v) {
            if ($v->start_rec <= $dayDate && ($v->end_rec >= $dayDate || is_null($v->end_rec)) && $v->day == strftime("%w", strtotime($dayDate))) {
                $event = clone $v; //because we use same event for differents dispo
                $event->event_date = $dayDate;
                array_push($tmpEvents, $event);
            }
        }

        if (count($data) + count($tmpEvents) < 30) {
            $data = array_merge($data, $tmpEvents);
        } else {
            break;
        }

        $dayCounter++;
    }

    parseJson::success('Les disponibilitées on bien été récuperées', $data)->printJson();

} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $validator = new Validator($_POST);

    $validator->is_id("event_id", "L'id de la disponibilité est mauvais", "L'id de la disponibilité est manquant");
    $validator->is_id("w_list_id", "L'id du client est invalide", "L'id du client est manquant");
    $validator->isToken("guest_token", "Le token du guest relation est invalide", "Le token du guest relation est manquant");
    $validator->is_date_bo('date', "Mauvaise date", "pas de date");
    $validator->is_stringRange('commentary', 'Le commentaire est invalide', 'Le commentaire est introuvable', [0,300]);

    if (!$validator->is_valid()) {
        parseJson::error("Impossible de récupérer les informations de la disponibilité")->printJson();
    }

    $fullCalendar = App::getFullCalendar();
    $customer_info = App::getWorkflow()->getCustomerByWaitingId($_POST['w_list_id']);
    $guest_info = App::getUser()->getUserByToken($_POST['guest_token']);
    $event = $fullCalendar->getEventById($_POST['event_id']);
    $event_start = new DateTime($event->start_rec);
    $event_end = new DateTime($event->end_rec);

    //delete first appointment
    $last_appointment = $fullCalendar->getEventsByAddressByCategory($customer_info->address_id, 1);
    foreach ($last_appointment as $k => $v) {
        if (!$fullCalendar->updateEventAvailability($v->id, 1))
            parseJson::error('Erreur lors du passage du rendez-vous en disponibilité')->printJson();
        if (!$fullCalendar->deleteEventForUser($v->id, $customer_info->user_id)){
            $fullCalendar->updateEventAvailability($v->id, 0);
            parseJson::error('Erreur lors de la suppression du lien entre la disponibilité et le client')->printJson();
        }
        if (!$fullCalendar->deleteEventForAddress($v->id, $customer_info->address_id)){
            $fullCalendar->addEvent($v->id, $customer_info->user_id);
            $fullCalendar->updateEventAvailability($v->id, 0);
            parseJson::error('Erreur lors de la suppression du lien entre la disponibilité et l\'adresse')->printJson();
        }

        $fullCalendar->updateEventCommentary($v->id, "Aucun commentaire");
    }

    if (!is_null($event->end_rec) && $event_start->diff($event_end, true)->format('%a') <= 7) {
        if (!$fullCalendar->updateEventAvailability($event->id, 0))
            parseJson::error('Erreur lors du passage de la disponibilité en rendez-vous')->printJson();
        if (!$fullCalendar->addEvent($customer_info->user_token, $event->id)){
            $fullCalendar->updateEventAvailability($event->id, 1);
            parseJson::error('Erreur lors de la liaison entre le client est la disponibilité')->printJson();
        }
        if (!$fullCalendar->addEventForAddress($customer_info->address_id, $event->id)){
            $fullCalendar->deleteEventForUser($event->id, $customer_info->user_id);
            $fullCalendar->updateEventAvailability($event->id, 1);
            parseJson::error('Erreur lors de la liaison entre l\'adresse est la disponibilité')->printJson();
        }
        $fullcalendar->updateEventTitle($event->id, "Premier rendez-vous avec: " . $customer_info->firstname . " " . $customer_info->lastname);
        $fullCalendar->updateEventCommentary($event->id, $_POST['commentary']);

        $event->event_date = $_POST['date'];
        $event->firstname = $guest_info->firstname;
        $event->lastname = $guest_info->lastname;

        parseJson::success('Le rendez-vous a bien été ajouté', $event)->printJson();
    } else {
        //insertion du rdv
        $start_rec = date("Y-m-d", strtotime("-1 days", strtotime($_POST['date'])));
        $end_rec = date("Y-m-d", strtotime("+1 days", strtotime($_POST['date'])));
        $newEvent_data = [
            "start" => $event->start,
            "end" => $event->end,
            "day" => $event->day,
            "start_rec" => $start_rec,
            "end_rec" => $end_rec,
            "category" => 1,
            "title" => "Premier rendez-vous avec: " . $customer_info->firstname . " " . $customer_info->lastname,
            "commentary" => $_POST['commentary']
        ];

        $newEvent_id = $fullCalendar->addNewEvent($newEvent_data);

        if (!$newEvent_id) 
            parseJson::error('Erreur lors de l\'ajout du rendez-vous')->printJson();
        if (!$fullCalendar->addEvent($_POST['guest_token'], $newEvent_id)){
            $fullcalendar->deleteEventById($newEvent_id);
            parseJson::error('Erreur lors de la liaison entre le rendez-vous et le guest relation')->printJson();
        }
        if (!$fullCalendar->addEvent($customer_info->user_token, $newEvent_id)){
            $fullcalendar->deleteEventById($newEvent_id);
            parseJson::error('Erreur lors de la liaison entre le rendez-vous et le client')->printJson();
        }
        if (!$fullCalendar->addEventForAddress($customer_info->address_id, $newEvent_id)){
            $fullcalendar->deleteEventById($newEvent_id);
            parseJson::error('Erreur lors de la liaison entre le rendez-vous et l\'adresse')->printJson();
        }
        $newEvent = $fullCalendar->getEventById($newEvent_id);
        $newEvent->event_date = $_POST['date'];
        $newEvent->firstname = $guest_info->firstname;
        $newEvent->lastname = $guest_info->lastname;

        //rescale dispo
        if ($event_start->diff(new DateTime($_POST['date']))->format('%a') <= 7) {
            $event->start_rec = date("Y-m-d", strtotime("+7 days", strtotime($event->start_rec)));
            if (!$fullcalendar->updateEvent((array)$event))
                    parseJson::error('Erreur lors de la modification du créneau', $e)->printJson();
        } elseif ($event_end->diff(new DateTime($_POST['date']))->format('%a') <= 7 && !is_null($event->end_rec)) {
            $event->end_rec = date("Y-m-d", strtotime("-7 days", strtotime($event->end_rec)));
            if (!$fullcalendar->updateEvent((array)$event))
                    parseJson::error('Erreur lors de la modification du créneau', $e)->printJson();
        } else {
            $newDispo_data = [
                "start" => $event->start,
                "end" => $event->end,
                "day" => $event->day,
                "start_rec" => date("Y-m-d", strtotime("+1 days", strtotime($_POST['date']))),
                "end_rec" => $event->end_rec,
                "category" => 1,
                "available" => 1,
                "commentary" => $event->commentary,
                "title" => $event->title
            ];
            $newDispo_id = $fullCalendar->addNewEvent($newDispo_data);
            
            if (!$newDispo_id){
                $fullcalendar->deleteEventById($newEvent_id);
                parseJson::error('Erreur lors de l\'ajout d\'une nouvelle disponibilité')->printJson();
            }
            if (!$fullCalendar->addEvent($_POST['guest_token'], $newDispo_id)){
                $fullcalendar->deleteEventById($newEvent_id);
                $fullcalendar->deleteEventById($newDispo_id);
                parseJson::error('Erreur lors de la liaison entre la nouvelle disponibilité et le guest relation')->printJson();
            }
            $event->end_rec = date("Y-m-d", strtotime("-1 days", strtotime($_POST['date'])));
            if (!$fullCalendar->updateEvent((array)$event)){
                $fullcalendar->deleteEventById($newEvent_id);
                $fullcalendar->deleteEventById($newDispo_id);
                parseJson::error('Erreur lors de la modification des disponibilités')->printJson();
            }
        }

        parseJson::success('Le rendez-vous a bien été ajouté', $newEvent)->printJson();
    }

    parseJson::error('Un problème inconnue est survenue')->printJson();

} else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    $_PUT = App::getRequest();

    $validator = new Validator($_PUT);

    $validator->is_id("event_id", "L'id de la disponibilité est mauvais", "L'id de la disponibilité est manquant");
    $validator->is_stringRange('commentary', 'Le commentaire est invalide', 'Le commentaire est introuvable', [0,300]);

    if (!$validator->is_valid()) {
        parseJson::error("Impossible de récupérer les informations du premier rendez-vous", $validator->getErrors())->printJson();
    }

    if (!$fullcalendar->updateEventCommentary($_PUT['event_id'], $_PUT['commentary'])) {
        parseJson::error("Erreur lors du changement du commentaire")->printJson();
    }

    parseJson::success("Le rendez-vous à bien été modifié")->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {

    $_DELETE = App::getRequest();

    $validator = new Validator($_DELETE);

    $validator->is_id("event_id", "L'id de la disponibilité est mauvais", "L'id de la disponibilité est manquant");
    $validator->is_id("w_list_id", "L'id du client est invalide", "L'id du client est manquant");

    if (!$validator->is_valid()) {
        parseJson::error("Impossible de récupérer les informations du premier rendez-vous", $validator->getErrors())->printJson();
    }

    if (!$fullcalendar->updateEventAvailability($_DELETE['event_id'], 1)) {
        parseJson::error("Erreur lors du changement du rendez-vous en disponibilité")->printJson();
    }

    if (!$fullcalendar->updateEventCommentary($_DELETE['event_id'], "Aucun commentaire")) {
        $fullcalendar->updateEventAvailability($_DELETE['event_id'], 0);
        parseJson::error("Erreur lors du changement de la suppression du commentaire")->printJson();
    }

    if (!$fullcalendar->updateEventTitle($_DELETE['event_id'], "Disponibilité pour premier rendez-vous")){
        $fullcalendar->updateEventAvailability($_DELETE['event_id'], 0);
        parseJson::error("Erreur lors du changement du rendez-vous en disponibilité")->printJson();
    }


    $customer_info = App::getWorkflow()->getCustomerByWaitingId($_DELETE['w_list_id']);

    if (!$customer_info) {
        $fullcalendar->updateEventAvailability($_DELETE['event_id'], 0);
        parseJson::error("Erreur lors de la récupération des informations du client")->printJson();
    }

    if (!$fullcalendar->deleteEventForAddress($_DELETE['event_id'], $customer_info->address_id)) {
        $fullcalendar->updateEventAvailability($_DELETE['event_id'], 0);
        parseJson::error("Erreur lors de la suppression de l'évenement pour l'adresse")->printJson();        
    }

    if (!$fullcalendar->deleteEventForUser($_DELETE['event_id'], $customer_info->user_id)) {
        $fullcalendar->updateEventAvailability($_DELETE['event_id'], 0);
        parseJson::error("Erreur lors du changement de la suppression du rendez-vous pour le client")->printJson();
    }

    parseJson::success("Le rendez-vous à bien été supprimé")->printJson();
}
