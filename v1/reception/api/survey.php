<?php
require_once ('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $color_scores = array_fill(0, 5, 0);
    $data = array();
    parse_str($_POST['form_data'], $data);

    $surveyClass = App::getSurvey();
    $validator = new Validator($data);

    /* We get all the questions of the survey */
    $questions = $surveyClass->getQuestionsBySurveyName($data['survey_name']);
    if (!$questions) {
        $json = parseJson::error('Impossible de récupérer les questions');
        $json->printJson();
    }

    $validator->is_id('user_id', 'Client invalide', 'Client introuvable');
    $validator->is_id('w_list_id', 'Impossible de récupérer le client dans la waiting list', 'Identifiant de la waiting_list vide');
    $validator->is_id('w_step_id', 'Impossible de récupérer l\'étape du workflow', 'Etape du workflow vide');

    if (!$validator->is_valid()) {
        $json = parseJson::error('Impossible de récupérer un des IDs');
        $json->printJson();
    }

    $total_score = 0;
    $it_questions = sizeof($questions);
    for ($i = 0; $i < $it_questions; $i++) {
        $q = $questions[$i];
        $ans_name = 'answer_' . $q->id;

        $r = $surveyClass->getTypeByQuestionId($q->id);
        if(!$r){
            $json = parseJson::error('Impossible de récupérer le type de la question');
            $json->printJson();
        }
        $type = $r->field;

        $field_exists = null;
        $field = $surveyClass->getTableAndColumnByRefId($q->field_ref_id);
        if($field) {
            $field_exists = true;
        }else if ($field == 0){
            $field_exists = false;
        }else{
            $json = parseJson::error('Impossible de récupérer le field d\'une des questions');
            $json->printJson();
        }

        /* For each question we check if an answer was given by the user */
        if (isset($data[$ans_name]) && !empty($data[$ans_name])) {
            $curr_answer = $data[$ans_name];

            if($type == 'date' || $type == 'number'){
                if($field_exists){
                    $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, $curr_answer, $data['user_id']);
                }else{
                    $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, null, $curr_answer);
                    if(!$ret){
                        $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, null, $curr_answer);
                    }
                }
                if(!$ret){
                    $json = parseJson::error('Impossible d\'insérer une date ou un nombre');
                    $json->printJson();
                }
            }else if($type == 'text') {
                /* We insert the text answer in the answer table */
                $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, $curr_answer);
                if(!$ret){
                    $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, $curr_answer);
                    if(!$ret){
                        $json = parseJson::error('Impossible d\'insérer la réponse text');
                        $json->printJson();
                    }
                }
                if($field_exists){
                    $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, "Texte", $data['user_id']);
                    if (!$ret) {
                        $json = parseJson::error('Impossible d\'insérer la valeur du champs Texte dans les détails du client');
                        $json->printJson();
                    }
                }
            }else if($type == 'radio') {
                $curr_offered_answer = $surveyClass->getOfferedAnswerById($curr_answer);

                $score = $surveyClass->getScoreByOfferedAnswerId($curr_offered_answer->id);
                if (!$score) {
                    $json = parseJson::error('Impossible de récupérer le score du bouton radio');
                    $json->printJson();
                }
                $total_score += $score->score;
                $color_scores[$score->score] += 1;

                if($field_exists){
                    $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, $curr_offered_answer->answer_text, $data['user_id']);
                }else{
                    $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, $curr_offered_answer->answer_text, $curr_answer);
                    if(!$ret){
                        $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, $curr_offered_answer->answer_text, $curr_answer);
                    }
                }
                if (!$ret) {
                    $json = parseJson::error('Impossible d\'insérer la valeur référencée par un bouton radio');
                    $json->printJson();
                }
            }else if($type == 'select') {
                $curr_offered_answer = $surveyClass->getOfferedAnswerById($curr_answer);

                /* The field "Autre" was not selected by the user */
                if (!isset($data['other_answer_' . $q->id])) {
                    /* We insert the answer in the customer details table */
                    $score = $surveyClass->getScoreByOfferedAnswerId($curr_offered_answer->id);
                    if (!$score) {
                        $json = parseJson::error('Impossible de récupérer le score du select');
                        $json->printJson();
                    }
                    $total_score += $score->score;
                    $color_scores[$score->score] += 1;

                    if($field_exists){
                        $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, $curr_offered_answer->answer_text, $data['user_id']);
                    }else{
                        $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, $curr_offered_answer->answer_text, $curr_offered_answer->id);
                        if(!$ret){
                            $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, $curr_offered_answer->answer_text, $curr_offered_answer->id);
                        }
                    }
                    if (!$ret) {
                        $json = parseJson::error('Impossible d\'insérer la valeur référencée par le select');
                        $json->printJson();
                    }
                }else {
                    /* We insert the answer in the answer table and "Autre" in the customer_details table */
                    $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, $data['other_answer_' . $q->id], $curr_answer);
                    if (!$ret) {
                        $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, $data['other_answer_' . $q->id], $curr_answer);
                    }
                    if($field_exists){
                        $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, "Autre", $data['user_id']);
                        if (!$ret) {
                            $json = parseJson::error('Impossible d\'insérer la valeur du champs Autre du select');
                            $json->printJson();
                        }
                    }
                }
            }else if($type == 'checkbox') {
                $it_checkbox = sizeof($curr_answer);
                /* If there is only one element we insert it in the customer_details table */
                if($it_checkbox == 1) {
                    $offered_answer = $surveyClass->getOfferedAnswerById($curr_answer[0]);
                    $score = $surveyClass->getScoreByOfferedAnswerId($offered_answer->id);
                    if (!$score) {
                        $json = parseJson::error('Impossible de récupérer le score des checkbox');
                        $json->printJson();
                    }
                    $total_score += $score->score;
                    $color_scores[$score->score] += 1;

                    if($field_exists){
                        $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, $offered_answer->answer_text, $data['user_id']);
                    }else{
                        $ret = $surveyClass->updateAnswer($data['user_id'], $q->id, null, $offered_answer->id);
                        if(!$ret){
                            $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, null, $offered_answer->id);
                        }
                    }
                    if (!$ret) {
                        $json = parseJson::error('Impossible d\'insérer la valeur référencée');
                        $json->printJson();
                    }
                }else {
                    $surveyClass->deleteAnswersByQuestionAndUserId($q->id, $data['user_id']);
                    /* We insert all the values in the answer table and "Multiple" in the customer_details table */
                    for($j = 0; $j < $it_checkbox; $j++) {
                        $check_answer = $curr_answer[$j];
                        $offered_answer = $surveyClass->getOfferedAnswerById($check_answer);

                        $ret = $surveyClass->insertAnswer($data['user_id'], $q->id, $offered_answer->answer_text, $check_answer);
                        if ($ret) {
                            $score = $surveyClass->getScoreByAnswerId($ret);
                            if (!$score) {
                                $json = parseJson::error('Impossible de récupérer le score des checkbox');
                                $json->printJson();
                            }
                            $total_score += $score->score;
                            $color_scores[$score->score] += 1;
                        }
                    }
                    if($field_exists){
                        $ret = $surveyClass->insertValueWithTableAndColumn($field->table_name, $field->field_name, "Multiple", $data['user_id']);
                        if (!$ret) {
                            $json = parseJson::error('Impossible d\'insérer la valeur référencée');
                            $json->printJson();
                        }
                    }
                }
            }
        }
    }

    $thresholds_refusal = App::getSurvey()->getThresholdBySurveyName($data['survey_name'], 'refusal');
    $thresholds_waiting = App::getSurvey()->getThresholdBySurveyName($data['survey_name'], 'waiting');
    $thresholds_accept = App::getSurvey()->getThresholdBySurveyName($data['survey_name'], 'acceptance');

    if($thresholds_refusal === false || $thresholds_waiting === false || $thresholds_accept === false){
        parseJson::error('Impossible de récupérer les seuils')->printJson();
    }

    $thresholds_refusal = empty($thresholds_refusal) ? array_fill(0, 5, 0) : $thresholds_refusal;
    $thresholds_waiting = empty($thresholds_waiting ) ? array_fill(0, 5, 0) : $thresholds_waiting ;
    $thresholds_accept = empty($thresholds_accept) ? array_fill(0, 5, 0) : $thresholds_accept;

    $thresholds = ["refusal" => $thresholds_refusal, "waiting" => $thresholds_waiting, "acceptance" => $thresholds_accept];

    $toReturn = array("total_score" => $total_score, "color_score" => $color_scores, "thresholds" => $thresholds);

    $json = parseJson::success(null,$toReturn);
    $json->printJson();
}
