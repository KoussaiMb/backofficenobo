<?php
require_once('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $validator = new Validator($_POST);

    $validator->is_id("address_id", "L'id de l'adresse est invalide", "L'id de l'adresse est vide");
    if(!$validator->is_valid()){
        $json = parseJson::error('Adresse invalide', $validator->getErrors());
        $json->printJson();
    }

    $validator->is_id('w_list_id', 'Pas d\'id client', 'Id client incorrect');
    $validator->is_id('w_step_id', 'L\'id n\'existe pas', 'L\'id est incorrect');
    $validator->no_symbol('batiment', 'Le batiment n\'est pas valide');
    $validator->is_digicode('digicode', 'Le digicode 1 n\'est pas valide');
    $validator->is_digicode('digicode_2', 'Le digicode 2 n\'est pas valide');
    $validator->no_symbol('doorBell', 'L\'interphone n\'est pas valide');
    $validator->no_symbol('door', 'La porte n\'est pas valide');
    $validator->no_symbol('floor', 'L\'etage n\'est pas valide');

    if(!$validator->is_valid()){
        $json = parseJson::error('Données d\'accès invalides', $validator->getErrors());
        $json->printJson();
    }

    $w_id = $_POST['w_list_id'];
    $w_step = $_POST['w_step_id'];

    $receptionClass = App::getReception();
    $ret = $receptionClass->updateBuildingAccessInfo($_POST);
    if(!$ret){
        $json = parseJson::error('Insertion des données d\'accès impossible', $validator->getErrors());
        $json->printJson();
    }

    $validator->is_num('surface', 'La surface n\'est pas valide', 'La surface n\'a pas été renseignée');
    $validator->is_num('roomNb', 'Le nombre de chambre n\'est pas valide');
    $validator->is_num('waterNb', 'Le nombre de salle d\'eau n\'est pas valide');
    $validator->is_num('wcNb', 'Le nombre de WC n\'est pas valide');
    $validator->is_id('l_pet_id', 'La liste d\'animaux n\'est pas valide');
    $validator->is_num('childNb', 'Le nombre d\'enfants n\'est pas valide');
    $validator->is_num('have_key', 'Le besoin des clés n\'est pas valide');
    $validator->is_num('peopleHome', 'Le nombre de personnes au domicile n\'est pas valide');

    if(!$validator->is_valid()){
        $json = parseJson::error('Les données de l\'adresse ne sont pas bonnes', $validator->getErrors());
        $json->printJson();
    }

    $ret = $receptionClass->updateAddressInfoById($_POST);
    if(!$ret){
        $json = parseJson::error('Erreur lors de l\'insertion des informations sur le batiment', $validator->getErrors());
        $json->printJson();
    }

    $json = parseJson::success('Modification du client réussie');
    $json->printJson();
}
