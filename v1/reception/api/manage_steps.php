<?php
require_once ('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    parse_str(file_get_contents('php://input'), $_PUT);
    $validator = new Validator($_PUT);

    $validator->is_id('w_list_id', 'Identifiant du client invalide', 'Identifiant du client non renseigné');
    if(!$validator->is_valid()){
        parseJson::error('Impossible de récuperer les infos clients')->printJson();
    }
    $ret = App::getWorkflow()->updateWaitingListAlerte($_PUT['w_list_id'], 1);
    if(!$ret){
        parseJson::error("Impossible de mettre le client en attente")->printJson();
    }
    parseJson::success("Le client a été mis en attente")->printJson();
}
