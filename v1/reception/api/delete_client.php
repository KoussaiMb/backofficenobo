<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();

    $validator = new Validator($_DELETE);

    $validator->is_id('w_list_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('w_list_id'));
        $json->printJson();
    }

    if (!isset($_DELETE['raisons'])) {
        $json = parseJson::error('Impossible de récupérer les raisons');
        $json->printJson();
    }

    $customerClass = App::getCustomer();

    $customer = App::getWorkflow()->getCustomerByWaitingId($_DELETE['w_list_id']);
    if(!$customer){
        $json = parseJson::error('Impossible de récupérer le client');
        $json->printJson();
    }

    $deleted = App::getUser()->deleteUserById($customer->user_id);
    if (!$deleted) {
        $json = parseJson::error('Erreur lors de la suppression du client!');
        $json->printJson();
    }

    $stop_nobo = $customerClass->getStopNoboByUserId($customer->user_id);
    if (!$stop_nobo) {
        $json = parseJson::error('Impossible de récupérer la dernière supression');
        $json->printJson();
    }

    $raisons = json_decode($_DELETE['raisons']);
    foreach ($raisons as $k => $v) {
        if ($v->checked) {
            $ret = $customerClass->insertStopRaison($stop_nobo->id, $v->id, $v->comment);

            if (!$ret) {
                $json = parseJson::error("Un problème est survenu lors de l'ajout des raisons");
                $json->printJson();
            }
        }
    }

    $json = parseJson::success("le client à correctement été supprimé.", $stop_nobo);
    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $list_reason = App::getListManagement()->getList('deleteReason');
    if(!$list_reason){
        $json = parseJson::error('Impossible de récupérer la liste des raisons');
        $json->printJson();
    }
    $json = parseJson::success(null,$list_reason);
    $json->printJson();
}
