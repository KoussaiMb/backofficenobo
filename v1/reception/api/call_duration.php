<?php
/**
 * file: action_history.php
 * auhtor: Jonathan BRICE
 * date: 06-04-2018
 * description:
 **/

require_once ('../../../inc/bootstrap.php');
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('c_id', 'L\'id est invalide', 'L\'id est manquant');
    $validator->is_timeFormat('duration', 'La durée est invalide', 'La durée est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error("La durée est invalide", $validator->getErrors());
        $json->printJson();
    }

    $ret = App::getCustomer()->updateCallDurationByCustomerId($_PUT['c_id'], $_PUT['duration']);

    if (!$ret) {
        $json = parseJson::error("Erreur lors de la modification de la durée d'appel");
        $json->printJson();
    }

    $json = parseJson::success("La durée d'appel à correctement été modifiée.");
    $json->printJson();
}
