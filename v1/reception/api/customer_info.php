<?php
require_once ('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $is_complete = true;
    $validator = new Validator($_POST);
    $w_id = $_POST['w_list_id'];
    $w_step = $_POST['w_step_id'];

    $validator->is_id('w_list_id', 'Pas d\'id client', 'Id client incorrect');
    $validator->is_id('w_step_id', 'L\'id n\'existe pas', 'L\'id est incorrect');
    $validator->isToken('user_token', 'Le token utilisateur n\'est pas valide', 'Le token utilisateur n\'a pas été renseigné');
    $customer = App::getWorkflow()->getCustomerByWaitingId($w_id);

    if ($validator->is_valid() === false)
        parseJson::error('Données de l\'utilisateur invalides', $validator->getErrors())->printJson();
    else if ($customer === false)
        parseJson::error('Erreur lors de la récupération du client', $validator->getErrors())->printJson();
    //update user
    $receptionClass = App::getReception();
    $ret = App::getReception()->updateUserByToken($_POST);
    if ($ret === false)
        parseJson::error('Erreur lors de l\'insertion de l\'utilisateur', $validator->getErrors())->printJson();
    //update adresse
    $validator->is_id('address_id', 'L\'id de l\'adresse n\'est pas valide', 'L\'id de l\'adresse n\'a pas été renseigné');
    if ($validator->is_valid())
        $addressUpdate = $receptionClass->updateAddressById($_POST);
    if (!isset($addressUpdate) || $addressUpdate === false)
        $validator->throwException('addressUpdate', 'Erreur lors de l\'insertion de l\'adresse');
    //update recurrence
    $validator_rec = new Validator($_POST);
    $validator_rec->is_id('select_frequency', 'Impossible de récupérer le type de fréquence');
    $validator_rec->is_num('nbHours', 'Impossible de récupérer le nombre d\'heure par prestation');
    if ($validator_rec->is_valid())
        $updateRec = $receptionClass->updateRecurrenceType($w_id, $_POST['select_frequency'], $_POST['nbHours']);
    if (!isset($updateRec) || $updateRec === false)
        $validator_rec->throwException('updateRec', 'Erreur lors de la modification de la recurrence du client');
    //update acquisition
    $validator_acquisition = new Validator($_POST);
    $validator_acquisition->is_id('select_acquisition', 'Impossible de récupérer le type d\'acquisition');
    if ($validator_acquisition->is_valid())
        $updateAcquisition = $receptionClass->updateAcquisitionByCustomerId($customer->id, $_POST['select_acquisition']);
    if (!isset($updateAcquisition) || $updateAcquisition === false)
        $validator_acquisition->throwException('updateAcquisition', 'Erreur lors de la modification de la recurrence du client');

    $errors = array_merge(
        $validator->getErrors(),
        $validator_rec->getErrors(),
        $validator_acquisition->getErrors()
    );
    if (!empty($errors))
        parseJson::error('Une errreur s\'est produit lors de la mise à jour de la fiche client', $errors)->printJson();

    $validator_null = new Validator($_POST);
    $validator_null->is_not_null('address', 'toto', 'toto');
    $validator_null->is_not_null('select_frequency', 'toto', 'toto');
    $validator_null->is_not_null('nbHours', 'toto', 'toto');
    $validator_null->is_not_null('select_acquisition', 'toto', 'toto');
    parseJson::success('Modification du client réussie',  $validator_null->is_valid() !== false)->printJson();
}