<?php
require_once ('../../../inc/bootstrap.php');

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('w_id', 'Pas d\'id client', 'Id client incorrect');
    $validator->is_id('step', 'L\'id n\'existe pas', 'L\'id est incorrect');
    $validator->is_alphanum('wf_name', 'Le nom du workflow n\'existe pas', 'Le nom du workflow est incorrect');
    if (!$validator->is_valid()) {
        $json = parseJson::error("les informations sont incorrects", $validator->getErrors());
        $json->printJson();
    }
    $workflow = App::getWorkflow();
    $step = $workflow->getStepByStepAndName($_PUT['step'], $_PUT['wf_name']);
    $user = $workflow->getCustomerByWaitingId($_PUT['w_id']);
    $ret = 1;
    if (!isset($step) || !$step || empty($step)) {
        $workflow->updateWaitingListDateListOut($_PUT['w_id']);
        $workflow->updateWaitingListAccepted($_PUT['w_id'], 1);
        $step = false;
    } else {
        $ret = $workflow->updateUserStep($user->user_id, $step->id);
    }

    $customer = App::getWorkflow()->getCustomerByWaitingId($_PUT['w_id']);
    if(!$customer){
        $json = parseJson::error('Erreur lors de la récupération du client', $validator->getErrors());
        $json->printJson();
    }

    $customer->hasCard = App::getCustomer()->hasPaymentAccount($customer->user_token);
    $customer->date_list_in = parseStr::DatetimeToDate($customer->date_list_in) . " à " . parseStr::DatetimeToTime($customer->date_list_in);

    if ($ret == 1){
        $json = parseJson::success("Changement d'étape pour le client réussi", ["step_info" => $step, "customer_info" => $customer]);
    } else {
        $json = parseJson::error("Il y a eu un problème lors du changement d'étape du client");
    }
    $json->printJson();
}
