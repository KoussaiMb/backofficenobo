<?php
/**
 * file: action_history.php
 * auhtor: Jonathan BRICE
 * date: 06-04-2018
 * description:
 **/

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);

    $validator->is_id('customerAction', 'L\'action n\'existe pas', 'L\'action est manquante');
    $validator->isToken('from_token', 'Le token est invalide', 'Le token est manquant');
    $validator->isToken('to_token', 'Le token est invalide', 'Le token est manquant');
    $validator->is_range('message', 'Le message est invalide', [0, 300]);
    $validator->is_num('call_request_id', 'L\'id du type de rappel est invalide', 'L\'id du type de rappel est manquant');
    if (!$validator->is_valid()) 
        parseJson::error("Les informations sont incorrects", $validator->getErrors())->printJson();

    $customerAction = $_POST['customerAction'];

    $from_token = $_POST['from_token'];
    $to_token = $_POST['to_token'];
    $message = $_POST['message'];
    $from_id = App::getUSer()->getUserByToken($from_token)->id;
    $to_id = App::getUSer()->getUserByToken($to_token)->id;

    $action_history_id = App::getCustomerActions()->addActionHistory($customerAction, $from_id, $to_id, $message);
    if ($action_history_id == 0)
        parseJson::error('Erreur lors de l\'ajout de l\'action dans l\'historique')->printJson();

    $call_request_id = $_POST['call_request_id'];

    if ($call_request_id != 0) {
        $validator->is_date_bo('call_date', 'La date de rappel est invalide', 'La date de rappel est introuvable');
        if (!$validator->is_valid())
            parseJson::error($validator->getError('call_date'))->printJson();
        $call_date = $_POST["call_date"];
        $call_time = null;

        $validator->is_timeFormat('call_time', 'L\'heure de rappel est invalide', 'L\'heure de rappel est introuvable');
        if ($validator->is_valid())
            $call_time = $_POST['call_time'];

        $ret = App::getCustomerActions()->addCallRequest($action_history_id, $call_date, $call_time, $call_request_id);
    }

    $action_info = App::getCustomerActions()->getActionInfoById($action_history_id);

    parseJson::success('L\'action a bien été ajouté dans l\'historique', $action_info)->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->isToken('to_token', 'Le token est invalide', 'Le token est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('to_token'));
        $json->printJson();
    }

    $history = App::getCustomerActions()->getActionHistoryByToToken($_GET['to_token']);

    if ($history === false) {
        $json = parseJson::error('Erreur lors de la récupération de l\'action');
    } else {
        $json = parseJson::success('L\'action a bien été récuperé', $history);
    }
    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('action_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid())
        parseJson::error($validator->getError('action_id'))->printJson();

    $action_id = $_PUT['action_id'];

    $validator->no_symbol('message', 'Le message est invalide', 'Le message est introuvable');
    if ($validator->is_valid()) {
        if(App::getCustomerActions()->editActionHistory($action_id, $_PUT['message']) === 0)
            parseJson::error('Erreur lors de la modification du commentaire de l\'action dans l\'historique')->printJson();
    }

    //re-inti validator because 'message' can be not valid and 'done' can be valid
    $validator = new Validator($_PUT);

    $validator->is_num('done', 'La validation de la demande de rappel est invalide', 
                        'La validation de la demande de rappel est introuvable');
    if ($validator->is_valid()){
        $action_info = App::getCustomerActions()->getActionInfoById($_PUT['action_id']);

        if ($_PUT['done'] == 1) {
            if (!App::getCustomerActions()->terminateCallRequestById($action_info->request_id))
                parseJson::error('Erreur lors de la validation de la demande de rappel')->printJson();
        } else {
            if (!App::getCustomerActions()->cancelCallRequestById($action_info->request_id))
                parseJson::error('Erreur lors de l\'annulation de la demande de rappel')->printJson();
        }
    }
        
    parseJson::success('L\'action a bien été modifié dans l\'historique')->printJson();

} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('action_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('action_id'));
        $json->printJson();
    }
    $ret = App::getCustomerActions()->deleteActionHistory($_DELETE['action_id']);

    if($ret === 0){
        $json = parseJson::error('Erreur lors de la suppression de l\'action dans l\'historique');
    } else {
        $json = parseJson::success('L\'action a bien été supprimé dans l\'historique', $ret);
    }
    $json->printJson();
}
