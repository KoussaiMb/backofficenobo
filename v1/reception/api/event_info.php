<?php

require_once ('../../../inc/bootstrap.php');
$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_id("event_id", "L'id de l'event est invalide", "l'id de l'event est introuvable");
    if (!$validator->is_valid())
        parseJson::error($validator->getError("event_id"))->printJson();

    $event = $fullcalendar->getEventById($_GET['event_id']);

    if (!$event)
        parseJson::error("impossible de récupérer les informations de l'event")->printJson();

    $start_time = explode(":", $event->start);
    $end_time = explode(":", $event->end);
    $nextday = date('Y-m-d H:i:s', strtotime('+'.($event->day)." days", strtotime('last Sunday', strtotime($event->start_rec))));
    $address = $fullcalendar->getFirstAddressByEventId($event->id);
    $attendees = $fullcalendar->getUserTokensByEventId($event->id);
    $startTime = date('Y-m-d\TH:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
    $endTime = date('Y-m-d\TH:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
    $event_info = (object) array(
        "start" => $startTime,
        "end" => $endTime,
        "id" => $event->id,
        "title" => $event->title,
        "commentary" => $event->commentary,
        "available" => $event->available == 1,
        "attendees" => $attendees ? $attendees : [],
        "address" => $address ? App::getAddress()->addressToString($address->id) : "Aucune adresse",
        "address_id" => $address ? $address->id : 0
    );
    
    parseJson::success('Les disponibilitées on bien été récuperées', $event_info)->printJson();
}
