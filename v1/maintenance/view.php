<?php
require('../../inc/bootstrap.php');
$mps = App::getMaintenancePlanningType()->getAll();
require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Liste des plannings de maintenance
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-12">
                <p><a href="/v1/list/maintenance-type/edit" target="_blank">Créer un type d'entretien</a></p>
            </div>
            <div class="col-xs-12">
                <?php foreach ($mps as $k => $mp): ?>
                    <div class="col-xs-3">
                        <a href="edit?id=<?= $mp->id; ?>" class="no-decoration">
                            <div class="mp-box">
                                <h5><?= $mp->name; ?></h5>
                                <p>De <?= $mp->min_area; ?>m² à <?= $mp->max_area; ?>m²</p>
                                <?= $mp->ironing == 1 ? "<p>Avec repassage</p>" : ""; ?>
                                <?= $mp->childs == 1 ? "<p>Avec enfants</p>" : ""; ?>
                                <?= empty($mp->recurrence) ? "" : "<p>" .$mp->recurrence. "</p>" ; ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
                <div class="col-xs-3">
                    <a href="add">
                        <button type="button" class="btn add-mp"><span class="glyphicon glyphicon-plus"></span></button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <style>
        .italic {
            font-style: italic;
        }
        .no-decoration {
            text-decoration: none!important;
        }
        .mp-box p {
            margin-top: 10px!important;
            margin-bottom: 0;
            color: #888888;
        }
        .mp-box h4, .mp-box p {
            margin-top: 0;
        }
        .mp-box {
            height: 150px;
            cursor: pointer;
            border: 1px solid rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            padding: 15px;
            text-align: center;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-content: stretch;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }
        .add-mp:hover, .mp-box:hover {
            color: inherit;
            border: 1px solid rgba(0, 0, 0, 0.3);
        }
        .add-mp {
            height: 150px;
            width: 100%;
            box-shadow: none;
            outline: 0;
            -webkit-box-shadow: none;
            background-image: none;
        }
        .add-mp > span{
            font-size: 3em;
        }
    </style>
<?php require('../../inc/footer_bo.php');