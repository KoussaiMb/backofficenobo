<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 08/08/2018
 * Time: 12:52
 */
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);
    $validator->is_id('mp_template_id', 'Le planning type est invalide', 'Le planning de maintenance est manquant');
    if ($validator->is_valid() === false)
        parseJson::error("Les données sont erronnées pour ajouter un type de planning", $validator->getErrors())->printJson();
    $mp_unit_id = App::getMaintenancePlanningType()->insertMpUnit($_POST['mp_template_id']);
    if ($mp_unit_id === false)
        parseJson::error("Impossible d'ajouter un nouveau type de planning")->printJson();
    parseJson::success("Un nouveau type de planning a été ajouté", ['mp_unit_id' => $mp_unit_id])->printJson();
} elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);
    $validator->is_id('mp_unit_id', 'Le planning type est invalide', 'Le planning type est manquant');
    if ($validator->is_valid() === false)
        parseJson::error("Les données sont erronnées pour supprimer le planning type", $validator->getErrors())->printJson();
    $success = App::getMaintenancePlanningType()->deleteMpUnitById($_DELETE['mp_unit_id']);
    if ($success === false)
        parseJson::error("Impossible de supprimer le planning type")->printJson();
    parseJson::success("Le planning type a été supprimé")->printJson();
} elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);
    $validator->is_id('mp_unit_id', 'Le planning type est invalide', 'Le planning type est manquant');
    $validator->is_id('l_maintenanceType_id', 'Le type de planning est invalide');
    if ($validator->is_valid() === false)
        parseJson::error("Les données sont erronnées", $validator->getErrors())->printJson();
    $rowCount = App::getMaintenancePlanningType()->editMpUnit($_PUT['mp_unit_id'], $_PUT['l_maintenanceType_id']);
    if ($rowCount === false)
        parseJson::error("Impossible d'éditer le type de planning")->printJson();
    parseJson::success("Le type de planning a été modifié")->printJson();
}
