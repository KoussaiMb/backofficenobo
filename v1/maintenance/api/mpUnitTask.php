<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 08/08/2018
 * Time: 12:52
 */
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);
    $validator->is_id('mp_unit_id', 'Le planning type est invalide', 'Le planning type est manquant');
    $validator->is_id('l_rollingTask_id', 'La tâche roulante est invalide', 'La tâche roulante est manquante');
    if ($validator->is_valid() === false)
        parseJson::error("Les données sont erronnées pour ajouter une tâche roulante", $validator->getErrors())->printJson();
    $mp_unit_task_id = App::getMaintenancePlanningType()->insertMpUnitTask($_POST['mp_unit_id'], $_POST['l_rollingTask_id']);
    if ($mp_unit_task_id === false)
        parseJson::error("Impossible d'ajouter la tâche roulante")->printJson();
    parseJson::success("La tâche roulante a été ajoutée", ['mp_unit_task_id' => $mp_unit_task_id])->printJson();
} elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);
    $validator->is_id('mp_unit_task_id', 'Le planning type est invalide', 'Le planning type est manquant');
    if ($validator->is_valid() === false)
        parseJson::error("Les données sont erronnées pour supprimer une tâche roulante", $validator->getErrors())->printJson();
    $success = App::getMaintenancePlanningType()->deleteMpUnitTaskById($_DELETE['mp_unit_task_id']);
    if ($success === false)
        parseJson::error("Impossible de supprimer la tâche roulante")->printJson();
    parseJson::success("La tâche roulante a été supprimée")->printJson();
}