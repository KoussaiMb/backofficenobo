<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 02/08/2018
 * Time: 15:56
 */
require('../../inc/bootstrap.php');

if (empty($_POST['action'])){
    App::redirect('view');
} elseif ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);

    $validator->no_symbol('name', 'Le nom est invalide', 'Le nom est introuvable ');
    $validator->is_num('min_area', 'le nombre min de la superficie est invalide', 'le nombre min de la superficie est manquant');
    $validator->is_num('max_area', 'le nombre max de la superficie est invalide', 'le nombre max de la superficie est manquant');
    $validator->is_id('ironing', 'L\'information sur le repassage est invalide');
    $validator->is_id('childs', 'L\'information sur le repassage est invalide');
    $validator->is_id('l_recurrence_id', 'L\'information sur la récurrence est invalide');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'add');
    $mp_id = App::getMaintenancePlanningType()->addMpType($_POST);
    if ($mp_id === false)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout du planning d\'entretien', 'add');
    App::setFlashAndRedirect('success', 'Veuillez maintenant éditer le planning d\'entretien', 'edit?id=' . $mp_id);
} elseif ($_POST['action'] == 'edit') {
    $validator = new Validator($_POST);

    $validator->no_symbol('name', 'Le nom est invalide', 'Le nom est introuvable ');
    $validator->is_num('min_area', 'le nombre min de la superficie est invalide', 'le nombre min de la superficie est manquant');
    $validator->is_num('max_area', 'le nombre max de la superficie est invalide', 'le nombre max de la superficie est manquant');
    $validator->is_id('recurrence_id', 'La récurrence est invalide');
    $validator->is_id('ironing', 'L\'information sur le repassage est invalide');
    $validator->is_id('childs', 'L\'information sur le repassage est invalide');
    $validator->is_id('mp_template_id', 'Le planning type est invalide', 'Le planning type est manquant');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'edit?id=' . $_POST['mp_template_id']);
    $rowCount = App::getMaintenancePlanningType()->editMpType($_POST);
    if ($rowCount === false)
        App::setFlashAndRedirect('danger', 'Le planning n\'a pas été modifié', 'edit?id=' . $_POST['mp_template_id']);
    App::setFlashAndRedirect('success', 'Le planning a bien été modifié', "edit?id=" . $_POST['mp_template_id']);
} elseif ($_POST['action'] == 'delete') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('mp_template_id', 'Le planning type est invalide', "Le planning type est introuvable");
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'view');
    $ret = App::getMaintenancePlanningType()->deleteMPType($_DELETE['mp_template_id']);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le planning n\'a pas été supprimé', 'edit?id=' . $_DELETE['mp_template_id']);
    App::setFlashAndRedirect('success', 'Le planning a été supprimé', 'view');
}
