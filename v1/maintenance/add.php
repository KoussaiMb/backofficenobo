<?php
require('../../inc/bootstrap.php');

$recurrences = App::getListManagement()->getList("recurrence");
$maintenanceTypes = App::getListManagement()->getList("maintenanceType");
$rollingTask = App::getListManagement()->getList("rollingTask");

if (empty($maintenanceTypes))
    App::setFlashAndRedirect('warning', 'Agrémenter la liste de type d\'entretien avant de créer un PE', 'view');

require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Création de planning d'entretien
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="container">
                <form id="form1" method="post" action="form" enctype="multipart/form-data">
                    <div class="col-xs-8 col-xs-offset-2 section-maintenance">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="name">Nom du planning d'entretien</label>
                                <input type="text" class="form-control input-sm" name="name" value="" placeholder="ex: PE Exemple" id="name" required>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="min_area">Surface minimum</label></div>
                                <input type="text" class="form-control input-sm" name="min_area" value="0" id="min_area" required>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="max_area">Surface maximum</label></div>
                                <input type="text" class="form-control input-sm" name="max_area" value="9999" id="max_area" required>
                            </div>
                        </div>
                        <div class="row SpaceTop">
                            <div class="col-xs-6">
                                <label for="l_recurrence_id">Récurrence</label>
                                    <select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control" name="l_recurrence_id" id="l_recurrence_id">
                                    <option></option>
                                    <?php foreach ($recurrences as $key => $value): ?>
                                        <option value="<?= $value->id ?>"><?= $value->field; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="ironing" class="form-check-label">Repassage</label></div>
                                <input type="hidden" class="form-check-input" name="ironing" value="0">
                                <input type="checkbox" class="form-check-input" name="ironing" value="1">
                            </div>
                            <div class="col-xs-3">
                                <div><label for="childs" class="form-check-label">Enfants</label></div>
                                <input type="hidden" class="form-check-input" name="childs" value="0">
                                <input type="checkbox" class="form-check-input" name="childs" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="view"><button type="button" class="btn btn-default">Revenir à la liste des PE</button></a>
                            <button type="submit" class="btn btn-success" name="action" value="add" >Ajouter le PE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
    <style>
        .section-maintenance {
            padding: 15px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 10px;
            margin-bottom: 15px;
        }
    </style>
<?php require('../../inc/footer_bo.php');
