<?php
require('../../inc/bootstrap.php');

if (empty($_GET['id']))
    App::setFlashAndRedirect('warning', 'Veuillez choisir un planning d\'entretien à éditer', 'view');

$mp = App::getMaintenancePlanningType()->getMpTemplateById($_GET['id']);
$mp_unit = App::getMaintenancePlanningType()->getMpUnitByTemplateId($_GET['id']);
if ($mp === false || $mp_unit === false)
    App::setFlashAndRedirect('danger', 'Impossible de récupérer le Planning d\'entretien type', 'view');

$recurrences = App::getListManagement()->getList("recurrence");
$maintenanceTypes = App::getListManagement()->getList("maintenanceType");
$rollingTask = App::getListManagement()->getList("rollingTask");
require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div id="result"></div>
        <div class="panel-heading">
            <a href="view">Liste des PE</a> / <?= $mp->name ;?>
            <form action="form" method="post">
                <input type="hidden" name="mp_template_id" value="<?= $mp->id; ?>" id="mp_template_id">
                <button class="btn btn-danger panel-heading-button-right btn-xs" onclick="return confirm('Voulez-vous vraiment supprimer le PE ?')" name="action" value="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </form>
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="container">
                <form action="form" method="post">
                    <input type="hidden" name="mp_template_id" value="<?= $mp->id; ?>" id="mp_template_id">
                    <div class="col-xs-8 col-xs-offset-2 section-maintenance">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="name">Nom du planning d'entretien</label>
                                <input type="text" class="form-control input-sm" name="name" value="<?= $mp->name; ?>" placeholder="ex: PE 1" id="name" required>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="min_area">Surface minimum</label></div>
                                <input type="text" class="form-control input-sm" value="<?= $mp->min_area; ?>" name="min_area" id="min_area" required>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="max_area">Surface maximum</label></div>
                                <input type="text" class="form-control input-sm" value="<?= $mp->max_area; ?>" name="max_area" id="max_area" required>
                            </div>
                        </div>
                        <div class="row SpaceTop">
                            <div class="col-xs-6">
                                <label for="l_recurrence_id">Récurrence</label>
                                <select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control" name="l_recurrence_id" id="l_recurrence_id">
                                    <option></option>
                                    <?php foreach ($recurrences as $key => $value): ?>
                                        <option value="<?= $value->id ?>" <?= $value->id == $mp->l_recurrence_id ? 'selected' : ''; ?>><?= $value->field; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="ironing" class="form-check-label">Repassage</label></div>
                                <input type="hidden" class="form-check-input" name="ironing" value="0">
                                <input type="checkbox" class="form-check-input" name="ironing" value="1" <?= $mp->ironing == 1 ? 'checked' : ''; ?>>
                            </div>
                            <div class="col-xs-3">
                                <div><label for="childs" class="form-check-label">Enfants</label></div>
                                <input type="hidden" class="form-check-input" name="childs" value="0">
                                <input type="checkbox" class="form-check-input" name="childs" value="1" <?= $mp->childs == 1 ? 'checked' : ''; ?>>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <button class="btn-link-v2 pull-right noMarge" name="action" value="edit">Appliquer le changement des infos</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-xs-12 SpaceTop task-container">
                        <?php foreach ($mp_unit as $unit): ?>
                            <div class='col-xs-3 task-view' data-id="<?= $unit->id; ?>">
                                <div class='col-xs-12 text-center'>
                                    <button type='button' class='close removeTask'>
                                        <span aria-hidden='true'>&times;</span>
                                    </button>
                                    <p>Type d'entretien</p>
                                    <select name='l_maintenanceType_id' required>
                                        <option value=''>Selectionner</option>
                                        <?php foreach($maintenanceTypes as $maintenanceType): ?>
                                            <option value='<?= $maintenanceType->id ?>' <?= $unit->l_maintenanceType_id == $maintenanceType->id ? 'selected' : ''; ?>><?= $maintenanceType->field; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class='col-xs-12 text-center' style='margin-top: 10px'>
                                        <button type='button' class='addRollingTask'>Ajouter une tâche roulante</button>
                                    </div>
                                    <div class='text-center'><span class='glyphicon glyphicon-minus'></span></div>
                                    <div class='rollingTaskContainer'>
                                        <?php
                                        $max = $unit->task_unit_nb;
                                        $rolling_task_ids =  explode(',', $unit->rolling_task_ids);
                                        $rolling_task_names =  explode(',', $unit->rolling_task_names);
                                        $mp_unit_task_ids =  explode(',', $unit->mp_unit_task_ids);
                                        for ($i = 0; $i < $unit->task_unit_nb; $i++) {
                                            echo "<p class='rollingTask' data-id=" . $rolling_task_ids[$i] .
                                            " data-mp-unit-task-id=" . $mp_unit_task_ids[$i] .">". $rolling_task_names[$i] .
                                            "</p>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="col-xs-3 add-task-div">
                            <div class="text-center">
                                <h1 class="glyphicon glyphicon-plus"></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
    <!-- MODAL TASK -->
    <div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Selectionne une tâche</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group">
                        <?php foreach($rollingTask as $value):?>
                            <input type="checkbox" name="<?= $value->field; ?>" value="<?= $value->id; ?>"> <?= $value->field; ?><br>
                        <?php endforeach; ?>
                    </div>
                    <div class="modal-footer d-flex justify-content-center text-center">
                        <a href="/v1/task/view" target="_blank"><button type="button" class="btn btn-info pull-left" name="action" value="view">Voir la liste des tâches</button></a>
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="addRollingTaskFromModal">Valider</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .section-maintenance {
            padding: 15px 15px 5px 15px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 10px;
        }
        .add-task-div {
            cursor: pointer;
        }
        .add-task-div:hover {
            cursor: pointer;
            color: #4ea64e;
        }
        .add-task-div, .task-view {
            padding: 20px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            overflow: auto;
        }
        .add-task-div > div:hover, .task-view > div:hover {
            border: 1px solid grey;
        }
        .add-task-div img, .task-view img {
            max-width: 100%;
            max-height: 100%;
        }
        .add-task-div > div, .task-view > div {
            border: 1px solid transparent;
            background-color: #f0f0f5;
            border-radius: 3px;
            padding-top: 10px;
            min-height: 150px;
        }
        .addRollingTask {
            color: #1b6d85;
            border: none;
            box-shadow: none;
        }
        .addRollingTask:hover {
            color: #00aeef;
            text-decoration: underline #00aeef;
        }
        .rollingTask {
            padding: 0;
            margin: 0;
            color: #777777;
        }
        .rollingTaskContainer {
            padding-bottom: 8px;
        }
        .panel-heading-button-right {
            position: absolute;
            right: 30px;
            top: 10px;
        }
        .btn-link-v2 {
            padding-top: 5px;
            padding-left: 2px;
            cursor: pointer;
            color: #1b6d85;
            background-color: transparent;
            box-shadow: none;
            border: none;
        }
        .btn-link-v2:hover {
            color: #00aeef;
            text-decoration: #00aeef underline;
        }
    </style>
    <script src="/js/maintenance.js"></script>
    <script>
        $(document).ready(function () {
            let mp_template_id = <?= $mp->id; ?>;
            let cur_taskView;
            let taskContainter = $('.task-container');
            let taskModal = $('#taskModal');
            let addTask = $('.add-task-div');

            addTask.on('click', function () {
                let mp_unit_add = addMpUnit(mp_template_id);

                mp_unit_add.done(function (res) {
                    if (res.code === '200')
                        addTask.before(getNewTaskViewDiv(res.data.mp_unit_id));
                    else
                        bo_notify(res.message, 'danger');
                });
            });

            taskContainter.on('click', function (e) {
                if ($(e.target).hasClass('removeTask')) {
                    let toDelete = $(e.target).closest('.task-view');
                    let delete_mp_unit = deleteMpUnit(toDelete.data('id'));

                    delete_mp_unit.done(function (res) {
                        if (res.code === '200')
                            toDelete.remove();
                        else
                            bo_notify(res.message, 'danger');
                    });
                }
            });

            taskContainter.on('click', '.addRollingTask', function (event) {
                cur_taskView = $(this).closest('.task-view');
                let tasks = cur_taskView.find('.rollingTaskContainer>p');

                taskModal.find('input').prop('checked', false);
                tasks.each(function (index, el) {
                    taskModal.find("input[value='"+el.dataset.id+"']").prop('checked', true);
                });
                taskModal.modal();
            });

            taskContainter.on('change', "select[name='l_maintenanceType_id']", function (e) {
                let mp_unit_edit = editMpUnit(
                    $(e.target).closest('.task-view').data('id'),
                    $(e.target).val()
                );

                mp_unit_edit.done(function (res) {
                    if (res.code !== '200')
                        bo_notify(res.message, 'danger');
                })
            });

            $('#addRollingTaskFromModal').on('click', function () {
                let old_rt_list = [];
                let new_rt_list = [];
                let inputTasks = taskModal.find('input:checked');
                let rolling_task_container = cur_taskView.find('.rollingTaskContainer');
                let old_rolling_task = rolling_task_container.find('p');

                old_rolling_task.each (function (index, el) {
                    old_rt_list.push(el.dataset.id);
                });
                inputTasks.each(function (index, el) {
                    new_rt_list.push(el.value);
                });

                let add_task_list = new_rt_list.diff(old_rt_list);
                let delete_task_list = old_rt_list.diff(new_rt_list);

                $.each(add_task_list, function (index, el) {
                   let add_unit_task = addMpUnitTask(cur_taskView.data('id'), el);
                   add_unit_task.done(function (res) {
                       if (res.code === '200')
                           rolling_task_container.append(getNewRollingTaskDiv(taskModal.find("input[value='" +el+ "']").prop('name'), el, res.data.mp_unit_task_id));
                   })
                });

                $.each(delete_task_list, function (index, el) {
                    let rolling_task = cur_taskView.find("p.rollingTask[data-id='"+el+"']");
                    let delete_unit_task = deleteMpUnitTask(rolling_task.data('mp-unit-task-id'));
                    delete_unit_task.done(function (res) {
                        if (res.code === '200')
                            rolling_task.remove();
                    })
                })
            });
        });

        function getNewRollingTaskDiv(name, rolling_task_id, mp_unit_task_id) {
            return "<p class='rollingTask'"
                + " data-id='" + rolling_task_id + "'"
                + " data-mp-unit-task-id='" + mp_unit_task_id+"'>"
                + name
                + "</p>";
        }

        function getNewTaskViewDiv(mp_unit_id) {
            return "<div class='col-xs-3 task-view' data-id='" +mp_unit_id+ "'>" +
                "<div class='col-xs-12 text-center'>" +
                "<button type='button' class='close removeTask'>" +
                "<span aria-hidden='true'>&times;</span>" +
                "</button>" +
                "<p>Type d'entretien</p>" +
                "<select name='l_maintenanceType_id' required>" +
                "<option value='''>Selectionner</option>" +
                <?php foreach($maintenanceTypes as $maintenanceType): ?>
                "<option value='<?= $maintenanceType->id ?>'><?= $maintenanceType->field; ?></option>" +
                <?php endforeach; ?>
                "</select>" +
                "<div class='col-xs-12 text-center' style='margin-top: 10px'>" +
                "<button type='button' class='addRollingTask'>Ajouter une tâche roulante</button>" +
                "</div>" +
                "<div class='text-center'><span class='glyphicon glyphicon-minus'></span></div>" +
                "<div class='rollingTaskContainer'></div>" +
                "</div>" +
                "</div>";
        }
    </script>
<?php require('../../inc/footer_bo.php');

