<?php
include_once('../../../inc/bootstrap.php');

if (empty($_POST['action']) && empty($_POST['setting']))
    App::redirect('../edit');

$product_redirect = '../edit?menu=cardex&subMenu=productManagement';
$room_redirect = '../edit?menu=cardex&subMenu=roomManagement';

if ($_POST['action'] == 'add' && $_POST['setting'] == 'addRoom')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('inputTextRoom', 'Le nom de la pièce est invalide', 'Vous avez oublié le nom de la pièce');
    $validator->is_len('inputTextRoom', null, 30, 'le nom entré est trop long');

    if ($validator->is_valid()) {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/nobo/logo/cardex/rooms/')) {
            $success_create = mkdir($_SERVER['DOCUMENT_ROOT'] . '/img/nobo/logo/cardex/rooms/', 0757, true);
            if(!$success_create){
                App::setFlashAndRedirect("danger", "Un problème est survenu lors de la création du dossier", $room_redirect);
            }
        }
        $img_name = parseStr::cleanImgName($_POST['inputTextRoom']);
        $logo_url = $validator->is_logo_uploaded($_FILES['logoRoom'], $img_name, '/img/nobo/logo/cardex/rooms/');
    }
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $room_redirect);
    $cardexClass = App::getCardex();
    $ret = $cardexClass->insertListReference('roomsList', $logo_url, $_POST['inputTextRoom']);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'La pièce n\'a pas été ajoutée', $room_redirect);
    App::setFlashAndRedirect('success', 'L\'ajout de la pièce a bien fonctionné', $room_redirect);

} else if ($_POST['action'] == 'add' && $_POST['setting'] == 'addProduct')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('inputTextProduct', 'Le nom du produit est invalide', 'Vous avez oublié le nom du produit');
    $validator->is_len('inputTextProduct', null, 30, 'le nom entré est trop long');

    if ($validator->is_valid()){
        if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/img/nobo/logo/cardex/products/')) {
            $success_create = mkdir($_SERVER['DOCUMENT_ROOT'].'/img/nobo/logo/cardex/products/', 0757, true);
            if(!$success_create){
                App::setFlashAndRedirect("danger", "Un problème est survenu lors de la création du dossier", $product_redirect);
            }
        }
        $img_name = parseStr::cleanImgName($_POST['inputTextProduct']);
        $logo_url = $validator->is_logo_uploaded($_FILES['logoProduct'], $img_name,'/img/nobo/logo/cardex/products/');
    }
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $product_redirect);

    $cardexClass = App::getCardex();
    $ret = $cardexClass->insertListReference('productsList', $logo_url, $_POST['inputTextProduct']);

    if($ret == 0)
        App::setFlashAndRedirect('danger', 'Le produit n\'a pas été ajouté', $product_redirect);
    App::setFlashAndRedirect('success', 'Le produit a bien été ajouté', $product_redirect);
}
else if ($_POST['action'] == 'delete' && $_POST['setting'] == 'room')
{
    $validator = new Validator($_POST);
    $validator->is_id('idToDelete', 'Suppression de la pièce impossible', 'Suppression de la pièce impossible (Non présente)');

    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $room_redirect);

    $cardexClass = App::getCardex();
    $idToDelete = $_POST['idToDelete'];

    $r1 = $cardexClass->deleteAllTasksByRoomId($idToDelete);
    $r2 = $cardexClass->deleteProductLocalizationByRoomId($idToDelete);
    $ret = $cardexClass->deleteFromListReferenceById($idToDelete);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'Suppression de la pièce impossible', $room_redirect);
    App::setFlashAndRedirect('success', 'Suppression effectuée avec succès', $room_redirect);
}
else if ($_POST['action'] == 'delete' && $_POST['setting'] == 'product' )
{
    $validator = new Validator($_POST);
    $validator->is_id('idToDelete', 'Suppression du produit impossible', 'Suppression du produit impossible (Non présent)');

    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $product_redirect);

    $cardexClass = App::getCardex();
    $r = $cardexClass->deleteProductLocalizationByProductId($_POST['idToDelete']);
    $ret = $cardexClass->deleteFromListReferenceById($_POST['idToDelete']);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'Suppression du produit impossible', $product_redirect);
    App::setFlashAndRedirect('success', 'Suppression effectuée avec succès', $product_redirect);
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'editRoom')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Room invalide', 'Room introuvable');
    $validator->no_symbol('name', 'Le nom entré comporte des caractères interdits', 'Le nom entré ne peut pas être vide');
    $validator->is_len('name', null, 30, 'Le nom entré est trop long');

    if(empty($_FILES['logo']['tmp_name'])) {
        $logo_url = $_POST['logo'];
    } elseif($validator->is_valid()){
        $img_name = parseStr::cleanImgName($_POST['name']);
        $logo_url = $validator->is_logo_uploaded($_FILES['logo'], $img_name, '/img/nobo/logo/cardex/rooms/');
    }
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $room_redirect);

    $cardexClass = App::getCardex();
    $ret = $cardexClass->updateListReferenceById($_POST['id'], $logo_url, $_POST['name']);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'La piece n\'a pas été changé', $room_redirect);
    App::setFlashAndRedirect('success', 'Modification effectuée avec succès', $room_redirect);
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'editProduct')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Produit invalide', 'Produit introuvable');
    $validator->no_symbol('name', 'Le nom entré comporte des caractères invalides', 'Le nom entré ne peut pas être vide');
    $validator->is_len('name', null, 30, 'Le nom entré est trop grand');

    if (empty($_FILES['logo']['tmp_name'])){
        $logo_url = $_POST['logo'];
    }elseif ($validator->is_valid()) {
        $img_name = parseStr::cleanImgName($_POST['name']);
        $logo_url = $validator->is_logo_uploaded($_FILES['logo'], $img_name, '/img/nobo/logo/cardex/products/');
    }
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $product_redirect);

    $cardexClass = App::getCardex();
    $ret = $cardexClass->updateListReferenceById($_POST['id'], $logo_url, $_POST['name']);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'Le produit n\'a pas été changé', $product_redirect);
    App::setFlashAndRedirect('success', 'Modification effectuée avec succès', $product_redirect);
}
