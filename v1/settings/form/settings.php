<?php
include_once('../../../inc/bootstrap.php');

if (empty($_POST['action']) && empty('setting'))
    App::redirect('../edit');

if ($_POST['action'] == 'edit' && $_POST['setting'] == 'calendarSetup')
{
    $local_days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];
    $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $max = count($days);
    $i = -1;
    $validator = new Validator($_POST);

    while (++$i < $max) {
        $validator->is_time($days[$i] . 'Start', 'L\'horaire de début le' . $local_days[$i] . 'n\'est pas valide', 'Veuillez indiquer un horaire de début le ' . $local_days[$i]);
        $validator->is_time($days[$i] . 'End', 'L\'horaire de fin le' . $local_days[$i] . 'n\'est pas valide', 'Veuillez indiquer un horaire de fin le ' . $local_days[$i]);
        $validator->is_time_superior($days[$i] . 'Start', $days[$i] . 'End', 'La durée du' . $local_days[$i] . 'n\'est pas valide');
    }
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=calendarSetup');
    }
    $conf = App::getConf();
    if ($conf->updateConf($_POST) === null)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors de l\'édition des paramètres du calendrier', '../edit?subMenu=calendarSetup');
    App::setFlashAndRedirect('success', 'Les paramètres du calendrier ont bien été mis à jour', '../edit?subMenu=calendarSetup');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'providerSetup')
{
    $validator = new Validator($_POST);
    $validator->is_num('intervalPrestationDay', 'Un nombre est attendu pour les prestations', 'Un nombre est attendu pour les prestations');
    $validator->is_num('intervalPrestationHour', 'Un nombre est attendu pour les prestations', 'Un nombre est attendu pour les prestations');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=providerSetup');
    }
    $data['intervalMinChangePrestation'] = 'P' . $_POST['intervalPrestationDay'] . 'DT' . $_POST['intervalPrestationHour'] . 'H';
    $conf = App::getConf();
    if ($conf->updateConf($data) === null)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors de l\'édition des paramètres du prestataire', '../edit?subMenu=providerSetup');
    App::setFlashAndRedirect('success', 'Les paramètres du prestataire ont bien été mis à jour', '../edit?subMenu=providerSetup');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'missionSetup')
{
    $validator = new Validator($_POST);
    $validator->is_time('prestation_min', 'Durée minimum de prestation invalide', 'Durée minimum de prestation manquante');
    $validator->is_time('prestation_max', 'Durée maximum de prestation invalide', 'Durée maximum de prestation manquante');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=missionSetup');
    }
    $conf = App::getConf();
    if ($conf->updateConf($data) === null)
        App::setFlashAndRedirect('danger', 'un problème est survenu', '../edit?subMenu=missionSetup');
    App::setFlashAndRedirect('success', 'Les paramètres ont bien été mis à jour', '../edit?subMenu=missionSetup');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'publicHoliday')
{
    $validator = new Validator($_POST);
    $validator->is_num('canWork', 'Cessons les bêtises, peut-on travailler ce jour là ?', 'Vous n\'avez pas coché de réponse pour savoir si on peut travailler ce jour là');
    $validator->is_num('addWagePercentage', 'Le pourcentage n\'est pas valide', 'Un nombre est attendu le pourcentage');
    $validator->is_num('addWageBonus', 'Le bonus n\'est pas valide', 'Un nombre est attendu le bonus');
    $validator->is_date_bo('date', 'Le format de la date n\'est pas valide', 'Il faut une date');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=publicHoliday');
    }
    $conf = App::getConf();
    if ($conf->addPublicHoliday($_POST) === null)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'insertion d\'un jour férié', '../edit?subMenu=publicHoliday');
    App::setFlashAndRedirect('success', 'Le jour férié a été créé', '../edit?subMenu=publicHoliday');
}
else if ($_POST['action'] == 'delete' && $_POST['setting'] == 'publicHoliday')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Le jour férié à supprimer n\'est pas valide', 'Aucun jour férié n\'a été trouvé');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=publicHoliday');
    }
    $conf = App::getConf();
    if ($conf->removePublicHoliday($_POST['id']) === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la suppression du jour férié', '../edit?subMenu=publicHoliday');
    App::setFlashAndRedirect('success', 'Le jour férié a bien été supprimé', '../edit?subMenu=publicHoliday');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'stripeKeys')
{
    $validator = new Validator($_POST);
    $validator->is_stripeKey('stripe_pk', 'La clé publique n\'est pas valide', 'Veuillez entrer une clé publique');
    $validator->is_stripeKey('stripe_sk', 'La clé privée n\'est pas valide', 'Veuillez entrer une clé privée');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?menu=clefs&subMenu=stripeKeys');
    }
    $conf = App::getConf();
    if ($conf->updateConf($_POST) === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la mise à jour des clés', '../edit?menu=clefs&subMenu=stripeKeys');
    App::setFlashAndRedirect('success', 'Les clés ont été mise à jour', '../edit?menu=clefs&subMenu=stripeKeys');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'mailjetKeys')
{
    $validator = new Validator($_POST);
    $validator->is_alphanum('mailjet_public_key', 'La clé publique n\'est pas valide', 'Veuillez entrer une clé publique');
    $validator->is_alphanum('mailjet_private_key', 'La clé privée n\'est pas valide', 'Veuillez entrer une clé privée');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?menu=Clefs&subMenu=mailjetKeys');
    }
    $conf = App::getConf();
    if ($conf->updateConf($_POST) === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la mise à jour des clés', '../edit?menu=clefs&subMenu=mailjetKeys');
    App::setFlashAndRedirect('success', 'Les clés ont été mise à jour', '../edit?menu=clefs&subMenu=mailjetKeys');

}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'dispoClientSetup')
{
    $var = ['morning', 'midday', 'afternoon'];
    $var_locale = ['le matin', 'le midi', 'l\'après-midi'];
    $i = -1;
    $max = count($var);
    $validator = new Validator($_POST);

    while (++$i < $max) {
        $validator->is_time($var[$i] . 'DurationStart', 'L\'horaire de début' . $var_locale[$i] . 'n\'est pas valide', 'Veuillez indiquer un horaire de début' . $var_locale[$i]);
        $validator->is_time($var[$i] . 'DurationEnd', 'L\'horaire de fin' . $var_locale[$i] . 'n\'est pas valide', 'Veuillez indiquer un horaire de fin' . $var_locale[$i]);
        $validator->is_time_superior($var[$i] . 'DurationStart', $var[$i] . 'DurationEnd', 'La durée' . $var_locale[$i] . 'n\'est pas valide');
    }
    $validator->is_time_superior('morningDurationEnd', 'middayDurationStart', 'Les créneaux ne peuvent se chevaucher');
    $validator->is_time_superior('middayDurationEnd', 'afternoonDurationStart', 'Les créneaux ne peuvent se chevaucher');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?subMenu=dispoClientSetup');
    }
    $conf = App::getConf();
    if ($conf->updateConf($_POST) === null)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors de l\'édition des paramètres des disponibilités clients', '../edit?subMenu=dispoClientSetup');
    App::setFlashAndRedirect('success', 'Les paramètres des disponibilités clients ont bien été mis à jour', '../edit?subMenu=dispoClientSetup');
}
else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'change_company_reference')
{
    $validator = new Validator($_POST);
    $validator->is_id('select_company', 'L\'entreprise sélectionnée est invalide', 'L\'entreprise sélectionnée n\existe pas');

    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('../edit?menu=system');
    }
    $companyClass = App::getCompany();
    $curr = $companyClass->getCompanyConf()->id;

    if ($curr === false)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout de la configuration', '../edit?menu=system');
    if (empty($curr)) {
        $ret = $companyClass->insertCompanyConf($_POST['select_company']);
        if($ret === false)
            App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout de la configuration', '../edit?menu=system');
        App::setFlashAndRedirect('success', 'La configuration a bien été ajoutée', '../edit?menu=system');
    }

    $r = $companyClass->updateCompanyConf($curr, $_POST['select_company']);
    if($r === false){
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'édition de la configuration', '../edit?menu=system');
    }
    App::setFlashAndRedirect('success', 'La configuration a bien été mise à jour', '../edit?menu=system');
}
App::redirect('../edit');