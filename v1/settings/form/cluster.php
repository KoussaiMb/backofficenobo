<?php
include_once('../../../inc/bootstrap.php');

if (empty($_POST['action']) && empty($_POST['setting']))
    App::redirect('../edit');

$cluster_redirect = '/v1/settings/edit?menu=cluster';

if ($_POST['action'] == 'edit' && $_POST['setting'] == 'cluster')
{
    $validator = new Validator($_POST);
    $validator->is_num('max_provider_in_bu', 'Il faut un nombre compris entre 1 et 100', 'Vous avez oublié de renseigner le nombre de prestataire par Business unit');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $cluster_redirect);
    $array = [[
        "name" => "max_provider_in_bu",
        "value" => $_POST['max_provider_in_bu']
    ]];
    $ret = App::getConf()->editConf($array);
    if($ret == 0)
        App::setFlashAndRedirect('danger', 'Les paramètres des clusters n\'ont pas pu etre modifié', $cluster_redirect);
    App::setFlashAndRedirect('success', 'Les paramètres des clusters ont été modifiés', $cluster_redirect);
} else if ($_POST['action'] == 'edit' && $_POST['setting'] == 'clusterLocation') {
    $validator = new Validator($_POST);

    $validator->no_symbol('clusterLocation', 'Localisation du cluster invalide', 'Localisation du cluster manquant');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), $cluster_redirect);

    $listManager = App::getListManagement();
    $ret = $listManager->addInput('clusterLocation', $_POST['clusterLocation']);
    if($ret === false)
        App::setFlashAndRedirect('danger', 'Localisation du cluster non ajoutée', $cluster_redirect);
    App::setFlashAndRedirect('success', 'Localisation du cluster bien ajoutée', $cluster_redirect);
}
App::redirect('/v1/settings');