<?php
require('../../inc/bootstrap.php');

//On récupère les confs
$conf = App::getConf()->getAll();
$public_holidays = App::getConf()->getAllPublicHolidays();

if (!empty($_GET['menu_id'])){
    $menu_id = substr($_GET['menu_id'], -1);
}
else if (!empty($_GET['menu'])) {
    $menu_tab = ['calendar', 'offer', 'system', 'clefs', 'cardex', 'cluster'];
    $menu_id = App::get_position_in_array($_GET['menu'], $menu_tab) + 1;
}
else {
    $menu_id = '1';
}

//Création des objets DateInterval
$intervalPrestation = !empty($conf['intervalMinChangePrestation']) ? new DateInterval($conf['intervalMinChangePrestation']) : null;
$medecinChecktime = !empty($conf['medecinChecktime']) ? new DateInterval($conf['medecinChecktime']) : null;
$daysOffPerMonth = !empty($conf['daysOffPerMonth']) ? new DateInterval($conf['daysOffPerMonth']) : null;
$companies = App::getCompany()->getAll();
if (isset($conf['companySetting']))
    $currentCompany = App::getCompany()->getCompanyById($conf['companySetting']);
$clusterLocations = App::getListManagement()->getList('clusterLocation');

require('../../inc/header_bo.php');
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div style="min-height: 30px">
                <div class="col-sm-12">
                    <ul class="nav nav-pills pillsWrapper customColor" id='tabs_provider'>
                        <input type="hidden" value="<?= $menu_id; ?>" id="get_menu_id" disabled>
                        <li id="tab1"><a data-toggle="tab" href="#menu1">Calendrier</a></li>
                        <li id="tab2"><a data-toggle="tab" href="#menu2">Offres</a></li>
                        <li id="tab3"><a data-toggle="tab" href="#menu3">Système</a></li>
                        <li id="tab4"><a data-toggle="tab" href="#menu4">Clefs API</a></li>
                        <li id="tab5"><a data-toggle="tab" href="#menu5">Cardex</a></li>
                        <li id="tab6"><a data-toggle="tab" href="#menu6">Cluster</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content">
            <?php include('../../inc/print_flash_helper.php'); ?>
                <!--Calendar-->
                <div id="menu1" class="tab-pane fade">
                    <div class="col-sm-3 select-side-wrapper subMenu">
                        <div class="select-side current" data-href="calendarSetup">
                            Horaires de travail
                        </div>
                        <div class="select-side" data-href="publicHoliday">
                            Jours fériés
                        </div>
                        <div class="select-side" data-href="providerSetup">
                            Paramètres prestataires
                        </div>
                        <!--
                        <div class="select-side" data-href="dispoClientSetup">
                            Disponibilités client
                        </div>
                        -->
                        <div class="select-side" data-href="missionSetup">
                            Paramètres mission
                        </div>
                    </div>
                    <div class="col-sm-9 content-side HideMe" id="missionSetup">
                        <form method="post" action="form/settings">
                            <div class="col-sm-10 confDayCalendar">
                                <label for="prestation_min" class="col-sm-2">Durée minimale</label>
                                <input type="time" class="form-control input-sm pickatime start" name="prestation_min" value="<?= $conf['prestation_min']; ?>" id="prestation_min"/>
                            </div>
                            <div class="col-sm-10 confDayCalendar">
                                <label for="prestation_max" class="col-sm-2">Durée maximale</label>
                                <input type="time" class="form-control input-sm pickatime start" name="prestation_max" value="<?= $conf['prestation_max']; ?>" id="prestation_max"/>
                            </div>
                            <div class="col-sm-12 text-center SpaceTop">
                                <input type="hidden" name="setting" value="missionSetup">
                                <button class="btn btn-sm btn-success" name="action" value="edit">Enregistrer les modifications</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-9 content-side" id="calendarSetup">
                        <div class="col-sm-12">
                            <form method="post" action="form/settings">
                                <span class="col-sm-2">Lundi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="mondayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" name="mondayStart" value="<?= $conf['mondayStart']; ?>" id="mondayStart"/>
                                    <label for="mondayEnd" class="col-sm-2">à</label>
                                    <input type="time" class="form-control input-sm pickatime end" name="mondayEnd" value="<?= $conf['mondayEnd']; ?>" id="mondayEnd"/>
                                </div>
                                <span class="col-sm-2">Mardi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="tuesdayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" name="tuesdayStart" value="<?= $conf['tuesdayStart']; ?>" id="tuesdayStart"/>
                                    <label for="tuesdayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" name="tuesdayEnd" value="<?= $conf['tuesdayEnd']; ?>" id="tuesdayEnd"/>
                                </div>
                                <span class="col-sm-2">Mercredi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="wednesdayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['wednesdayStart']; ?>" name="wednesdayStart" id="wednesdayStart"/>
                                    <label for="wednesdayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['wednesdayEnd']; ?>" name="wednesdayEnd" id="wednesdayEnd"/>
                                </div>
                                <span class="col-sm-2">Jeudi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="thursdayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['thursdayStart']; ?>" name="thursdayStart" id="thursdayStart"/>
                                    <label for="thursdayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['thursdayEnd']; ?>" name="thursdayEnd" id="thursdayEnd"/>
                                </div>
                                <span class="col-sm-2">Vendredi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="fridayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['fridayStart']; ?>" name="fridayStart" id="fridayStart"/>
                                    <label for="fridayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['fridayEnd']; ?>" name="fridayEnd" id="fridayEnd"/>
                                </div>
                                <span class="col-sm-2">Samedi</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="saturdayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['saturdayStart']; ?>" name="saturdayStart" id="saturdayStart"/>
                                    <label for="saturdayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['saturdayEnd']; ?>" name="saturdayEnd" id="saturdayEnd"/>
                                </div>
                                <span class="col-sm-2">Dimanche</span>
                                <div class="col-sm-10 confDayCalendar">
                                    <label for="sundayStart" class="col-sm-2">De</label>
                                    <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['sundayStart']; ?>" name="sundayStart" id="sundayStart"/>
                                    <label for="sundayEnd" class="col-sm-2"> à </label>
                                    <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['sundayEnd']; ?>" name="sundayEnd" id="sundayEnd"/>
                                </div>
                                <div class="col-sm-12 text-center SpaceTop">
                                    <input type="hidden" name="setting" value="calendarSetup">
                                    <button class="btn btn-sm btn-success" name="action" value="edit">Enregistrer les modifications</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-9 content-side HideMe" id="providerSetup">
                        <form method="post" action="form/settings">
                            <div class="col-sm-12">
                                <label class="col-sm-12">Interval minimal avant modification d'une prestation: </label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control input-sm" value="<?= $intervalPrestation->d; ?>" name="intervalPrestationDay" id="intervalPrestationDay" min="0" max="14" step="1"/>
                                    <label for="intervalPrestationDay" class="add-prev sr-only">Jours</label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control input-sm" value="<?= $intervalPrestation->h; ?>" name="intervalPrestationHour" id="intervalPrestationHour" min="0" max="23" step="1"/>
                                    <label for="intervalPrestationHour" class="add-prev sr-only">Heures</label>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center SpaceTop">
                                <input type="hidden" name="setting" value="providerSetup">
                                <button class="btn btn-sm btn-success" name="action" value="edit">Enregistrer les modifications</button>
                            </div>
                        </form>
                    </div>
                    <!--
                    <div class="col-sm-9 content-side HideMe" id="dispoClientSetup">
                        <form method="post" action="form/settings">
                            <span class="col-sm-2" style="vertical-align: center">créneaux matin</span>
                            <div class="col-sm-10 confDayCalendar">
                                <label for="morningDurationStart" class="col-sm-2">De</label>
                                <input type="time" class="form-control input-sm pickatime start" name="morningDurationStart" value="<?= $conf['morningDurationStart']; ?>" id="morningDurationStart"/>
                                <label for="morningDurationEnd" class="col-sm-2">à</label>
                                <input type="time" class="form-control input-sm pickatime end" name="morningDurationEnd" value="<?= $conf['morningDurationEnd']; ?>" id="morningDurationEnd"/>
                            </div>
                            <span class="col-sm-2">créneaux midi</span>
                            <div class="col-sm-10 confDayCalendar">
                                <label for="middayDurationStart" class="col-sm-2">De</label>
                                <input type="time" class="form-control input-sm pickatime start" name="middayDurationStart" value="<?= $conf['middayDurationStart']; ?>" id="middayDurationStart"/>
                                <label for="middayDurationEnd" class="col-sm-2"> à </label>
                                <input type="time" class="form-control input-sm pickatime end" name="middayDurationEnd" value="<?= $conf['middayDurationEnd']; ?>" id="middayDurationEnd"/>
                            </div>
                            <span class="col-sm-2">créneaux après-midi</span>
                            <div class="col-sm-10 confDayCalendar">
                                <label for="afternoonDurationStart" class="col-sm-2">De</label>
                                <input type="time" class="form-control input-sm pickatime start" value="<?= $conf['afternoonDurationStart']; ?>" name="afternoonDurationStart" id="afternoonDurationStart"/>
                                <label for="afternoonDurationEnd" class="col-sm-2"> à </label>
                                <input type="time" class="form-control input-sm pickatime end" value="<?= $conf['afternoonDurationEnd']; ?>" name="afternoonDurationEnd" id="afternoonDurationEnd"/>
                            </div>
                            <hr class="col-sm-11 hrBo">
                            <div class="col-sm-12 text-center SpaceTop">
                                <input type="hidden" name="setting" value="dispoClientSetup">
                                <button class="btn btn-sm btn-success" name="action" value="edit">Enregistrer les modifications</button>
                            </div>
                        </form>
                    </div>
                    -->
                    <div class="col-sm-9 content-side HideMe " id="publicHoliday">
                        <div class="col-sm-8 vr-right">
                            <form method="post" action="form/settings">
                                <div class="form-group col-xs-12">
                                    <label for="publicHolidayDate">Date</label>
                                    <input type="text" class="form-control input-sm pickadate" name="date" id="publicHolidayDate" required/>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label>Peut travailler ce jour là</label>
                                    <div class="input-group">
                                        <label class="checkbox-inline"><input type="checkbox" name="canWork" value="1" id="canWork" onclick="$('#cantWork').prop('checked', function(_, attr){return !attr})" checked>Oui</label>
                                        <label class="checkbox-inline"><input type="checkbox" name="canWork" value="0" id="cantWork" onclick="$('#canWork').prop('checked', function(_, attr){return !attr})">Non</label>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="addWagePercentage">Prime ce jour là en pourcentage</label>
                                    <input type="number" class="form-control input-sm" name="addWagePercentage" value="0" id="addWagePercentage" required/>
                                    <label for="addWagePercentage" class="add-prev sr-only">%</label>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="addWagePercentage">En euros</label>
                                    <input type="number" class="form-control input-sm" name="addWageBonus" value="0" id="addWageBonus" required/>
                                    <label for="addWageBonus" class="add-prev sr-only">Euros</label>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <input type="hidden" name="setting" value="publicHoliday">
                                    <button class="btn btn-sm btn-success" name="action" value="edit">Ajouter</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-4">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <?php foreach($public_holidays as $k => $v): ?>
                                        <tr class="getInfo">
                                            <td class="notHide col-xs-8"><?= parseStr::date_to_str($v->date); ?></td>
                                            <td>
                                                <form method="post" action="form/settings" class="noMargeBottom">
                                                    <input type="hidden" name="setting" value="publicHoliday">
                                                    <input type="hidden" name="id" value="<?= $v->id; ?>">
                                                    <button class="btn btn-danger btn-xs" name="action" value="delete"><span class="glyphicon glyphicon-remove"></span></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <tr class="hideInfo bg-info"><td>Peut travailler ?</td><td><?= $v->canWork ? 'oui' : 'non'; ?></td></tr>
                                        <tr class="hideInfo bg-info"><td>Bonus pourcentage</td><td><?= $v->addWagePercentage; ?></td></tr>
                                        <tr class="hideInfo bg-info"><td>Bonus euros</td><td><?= $v->addWageBonus; ?></td></tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               <!--Offers-->
                <div id="menu2" class="tab-pane fade">

                </div>
                <!--System-->
                <div id="menu3" class="tab-pane fade">
                    <div class="col-sm-3 select-side-wrapper subMenu">
                        <div class="select-side current" data-href="fiscality">
                            Fiscalité
                        </div>
                    </div>
                    <div class="col-sm-9 content-side" id="fiscality">
                        <h4>Factures</h4>
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                        <div class="form-group">
                                            <form method="post" action="form/settings" class="form-inline">
                                                <label for="company_ref">Référence entreprise</label>
                                                <select class="form-control" id="company_ref" name="select_company">
                                                    <?php foreach ($companies as $c): ?>
                                                        <option value="<?= $c->id ?>">
                                                         <?= $c->corporate_name .' - ' . $c->business_name ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <input type="hidden" name="setting" value="change_company_reference">
                                                <button type="submit" name="action" value="edit" class="btn btn-default">Valider</button>
                                            </form>
                                        </div>
                                    <div class="col-sm-12">
                                        <?php if(!isset($currentCompany)): ?>
                                            <h4>Aucune entreprise n'est actuellement configurée</h4>
                                        <?php else: ?>
                                        <h5>
                                            <br><br>Entreprise actuellement configurée :
                                            <b><?= $currentCompany->corporate_name . ' - ' . $currentCompany->business_name; ?></b>
                                        </h5>
                                        <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--clefs-->
                <div id="menu4" class="tab-pane fade">
                    <div class="col-sm-3 select-side-wrapper subMenu">
                        <div class="select-side current" data-href="stripeKeys">
                            Paiement
                        </div>
                        <div class="select-side" data-href="mailjetKeys">
                            Mail
                        </div>
                    </div>
                    <div class="col-sm-9 content-side" id="stripeKeys">
                        <form method="post" action="form/settings">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="stripe_sk">Clé secrète</label>
                                        <input type="text" class="input-sm form-control" name="stripe_sk" value="<?= $conf['stripe_sk']; ?>" id="stripe_sk"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="stripe_pk">Clé publique</label>
                                        <input type="text" class="input-sm form-control" name="stripe_pk" value="<?= $conf['stripe_pk']; ?>" id="stripe_pk"/>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="setting" value="stripeKeys">
                            <button type="submit" class="btn btn-primary col-xs-12" name="action" value="edit">Sauvegarder les changements</button>
                        </form>
                    </div>
                    <div class="col-sm-9 content-side HideMe" id="mailjetKeys">
                        <form method="post" action="form/settings">
                            <div class="col-sm-12 ">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="mailjet_public_key">Clé publique</label>
                                        <input type="text" class="input-sm form-control" name="mailjet_public_key" value="<?= $conf['mailjet_public_key']; ?>" id="mailjet_public_key"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="mailjet_private_key">Clé privée</label>
                                        <input type="text" class="input-sm form-control" name="mailjet_private_key" value="<?= $conf['mailjet_private_key']; ?>" id="mailjet_private_key"/>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="setting" value="mailjetKeys">
                            <button type="submit" class="btn btn-primary col-xs-12" name="action" value="edit">Sauvegarder les changements</button>
                        </form>
                    </div>
                </div>
                <!-- Cardex -->
                <div id="menu5" class="tab-pane fade">
                    <div class="col-sm-3 select-side-wrapper subMenu">
                        <div class="select-side current" data-href="roomManagement">
                            Lieux de vie
                        </div>
                        <div class="select-side" data-href="productManagement">
                            Produits
                        </div>
                    </div>
                    <div class="col-sm-9 content-side" id="roomManagement">
                        <form method='POST' class="form-inline text-center" id="formAddRoom" action="form/cardex" enctype="multipart/form-data">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Upload un logo <input type="file" name="logoRoom" id="logoRoom" class="hidden" required>
                                    </span>
                                </label>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <label for="inputRoomId" class="sr-only"></label>
                            <input type="text" class="form-control" id="inputRoomId" name="inputTextRoom" placeholder="Nom de la pièce" required>
                            <input type="hidden" name="setting" value="addRoom">
                            <button type="submit" class="btn btn-success" id="addRoom" name="action" value="add">Ajouter un lieu de vie</button>
                            <br>
                        </form>
                        <div class="table-responsive col-sm-10 col-sm-offset-1">
                            <table id="table_rooms" class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Nom</th>
                                    <th>Editer</th>
                                    <th>Supprimer</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $list_references_rooms = App::getCardex()->getAllItemsOfListByName('roomsList');
                                foreach ($list_references_rooms as $ref_room):
                                ?>
                                <tr>
                                    <td>
                                        <img class="img-logo" src="<?= $ref_room->url; ?>">
                                    </td>
                                    <td style="font-size: 15px "><?= $ref_room->value; ?></td>
                                    <td>
                                        <input type="hidden" name="id" value="<?= $ref_room->id; ?>">
                                        <input type="hidden" name="url" value="<?= $ref_room->url; ?>">
                                        <input type="hidden" name="value" value="<?= $ref_room->value; ?>">
                                        <button type="button" class="btn btn-default" name="setting" value="editRoom"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                    <td>
                                        <form method="POST" action="form/cardex" enctype="multipart/form-data" onsubmit="return confirm('Les tâches du cardex et emplacements de produits associés à cette pièce seront également supprimés, voulez-vous continuer ? ');">
                                            <input type="hidden" name="setting" value="room">
                                            <input type="hidden" name="idToDelete" value="<?= $ref_room->id ?>">
                                            <button type="submit" class="btn btn-default" name="action" value="delete"><span class="glyphicon glyphicon-remove"></span></button>
                                        </form>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-9 content-side HideMe" id="productManagement">
                        <form method="POST" class="form-inline text-center" id="formAddProduct" action="form/cardex" enctype="multipart/form-data">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Upload un logo <input type="file" name="logoProduct" id="logoProduct" class="hidden" required>
                                    </span>
                                </label>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <label for="inputProdutId" class="sr-only"></label>
                            <input type="text" class="form-control" id="inputProdutId" name="inputTextProduct" placeholder="Nom du produit" required>
                            <input type="hidden" name="setting" value="addProduct">
                            <button type="submit" class="btn btn-success" id="addProduct" name="action" value="add">Ajouter un produit</button>
                            <br>
                        </form>
                        <div class="table-responsive col-sm-10 col-sm-offset-1">
                            <table id="table_rooms" class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Nom</th>
                                    <th>Editer</th>
                                    <th>Supprimer</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $list_references_products = App::getCardex()->getAllItemsOfListByName('productsList');
                                foreach ($list_references_products as $ref_product):
                                ?>
                                <tr>
                                    <td>
                                        <img class="img-logo" src="<?= $ref_product->url; ?>">
                                    </td>
                                    <td style="font-size: 15px "><?= $ref_product->value; ?></td>
                                    <td>
                                        <input type="hidden" name="id" value="<?= $ref_product->id; ?>">
                                        <input type="hidden" name="url" value="<?= $ref_product->url; ?>">
                                        <input type="hidden" name="value" value="<?= $ref_product->value; ?>">
                                        <button type="button" class="btn btn-default" name="setting" value="editProduct"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                    <td>
                                        <form method="POST" action="form/cardex" enctype="multipart/form-data" onsubmit="return confirm('Les localisations associées à ce produit seront également supprimées, voulez-vous continuer ? ');">
                                            <input type="hidden" name="setting" value="product">
                                            <input type="hidden" name="idToDelete" value="<?= $ref_product->id ?>">
                                            <button type="submit" class="btn btn-default" name="action" value="delete"><span class="glyphicon glyphicon-remove"></span></button>
                                        </form>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- cluster -->
                <div id="menu6" class="tab-pane fade">
                    <form method="post" action="form/cluster">
                        <input type="hidden" name="setting" value="cluster">
                        <div class="col-sm-6 col-sm-offset-3 form-group">
                            <label for="max_provider_in_bu">Maximum de prestataire par Business unit</label>
                            <input type="text" class="form-control input-sm" name="max_provider_in_bu" id="max_provider_in_bu" value="<?= $conf['max_provider_in_bu']; ?>">
                        </div>
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-primary" name="action" value="edit">Sauvegarder les changements</button>
                        </div>
                    </form>
                    <form method="post" action="form/cluster">
                        <input type="hidden" name="setting" value="clusterLocation">
                        <div class="col-sm-8 col-sm-offset-2 form-group">
                            <div class="col-sm-6">
                                <label for="clusterLocation">Entrez une ville</label>
                                <input type="text" class="form-control input-sm" name="clusterLocation" id="clusterLocation" placeholder="Choisissez une ville">
                            </div>
                            <div class="col-sm-6">
                                <label>villes actuelles</label>
                                <select class="form-control">
                                    <?php foreach ($clusterLocations as $cl): ?>
                                        <option><?= $cl->field; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-primary" name="action" value="edit">Ajouter une ville</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .select-side:hover {
            border: 1px solid #3277B2;
        }
        .select-side {
            text-align: center;
            padding: 5px;
            cursor: pointer;
            border: 1px solid transparent;
        }
        .select-side.current {
            background-color: #3277B2;
            color: white;
        }
        .content-side {
            border-left: 2px solid #3277B2;
        }
        .alpha-addon {
            background-color: transparent;
        }
    </style>

    <script type="text/javascript" src='/js/app.js'></script>
    <script type="text/javascript" src='/js/cardex.js'></script>
    <script>
        jQuery(document).ready(function() {
            var menu_id = $('#get_menu_id').val();
            var subMenu = '<?php echo !empty($_GET['subMenu']) ? $_GET['subMenu'] : '';?>';

            $('#tab' + menu_id).addClass('active');
            $('#menu' + menu_id).addClass('in active');

            if (subMenu) {
                $('.subMenu').each(function () {
                    if (isTextInSubMenu($(this), subMenu)) {
                        showSubMenu($('div[data-href=' + subMenu + ']'));
                        return false;
                    }
                });
            }

            $(".select-side").on('click', function () {
                showSubMenu($(this));
            });

            $('.pickatime-v2').timepicker({
                'timeFormat': 'i:s',
                'step' : 0.5,
                'maxTime': '00:20'
            });
            $('.pickatime:not(.start, .end)').timepicker({
                'timeFormat': 'H:i',
                'step' : 30,
                'noneOption' : [{'label': '24:00', 'value': '24:00'}]
            });

            $('.lunchtime').timepicker({
                'timeFormat': 'H:i',
                'step' : 15,
                maxTime: '03:00'
            });
            $('.getInfo').on('click', function () {
                var temp;

                temp = ($(this).next('tr').css('display') == 'none');

                $('.hideInfo').hide();
                if (temp)
                    $(this).nextUntil('tr[class=getInfo]').show();
            })
        });
</script>
<!-- Cardex JS -->
<script>
    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready(function(){
        $(':file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if(input.length && log)
                input.val(log);
        });
        $("button[name='setting']").on('click', function () {
            var parent = $(this).parent();
            var setting = $(this).val();
            var id = parent.find("input[name='id']").val();
            var value = parent.find("input[name='value']").val();
            var url = parent.find("input[name='url']").val();
            var cardexForm = constructCardexForm(url, value, id, setting);
            var modal = bootbox.dialog({
                backdrop: true,
                size: "small",
                title: "Editez les champs que vous souhaitez !",
                message: cardexForm
            });
            modal.init(function () {
                $('#cancel').on('click', function () {
                    modal.modal('hide');
                })
            });
        })
    });
</script>
<?php
require('../../inc/footer_bo.php');