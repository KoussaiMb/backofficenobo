<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');
$db = App::getDB();

if ($_POST['action'] == 'edit') {
    $validator = new Validator($_POST);

    $validator->is_alphanum('code', "le code n'est pas correct.", "Entrez un code de promotion");
    if ($validator->is_valid() && $_POST['code'] != $_POST['old_code'])
        $validator->is_uniq($db, 'promocode', 'code', 'code', 'Ce code existe déjà');
    $validator->is_num('id', "L'id n'est pas valide", "L'id n'est pas valide");
    $validator->no_symbol('name', "le nom n'est pas correct.", "Entrez un nom qui désigne le code de promotion");
    if ($validator->is_valid())
        $validator->is_uniqButHimself($db, 'promocode', 'name', 'name', $_POST['id'], 'Ce nom existe déjà');
    $validator->is_num('percentage', "la réduction en pourcentage n'est pas correcte.");
    $validator->is_range('percentage', "la réduction en pourcentage n'est pas correcte.", [0, 100]);
    $validator->is_num('euro', "La réduction en euro est incorrecte.");
    $validator->is_range('euro', "La réduction en euro est incorrecte", [0, 100]);
    $validator->is_date_bo('start', "La date du commencement de la promotion n'est pas correcte.", "Entrez une date de début de promotion.");
    $validator->is_date_bo('end', "La date de fin de la promotion n'est pas correcte.", "Entrez une date de fin de promotion");
    $validator->is_num('number', "Le nombre de promotion est incorrect", "Entrez un nombre limite de code");
    $validator->is_range('number', "Le nombre de promotion est incorrect", [0, 1000]);
    $validator->is_num('promocodeGroup_id', 'Le groupe de promocode n\'est pas valide', 'Le groupe de promocode est maquant');
    $validator->in_list('promocodeGroup_id', App::getListManagement()->getList('promocodeGroup'), 'Le groupe n\'existe pas');
    if ($_POST['start'] > $_POST['end'])
        $validator->throwException('date', 'La période du code de promotion est invalide');
    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), "edit?id=" . $_POST['id']);
    $ret = App::getPromoCode()->editPromoCode($_POST);
    if ($ret === false)
        App::setFlashAndRedirect("danger", "Le code de promotion n'a pas été modifié", "edit?id=" . $_POST['id']);
    App::setFlashAndRedirect("success", "Le code de promotion a été modifié avec succès", "edit?id=" . $_POST['id']);
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);

    $validator->is_id('id', "L'id n'est pas valide", "L'id n'est pas valide");
    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), 'add');
    $ret = App::getPromoCode()->deletePromoCode($_POST['id']);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la suppression du code de promotion', "edit?id=" . $_POST['id']);
    App::setFlashAndRedirect('success', 'Le code de promotion a été supprimé avec succès', 'view');
}
else if($_POST['action'] == 'add') {
    $_POST['created_by'] = App::getAuth()->user()->id;
    $validator = new Validator($_POST);

    $validator->is_alphanum('code', "le code n'est pas correct.", "Entrez un code de promotion");
    if ($validator->is_valid())
        $validator->is_uniq($db, 'promocode', 'code', 'code', 'Ce code existe déjà');
    $validator->no_symbol('name', "le nom n'est pas correct.", "Entrez un nom qui désigne le code de promotion");
    if ($validator->is_valid())
        $validator->is_uniq($db, 'promocode', 'name', 'name', 'Ce nom existe déjà');
    $validator->is_num('percentage', "la réduction en pourcentage n'est pas correcte.");
    $validator->is_id('created_by', "Vous nous êtes étranger", "Vous n'êtes pas identifié");
    $validator->is_range('percentage', "la réduction en pourcentage n'est pas correcte.", [0, 100]);
    $validator->is_num('euro', "La réduction en euro est incorrecte.");
    $validator->is_range('euro', "La réduction en euro est incorrecte", [0, 100]);
    $validator->is_date_bo('start', "La date du commencement de la promotion n'est pas correcte.", "Entrez une date de début de promotion.");
    $validator->is_date_bo('end', "La date de fin de la promotion n'est pas correcte.", "Entrez une date de fin de promotion");
    $validator->is_num('number', "Le nombre de promotion est incorrect", "Entrez un nombre limite de code");
    $validator->is_range('number', "Le nombre de promotion est incorrect", [0, 1000]);
    $validator->is_num('promocodeGroup_id', 'Le groupe de promocode n\'est pas valide', 'Le groupe de promocode est maquant');
    $validator->in_list('promocodeGroup_id', App::getListManagement()->getList('promocodeGroup'), 'Le groupe n\'existe pas');
    if ($_POST['start'] > $_POST['end'])
        $validator->throwException('date', 'La période du code de promotion est invalide');
    if (!$validator->is_valid()) {
        Session::getInstance()->write('promocode', $validator->getData());
        App::array_to_flash($validator->getErrors(), 'add');
    }
    $ret = App::getPromoCode()->addPromoCode($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le promocode n\'a pas été ajouté', 'add');
    Session::getInstance()->delete('promocode');
    App::setFlashAndRedirect('success', "Le promocode a été ajouté", "view");
}
App::redirect('view');