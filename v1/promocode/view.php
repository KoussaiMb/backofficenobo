<?php
require('../../inc/bootstrap.php');


$codeList = App::getPromoCode()->getPromoCodes();
$promocodeGroup = App::getListManagement()->getList("promocodeGroup");

if (!$codeList)
    Session::getInstance()->setFlash('info', 'Aucun code de promotion trouvé');

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Gestion des codes de promotion
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-sm-3">
            <label for="promocodeGroup">Groupe de promocode</label>
            <select name="promocodeGroup" class="form-control" id="promocodeGroup">
                <option value="0"></option>
                <?php foreach ($promocodeGroup as $k => $v): ?>
                    <option value="<?= $v->id; ?>"><?= $v->field; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label for="promoCode_id">Liste des codes</label>
            <select name="promoCode_id" class="form-control" id="promoCode_id">
                <?php foreach ($codeList as $k => $v): ?>
                    <option value="<?= $v->id; ?>"><?= $v->name; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-sm-5 text-center" style="padding-top: 22px">
            <button type="button" class="btn btn-default" name="action" value="edit" onclick="goToEditPromoCode();">Editer le code promo</button>
            <a href="add"><button type="button" class="btn btn-success" name="action" value="add">Ajouter un code promo</button></a>
        </div>
        <div class="col-xs-12 view-container">
            <?php foreach ($codeList as $k => $v): ?>
                <div class="col-xs-4 promocode-view">
                    <div class="col-xs-12">
                        <input type="hidden" name="promocodeGroup_id" value="<?= $v->promocodeGroup_id;?>">
                        <input type="hidden" name="id" value="<?= $v->id;?>">
                        <div class="col-xs-12">
                            <p style="color: #204d74;"><?= $v->name?></p>
                        </div>
                        <div class="col-xs-6">
                            <p>
                                <?= $v->percentage == 0 ? $v->euro . '€' : $v->percentage . '%'; ?>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p><?= $v->code?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<style>
    .view-container {
        overflow-y: scroll;
        margin-top: 2em;
        border: 2px solid #DBEED4;
        border-radius: 3px;
    }
    .promocode-view {
        padding: 20px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .promocode-view > div:hover {
        border: 1px solid grey;

    }
    .promocode-view > div {
        background-color: #c2c2d6;
        border-radius: 3px;
        padding-top: 10px;
        height: 80px;
        cursor: pointer;
    }
    .promocode-view > div div:first-child {
        color: ;
    }
    .promocode-view > div div:last-child {
        color: #e60073;
    }
</style>
<script>
    function goToEditPromoCode() {
        var url = 'edit?id=' + $('#promoCode_id').val();
        window.location = url;
    }
    jQuery(document).ready(function() {
        var allPromocode = $('.promocode-view > div');
        var allDiv = $('.promocode-view');

        $(".view-container").css('max-height', $(window).height() * 0.7);
        allPromocode.on('click', function () {
            window.location = 'edit?id=' + $(this).find("input[name='id']").val();
        });
        $('#promocodeGroup').on('change', function () {
            pG_id = $(this).val();
            if (pG_id == 0)
                allDiv.show();
            else {
                allDiv.hide();
                $("input[name='promocodeGroup_id'][value='"+ pG_id +"']").parentsUntil("div[class='promocode-view']").show();
            }
        })
    });
</script>
<?php
require('../../inc/footer_bo.php');