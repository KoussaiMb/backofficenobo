<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('promocode');

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Création d'un nouveau code de promotion
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <form method="post" action="form">
                <div class="form-group">
                    <div class="input-group">
                        <?php $promocodeGroup = App::getListManagement()->getList('promocodeGroup'); ?>
                        <select class="form-control input-sm" name="promocodeGroup_id"  id="promocodeGroup_id">
                            <?php foreach ($promocodeGroup as $k => $v): ?>
                                <option value="<?= $v->id; ?>" <?=  isset($data) && $data['promocodeGroup_id'] == $v->id ?  : 'selected'; ?>><?= $v->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="input-group-btn">
                            <a href="/v1/list/promocode/edit" target="_blank"><button type="button" class="btn btn-primary btn-sm">Ajouter un groupe</button></a>
                        </span>
                    </div>
                </div>
                <div class="form-group col-sm-6" style="padding-left: 0">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="Code nucléaire" value="<?= isset($data) ? $data['name'] : null; ?>" id="name" required>
                </div>
                <div class="form-group col-sm-6" style="padding-right: 0">
                    <label for="code">Code</label>
                    <input type="text" class="form-control input-sm" name="code" placeholder="Nobo123" value="<?=  isset($data) ? $data['code'] : null; ?>" id="code" required>
                </div>
                <div class="form-group col-sm-6" style="padding-left: 0">
                    <label for="start">Début de la promotion</label>
                    <input type="text" class="form-control input-sm pickadate" name="start" value="<?=  isset($data) ? $data['start'] : null; ?>" placeholder="click here" id="start" required>
                </div>
                <div class="form-group col-sm-6" style="padding-right: 0">
                    <label for="end">Fin de la promotion</label>
                    <input type="text" class="form-control input-sm pickadate" name="end" value="<?= isset($data) ? $data['end'] : null; ?>" placeholder="click here" id="end" required>
                </div>
                <div class="form-group">
                    <label for="number">Nombre de code à distribuer</label>
                    <input type="text" class="form-control input-sm" name="number" value="<?= isset($data) ? $data['number'] : null; ?>" placeholder="1000 max" id="number" required>
                </div>
                <div class="form-group text-center">
                    <label class="col-xs-12">Choisir le type de reduction</label>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default" name="show" value="percentage">Pourcentage</button>
                        <button type="button" class="btn btn-default" name="show" value="euro">Réduction</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group two-addon showField hidden">
                        <label class="input-group-addon" for="percentage">Pourcentage de la promotion <span class="feature-description">[0-100]</span></label>
                        <input type="text" class="form-control input-sm" name="percentage" value="<?=  isset($data['percentage']) ? $data['percentage'] : 0; ?>" id="percentage">
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group two-addon showField hidden">
                        <label class="input-group-addon" for="euro">Réduction de la promotion <span class="feature-description">[0-100]</span></label>
                        <input type="text" class="form-control input-sm" name="euro" value="<?=  isset($data['euro']) ? $data['euro'] : 0; ?>" id="euro">
                        <span class="input-group-addon">€</span>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-success" name="action" value="add">Ajouter le code de promotion</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        $('button[name=show]').on('click', function () {
            $('.showField').addClass('hidden');
            $('#' + $(this).val()).closest('div').removeClass('hidden');
        })
    })
</script>
<?php
require('../../inc/footer_bo.php');