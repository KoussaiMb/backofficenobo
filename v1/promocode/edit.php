<?php
require('../../inc/bootstrap.php');

if (empty($_GET['id']))
    App::setFlashAndRedirect('warning', "Choisissez un code de promotion valide", "view");
$promoCode = App::getPromoCode()->getPromoCodeById($_GET['id']);
if (empty($promoCode))
    App::setFlashAndRedirect('info', 'Imposible de retrouver le code de promotion demandé.', 'view');

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Gestion des codes de promotion
        <span style="float: right">Codes restants:  <span class="badge"><?= $promoCode->number - $promoCode->used; ?></span></span>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <form method="post" action="form">
                <input class="hidden" name="id" value="<?= $promoCode->id; ?>" id="promoId">
                <div class="form-group">
                    <div class="input-group">
                        <?php $promocodeGroup = App::getListManagement()->getList('promocodeGroup'); ?>
                        <select class="form-control input-sm" name="promocodeGroup_id"  id="promocodeGroup_id">
                            <?php foreach ($promocodeGroup as $k => $v): ?>
                                <option value="<?= $v->id; ?>" <?=  $promoCode->promocodeGroup_id == $v->id ? 'selected' : null; ?>><?= $v->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="input-group-btn">
                            <a href="/v1/list/promocode/edit" target="_blank"><button type="button" class="btn btn-primary btn-sm">Ajouter un groupe</button></a>
                        </span>
                    </div>
                </div>
                <div class="form-group col-sm-6" style="padding-left: 0">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="Code nucléaire" value="<?= $promoCode->name; ?>" id="name" required>
                </div>
                <div class="form-group col-sm-6" style="padding-right: 0">
                    <label for="code">Code</label>
                    <input type="hidden" name="old_code" value="<?= $promoCode->code; ?>">
                    <input type="text" class="form-control input-sm" name="code" placeholder="Nobo123" value="<?= $promoCode->code; ?>" id="code" required>
                </div>
                <div class="form-group col-sm-6" style="padding-left: 0">
                    <label for="start">Début de la promotion</label>
                    <input type="text" class="form-control input-sm pickadate" name="start" value="<?= $promoCode->start; ?>" placeholder="click here" id="start" required>
                </div>
                <div class="form-group col-sm-6" style="padding-right: 0">
                    <label for="end">Fin de la promotion</label>
                    <input type="text" class="form-control input-sm pickadate" name="end" value="<?= $promoCode->end; ?>" placeholder="click here" id="end" required>
                </div>
                <div class="form-group">
                    <label for="number">Nombre de code à distribuer</label>
                    <input type="text" class="form-control input-sm" name="number" value="<?= $promoCode->number; ?>" placeholder="1000 max" id="number" required>
                </div>
                <div class="form-group">
                    <div class="input-group two-addon">
                        <label class="input-group-addon" for="percentage">Pourcentage de la promotion</label>
                        <input type="text" class="form-control input-sm" name="percentage" value="<?= $promoCode->percentage; ?>" placeholder="0 - 100" id="percentage">
                        <span class="input-group-addon">%</span>
                    </div>
                    <div class="input-group two-addon">
                        <label class="input-group-addon" for="euro">Réduction de la promotion</label>
                        <input type="text" class="form-control input-sm" name="euro" value="<?= $promoCode->euro; ?>" placeholder="100 euros max" id="euro">
                        <span class="input-group-addon">€</span>
                    </div>
                </div>
                <div class="SpaceTop" style="text-align: center;">
                    <button type="submit" class="btn btn-success" name="action" value="edit">Sauvegarder les modifications</button>
                    <button type="submit" class="btn btn-danger" name="action" value="delete">Supprimer le code de promotion</button>
                    <a href="view"><button type="button" class="btn btn-warning btn-sm SpaceTop" name="action" value="view">Revernir aux promocodes</button></a>
                </div>
                <div class="col-xs-6 SpaceTop">
                    <p class=""></p>
                    <p class="">Auteur:  <?= $promoCode->firstname . ' ' . $promoCode->lastname; ?></p>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
require('../../inc/footer_bo.php');