<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('/v1/client/edit');
$db = App::getDB();

if ($_POST['action'] == 'add')
{
    $validator = new Validator($_POST);
    $validator->is_id('home', 'Est-ce son domicile ?', 'Est-ce son domicile ?');
    $validator->isToken('user_token', 'Le token est invalide', 'Le token est manquant');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("add?user_token=" . $_POST['user_token']);
    }
    $address = App::getAddress();
    $user = App::getUser()->getUserByToken($_POST['user_token']);
    $_POST['user_id'] = $user->id;
    if ($_POST['home'] == 1 && $address->setAllAddressWithoutHome($user->id) == false)
        App::setFlashAndRedirect('danger', "Impossible de mettre le domicile en tant ", '/v1/client/edit?user_token=' . $_POST['user_token']);
    $address_token = $address->addAddress($_POST);
    $validator->is_num('radio_add_wlist', 'Impossible de récupérer l\'ajout à la waiting list');
    if(!$validator->is_valid()){
        App::array_to_flash($validator->getErrors());
        App::redirect("add?user_token=" . $_POST['user_token']);
    }
    if(!$address_token){
        App::setFlashAndRedirect('danger', 'L\'adresse n\'a pas été créée', 'add');
    }
    if($_POST['radio_add_wlist']){
        $address_id = $address->getAddressByToken($address_token);
        if(!$address_id->id){
            App::setFlashAndRedirect('danger', "Impossible d'ajouter l'utilisateur à la liste d'attente", '/v1/client/edit?user_token=' . $_POST['user_token']);
        }
        $user_info = App::getUser()->addWaitingList($address_id->id);
        if($user_info){
            App::setFlashAndRedirect('success', "L'adresse a bien été créée", "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $address_token . "&menu_id=menu2");
        }
    }
    App::setFlashAndRedirect('success', "L'adresse a bien été créée", "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $address_token . "&menu_id=menu2");
}
else if ($_POST['action'] == 'delete')
{
    $validator = new Validator($_POST);
    $validator->isToken('address_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->isToken('user_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
    }
    if (App::getAddress()->deleteAddressByToken($_POST['address_token']))
        App::setFlashAndRedirect('warning', "L'adresse a été supprimée", "/v1/client/edit?id=" . $_POST['user_token'] . "&menu_id=menu2");
    App::setFlashAndRedirect('danger', "L'adresse n'a pas pu être supprimée", "/v1/client/edit?id=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
}
else if ($_POST['action'] == 'edit' && $_POST['who'] == 'client')
{
    $validator = new Validator($_POST);
    $validator->isToken('address_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->isToken('user_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->is_num('roomNb', 'Le nombre de chambre doit être un chiffre');
    $validator->is_num('waterNb', 'Le nombre de toilette doit être un chiffre');
    $validator->is_num('surface', 'La surface doit être un nombre');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
    }
    $address = App::getAddress();
    $user = App::getUser()->getUserByToken($_POST['user_token']);
    $_POST['user_id'] = $user->id;
    if ($_POST['home'] == 1 && $address->setAllAddressWithoutHome($user->id) == false)
        App::setFlashAndRedirect('danger', "Impossible de mettre le domicile en tant que domicile principal", '/v1/client/edit?user_token=' . $_POST['user_token']);
    if ($address->updateAddressFromCustomer($_POST) === false)
        App::setFlashAndRedirect('danger', 'L\'adresse n\'a pas pu être modifiée',  "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
    App::setFlashAndRedirect('info', "L'adresse a bien été modifiée", "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
}
else if ($_POST['action'] == 'edit' && $_POST['who'] == 'provider')
{
    $validator = new Validator($_POST);
    $validator->isToken('address_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->isToken('user_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
    }
    $user = App::getUser()->getUserByToken($_POST['user_token']);
    $_POST['user_id'] = $user->id;
    if (App::getAddress()->updateAddressFromProvider($_POST) === false)
        App::setFlashAndRedirect('danger', 'L\'adresse n\'a pas pu être modifiée',  "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
    App::setFlashAndRedirect('info', "L'adresse a bien été modifiée", "/v1/client/edit?user_token=" . $_POST['user_token'] . "&address_token=" . $_POST['address_token'] . "&menu_id=menu2");
}
