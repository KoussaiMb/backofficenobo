<?php
require('../../inc/bootstrap.php');

if (empty($_GET['user_token']) || preg_match('/^[a-f\d]+$/', $_GET['user_token']) == false)
    App::setFlashAndRedirect('danger', "Client inconnu au bataillon", "../client/view");

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Création d'une nouvelle adresse
    </div>
    <div class="panel-body">
        <div class="col-sm-3">
            <div class="alert alert-info col-sm-12" role="alert">
                <p>Entrez l'adresse grâce à l'<strong>autocomplétion</strong></p>
            </div>
        </div>
        <div class="col-sm-6">
            <form method="post" action="form">
                <input type="hidden" name="lat" id="lat_home" readonly>
                <input type="hidden" name="lng" id="lng_home" readonly>
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                        <input type="text" class="form-control" id="autocomplete_home" required>
                        <span class="input-group-btn"><button type="button" class="btn btn-default" onclick="$('#trigger').trigger('click');">Remove security</button></span>
                    </div>
                    <br/>
                    <input type="hidden" class="form-control" name="street_number" id="street_number_home" disabled>
                    <input type="hidden" class="form-control" name="route" id="route_home" disabled>
                    <input type="text" class="form-control" name="address" id="address_home" readonly>
                    <input type="text" class="form-control" name="address_ext" id="administrative_area_level_1_home" readonly>
                    <input type="text" class="form-control" name="zipcode" id="postal_code_home" readonly>
                    <input type="text" class="form-control" name="city" id="locality_home" readonly>
                    <input type="text" class="form-control" name="country" id="country_home" readonly>
                    <input type="hidden" id="trigger" onclick="$(this).prevUntil('button').attr('readonly', function(_, attr){return !attr});">
                    <input type="hidden" name="home" value="0">
                    <button type="button" class="btn btn-danger col-sm-12" onclick="$(this).prev().val() === '0' ? $(this).removeClass('btn-danger').addClass('btn-primary').text('Cette adresse est aussi son domicile').prev().val(1) : $(this).addClass('btn-danger').removeClass('btn-primary').text('Cette adresse n\'est pas son domicile').prev().val(0);">Cette adresse n'est pas son domicile</button>
                    <div class="row col-sm-12">
                        <label for="radio_wlst_yes">Ajouter le client en liste d'attente ?</label>
                        <div>
                            <label class="radio-inline">
                                <input name="radio_add_wlist" id="radio_wlist_yes" value="1" type="radio" />Oui
                            </label>
                            <label class="radio-inline">
                                <input name="radio_add_wlist" id="radio_wlist_no" value="0" type="radio" checked/>Non
                            </label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="user_token" value="<?= $_GET['user_token'];?>">
                <button type="submit" class="btn btn-success col-sm-6 col-sm-offset-3" name="action" value="add" style="margin-top: 1em">Créer une nouvelle adresse</button>
            </form>
        </div>
        <div class="col-sm-3">
            <?php require('../../inc/print_flash_create.php'); ?>
        </div>
    </div>
</div>
<script>
    var placeSearch, autocomplete_home;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };
    function fillInAddr_home() {
        let id = 'home';
        let place = autocomplete_home.getPlace();
        for (let component in componentForm) {
            document.getElementById(component+ '_' + id).value = '';
        }
        for (let i = 0; i < place.address_components.length; i++) {
            let addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType + '_' + id).value = val;
            }
        }
        $('#lat_' + id).val(place.geometry.location.lat());
        $('#lng_' + id).val(place.geometry.location.lng());
        $('#address_' + id).val($('#street_number_' + id).val() + ' ' + $('#route_' + id).val());
    }
    function initialize() {
        initGoogleAutocomplete_home('autocomplete_home');
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBeMne5-vmGGRYsdcCxDb-PYs3McbjVmCg&libraries=places&callback=initialize" async defer></script>
<?php
require('../../inc/footer_bo.php');