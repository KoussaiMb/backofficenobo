<?php
include_once ("../../../inc/bootstrap.php");

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);

    $validator->is_id('subscriber_id', 'Prospect invalide', 'prospect non renseigné');
    if (!$validator->is_valid())
        parseJson::error('Le prospect n\'a pas été ajouté à la liste d\'attente', $validator->getErrors())->printJson();
    $ret = App::getUser()->subscriberToWaitingList($_POST['subscriber_id']);
    if ($ret === false)
        $json = parseJson::error('Le prospect n\'a pas été ajouté à la liste d\'attente', $ret);
    else
        $json = parseJson::success('Le prospect a bien été ajouté à la liste d\'attente', $ret);
    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('subscriber_id', 'Prospect invalide', 'prospect non renseigné');
    if (!$validator->is_valid())
        parseJson::error('Le prospect n\'a pas été supprimé', $validator->getErrors())->printJson();
    $ret = App::getUser()->deleteSubscriber($_DELETE['subscriber_id']);
    if ($ret === false)
        $json = parseJson::error('Le prospect n\'a pas été supprimé');
    else
        $json = parseJson::success('Le prospect a bien été supprimé');
    $json->printJson();
}