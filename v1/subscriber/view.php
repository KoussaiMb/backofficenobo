<?php
require('../../inc/bootstrap.php');

$subscribers = App::getUser()->getSubscriber();

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Gestion des prospects
    </div>
    <div class="panel-body">
        <div class="col-sm-12" id="test">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <?php if(isset($subscribers)): ?>
            <div class="col-xs-12 table-responsive">
            <table class="table table-hover" id="subscriber">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Superficie</th>
                    <th>Temps de travail</th>
                    <th>Récurrence</th>
                    <th>Date</th>
                    <th>price</th>
                    <th>Address</th>
                    <th>Zipcode</th>
                    <th>payed</th>
                    <th>modif.</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($subscribers as $key => $v): ?>
                    <tr>
                        <?php $workTime = explode(':', $v->workTimeChosen); ?>
                        <input type="hidden" name="subscriber_id" value="<?= $v->id ?>">
                        <td><?= $v->firstname . ' ' . $v->lastname; ?></td>
                        <td><?= $v->surface; ?></td>
                        <td><?= $workTime[0] . 'h' . $workTime[1]; ?></td>
                        <td class="size-xs"><?= App::getListManagement()->getFieldById($v->recurrence_id); ?></td>
                        <td><?= substr($v->date, 0, -9); ?></td>
                        <td><?= substr($v->price, 0, -3); ?></td>
                        <td class="size-xs"><?= $v->address; ?></td>
                        <td><?= $v->zipcode; ?></td>
                        <td><?= $v->payed; ?></td>
                        <td><?= $v->modifiedNumber; ?></td>
                        <td class="subscriber-action">
                            <button class="btn btn-success btn-xs" name="action" value="add"><span class="glyphicon glyphicon-plus"></button>
                            <button class="btn btn-danger btn-xs" name="action" value="delete"><span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
<style>
    .subscriber-action > a:last-child > button {
        color: red;
    }
    .subscriber-action > a:first-child > button {
        color: green;
    }
    .subscriber-action button{
        padding: 1px 5px;
    }
    .subscriber-action > button {
    }
    .subscriber-action {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }
    .size-xs {
        ;
    }
    .bg-checked:hover, .bg-notChecked:hover {
        border: 2px solid black;
    }
</style>
<script src="/js/app.js"></script>
<script>
    jQuery(document).ready(function() {
        $('.subscriber-action>button').on('click', function () {
            let row = $(this).closest('tr');
            let action_message = {
                add: "Voulez-vous ajouter " + row.find('td:first-child').text() + " à la liste d'attente ?",
                delete: "Voulez-vous supprimer " + row.find('td:first-child').text() + " de la liste des prospects ?"
            };
            let ajax_method = {
                add : 'POST',
                delete : 'DELETE'
            };
            let message = action_message[$(this).val()];

            if (message !== undefined && confirm(message)) {
                let data = {subscriber_id : row.find("input[name='subscriber_id']").val()};

                console.log(data);
                console.log(ajax_method[$(this).val()]);

                ajax_call('api/subscriber', ajax_method[$(this).val()], data, function (ret) {
                    console.log(ret);
                    if (ret.code === '200') {
                        bo_notify(ret.message, 'success');
                        row.remove();
                    } else
                        bo_notify(ret.message, 'danger');
                    }, function (error) {
                    bo_notify('Un problème est survenu, appelez un admin', 'danger');
                    ajax_log(error.responseText, '/v1/subscriber/view', 'api/subscriber en ' + ajax_method[$(this).val()])
                });
            }
        })
    });
</script>
<?php
require('../../inc/footer_bo.php');