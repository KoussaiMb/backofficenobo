<?php
require('../../inc/bootstrap.php');

$conf = App::getConf()->getAll();

if (!empty($_GET['menu_id'])){
    $menu_id = substr($_GET['menu_id'], -1);
}
else if (!empty($_GET['menu'])) {
    $menu_tab = ['references'];
    $menu_id = App::get_position_in_array($_GET['menu'], $menu_tab) + 1;
}
else {
    $menu_id = '1';
}

$tables = App::getSurvey()->getTableNames();
if ($tables === null){
    Session::getInstance()->setFlash('danger', 'Impossible de récupérer les informations du serveur');
}

require('../../inc/header_bo.php');
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div style="min-height: 30px">
                <div class="col-sm-12">
                    <ul class="nav nav-pills pillsWrapper customColor" id='tabs_provider'>
                        <input type="hidden" value="<?= $menu_id; ?>" id="get_menu_id" disabled>
                        <li id="tab1"><a data-toggle="tab" href="#menu1">Références tables</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="tab-content">
                <div id="menu1" class="tab-pane fade">
                    <div class="col-sm-3 select-side-wrapper subMenu">
                        <div class="select-side current" data-href="referencesSetup">
                            Gestion des références
                        </div>
                    </div>
                    <div class="col-sm-9 content-side" id="referencesManagement">
                        <form method='POST' class="form-inline" action="form">

                            <label for="select_table_id">Table</label>
                            <select class="form-control" id="select_table_id" name="select_table" required>
                                <?php foreach ($tables as $tab){ ?>
                                    <option value="<?= $tab->table_name ?>"><?= $tab->table_name ?></option>
                                <?php } ?>
                            </select>

                            <label for="select_table_id">Colonne</label>
                            <select class="form-control" id="select_column_id" name="select_column" required>
                            </select>

                            <label for="nameId" class="sr-only">Nom</label>
                            <input type="text" class="form-control" id="nameId" name="ref_name" placeholder="Nom" required>

                            <button type="submit" class="btn btn-success" id="addReference" name="action" value="add">Ajouter une référence</button>
                        </form>
                        <br><br>
                        <div class="table-responsive col-sm-10 col-sm-offset-1">
                            <table id="table_rooms" class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Table</th>
                                    <th>Champs</th>
                                    <th>Nom</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $references = App::getSurvey()->getAllFieldsReferences();
                                    foreach ($references as $ref) {
                                        ?>
                                        <tr>
                                            <td class="big-font"><?= $ref->table_name ?></td>
                                            <td class="big-font"><?= $ref->field_name ?></td>
                                            <td class="big-font"><?= $ref->ref_name ?></td>
                                            <td>
                                                <form method='POST' action="form">
                                                    <input type="hidden" name="ref_id" value="<?= $ref->id ?>">
                                                    <button type="submit" class="btn btn-danger" name="action" value="delete" onclick="return confirm('Attention ! Supprimer cette référence supprimera également les questions correspondantes. Continuer ?');">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .select-side:hover {
            border: 1px solid #3277B2;
        }
        .select-side {
            text-align: center;
            padding: 5px;
            cursor: pointer;
            border: 1px solid transparent;
        }
        .select-side.current {
            background-color: #3277B2;
            color: white;
        }
        .content-side {
            border-left: 2px solid #3277B2;
        }
        .big-font {
            font-size: 14px;
        }
    </style>

    <script type="text/javascript" src='/js/app.js'></script>
    <script>
        jQuery(document).ready(function() {
            var menu_id = $('#get_menu_id').val();
            var subMenu = '<?php echo !empty($_GET['subMenu']) ? $_GET['subMenu'] : '';?>';

            $('#tab' + menu_id).addClass('active');
            $('#menu' + menu_id).addClass('in active');

            if (subMenu) {
                $('.subMenu').each(function () {
                    if (isTextInSubMenu($(this), subMenu)) {
                        showSubMenu($('div[data-href=' + subMenu + ']'));
                        return false;
                    }
                });
            }

            $(".select-side").on('click', function () {
                showSubMenu($(this));
            });


            $('#select_table_id').on('change', function(){
                var selected_table_val = $('#select_table_id').val();

                $.ajax({
                    url: 'api',
                    type: "GET",
                    data: {
                        table_name : selected_table_val
                    },
                    success: function (json) {
                        var res = JSON.parse(json);
                        if (res.code !== 400){
                            createColumnSelect(res.data);
                        } else {
                            bo_notify('Impossible de charger les informations de l\'autocomplete', 'danger');
                        }
                    },
                    error: function () {
                        bo_notify('Erreur inconnue lors de la construction de l\'autocomplete, contacter un admin', 'danger');
                    }
                })
            }).trigger('change');

            });
        
        function createColumnSelect(data)
        {

            var select = document.getElementById('select_column_id');
            select.innerHTML = "";

            var data_length = data.length;
            for (var i = 0; i < data_length; i++){
                var opt = document.createElement('option');
                opt.value = data[i]['value'];
                opt.innerHTML = data[i]['value'];
                select.appendChild(opt);
            }
        }
    </script>
<?php
require('../../inc/footer_bo.php');