<?php
require_once ('../../inc/bootstrap.php');


if ($_POST['action'] === 'add') {
    $validator = new Validator($_POST);

    if (empty($_POST['select_column']))
        $validator->throwException('colonne', 'Erreur lors de la récupération de la colonne');
    if (empty($_POST['select_table']))
        $validator->throwException('table', 'Erreur lors de la récupération de la table');
    $validator->no_symbol('ref_name', 'Le nom est invalide', 'Le nom est vide');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', 'Le nom renseigné est incorrect', 'edit');

    $ret = App::getSurvey()->insertFieldReference($_POST['select_table'], $_POST['select_column'], $_POST['ref_name']);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Impossible d\'insérer la référence', 'edit');

    App::setFlashAndRedirect('success', 'Réference insérée avec succès', 'edit');
}
else if ($_POST['action'] === 'delete'){
    $validator = new Validator($_POST);

    $validator->is_id('ref_id');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', 'Impossible de récupérer l\'élément sélectionné', 'edit');

    $ret = App::getSurvey()->deleteReferenceById($_POST['ref_id']);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Impossible de supprimer l\'élément', 'edit');

    $questions = App::getSurvey()->getQuestionsByRefId($_POST['ref_id']);
    if ($questions){
        $ret = App::getSurvey()->halfDeleteQuestionByRefId($_POST['ref_id']);
        if (!$ret)
            App::setFlashAndRedirect('danger', 'Impossible de supprimer les questions correspondantes', 'edit');
    }
    App::setFlashAndRedirect('success', 'Réference supprimée avec succès', 'edit');
}