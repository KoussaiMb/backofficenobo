<?php
require_once ('../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if(isset($_GET['table_name']) && !empty($_GET['table_name'])){

        $table_name = $_GET['table_name'];
        $column_autocomplete = array();

        $columns = App::getSurvey()->getColumnsInTable($table_name);
        if ($columns == false){
            $json = parseJson::error('Impossible de récupérer les colonnes');
            $json->printJson();
        }
        foreach ($columns as $col) {
            array_push($column_autocomplete, [
                'value' => $col->column_name,
                'data' => [
                    'field' => $col->column_name,
                ],
            ]);
        }
        $json = parseJson::success(null,$column_autocomplete);
        $json->printJson();
    }
    $json = parseJson::error('Impossible de récupérer le nom de la table');
    $json->printJson();
}