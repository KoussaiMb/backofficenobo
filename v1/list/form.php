<?php
require('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin' || empty($_POST) || empty($_POST['action']))
    App::setFlashAndRedirect('danger', 'Une erreur est survenue', 'edit');

if ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    $validator->is_alpha('listName', 'La liste est invalide', 'Il manque le nom de la liste');
    $validator->is_alpha('field', 'La valeur du champ est invalide', 'Il faut une valeur');
    $validator->is_num('position', 'La position est invalide');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->addInput($_POST['listName'], $_POST['field'], $_POST['position']);
    if ($ret == false)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors de l\'ajout de l\'élément', 'edit?listName=' . $_POST['listName']);
    App::setFlashAndRedirect('success', 'L\'élément a bien été ajouté', 'edit?listName=' . $_POST['listName'] );
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Liste ou champ invalide', 'Liste ou champ introuvable');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->delInput($_POST['id']);
    if ($ret == 0)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors de la suppression de l\'élément', 'edit?listName=' . $_POST['listName']);
    App::setFlashAndRedirect('success', 'L\'élément a bien été supprimé', 'edit?listName=' . $_POST['listName']);
}
else if ($_POST['action'] == 'callback') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Liste ou champ invalide', 'Liste ou champ introuvable');
    $validator->is_alpha('listName', 'La liste est invalide', 'Il manque le nom de la liste');
    $validator->is_num('position', 'La position est invalide');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->callbackInput($_POST['listName'], $_POST['id'],$_POST['position']);
    if ($ret == 0)
        App::setFlashAndRedirect('danger', 'un problème est survenu lors du callback de l\'élément', 'edit' . $_POST['listName']);
    App::setFlashAndRedirect('success', 'L\'élément a bien été remis actif', 'edit?listName=' . $_POST['listName']);
}