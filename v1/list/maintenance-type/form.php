<?php
require('../../../inc/bootstrap.php');

if (empty($_POST) || empty($_POST['action']))
    App::redirect("edit");

if ($_POST['action'] == 'add')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('field', 'Le nom du type d\'entretien est invalide', 'Il faut renseigner un nom pour le type d\'entretien');
    $validator->is_num('position', 'La position doit être un nombre');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->addInput('maintenanceType', $_POST['field'], $_POST['position']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'Le type d\'entretien a été ajouté à la liste');
    else
        Session::getInstance()->setFlash('danger', 'Le type d\'entretien n\'a pas été ajouté à la liste');
}
else if ($_POST['action'] == 'delete')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Le type d\'entretien n\'est pas valide', 'Le type d\'entretien est manquant');
    if (!App::getListManagement()->inList('maintenanceType', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la liste des types d\'entretien');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->delInput($_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'Le type d\'entretien a été supprimé de la liste');
    else
        Session::getInstance()->setFlash('danger', 'Le type d\'entretien n\'a pas été supprimé de la liste');
}
else if ($_POST['action'] == 'callback')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Le type d\'entretien n\'est pas valide', 'Le type d\'entretien est manquant');
    if (!App::getListManagement()->inDeletedList('maintenanceType', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la liste des types d\'entretien');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->callbackInput('maintenanceType', $_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'Le type d\'entretien a été réintroduit dans la liste');
    else
        Session::getInstance()->setFlash('danger', 'Le type d\'entretien n\'a pas été réintroduit dans la liste');
}
App::redirect("edit");
