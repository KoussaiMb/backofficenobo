<?php
require('../../../inc/bootstrap.php');

if (empty($_POST) || empty($_POST['action']))
    App::redirect("edit");

if ($_POST['action'] == 'add')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('field', 'Le nom du groupe est invalide', 'Il faut renseigner un nom de groupe');
    $validator->is_num('position', 'La position doit être un nombre');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->addInput('promocodeGroup', $_POST['field'], $_POST['position']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'le groupe de promocode a été ajouté');
    else
        Session::getInstance()->setFlash('danger', 'le groupe de promocode n\'a pas été ajouté');
}
else if ($_POST['action'] == 'delete')
{
    $validator = new Validator($_POST);
    $validator->is_num('id', 'Le groupe de promocode n\'est pas valide', 'Le groupe de promocode n\'est pas valide');
    if (!App::getListManagement()->inList('promocodeGroup', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la liste des groupes de promocode');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->delInput($_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'le groupe de promocode a été supprimé');
    else
        Session::getInstance()->setFlash('danger', 'le groupe de promocode n\'a pas été supprimé');
}
else if ($_POST['action'] == 'callback')
{
    $validator = new Validator($_POST);
    $validator->is_num('id', 'Le groupe de promocode n\'est pas valide', 'Le groupe de promocode n\'est pas valide');
    if (!App::getListManagement()->inDeletedList('promocodeGroup', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la list des groupes de promocode');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->callbackInput('promocodeGroup', $_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'le groupe de promocode a été réinitioalisé');
    else
        Session::getInstance()->setFlash('danger', 'le groupe de promocode n\'a pas été réinitialisé');
}
App::redirect("edit");
