<?php
require('../../inc/bootstrap.php');

/**if(!empty($_GET['field'])){
    var_dump($_GET['action'], $_GET['listName'] ,$_GET['field'], $_GET['position']);
}**/

if (!empty($_GET['listName'])){
    $IsOk = 1;
    $table = App::getListManagement()->getList($_GET['listName']);
    $table_deleted = App::getListManagement()->getDeletedList($_GET['listName']);
}else {
    $IsOk = 0;
}
$listName_deleted =  App::getListManagement()->getAllDeletedList();
$listName =  App::getListManagement()->getAllList();

require('../../inc/header_bo.php');
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        Listes actuelles
    </div>
    <div class="panel-body">
        <form method="get">
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label" for="listName">listName</label>
                <div class="col-sm-6">
                    <select class="form-control" name="listName" id="listName">
                    <?php foreach ($listName as $val): ?>
                        <?php $selected = $IsOk && $val->listName == $_GET['listName'] ? 'selected' : null; ?>
                        <option value="<?= $val->listName; ?>" <?= $selected; ?>><?= $val->listName; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
                <button  class="btn btn-default col-sm-offset-1 col-sm-1 ">Afficher</button>
            </div>
        </form>
        <form method="post" action="form.php" class="form-inline">
            <div class="form-group col-sm-12">
                <label class="col-sm-1 control-label" for="toshow">Champs</label>
                <input class="col-sm-2 col-sm-offset-1 form-control" type="text" name="position" value="" placeholder="position">
                <input class="col-sm-2 form-control" type="text" name="field" value="" placeholder="field">
                <input class="col-sm-2 form-control" type="text" name="listName" value="" id="toshow" onclick="SwapStatement()" readonly>
                <button class="btn btn-default col-sm-offset-1 col-sm-1" name="action" value="add">Ajouter</button>
            </div>
        </form>

        <?php if(isset($table)): ?>
            <div class="table-responsive col-sm-6 col-sm-offset-2" style="margin-top: 2em">
                <table id="table_List" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-sm-1">position</th>
                        <th class="col-sm-2">Champ</th>
                        <th class="col-sm-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($table as $key): ?>
                        <tr>
                            <td><?= $key->position; ?></td>
                            <td><?= $key->field; ?></td>
                            <td style="text-align: right;">
                                <form action="form.php" method="post">
                                    <input type="hidden" name="listName" value="<?= $_GET['listName']; ?>">
                                    <input type="hidden" name="id" value="<?= $key->id; ?>">
                                    <button name="action" value="delete" class="btn glyphicon glyphicon-remove" style="background-color: #e6253c; color: whitesmoke"></button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        Anciennes listes
    </div>
    <div class="panel-body">
        <form method="get">
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label" for="listName_deleted">listName</label>
                <div class="col-sm-6">
                    <select class="form-control" name="listName" id="listName_deleted">
                        <?php foreach ($listName_deleted as $val): ?>
                            <?php $selected = $IsOk && $val->listName == $_GET['listName'] ? 'selected' : null; ?>
                            <?php if ($val->listName == 'attr'){continue;}?>
                            <option value="<?= $val->listName; ?>" <?= $selected; ?>><?= $val->listName; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button name="afficher" class="btn btn-default col-sm-offset-1 col-sm-1">Afficher</button>
            </div>
        </form>
        <?php if(isset($table_deleted)): ?>
            <div class="table-responsive col-sm-6 col-sm-offset-2" style="margin-top: 2em">
                <table id="table_DeletedList" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-sm-1">position</th>
                        <th class="col-sm-2">Champ</th>
                        <th class="col-sm-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($table_deleted as $key): ?>
                        <tr>
                            <td><?= $key->position; ?></td>
                            <td><?= $key->field; ?></td>
                            <td style="text-align: right;">
                                <form action="form.php" method="post">
                                    <input type="hidden" name="listName" value="<?= $_GET['listName']; ?>">
                                    <input type="hidden" name="id" value="<?= $key->id; ?>">
                                    <button name="action" value="callback" class="btn glyphicon glyphicon-refresh" style="background-color: #ADEA38; color: whitesmoke"></button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
<script>
    function SwapStatement(){
        if ($('#toshow').hasClass('clicked')){
            $('#toshow').prop('readonly', !$('#toshow').prop('readonly')).removeClass('clicked');
        }else{
            $('#toshow').addClass('clicked');
        }
    }
    function showit(){
        $('#toshow').attr('value', $('#listName').val());
    }
    jQuery(document).ready(function() {
        showit();
        $('#listName').change(showit);
    });
</script>
<?php
require('../../inc/footer_bo.php');