<?php
require('../../../inc/bootstrap.php');

$list = App::getListManagement()->getList('noble_materials');
$list_deleted = App::getListManagement()->getDeletedList('noble_materials');
if ($list === false || $list_deleted === false)
    App::setFlashAndRedirect('danger', "Un problème est survenu, réessayez plus tard", "/");

require('../../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        Liste des matériaux nobles
    </div>
    <div class="panel-body">
        <div class="col-xs-12 text-center">
            <?php include("../../../inc/print_flash_helper.php"); ?>
            <form method="post" action="form" class="form-inline">
                <div class="form-group">
                    <input class="form-control" type="hidden" name="position" value="" placeholder="position">
                    <input class="form-control" type="text" name="field" value="" placeholder="Matériau noble">
                    <button class="btn btn-default" name="action" value="add">Ajouter</button>
                </div>
            </form>
        </div>
        <div class="col-sm-6" style="margin-top: 1em">
            <div class="table-responsive">
                <table id="table_List" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-xs-2"></th>
                        <th class="col-xs-8">Liste active</th>
                        <th class="col-xs-2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($list as $key): ?>
                        <tr>
                            <td><?= $key->position; ?></td>
                            <td><?= $key->field; ?></td>
                            <td class="text-center">
                                <form method="post" action="form">
                                    <input type="hidden" name="id" value="<?= $key->id; ?>">
                                    <button name="action" value="delete" class="btn glyphicon glyphicon-remove btn-delete-soft"></button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6" style="margin-top: 1em">
            <div class="table-responsive">
                <table id="table_DeletedList" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-xs-2"></th>
                        <th class="col-xs-8">Liste inactive</th>
                        <th class="col-xs-2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($list_deleted as $key): ?>
                        <tr>
                            <td><?= $key->position; ?></td>
                            <td><?= $key->field; ?></td>
                            <td class="text-center">
                                <form method="post" action="form">
                                    <input type="hidden" name="id" value="<?= $key->id; ?>">
                                    <button name="action" value="callback" class="btn glyphicon glyphicon-refresh btn-delete-soft"></button>
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>