<?php
require('../../../inc/bootstrap.php');

if (empty($_POST) || empty($_POST['action']))
    App::redirect("edit");

if ($_POST['action'] == 'add')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('field', 'Le nom de la catégorie est invalide', 'Le nom de la catégorie est manquante');
    $validator->is_num('position', 'La position doit être un nombre');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->addInput('rs_category', $_POST['field'], $_POST['position']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'la catégorie de service connexe a été ajoutée');
    else
        Session::getInstance()->setFlash('danger', 'la catégorie de service connexe n\'a pas été ajoutée');
}
else if ($_POST['action'] == 'delete')
{
    $validator = new Validator($_POST);
    $validator->is_num('id', 'La catégorie de service connexe n\'est pas valide', 'La catégorie de service connexe est manquante');
    if (!App::getListManagement()->inList('rs_category', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la liste des catégories des services connexes');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->delInput($_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'la catégorie de service connexe a été supprimée');
    else
        Session::getInstance()->setFlash('danger', 'la catégorie de service connexe n\'a pas été supprimée');
}
else if ($_POST['action'] == 'callback')
{
    $validator = new Validator($_POST);
    $validator->is_num('id', 'La catégorie de service connexe n\'est pas valide', 'La catégorie de service connexe est manquante');
    if (!App::getListManagement()->inDeletedList('rs_category', $_POST['id']))
        $validator->throwException('id', 'L\'élément ne fait pas parti de la liste des catégories des services connexes');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('edit');
    }
    $ret = App::getListManagement()->callbackInput('promocodeGroup', $_POST['id']);
    if ($ret)
        Session::getInstance()->setFlash('success', 'la catégorie de service connexe a été réinitialisée');
    else
        Session::getInstance()->setFlash('danger', 'la catégorie de service connexe n\'a pas été réinitialisée');
}
App::redirect("edit");
