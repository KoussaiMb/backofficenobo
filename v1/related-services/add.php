<?php
require('../../inc/bootstrap.php');

$rs_category_list = App::getListManagement()->getList('rs_category');
$data = Session::getInstance()->read('rs_add_temp');

require_once('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Création d'un nouveau service connexe
        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <div class="alert alert-info col-sm-12" role="alert">
                    <p><strong>Tous</strong> les champs sont requis</p>
                </div>
            </div>
            <div class="col-sm-6">
                <form method="post" action="form">
                    <div class="input-group">
                        <div class="input-group-addon"><span>Catégorie</span></div>
                        <select class="form-control input-sm" name="l_rs_category_id">
                            <?php foreach ($rs_category_list as $k => $rs_category): ?>
                                <option value="<?= $rs_category->id;?>" <?= isset($data['l_rs_category_id']) && $data['l_rs_category_id'] == $rs_category->id ? "selected" : ""; ?>><?= $rs_category->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control input-sm" name="name" value="<?=  isset($data['name']) ? $data['name'] : null; ?>" placeholder="Nom du service" required/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-globe"></span></div>
                        <input type="text" class="form-control input-sm" name="website" value="<?= isset($data['website']) ? $data['website'] : null; ?>" placeholder="https://nobo.life" required/>
                    </div>
                    <textarea class="form-control input-sm" name="description" maxlength="300" style="resize: none" placeholder="Description en quelques mots..." required><?=  isset($data['description']) ? $data['description'] : null; ?></textarea>
                    <button type="submit" class="btn btn-success col-sm-6 col-sm-offset-3 SpaceTop" name="action" value="add">Créer un nouveau service connexe</button>
                </form>
            </div>
            <div class="col-sm-3">
                <?php require('../../inc/print_flash_create.php'); ?>
            </div>
        </div>
    </div>
<?php
require('../../inc/footer_bo.php');