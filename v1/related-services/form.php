<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');

if ($_POST['action'] == 'add') {
    Session::getInstance()->write('rs_add_temp', $_POST);
    $validator = new Validator($_POST);

    $validator->is_id('l_rs_category_id', 'la catégorie est invalide', 'la catégorie est manquante');
    $validator->no_symbol('name', 'le nom est invalide', 'le nom est manquant');
    $validator->no_symbol('website', 'le website est invalide', 'le website est manquant');
    $validator->no_symbol('description', 'la description est invalide', 'la description est manquante');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'add');
    $ret = App::getRelatedServices()->addService($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le service n\'a pas été ajouté', 'add');
    Session::getInstance()->delete('rs_add_temp');
    App::setFlashAndRedirect('success', 'Le service a bien été ajouté', 'view');
}
else if ($_POST['action'] == 'edit') {
    $validator = new Validator($_POST);

    $validator->is_id('id', 'le service est invalide', 'le service est manquant');
    $validator->is_id('l_rs_category_id', 'la catégorie est invalide', 'la catégorie est manquante');
    $validator->no_symbol('name', 'le nom est invalide', 'le nom est manquant');
    $validator->no_symbol('website', 'le website est invalide', 'le website est manquant');
    $validator->no_symbol('description', 'la description est invalide', 'la description est manquante');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'edit?rs_id=' . $_POST['id']);
    $ret = App::getRelatedServices()->editService($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le service n\'a pas été édité', 'edit?rs_id=' . $_POST['id']);
    App::setFlashAndRedirect('success', 'Le service a bien été édité', 'view');
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);

    $validator->is_id('id', 'le service est invalide', 'le service est manquant');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'edit?rs_id=' . $_POST['id']);
    $ret = App::getRelatedServices()->deleteService($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le service n\'a pas été supprimé', 'edit?rs_id=' . $_POST['id']);
    App::setFlashAndRedirect('success', 'Le service a bien été supprimé', 'view');
}
App::redirect('view');