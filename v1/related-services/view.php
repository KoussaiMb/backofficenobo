<?php
require_once('../../inc/bootstrap.php');

$relatedServices = App::getRelatedServices()->getAllServices();
$rs_category_list = App::getListManagement()->getList('rs_category');

require_once('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Services connexes
    </div>
    <div class="panel-body SpaceTop">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php include('../../inc/print_flash_helper.php'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-xs-offset-0 col-sm-4 col-sm-offset-2">
                    <label for="l_rs_category_id">Categorie du service connexe</label>
                    <a href="/v1/list/rs_category/edit" target="_blank" class="pull-right">Gérer</a>
                    <select name="l_rs_category_id" class="form-control" id="l_rs_category_id">
                        <option value="0"></option>
                        <?php foreach ($rs_category_list as $k => $rs_category): ?>
                            <option value="<?= $rs_category->id; ?>"><?= $rs_category->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 text-left" style="padding-top: 22px">
                    <a href="add"><button type="button" class="btn btn-success" name="action" value="add">Ajouter un service connexe</button></a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 view-container">
                    <?php foreach ($relatedServices as $k => $service): ?>
                    <a href="edit?rs_id=<?= $service->id; ?>">
                        <div class="col-xs-4 rs-view">
                            <div class="col-xs-12">
                                <div class="col-xs-12">
                                    <p style="color: #204d74;"><?= $service->name; ?></p>
                                </div>
                                <div class="col-xs-6">
                                    <p>
                                        <span class="glyphicon glyphicon-eye-open"> <?= $service->viewed_nb; ?></span>
                                    </p>
                                </div>
                                <div class="col-xs-6">
                                    <p>
                                        <input type="hidden" name="l_rs_category_id" value="<?= $service->l_rs_category_id; ?>">
                                        <?= $service->rs_category; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .view-container {
        overflow-y: scroll;
        margin-top: 2em;
        border: 2px solid #DBEED4;
        border-radius: 3px;
    }
    .rs-view {
        padding: 20px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .rs-view > div:hover {
        border: 1px solid grey;

    }
    .rs-view > div {
        background-color: #c2c2d6;
        border-radius: 3px;
        padding-top: 10px;
        height: 80px;
        cursor: pointer;
    }
    .rs-view > div div:first-child {
        color: ;
    }
    .rs-view > div div:last-child {
        color: #e60073;
    }
</style>
<script>
    jQuery(document).ready(function() {
        let related_services = $('.rs-view');
        let l_rs_category_id = $('#l_rs_category_id');

        $(".view-container").css('max-height', $(window).height() * 0.7);

        l_rs_category_id.on('change', function () {
            let rs_cat_id = $(this).val();

            if (rs_cat_id === '0')
                related_services.show();
            else {
                related_services.hide();
                $("input[name='l_rs_category_id'][value='"+ rs_cat_id +"']").parentsUntil("div[class='rs-view']").show();
            }
        })
    });
</script>

<?php
require('../../inc/footer_bo.php');