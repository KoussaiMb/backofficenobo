<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $validator = new Validator($_GET);

    $validator->isToken('user_token', 'Utilisateur invalide', 'Utilisateur non rengeigné');
    $validator->is_id('company', 'Entreprise invalide', 'Entreprise non rensseignée');
    $validator->is_num('year', 'Année invalide', 'Année non renseignée');

    if(!isset($_GET['action']) || empty($_GET['action'])){
        $json = parseJson::error('Impossible de récupérer l\'action souhaitée');
        $json->printJson();
    }

    if(!$validator->is_valid()){
        $json = parseJson::error('Une des données reçues n\'est pas valide');
        $json->printJson();
    }

    $user_token = $_GET['user_token'];
    $companyId = $_GET['company'];
    $year = $_GET['year'];
    $type = 'Mission';
    $autoSalary = App::getConf()->getConfByName('autoentrSalary')->value;

    $company = App::getCompany()->getCompanyById($companyId);
    if(!$company){
        $json = parseJson::error('Impossible de récupérer l\'entreprise');
        $json->printJson();
    }

    $conf = App::getConf()->getAll();
    if(!$conf){
        $json = parseJson::error('Impossible de récupérer les configurations');
        $json->printJson();
    }

    $userClass = App::getUser();
    $missionClass = App::getPrestation();

    $user = $userClass->getUserByToken($user_token);
    if(!$user){
        $json = parseJson::error('Impossible de récupérer l\'utilisateur');
        $json->printJson();
    }

    $address = $userClass->getAddressHomeByUserToken($user_token);
    if(!$address){
        $json = parseJson::error('Impossible de récupérer l\'adresse');
        $json->printJson();
    }

    /*$missions = $missionClass->getAllMissionInfoByUserTokenAndYear($user_token, $year, $type);
    if(!$missions){
        $json = parseJson::error('Impossible de récupérer les missions');
        $json->printJson();
    }*/

    $carts = $missionClass->getCartsByUserTokenTypeAndYear($user_token, $type, $year);

    $jsonData['companyName'] = $company->corporate_name;
    $jsonData['companyRealName'] = $company->business_name;

    $jsonData['companyAddress'] = $company->address;
    $jsonData['companyZipCity'] = $company->zipcode . ', ' . $company->city;

    $jsonData['companySiret'] = $company->siret;
    $jsonData['companySap'] = $company->sap;

    $jsonData['companyChief'] = $company->ceo_firstname . ' ' . $company->ceo_lastname;
    $jsonData['pathToChiefSignature'] = str_replace('\/', '/', $company->ceo_signature);

    $jsonData['userFirstname'] = $user->firstname;
    $jsonData['userLastname'] = $user->lastname;
    $jsonData['userAddress'] = $address->address;
    $jsonData['userZipCity'] = $address->zipcode . ', ' . $address->city;

    $jsonData['autoentrSalary'] = $autoSalary;

    $jsonData['year'] = $year;

    $it_carts = sizeof($carts);
    for($i = 0; $i < $it_carts; $i++){
        $jsonData['firstname' . $i] = $carts[$i]->firstname;
        $jsonData['lastname' . $i] = $carts[$i]->lastname;
        $jsonData['payroll_identity' . $i] = $carts[$i]->payroll_identity;
        $jsonData['price_final' . $i] = $carts[$i]->price_final;
        $jsonData['date' . $i] = $carts[$i]->start;
        $jsonData['quantity' . $i] = $carts[$i]->quantity;
        $jsonData['unit_price' . $i] = $carts[$i]->price;
        $jsonData['vat' . $i] = $carts[$i]->vat;
        $jsonData['auto' . $i] = 0;
        //Cas autoentrepreneur
        if(substr($carts[$i]->reference_name, 0, 4) === "inde"){
            $jsonData['auto' . $i] = 1;
        }
    }
    $jsonData['cartsCount'] = $it_carts;

    $jsonData['action'] = $_GET['action'];

    $json = parseJson::success(null, $jsonData);
    $json->printJson();

}