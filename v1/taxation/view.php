<?php
require('../../inc/bootstrap.php');

if(!isset($_POST) || empty($_POST)){
    ?>

    <h2>Le crédit d'impôt a expiré</h2>
    <a href="/v1/client/view"><button id="back_btn" class="btn btn-danger" type="submit">Retour</button></a>


    <?php
}else {

    $action = $_POST['action'];
    $company_info = constructCompanyInfo($_POST);
    $client = constructClient($_POST);
    $carts_info = constructRows($_POST);

    $taxGenerator = new TaxCreditGenerator($company_info, $client, $carts_info, $action);
}


function constructCompanyInfo($data)
{
    $company_info = array();

    $company_info['siret'] = "N° SIRET : " . $data['companySiret'];
    $company_info['sap'] = "N° SAP : " . $data['companySap'];
    $company_info['name'] = $data['companyName'];
    $company_info['address'] = $data['companyAddress'];
    $company_info['zipcity'] = $data['companyZipCity'];
    $company_info['realName'] = $data['companyRealName'];
    $company_info['chief'] = $data['companyChief'];
    $company_info['autoentrSalary'] = $data['autoentrSalary'];
    try {
        $signature_path = $_SERVER['DOCUMENT_ROOT'] . $data['pathToChiefSignature'];
        if (!file_exists($signature_path)) {
            throw new Exception('Le logo de l\'entreprise n\'existe pas, vérifiez son url');
        }
        if(!is_readable($signature_path)){
            throw new Exception('Permission de récupérer le logo de l\'entreprise non accordée');
        }
        $company_info['chiefSignature'] = $signature_path;
    } catch(Exception $e) {
        App::setFlashAndRedirect('danger', $e->getMessage(), 'view');
    }
    return $company_info;
}

function constructClient($data)
{
    $client = array();

    $client['firstname'] = $data['userFirstname'];
    $client['lastname'] = $data['userLastname'];
    $client['address'] = $data['userAddress'];
    $client['zipcity'] = $data['userZipCity'];
    $client['year'] = $data['year'];

    return $client;
}

function constructRows($data)
{

    $count = $data['cartsCount'];
    $rows = array();

    for($i = 0; $i < $count; $i++){
        $rows[] = constructRow($data, $i);
    }
    return $rows;
}

function constructRow($data, $index)
{
    $row = array();

    $row['firstname'] = $data['firstname'.$index];
    $row['lastname'] = $data['lastname'.$index];
    $row['payroll_identity'] = $data['payroll_identity'.$index];
    $row['price_final'] = $data['price_final'.$index];
    $row['quantity'] = $data['quantity'.$index];
    $row['date'] = $data['date'.$index];
    //$row['vat'] = $data['vat'.$index];
    $row['unit_price'] = $data['unit_price'.$index];
    $row['auto'] = $data['auto'.$index];

    return $row;
}
