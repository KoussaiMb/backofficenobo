<?php
require('../../../inc/bootstrap.php');


if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $validator = new Validator($_GET);

    $redirect_url = 'view?client_token=' . $_GET['user_token'];

    $validator->isToken('user_token', 'Token invalide', 'Token vide');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('user_token'));
        $json->printJson();
    }
    $user_token = $_GET['user_token'];


    $validator->isToken('address_token', 'L\'adresse est invalide', 'L\'adresse est vide');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('address_id'));
        $json->printJson();
    }
    $address_token = $_GET['address_token'];

    $missionClass = App::getPrestation();

    /* Call for user information */
    $userInfo = $missionClass->getUserInfoByToken($user_token);
    if(!$userInfo){
        $json = parseJson::error('Impossible de récupérer les informations du client');
        $json->printJson();
    }
    $jsonData['userFirstname'] = $userInfo->firstname;
    $jsonData['userLastname'] = $userInfo->lastname;

    $addressInfo = App::getAddress()->getAddressByToken($address_token);
    if(!$addressInfo){
        $json = parseJson::error('Impossible de récupérer les informations de l\'adresse');
        $json->printJson();
    }
    $jsonData['userAddress'] = $addressInfo->address;
    $jsonData['userZipCity'] = $addressInfo->zipcode . ', ' . $addressInfo->city;

    /* Information used to compute the invoice name from the user subscription date */
    $date_subscribed = $userInfo->sub_date;
    $sub_customer = strtotime($date_subscribed);
    $sub_customer_date = date('dmy', $sub_customer);
    $sub_month = date('m', $sub_customer);
    $sub_year = date('Y', $sub_customer);

    $validator->is_id('company', 'Entreprise invalide', 'Entreprise vide');
    if (!$validator->is_valid()) {
        $json =  parseJson::error($validator->getErrorString('company'));
        $json->printJson();
    }
    $company_id = $_GET['company'];

    /* Call for selected company */
    $company = App::getCompany()->getCompanyById($company_id);

    $jsonData['companyName'] = $company->corporate_name;
    $jsonData['companyAddress'] = $company->address;
    $jsonData['companyZipCity'] = $company->zipcode . ', ' . $company->city;
    $jsonData['companySiret'] = $company->siret;
    $jsonData['companyLogo'] = $company->logo;

    /* Download or display the invoice */
    if (!isset($_GET['action']) || empty($_GET['action'])) {
        $json =  parseJson::error('Aucune action définie pour le fichier');
        $json->printJson();
    }
    $action = $_GET['action'];
    $jsonData['action'] = $action;

    /* Last letter of invoice name */
    $bill_period_type = '';

    /* Array to gather carts */
    $cartInfo = array();

    if (!isset($_GET['request_type']) || empty($_GET['request_type'])) {
        $json =  parseJson::error('Aucun type de requete renseigné');
        $json->printJson();
    }

    $autoSalary = App::getConf()->getConfByName('autoentrSalary')->value;
    if(!$autoSalary){
        $json =  parseJson::error('Impossible de récupérer le salaire des autoentrepreneurs');
        $json->printJson();
    }
    $jsonData['autoSalary'] = $autoSalary;

    /* Request from a selection */
    if($_GET['request_type'] == 'CART_REQUEST'){

        if (!isset($_GET['cart_ids']) || empty($_GET['cart_ids'])) {
            $json =  parseJson::error('Sélection non valide');
            $json->printJson();
        }

        $c_ids = $_GET['cart_ids'];

        $ids_size = sizeof($c_ids);
        for($i = 0; $i < $ids_size; $i++){
           $c_id = $c_ids[$i];
           $cartInfo[] = $missionClass->getCartById($c_id);
        }

        $bill_period_type = 'UNIQ';
        $elapsed_time = '';

        /* If we only generate one invoice by selection, we get the cart_count of the requested cart */
        if($ids_size === 1){
            $cart_uniq_count = $cartInfo[0]->cart_count;
            $elapsed_time = $cart_uniq_count;
            $bill_period_type = 'C';
        }

        $type = "Tous";

    /* Request from the date filter */
    }else if ($_GET['request_type'] == 'FILTER_REQUEST'){

        $validator->is_id('item_type', 'Type d\'item invalide', 'Type d\'item vide');
        if (!$validator->is_valid()) {
            $json =  parseJson::error($validator->getErrorString('item_type'));
            $json->printJson();
        }
        $item_type = $_GET['item_type'];

        $validator->is_num('month', 'Mois invalide', 'Mois vide', 2);
        if (!$validator->is_valid()) {
            $json =  parseJson::error($validator->getErrorString('month'));
            $json->printJson();
        }

        $validator->is_num('year', 'Année invalide', 'Année vide', 4);
        if (!$validator->is_valid()) {
            $json =  parseJson::error($validator->getErrorString('year'));
            $json->printJson();
        }

        if($item_type == 0){
            $type = "Tous";
        }else{
            $type = App::getItem()->getCategoryById($item_type)->name;
            if(!$type){
                $json =  parseJson::error('Impossible de récupérer le type d\'item');
                $json->printJson();
            }
        }


        /* Month and year of the request */
        $request_month = $_GET['month'];
        $request_year = $_GET['year'];

        if($type === "Tous"){
            $cartInfo = $missionClass->getCartByAddressTokenAndDate($address_token, $request_month, $request_year);
        }else{
            $cartInfo = $missionClass->getCartByAddressTokenAndDateAndItemType($address_token, $request_month, $request_year, $type);
        }


        /* Adding the items not in the requested month for customer invoice */
        if(isset($_GET['cart_ids']) && !empty($_GET['cart_ids'])) {
            $c_ids = $_GET['cart_ids'];

            $ids_size = sizeof($c_ids);
            for ($i = 0; $i < $ids_size; $i++) {
                $c_id = $c_ids[$i];
                if(!in_array($missionClass->getCartById($c_id), $cartInfo)) {
                    $cartInfo[] = $missionClass->getCartById($c_id);
                }
            }
        }
        /* Type for invoice name */
        $bill_period_type = 'M';
        $elapsed_time = ($request_month - $sub_month) + 1 + (($request_year - $sub_year)*12);
    }

    /* Name of invoice : FA# + bill type letter + last 3 digits of user token +  date of customer subscription + bill date letter + time since subscription */
    $bill_name = "FA#" . strtoupper(substr($user_token, sizeof($user_token) - 4, 3)) . $sub_customer_date . $bill_period_type . $elapsed_time;
    $jsonData['bill_name'] = $bill_name;

    $mission_nb = sizeof($cartInfo);
    $jsonData['mission_nb'] = $mission_nb;

    /* Date for getting the first item to complete invoice name */
    $date_min = date('YYYY-mm-dd');

    /* Creating a field for each mission and additional tasks */
    for ($i = 0; $i < $mission_nb; $i++) {

        $ci = $cartInfo[$i];

        $jsonData['datePrestation' . $i] = $ci->date_create;

        if($ci->date_create < $date_min){
            $date_min = $ci->date_create;
        }

        $jsonData['unitPricePrestation' . $i] = $ci->price;
        $jsonData['totalTtcPrice' . $i] = $ci->price_final;

        $time = $ci->quantity;

        $jsonData['time' . $i] = $time;
        $jsonData['description' . $i] = $ci->name;

        $discount = "";
        $discountPromo = "";
        $jsonData['discountType' . $i] = "";
        $jsonData['promoType' . $i] = "";

        $promocode = null;
        if($ci->promocode_id){
            $promocode = App::getPromoCode()->getPromoCodeById($ci->promocode_id);
        }

        /* Getting the discount type && the value (Can't be both) */
        if (isset($ci->euro) && !empty($ci->euro)) {
            $discount = $ci->discount_euro;
            $jsonData['discountType' . $i] = "euro";
        } else if (isset($ci->percentage) && !empty($ci->percentage)) {
            $discount = $ci->discount_percentage;
            $jsonData['discountType' . $i] = "percentage";
        }
        $jsonData['discount' . $i] = $discount;

        if (isset($promocode->euro) && !empty($promocode->euro)) {
            $discountPromo = $promocode->euro;
            $jsonData['promoType' . $i] = "euro";
        } else if (isset($promocode->percentage) && !empty($promocode->percentage)){
            $discountPromo = $promocode->percentage;
            $jsonData['promoType' . $i] = "percentage";
        }
        $jsonData['promo' . $i] = $discountPromo;
        $jsonData['tva' . $i] = $ci->vat;

        $jsonData['auto' . $i] = 0;
        if($ci->category_name == 'Mission') {
            if(substr($ci->reference_name, 0, 4) === "inde"){
                $jsonData['auto' . $i] = 1;
            }
        }
    }
    $json =  parseJson::success(null,$jsonData);
    $json->printJson();
}
