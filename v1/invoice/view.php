<?php
require('../../inc/bootstrap.php');

$customers = App::getCustomer();

$autocompleteClients = $customers->getAutocompleteName();
$checkedCustomers = $customers->getAll('checked');
if ($autocompleteClients === null || $checkedCustomers === null) {
    Session::getInstance()->setFlash('danger', 'Impossible de récupérer les informations serveur');
}

require('../../inc/header_bo.php');
?>

    <div id="hidden_form_container" style="display:none;"></div>

    <div class="panel panel-success">
        <div class="panel-heading">
            Gestion des factures
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-sm-12">
                <?php if(!isset($_GET['client_token']) || empty($_GET['client_token'])){ ?>
                    <form method="GET" action="">
                        <div class="col-sm-4">
                            <label for="customer_name">Entrer un client pour générer sa page de facture</label>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nom, prénom" id="customer_name">
                                <input type="hidden" name="client_token" id="client_search">
                                <button type="submit" class="btn btn-success form-control" id="customer_name_btn">
                                    <span class="glyphicon glyphicon-ok"></span> Valider
                                </button>
                            </div>
                            <div class="form-group">
                            </div>
                        </div>
                    </form>
                <?php } else {

                $user_token = $_GET['client_token'];
                $mission_list = App::getPrestation()->getAllCartsInfoByUserToken($user_token);

                ?>

                <input type="hidden" name="client_token" id="client_token" value="<?= $user_token ?>">
                <form action="form" method="POST">
                <div class="col-sm-1">
                    <label for="datepicker1"> Mois </label>
                    <div class="form-group">
                        <input type="text" name="datepicker1" id="datepicker1" class="form-control datepicker" readonly="readonly"/>
                    </div>
                </div>
                <div class="col-sm-1">
                    <?php
                    $item_type_list = App::getItem()->getCategories();
                    ?>
                    <label for="item_type_select">Type</label>
                    <div class="form-group">
                        <select class="form-control" name="item_type_select" id="item_type_select" >
                            <option data-select="Tous" value="0">Tous</option>
                            <?php foreach($item_type_list as $itl){?>
                                <option data-select="<?= $itl->name ?>" value="<?= $itl->id ?>"><?= $itl->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <?php
                    $user_addresses = App::getUser()->getAddressesByUserToken($_GET['client_token']);
                    ?>
                    <label for="company_ref">Adresse</label>
                    <div class="form-group">
                        <select class="form-control" id="customer_address" name="select_address">
                            <?php
                            foreach ($user_addresses as $address){
                                ?>
                                <option value="<?= $address->address_token ?>">
                                    <?= $address->address .', ' . $address->zipcode .', '. $address->city ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <?php
                    $companies = App::getCompany()->getAll();
                    ?>
                    <label for="company_ref">Entreprise</label>
                    <div class="form-group">
                        <select class="form-control" id="company_ref" name="select_company">
                            <?php
                            foreach ($companies as $c){
                                ?>
                                <option value="<?= $c->id ?>">
                                    <?= $c->corporate_name .' - ' . $c->business_name ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-1">
                    <label for="reinit_filters">Filtres</label>
                    <div class="form-group">
                        <button type="button" class="btn btn-success form-control" id="reinit_filters">
                            <span class="glyphicon glyphicon-refresh"></span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="display_bill">Facture du mois</label>
                    <div class="form-group">
                        <button type="button" class="btn btn-default form-control" id="display_bill" disabled>
                            Afficher
                        </button>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default form-control" id="download_bill" disabled>
                            Télécharger
                        </button>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="bill_cart">Depuis sélection</label>
                    <div class="form-group">
                        <button type="button" id="bill_cart_display" class="btn btn-primary form-control" disabled>
                            <span class="glyphicon glyphicon-shopping-cart"></span>  Afficher
                        </button>
                    </div>
                    <div class="form-group">
                        <button type="button" id="bill_cart_download" class="btn btn-primary form-control" disabled>
                            <span class="glyphicon glyphicon-shopping-cart"></span>  Télécharger
                        </button>
                    </div>
                </div>
                <div class="col-sm-1">
                    <label for="reinit_client">Client</label>
                    <div class="form-group">
                        <button type="button" class="btn btn-success form-control" id="reinit_client" onclick="return confirm('Voulez-vous vraiment changer de client ?');">
                            <span class="glyphicon glyphicon-user"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div>
                <div class="scroll-table">
                    <table id="table_missions" class="table table-hover view-container">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Client</th>
                            <th>Adresse</th>
                            <th>Type</th>
                            <th>Designation</th>
                            <th><input id="checkbox_all" type="checkbox"/> All</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $it = sizeof($mission_list);
                        ?>

                        <input type="hidden" id="mission_nb" name="mission_nb" value="<?= $it ?>">

                        <?php for($i = 0; $i < $it; $i++){
                            $m = $mission_list[$i];
                            ?>

                            <input type="hidden" id="date<?= $i ?>" value="<?= date('d-m-Y', strtotime($m->date_create)); ?>">
                            <input type="hidden" id="address<?= $i ?>" value="<?= $m->address .', '. $m->zipcode .', '.$m->city; ?>">
                            <input type="hidden" id="item_type<?= $i ?>" value="<?= $m->category_name ?>">
                            <input type="hidden" id="designation<?= $i ?>" value="<?= $m->name ?>">

                            <tr id="<?= $i ?>">
                                <td><?= date('d-m-Y', strtotime($m->date_create)); ?></td>
                                <td><?= $m->firstname . ' ' . $m->lastname ?></td>
                                <td><?= $m->address .', '. $m->zipcode .', '.$m->city; ?></td>
                                <td><?= $m->category_name ?></td>
                                <td><?= $m->name ?></td>
                                <td>
                                    <label><input class="checkbox_invoice" type="checkbox" name="cart_checkbox" value="<?=$m->id?>"></label>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <style>
        .highlight{
            background-color: aliceblue;
        }

        .scroll-table{
            table-layout: fixed;
            width:100%;
            overflow-y: auto;
            max-height: 500px;
        }
    </style>

    <script src="../../js/invoice.js"></script>
    <script>
        /* Autocomplete for the customer name before displaying invoices */
        jQuery(document).ready(function(){
            $('#customer_name').devbridgeAutocomplete({
                lookup: <?php if (!empty($autocompleteClients)){echo $autocompleteClients;} else {echo "[]";}?>,
                maxHeight: 200,
                onSelect: function (suggestion) {
                    $('#client_search').val(suggestion.data);
                }
            });
        });
    </script>
<?php
require('../../inc/footer_bo.php');