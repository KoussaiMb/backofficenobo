<?php
require('../../inc/bootstrap.php');

if(!isset($_POST) || empty($_POST)){
    ?>
    <h2>La facture a expiré</h2>
    <a href="view"><button id="back_btn" class="btn btn-primary" type="submit">Retour</button></a>
<?php
}else {

    $client = constructClient($_POST);
    $company_info = constructCompanyInfo($_POST);
    $invoice_info = constructInvoiceInfo($_POST);
    $missions_info = constructRows($_POST);
    $action = $_POST['action'];

    $invoiceGenerator = new InvoiceGenerator($client, $company_info, $invoice_info, $missions_info, $action);
}
function constructClient($data)
{
    $client = array();

    $client['firstname'] = $data['userFirstname'];
    $client['lastname'] = $data['userLastname'];
    $client['address'] = $data['userAddress'];
    $client['zipcity'] = $data['userZipCity'];

    return $client;
}

function constructCompanyInfo($data)
{
    $company_info = array();

    $company_info['siret'] = "N° SIRET : " . $data['companySiret'];
    $company_info['name'] = $data['companyName'];
    $company_info['address'] = $data['companyAddress'];
    $company_info['zipcity'] = $data['companyZipCity'];
    $company_info['autoSalary'] = $data['autoSalary'];
    try {
        $logo_path = $_SERVER['DOCUMENT_ROOT'] . $data['companyLogo'];
        if (!file_exists($logo_path)) {
            throw new Exception('Le logo de l\'entreprise n\'existe pas, vérifiez son url');
        }
        if(!is_readable($logo_path)){
            throw new Exception('Permission de récupérer le logo de l\'entreprise non accordée');
        }
        $company_info['companyLogo'] = $logo_path;
    } catch(Exception $e) {
        App::setFlashAndRedirect('danger', $e->getMessage(), 'view');
    }
    return $company_info;
}

function constructInvoiceInfo($data)
{
    $invoice_info = array();

    $invoice_info['num'] = $data['bill_name'];
    $invoice_info['date'] = date('d-m-Y');

    return $invoice_info;
}

function constructRows($data)
{
    $missions_info = array();
    $missions_info['mission_nb'] = 0;
    if(isset($data['mission_nb']) && !empty($data['mission_nb']) && $data['mission_nb'] > 0) {
        $count = $data['mission_nb'];
        $missions_info['mission_nb'] = $count;
        for ($i = 0; $i < $count; $i++) {
            $missions_info[] = constructRow($data, $i);
        }
    }
    return $missions_info;
}

function constructRow($data, $index)
{
    $row = array();

    $time = $data['time' . $index];

    $row['datePrestation'] = $data['datePrestation'. $index];
    $row['unitPricePrestation'] = $data['unitPricePrestation'. $index];
    $row['quantity'] = $time;
    $row['description'] = $data['description'. $index];
    $row['tva'] = $data['tva'. $index];
    $row['totalTtcPrice'] = $data['totalTtcPrice'. $index];
    $row['discount'] = $data['discount'. $index];
    $row['discountType'] = $data['discountType'. $index];
    $row['promoType'] = $data['promoType'. $index];
    $row['promo'] = $data['promo'. $index];
    $row['auto'] = $data['auto' . $index];

    return $row;
}