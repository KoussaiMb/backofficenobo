<?php
/**
 * file: task.php
 * auhtor: Jonathan BRICE
 * date: 18-04-2018
 * description:
 **/

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);
    $validator->is_id("cardex_id", "Le cardex n'est pas valide", "Le cardex est manquant");
    $validator->is_id("l_task_id", "La tâche est invalide", "La tâche est manquante");
    $validator->is_id("l_ponderation_id", "La pondération est invalide", "La pondération est manquante");
    $validator->is_id("lr_room_id", "Le lieu est invalide", "La lieu est manquant");
    $validator->is_color_hex("color_hex", "La couleur est invalide", "La couleur est manquante");
    if (!$validator->is_valid())
        parseJson::error('Les données pour ajouter la tâche son erronnées', $validator->getErrors())->printJson();
    $taskAdded = App::getCardex()->addCardexTask($_POST);
    if ($taskAdded === false)
        parseJson::error('Impossible de rajouter la tâche au cardex')->printJson();
    parseJson::success('La tâche a bien été rajoutée au cardex', ['task_id' => $taskAdded])->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);
    $validator->is_id("cardex_id", "Le cardex n'est pas valide", "Le cardex est manquant");
    if (!$validator->is_valid())
        parseJson::error('Les données pour récupérer les tâches du cardex sont invalides', $validator->getErrors())->printJson();
    $cardex_tasks = App::getCardex()->getCardexTaskByCardexId($_GET['cardex_id']);
    if ($cardex_tasks === false)
        parseJson::error('Impossible de récupérer les tâches du cardex')->printJson();
    parseJson::success('Les tâches du cardex a bien été récupéré', $cardex_tasks)->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);
    $validator->is_id("cardex_task_id", "Le cardex n'est pas valide", "Le cardex est manquant");
    if (!$validator->is_valid())
        parseJson::error('Les données pour supprimer la tâche sont invalides', $validator->getErrors())->printJson();
    $ret = App::getCardex()->deleteCardexTask($_DELETE['cardex_task_id']);
    if ($ret === false)
        parseJson::error('La tâche n\'a pas été supprimée du cardex')->printJson();
    parseJson::success('La tâche a été supprimée du cardex')->printJson();
}