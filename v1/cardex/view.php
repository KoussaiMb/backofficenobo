<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 19/07/2018
 * Time: 16:17
 */
require('../../inc/bootstrap.php');
$cardexes = App::getCardex()->getAll();
$maintenanceTypes = App::getCardex()->getMaintenanceTypeNotUsed();
$maintenanceType_nb = count($maintenanceTypes);
require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Liste des cardex
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-12">
                <p><a href="/v1/list/maintenance-type/edit" target="_blank">Créer un type d'entretien</a></p>
            </div>
            <div class="col-xs-12 noPadding">
                <?php foreach ($cardexes as $k => $cardex): ?>
                <div class="col-xs-3">
                    <a href="edit?cardex_id=<?= $cardex->id; ?>" class="no-decoration">
                        <div class="cardex-box">
                            <span class="task_nb"><?= $cardex->task_nb; ?></span>
                            <p>Type d'entretien</p>
                            <h4><?= $cardex->maintenance_type; ?></h4>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
                <div class="col-xs-3">
                    <a href="add">
                        <button type="button" class="btn add-cardex" <?= $maintenanceType_nb == 0 ? 'disabled' : ''; ?>>
                            <span class="glyphicon glyphicon-plus"></span>
                            <p class="cardex-detail">Il reste <?= $maintenanceType_nb; ?> cardex à créer</p>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <style>
        .italic {
            font-style: italic;
        }
        .no-decoration {
            text-decoration: none!important;
        }
        .cardex-box p {
            margin-top: 10px!important;
            margin-bottom: 0;
            color: #888888;
        }
        .cardex-box h4, .cardex-box p {
            margin-top: 0;
        }
        .cardex-box {
            height: 150px;
            cursor: pointer;
            border: 1px solid rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            padding: 15px;
            text-align: center;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-content: stretch;
            -ms-flex-line-pack: stretch;
            align-content: stretch;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
        }
        .add-cardex:hover, .cardex-box:hover {
            color: inherit;
            border: 1px solid rgba(0, 0, 0, 0.3);
        }
        .add-cardex {
            height: 150px;
            width: 100%;
            box-shadow: none;
            outline: 0;
            -webkit-box-shadow: none;
            background-image: none;
        }
        .add-cardex > span{
            font-size: 3em;
        }
        .task_nb {
            position: absolute;
            right: 25px;
            top: 5px;
            color: indianred;
        }
        .cardex-detail {
            font-size: 10px!important;
            padding-top: 20px!important;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
        });
    </script>
<?php require('../../inc/footer_bo.php');