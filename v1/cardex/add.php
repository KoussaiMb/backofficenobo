<?php
require('../../inc/bootstrap.php');
$maintenanceTypes = App::getCardex()->getMaintenanceTypeNotUsed();
if ($maintenanceTypes === false)
    App::setFlashAndRedirect('danger', 'Impossible de récupérer les types d\'entretien', 'view');
if (empty($maintenanceTypes))
    App::setFlashAndRedirect('warning', 'Aucun type d\'entretien est disponible, créez en...', 'view');
require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        <a href="/v1/cardex/view">Liste des cardex</a> / Création d'un nouveau cardex
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="fluid-container">
            <div class="col-xs-3">
                <div class="alert alert-warning col-sm-12">
                    <p>NOM DU CARDEX</p>
                    <p>80 caractères maximum</p>
                </div>
            </div>
            <div class="col-xs-6">
                <form action="form" method="post">
                    <div class="form-group">
                        <label for="l_maintenanceType_id">Type d'entretien</label>
                        <select required class="form-control" name="l_maintenanceType_id" id="l_maintenanceType_id">
                            <option>Selectionner</option>
                            <?php foreach ($maintenanceTypes as $maintenanceType): ?>
                                <option value="<?= $maintenanceType->id ?>"><?= $maintenanceType->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <a href="view"><button class="btn btn-danger">Annuler</button></a>
                        <button class="btn btn-success" name="action" value="add">Créer le cardex</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php require('../../inc/footer_bo.php');