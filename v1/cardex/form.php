<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 06/07/2018
 * Time: 15:21
 */
include_once('../../inc/bootstrap.php');

if (empty($_POST['action'])){
    App::redirect('view');
} elseif ($_POST['action'] === 'add') {
    $validator = new Validator($_POST);

    $validator->is_id('l_maintenanceType_id', 'Le type de maintenance est invalide', 'Le type de maintenance est requis');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'add');

    $cardex_id = App::getCardex()->addCardex($_POST['l_maintenanceType_id']);

    if ($cardex_id === false)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout du cardex', 'add');
    App::setFlashAndRedirect('success', 'Veuillez maintenant éditer le cardex', 'edit?cardex_id=' . $cardex_id);
} elseif ($_POST['action'] === 'delete') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('cardex_id', 'Le nom du cardex est invalide', 'Le nom du cardex est manquant');
    if ($validator->is_valid() === false)
        App::array_to_flash($validator->getErrors(), 'view');

    $ret = App::getCardex()->deleteCardexById($_DELETE['cardex_id']);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le cardex n\'a pas été supprimé', 'edit?cardex_id=' . $_DELETE['cardex_id']);
    App::setFlashAndRedirect('success', 'Le cardex a été supprimé', 'view');
}
App::redirect("view");