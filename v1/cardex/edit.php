<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 02/08/2018
 * Time: 10:41
 */
require('../../inc/bootstrap.php');

if (empty($_GET['cardex_id']))
    App::setFlashAndRedirect('warning', 'Veuillez choisir un cardex à éditer', 'view');
$cardex = App::getCardex()->getCardexById($_GET['cardex_id']);
if ($cardex === false)
    App::setFlashAndRedirect('danger', 'Impossible de récupérer le cardex', 'view');
$cardex_tasks = App::getCardex()->getCardexTaskByCardexId($_GET['cardex_id']);
if ($cardex_tasks === false)
    App::setFlashAndRedirect('danger', 'Impossible de récupérer les tâches du cardex', 'view');
$maintenanceTypes = App::getListManagement()->getList("maintenanceType");
$ponderation = App::getListManagement()->getList("ponderation");
$rooms = App::getListManagement()->getListReference("roomsList");
$tasks = App::getListManagement()->getList("task");

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        <a href="view">Liste des cardex</a> / <?= $cardex->maintenance_type; ?>
        <form action="form" method="post">
            <input type="hidden" name="cardex_id" value="<?= $cardex->id; ?>" id="cardex_id">
            <button class="btn btn-danger panel-heading-button-right btn-xs" onclick="return confirm('Voulez-vous vraiment supprimer le cardex ?')" name="action" value="delete">
                <span class="glyphicon glyphicon-trash"></span>
            </button>
        </form>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="color_hex">Couleur</label>
                        <select class="form-control select-full" name="color_hex" id="color_hex">
                            <option value="#0f0f0f" style="color:#0f0f0f">Noir</i></option>
                            <option value="#FFFFFF" style="color:#FFFFFF">Blanc</option>
                            <option value="#ff4d4d" style="color:#ff4d4d">Rouge</option>
                            <option value="#3399ff" style="color:#3399ff">Bleu</option>
                            <option value="#33cc33" style="color:#33cc33">Vert</option>
                            <option value="#ff9966" style="color:#ff9966">Orange</option>
                            <option value="#ff3399" style="color:#ff3399">Violet</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="lr_room_id">Lieu</label>
                        <select class="form-control select-full" name="lr_room_id" id="lr_room_id">
                            <?php foreach ($rooms as $key => $value): ?>
                                <option value="<?= $value->id ?>"><?= $value->value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="l_task_id">Tâche</label>
                        <select data-live-search="true" data-live-search-style="startsWith" class="selectpicker form-control col-xs-3 select-full" name="l_task_id" id="l_task_id">
                            <?php foreach ($tasks as $key => $value): ?>
                                <option value="<?= $value->id ?>"><?= $value->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <a class="btn-link-v2 pull-left" href="/v1/task/view" target="_blank">Liste des tâches</a>
                    </div>
                    <div class="col-xs-12 hidden">
                        <label for="l_ponderation_id">ponderation</label>
                        <select class="form-control col-xs-3 select-full" name="l_ponderation_id" id="l_ponderation_id">
                            <?php foreach ($ponderation as $key => $value): ?>
                                <option value="<?= $value->id ?>"><?= $value->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1 container-btn-cardex">
                    <button class="btn btn-success" id="addTask"><span class="glyphicon glyphicon-plus"></span></button>
                </div>
                <!--
                <div class="col-xs-12 col-sm-4 cardex-info">
                      <div class="">
                        <label for="l_maintenanceType_id">Type d'entretien</label>
                        <select class="form-control" id="l_maintenanceType_id" disabled>
                            <option><?= $cardex->maintenance_type; ?></option>
                        </select>
                    </div>
                </div>
                -->
                <div class="col-xs-8" id="cardex-container">
                    <button type="button" class="btn btn-warning" id="open-all-cardex"><span class="glyphicon glyphicon-eye-open"></span></button>
                    <button type="button" class="btn btn-danger" id="close-all-cardex"><span class="glyphicon glyphicon-eye-close"></span></button>
                    <a class="btn-link-v2 pull-right" href="/v1/task/view" target="_blank">Liste des tâches</a>
                    <div class="SpaceTop" id='task-cardex-container'>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    @media screen and (min-width: 768px) {
        .container-btn-cardex {
            height: 240px;
            padding-top: 20px;
        }
        .container-btn-cardex>button {
            height: 180px;
        }
    }
    #cardex-container {
        border-left: #eeeeee 2px solid;
    }
    .select-full {
        width: 100%!important;
    }
    .cardex-task {
        border: 2px solid grey;
        border-radius: 4px;
        margin-bottom: 3px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
    }
    .cardex-task:hover {
        background-color: #ffcccc;
    }
    .img-cardex {
        height: 32px;
        width: 32px;
    }
    .text-bold-title {
        padding-top: 6px;
        font-size: 15px;
        font-weight: bold;
    }
    .panel-heading-button-right {
        position: absolute;
        right: 30px;
        top: 10px;
    }
    .overflow-task{
        display: none;
    }
    .btn-link-v2 {
        padding-top: 5px;
        padding-left: 2px;
        cursor: pointer;
        color: #1b6d85;
    }
    .btn-link-v2:hover {
        color: #00aeef;
        text-decoration: #00aeef underline;
    }
</style>
<script src="/js/cardex.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
<script>
    jQuery(document).ready(function() {
        let cardex_id = $('#cardex_id').val();

        refreshCardexTasks(cardex_id);
        $("#addTask").on('click', function () {
            let ajax_data = {
                cardex_id: cardex_id,
                l_task_id: $('#l_task_id').val(),
                l_ponderation_id: $('#l_ponderation_id').val(),
                lr_room_id: $('#lr_room_id').val(),
                color_hex: $('#color_hex').val()
            };

            addCardexTask(ajax_data);
        });

        $(document).on('click', '.cardex-task', function () {
            let overflow_selected = $(this).next();
            let already_open = overflow_selected.hasClass('selected-cardex');
            $(".overflow-task").slideUp().removeClass('selected-cardex');

            if (!already_open)
                overflow_selected.addClass('selected-cardex').slideDown();
        });

        $("#open-all-cardex").on("click", function () {
            $(".overflow-task").slideDown();
        });
        $("#close-all-cardex").on("click", function () {
            $(".overflow-task").slideUp();
        });
    });
</script>

