<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 03/07/2018
 * Time: 12:28
 */
require('../../inc/bootstrap.php');

if (empty($_GET['idMission']) || empty($_GET['idUser'] || $_GET['price']))
    App::setFlashAndRedirect('danger', 'Pas d\'informations sur la mission ou l\'utilisateur', '/');
$mission_user = App::getMission()->getMissionAndUser($_GET['idMission']);
if($mission_user->paymentAccount === null)
    App::setFlashAndRedirect('danger', 'l\'utilisateur n\'a pas de compte ', '/');
$price = $_GET['price'];
$getDiff = App::getMission()->getPriceDiff($_GET['idMission']);
$price_final = $price-$getDiff->p*100;
$stripe_pk = App::getConf()->getConfByName("stripe_pk");
require('../../inc/header_bo.php');
?>
    <link rel="stylesheet" href="css/style.css">
    <div class="panel panel-success">
        <div class="panel-heading">
            <input type="hidden" name="stripe_pk" id="stripe_pk" value="<?= $stripe_pk->value ;?>">
            Paiement de client : <?= $mission_user->firstname.' '.$mission_user->lastname  ?>
            <input type="hidden" name="userID"  id="userID" value="<?= $_GET['idUser']; ?>">
            <input type="hidden" name="missionID"  id="missionID" value="<?= $_GET['idMission']; ?>">
        </div>
        <div class="panel-body">
            <h2 class="my-4 text-center">montant à payer : <?= $price_final .' € Vous pouvez le modifier'?> </h2>
            <div class="form-row">
                <input type="hidden" name="priceFinale" id="priceFinale" value="<?= $price ?>">
                <input type="text" name="first_name" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="First Name" value="<?= $mission_user->firstname ?>">
                <input type="text" name="last_name" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="Last Name" value="<?= $mission_user->lastname  ?>">
                <input type="hidden" name="idUserIn" class="form-control mb-3 StripeElement StripeElement--empty"  value="<?=  $_GET['idUser']  ?>">
                <input type="hidden" name="idMissionIn" class="form-control mb-3 StripeElement StripeElement--empty"  value="<?=  $_GET['idMission']  ?>">
                <input type="email" name="email" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="Email Address" value="<?= $mission_user->email  ?>">
                <input type="number" name="priceOFMission" id="priceOFMission" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="Montant entre 0.5 €  ET <?= $price_final?> € ">
                <div id="alertMontant" class="alert alert-danger" role="alert" style="text-align: left;width: 50%;display: none;">
                    montant incorrect
                </div>
                <br>
            </div>
            <button type="submit" id="btnPayement" name="btnPayement" value="valider" > Valider</button>
        </div>
    </div>

    <p><a href="view" class="btn btn-light mt-2">Retour </a></p>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="/v1/payment/js/charge.js"></script>
    <script src="/v1/payment/js/PaymentAccount.js"></script>
<?php
require('../../inc/footer_bo.php');