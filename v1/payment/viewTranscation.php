<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 24/06/2018
 * Time: 23:28
 */

require('../../inc/bootstrap.php');


$transactions = App::getTransaction()->getAllPayement();

require('../../inc/header_bo.php');
?>

<div id="hidden_form_container hidden"></div>
<div class="panel panel-success">
    <div class="panel-heading">
        Transaction
    </div>
    <div class="transaction_buttons">
        <a href="Remboursement"    class="btn btn-primary">Remboursement</a>
        <a href="viewTranscation" class="btn btn-primary">Paiement </a>
    </div>
    <p><a href="view" class="btn btn-light mt-2">Retour </a></p>
    <table class="table">
        <thead>
        <tr>
            <th>numero transaction</th>
            <th>Prix</th>
            <th>Client</th>
            <th>Statut</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <?php foreach ($transactions as $key => $v): ?>
          <td><?=$v->transaction_ext_ref?></td>
          <td><?=$v->price.' €'?></td>
          <td><?=$v->firstname.' '.$v->lastname?></td>
          <td>Payé</td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<style>
    .transaction_buttons {
        margin-top: 10px;
        margin-left: 800px;
    }
</style>
