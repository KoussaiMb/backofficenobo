<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 25/06/2018
 * Time: 13:55
 */

require('../../inc/bootstrap.php');

require_once('../../vendor/autoload.php');

$stripe_sk = App::getConf()->getConfByName("stripe_sk");
\Stripe\Stripe::setApiKey($stripe_sk->value);

if ( empty($_GET['idMission']) || empty( $_GET['idUser']) || empty($_GET['price']) || empty($_GET['pricefinal']))
    App::setFlashAndRedirect('danger', 'Pas d\'informations sur la mission ou l\'utilisateur', '/');
$error = [];
$current_customer_id = $_GET['idUser'];
$mission = $_GET['idMission'];
$price = round($_GET['price']*100);
$priceOfMission = ($_GET['pricefinal']);
$transactions = App::getTransaction()->getPayementAccountByUserId($current_customer_id);
$checkIfExist = App::getMission()->checkIfMissionExist($mission);

foreach ($transactions as $key => $v):
    try {
        $charge = \Stripe\Charge::create(array(
            "amount" => $price,
            "currency" => "eur",
            "description" => "paiement v",
            "customer" => $transactions->paymentAccount
        ));

        $transactionData = [
            'id' => $charge->id,
            'customer_id' => $charge->customer,
            'product' => $charge->description,
            'amount' => $charge->amount,
            'currency' => $charge->currency,
            'status' => $charge->status,
        ];

        $transcation = [
            'id' => $charge->id,
        ];

        //vérifier si la mission est déja payée
        if ($checkIfMissionExist->number != 0) {
            $upPrice = App::getMission()->updatePrice($price/100,$mission_id);
        } else {
            $add_transaction = App::getTransaction()->addToTransaction($current_customer_id, $transactions->paymentAccount, $price/100, 1, $charge->id );
        }
        App::setFlashAndRedirect('success', 'Transaction effectuée avec succès pour le client', 'view');
    } catch(Stripe_CardError $e) {
        $error['Stripe_CardError'] = $e;
    } catch (Stripe_InvalidRequestError $e) {
        $error['Stripe_InvalidRequestError'] = $e;
    } catch (Stripe_AuthenticationError $e) {
        $error['Stripe_AuthenticationError'] = $e;
    } catch (Stripe_ApiConnectionError $e) {
        $error['Stripe_ApiConnectionError'] = $e;
    } catch (Stripe_Error $e) {
        $error['Stripe_Error'] = $e;
    } catch (Exception $e) {
        $error['Exception'] = $e;
    } finally {
        foreach ($error as $k)
           // App::logPDOException($k, 1);
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors du paiement', 'view');
    }
endforeach;





