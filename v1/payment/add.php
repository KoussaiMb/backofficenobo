<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 19/06/2018
 * Time: 10:20
 */
require('../../inc/bootstrap.php');

if ( empty($_GET['idMission']) || empty( $_GET['idUser']) || empty($_GET['price']))
    App::setFlashAndRedirect('danger', 'Pas d\'informations sur la mission ou l\'utilisateur', '/');

$getDiff = App::getMission()->getPriceDiff($_GET['idMission']);
$mission_user = App::getMission()->getMissionAndUser($_GET['idMission']);
$price_final = $_GET['price'];
$stripe_pk = App::getConf()->getConfByName("stripe_pk");
require('../../inc/header_bo.php');
?>
    <link rel="stylesheet" href="css/style.css">
    <div class="panel panel-success">
        <div class="panel-heading">
            <input type="hidden" name="stripe_pk" id="stripe_pk" value="<?= $stripe_pk->value ;?>">
            Paiement de client : <?= $mission_user->firstname.' '.$mission_user->lastname  ?>
            <input type="hidden" name="userID" value="<?= $_GET['idUser']; ?>">
            <input type="hidden" name="missionID" value="<?= $_GET['idMission']; ?>">
        </div>

        <div class="panel-body">
            <h2 class="my-4 text-center">montant à payer : <?= $price_final .' €'?> </h2>
            <form action="charge" method="post" id="payment-form">
                <div class="form-row">
                    <input type="hidden" name="priceFinale" value="<?= $price_final ?>">
                    <input type="text" name="firstname" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="First Name" value="<?= $mission_user->firstname ?>">
                    <input type="text" name="lastname" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="Last Name" value="<?= $mission_user->lastname  ?>">
                    <input type="hidden" name="current_customer_id" class="form-control mb-3 StripeElement StripeElement--empty"  value="<?=  $_GET['idUser']  ?>">
                    <input type="hidden" name="mission_id" class="form-control mb-3 StripeElement StripeElement--empty"  value="<?=  $_GET['idMission']  ?>">
                    <input type="email" name="email" class="form-control mb-3 StripeElement StripeElement--empty" required placeholder="Email Address" value="<?= $mission_user->email  ?>">
                    <div id="card-element" class="form-control">
                        <!-- a Stripe Element will be inserted here. -->
                    </div>
                    <!-- Used to display form errors -->
                    <br>
                    <div id="card-errors" role="alert" style="color: red;"></div>
                </div>
                <button>Valider</button>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="/v1/payment/js/charge.js"></script>
<?php
require('../../inc/footer_bo.php');
