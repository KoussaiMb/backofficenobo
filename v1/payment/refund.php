<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 25/06/2018
 * Time: 12:02
 */
require('../../inc/bootstrap.php');

if ( empty($_GET['idMission']) || empty( $_GET['idUser']) || empty($_GET['price']))
    App::setFlashAndRedirect('danger', 'Pas d\'informations sur la mission ou l\'utilisateur', '/');
$useUser = $_GET['idUser'];
$useMission = $_GET['idMission'];
$price = $_GET['price'];
$mission_user = App::getMission()->getMissionAndUser($_GET['idMission']);
$mission_userPrice = App::getMission()->getCartByMission($_GET['idMission']);

require('../../inc/header_bo.php');

?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Rembourser pour client : <?= $mission_user->firstname.' '.$mission_user->lastname  ?>
            <input type="hidden" name="userID" value="<?= $_GET['idUser']; ?>">
            <input type="hidden" name="missionID" value="<?= $_GET['idMission']; ?>">
        </div>
        <div class="container">
            <h2>Selectioner un raison</h2>
            <form action="chargeRefund" method="post">
                <?php
                $getRaisonFromList = App::getListManagement()->getList("refundList");
                ?>
                <div class="form-group">
                    <select required class="form-control" name="item_type_select" id="item_type_select"  style="width: 30%;"  >
                        <option  name="dd" data-select="Tous" value="">Raison ?</option>
                        <?php foreach($getRaisonFromList as $itl){?>
                            <option id=<?=$getID =$itl->id ?>><?= $itl->field ?></option>
                            <?php
                        } ?>
                    </select>
                </div>
                <div class="form-group row">
                    <div class="col-xs-4">
                        <input class="form-control" name="name" type="text" value="<?= $mission_user->firstname.' '.$mission_user->lastname; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-4">
                        <input class="form-control" name="email" type="text" value="<?= $mission_user->email; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-4">
                        <input class="form-control" name="amount" type="number" required placeholder="montant min 1 max <?=$price;?>"  min="1" max="<?= $price;?>" >
                    </div>
                </div>
                <input type="hidden" name="idMissions" id="idMissions" value="<?= $_GET['idMission']; ?>">
                <input type="hidden" name="user_id" id="user_id" value="<?= $_GET['idUser']; ?>">
                <button type="submit" class="btn btn-primary mb-2">Valider</button>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#item_type_select").on('change', function () {
                document.getElementById("c").value = ($(this).find('option:selected').attr('id'));
            });


        });
    </script>
<?php
require('../../inc/footer_bo.php');