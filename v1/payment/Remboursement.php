<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 29/06/2018
 * Time: 15:59
 */

require('../../inc/bootstrap.php');

$refunds = App::getRefund()->getAllRefund();

require('../../inc/header_bo.php');
?>

<div id="hidden_form_container" style="display:none;"></div>
<div class="panel panel-success">
    <div class="panel-heading">
        Remboursement
    </div>
    <div style="margin-top: 20px;margin-left: 860px;margin-bottom: 30px;">
        <a href="Remboursement"    class="btn btn-primary">Remboursement</a>
        <a href="viewTranscation" class="btn btn-primary">Paiement </a>
    </div>
    <p><a href="view" class="btn btn-light mt-2">Retour </a></p>
    <table class="table">
        <thead>
        <tr>
            <th>numero transaction</th>
            <th>Prix</th>
            <th>Client</th>
            <th>Statut</th>
            <th>Raison</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <?php foreach ($refunds as $key => $v): ?>
             <td><?=$v->transaction_ext_ref?></td>
             <td><?=$v->amount?></td>
             <td><?=$v->firstname?></td>
             <td>Remboursé</td>
             <td><?= App::getListManagement()->getFieldById($v->l_refund_id)?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
