<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 25/06/2018
 * Time: 17:20
 */
require('../../inc/bootstrap.php');
require_once('../../vendor/autoload.php');
$stripe_sk = App::getConf()->getConfByName("stripe_sk");
\Stripe\Stripe::setApiKey($stripe_sk->value);
$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
$mission_id = $POST['idMissions'];
$current_customer_id = $POST['user_id'];
$error = [];
$priceForStripe = $POST['amount']* 100;
$transcDataUserAccountID = App::getRefund()->getByUserIDForTrnasaction($current_customer_id);
$l_refundReason_id = null;

try {
$charge = \Stripe\Charge::create(array(
    "amount" => $priceForStripe,
    "currency" => "eur",
    "description" => "paiement v",
//    "customer" => "cus_DDVUxHc9nUrUrK"
    "customer" => $transcDataUserAccountID->accountID
));

$transactionData = [
    'id' => $charge->id,
    'customer_id' => $charge->customer,
    'product' => $charge->description,
    'amount' => $charge->amount,
    'currency' => $charge->currency,
    'status' => $charge->status,
];

$transcation = [
    'id' => $charge->id,
];

$re = \Stripe\Refund::create(array(
    "charge" => $transcDataUserAccountID->idTransc,
     "amount" => $priceForStripe,
));

$refuTable = App::getRefund()->addRefund($mission_id, $priceForStripe, $l_refundReason_id, $charge->id, $current_customer_id);

App::setFlashAndRedirect('success', 'Le remboursement a bien été effectué', 'view');
} catch(Stripe_CardError $e) {
    $error['Stripe_CardError'] = $e;
} catch (Stripe_InvalidRequestError $e) {
    $error['Stripe_InvalidRequestError'] = $e;
} catch (Stripe_AuthenticationError $e) {
    $error['Stripe_AuthenticationError'] = $e;
} catch (Stripe_ApiConnectionError $e) {
    $error['Stripe_ApiConnectionError'] = $e;
} catch (Stripe_Error $e) {
    $error['Stripe_Error'] = $e;
} catch (Exception $e) {
    $error['Exception'] = $e;
} finally {
    foreach ($error as $k)
        //App::logPDOException($k, 1);
    App::setFlashAndRedirect('danger', 'Un problème est survenu lors du paiement', 'view');
}

