<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 26/06/2018
 * Time: 14:17
 */
require('../../inc/bootstrap.php');

$user_id = $_REQUEST['action'];

$today =date("Y/m/d");
$missionStatut ="mission non payée";
$user_transactions = App::getMission()->getTransactionByUser($user_id);
$user = App::getTransaction()->getPayementAccountByUserId($user_id);
$paymentAccount = $user->paymentAccount;
$customers = App::getCustomer();
$autocompleteClients = $customers->getAutocompleteName();
$checkedCustomers = $customers->getAll('checked');
if ($autocompleteClients === null || $checkedCustomers === null) {
    Session::getInstance()->setFlash('danger', 'Impossible de récupérer les informations serveur');
}

?>

<body>
<table id="example" class="display" style="width:100%">
    <thead>
    <tr>
        <th class="hidden">mission_id</th>
        <th>Date</th>
        <th>Durée</th>
        <th>Prix</th>
        <th class="hidden">Prix</th>
        <th>statut</th>
        <th>Payer via carte</th>
        <th>Payer via compte</th>
        <th>Rembourser</th>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($user_transactions as $key => $transaction): ?>
        <tr>
            <td class="hidden" id="mission_id"><?=$transaction->id ?></td>
            <td><?php echo date("d-M-Y ",strtotime($transaction->start)); ?></td>
            <td><?= $start_date = date('H:i', strtotime($transaction->work_time)); ?></td>
            <td><?= $transaction->price_final_cart.' € '; ?></td>
            <td class="hidden" id="price_final"><?= $transaction->price_final_cart; ?></td>
            <?php $getDiff = App::getMission()->getPriceDiff($transaction->id); ?>
            <?php foreach ($getDiff as $key => $status): ?>
            <?php if ($status->difference == 0 && $status->payed == 1): ?>
                <td>mission payée<i class="fa fa-check" style="font-size:24px;color:green"></i></td>
                <td><button type="button" name="paymentCard" value="add" class="btn btn-primary paymentButton"  disabled>Payer</button></td>
                <td><button type="button" name="paymentAccount" value="addCompte" class="btn btn-primary paymentButton"  disabled>Payer</button></td>
                <td><button type="button" name="refund" value="refund" class="btn btn-primary paymentButton">Rembourser</button></td>
            <?php elseif ($status->difference > 0 && $status->payed == 1): ?>
                <td style="color: red"> <?=$status->difference.' € reste à payer';?></td>
                <td><button type="button" name="paymentCard" value="add" class="btn btn-primary paymentButton">Payer</button></td>
                <td><button type="button" name="paymentAccount" value="addCompte" class="btn btn-primary paymentButton">Payer</button></td>
                <td><button type="button" name="refund" value="refund" class="btn btn-primary paymentButton" disabled>Rembourser</button></td>
            <?php elseif ($status->payed == 0): ?>
                <td>mission non payée<i class="fa fa-check" style="font-size:24px;color:red"></i></td>
                <td><button type="button" name="paymentCard" value="add" class="btn btn-primary paymentButton">Payer</button></td>
                <td><button type="button" name="paymentAccount" value="addCompte" class="btn btn-primary paymentButton">Payer</button></td>
                <td><button type="button" name="refund" value="refund" class="btn btn-primary paymentButton" disabled>Rembourser</button></td>
                <?php endif; ?>
            <?php endforeach?>

        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th>Date Deb</th>
        <th>Validée</th>
        <th>Durée</th>
        <th>statu</th>
        <th>Payer via compte</th>
        <th>Payer via carte</th>
        <th>Rembourser</th>
    </tr>
    </tfoot>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>

<script>
    $('.paymentButton').on( "click", function() {
        var price = $(this).closest('tr').find('#price_final').text();
        var mission_id = $(this).closest('tr').find('#mission_id').text();
        let page_red = $(this).val();
        let current_customer_id = $('#current_customer_id').val();
        window.location = page_red +"?idUser="+ current_customer_id+"&idMission="+mission_id+"&price="+price;
    });
</script>

<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    let column = this;
                    let select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            let val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );
</script>
