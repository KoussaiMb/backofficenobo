$('#btnPayement').click(function(){

    let price = $('#priceFinale').val();
    let mission_price = $('#priceOFMission').val();
    let mission_id = $('#missionID').val();
    let customer_id = $('#userID').val();

    let price_to_float = parseFloat(price);
    let mission_id_to_float = parseFloat(mission_price);


    if (mission_id_to_float < price_to_float && mission_id_to_float > 0.5){
        window.location = "chargeUser?idUser="+customer_id+"&idMission="+mission_id+"&price="+mission_id_to_float+"&pricefinal="+price_to_float;
    } else {
        $('#alertMontant')
            .text("montant non valide : le montant doit etre compris entre 0.5 ET " + price  + " euros ")
            .css('display', 'block');
    }


});