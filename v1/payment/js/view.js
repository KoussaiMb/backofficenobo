$(document).ready(function() {
    $("#item_type_select").on('change', function () {
        let current_customer_id = $(this).find('option:selected').attr('id');
        let dataString = 'action='+ current_customer_id;

        $("#current_customer_id").val(current_customer_id);
        $.ajax
        ({
            url: './getproducts',
            data: dataString,
            cache: false,
            success: function(r)
            {
                $('#txtHint').html(r);
            },
            error: function () {
                bo_notify('Erreur inconnue, contacter un admin', 'danger');
            }
        });
    });
});
