<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 19/06/2018
 * Time: 10:20
 */
require('../../inc/bootstrap.php');

$customers = App::getCustomer();
$autocompleteClients = $customers->getAutocompleteName();
$checkedCustomers = $customers->getAll('checked');
if ($autocompleteClients === null || $checkedCustomers === null) {
    Session::getInstance()->setFlash('danger', 'Impossible de récupérer les informations serveur');
}
require('../../inc/header_bo.php');
?>
    <div id="hidden_form_container" style="display:none;"></div>
    <div class="panel panel-success">
        <div class="panel-heading">
            Gestion des Paiement
        </div>
        <div class="btn-group buttonsView" role="group">
            <a href="viewTranscation" class="btn btn-primary" style="margin-right: 5px;">Voir Transactions</a>
            <a href="Remboursement" class="btn btn-primary">Voir Remboursement</a>
        </div>
        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="container">
                <form method="GET" action="">
                    <?php
                    $item_type_list = App::getCustomer()->getAll();
                    ?>
                    <div class="form-group">
                        <input type="hidden" id ="current_customer_id" name="current_customer_id">
                        <input type="hidden" name="id" id="idForPaiement">
                        <input type="hidden" name="id" id="amountChoice">
                        <input type="hidden" name="testInputE" id="testInputE">
                        <input type="hidden" name="cacheAmount" id="cacheAmount">
                        </br></br>
                        <select required  data-live-search="true" data-live-search-style="startsWith" class="selectpicker" name="item_type_select" id="item_type_select"  style="width: 30%;" >
                            <option class="hidden" selected>Selectionner un Client</option>
                            <?php foreach($item_type_list as $itl):?>
                                <option id=<?=$getID =$itl->id ?>><?= $itl->firstname .' ' .$itl->lastname?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
            </div>
        </div>
        <div id="txtHint">
            <div class="alert alert-info alert-noinfo" role="alert">
                Pas des informations
            </div>
        </div>
    </div>

    <style>
        .highlight{
            background-color: aliceblue;
        }
        .scroll-table{
            table-layout: fixed;
            width:100%;
            overflow-y: auto;
            max-height: 500px;
        }
        .alert-noinfo {
            width: 50%;
            margin-left: 200px;
        }
        .buttonsView{
            margin-top: 60px;
            float: right;
            margin-right: 20px;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
    <script src="./js/view.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                initComplete: function () {
                    this.api().columns().every( function () {
                        let column = this;
                        let select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                let val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
        } );
        } );
    </script>
<?php
require('../../inc/footer_bo.php');
