<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 22/06/2018
 * Time: 16:39
 */
require('../../inc/bootstrap.php');
require_once('../../vendor/autoload.php');
$stripe_sk = App::getConf()->getConfByName("stripe_sk");
\Stripe\Stripe::setApiKey($stripe_sk->value);
$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
$firstname = $POST['firstname'];
$lastname = $POST['lastname'];
$email = $POST['email'];
$token = $POST['stripeToken'];
$current_customer_id = $POST['current_customer_id'];
$mission_id = $POST['mission_id'];
$price = round($POST['priceFinale']*100);
$error = [];
$checkIfExist = App::getMission()->checkIfMissionPayed($mission_id);

try {
    $customer = \Stripe\Customer::create(array(
        "email" => $email,
        "source" => $token
    ));
    $charge = \Stripe\Charge::create(array(
        "amount" => $price,
        "currency" => "eur",
        "description" => "paiement v",
        "customer" => $customer->id
    ));

    $customerData = [
        'id' => $charge->customer,
        'first_name' => $firstname,
        'last_name' => $lastname,
        'email' => $email
    ];

    $transactionData = [
        'id' => $charge->id,
        'customer_id' => $charge->customer,
        'product' => $charge->description,
        'amount' => $charge->amount,
        'currency' => $charge->currency,
        'status' => $charge->status,
    ];

    $transcation = [
        'id' => $charge->id,
    ];

    //vérifier si la mission est déja payée
    if ($checkIfMissionExist->number != 0) {
        $upPrice = App::getMission()->updatePrice($price,$mission_id);
    } else {
        $add_transaction = App::getTransaction()->addToTransaction($current_customer_id, $customer->id, $price, 1, $charge->id );
    }

    App::setFlashAndRedirect('success', 'Transaction effectuée avec succès pour le client ' . $firstname . ' ' . $lastname, 'view');
} catch(Stripe_CardError $e) {
    $error['Stripe_CardError'] = $e;
} catch (Stripe_InvalidRequestError $e) {
    $error['Stripe_InvalidRequestError'] = $e;
} catch (Stripe_AuthenticationError $e) {
    $error['Stripe_AuthenticationError'] = $e;
} catch (Stripe_ApiConnectionError $e) {
    $error['Stripe_ApiConnectionError'] = $e;
} catch (Stripe_Error $e) {
    $error['Stripe_Error'] = $e;
} catch (Exception $e) {
    $error['Exception'] = $e;
} finally {
    foreach ($error as $k)
        App::logPDOException($k, 1);
    App::setFlashAndRedirect('danger', 'Un problème est survenu lors du paiement', 'view');
}