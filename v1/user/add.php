<?php
require('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin') {
    App::redirect('error/404');
}

$groups = App::getManageGroup()->groupsAsOption();
$formData = Session::getInstance()->read('formData');

if (empty($groups))
    Session::getInstance()->setFlash('info', 'Aucun groupe n\'a été trouvé');

require('../../inc/header_bo.php');
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Manage Bo users
        </div>
        <div class="panel-body">
            <div class="col-xs-3">
                <!--
                <div class="alert alert-info col-sm-12" role="alert">
                    <p>L'<strong>email</strong> peut être un pseudo, il ne sera pas vérifié</p>
                </div>
                -->
                <div class="alert alert-warning col-sm-12" role="alert">
                    <p>Le <strong>mot de passe</strong> doit comporter:</p>
                    <ul>
                        <li>minimum 1 lettre</li>
                        <li>minimum 1 chiffre</li>
                        <li>entre 8 et 20 caractères</li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6">
                <form method="post" action="form" class="form-horizontal" id="form_user" enctype="multipart/form-data">
                    <?php require('../../inc/print_flash_helper.php');?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="firstname">firstname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="firstname" placeholder="Prénom" value="<?= isset($formData) && isset($formData->firstname) ? $formData->firstname : null; ?>" id="firstname" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="lastname">lastname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="lastname" placeholder="Nom" value="<?= isset($formData) && isset($formData->lastname) ? $formData->lastname : null; ?>" id="lastname" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="email">email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="email" placeholder="Adresse email" value="<?= isset($formData) && isset($formData->email) ? $formData->email : null; ?>" id="email" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="password">Mot de passe</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" name="password" placeholder="Mot de passe" id="password"/>
                                <div class="input-group-btn"><button type="button" class="btn btn-default" onclick="$('#password').val(rand());">Random</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="group">Groupe</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="group" id="group">
                                <?php echo $groups; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center;">
                        <button type="submit" name="action" value="add" class="btn btn-primary col-xs-10 col-xs-offset-2">Créer un nouveal utilisateur BackOffice</button>
                    </div>
                </form>
            </div>
            <div class="col-xs-3" style="text-align: center">
                <a href="/v1/group/add"><button class="btn btn-warning" style="height: 3.5em">Ajouter un groupe</button></a>
            </div>
        </div>
    </div>
<script>
    //todo: validator para el groupo
</script>
<?php
require('../../inc/footer_bo.php');