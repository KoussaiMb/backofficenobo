<?php
include_once('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/error/404');

$db = App::getDB();

if (!isset($_POST['action']))
    App::redirect('add');

Session::getInstance()->update('formData', (object)$_POST);

if ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    $validator->is_digit('group', 'Groupe non valide', 'Indiquer un groupe');
    $validator->is_alpha('firstname', 'Le prénom est invalide', 'Vous avez oublié le prénom');
    $validator->is_alpha('lastname', 'Le nom est invalide', 'Vous avez oublié le nom');
    $validator->is_email('email', 'L\'email est invalide', 'Vous avez oublié l\'email');
    $validator->is_uniq($db, 'user', 'email', 'email', 'l\'email/pseudo est déjà utilisé');
    $validator->is_pw_light('password', 'Le password n\'est pas valide', 'Il manque le password');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('add');
    }
    $ret = App::getUser()->addBoUser($_POST);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout de l\'utilisateur', "add");
    Session::getInstance()->delete('formData');
    App::setFlashAndRedirect('success', 'L\'utilisateur a été ajouté avec succès', "add");
}
else if ($_POST['action'] == 'edit') {
    $m_groups = App::getManageGroup();
    $group = $m_groups->getGroup($_POST['groupe_id']);
    if (!$group) {
        Session::getInstance()->setFlash('danger', 'Le groupe à modifier n\'existe pas');
        App::redirect("/v1/group/view");
    }
    $parsed_arr = $m_groups->parseFromUserEdit($_POST);
    if (!$parsed_arr)
        Session::getInstance()->setFlash('danger', "Les informations ne sont pas valides");
    elseif (!empty($parsed_arr['out']) && !$m_groups->removeUserRights($parsed_arr['out'], $_POST['groupe_id']))
        Session::getInstance()->setFlash('danger', "Un problème est survenu en retirant les droits");
    elseif (!empty($parsed_arr['in']) && !$m_groups->addUserRights($parsed_arr['in'], $_POST['groupe_id']))
        Session::getInstance()->setFlash('danger', "Un problème est survenu en ajoutant les droits");
    else {
        Session::getInstance()->delete('formData');
        Session::getInstance()->setFlash('success', "Les droits des utilisateurs ont bien été ajoutés");
    }
    App::redirect('/v1/group/edit?id=' . $_POST['groupe_id'] . '&menu=user');
}
App::redirect('add');
