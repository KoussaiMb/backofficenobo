<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    if (!$validator){
        $json = parseJson::error('Impossible de récupérer les droits de l\'utilisateur', $validator->getErrors());
        $json->printJson();
    }
    try {
        $output = App::getManageGroup()->getUserRights($_GET['user_token']);
        $json = parseJson::success('Les droits du group ont été mis à jour', $output);
    } catch (Exception $e) {
        $json = parseJson::error('Impossible de récupérer les droits de l\'utilisateur', $e->getMessage());
    } finally {
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    if (!$validator){
        $json = parseJson::error('Impossible de mettre à jour les droits de l\'utilisateur', $validator->getErrors());
        $json->printJson();
    }
    try {
        $_PUT['edited_by'] = App::getAuth()->user()->id;
        $output = App::getManageGroup()->updateUserRights($_PUT);
        $json = parseJson::success('Les droit de l\'utilisateur ont été mis à jour', $output);
    } catch (Exception $e) {
        $json = parseJson::error('Impossible de mettre à jour les droits de l\'utilisateur', $e->getMessage());
    } finally {
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    if (!$validator->is_valid()) {
        $json = parseJson::error('Impossible de supprimer les droits de l\'utilisateur', $validator->getErrors());
        $json->printJson();
    }
    $output = App::getManageGroup()->removeUserRights($_DELETE['user_token']);
    if ($output)
        $json = parseJson::success('Les droits de l\'utilisateur ont été entièrement supprimé', $output);
    else
        $json = parseJson::error('Impossible de supprimer les droits de l\'utilisateur');
    $json->printJson();
}