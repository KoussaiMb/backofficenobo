<?php
require('../../inc/bootstrap.php');

$db = App::getDB();
$tasks = App::getTaskTwo()->getAll();

if ($tasks === false)
    Session::getInstance()->setFlash('danger', 'Une erreur est survenue lors de l\'affichage des tâches');
elseif (empty($tasks))
    Session::getInstance()->setFlash('danger', 'Aucune tâche n\'existe');

require('../../inc/header_bo.php');

?>

<div class="panel panel-success">
    <div class="panel-heading">
        Choisissez une tâche
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-12 text-center">
            <label class="col-xs-3 text-right" for="task_list" style="margin-top: 0.6em">Liste des tâches</label>
            <div class="col-xs-3">
                <input type="hidden" class="form-control" id="task_list" disabled>
            </div>
            <div class="col-xs-5" style="margin-top: 0.6em">
                <select class="form-control" name="filter" id="filter">
                    <option value="0">Toutes</option>
                    <option value="1">Roulantes</option>
                    <option value="2">Non roulantes</option>
                </select>
            </div>
            <div class="col-xs-1 text-right">
                <a href="add"><button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button></a>
            </div>
        </div>
        <div class="col-xs-12 task-container">
        <?php if ($tasks): ?>
            <?php foreach ($tasks as $k => $v): ?>
                <div class="col-xs-4 task-view">
                    <input type="hidden" name="rol" value="<?= $v->listName == "rollingTask" ? 1 : 0;?>">
                    <input type="hidden" name="id" value="<?= $v->id;?>">
                    <div class="col-xs-12">
                        <p><strong><?= $v->field; ?></strong></p>
                        <p>
                            <?= $v->listName == "rollingTask" ? 'Roulante' : '';?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        </div>
    </div>
</div>
<style>
    .task-view {
        padding: 20px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .task-view > div:hover {
        border: 1px solid grey;

    }
    .task-view img {
        max-width: 100%;
        max-height: 100%;
    }
    .task-view > div {
        background-color: #f0f0f5;
        border-radius: 3px;
        padding-top: 10px;
        height: 80px;
    }
    .task-view p:last-child {
        color: #e60073;
    }
    .task-container {
        overflow-y: scroll;
        margin-top: 2em;
        border: 2px solid #DBEED4;
        border-radius: 3px;
    }
</style>
<script>
    jQuery(document).ready(function() {
        let allTask = $('.task-view');
        let rol = $("input[name='rol'][value='1']");
        let no_rol = $("input[name='rol'][value='0']");
        let is_rolling = $("#is_rolling");

        $("#filter").on("change", function(){
            allTask.hide();

            if ($("#filter").val() === '1') {
                rol.parent('.task-view').show();
            } else if ($("#filter").val() === '2'){
                no_rol.parent('.task-view').show();
            } else {
                allTask.show();
            }
        });

        $(".task-container").css('max-height', $(window).height() * 0.7);

        allTask.on('click', function () {
            window.location = 'edit?id=' + $(this).find("input[name='id']").val();
        })
    });
</script>
<?php
require('../../inc/footer_bo.php');