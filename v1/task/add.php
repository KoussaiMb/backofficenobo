<?php
require('../../inc/bootstrap.php');

$task = Session::getInstance()->read('task');

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Création d'une nouvelle tâche
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-3">
            <div class="alert alert-warning col-sm-12">
                <p>NOM DE TACHE</p>
                <p>80 caractères maximum</p>
            </div>
        </div>
        <div class="col-xs-6">
            <form method="post" action="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nom de la tâche</label>
                    <input type="text" class="form-control" name="name" value="<?= empty($task) ? null : $task['name']; ?>" placeholder="ex: repassage" id="name" required>
                </div>
                <div class="form-group">
                    <div><label for="type">Type de tâche</label></div>
                    <input type="hidden" name="is_rolling" value="0">
                    <label class="checkbox-inline"><input type="checkbox" name="is_rolling" value="1" <?= !empty($task) && $task['is_rolling'] == 1 ? "checked" : null; ?>>Roulante</label>
                </div>

                <button type="submit" class="btn btn-success" name="action" value="add">Ajouter une tâche</button>
            </form>
            <a href="view"><button type="submit" class="btn btn-default" style="margin-top: -50px;margin-left: 150px;">Revenir à la gestion des tâches</button></a>

        </div>
    </div>
</div>
<script>

</script>
<?php
require('../../inc/footer_bo.php');