<?php
require('../../inc/bootstrap.php');

$validator = new Validator($_GET);

$validator->is_id("id", "L'id est invalide", "L'id est introuvable");

if (!$validator->is_valid())
    App::redirect('view');

$task = App::getTaskTwo()->getTaskById($_GET['id']);


if (!$task) {
    App::setFlashAndRedirect('warning', "L'id est invalide", 'view');
}

require('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading">
        modifier une tâche
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-3">
        <div class="alert alert-warning col-sm-12">
                <p>NOM DE TACHE</p>
                <p>80 caractères maximum</p>
            </div>
        </div>
        <div class="col-xs-6">
            <form method="post" action="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nom de la tâche</label>
                    <input type="text" class="form-control" name="name" value="<?= $task->field; ?>" id="name">
                </div>
                <div class="form-group">
                    <div><label>Type de tâche possible</label></div>
                    <input type="hidden" name="is_rolling" value="0">
                    <label class="checkbox-inline"><input type="checkbox" name="is_rolling" value="1" <?= $task->listName == "rollingTask" ? 'checked' : '' ; ?>>Roulante</label>
                </div>
                <div class="text-center">
                    <input type="hidden" name="id" value="<?= $_GET['id']; ?>">
                    <a href="view"><button type="submit" class="btn btn-default">Revenir à la gestion des tâches</button></a>
                    <button type="submit" class="btn btn-success" name="action" value="edit">Modifier la tâche</button>
                    <button type="submit" class="btn btn-danger" name="action" value="delete" onclick="return confirm('Ceci va supprimer définitivement la tâche dans tous les plannings d\'entretiens')"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

</script>
<?php
require('../../inc/footer_bo.php');