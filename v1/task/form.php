<?php
include_once('../../inc/bootstrap.php');

error_log("coucou");

if ($_GET['action'] == 'add_ironing')
{
    Session::getInstance()->write('task', $_POST);

    $ret = App::getTaskTwo()->addTask($_GET['name'], 1, 1);
    if ($ret === -1)
        App::setFlashAndRedirect("danger", "Un problème est survenu lors de l'ajout de la tâche", "add");
    else {
        Session::getInstance()->delete('task');
        App::setFlashAndRedirect("success", "La tâche a bien été ajoutée", "view");
    }
}
elseif ($_GET['action'] == 'view')
{
    App::setFlashAndRedirect("", "", "view");

}
elseif ($_GET['action'] == 'addTask')
{
    App::setFlashAndRedirect("", "", "add");
}
if ($_POST['action'] == 'add' )
{
    $validator = new Validator($_POST);
    $validator->no_symbol('name', 'Le nom de la tâche est invalide', 'Vous avez oublié le nom de la tâche');
    $validator->is_num('is_rolling', 'Un problème avec le type de tâche', 'Un problème avec le type de tâche');

    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), "add");

    Session::getInstance()->write('task', $_POST);

    $ret = App::getTaskTwo()->addTask($_POST['name'], $_POST['is_rolling'] == '1', 1);
    if ($ret === -1)
        App::setFlashAndRedirect("danger", "Un problème est survenu lors de l'ajout de la tâche", "add");
    else {
        Session::getInstance()->delete('task');
        App::setFlashAndRedirect("success", "La tâche a bien été ajoutée", "view");
    }
}
elseif ($_POST['action'] == 'edit')
{
    $validator = new Validator($_POST);
    $validator->no_symbol('name', 'Le nom de la tâche est invalide', 'Vous avez oublié le nom de la tâche');
    $validator->is_num('is_rolling', 'Un problème avec le type de tâche', 'Un problème avec le type de tâche');
    $validator->is_id('id', 'La tâche est inconnue', 'La tâche est inconnue');

    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), "edit?id=" . $_POST['id']);

    $ret = App::getTaskTwo()->editTask($_POST["id"], $_POST["name"], $_POST["is_rolling"] == '1');
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'La modification de la tâche a échoué', "edit?id=" . $_POST['id']);
    App::setFlashAndRedirect("success", "La tâche a bien été modifiée", "view");
}
elseif ($_POST['action'] == 'delete')
{
    $validator = new Validator($_POST);
    $validator->is_id('id', 'Id erronné', 'Pas d\'id');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', "Id erronné", 'view');
    $ret = App::getTaskTwo()->deleteTask($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'La suppression de la tâche a échoué', "edit?id=" . $_POST['id']);
    App::setFlashAndRedirect('success', 'La tâche a bien été supprimée', "view");
}
elseif ($_POST['action'] == 'addTaskSpecific')
{

    Session::getInstance()->write('task', $_POST);

    $mp_id = $_POST['mp_id'];
    $address_id = $_POST['address_id'];
    $name = $_POST['task-specific-name'];

    $ret = App::getTaskTwo()->addTask($name, 1, 1);
    $ret_task = App::getMaintenancePlanningType()->insertMPT($mp_id,$ret,$address_id);
    if ($ret === -1)
        App::setFlashAndRedirect("danger", "Un problème est survenu lors de l'ajout de la tâche", "add");
    if (!$ret_task)
        App::setFlashAndRedirect("danger", "Un problème est survenu lors de l'ajout de la tâche", "add");
    else {
        Session::getInstance()->delete('task');
        App::setFlashAndRedirect("success", "La tâche a bien été ajoutée", "view");
    }
}


App::redirect("view");
