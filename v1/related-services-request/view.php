<?php
require_once('../../inc/bootstrap.php');

$relatedServicesRequested = App::getRelatedServices()->getAllServicesRequested();

require_once('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Requêtes des services connexes
    </div>
    <div class="panel-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php include('../../inc/print_flash_helper.php'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h4><span class="glyphicon glyphicon-cog"></span> Filtres</h4>
                        </div>
                    </div>
                    <div class="row">
                        <!-- liste des paramètres -->
                        <div class="col-xs-12">
                            <div class="radio">
                                <label for="processed">
                                    <input type="radio" name="filter_request" value="processed">
                                    Requêtes traitées
                                </label>
                            </div>
                            <div class="radio">
                                <label for="canceled">
                                    <input type="radio" name="filter_request" value="canceled">
                                    Annulations
                                </label>
                            </div>
                            <div class="radio">
                                <label for="">
                                    <input type="radio" name="filter_request" value="waiting" checked>
                                    En attente
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h4>Requêtes de services connexes</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 text-center">
                            <p><span class="glyphicon glyphicon-stop alert-success"></span>Requêtes traitées</p>
                        </div>
                        <div class="col-xs-4 text-center">
                            <p><span class="glyphicon glyphicon-stop alert-danger"></span>Problèmes</p>
                        </div>
                        <div class="col-xs-4 text-center">
                            <p><span class="glyphicon glyphicon-stop alert-warning"></span>Annulations</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 SpaceTop">
                            <table class="row-border" id="request-table">
                                <thead>
                                <tr>
                                    <th class="">customer_id</th>
                                    <th class="">rs_request_id</th>
                                    <th class="">processed</th>
                                    <th class="">canceled</th>
                                    <th class="">waiting</th>
                                    <th>Client</th>
                                    <th>Date</th>
                                    <th>Service</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($relatedServicesRequested as $request): ?>
                                <tr class="select-client">
                                    <td class=""><?= $request['customer_id']; ?></td>
                                    <td class=""><?= $request['rs_request_id']; ?></td>
                                    <td class=""><?= $request['processed']; ?></td>
                                    <td class=""><?= $request['canceled']; ?></td>
                                    <td class=""><?= $request['waiting']; ?></td>
                                    <td class="text-overflow"><?= $request['customer_name']?></td>
                                    <td><?= $request['requested_at']; ?></td>
                                    <td><?= $request['service_name']; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal delete dispo -->
<div class="modal fade" id="rs_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="col-xs-12" style="padding-top: 5px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                </div>
                <div class="col-xs-12">
                    <p id="customer_name"></p>
                    <p id="customer_email"></p>
                    <p id="customer_phone"></p>
                    <p id="customer_address"></p>
                </div>
                <div class="col-xs-12">
                    <div class="btn-group btn-group-justified" role="group">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-success btn-action-rs" name="action" value="processed" data-target="processDisplay">
                                Prise en charge
                            </button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning btn-action-rs" name="action" value="canceled" data-target="cancelDisplay">
                                Annulation
                            </button>
                        </div>
                    </div>
                    <div class="text-center" style="padding: 10px">
                        <div class="display-div hidden" id="processDisplay">
                            Le client va consommer ce service
                            <span class="glyphicon glyphicon-ok"></span>
                        </div>
                        <div class="display-div hidden" id="cancelDisplay">
                            <textarea class=" form-control no-resize" placeholder="Raison de l'annulation..." id="cancel_comment"></textarea>
                            <input type="hidden" class="input-rs" id="processed" name="processed" value="0">
                            <input type="hidden" class="input-rs" id="canceled" name="canceled" value="0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border: none;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-default hidden" name="action" value="validate" id="validation">Valider</button>
            </div>
        </div>
    </div>
</div>
<style>
    .text-overflow {
        text-overflow: ellipsis;
    }
    table>tbody>tr.select-client:hover {
        background-color: #ccff99;
    }
    .select-client {
        cursor: pointer;
    }
    #request-table_filter {
        display: none;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
    jQuery(document).ready(function() {
        let table = $('#request-table').DataTable({
            "lengthChange": false,
            "pageLength": 20,
            "columnDefs": [{className: "hidden", "targets": [0, 1, 2, 3, 4]}]
        });


        $("input[type='radio']").on('change', function () {
            let filter_request = $("input[name='filter_request']:checked").val();

            table.search(filter_request).draw();
        }).change();

        $('.select-client').on('click', function () {
            let rowData = table.row($(this)).data();
            let customer_id = rowData[0];
            $(this).addClass('selected-row');

            ajax_call('/v1/client/api/info', 'GET', {customer_id: customer_id}, function (res) {
                if (res.code === '200') {
                    updateModalWithClientInfo(res.data);
                    $('.display-div').addClass('hidden');
                    $('#validation').addClass('hidden');
                    $('#rs_modal').modal();
                }
                else
                    bo_notify(res.message, 'danger')
            })
        });

        function updateModalWithClientInfo(cus_info) {
            if (cus_info.address !== null)
                $('#customer_address').text(cus_info.address + ', ' + cus_info.city + ', ' + cus_info.zipcode);
            else
                $('#customer_address').text('');
            $('#customer_phone').text(cus_info.phone);
            $('#customer_email').text(cus_info.email);
            $('#customer_name').text(cus_info.firstname + ' ' + cus_info.lastname);
        }

        $('#validation').on('click', function () {
            let row = $('.selected-row');
            let rs_request_id = table.row(row).data()[1];

            ajax_call('/v1/related-services-request/api/request', 'PUT', {
                rs_request_id: rs_request_id,
                canceled: $('#canceled').val(),
                cancel_comment: $('#cancel_comment').text(),
                processed: $('#processed').val()
            }, function (res) {
                if (res.code === '200') {
                    bo_notify(res.message, 'success');
                    table.row(row).remove().draw();
                    row.removeClass('selected-row');
                } else {
                    bo_notify(res.message, 'danger');
                }
            });
            $('#rs_modal').modal('hide');
        });

        $('.btn-action-rs').on('click', function () {
            let target = $('#' + $(this).data('target'));
            let targets = $('.display-div');
            let btnValidation = $('#validation');
            $('.input-rs').val(0);
            $('#' + $(this).val()).val(1);

            if (target.hasClass('hidden')) {
                targets.addClass('hidden');
                target.removeClass('hidden');
                btnValidation.removeClass('hidden');
            } else {
                targets.addClass('hidden');
                btnValidation.addClass('hidden');

            }
        })
    });
</script>

<?php
require('../../inc/footer_bo.php');