<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 09/08/2018
 * Time: 17:34
 */
require_once('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('rs_request_id', 'Le service connexe est invalide', 'Le service connexe est manquant');
    $validator->is_id('processed', 'Le succès de l\'action est invalide');
    $validator->is_id('canceled', 'L\'annulation est invalide');
    $validator->no_symbol('cancel_comment', 'La raison d\'annulation est invalide');
    if ($validator->is_valid() === false)
        parseJson::error("Les données pour mettre à jour la requête du service connexe est invalide")->printJson();
    if ($_PUT['canceled'] == 0)
        $_PUT['cancel_comment'] = null;
    $rowCount = App::getRelatedServices()->updateRequest($_PUT);
    if ($rowCount === false)
        parseJson::error("Impossible de mettre à jour la requête du service connexe")->printJson();
    parseJson::success("La requête du service connexe a été mise à jour")->printJson();
}