<?php

require('../../inc/bootstrap.php');
$msg = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['upload'])) {
        if (isset($_POST['upload'])) {
            $image = $_FILES['image']['name'];
            $directoryName = 'images/';
            if (!is_dir($directoryName)) {
                mkdir($directoryName, 0755);
            }
            $directoryName = "images/" . basename($image);
            $insertToRoom = App::getListManagement()->addToRefList($_POST['image_text'], $directoryName);
            if (move_uploaded_file($_FILES['image']['tmp_name'], $directoryName)) {
                App::setFlashAndRedirect('success', 'pièce ajoutée avec succées', '../cardex/add');
            } else {
                App::setFlashAndRedirect('danger', 'Un problème est survenu lors de l\'ajout de pièce', '../cardex/add');
            }
        } elseif (isset($_POST['addCardex'])) {
            $name = $_POST['name'];
        }
    } else if (isset($_POST['edit'])) {
        if (!empty($_POST['id'] || $_POST['url_image'] || $_POST['image_text'])) {
            $url = $_POST['url_image'];
            $image_name = $_POST['image_text'];
            $id = $_POST['id'];
        } else {
            App::setFlashAndRedirect('danger', 'Un problème est survenu lors les données a modifier', 'edit');

        }


        $image = $_FILES['image']['name'];

        $directoryName = 'images/';
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755);
        }
        $directoryName = "images/" . basename($image);
        $listname = "roomsList";
        $updateRoom = App::getListManagement()->upRoom($listname, $_POST['image_text'], $directoryName, $id);
        if (move_uploaded_file($_FILES['image']['tmp_name'], $directoryName && $updateRoom)) {
            App::setFlashAndRedirect('success', 'pièce modifiée  avec succées', 'view');
        } else {
            App::setFlashAndRedirect('danger', 'Un problème est survenu lors les données a modifier', 'view');
        }

    } elseif (isset($_POST['view_room'])) {
        App::setFlashAndRedirect('success', 'Liste des pièces', 'view');
    } elseif (isset($_POST['action'])) {
        $id = $_POST['id'];
        $rommIsDeleted = App::getListManagement()->deleteFromListReferenceById($id);
        if ($rommIsDeleted == 1) {
            App::setFlashAndRedirect('success', 'pièce supprimée  avec succées', 'view');

        } else {
            App::setFlashAndRedirect('danger', 'Un problème est survenu lors les données a modifier', 'view');
        }
    }
} else
    App::setFlashAndRedirect('danger', 'errure lors du chargement de l\'image', '/');
require('../../inc/header_bo.php'); ?>

