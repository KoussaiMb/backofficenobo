﻿<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 09/07/2018
 * Time: 11:14
 */
require('../../inc/bootstrap.php');
$validator = new Validator($_GET);
$validator->is_id("id", "L'id est invalide", "L'id est introuvable");
if (!$validator->is_valid())
    App::redirect('view');
$romms = App::getListManagement()->getRoomById($_GET['id']);
if (!$romms) {
    App::setFlashAndRedirect('warning', "L'id est invalide", 'view');
}
require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading">
            modifier une pièce
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-4">
                <form method="post" action="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Nom de la pièce</label>
                        <input type="text" class="form-control" name="image_text" value="<?= $romms->value; ?>">
                    </div>
                    <div class="form-group">
                        <div><label>Image</label></div>
                        <?php echo "<img  src='".$romms->url."' >";?>
                         <div class="form-groupupImage">
                                <input type="file" name="image"  accept="image/*">
                            </div>
                    </div>
                    </br></br>
                    <div class="text-center">
                        <input type="hidden" name="id" value="<?= $_GET['id']; ?>">
                        <input type="hidden" name="url_image" value="<?= $romms->url; ?>">
                        <button type="submit" class="btn btn-success" name="edit" value="edit">Modifier</button>
                        <button type="submit" class="btn btn-danger" name="action" value="delete" onclick="return confirm('voulez-vous vraiment supprimer ?')"><span class="glyphicon glyphicon-remove"></span></button>
                    </div>
                </form>
                <a href="view"> <button  class="btn btn-info btn_retour"  name="action">Retour</button></a>
            </div>
        </div>
    </div>
<style>
    img{
        position: relative;
        width: 300px;
        height: 100px;
    }
    .form-groupupImage{
        margin-top: -50px;
        margin-left: 320px;
    }
    .text-center{
        position: absolute;
    }
    .btn_retour{
        margin-left: 130px;
    }
</style>
<?php require('../../inc/footer_bo.php');
