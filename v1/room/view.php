<?php
require('../../inc/bootstrap.php');
$romms = App::getListManagement()->getListReference("roomsList");
require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        Gestion des pièces
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-12 task-container">
                <?php foreach ($romms as $k => $v): ?>
                    <div class="col-xs-4 task-view">
                        <input type="hidden" name="id" value="<?= $v->id;?>">
                        <div class="col-xs-12">
                            <p><strong><?= $v->value; ?></strong></p>
                            <?php echo "<img  src='".$v->url."' >";?>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
    </div>
</div>
    <style>
        .task-view {
            padding: 20px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .task-view > div:hover {
            border: 1px solid grey;

        }
        .task-view img {
            margin-top: -9px;
            position: relative;
            width: 270px;
            height: 80px;
        }
        .task-view > div {
            background-color: #f0f0f5;
            border-radius: 3px;
            padding-top: 10px;
            height: 120px;
        }
        .task-view p:last-child {
            color: #e60073;
        }
        .task-container {
            overflow-y: scroll;
            margin-top: 2em;
            border: 2px solid #DBEED4;
            border-radius: 3px;
        }
    </style>
    <script>
        jQuery(document).ready(function() {
            let allTask = $('.task-view');
            let rol = $("input[name='rol'][value='1']");
            let no_rol = $("input[name='rol'][value='0']");
            let is_rolling = $("#is_rolling");
            $("#filter").on("change", function(){
                allTask.hide();
                if ($("#filter").val() === '1') {
                    rol.parent('.task-view').show();
                } else if ($("#filter").val() === '2'){
                    no_rol.parent('.task-view').show();
                } else {
                    allTask.show();
                }
            });
            $(".task-container").css('max-height', $(window).height() * 0.7);
            allTask.on('click', function () {
                window.location = 'edit?id=' + $(this).find("input[name='id']").val();
            })
        });
    </script>
<?php
require('../../inc/footer_bo.php');