<?php
require('../../inc/bootstrap.php');

$company = Session::getInstance()->read('company');

require('../../inc/header_bo.php');
?>

    <div class="panel panel-success">
        <div class="panel-heading">
            Ajout d'une nouvelle entreprise
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-3">
                <div class="alert alert-warning col-sm-12">
                    <p>LOGO</p>
                    <p>Taille maximum: <strong>5ko</strong></p>
                    <p>Extensions autorisées: <strong>png, jpg</strong></p>
                </div>
            </div>
            <div class="col-xs-6">
                <form method="post" action="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Dénomination sociale</label>
                        <input type="text" class="form-control" name="corporate_name" value="<?= empty($company) ? null : $company['corporate_name']; ?>" placeholder="D&L Services" id="denominationSociale" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nom commercial</label>
                        <input type="text" class="form-control" name="business_name" value="<?= empty($company) ? null : $company['business_name']; ?>" placeholder="Nobo" id="nomCommercial" required>
                    </div>
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <input type="file" name="logo" id="logo" required>
                    </div>
                    <div class="form-group">
                        <label for="name">N°SIREN</label>
                        <input type="text" class="form-control" name="siren" value="<?= empty($company) ? null : $company['siren']; ?>" placeholder="820890739" id="siren" required>
                    </div>
                    <div class="form-group">
                        <label for="name">N°SIRET</label>
                        <input type="text" class="form-control" name="siret" value="<?= empty($company) ? null : $company['siret']; ?>" placeholder="82089073900023" id="siret" required>
                    </div>
                    <div class="form-group">
                        <label for="sap">N°SAP</label>
                        <input type="text" class="form-control" name="sap" value="<?= empty($company) ? null : $company['sap']; ?>" placeholder="SAP820890739" id="sap" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Adresse</label>
                        <input type="text" class="form-control" name="address" value="<?= empty($company) ? null : $company['address']; ?>" placeholder="18 Boulevard Montmatre" id="address" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Zipcode</label>
                        <input type="text" class="form-control" name="zipcode" value="<?= empty($company) ? null : $company['zipcode']; ?>" placeholder="75009" id="zipcode" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Ville</label>
                        <input type="text" class="form-control" name="city" value="<?= empty($company) ? null : $company['city']; ?>" placeholder="Paris" id="city" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nom du président</label>
                        <input type="text" class="form-control" name="ceo_lastname" value="<?= empty($company) ? null : $company['ceo_lastname']; ?>" placeholder="Douat" id="ceo_lastname" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Prénom du président</label>
                        <input type="text" class="form-control" name="ceo_firstname" value="<?= empty($company) ? null : $company['ceo_firstname']; ?>" placeholder="Damien" id="ceo_firstname" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Description de l'entreprise</label>
                        <input type="text" class="form-control" name="description" value="<?= empty($company) ? null : $company['description']; ?>" placeholder="Service à la personne" id="description" required>
                    </div>
                    <div class="form-group">
                        <label for="logo">Signature du président de l'entreprise</label>
                        <input type="file" name="ceo_signature" id="signature" required>
                    </div>
                    <br>
                    <a href="view"><button type="submit" class="btn btn-default">Revenir à la gestion des entreprises</button></a>
                    <button type="submit" class="btn btn-success" name="action" value="add">Ajouter une entreprise</button>
                </form>
            </div>
        </div>
    </div>
<?php
require('../../inc/footer_bo.php');