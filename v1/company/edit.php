<?php
require('../../inc/bootstrap.php');

if (!isset($_GET['id']) || isset($_GET['id']) && !preg_match("/^[\d]+$/", $_GET['id']))
    App::redirect('view');

$db = App::getDB();
$company = $db->query("SELECT * FROM `company` WHERE `id` = ?", [$_GET['id']])->fetch();

if (!$company) {
    App::setFlashAndRedirect('warning', "L'id est invalide", 'view');
}

require('../../inc/header_bo.php');
?>

    <div class="panel panel-success">
        <div class="panel-heading">
            Modifier une entreprise
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-3">
                <div class="alert alert-warning col-sm-12">
                    <p>LOGO</p>
                    <p>Taille maximum: <strong>5ko</strong></p>
                    <p>Extensions autorisées: <strong>png, jpg</strong></p>
                </div>
            </div>
            <div class="col-xs-6">
                <form method="post" action="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Dénomination sociale</label>
                        <input type="text" class="form-control" name="corporate_name" value="<?= $company->corporate_name; ?>" id="denominationSociale">
                    </div>
                    <div class="form-group">
                        <label for="name">Nom commercial</label>
                        <input type="text" class="form-control" name="business_name" value="<?= $company->business_name; ?>" id="nomCommercial">
                    </div>
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <div class="valign-flex">
                            <input type="file" name="logo" id="logo">
                            <img class="img-logo" src="<?= $company->logo; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">N°SIREN</label>
                        <input type="text" class="form-control" name="siren" value="<?= $company->siren; ?>" id="siren">
                    </div>
                    <div class="form-group">
                        <label for="name">N°SIRET</label>
                        <input type="text" class="form-control" name="siret" value="<?= $company->siret; ?>" id="siret">
                    </div>
                    <div class="form-group">
                        <label for="sap">N°SAP</label>
                        <input type="text" class="form-control" name="sap" value="<?= $company->sap; ?>" id="sap">
                    </div>
                    <div class="form-group">
                        <label for="name">Adresse</label>
                        <input type="text" class="form-control" name="address" value="<?= $company->address; ?>" id="address">
                    </div>
                    <div class="form-group">
                        <label for="name">Zipcode</label>
                        <input type="text" class="form-control" name="zipcode" value="<?= $company->zipcode; ?>" id="zipcode">
                    </div>
                    <div class="form-group">
                        <label for="name">Ville</label>
                        <input type="text" class="form-control" name="city" value="<?= $company->city; ?>" id="city">
                    </div>
                    <div class="form-group">
                        <label for="name">Nom du président</label>
                        <input type="text" class="form-control" name="ceo_lastname" value="<?= $company->ceo_lastname; ?>" id="ceo_lastname">
                    </div>
                    <div class="form-group">
                        <label for="name">Prénom du président</label>
                        <input type="text" class="form-control" name="ceo_firstname" value="<?= $company->ceo_firstname; ?>" id="ceo_firstname">
                    </div>
                    <div class="form-group">
                        <label for="name">Description de l'entreprise</label>
                        <input type="text" class="form-control" name="description" value="<?= $company->description; ?>" id="description">
                    </div>
                    <div class="form-group">
                        <label for="signature">Signature du président de l'entreprise</label>
                        <div class="valign-flex">
                            <input type="file" name="ceo_signature" id="signature">
                            <img class="img-logo" src="<?= $company->ceo_signature; ?>">
                        </div>
                    </div>
                    <br>
                    <input type="hidden" name="id" value="<?= $_GET['id']; ?>">
                    <a href="view"><button type="submit" class="btn btn-default">Revenir à la gestion des entreprises</button></a>
                    <input type="hidden" name="logo" value="<?= $company->logo; ?>">
                    <input type="hidden" name="ceo_signature" value="<?= $company->ceo_signature; ?>">
                    <button type="submit" class="btn btn-success" name="action" value="edit">Modifier</button>
                    <button type="submit" class="btn btn-danger" name="action" value="delete" onclick="confirm('Attention, si cette entreprise est l\'entreprise configurée par défaut dans les paramètres, la configuration sera supprimée, voulez-vous continuer ?')">Supprimer</button>
                </form>
            </div>
        </div>
    </div>
<?php
require('../../inc/footer_bo.php');