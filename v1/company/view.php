<?php
require('../../inc/bootstrap.php');

$companies = App::getCompany()->getAll();

if (empty($companies))
    Session::getInstance()->setFlash('danger', 'Aucune entreprise n\'est présente dans la base de données');

require('../../inc/header_bo.php');

?>

    <div class="panel panel-success">
        <div class="panel-heading">
            Gestion des entreprises
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-xs-12 text-center">
                <div class="col-xs-1 text-right">
                    <a href="add"><button type="button" class="btn btn-success">Ajouter une entreprise</span></button></a>
                </div>
            </div>
            <div class="col-xs-12 company-container">
                <?php
                if(!empty($companies)){
                foreach ($companies as $k => $v):

                    ?>
                    <div class="col-xs-4 company-view">
                        <input type="hidden" name="id" value="<?= $v->id;?>">
                        <div class="col-xs-12">
                            <div class="col-xs-4">
                                <img class="img-responsive" src="<?= $v->logo; ?>">
                            </div>
                            <div class="col-xs-8">
                                <p><strong><?= $v->corporate_name; ?></strong>
                                <p>
                                    <?= $v->business_name;?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; }?>
            </div>
        </div>
    </div>
    <style>
        .company-view {
            padding: 20px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .company-view > div:hover {
            border: 1px solid grey;

        }
        .company-view > div {
            background-color: #f0f0f5;
            border-radius: 3px;
            padding-top: 10px;
            height: 80px;
        }
        .company-view p:last-child {
            color: #e60073;
        }
        .company-container {
            overflow-y: scroll;
            margin-top: 2em;
            border: 2px solid #DBEED4;
            border-radius: 3px;
        }
    </style>
    <script>
        jQuery(document).ready(function() {
            allCompany = $('.company-view');
            $(".company-container").css('max-height', $(window).height() * 0.7);

            allCompany.on('click', function () {
                window.location = 'edit?id=' + $(this).find("input[name='id']").val();
            })
        });
    </script>
<?php
require('../../inc/footer_bo.php');