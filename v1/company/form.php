<?php
include_once('../../inc/bootstrap.php');

if (empty($_POST['action'])){
    App::redirect('view');

}else if ($_POST['action'] == 'add' ){
    $validator = new Validator($_POST);

    $validator->no_symbol('corporate_name', 'La dénomination sociale est invalide', 'Vous avez oublié la dénomination sociale de l\'entreprise');
    $validator->is_len('corporate_name', null, 50, 'Le nom entré est trop long');

    $validator->no_symbol('business_name', 'Le nom commercial n\'est pas valide', 'Vous avez oublié d\'indiquer le nom commercial de l\'entreprise');
    $validator->is_len('business_name', null, 50, 'Le nom entré est trop long');

    $validator->is_num('siren', 'Le SIREN n\'est pas valide', 'Vous avez oublié d\'indiquer le SIREN de l\'entreprise', 9);
    $validator->is_num('siret', 'Le SIRET n\'est pas valide', 'Vous avez oublié le numéro SIRET de l\'entreprise', 14);
    $validator->is_alphanum('sap', 'Le numéro SAP n\'est pas valide', 'Vous avez oublié de renseigner le numéro SAP de l\'entreprise', 15);
    $validator->no_symbol('address', 'L\'adresse n\'est pas valide', 'Vous avez oublié de renseigner l\'adresse de l\'entreprise');
    $validator->is_num('zipcode', 'Le zipcode n\'est pas valide', 'Vous avez oublié de renseigner le zipcode de l\'entreprise', 6);
    $validator->is_alpha('city', 'La ville n\'est pas valide', 'Vous avez oublié de renseigner la ville de l\'entreprise');
    $validator->is_alpha('ceo_lastname', 'Le nom du président de l\'entreprise n\'est pas valide', 'Vous avez oublié de renseigner le nom du président de l\'entreprise');
    $validator->is_alpha('ceo_firstname', 'Le prénom du président de l\'entreprise n\'est pas valide', 'Vous avez oublié de renseigner le prénom du président de l\'entreprise');

    $validator->no_symbol('description', 'La description contient des caractères interdits', 'Vous n\'avez pas renseigné de description');
    $validator->is_len('description', null, 100, 'Le nom entré est trop long');

    Session::getInstance()->write('company', $_POST);

    if ($validator->is_valid()){
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/nobo/logo/company/signature/')) {
            $success_create = mkdir($_SERVER['DOCUMENT_ROOT'] . '/img/nobo/logo/company/signature/', 0757, true);
            if(!$success_create){
                App::setFlashAndRedirect("danger", "Un problème est survenu lors de la création du dossier", "add");
            }
        }
        $trim_symbols = parseStr::cleanSymbols($_POST['business_name']);
        $trim_business_name = strtolower(str_replace(' ', '-', $trim_symbols));

        $logo_url = $validator->is_logo_uploaded($_FILES['logo'], $trim_business_name, '/img/nobo/logo/company/', null, ['png', 'jpg']);
        $signature_url = $validator->is_logo_uploaded($_FILES['ceo_signature'], $trim_business_name.'_signature', '/img/nobo/logo/company/signature/', null, ['png', 'jpg']);
    }

    if (!$validator->is_valid() || empty($logo_url) || empty($signature_url)){
        App::array_to_flash($validator->getErrors(), "add");
    }

    $ret = App::getCompany()->addCompany($_POST, $logo_url, $signature_url);
    if(!$ret){
        App::setFlashAndRedirect("danger", "Un problème est survenu lors de l'ajout de l'entreprise", "add");
    }else{
        Session::getInstance()->delete('company');
        App::setFlashAndRedirect("success", "L'entreprise a bien été ajoutée", "view");
    }
} else if ($_POST['action'] == 'edit'){
    $validator = new Validator($_POST);

    $validator->no_symbol('corporate_name', 'La dénomination sociale est invalide', 'Vous avez oublié la dénomination sociale de l\'entreprise');
    $validator->is_len('corporate_name', null, 50, 'Le nom entré est trop long');

    $validator->no_symbol('business_name', 'Le nom commercial n\'est pas valide', 'Vous avez oublié d\'indiquer le nom commercial de l\'entreprise');
    $validator->is_len('business_name', null, 50, 'Le nom entré est trop long');

    $validator->is_num('siren', 'Le SIREN n\'est pas valide', 'Vous avez oublié d\'indiquer le SIREN de l\'entreprise', 9);
    $validator->is_num('siret', 'Le SIRET n\'est pas valide', 'Vous avez oublié le numéro SIRET de l\'entreprise', 14);
    $validator->is_alphanum('sap', 'Le numéro SAP n\'est pas valide', 'Vous avez oublié de renseigner le numéro SAP de l\'entreprise', 15);
    $validator->no_symbol('address', 'L\'adresse n\'est pas valide', 'Vous avez oublié de renseigner l\'adresse de l\'entreprise');
    $validator->is_num('zipcode', 'Le zipcode n\'est pas valide', 'Vous avez oublié de renseigner le zipcode de l\'entreprise', 6);
    $validator->is_alpha('city', 'La ville n\'est pas valide', 'Vous avez oublié de renseigner la ville de l\'entreprise');
    $validator->is_alpha('ceo_lastname', 'Le nom du président de l\'entreprise n\'est pas valide', 'Vous avez oublié de renseigner le nom du président de l\'entreprise');
    $validator->is_alpha('ceo_firstname', 'Le prénom du président de l\'entreprise n\'est pas valide', 'Vous avez oublié de renseigner le prénom du président de l\'entreprise');

    $validator->no_symbol('description', 'La description contient des caractères interdits', 'Vous n\'avez pas renseigné de description');
    $validator->is_len('description', null, 100, 'Le nom entré est trop long');

    if (isset($_FILES['logo']) && $_FILES['logo']['size'] > 0 && $validator->is_valid()){
        $trim_symbols = parseStr::cleanSymbols($_POST['business_name']);
        $trim_business_name = strtolower(str_replace(' ', '-', $trim_symbols));

        $logo_url = $validator->is_logo_uploaded($_FILES['logo'], $trim_business_name, '/img/nobo/logo/company/', null, ['png', 'jpg']);
    }else {
        $logo_url = $_POST['logo'];
    }

    if (isset($_FILES['ceo_signature']) && $_FILES['ceo_signature']['size'] > 0 && $validator->is_valid()){
        $trim_symbols = parseStr::cleanSymbols($_POST['business_name']);
        $trim_business_name = strtolower(str_replace(' ', '-', $trim_symbols));

        $signature_url = $validator->is_logo_uploaded($_FILES['ceo_signature'], $trim_business_name, '/img/nobo/logo/company/signature/', null, ['png', 'jpg']);
    }else {
        $signature_url = $_POST['ceo_signature'];
    }

    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), "edit?id=" . $_POST['id']);

    $ret = App::getCompany()->editCompany($_POST, $logo_url, $signature_url);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'La modification de l\'entreprise a échoué', "edit?id=" . $_POST['id']);
    else
        App::setFlashAndRedirect("success", "L'entreprise a bien été modifiée", "view");

} else if ($_POST['action'] == 'delete'){

    $validator = new Validator($_POST);
    $validator->is_id('id', 'Id invalide', 'Id non renseigné');

    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', "Id invalide", 'view');

    $companyClass = App::getCompany();
    $curr = $companyClass->getCompanyConf();

    if($curr->id != 0 && $curr->value == $_POST['id']){
        $companyClass->deleteConf($curr->id);
    }

    $company = App::getCompany()->getCompanyById($_POST['id']);
    if(!$company){
        App::setFlashAndRedirect('danger', 'Impossible de récupérer l\'entreprise à supprimer', "edit?id=" . $_POST['id']);
    }

    $logo_url = $company->logo;
    $signature = $company->ceo_signature;

    if (file_exists($logo_url)) {
        unlink($logo_url);
    } else {
        Session::getInstance()->setFlash('danger', 'Impossible de récupérer l\'url du logo de l\'entreprise');
    }

    if (file_exists($signature)) {
        unlink($signature);
    } else {
        Session::getInstance()->setFlash('danger', 'Impossible de récupérer l\'url de la signature à supprimer');
    }

    $ret = $companyClass->deleteCompany($_POST);
    if ($ret == 0)
        App::setFlashAndRedirect('danger', 'La suppression de l\'entreprise a échoué', "edit?id=" . $_POST['id']);
    else
        App::setFlashAndRedirect('success', 'L\'entreprise a bien été supprimée', "view");
}

App::redirect("view");
