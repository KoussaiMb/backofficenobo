<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');
$fullcalendar = App::getFullCalendar();

$db = App::getDB();

if ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    $validator->is_email('email', 'Email invalide');
    if ($validator->is_valid() == true)
        $validator->is_uniq($db, 'user', 'email', 'email', 'Email déjà utilisé');
    $validator->no_symbol('lastname', 'Le nom n\'est pas valide');
    $validator->no_symbol('firstname', 'Le prénom n\'est pas valide');
    $validator->is_phone('phone', 'Mauvais format du téléphone');
    $validator->is_id('gender_id', 'Problème au niveau du genre');
    $validator->is_id('convention_id', 'Le contrat ne semble pas valide', 'Selectionnez un contrat');
    Session::getInstance()->write('add-provider', $validator->getData());
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("add");
   }
    $user_token = App::getUser()->addUser($_POST, 'provider');
    if (!$user_token)
        App::setFlashAndRedirect('danger', "Une erreur est survenue lors de l'ajout du prestataire", "add");
    Session::getInstance()->delete('add-provider');
    App::setFlashAndRedirect('success', "Veuillez éditer le prestataire que vous avez créé ", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'delete') {
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le prestataire n\'est pas valide', 'Le prestataire n\'est pas renseigné');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getError('user_token'), "edit?user_token=" . $user_token);
    if (App::getUser()->deleteUserByToken($user_token))
        App::setFlashAndRedirect('warning', "Le prestataire a bien été supprimé", "view");
    App::setFlashAndRedirect('danger', "Le prestataire n'a pas pu être supprimé", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'edit') {
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le token est invalide', 'Le token est manquant');
    $validator->is_id('gender_id', 'Le genre n\'est pas valide', 'Le genre n\'est pas défini');
    $validator->is_alpha('contract', 'Le contract n\'est pas valide');
    $validator->is_id('convention_id', 'La convention n\'est pas valide', 'La convention n\'est pas défini');
    $validator->no_symbol('lastname', 'Le nom n\'est pas valide');
    $validator->no_symbol('firstname', 'Le prénom n\'est pas valide');
    $validator->is_phone('phone', 'Mauvais format du téléphone');
    $validator->is_date_bo('birthdate', 'Format date de naissance invalide');
    $validator->is_email('email', 'Email invalide');
    $user = App::getUser()->getUserByToken($user_token);
    $_POST['user_id'] = $user->id;
    if ($user == null)
        $validator->throwException('system', 'impossible de vérifier l\'email');
    $validator_email = new Validator(['email' => $_POST['email']]);
    $validator_email->is_uniq($db, 'user', 'email', 'email', 'existe déjà');
    if ($validator->is_valid() && $user->email != $_POST['email'] && !$validator_email->is_valid())
        $validator->throwException('email', 'l\'adresse email entrée existe déjà');
    if (!$validator->is_valid()){
        App::array_to_flash($validator->getErrors());
        App::redirect("edit?user_token=" . $user_token);
    }
    $validator->is_photo_profile('photo_profile', 1000000, array('png', 'jpg'));
    $path = "../../upload/$user_token/img/";
    $photo_profile = $user->photo_profile;
    if ($validator->is_valid()){
        App::createPath($path, 757);
        $new_photo = parseStr::GenPhotoName($_FILES['photo_profile']['name'], my_crypt::genToken(6));
        if (move_uploaded_file($_FILES['photo_profile']['tmp_name'], $path.$new_photo)) {
            if ($photo_profile)
                App::getUser()->deletePhotoByToken($user_token);
            $photo_profile = $path.$new_photo;
        }
        else
            Session::getInstance()->setFlash('danger', 'Problème d\'upload avec la photo de profil');
    }
    $_POST['photo_profile'] = $photo_profile;
    if (!App::getUser()->updateUserAndProvider($_POST))
        App::setFlashAndRedirect('danger', "Les informations du prestataire n'ont pas pu être modifiés", "edit?user_token=" . $user_token);
    if (empty($_POST['address']))
        App::setFlashAndRedirect('success', "Le prestataire a bien été modifié", "edit?user_token=" . $user_token);
    if (empty($_POST['address_id']))
        $ret = App::getAddress()->addAddress($_POST);
    else
        $ret = App::getAddress()->updateAddressById($_POST);
    if (!$ret)
        App::setFlashAndRedirect('danger', "L'adresse du prestataire n'a pas pu être modifiée", "edit?user_token=" . $user_token);
    App::setFlashAndRedirect('success', "Le prestataire a bien été modifié", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'reload_password')
{
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le prestataire n\'est pas valide', 'Le prestataire est introuvable');
    $validator->is_email('email', 'L\'email n\'est pas valide', 'L\'email est introuvable');
    $provider = App::getUser()->getUserByToken($_POST['user_token']);
    if ($validator->is_valid() && $_POST['email'] != $provider->email)
        $validator->throwException('email', 'L\'email de ne correspond pas au prestataire');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect("edit?user_token=" . $user_token);
    }
    if (App::getAuth()->forget($_POST['email']))
        App::setFlashAndRedirect('success', "Un nouveau mot de passe a été envoyé au prestataire", "edit?user_token=" . $user_token);
    App::setFlashAndRedirect('danger', "Le nouveau mot de passe n'a pas été envoyé", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'delete_photo')
{
    $user_token = $_POST['user_token'];
    $id = App::getUser()->getUserByToken($user_token);
    $ret = App::getUser()->deletePhotoById($id);

    if (!empty($ret))
        Session::getInstance()->setFlash('success', "La photo de profil du prestataire a été supprimée");
    else
        Session::getInstance()->setFlash('danger', "La photo de profil du prestataire n'a pas été supprimée");
    App::redirect("edit?user_token=" . $user_token);
}
App::redirect('view');