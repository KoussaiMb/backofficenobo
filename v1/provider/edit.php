<?php
require_once('../../inc/bootstrap.php');

Session::getInstance()->upLog('provider_checked');
$db = App::getDB();
$userClass = App::getUser();

if (!empty($_GET['user_token']) && preg_match('/^[a-f\d]+$/', $_GET['user_token']))
    $user = $userClass->getProviderByToken($_GET['user_token']);
if (empty($user))
    App::setFlashAndRedirect('danger', 'Prestataire inconnu au régiment', 'view');
$address = $userClass->getAddressHomeByUserToken($_GET['user_token']);
$convention = App::getConf()->getAllConvention();
$conf = App::getConf()->getAll();
$promo_autocomplete = App::getPromoCode()->getAutocompletePromocode();
$maritalStatus_list = App::getListManagement()->getList('maritalStatus');
$gender_list = App::getListManagement()->getList('Gender');
$contract_list = App::getProvider()->getContractsAndCurrentContract($user->contract);

if (isset($_POST['menu_id']))
    $menu_id = substr($_POST['menu_id'], -1);
else
    $menu_id = '1';
require_once('../../inc/header_bo.php');
?>


<div class="panel panel-success">
    <div class="panel-heading">
        <div style="min-height: 30px">
            <div class="col-sm-3">
                <img src="<?= !empty($user->photo_profile) ? $user->photo_profile :'/img/user/avatar_femme.jpg'; ?>" alt="Photo de profil" class="img-circle" style="border: 2px solid #39D2B4;" width="30px" height="30px">
                <input type="hidden" name="user_token" value="<?= $_GET['user_token'];?>"/>
                <div class="btn-group btn-drop-user">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $user->firstname . ' ' . $user->lastname; ?> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <form method="post" action="form" class="text-right">
                            <input type="hidden" name="user_token" value="<?= $user->user_token; ?>">
                            <li><button type="button" name="action" value="reload_password" class="btn btn-sm" id="reload_password">Réinitialiser le mot de passe</button></li>
                            <li><button name="action" value="delete_photo" class="btn btn-sm">Supprimer la photo de profil</button></li>
                            <li role="separator" class="divider"></li>
                            <li><button class="btn btn-sm text-danger" name="action" value="delete" onclick="return confirm('Ceci supprimera de manière définitive le prestataire sélectionné')">Supprimer le prestataire</button></li>
                        </form>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">
                <ul class="nav nav-pills" id='tabs_provider' style="display: flex; justify-content: space-around;">
                    <input type="hidden" value="<?= $menu_id; ?>" id="get_menu_id" disabled>
                    <li id="tab1"><a data-toggle="tab" href="#menu1">Profil</a></li>
                    <li id="tab3"><a data-toggle="tab" href="#menu4">Missions</a></li>
                    <li id="tab4"><a data-toggle="tab" href="#menu5">Disponibilités</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <!--Profil-->
            <div id="menu1" class="tab-pane fade">
                <form id="form_profile" enctype="multipart/form-data" method="post" action="form">
                    <input type="hidden" name="who" value="provider">
                    <input type="hidden" name="user_token" value="<?= $user->user_token; ?>"/>
                    <div class="col-sm-12">
                        <div class="col-sm-6 SpaceTop">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                    <select class="form-control input-sm" name="gender_id">
                                        <?php foreach ($gender_list as $k => $v): ?>
                                            <option value="<?= $v->id;?>" <?= $v->id == $user->gender_id ? 'selected=1' : '';?>><?= $v->field; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="text" class="form-control input-sm" name="firstname" value="<?= $user->firstname; ?>" placeholder="Prénom" required/>
                                    <input type="text" class="form-control input-sm" name="lastname" value="<?= $user->lastname; ?>" placeholder="Nom" required/>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm text-right" name="phone" value="<?= $user->phone; ?>" placeholder="Téléphone" required/>
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control pickadate input-sm text-right" name="birthdate" value="<?= $user->birthdate; ?>" placeholder="Date de naissance" id="birthdate"/>
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                </div>
                                <div class="input-group">
                                    <input type="file" name="photo_profile" style="display: none">
                                    <button type="button" class="btn btn-default btn-sm" style="width:100%" onclick="$(this).prev().trigger('click')">Upload une photo</button>
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                    <input type="text" class="form-control input-sm text-center" name="email" value="<?= $user->email; ?>" placeholder="email" required/>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input-group-span">Enfants</span>
                                    <input type="number" class="form-control input-sm" min="0" step="1" max="10" name="childNb" value="<?= $user->childNb; ?>" placeholder="Nombre d'enfants">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input-group-span">Etat civil</span>
                                    <label class="sr-only" for="maritalStatus"></label>
                                    <select class="form-control input-sm" name="maritalStatus" id="maritalStatus">
                                        <?php foreach ($maritalStatus_list as $k => $v): ?>
                                            <option value="<?= $v->id;?>" <?= $v->id == $user->maritalStatus_id ? 'selected' : '';?>><?= $v->field; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input-group-span">Contrat</span>
                                    <label class="sr-only" for="contract"></label>
                                    <select class="form-control input-sm" name="contract" id="contract">
                                        <option value="">Aucun contrat</option>
                                        <?php foreach ($contract_list as $contract): ?>
                                            <option value="<?= $contract['name']; ?>" <?= $contract['selected']; ?>><?= $contract['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input-group-span">Convention</span>
                                    <label class="sr-only" for="convention_id"></label>
                                    <select class="form-control input-sm" name="convention_id" id="convention_id">
                                        <option value="">Aucune</option>
                                        <?php foreach ($convention as $k): ?>
                                            <option value="<?= $k->id;?>" <?= $user->convention_id == $k->id ? 'selected' : '';?>><?= $k->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="margin-top: 23px;">
                            <input type="hidden" name="tab" value="profile"/>
                            <div class="col-sm-12">
                                <?php if (!empty($user->password_asked_at)): ?>
                                    <div class="col-sm-6">
                                        <div class="alert alert-info">Un password temporaire a été généré le <?= $user->password_asked_at; ?></div>
                                    </div>
                                    <hr class="col-sm-11 hrBo">
                                <?php elseif(!empty($user->reset_asked_at)): ?>
                                    <div class="col-sm-6">
                                        <div class="alert alert-info">Un token temporaire a été demandé et envoyé le <?= $user->reset_asked_at; ?></div>
                                    </div>
                                    <hr class="col-sm-11 hrBo">
                                <?php endif; ?>
                            </div>
                            <input type="hidden" name="lat" value="<?= $address ? $address->lat : null; ?>" id="lat_home" readonly>
                            <input type="hidden" name="lng" value="<?= $address ? $address->lng : null; ?>" id="lng_home" readonly>
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                                <input type="text" class="form-control input-sm" id="autocomplete_home">
                                <div class="input-group-btn"><button type="button" class="btn btn-default btn-sm" onclick="$('#home_security').trigger('click');">Remove security</button></div>
                            </div>
                            <br/>
                            <input type="hidden" class="form-control input-sm" name="street_number" id="street_number_home" disabled>
                            <input type="hidden" class="form-control input-sm" name="route" id="route_home" disabled>
                            <input type="text" class="form-control input-sm" name="address" value="<?= $address ? $address->address : null; ?>" placeholder="Adresse" id="address_home" readonly>
                            <input type="text" class="form-control input-sm" name="address_ext" value="<?= $address ? $address->address_ext : null; ?>" placeholder="Region" id="administrative_area_level_1_home" readonly>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm" name="zipcode" value="<?= $address ? $address->zipcode : null; ?>" placeholder="Code postal" id="postal_code_home" readonly>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm" name="city" value="<?= $address ? $address->city : null; ?>" placeholder="Ville" id="locality_home" readonly>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm" name="country" value="<?= $address ? $address->country : null; ?>" placeholder="Pays" id="country_home" readonly>
                            </div>
                            <input type="hidden" id="home_security" onclick="$(this).prevUntil('br').attr('readonly', function(_, attr){return !attr});$(this).prevUntil('br').children().attr('readonly', function(_, attr){return !attr});">
                            <input type="hidden" name="home" value="1">
                            <input type="hidden" name="address_id" value="<?= $address ? $address->id : null; ?>">
                        </div>
                    </div>
                    <div class="col-xs-12 SpaceTop">
                        <div class="col-sm-3 col-sm-offset-3"">
                            <label for="level">level</label>
                            <div class="input-group">
                                <div class="input-group-addon"><span class="glyphicon glyphicon-flash"></span></div>
                                <input type="text" class="form-control input-sm" value="<?= $user->level; ?>" id="level" disabled/>
                            </div>
                        </div>
                        <div class="col-sm-3 text-right"">
                            <label for="workTime">Temps total de travail</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm text-right" value="<?= $user->workTime; ?>" id="workTime" disabled/>
                                <div class="input-group-addon"><span class="glyphicon glyphicon-time"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <hr class="col-sm-11 hrBo">
                        <button class="btn btn-info btn-sm" name="action" value="edit">Sauvegarder les changements</button>
                    </div>
                </form>
            </div>
            <!--Prestations-->
            <div id="menu4" class="tab-pane fade">
                <div class="col-sm-12">
                    <div id="calendar_planning"></div>
                </div>
            </div>
            <!--Disponibilités-->
            <div id="menu5" class="tab-pane fade">
                <div class="col-sm-12">
                    <div id="calendar"></div>
                </div>
            </div>
            <!--Modal_AGENDA-->
            <div id="modal_agenda" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                            <h4 id="modal_agenda_Title" class="modal-title"></h4>
                        </div>
                        <div id="modal_agenda_Body" class="modal-body">
                            <div class="info_event"></div>
                            <div><h4>Option de suppression :</h4><br>
                                <span style="font-size: 13px;font-weight: bold;">Voulez vous mettre en attente ou supprimer tous les créneaux prévisionnel ?</span>
                            </div>
                            <div class="radio option_agenda_1">
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option_agenda_1" value="pending" checked>Mettre en attente</label>
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option_agenda_1" value="delete" >Supprimer</label>
                            </div>
                            <div class="info_agenda"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <button type="button" class="btn btn-warning" data-href="#" data-toggle="modal" id="confirm-edit-agenda">Mettre le créneau en attente</button>
                            <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-edit-all-agenda">Mettre tout les créneaux liés à cette adresse en attente</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modal_PLANNING-->
            <div id="modal_planning" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                            <h4 id="modal_planning_Title" class="modal-title"></h4>
                        </div>
                        <div id="modal_planning_Body" class="modal-body">
                            <!--
                            <div><h4>Option de suppression :</h4><br>
                                <span style="font-size: 13px;font-weight: bold;">Voulez vous mettre en attente ou supprimer ?</span>
                            </div>
                            -->
                            <div class="radio option_planning_1 hidden">
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option_planning_1" value="pending">Mettre en attente</label>
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option_planning_1" value="delete" checked>Supprimer</label>
                            </div>
                            <div class="info_agenda"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-href="#" data-toggle="modal" id="confirm-validate-mission">Valider la mission</button>
                            <!--
                            <button type="button" class="btn btn-warning" data-href="#" data-toggle="modal" id="confirm-edit-planning">Mettre la mission en attente</button>
                            <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-edit-all-planning">Mettre toute les missions suivantes en attente</button>
                            -->
                            <button type="button" class="btn btn-warning" data-href="#" data-toggle="modal" id="confirm-edit-planning">Supprimer cette mission</button>
                            <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-edit-all-planning">Supprimer les missions suivantes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modal_MISSION-->
            <div id="modal_mission" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modal_Dispo-->
            <div id="modal_dispo" class="modal fade">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                            <h4 id="modal_dispo_Title" class="modal-title"></h4>
                        </div>
                        <div id="modal_dispo_Body" class="modal-body">
                            <div><h4>Option de suppression :</h4><br>
                                <span style="font-size: 13px;font-weight: bold;">Voulez vous mettre en attentes ou supprimer toutes les missions de ce créneau ?</span>
                            </div>
                            <div class="radio option_dispo">
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option" value="pending" checked>Mettre en attente</label>
                                <label style="font-size: 12px" class="radio-inline"><input type="radio" name="optradio_option" value="delete" >Supprimer</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-delete-dispo-all">Supprimer le créneau de disponibilité</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBeMne5-vmGGRYsdcCxDb-PYs3McbjVmCg&libraries=places&callback=initialize" async defer></script>
<script>
    var placeSearch, autocomplete_home;
    var autocomplete_promo;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function fillInAddr_home() {
        var id = 'home';
        var place = autocomplete_home.getPlace();
        for (var component in componentForm) {
            document.getElementById(component+ '_' + id).value = '';
        }
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType + '_' + id).value = val;
            }
        }
        $('#lat_' + id).val(place.geometry.location.lat());
        $('#lng_' + id).val(place.geometry.location.lng());
        $('#address_' + id).val($('#street_number_' + id).val() + ' ' + $('#route_' + id).val());
    }

    function initialize() {
        initGoogleAutocomplete_home('autocomplete_home');
    }

    jQuery(document).ready(function() {
        var token = '<?php echo $_GET['user_token'] ?>';
        var conf = '<?php echo json_encode($conf) ?>';

        //Gestion des onglets
        var menu_id = $('#get_menu_id').val();
        $('#tab' + menu_id).addClass('active');
        $('#menu' + menu_id).addClass('in active');


        // toggle les bouton des option de suppression / mise en attente de l'agenda
        $('.option_agenda_1').on('change', function(){
            var option = $('input[name=optradio_option_agenda_1]:checked').val();

            if (option === "pending"){
                $('#confirm-edit-agenda').text("Mettre le créneau en attente");
                $('#confirm-edit-all-agenda').text("Mettre tout les créneaux liés à cette adresse en attente");
            }
            else if (option === "delete"){
                $('#confirm-edit-agenda').text("Supprimer le créneau");
                $('#confirm-edit-all-agenda').text("Supprimer tout les créneaux liés à cette adresse");
            }
        });

        $('.option_planning_1').on('change', function(){
            var option = $('input[name=optradio_option_planning_1]:checked').val();

            if (option == "pending"){
                $('#confirm-edit-planning').text("Mettre la mission en attente");
                $('#confirm-edit-all-planning').text("Mettre toutes les missions suivantes en attente");
            }
            else if (option == "delete"){
                $('#confirm-edit-planning').text("Supprimer la mission");
                $('#confirm-edit-all-planning').text("Supprimer toutes les missions suivantes");
            }
        });

        /* Handler when changing the promotype with the select*/
        $(document).on('change', '.promo_type_class', function(){
            var form_promo_data = $('#form_promo').serialize().split('&');
            var type = form_promo_data[0].split('=')[1];

            if(type === 'no_discount'){
                $('.promo_value_class').addClass('hidden');
            }else if(type === 'promocode') {
                $('.promo_value_class').removeClass('hidden');
                $('#promo_value_id').devbridgeAutocomplete({
                    lookup: <?php if (!empty($promo_autocomplete)) {
                    echo $promo_autocomplete;
                } else {
                    echo "[]";
                }?>,
                    maxHeight: 200,
                    onSelect: function (suggestion) {
                        $('#promo_value_id').val(suggestion.value);
                    }
                });
            }else{
                $('.promo_value_class').removeClass('hidden');
            }
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if ($(this).attr('href') == "#menu4"){
                $('#calendar_planning').fullCalendar('destroy');
                calendar_planning_provider(token, conf);
                $('#calendar_planning').fullCalendar('render');
            }
            else if ($(this).attr('href') == "#menu5") {
                $('#calendar').fullCalendar('destroy');
                calendar_dispo_provider(token);
                $('#calendar').fullCalendar('render');
            }
        });

        $("#reload_password").on("click", function () {
            bootbox.prompt({
                size: "small",
                title: "Réinitialiser le mot de passe",
                onEscape: true,
                backdrop: true,
                callback: function(result){
                    if (result !== null) {
                        ajax_call(
                            "api/password",
                            "PUT",
                            {password : result, user_token : token},
                            function (ret) {
                                bo_notify(ret.message, ret.data.message_color)
                            }, function () {
                                bo_notify("Un problème est survenu", "danger")
                            }
                        )
                    }
                }
            })
        });
    });

    /* Handler when changing the promotype with the select*/
    $(document).on('change', '.promo_type_class', function(){
        var form_promo_data = $('#form_promo').serialize().split('&');
        var type = form_promo_data[0].split('=')[1];

        if(type === 'no_discount'){
            $('.promo_value_class').addClass('hidden');
        }else if(type === 'promocode') {
            $('.promo_value_class').removeClass('hidden');
            $('#promo_value_id').devbridgeAutocomplete({
                lookup: <?php if (!empty($promo_autocomplete)) {
                echo $promo_autocomplete;
            } else {
                echo "[]";
            }?>,
                maxHeight: 200,
                onSelect: function (suggestion) {
                    $('#promo_value_id').val(suggestion.value);
                }
            });
        }else {
            $('.promo_value_class').removeClass('hidden');
        }
    });

</script>
<script src="/js/edit_provider.js"></script>
<script src="/js/app.js"></script>

<?php
require('../../inc/footer_bo.php');