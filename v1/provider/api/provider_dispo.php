<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 13/09/2017
 * Time: 17:00
 */

require_once ('../../../inc/bootstrap.php');

$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['token']) && isset($_POST['day']) && isset($_POST['start']) && isset($_POST['end'])) {
        $event = array (
            "token" => $_POST['token'],
            "day" => $_POST['day'],
            "start" => $_POST['start'],
            "end" => $_POST['end']
        );

        $ret = $fullcalendar->addDispoProvider($event);
        if($ret === false)
            parseJson::error('Erreur lors de la création de votre créneau')->printJson();
        parseJson::success('Le créneau a bien été crée')->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = App::get_instance_validator($_GET);
    $weekday = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $json_data = [];

    $validator->isToken('user_token', 'Le prestataire est invalide', 'Le prestataire est introuvable');
    if (!$validator->is_valid())
        parseJson::error('Les informations pour récupérer les disponibilités sont corompues', $validator->getErrors())->printJson();

    $dispoProvider = $fullcalendar->getDispoProvider($_GET);
    if ($dispoProvider === false)
        parseJson::error('Erreur lors de la récupération des disponibilité')->printJson();

    if (count($dispoProvider) == 0) {
        for ($i = 1; $i <= 5; $i++) {
            $event = array (
                "token" => $_GET['user_token'],
                "day" => $i,
                "start" => "08:00:00",
                "end" => "19:00:00",
            );

            $dispo_id = $fullcalendar->addDispoProvider($event);
            if ($dispo_id === false)
                parseJson::error('Erreur lors de l\'ajout de la disponibilité client')->printJson();

            $start_time = explode(":", $event["start"]);
            $end_time = explode(":", $event["end"]);
            $nextday = date('Y-m-d H:i:s', strtotime('next '.$weekday[$i], strtotime('previous sunday')));
            $startTime = date('Y-m-d H:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
            $endTime = date('Y-m-d H:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
            array_push($json_data, array("start" => $startTime, "end" => $endTime, "id" => $dispo_id));
        }
    } else {
        foreach ($dispoProvider as $key => $value){
            $start_time = explode(":", $value->start);
            $end_time = explode(":", $value->end);
            $nextday = date('Y-m-d H:i:s', strtotime('next '.$weekday[$value->day], strtotime('previous sunday')));
            $startTime = date('Y-m-d H:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
            $endTime = date('Y-m-d H:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
            array_push($json_data, array("start" => $startTime, "end" => $endTime, "id" => $value->id));
        }
    }
    parseJson::success('Les disponibilitées on bien été récuperées', $json_data)->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    if (isset($_PUT['start']) && isset($_PUT['end']) && isset($_PUT['id']) && isset($_PUT['day'])) {
        $event = array(
            "start" => $_PUT['start'],
            "end" => $_PUT['end'],
            "id" => $_PUT['id'],
            "day" => $_PUT['day']
        );

        $return = $fullcalendar->updateDispoProvider($event);
        if($return === false){
            $json = parseJson::error('Erreur lors de l\'édition du créneau');
            $json->printJson();
        }
        $json = parseJson::success('Le créneau a bien été édité');
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    if (isset($_DELETE['provider_token']) && isset($_DELETE['id']) && isset($_DELETE['del_option']) && isset($_DELETE['day'])) {

        $data = [
            "id" => $_DELETE['id'],
            "provider_token" => $_DELETE['provider_token'],
            "del_option" => $_DELETE['del_option'],
            "day" => $_DELETE['day']
        ];
        $return = $fullcalendar->delDispoProvider($data);
        if($return === false){
            $json = parseJson::error('Erreur lors de la suppression du créneau');
            $json->printJson();
        }
        $json = parseJson::success('Le créneau a bien été supprimer', $_DELETE['id']);
        $json->printJson();
    }
}


