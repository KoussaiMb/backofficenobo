<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 09/11/2017
 * Time: 10:56
 */

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!isset($_GET['user_token']))
        parseJson::error('Le prestataire est introuvable')->printJson();

    $business_hours = [];
    $data = array('user_token' => $_GET['user_token']);
    $providerDispo = App::getFullCalendar()->getDispoProvider($data);

    if ($providerDispo === false)
        parseJson::error('Erreur lors de la recuperation des business hours')->printJson();

    foreach($providerDispo as $index => $dispo){
        $business_hours[] = (object) [
            "dow" => [
                $dispo->day
            ],
            "start" => $dispo->start,
            "end" => $dispo->end,
            "duration" => parseStr::subtract_the_time($dispo->end, $dispo->start)
        ];
    }

    parseJson::success('Les business hours on bien été récuperées', $business_hours)->printJson();
}