<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 14/09/2017
 * Time: 16:03
 */

require_once ('../../../inc/bootstrap.php');
$fullcalendar = App::getFullCalendar();

function create_stop_nobo_array($stop_nobo, $customer, $allDay, $end){
    return array(
        "title" => "stop_nobo",
        "start" => $stop_nobo->date_stop,
        "end" => $end, // FULLCALENDAR DOESN'T HANDLE EVENT WITHOUT END DATE , SO WE FILL STOP_NOBO WITH TEMPORARY END EQUAL TO STOP_NOBO + 10 DAY
        "color" => "rgb(255, 194, 179)",
        "rendering" => "background",
        "overlap" => false,
        "className" => "hidden ". $customer->user_token,
        "customer_token" => $customer->user_token,
        "allDay" => $allDay
    );
}

function create_holidays_array($holidays, $customer, $allDay){
    return array(
        "title" => "holidays",
        "start" => $holidays->start,
        "end" => $holidays->end." 23:59:59",
        "color" => "rgb(255, 194, 179)",
        "rendering" => "background",
        "className" => "hidden ". $customer->user_token,
        "customer_token" => $customer->user_token,
        "allDay" => $allDay
    );
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['user_token']) && isset($_GET['week_start']) && isset($_GET['week_end'])) {

        $db = App::getDB();
        $event_tab = [];
        $start = json_decode($_GET['week_start']);
        $end = json_decode($_GET['week_end']);

        $diff = parseStr::get_date_diff($start, $end, "%a");

        $data_generate_calendar = [
            "start" => $start,
            "end" => $end,
            "fullcalendar" => $fullcalendar,
            "user_token" => $_GET['user_token'],
            "editable" => true,
            "start_date" => date('Y-m-d', strtotime($start))
        ];

        App::getPlanning()->generate_calendar_event($data_generate_calendar, $event_tab);

        $customers_for_current_provider = App::getUser()->getUserInMissionByProvider($_GET['user_token']);
        foreach($customers_for_current_provider as $index => $customer){
            /* create stop_nobo event */
            $stop_nobo = App::getCustomer()->getStopNoboByUserToken($customer->user_token);
            if (!empty($stop_nobo))
                array_push($event_tab, create_stop_nobo_array($stop_nobo, $customer, false, $end)); // create stop-nobo for week view

           /* create holidays event */
           $customer_address = App::getAddress()->getUserAddress($customer->user_id);
           foreach($customer_address as $index_address => $address) {
               $holidays_customer = $fullcalendar->get_vacation_by_address($address->token);
               foreach ($holidays_customer as $index_holidays => $holidays) {
                   array_push($event_tab, create_holidays_array($holidays, $customer, false)); // create holidays for week view
               }
           }
       }

        //Including mission validated
        //App::getPlanning()->generate_calendar_event($data_generate_calendar, $event_tab);
        $provider = App::getUser()->getProviderByToken($_GET['user_token']);
        if ($provider === false)
            parseJson::success('Impossible d\'afficher le planning', ['user_id' => 'Impossible de récupérer l\'id'])->printJson();
        $options = [
            'who' => 'provider',
            'editable' => 'false',

        ];
        $missions = App::getMission()->getAllMissionForCalendar($start, $end, $provider->id,  $options);
        $events = App::getPlanning()->merge_mission_agenda($event_tab, $missions);
        $json = parseJson::success('Le planning a été charger avec success', $events);
        $json->printJson();
    }
}