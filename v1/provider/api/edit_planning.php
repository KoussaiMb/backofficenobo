<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $param_needed = ['start', 'end', 'id', 'user_token', 'type', 'edit'];

    foreach ($param_needed as $k) {
        if (!isset($_GET['ajax_data'][$k])) {
            $json = parseJson::error("Les paramètres de la mission ne sont pas valables");
            $json->printJson();
        }
    }

    $user_token = json_decode($_GET['ajax_data']['user_token']);
    $id = json_decode($_GET['ajax_data']['id']);
    $start = explode("T", json_decode($_GET['ajax_data']['start']));
    $end = explode("T", json_decode($_GET['ajax_data']['end']));
    $type = json_decode($_GET['ajax_data']['type']);
    $edit = json_decode($_GET['ajax_data']['edit']);
    $moved_slot = App::getFullCalendar()->getAgendaById($id);
    if ($moved_slot === false){
        $json = parseJson::error('Impossible de récupérer l\'agenda');
        $json->printJson();
    }

    $data_constraint = [
        "start" => $start,
        "end" => $end,
        "id" => $id,
        "user_token" => $user_token,
        "day" => date("w", (strtotime(date($end[0] )))),
        "type" => $type,
        "end_rec" => $moved_slot->end_rec,
        "edit" => $edit
    ];
    App::getProviderEditPlanning()->is_pass_constraint($data_constraint);

    $json = parseJson::success('Aucun conflit de missions trouvés');
    $json->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $param_needed = ['start', 'end', 'id', 'user_token', 'type'];

    foreach ($param_needed as $k) {
        if (!isset($_POST['ajax_data'][$k])) {
            $json = parseJson::error("Les paramètres de la mission ne sont pas valables");
            $json->printJson();
        }
    }

    $user_token = json_decode($_POST['ajax_data']['user_token']);
    $id = json_decode($_POST['ajax_data']['id']);
    $start = explode("T", json_decode($_POST['ajax_data']['start']));
    $end = explode("T", json_decode($_POST['ajax_data']['end']));
    $type = json_decode($_POST['ajax_data']['type']);

    $validator = new Validator($_POST['ajax_data']);

    $validator->is_id('id', 'Impossible de récupérer l\'agenda', 'Impossible de récupérer l\'agenda');
    $validator->isToken('user_token', 'Impossible de récupérer le token de l\'utilisateur','Impossible de récupérer le token de l\'utilisateur');


    if(!$validator->is_valid())
       parseJson::error('Impossible d\'accéder aux données du calendrier')->printJson();

    $discount_type = $_POST['type_val'][0];
    $discount_value = $_POST['type_val'][1];

    $start_datetime_str = $start[0] . $start[1];
    $start_datetime = new DateTime($start_datetime_str);
    $start_datetime = $start_datetime->format('Y-m-d H:i:s');
    $time_to_wait_before_validation = 0; //60*60*24 for a day

    if((time()- $time_to_wait_before_validation) < strtotime($start_datetime)){
        $json = parseJson::error("Impossible de valider une mission dans le futur");
        $json->printJson();
    }

    $end_datetime_str = $end[0] . $end[1];
    $end_datetime = new DateTime($end_datetime_str);
    $end_datetime = $end_datetime->format('Y-m-d H:i:s');

    $create_mission = App::getMission();
    $create_transaction = new transaction(App::getDB());

    $mission = $create_mission->is_mission_validated($start_datetime, $id);

    if (!$mission) {
        $mission_instance = App::getMission();
        $agenda = $mission_instance->get_agenda($id);
        if ($agenda === false)
            parseJson::error("Impossible de récupérer l'agenda afin de valider la mission")->printJson();
        $agenda->mission_token = my_crypt::genStandardToken();
        if (isset($agenda->provider_id))
            $governess_id = App::getCluster()->getGovernessByProviderId($agenda->provider_id);
        $agenda->governess_id = isset($governess_id->id) ? $governess_id->id : null;
        $agenda->start = $start_datetime;
        $agenda->end = $end_datetime;

        $mission_id = $mission_instance->add_mission($agenda);
        if ($mission_id === false)
            parseJson::error("La mission n'a pas été validée")->printJson();
        $update_address = $create_mission->update_mission_cur_address($agenda->address_id);
        if ($update_address === false)
            parseJson::error('Impossible d\'incrémenter la mission actuelle dans l\'adresse')->printJson();

        $transaction_success = App::getTransaction()->createTransactionFromMission([$mission_id]);

        if ($transaction_success === false)
            parseJson::success("La mission a bien été validée mais la transaction a été suspendue")->printJson();
        parseJson::success("La mission a bien été validée et la transaction initialisée")->printJson();
        
        //promocode effects is not about those things
        /*
        try {
            $agenda = $create_mission->get_agenda($id);
            $create_mission->start_mission($start_datetime, $end_datetime);
            $mission = $create_mission->get_mission();

            if ($discount_type == 'promocode')
                    $discount_value = App::getPromoCode()->getPromocodeByCode($discount_value)->id;

            $cart_data = $create_mission->create_cart_data_from_mission($mission, $discount_type, $discount_value);
            if ($cart_data === false)
                parseJson::error("Impossible de générer les données du panier")->printJson();
            $create_transaction->add_cart($cart_data);
            $tr_data = $create_transaction->create_transaction_data($agenda->user_id);
            if ($tr_data === false)
                parseJson::error("Impossible de générer la transaction", $tr_data)->printJson();
            $create_transaction->set_transaction($tr_data);
            $ins_cart = $create_transaction->insert_uniq_cart();
            if ($ins_cart === false)
                parseJson::error('Impossible d\'insérer le panier')->printJson();
            $updt_miss = $create_mission->update_mission_cart_id($ins_cart);
            if ($updt_miss === false)
                parseJson::error('Impossible de lier la mission avec le panier')->printJson();
            $ins_transaction = $create_transaction->update_uniq_transaction();
        } catch (Exception $e) {
            App::addLog('add_cart', $e->getMessage(), App::getAuth()->user()->id, 'transaction.php');
            parseJson::error("Impossible d'associer la mission au paiement")->printJson();
        if ($ins_transaction === false)
            parseJson::error('Impossible de générer la transaction')->printJson();
        $create_transaction->update_transaction_price_final();
        }*/

    } else {
        $error_messages = [
            'no_discount' =>'Impossible de mettre à jour le discount en euro',
            'promocode' => 'Impossible de mettre à jour le promocode',
            'discount_euro' => 'Impossible de mettre à jour le discount en euro',
            'discount_percentage' => 'Impossible de mettre à jour le discount en pourcentage'
        ];
        $is_payed = $create_transaction->is_payed_transaction($mission['cart_id']);
        $discount_updated = false;

        if ($is_payed && $is_payed['payed'])
            parseJson::error('Impossible de modifier une transaction déjà payée')->printJson();
        if ($discount_type == 'no_discount')
            $discount = $create_transaction->remove_all_promo($mission['cart_id']);
        else if ($discount_type == 'promocode') {
            $promocode_id = App::getPromoCode()->getPromocodeByCode($discount_value)->id;
            $discount = $create_transaction->update_promocode_cart($promocode_id, $mission['cart_id']);
        }
        else if ($discount_type == 'discount_euro')
            $discount = $create_transaction->update_discount_cart($discount_value, 'euro', $mission['cart_id']);
        else if ($discount_type == 'discount_percentage')
            $discount = $create_transaction->update_discount_cart($discount_value, 'percentage', $mission['cart_id']);
        else
            parseJson::error("La mission a déjà été validée")->printJson();
        if ($discount === false)
            parseJson::error($error_messages[$discount_type])->printJson();
        parseJson::success("Les promotions ont été mises à jour");
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $param_needed = ['start', 'end', 'id', 'user_token', 'edit', 'type', 'previous_date'];
    $_PUT = App::getRequest();
    $fullCalendar = App::getFullCalendar();

    foreach ($param_needed as $k)
        if (!isset($_PUT['ajax_data'][$k]))
            parseJson::error("Les paramètres de la mission ne sont pas valables")->printJson();

    /* we initialize all the variable */


    $weekday = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $id = json_decode($_PUT['ajax_data']['id']);
    $start = explode("T", json_decode($_PUT['ajax_data']['start']));
    $end = explode("T", json_decode($_PUT['ajax_data']['end']));
    $user_token = json_decode($_PUT['ajax_data']['user_token']);
    $previous_date = json_decode($_PUT['ajax_data']['previous_date']);
    $edit = json_decode($_PUT['ajax_data']['edit']);
    $action = $_PUT['action'];
    $type = json_decode($_PUT['ajax_data']['type']);
    $moved_agenda = App::getFullCalendar()->getAgendaById($id);

    if ($moved_agenda == false){
        $json = parseJson::error('Erreur lors de la récupération de l\'agenda');
        $json->printJson();
    }

    /* we get the customer moved_slot info */
    $customer = App::getUser()->getCustomerByUserId($moved_agenda->user_id);
    if ($customer === false)
        parseJson::error('Impossible de récupérer les données du client');
    $user = App::getUser()->getUserById($moved_agenda->user_id);
    if ($user === false)
        parseJson::error('Impossible de récupérer les données de l\'utilisateur');
    $address = App::getUser()->getAddressById($moved_agenda->address_id);
    if ($address === false)
        parseJson::error('Impossible de récupérer les données de l\'adresse');
    $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

    $data = [
        "data_common" => [
            "customer_token" => $user->user_token,
            "address_token" => $address->address_token,
            "user_token" => $user_token,
            "title" => "title",
            "duration" => $moved_agenda->duration,
            "locked" => $moved_agenda->locked,
            "pending" => $moved_agenda->pending,
            "type" => $moved_agenda->type,
            "id" => $id
        ],
        "start" => $start,
        "end" => $end,
        "edit" => $edit,
        "moved_agenda" => $moved_agenda,
        "previous_date" => $previous_date
    ];

    $data_constraint = [
        "start" => $start,
        "end" => $end,
        "edit" => $edit,
        "id" => $id,
        "user_token" => $user_token,
        "day" => date("w", (strtotime(date($end[0] )))),
        "type" => $type,
        "end_rec" => $moved_agenda->end_rec
    ];

    if ($action != "delete")
        App::getProviderEditPlanning()->is_pass_constraint($data_constraint);

    /* we check the position of the moved_slot in the "period list"
     (example : if first date / last date, between start / end) */
    App::getProviderEditPlanning()->check_pos_in_list($data, $action);

    parseJson::success('Votre édition a bien été prise en compte')->printJson();
}