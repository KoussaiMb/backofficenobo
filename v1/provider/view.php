<?php
require_once('../../inc/bootstrap.php');

Session::getInstance()->upLog('provider_checked');

$providerSearch = App::getUser()->getAutocompleteProvider();
$providersHired = App::getUser()->getProvidersHired();
$providersNotHired = App::getUser()->getProvidersNotHired();

require_once('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Rechercher un prestataire:
    </div>
    <div class="panel-body SpaceTop">
        <div class="col-sm-10">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <form class="form-inline">
                <label class="col-sm-2 col-sm-offset-3" for="providersearch">Rechercher un prestataire</label>
                <input type="hidden" name="id" id="providersearch_hidden">
                <input type="text" class="form-control" id="providersearch" placeholder="prenom, nom">
                <button type="button" class="btn btn-default" onclick="window.location = 'edit?user_token=' + $('#providersearch_hidden').val()">Fiche prestataire</button>
            </form>
        </div>
        <div class="col-sm-2">
            <a href="add"><button class="btn btn-success" name="action" value="add" style="float: right;"><span class="glyphicon glyphicon-plus"></span></button></a>
        </div>
        <!-- Providers processing -->
        <div class="col-sm-6 SpaceTop">
            <h4>Prestataires en période d'essai</h4>
            <div class="table-responsive">
                <table id="table_provider_pending" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($providersNotHired as $key => $row): ?>
                        <tr class="clickable-row <?= $row->pending == 0 ? 'bg-danger' : '';?>" data-href="<?= 'edit?user_token=' . $row->user_token;?>">
                            <td><?= $row->firstname . ' ' . $row->lastname ?></td>
                            <td><?= $row->email ?></td>
                            <td><?= $row->phone ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Prestataires validés -->
        <div class="col-sm-6 SpaceTop">
            <h4>Prestataires</h4>
            <div class="table-responsive">
                <table id="table_provider_validated" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($providersHired as $key => $v): ?>
                        <tr class="clickable-row" data-href="<?= 'edit?user_token=' . $v->user_token;?>">
                            <td><?= $v->firstname . ' ' . $v->lastname; ?></td>
                            <td><?= $v->email; ?></td>
                            <td><?= $v->phone; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        var providersearch = $('#providersearch');

        providersearch.devbridgeAutocomplete({
            lookup: <?php if (!empty($providerSearch)) {echo $providerSearch;} else {echo "[]";}?>,
            minChars: 1,
            maxHeight: 200,
            onSelect: function (suggestion) {
                $('#providersearch_hidden').val(suggestion.data);
            }
        });

        providersearch.keypress(function (e) {
            var key = e.which;

            if(key == 13)
            {
                window.location = 'edit?user_token=' + $('#providersearch_hidden').val();
                return false;
            }
        });
    });
</script>

<?php
require('../../inc/footer_bo.php');