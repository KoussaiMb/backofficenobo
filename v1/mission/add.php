<?php
require('../../inc/bootstrap.php');

$db = App::getDB();

$conf = App::getConf()->getAll();
$allClient = App::getCustomer()->getAutocompleteName();
$allAddress = App::getAddress()->getAutocompAddresses();
$allProvider = App::getUser()->getAutocompleteProvider();

if ($allAddress === null || $allClient === null || $allProvider === null)
    App::setFlashAndRedirect('danger', 'impossible de récupérer les données', '/');

if ($allAddress)
{
    foreach ($allAddress as $k => $v) {
        $allAddress[$k]->address = utf8_encode($v->address);
    }
}

if (isset($_POST['menu_id']))
    $menu_id = substr($_POST['menu_id'], -1);
else
    $menu_id = '1';

require('../../inc/header_bo.php');
?>
    <link type='text/css' rel='stylesheet' href='/css/prestation.css'/>

    <div class="panel panel-success">
        <div class="panel-heading">
            <div style="min-height: 30px">
                <div class="col-sm-3">Créer une nouvelle mission</div>
                <div class="col-sm-9">
                    <ul class="nav nav-pills" id='tabs_provider' style="display: flex; justify-content: space-around;">
                        <input type="hidden" value="<?= $menu_id; ?>" id="get_menu_id" disabled>
                        <li id="tab1"><a data-toggle="tab" href="#menu1">Rechercher un créneau</a></li>
                        <li id="tab3"><a data-toggle="tab" href="#menu3">Client en attente</a></li>
                        <!--<li id="tab4"><a data-toggle="tab" href="#menu4">Mission client en attente</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <?php include('../../inc/print_flash_helper.php'); ?>
                <!--Modal_ponctual_date-->
                <div id="modal_confirm_ponctuel" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <div style="font-size: 15px; font-weight: bold;">Selectionner une date</div>
                            </div>
                            <div class="modal-footer">
                                <form onsubmit="return valid_select();" class="form_confirm">

                                    <div class="col-sm-12">
                                        <label class="col-xs-12 text-left" > date : <input name="date" type="text" class="form-control input-sm date_picker date_confirm_ponctual" data-provide="datepicker" required></label>
                                    </div>
                                    <div class="form-group col-sm-12" style="margin-top: 2em;" id="confirm">
                                        <button type="submit" class="btn btn-primary col-xs-12 btn-sm">Valider</button>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal ponctual_date-->
                <!--Modal_alert-->
                <div id="modal_alert" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <div style="font-size: 15px; font-weight: bold;">Attention un ou plusieurs conflits on lieu, voulez-vous continuer?</div>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button id="btn-refuse" type="button" class="btn btn-warning">Non</button>
                                <button id="btn-accept" type="button" class="btn btn-default">Oui</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal alert-->
                <!--Modal_holidays-->
                <div id="modal_holidays" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <div style="font-size: 15px; font-weight: bold;">Attention le client a des vacances dans la période choisie, voulez-vous conserver les missions ?</div>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button id="btn-return-holidays" type="button" class="btn btn-info">Retour</button>
                                <button id="btn-refuse-holidays" type="button" class="btn btn-warning">Non</button>
                                <button id="btn-accept-holidays" type="button" class="btn btn-default">Oui</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal holidays-->
                <!--Modal_confirm_start-->
                <div id="modal_confirm" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <div style="font-size: 15px; font-weight: bold;">Selectionner l'intervalle pour votre récurrence</div>
                            </div>
                            <div class="modal-footer">
                                <form onsubmit="return valid_select();" class="form_confirm">

                                    <div class="col-sm-12">
                                       <label class="col-xs-6 text-left" > début : <input name="date" type="text" class="form-control input-sm date_picker date_confirm" data-provide="datepicker" required></label>
                                        <label class="col-xs-6 text-left" > fin : <input name="date" type="text" class="form-control input-sm date_picker date_confirm_end" data-provide="datepicker"></label>
                                    </div>
                                    <div class="form-group col-sm-12" style="margin-top: 2em;" id="confirm">
                                        <button type="submit" class="btn btn-primary col-xs-12 btn-sm">Valider</button>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal confirm_end -->
                <!--Modal_Search-->
                <div id="modal_search" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <h4 id="modal_search_Title" class="modal-title"></h4>
                                <div id="duration_drag" style="margin-bottom: 10px;" class="col-sm-12"></div>
                                <div class="col-sm-12 provider_list"></div>
                            </div>
                            <div id="modal_search_Body" class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal Search -->
                <!--Modal_dispo-->
                <div id="modal_dispo" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <div style="font-size: 15px; font-weight: bold;">Disponibilité client</div>
                            </div>
                            <div class="modal-body">
                                <div id="calendar_dispo"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin modal dispo-->
                <!--Modal_delete_dispo-->
                <div id="modal_delete_dispo" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                <h4 id="modal_delete_dispo_Title" class="modal-title"></h4>
                            </div>
                            <div id="modal_delete_dispo_Body" class="modal-body"></div>
                            <div class="modal-footer">
                                <button id="close_dispo" type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-delete-dispo">Supprimer le créneau de disponibilité</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fin Modal_delete_dispo-->
                <div id="menu1" class="tab-pane fade">
                    <div class="col-sm-12">
                        <div class="form-group col-sm-6">
                            <select class="form-control frequency" style="height: 30px; padding: 5px 10px; font-size: 12px; line-height: 1.5;" id="select_frequency">
                                <option value="0" data-duration_selection_nb="1">Une fois par semaine</option>
                                <option value="0" data-duration_selection_nb="2">Deux fois par semaine</option>
                                <option value="0" data-duration_selection_nb="3">Trois fois par semaine</option>
                                <option value="0" data-duration_selection_nb="4">Quatre fois par semaine</option>
                                <option value="0" data-duration_selection_nb="5">Cinq fois par semaine</option>
                                <option value="1" data-duration_selection_nb="1">Toutes les 2 semaines</option>
                                <option value="3" data-duration_selection_nb="1">Ponctuelle</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <select class="form-control auto_search_recurrent" style="height: 30px; padding: 5px 10px; font-size: 12px; line-height: 1.5;" id="select_auto">
                                <option value="0">Semi-automatique</option>
                                <!-- <option value="1">Automatique</option> -->
                            </select>
                        </div>
                        <form id="search_client">
                            <div class="form-group">
                                <label for="client">Client</label>
                                <input type="hidden" class="user_token" name="user_token" value="">
                                <input type="text" class="form-control input-sm clientAutoComplete" placeholder="prenom, nom" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Adresse</label>
                                <select name="ID" class="form-control input-sm addressAutoComplete address_recurent" required>
                                </select>
                            </div>
                        </form>

                        <!-- FORM RECURRENT -->
                        <form class="hidden" id="form_valid_recurrent">
                            <div class="col-xs-12 duration_label_name hidden">
                                <label for="duration">Durée</label>
                            </div>
                            <div class="form-group col-xs-6 hidden" id="select_duration_recurent">
                            </div>
                            <div class="col-xs-6">
                                <button type="button" style="margin-bottom: 5px; max-width: 120px;" class="btn btn-primary dispo_client_defined"><span class="glyphicon glyphicon-calendar"></span> Dispo Client</button>
                            </div>
                            <div class="form-group hidden" style="margin-top: 2em;" id="validation">
                                <button type="submit" class="btn btn-primary col-xs-12 btn-sm">Rechercher</button>
                            </div>
                        </form>

                        <!-- FORM RECURRENT -->

                    </div>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <div class="col-sm-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">Créneaux prévisionnel en attente :</div>
                            <div class="panel-body">
                                <div class="pending_container_recurent container col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="provider_container"></div>
                    </div>
                </div>
                <!--<div id="menu4" class="tab-pane fade">
                    <div class="col-sm-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">Mission en attente</div>
                            <div class="panel-body">
                                <div class="pending_container_ponctuel container col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="provider_container"></div>
                    </div>
                </div>-->
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <!--<div id="calendar" style="display: none;"></div>-->
            <div class="col-sm-12">
                <div id="calendarProvider" style="display: none;"></div>
            </div>
            <div class="col-sm-6">
                <div id="calendarSupervisor" style="display: none;"></div>
            </div>
            <div id="loader"></div>
        </div>
        <div class="col-sm-2">
            <div id="showStatus" class="col-sm-12"></div>
        </div>
    <script src="/js/add_prestation.js"></script>
    <script src="/js/lib/jquery-ui.min.js"></script>
    <script src="/js/app.js"></script>
    <script>
        function filterAddress(user_token, address, allAddress) {
            sessionStorage.setItem('user_token', user_token);
            address.find('option').remove().end();
            $.each(allAddress, function (i, item) {
                if (user_token === item.user_token) {
                    address.append($('<option>', {
                        value: item.address_token,
                        text: item.address
                    }));
                }
            });
            address.change();
        }

        function remove_conflic_elements() {
            $('.calendar_agenda').remove();
            $('.provider_name').remove();
            $('.draggable_duration').remove();
            $('#valid_choice').remove();
        }
        jQuery(document).ready(function() {
            var allClient = <?php if (isset($allClient)){echo $allClient;} else {echo "[]";}?>;
            var allAddress = <?php if (isset($allAddress)){echo json_encode($allAddress);} else {echo "[]";}?>;
            var menu_id = $('#get_menu_id').val();
            var dispo_aldready_display = 0;
            var date = "";
            var token;
            let form_validation = $('#form_valid_recurrent');

            $('#tab' + menu_id).addClass('active');
            $('#menu' + menu_id).addClass('in active');

            $('#select_frequency').on('change', function () {
                let duration_selection_container = $("#select_duration_recurent");
                let duration_selection_nb = $(this).find(':selected').data('duration_selection_nb');
                let min_time_prestation = <?php echo json_encode(isset($conf['prestation_min']) ? $conf['prestation_min'] : "[]"); ?>;
                let max_time_prestation = <?php echo json_encode(isset($conf['prestation_max']) ? $conf['prestation_max'] : "[]"); ?>;
                duration_selection_container.empty();

                for (let i = 1; i <= duration_selection_nb; i++) {
                    duration_selection_container.append(create_duration_selection(i));
                    create_timepicker($('#duration_recurent_' + i), min_time_prestation, max_time_prestation);
                }
            }).change();

            form_validation.on('submit', function (event) {
                confirm_start_rec($('#select_frequency').find(':selected').data('duration_selection_nb'), 'recurent');
                event.preventDefault();
            });

            if (allClient && allAddress) {
                $('.clientAutoComplete').devbridgeAutocomplete({
                    lookup: allClient,
                    maxHeight: 200,
                    onSelect: function (suggestion) {
                        let address = $(this).closest('form').find('.addressAutoComplete');
                        $(this).prev().val(suggestion.data);
                        filterAddress(suggestion.data, address, allAddress);
                    }
                });
            }

            $('.btn-return-confirm').on('click', function() {
                $('#modal_confirm').modal('hide');
                $('#modal_search').modal('show');
            });

            $('.duration_slot').timepicker({
                'minTime': "<?php echo $conf['prestation_min']; ?>",
                'maxTime': "<?php echo $conf['prestation_max']; ?>"
            });

            $('.dispo_slot').timepicker({
                'minTime': '08:00',
                'maxTime': '22:00'
            });

            $('#modal_dispo').on('shown.bs.modal', function () {
                if (dispo_aldready_display == 0) {
                    $.when(create_dispo_calendar()).then(
                        function () {
                            $('#modal_dispo').removeClass('hidden_item');
                        },
                        function () {
                            console.log("fail");
                        }
                    );
                    dispo_aldready_display++;
                }
            });

            $('.dispo_client_defined').on('click', function(){
                if (dispo_aldready_display == 0) {
                    $('#modal_dispo').addClass('hidden_item');
                }
                $('#modal_dispo').modal("show");
            });

            /* -- wth is that ?? */
            $('.date_ponctuel').val(date);

            $('.date_ponctuel').on('change', function(){
                date = $('.date_ponctuel').val();
            });
            /* wth is that ?? -- */

            $('.address_recurent').on('change', function () {
                dispo_aldready_display = 0;
                sessionStorage.setItem('address_token', $('.address_recurent').val());

                //token = $('.user_token').val();
                $('#calendar_dispo').fullCalendar('destroy');
                $('#validation_defined').removeClass('hidden');
                $('#select_duration_recurent').removeClass('hidden');
                $('#validation').removeClass('hidden');
                form_validation.removeClass('hidden');
                $('.duration_label_name').removeClass('hidden');
            });

            $('.type_address_ponctuel').on('change', function () {
                sessionStorage.setItem('address_token', $('.type_address_ponctuel').val());
                $('.form_valid_ponctuel').removeClass('hidden');
            });
        });
    </script>
<?php
require('../../inc/footer_bo.php');