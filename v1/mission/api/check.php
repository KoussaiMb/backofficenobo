<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 12/10/2017
 * Time: 15:55
 */

require_once ('../../../inc/bootstrap.php');

$fullcalendar = App::getFullCalendar();

function format_events($events, $data){
    $formattedEvents = [];
    foreach($events as $index => $currentEvent){
        $formattedEvents[] = [
            "dayNumeric" => date('w', strtotime($currentEvent->start)),
            "dayString" => date('l', strtotime($currentEvent->start)),
            "hours_start" => date('H:i:s', strtotime($currentEvent->start)),
            "hours_end" => date('H:i:s', strtotime($currentEvent->end)),
            "duration" => $currentEvent->duration,
            "date_start" => $data['date_start'],
            "date_end" => $data['date_end'],
            "type" => $data['type'],
            "user_token" => $data['user_token']
        ];
    }
    return $formattedEvents;
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = App::get_instance_validator($_GET);

    $validator->is_not_null('events', 'Evenement invalide', 'Evenement manquant');
    $validator->is_not_null('frequency', 'Fréquence invalide', 'Fréquence manquante');
    $validator->is_not_null('customer_token', 'Client invalide', 'Client manquant');
    $validator->is_not_null('address_token', 'Adresse invalide', 'Adresse manquante');
    $validator->is_not_null('user_token', 'Utilisateur invalide', 'Utilisateur manquant');
    $validator->is_not_null('type', 'Type de prestation invalide', 'Type de prestation manquant');
    $validator->is_not_null('date_start', 'Date de début invalide', 'Date de début manquante');
    if (!isset($_GET['date_end']))
        $validator->throwException('date_end', 'Date de fin manquante');
    if (!$validator->is_valid()) {
        $json = parseJson::error("Les informations sont incorrectes", $validator->getErrors());
        $json->printJson();
    }

    setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
    $conflict = [];
    $conflict_message = [];
    $events = json_decode($_GET['events']);
    $data = [
        "frequency" => json_decode($_GET['frequency']),
        "customer_token" => json_decode($_GET['customer_token']),
        "user_token" => json_decode($_GET['user_token']),
        "address_token" => json_decode($_GET['address_token']),
        "date_start" => json_decode($_GET['date_start']),
        "date_end" => json_decode($_GET['date_end']),
        "conflict_tab" => [],
        "type" => json_decode($_GET['type']),
        "valid_mission" => []
    ];

    $formattedEvents = format_events($events, $data);
    foreach($formattedEvents as $index_event => $current_formatted_event){
        $conflict = $fullcalendar->getAgendaConflictInInterval($current_formatted_event);
        if ($conflict === false){
            $json = parseJson::error('Impossible de récupérer les conflits');
            $json->printJson();
        }
        if (!empty($conflict)){
            foreach($conflict as $index_conflict => $current_conflict){
                $format_date_start = strftime("%A %d %B %Y", strtotime($current_conflict->start_rec));
                $format_date_end = is_null($current_conflict->end_rec) ? "indeterminé" : strftime("%A %d %B %Y", strtotime($current_conflict->end_rec));
                $conflict_message[] =
                    "<table style='margin-top: 10px;' class='table table-condensed table-bordered'><thead><tr><th>"
                    . "<span class='glyphicon glyphicon-user'></span>&nbsp&nbsp Client : "
                    . $current_conflict->firstname. " " . $current_conflict->lastname
                    . "</th></th><th><span class='glyphicon glyphicon-time'></span>&nbsp&nbsp Plage Horaire : "
                    . str_replace(":", "H", substr($current_conflict->start, 0, 5))
                    . " à "
                    . str_replace(":", "H", substr($current_conflict->end, 0, 5))
                    . "</th><th><span class='glyphicon glyphicon-calendar'></span>&nbsp&nbsp Du : "
                    . $format_date_start
                    . " au "
                    . $format_date_end
                    . " </th></tr></thead><tbody><tr style='background-color: lightgoldenrodyellow;'>"
                    . "<th colspan='3' style='text-align: center;'>&nbsp&nbsp Attention, le créneau que vous êtes"
                    . " en train de crée entrera en conflict avec ce créneau</th></tr></tbody></table>";
            }
        }
    }
    if ($conflict_message == [])
        $json = parseJson::success('Aucun conflit détecté');
    else
        $json = parseJson::success('Des conflits sont intervenus : ', $conflict_message);
    $json->printJson();
}