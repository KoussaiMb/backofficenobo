<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 24/08/2017
 * Time: 13:45
 */
require_once ('../../../inc/bootstrap.php');

$fullcalendar = App::getFullCalendar();
$db = App::getDB();

function get_date_without_holidays($holidays_customer, $event, $date_end, $stop_nobo){
    $event_date_tab = [];
    /* we format the start_date */
    $start_event = date('Y-m-d', strtotime($event->start));

    /*
     * we check if the event had a end_date,
     *
     * if $date_end is not null we get the last day before the end
     * and we check if the date are not above stop_nobo (if stop nobo exist)
     *
     * if $date_end is null we just check if stop_nobo exist
     */

    if ($date_end !== null) {
        $day = date('l', strtotime($date_end));
        $last_day = empty($stop_nobo) ? parseStr::get_last_week_day($date_end, $day) : $stop_nobo->date_stop;
    }
    else {
        $last_day = empty($stop_nobo) ? null : $stop_nobo->date_stop;
    }


    /*
     * we goes through holidays
     */
    foreach ($holidays_customer as $index => $holidays){
        /*
         *  we check (if stop_nobo exist) if holidays_end is after stop_nobo and if is true if holidays_start
         *  is before or after stop nobo, then we fill $last_day with the earlier and break
        */
        if (!empty($stop_nobo) && parseStr::compare_time_strict_equal($holidays->end, $stop_nobo->date_stop)){
            $last_day = parseStr::compare_time_strict($holidays->start, $stop_nobo->date_stop) ? $stop_nobo->date_stop : $holidays->start;
            break;
        }

        /*
         * we check if the interval between holidays contain the days we want if not, we dont save the interval
         */
        if (parseStr::compare_time_strict_equal(parseStr::get_last_week_day($holidays->start, date('l', strtotime($event->start))), $start_event)) {
            $event_date_tab[] = array(
                "date_start" => $start_event,
                "date_end" => $holidays->start
            );
        }
        $start_event = $holidays->end;
    }
    $event_date_tab[] = array(
        "date_start" => $start_event,
        "date_end" => $last_day
    );

    return $event_date_tab;
}

function create_stop_nobo_array($stop_nobo, $customer_token, $allDay, $end){
    return array(
        "title" => "holidays",
        "start" => $stop_nobo->date_stop,
        "end" => $end, // FULLCALENDAR DOESN'T HANDLE EVENT WITHOUT END DATE , SO WE FILL STOP_NOBO WITH TEMPORARY END EQUAL TO STOP_NOBO + 10 DAY
        "color" => "rgb(255, 194, 179)",
        "rendering" => "background",
        "overlap" => false,
        "className" => $customer_token,
        "customer_token" => $customer_token,
        "allDay" => $allDay
    );
}

function create_holidays_array($holidays, $customer_token, $allDay){
    return array(
        "title" => "holidays",
        "start" => $holidays->start,
        "end" => $holidays->end." 23:59:59",
        "color" => "rgb(255, 194, 179)",
        "rendering" => "background",
        "overlap" => false,
        "className" => $customer_token,
        "customer_token" => $customer_token,
        "allDay" => $allDay
    );
}

function sort_event_by_day($event_tab, $data){
    $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $sort_event = [];
    foreach ($event_tab as $index => $event){
        $next_day = parseStr::get_next_week_day($data['start_date'], $weekDay[$event->day]);
        if (((bool)!(date('W', strtotime($next_day)) % 2) + 1) != $event->type || $event->type == 0 || $event->type == 3)
            $sort_event[$event->day][] = $event;
    }
    return $sort_event;
}

function create_event_array($day, $start, $end, $editable){
    return array(
        "start" => empty($start) ? NULL : $start,
        "editable" => $editable,
        "end" => empty($end) ? NULL : $end,
        "color" => "rgb(255,0,0)",
        "allDay" => false,
        "day_event" => $day,
        "rendering" => "background",
    );
}

function getWeekday($date) {
    return date('w', strtotime($date));
}

/* this function get the red backgroundevent to prevent conflict on future */
function fill_future_background_event($data, &$event_tab){
    /* we get all future event between choosen date */
    $event_brut = $data['fullcalendar']->getAgendaInInterval($data);
    $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $sort_event = sort_event_by_day($event_brut, $data);
    foreach ($sort_event as $index_day => $day){
        foreach($day as $index_event => $event) {
            foreach($data['business_hours'] as $day_restrict => $restrict)
            {
                if ($restrict->dow[0] == $event->day){
                    /* we define the start and the end of the event, and the start and the end to check */
                    $interval_hours_set = [
                        "start" => $restrict->start,
                        "end" => $restrict->end
                    ];
                    $hours_set_to_check = [
                        "start" => $event->start,
                        "end" => $event->end
                    ];
                    /* if the interval to check is between the event interval we add a background event */
                    if (parseStr::in_interval($interval_hours_set, $hours_set_to_check) === true) {
                        if (parseStr::compare_time_strict_equal($restrict->start, $event->start) && parseStr::compare_time_strict($event->end, $restrict->start))
                            $event->start = $restrict->start;
                        if (parseStr::compare_time_strict($event->end, $restrict->end) && parseStr::compare_time_strict($event->end, $restrict->start))
                            $event->end = $restrict->end;
                        $start_time = explode(":", $event->start);
                        $end_time = explode(":", $event->end);
                        $next_day = parseStr::get_next_week_day($data['start_date'], $weekDay[$index_day]);
                        $start = date('Y-m-d H:i:s', strtotime('+' . $start_time[0] . ' hours' . '+' . $start_time[1] . ' minutes', strtotime($next_day)));
                        $end = date('Y-m-d H:i:s', strtotime('+' . $end_time[0] . ' hours' . '+' . $end_time[1] . ' minutes', strtotime($next_day)));
                        array_push($event_tab, create_event_array($day, $start, $end, false));
                    }
                }
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['events']) && isset($_POST['frequency']) && isset($_POST['customer_token']) && isset($_POST['address_token']) && isset($_POST['user_token']) && isset($_POST['type']) && isset($_POST['date_start'])) {
        $event = json_decode($_POST['events']);
        $frequency = json_decode($_POST['frequency']);
        $customer_token = json_decode($_POST['customer_token']);
        $user_token = json_decode($_POST['user_token']);
        $address_token = json_decode($_POST['address_token']);
        $type = json_decode($_POST['type']);
        $date_start = json_decode($_POST['date_start']);
        $date_end = json_decode($_POST['date_end']);
        $holidays_action = json_decode($_POST['holidays']);
        $stop_nobo = App::getCustomer()->getStopNoboByUserToken($customer_token);
        $holidays_customer = $fullcalendar->get_vacation_by_address($address_token);

        foreach ($event as $index => $current_event) {
            if (!empty($holidays_customer) && $holidays_action == 'remove'){
                $date_without_holidays = get_date_without_holidays($holidays_customer, $current_event, $date_end, $stop_nobo);
                foreach ($date_without_holidays as $index_date => $date){
                    $dayWeek = getWeekday($current_event->start);
                    $hours_start = date('H:i:s', strtotime($current_event->start));
                    $hours_end = date('H:i:s', strtotime($current_event->end));
                    $weeknb = ((bool)!(date('W', strtotime($current_event->start)) % 2) + 1);

                    if ($frequency->frequency == 'recurent') {
                        $data = [
                            "user_token" => $user_token,
                            "customer_token" => $customer_token,
                            "address_token" => $address_token,
                            "title" => "a title",
                            "day" => $dayWeek,
                            "start" => $hours_start,
                            "end" => $hours_end,
                            "duration" => $current_event->duration,
                            "type" => ($type == 0 || $type == 3) ? $type : $weeknb,
                            "locked" => $current_event->locked,
                            "pending" => 0,
                            "start_rec" => $date['date_start'],
                            "end_rec" => $date['date_end']

                        ];
                        $id = $fullcalendar->addAgenda($data);
                        if ($id === false)
                            parseJson::error('Une erreur est survenue lors de l\'ajout de l\'agenda')->printJson();
                    }
                }
            }
            else {
                /* on verifie si il a un stop_nobo, et  si il y en a un on vérifie qu'il est bien supérieur a la date de début de l'événement */
                if (empty($stop_nobo) || parseStr::compare_time_strict($stop_nobo->date_stop, $current_event->start)) {
                    $dayWeek = getWeekday($current_event->start);
                    $hours_start = date('H:i:s', strtotime($current_event->start));
                    $hours_end = date('H:i:s', strtotime($current_event->end));
                    $weeknb = ((bool)!(date('W', strtotime($current_event->start)) % 2) + 1);

                    if ($frequency->frequency == 'recurent') {
                        $data = [
                            "user_token" => $user_token,
                            "customer_token" => $customer_token,
                            "address_token" => $address_token,
                            "title" => "a title",
                            "day" => $dayWeek,
                            "start" => $hours_start,
                            "end" => $hours_end,
                            "duration" => $current_event->duration,
                            "type" => ($type == 0 || $type == 3) ? $type : $weeknb,
                            "locked" => $current_event->locked,
                            "pending" => 0,
                            "start_rec" => date('Y-m-d', strtotime($current_event->start)),
                            "end_rec" => empty($stop_nobo) ? $date_end : $stop_nobo->date_stop

                        ];

                        $id = $fullcalendar->addAgenda($data);
                        if ($id === false)
                            parseJson::error('Une erreur est survenue lors de l\'ajout de l\'agenda')->printJson();
                    }
                }
            }
        }
        parseJson::success('La mission a bien été ajoutée')->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
    $_PUT = App::getRequest();
    if (isset($_PUT['events']) && isset($_PUT['frequency']) && isset($_PUT['customer_token']) && isset($_PUT['address_token']) && isset($_PUT['user_token']) && isset($_PUT['type']) && isset($_PUT['date_start'])) {
        $event = json_decode($_PUT['events']);
        $frequency = json_decode($_PUT['frequency']);
        $customer_token = json_decode($_PUT['customer_token']);
        $user_token = json_decode($_PUT['user_token']);
        $address_token = json_decode($_PUT['address_token']);
        $type = json_decode($_PUT['type']);
        $date_start = json_decode($_PUT['date_start']);
        $date_end = json_decode($_PUT['date_end']);
        $holidays_action = json_decode($_PUT['holidays']);
        $stop_nobo = App::getCustomer()->getStopNoboByUserToken($customer_token);
        $holidays_customer = $fullcalendar->get_vacation_by_address($address_token);

        foreach($event as $index => $current_event){
            if (!empty($holidays_customer) && $holidays_action == 'remove'){
                $date_without_holidays = get_date_without_holidays($holidays_customer, $current_event, $date_end, $stop_nobo);
                $fullcalendar->delAgendaEvent($current_event->id);
                foreach ($date_without_holidays as $index_date => $date){
                    $dayWeek = getWeekday($current_event->start);
                    $hours_start = date('H:i:s', strtotime($current_event->start));
                    $hours_end = date('H:i:s', strtotime($current_event->end));
                    $weeknb = ((bool)!(date('W', strtotime($current_event->start)) % 2) + 1);

                    if ($frequency->frequency == 'recurent') {
                        $data = [
                            "user_token" => $user_token,
                            "customer_token" => $customer_token,
                            "address_token" => $address_token,
                            "title" => "a title",
                            "day" => $dayWeek,
                            "start" => $hours_start,
                            "end" => $hours_end,
                            "duration" => $current_event->duration,
                            "type" => ($type == 0 || $type == 3) ? $type : $weeknb,
                            "locked" => $current_event->locked,
                            "pending" => 0,
                            "start_rec" => $date['date_start'],
                            "end_rec" => $date['date_end']

                        ];
                        $id = $fullcalendar->addAgenda($data);
                        if ($id === false) {
                            $json = parseJson::error('Une erreur est survenue lors de l\'ajout de l\'agenda');
                            $json->printJson();
                        }
                    }
                }
            }
            else {
                if($stop_nobo){
                    if (parseStr::compare_time_strict($stop_nobo->date_stop, $current_event->start)) {
                        $dayWeek = getWeekday($current_event->start);
                        $hours_start = date('H:i:s', strtotime($current_event->start));
                        $hours_end = date('H:i:s', strtotime($current_event->end));
                        $weeknb = ((bool)!(date('W', strtotime($current_event->start)) % 2) + 1);
                        if ($frequency->frequency == 'recurent') {
                            $data = [
                                "user_token" => $user_token,
                                "day" => $dayWeek,
                                "start" => $hours_start,
                                "end" => $hours_end,
                                "duration" => $current_event->duration,
                                "type" => ($type == 0 || $type == 3) ? $type : $weeknb,
                                "start_rec" => date('Y-m-d', strtotime($current_event->start)),
                                "end_rec" => empty($stop_nobo) ? $date_end : $stop_nobo->date_stop,
                                "event_id" => $current_event->id

                            ];
                            if (($fullcalendar->UpdateAgendaEventPending($data)) === false) {
                                $json = parseJson::error('Une erreur est survenue lors de l\'ajout de l\'agenda');
                                $json->printJson();
                            }
                        }
                    }
                }
            }
        }
        $json = parseJson::success('La mission a bien été ajoutée');
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['token'])
        && isset($_GET['week_start'])
        && isset($_GET['week_end'])
        && isset($_GET['start'])
        && isset($_GET['end'])
        && isset($_GET['type'])
        && isset($_GET['address_token'])
        && isset($_GET['customer_token'])
        && isset($_GET['business_hours']))
    {
        $event_tab = [];
        $start = json_decode($_GET['week_start']);
        $end = json_decode($_GET['week_end']);

        $diff = parseStr::get_date_diff($start, $end, "%a");

        $data_generate_calendar = [
            "start" => $start,
            "end" => $end,
            "fullcalendar" => $fullcalendar,
            "user_token" => $_GET['token'],
            "type" => json_decode($_GET['type']),
            "editable" => false,
            "start_date" => $start,
            "db" => $db,
            "start_rec" => json_decode($_GET['start']),
            "end_rec" => json_decode($_GET['end']),
            "business_hours" => json_decode($_GET['business_hours'])
        ];


        App::getPlanning()->generate_calendar_event($data_generate_calendar, $event_tab);
        fill_future_background_event($data_generate_calendar, $event_tab);
        $holidays_customer = $fullcalendar->get_vacation_by_address($_GET['address_token']);
        foreach ($holidays_customer as $index_holidays => $holidays){
            array_push($event_tab, create_holidays_array($holidays, $_GET['customer_token'], false)); // create holidays for week view
        }
        $stop_nobo = App::getCustomer()->getStopNoboByUserToken($_GET['customer_token']);
        if (!empty($stop_nobo))
            array_push($event_tab, create_stop_nobo_array($stop_nobo, $_GET['customer_token'], false, $end));
        $json = parseJson::success('Le planning a bien été chargé', $event_tab);
        $json->printJson();
   }
}