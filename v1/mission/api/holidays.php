<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 16/10/2017
 * Time: 14:42
 */

require_once ('../../../inc/bootstrap.php');
$db = App::getDB();

$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['address_token'])) {
        $address_token = json_decode($_GET['address_token']);
        $holidays = $fullcalendar->get_vacation_by_address($address_token);
        if ($holidays == -1) {
            $json = parseJson::error('Impossible de récupérer les vacances');
        }else{
            $json = parseJson::success('Les vacances ont bien été récupérées', $holidays);
        }
        $json->printJson();
    }
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['address_token']) && isset($_POST['start']) && isset($_POST['end'])) {
        $data = [
            "address_token" => json_decode($_POST['address_token']),
            "start" => json_decode($_POST['start']),
            "end" => json_decode($_POST['end'])
        ];
        $add_holidays = $fullcalendar->add_vacation_by_address($data);
        if ($add_holidays === false){
            $json = parseJson::error('Erreur lors de l\'ajout des vacances');
        }else {
            $json = parseJson::success('Les vacances ont été ajoutées avec succès');
        }
        $json->printJson();
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    if (isset($_DELETE['id'])) {
        $data = [
            "id" => json_decode($_DELETE['id'])
        ];
        $remove_holidays = $fullcalendar->remove_vacation_by_id($data);
        if ($remove_holidays === false) {
            $json = parseJson::error('Erreur lors de la suppression des vacances');
        } else {
            $json = parseJson::success('Les vacances ont été bien été supprimées');
        }
        $json->printJson();
    }
}
