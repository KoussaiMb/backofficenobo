<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 04/10/2017
 * Time: 14:36
 */

require_once ('../../../inc/bootstrap.php');
$db = App::getDB();

$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $pending = $fullcalendar->getAllPendingAgenda();
    if ($pending === false)
        parseJson::error('Erreur lors de la récupération des agendas')->printJson();
    parseJson::success('Les missions ont bien été récupérées', $pending)->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();

    if (!isset($_DELETE['id']))
        parseJson::error('Erreur lors de la récupération des agendas')->printJson();
    $pending = $fullcalendar->delAgendaEventPending($_DELETE['id']);
    if ($pending === false)
        parseJson::error('Erreur lors de la récupération des agendas')->printJson();
    parseJson::success('Les missions ont bien été récupérées', $pending)->printJson();
}