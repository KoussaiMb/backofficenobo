<?php

require_once ('../../../inc/bootstrap.php');

$class_algorithm = App::getSearchAlgorithm();
$fullcalendar = App::getFullCalendar();

function getWeekday($date) {
    return date('w', strtotime($date));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// BLOC CORE DE CHECK DES MISSIONS EN FONCTION DE LA FREQUENCE CHOISI //////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['date_start']) && isset($_POST['duration']) && isset($_POST['frequency']) &&
        isset($_POST['address_token']) && isset($_POST['customer_token']) && isset($_POST['type'])) {

        $data = [
            "frequency" => json_decode($_POST['frequency']),
            "duration" => json_decode($_POST['duration']),
            "address_token" => json_decode($_POST['address_token']),
            "user_token" => json_decode($_POST['customer_token']),
            "type" => json_decode($_POST['type']),
            "date" => str_replace('/', '-', json_decode($_POST['date_start'])),
        ];

        $dispo_client = $fullcalendar->getDispoClient($data);
        if ($dispo_client === false) {
            $json = parseJson::error('Erreur lors de la récupération des disponibilités client');
            $json->printJson();
        }

        foreach ($dispo_client as $key => $value) {
            $data['dispo_client_tab'][$value->day][] = ["day" => $value->day, "start" => $value->start, "end" => $value->end];
        }

        /* if it's a ponctual slot we limit the disponibility to the day choosen */

        if ($data['frequency']->frequencyValue == 3) {
            $ponctual_dispo = [];
            $ponctual_day = getWeekday($data['date']);

            if (isset($data['dispo_client_tab'][$ponctual_day]))
                $ponctual_dispo[$ponctual_day] = $data['dispo_client_tab'][$ponctual_day];
            $data['dispo_client_tab'] = $ponctual_dispo;
        }

        $valid_fdc = $class_algorithm->search_provider($data);
        if ($valid_fdc == -1) {
            Session::getInstance()->setFlash("danger", "Erreur critique, veuillez contacter un administrateur.");
            $json = parseJson::error('Erreur lors de la récupération des femmes de chambre');
        } else if (empty($valid_fdc)) {
            $json = new parseJson('324', 'Aucune femme de chambre disponible', null);
        } else {
            $json = parseJson::success('Les femmes de chambre ont bien été récupérées', $valid_fdc);
        }
        $json->printJson();
    }
}
