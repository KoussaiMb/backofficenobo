<?php
require_once ('../../inc/bootstrap.php');

if($_POST['action'] == 'add'){

    if(!isset($_POST['survey_name_new_q']) || empty($_POST['survey_name_new_q'])){
        App::setFlashAndRedirect('danger', 'Nom du questionnaire incorrect', 'view');
    }

    $redirect_url = 'view?name=' . $_POST['survey_name_new_q'];

    $validator = new Validator($_POST);

    if($_POST['select_ref'] != 0){
        $validator->is_id('select_ref', 'Reference invalide', 'Reference non renseignée');
    }
    $validator->is_id('select_input', 'Type d\'input invalide', 'Type d\'input non renseigné');

    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'La référence ou le type d\'input est invalide', $redirect_url);
    }

    if(empty($_POST['question_text_name']) || !isset($_POST['question_text_name'])){
        App::setFlashAndRedirect('danger', 'La question n\'a pas été renseignée', $redirect_url);
    }

    $validator->is_num($_POST['question_choice_nb']);
    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des choix', $redirect_url);
    }

    $pos = App::getSurvey()->getLastPositionInSurvey($_POST['survey_name_new_q']);
    if(!$pos){
        App::setFlashAndRedirect('danger', 'Impossible de récupérer la position de la question', $redirect_url);
    }

    $ret = App::getSurvey()->insertQuestion($_POST['select_input'], $_POST['select_ref'], $_POST['survey_name_new_q'], $_POST['question_text_name'], ($pos->max_pos+1));
    if(!$ret){
        App::setFlashAndRedirect('danger', 'Impossible d\'insérer la question', $redirect_url);
    }

    $validator->is_num('other_field_bool', 'Le champs Autre est invalide');
    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'Problème avec le champs Autre', $redirect_url);
    }

    $it_choice = $_POST['question_choice_nb'];
    for($i = 1; $i <= $it_choice; $i++){
        $choice_name = 'choice_' . $i;
        $score_name = 'select_score_color_' . $i;

        $curr_score = $_POST[$score_name] ? $_POST[$score_name] : 0;

        if(!empty($_POST[$choice_name])){
            $r = App::getSurvey()->insertOfferedAnswer($ret, $_POST[$choice_name], $curr_score, $i);
            if(!$r){
                App::setFlashAndRedirect('danger', 'Impossible d\'insérer le choix de réponse '. $i, $redirect_url);
            }
        }
    }

    if($_POST['other_field_bool']){
        $r = App::getSurvey()->insertOfferedAnswer($ret, 'Autre', 0, $it_choice+1);
        if(!$r){
            App::setFlashAndRedirect('danger', 'Impossible d\'ajouter le champs Autre', $redirect_url);
        }
    }
    App::setFlashAndRedirect('success', 'Question ajoutée avec succès', $redirect_url);

}

if($_POST['action'] == 'edit'){

    if(!isset($_POST['survey_name_edit_q']) || empty($_POST['survey_name_edit_q'])){
        App::setFlashAndRedirect('danger', 'Nom du questionnaire incorrect', 'view');
    }

    $redirect_url = 'view?name=' . $_POST['survey_name_edit_q'];

    $validator = new Validator($_POST);

    if($_POST['select_ref'] != 0){
        $validator->is_id('select_ref', 'Reference invalide', 'Reference non renseignée');
    }

    $validator->is_id('select_input', 'Type d\'input invalide', 'Type d\'input non renseigné');

    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'La référence ou le type d\'input est invalide', $redirect_url);
    }

    $validator->is_id('question_id', 'Id de la question invalide', 'Id de la question non renseigné');
    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'Impossible de récupérer la question', $redirect_url);
    }

    if(empty($_POST['question_text_name']) || !isset($_POST['question_text_name'])){
        App::setFlashAndRedirect('danger', 'La question n\'a pas été renseignée', $redirect_url);
    }

    $validator->is_num($_POST['question_choice_nb']);
    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'Erreur lors de la récupération des choix', $redirect_url);
    }

    $pos = App::getSurvey()->getLastPositionInSurvey($_POST['survey_name_edit_q']);
    if(!$pos){
        App::setFlashAndRedirect('danger', 'Impossible de récupérer la position de la question', $redirect_url);
    }

    $ret = App::getSurvey()->updateQuestion($_POST['question_id'], $_POST['select_input'], $_POST['select_ref'], $_POST['question_text_name']);
    if(!$ret){
        Session::getInstance()->setFlash('danger', "Echec de la modification du libelé de la question");
    }

    $validator->is_num('other_field_bool', 'Le champs Autre est invalide');
    if(!$validator->is_valid()){
        App::setFlashAndRedirect('danger', 'Problème avec le champs Autre', $redirect_url);
    }

    $it_choice = $_POST['question_choice_nb'];
    for($i = 0; $i < $it_choice; $i++){
        $next_i = $i + 1;
        $choice_name = 'choice_' . $next_i;
        $score_name = 'select_score_color_' . $next_i;
        $off_id = 'off_id_' . $next_i;

        if(!empty($_POST[$choice_name])){
            if(isset($_POST[$off_id])){
                $r = App::getSurvey()->updateOfferedAnswer($_POST[$off_id], $_POST[$choice_name], $_POST[$score_name], $next_i);
                if(!$r){
                    App::setFlashAndRedirect('danger', 'Impossible d\'insérer le choix de réponse '. $i, $redirect_url);
                }
            }else{
                $r = App::getSurvey()->insertOfferedAnswer($_POST['question_id'], $_POST[$choice_name], $_POST[$score_name], $next_i);
                if(!$r){
                    App::setFlashAndRedirect('danger', 'Impossible d\'insérer le choix de réponse '. $i, $redirect_url);
                }
            }
        }
    }
    if($_POST['other_field_bool']){
        $r = App::getSurvey()->insertOfferedAnswer($ret, 'Autre', 0, $it_choice+1);
    }
    App::setFlashAndRedirect('success', 'Question modifiée avec succès', $redirect_url);
}
App::setFlashAndRedirect('danger', 'Impossible de récupérer le questionnaire correspondant', 'view');

