<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();

    if(!isset($_DELETE['rmv_id']) || empty($_DELETE['rmv_id'])){
        $json =  parseJson::error('Impossible de récupérer la réponse à supprimer');
        $json->printJson();
    }

    $pos = App::getSurvey()->getPositionByOfferedAnswerId($_DELETE['rmv_id']);
    if(!$pos){
        $json =  parseJson::error('Impossible de récupérer la position de la question à supprimer');
        $json->printJson();
    }
    $position = $pos->position;

    $q_id = app::getSurvey()->getQuestionIdByOfferedAnswerId($_DELETE['rmv_id']);
    if(!$q_id){
        $json =  parseJson::error('Impossible de récupérer la question correspondante');
        $json->printJson();
    }
    $question_id = $q_id->question_id;

    $last_pos = app::getSurvey()->getLastOfferedAnswerByQuestionId($question_id);
    if(!$last_pos){
        $json =  parseJson::error('Impossible de récupérer la dernière position du questionnaire');
        $json->printJson();
    }
    $last_position = $last_pos->max_pos;

    if($position != $last_pos->max_pos){
        $ret = App::getSurvey()->modifyOfferedAnswerPositionByQuestionId($question_id, $position, -1);
        if(!$ret){
            $json =  parseJson::error('Impossible de modifier les positions des réponses');
            $json->printJson();
        }
    }

    $ret = App::getSurvey()->deleteOfferedAnswer($_DELETE['rmv_id']);
    if(!$ret){
        $json =  parseJson::error('Erreur lors de la suppression de la réponse proposée');
        $json->printJson();
    }

    $json = parseJson::success();
    $json->printJson();
}
?>