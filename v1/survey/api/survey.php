<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();

    if(!isset($_DELETE['survey_name']) || empty($_DELETE['survey_name'])){
        $json =  parseJson::error('Impossible de récupérer le questionnaire à supprimer');
        $json->printJson();
    }

    $ret = App::getSurvey()->halfDeleteSurvey($_DELETE['survey_name']);
    if(!$ret){
        $json =  parseJson::error('Erreur lors de la suppression du questionnaire');
        $json->printJson();
    }
    $json =  parseJson::success();
    $json->printJson();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    if(isset($_GET['list_name']) && !empty($_GET['list_name'])){

        $inputs = App::getListManagement()->getList($_GET['list_name']);
        if(!$inputs){
            $json = parseJson::error('Impossible de récupérer les inputs');
            $json->printJson();
        }
        $refs = App::getSurvey()->getAllFieldsReferences();
        if(!$refs){
            $json = parseJson::error('Impossible de récupérer les références');
            $json->printJson();
        }
        $data['inputs'] = $inputs;
        $data['refs'] = $refs;

        $json = parseJson::success(null, $data);
        $json->printJson();
    }
    $json = parseJson::error('Impossible de récupérer le nom de la liste');
    $json->printJson();
}

if ($_SERVER['REQUEST_METHOD'] === 'EDIT') {
    $_EDIT = App::getRequest();

    if(!isset($_EDIT['current_name']) || empty($_EDIT['current_name'])){
        $json = parseJson::error('Impossible de récupérer le nom actuel');
        $json->printJson();
    }

    if(!isset($_EDIT['new_name']) || empty($_EDIT['new_name'])){
        $json = parseJson::error('Impossible de récupérer le nouveau nom');
        $json->printJson();
    }

    $ret = App::getSurvey()->updateSurveyName($_EDIT['current_name'], $_EDIT['new_name']);
    if(!$ret){
        $json = parseJson::error('Impossible de modifier le nom du questionnaire');
        $json->printJson();
    }
    $json = parseJson::success();
    $json->printJson();
}