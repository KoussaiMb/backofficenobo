<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(!isset($_POST['survey_name']) || !isset($_POST['thresh_data'])){
        parseJson::error()->printJson();
    }

    $survey_name = $_POST['survey_name'];

    $data = array();
    parse_str($_POST['thresh_data'], $data);

    $refusal_thresh = $data['refusal_thresh'];
    $waiting_thresh = $data['waiting_thresh'];
    $accept_thresh = $data['accept_thresh'];

    $surveyClass = App::getSurvey();
    $surveyClass->deleteSurveyThreshold($survey_name);

    $refusal_length = sizeof($refusal_thresh);
    for($i = 0; $i < $refusal_length; $i++){
        $quantity = empty($refusal_thresh[$i]) ? 0 : $refusal_thresh[$i];
        $score = $i;
        $ret = $surveyClass->insertSurveyThreshold($survey_name, $score, $quantity, 'refusal');
        if(!$ret){
            parseJson::error('Impossible d\'insérer l\'un des seuils de refus')->printJson();
        }
    }

    $waiting_length = sizeof($waiting_thresh);
    for($i = 0; $i < $waiting_length; $i++){
        $quantity = empty($waiting_thresh[$i]) ? 0 : $waiting_thresh[$i];
        $score = $i;
        $ret = $surveyClass->insertSurveyThreshold($survey_name, $score, $quantity, 'waiting');
        if(!$ret){
            parseJson::error('Impossible d\'insérer l\'un des seuils de mise en attente')->printJson();
        }
    }

    $accept_length = sizeof($accept_thresh);
    for($i = 0; $i < $accept_length; $i++){
        $quantity = empty($accept_thresh[$i]) ? 0 : $accept_thresh[$i];
        $score = $i;
        $ret = $surveyClass->insertSurveyThreshold($survey_name, $score, $quantity, 'acceptance');
        if(!$ret){
            parseJson::error('Impossible d\'insérer l\'un des seuils d\'acceptation')->printJson();
        }
    }
    parseJson::success()->printJson();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    if(!isset($_GET['survey_name'])){
        parseJson::error()->printJson();
    }
    $survey_name = $_GET['survey_name'];

    $thresholds_refusal = App::getSurvey()->getThresholdBySurveyName($survey_name, 'refusal');
    $thresholds_waiting = App::getSurvey()->getThresholdBySurveyName($survey_name, 'waiting');
    $thresholds_accept = App::getSurvey()->getThresholdBySurveyName($survey_name, 'acceptance');

    if($thresholds_refusal === false || $thresholds_waiting === false || $thresholds_accept === false){
        parseJson::error('Impossible de récupérer les seuils')->printJson();
    }

    $thresholds_refusal = empty($thresholds_refusal) ? array_fill(0, 5, 0) : $thresholds_refusal;
    $thresholds_waiting = empty($thresholds_waiting ) ? array_fill(0, 5, 0) : $thresholds_waiting ;
    $thresholds_accept = empty($thresholds_accept) ? array_fill(0, 5, 0) : $thresholds_accept;

    $thresholds = ["refusal" => $thresholds_refusal, "waiting" => $thresholds_waiting, "acceptance" => $thresholds_accept];
    parseJson::success(null, $thresholds)->printJson();

}