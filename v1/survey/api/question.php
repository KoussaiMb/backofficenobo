<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'EDIT') {
    $_EDIT = App::getRequest();

    $validator = new Validator($_EDIT);

    $validator->is_id('question_id', 'Question invalide', 'Question vide');
    if(!$validator->is_valid()){
        $json = parseJson::error('Impossible de récupérer la question');
        $json->printJson();
    }

    $question = App::getSurvey()->getQuestionAndTypeById($_EDIT['question_id']);
    if(!$question){
        $json = parseJson::error('Impossible de récupérer les références');
        $json->printJson();
    }

    if($question->field == "select" || $question->field == "radio" || $question->field == "checkbox"){
        $offered_answers = App::getSurvey()->getOfferedAnswersByQuestionId($_EDIT['question_id']);
        if(!$offered_answers){
            $json = parseJson::error('Impossible de récupérer les réponses proposées');
            $json->printJson();
        }
    }

    $inputs = App::getListManagement()->getList($_EDIT['list_name']);
    if(!$inputs){
        $json = parseJson::error('Impossible de récupérer les inputs');
        $json->printJson();
    }

    $refs = App::getSurvey()->getAllFieldsReferences();
    if(!$refs){
        $json = parseJson::error('Impossible de récupérer les références');
        $json->printJson();
    }

    $data = array();
    $data['question'] = $question;
    $data['offered_answers'] = "";
    if(isset($offered_answers)){
        $data['offered_answers'] = $offered_answers;
    }
    $data['inputs'] = $inputs;
    $data['refs'] = $refs;

    $json = parseJson::success(null, $data);
    $json->printJson();
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_POST = App::getRequest();

    $validator = new Validator($_POST);

    $validator->is_id('question_id', 'La question à déplacer est invalide', 'Impossible de récupérer la question à déplacer');
    if(!$validator->is_valid()){
        $json =   parseJson::error('La question à déplacer est introuvable');
        $json->printJson();
    }

    if(!isset($_POST['action']) && empty($_POST['action'])){
        $json =   parseJson::error('L\'action n\'est pas définie');
        $json->printJson();
    }

    $question_to_move = App::getSurvey()->getQuestionById($_POST['question_id']);
    if(!$question_to_move){
        $json =   parseJson::error('Impossible de récupérer la question');
        $json->printJson();
    }

    $last = App::getSurvey()->getLastNotDeletedPositionInSurvey($question_to_move->survey_name);
    if(!$last){
        $json =   parseJson::error('Impossible de récupérer le dernier index');
        $json->printJson();
    }

    $position_to_move = $question_to_move->position;
    $position_to_switch = $position_to_move;
    if($_POST['action'] == 'up'){
        if($position_to_move == $last->max_pos){
            $json =   parseJson::error('Impossible de modifier la position (max)');
            $json->printJson();
        }
        $position_to_switch = $position_to_move + 1;
    }else if($_POST['action'] == 'down'){
        if($position_to_move == 1){
            $json =   parseJson::error('Impossible de modifier la position (min)');
            $json->printJson();
        }
        $position_to_switch = $position_to_move - 1;
    }

    $question_to_switch = App::getSurvey()->getQuestionBySurveyNameAndPosition($question_to_move->survey_name, $position_to_switch);
    if(!$question_to_switch){
        $json =   parseJson::error('Impossible de récupérer la question à switcher');
        $json->printJson();
    }

    $rtm = App::getSurvey()->updatePositionByQuestionId($question_to_move->id, $position_to_switch);
    if(!$rtm){
        $json =   parseJson::error('Impossible de modifier la position de la question à changer');
        $json->printJson();
    }

    $rts = App::getSurvey()->updatePositionByQuestionId($question_to_switch->id, $position_to_move);
    if(!$rts){
        $json =   parseJson::error('Impossible de modifier la position de la question à switcher');
        $json->printJson();
    }

    $data['move'] = $question_to_move;
    $data['switch'] = $question_to_switch;

    $json = parseJson::success(null,$data);
    $json->printJson();
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();

    $validator = new Validator($_DELETE);

    $validator->is_id('question_id', 'La question à supprimer est invalide', 'Impossible de récupérer la question à supprimer');
    if(!$validator->is_valid()){
        $json =  parseJson::error('La question à supprimer est introuvable');
        $json->printJson();
    }

    $ret = App::getSurvey()->halfDeleteQuestion($_DELETE['question_id']);
    if(!$ret){
        $json =  parseJson::error('Erreur lors de la suppression de la question');
        $json->printJson();
    }

    $surv_name = App::getSurvey()->getSurveyByQuestionId($_DELETE['question_id']);
    if(!$surv_name){
        $json =  parseJson::error('Impossible de récupérer le nom du questionnaire avec la question');
        $json->printJson();
    }
    $survey_name = $surv_name->survey_name;

    $last_pos = app::getSurvey()->getLastPositionInSurvey($survey_name);
    if(!$last_pos){
        $json =  parseJson::error('Impossible de récupérer la dernière position du questionnaire');
        $json->printJson();
    }
    $last_position = $last_pos->max_pos;

    $pos = App::getSurvey()->getPositionByQuestionId($_DELETE['question_id']);
    if(!$pos){
        $json =  parseJson::error('Impossible de récupérer la position de la question à supprimer');
        $json->printJson();
    }
    $position = $pos->position;

    if($position != $last_position){
        $ret = App::getSurvey()->modifyPositionBySurveyName($survey_name, $position, -1);
        if(!$ret){
            $json =  parseJson::error('Impossible de modifier les positions des questions');
            $json->printJson();
        }

        $ret = App::getSurvey()->updatePositionByQuestionId($_DELETE['question_id'], $last_position);
        if(!$ret){
            $json =  parseJson::error('Impossible de modifier la position de la question supprimée');
            $json->printJson();
        }
    }

    $json =  parseJson::success();
    $json->printJson();
}