<?php
require_once ('../../inc/bootstrap.php');

$surveyNames = App::getSurvey()->getDistinctSurveyNames();
$survey_name = '';
if(isset($_GET['name']) && !empty($_GET['name'])){
    $survey_name = $_GET['name'];
}else{
    if($surveyNames){
        $survey_name = $surveyNames[0]->survey_name;
    }
}
$last_pos = 0;
if(isset($survey_name)){
    $survey = new SurveyBuilder($survey_name);
    $last_pos = App::getSurvey()->getLastPositionInSurvey($survey_name)->max_pos;
}

require_once ('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Questionnaire
        </div>

        <input type="hidden" name="last_position" id="last_position_id" value="<?= $last_pos ?>">
        <input type="hidden" name="survey_name" id="survey_name_id" value="<?= $survey_name ?>">

        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="col-sm-12">
                <div class="col-sm-3 col-sm-offset-3" style="font-size: 15px">
                    <select id="select_survey_name" name="survey_name" class="form-control">
                        <?php
                        foreach($surveyNames as $name){
                            if($name->survey_name === $survey_name) {
                                ?>
                                <option value="<?= $name->survey_name ?>" selected><?= $name->survey_name ?></option>
                                <?php
                            }else{ ?>
                                <option value="<?= $name->survey_name ?>"><?= $name->survey_name ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <div class="btn-group" role="group">
                        <button type="button" name="addSurvey" id="add_survey_btn" class="btn btn-info">Ajouter un questionnaire</button>
                        <button type="button" name="editSurveyName" id="edit_survey_btn" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></button>
                        <button type="button" name="deleteSurveyName" id="delete_survey_btn" class="btn btn-danger"><span class="glyphicon glyphicon-minus"></span></button>
                        <button type="button" class="btn btn-info" id="configure_threshold" name="action" value="edit">Configurer les seuils</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <button type="button" class="btn btn-success" id="addQuestion" name="action" value="add">
                        Ajouter une question <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </div>
            </div>
            <div class="col-sm-12 survey-container" id="preview_div_id" style="margin: 10px">
                <?php
                    if(isset($survey)){
                        echo $survey->getEditCompleteHtml();
                    }
                    ?>
            </div>
        </div>
    </div>


    <style>

        .btn-group button{
            margin: 0px 10px;
        }

        .survey-container{
            margin: 5px;
            padding: 20px;
        }

        .question_div :first-child :nth-child(4) :first-child{
            display: none;
        }

        .question_div :last-child :nth-child(5) :first-child{
            display: none;
        }

        .square{
            width: 25px;
            height: 25px;
        }

    </style>

<script src="../../js/survey.js"></script>
<?php
require('../../inc/footer_bo.php');