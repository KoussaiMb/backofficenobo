<?php
include_once('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/error/404');

$db = App::getDB();

if (!isset($_POST['action']))
    App::redirect('add');

if ($_POST['action'] == 'add') {
    $db = App::getDB();
    $password = my_crypt::genHash($_POST['password']);
    $token = my_crypt::genStandardToken();
    $supp = "";

    if (isset($_POST['who']) && $_POST['who'] == 'governess')
        $supp .= "INSERT INTO `governess` SET `user_id` = LAST_INSERT_ID();";

    $ret = $db->query_log("
INSERT INTO `user` SET `user_token` = ?, `email` = ?, `password` = ?, `firstname` = ?, `lastname` = ?, `who` = ?, `accesslevel` = ?, `pending` = 0, `password_temp` = 1;" . $supp,
        [
            $token,
            $_POST['email'],
            $password,
            $_POST['firstname'],
            $_POST['lastname'],
            $_POST['who'],
            $_POST['accesslevel']
        ]
    );

    if ($ret === false)
        App::setFlashAndRedirect('success', "Something went wrong my lord ...", 'add');
    else
        App::setFlashAndRedirect('danger', "It worked !", 'add');
}