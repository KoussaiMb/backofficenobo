<?php
require('../../inc/bootstrap.php');
require('../../inc/header_bo.php');
?>
<div class="panel panel-danger">
    <div class="panel-heading">
        Manage users because you're badass
    </div>
    <div class="panel-body">
        <div style="width: 60%; margin: auto">
            <form method="post" action="form" class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-12">
                        <?php require('../../inc/print_flash_helper.php');?>
                    </div>
                    <div class="col-xs-12">
                        <label class="col-sm-2 control-label" for="who">type d'utilisateur</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="who" id="who">
                                <!--<option value="customer">client</option>-->
                                <!--<option value="provider">prestataire</option>-->
                                <option value="governess">gouvernante</option>
                                <option value="bo">backoffice</option>
                                <option value="admin">admin</option>
                                <option value="guestrelation">guestrelation</option>
                            </select>
                        </div>
                    </div>
                    <label class="col-sm-2 control-label" for="firstname">firstname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="firstname" placeholder="Prénom" id="firstname" required/>
                    </div>
                    <label class="col-sm-2 control-label" for="lastname">lastname</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="lastname" placeholder="Nom" id="lastname" required/>
                    </div>
                    <label class="col-sm-2 control-label" for="accesslevel">accesslevel</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="accesslevel" placeholder="accesslevel" id="accesslevel" required/>
                    </div>
                    <label class="col-sm-2 control-label" for="email">email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" placeholder="Adresse email" id="email" required/>
                    </div>
                    <label class="col-sm-2 control-label" for="password">Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="password" placeholder="Mot de passe" id="password"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary col-sm-10 col-sm-offset-2" name="action" value="add">Créer un nouveal utilisateur</button>
            </form>
        </div>
    </div>
</div>
<?php
require('../../inc/footer_bo.php');