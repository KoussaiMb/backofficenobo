<?php
require_once('../../inc/bootstrap.php');

Session::getInstance()->upLog('guestrelation_checked');

$guestRelationsSearch = App::getUser()->getAutoComplete("guestrelation");
$guestRelations = App::getUser()->getUsersByWho("guestrelation");

require_once('../../inc/header_bo.php');
?>

<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Rechercher un guest relation:
    </div>
    <div class="panel-body SpaceTop">
        <div class="col-sm-10">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <form class="form-inline">
                <label class="col-sm-2 col-sm-offset-3" for="guestrelationsearch">Rechercher un guest relation</label>
                <input type="hidden" name="id" id="guestrelationsearch_hidden">
                <input type="text" class="form-control" id="guestrelationsearch" placeholder="prenom, nom">
                <button type="button" class="btn btn-default" onclick="window.location = 'edit?user_token=' + $('#guestrelationsearch_hidden').val()">Fiche guest-relation</button>
            </form>
        </div>
        <div class="col-sm-2">
            <a href="add"><button class="btn btn-success" name="action" value="add" style="float: right;"><span class="glyphicon glyphicon-plus"></span></button></a>
        </div>
        <!-- guestrelations processing -->
        <div class="col-sm-12 SpaceTop">
            <h4>Guest Relations</h4>
            <div class="table-responsive">
                <table id="table_guestrelation_pending" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($guestRelations as $key => $row): ?>
                        <tr class="clickable-row" data-href="<?= 'edit?user_token=' . $row->user_token;?>">
                            <td><?= $row->firstname . ' ' . $row->lastname ?></td>
                            <td><?= $row->email ?></td>
                            <td><?= $row->phone ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        var guestrelationsearch = $('#guestrelationsearch');

        guestrelationsearch.devbridgeAutocomplete({
            lookup: <?php if (!empty($guestRelationsSearch)) {echo $guestRelationsSearch;} else {echo "[]";}?>,
            minChars: 1,
            maxHeight: 200,
            onSelect: function (suggestion) {
                $('#guestrelationsearch_hidden').val(suggestion.data);
            }
        });

        guestrelationsearch.keypress(function (e) {
            var key = e.which;

            if(key == 13)
            {
                window.location = 'edit?user_token=' + $('#guestrelationsearch_hidden').val();
                return false;
            }
        });
    });
</script>

<?php
require('../../inc/footer_bo.php');