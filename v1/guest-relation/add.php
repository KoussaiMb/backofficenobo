<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('add-guest-relation');

$usersearch = App::getUser()->getAutoComplete("");

require_once('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Création d'un nouveau guest relation
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <div class="col-sm-6 col-sm-offset-3">
                    <h3>A partir d'un utilisateur:</h3>
                    <form class="form-group">
                        <label for="usersearch">Rechercher un utilisateur</label>
                        <input type="hidden" name="id" id="usersearch_hidden">
                        <input type="text" class="form-control" id="usersearch" placeholder="prenom, nom">
                        <br>
                        <button type="button" class="btn btn-success" onclick="changeWho()">Passer en guest relation</button>
                    </form>
                </div>
            </div>
            <hr>
            <div class="col-sm-12">
                <h3 class="col-sm-8 col-sm-offset-3">Nouvel utilisateur:</h3>
                <div class="col-sm-3">
                    <div class="alert alert-info col-sm-12" role="alert">
                        <p><strong>Tous</strong> les champs sont requis</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <form id="form_new_user" method="post" action="form">
                        <div class="input-group">
                            <div class="input-group-addon"><span>Genre</span></div>
                            <select class="form-control" name="gender_id">
                                <?php $list = App::getListManagement()->getList('gender'); ?>
                                <?php foreach ($list as $k => $v): ?>
                                    <option value="<?= $v->id;?>" <?= isset($data['gender_id']) && $data['gender_id'] == $v->id ? "selected" : ""; ?>><?= $v->field; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                            <input type="text" class="form-control" name="email" value="<?= isset($data['email']) ? $data['email'] : null; ?>" placeholder="email" required/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input type="text" class="form-control" name="firstname" value="<?=  isset($data['firstname']) ? $data['firstname'] : null; ?>" placeholder="Prénom" required/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input type="text" class="form-control" name="lastname" value="<?=  isset($data['lastname']) ? $data['lastname'] : null; ?>" placeholder="Nom" required/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                            <input type="text" class="form-control" name="phone" value="<?=  isset($data['phone']) ? $data['phone'] : null; ?>" placeholder="Téléphone" required/>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                            <input type="text" class="form-control" name="password" placeholder="Mot de passe" id="password" value="<?=  isset($data['password']) ? $data['password'] : null;?>"/>
                            <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="$('#password').val(rand());">Random</button></span>
                        </div>
                        <button type="submit" class="btn btn-success col-sm-6 col-sm-offset-3" name="action" value="add">Créer un nouveau guest relation</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-3">
                <?php require('../../inc/print_flash_create.php'); ?>
            </div>
        </div>
    </div>
    <script src="/js/app.js"></script>
    <script>
        jQuery(document).ready(function() {
            var usersearch = $('#usersearch');

            usersearch.devbridgeAutocomplete({
                lookup: <?php if (!empty($usersearch)) {echo $usersearch;} else {echo "[]";}?>,
                minChars: 1,
                maxHeight: 200,
                onSelect: function (suggestion) {
                    $('#usersearch_hidden').val(suggestion.data);
                }
            });
        });

        function changeWho() {
            new Promise((res, err) => {
                bootbox.confirm({
                    message: `Êtes-vous sure de vouloir passer ${$('#usersearch').val()} en tant que guest relation?`,
                    buttons:{
                        confirm:{
                            label: "Oui",
                            className: "btn-success"
                        },
                        cancel: {
                            label: "Non",
                            className: "btn-danger"
                        }
                    },
                    callback: function(yes){
                        if (yes)
                            res();
                    }
                })
            }).then(() => {
                ajax_call(
                    "api/who",
                    "PUT",
                    {
                        user_token: $('#usersearch_hidden').val(),
                        who: "guestrelation"
                    },
                    function(result){
                        if (result.code == 200) {
                            window.location = 'edit?user_token=' + $('#usersearch_hidden').val();
                        } else {
                            bo_notify(result.message, "danger");
                        }
                    }
                )
            }).catch(err => {
                console.log(err);
                bo_notify("Une erreur est survenue", "danger");
            })
        }
    </script>
<?php
require('../../inc/footer_bo.php');
