<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 13/09/2017
 * Time: 17:00
 */

require_once ('../../../inc/bootstrap.php');

$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);
    $end_rec = null;

    $validator->isToken('user_token', 'Le token est invalide', 'le token est introuvable');
    $validator->is_num('day', 'Le jour est invalide', 'le jour est introuvable');
    $validator->is_timeFormat('start', 'L\'heure de début est invalide', 'L\'heure de début est introuvable');
    $validator->is_timeFormat('end', 'L\'heure de fin est invalide', 'L\'heure de fin est introuvable');
    $validator->is_date_bo('start_rec', 'La date de début est invalide', 'La date de début est introuvable');
    $validator->is_stringRange('commentary', 'Le commentaire est invalide', 'Le commentaire est introuvable', [0,300]);

    if (!$validator->is_valid())
        parseJson::error('Le informations de la disponibilités sont incorrectes', $validator->getErrors())->printJson();

    $validator->is_date_bo('end_rec', 'La date de fin est invalide', 'La date de fin est introuvable');
    if ($validator->is_valid())
        $end_rec = $_POST['end_rec'];

    $event = array (
        "category" => 1,
        "day" => $_POST['day'],
        "start" => $_POST['start'],
        "end" => $_POST['end'],
        "start_rec" => $_POST['start_rec'],
        "end_rec" => $end_rec,
        "title" => "Disponibilité pour premier rendez-vous",
        "available" => 1,
        "commentary" => $_POST['commentary'],
        "id" => 0
    );

    $conflicts = $fullcalendar->lookForConflicts($event, $_POST['user_token']);

    if(count($conflicts) > 0) 
        parseJson::error('Il y a des conflits de disponibilités', $conflicts)->printJson();
    

    $event_id = $fullcalendar->addNewEvent($event);

    if(!$event_id)
        parseJson::error('Erreur lors de la création de votre créneau')->printJson();

    if(!$fullcalendar->addEvent($_POST['user_token'], $event_id))
    {
        $fullcalendar->deleteEventById($event_id);
        parseJson::error('Erreur lors de l\'attribution du créneau à l\'utilisateur')->printJson();
    }

    $newEvent = $fullcalendar->getEventById($event_id);
    $newEvent->attendess = [$_POST['user_token']];

    parseJson::success('Le créneau a bien été crée', $newEvent)->printJson();

} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = App::get_instance_validator($_GET);
    $json_data = [];

    $validator->isToken('user_token', 'Le token est invalide', 'Le token est introuvable');
    $validator->is_date_bo('start', 'La date de début est invalide', 'La date de début est introuvable');
    $validator->is_date_bo('end', 'La date de fin est invalide', 'La date de fin est introuvable');
    if (!$validator->is_valid())
        parseJson::error('Les informations pour récupérer les disponibilités sont corompues', $validator->getErrors())->printJson();

    $dispos = $fullcalendar->getEventsByUserByWeek($_GET['user_token'], $_GET['start'], $_GET['end']);
    if ($dispos === false)
        parseJson::error('Erreur lors de la récupération des disponibilité')->printJson();

    foreach ($dispos as $key => $value){
        $start_time = explode(":", $value->start);
        $end_time = explode(":", $value->end);
        $nextday = date('Y-m-d H:i:s', strtotime('+'.($value->day - 1)." days", strtotime($_GET['start'])));
        $address = $fullcalendar->getFirstAddressByEventId($value->id);
        $attendees = $fullcalendar->getUserTokensByEventId($value->id);

        if ($nextday > $value->start_rec && (is_null($value->end_rec) || $nextday < $value->end_rec) && $attendees) {
            $startTime = date('Y-m-d\TH:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
            $endTime = date('Y-m-d\TH:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
            array_push($json_data, array(
              "start" => $startTime,
              "end" => $endTime,
              "id" => $value->id,
              "color" => ($value->available == 1) ? "" : "red",
              "title" => $value->title,
              "commentary" => $value->commentary,
              "start_rec" => $value->start_rec,
              "end_rec" => $value->end_rec,
              "overlap" => false,
              "available" => $value->available == 1,
              "attendees" => $attendees,
              "address" => $address ? App::getAddress()->addressToString($address->id) : "Aucune adresse",
              "address_id" => $address ? $address->id : 0
            ));
        }
    }

    parseJson::success('Les disponibilitées on bien été récuperées', $json_data)->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);
    $end_rec = null;

    $validator->is_timeFormat('start', 'L\'heure de début est invalide', 'L\'heure de début est introuvable');
    $validator->is_timeFormat('end', 'L\'heure de fin est invalide', 'L\'heure de fin est introuvable');
    $validator->is_id('id', 'L\'id de la disponibilité est invalide', 'L\'id de la disponibilité est introuvable');
    $validator->is_num('day', 'Le jour est invalide', 'Le jour est introuvable');
    $validator->is_date_bo('start_rec', 'La date de début est invalide', 'La date de début est introuvable');
    $validator->is_stringRange('commentary', 'Le commentaire est invalide', 'Le commentaire est introuvable', [0,300]);
    $validator->isToken('user_token', 'Le token est invalide', 'Le token est introuvable');

    if (!$validator->is_valid())
        parseJson::error('Les informations pour modifier la disponibilité sont incorrectes', $validator->getErrors())->printJson();

    $validator->is_date_bo('end_rec', 'La date de fin est invalide', 'La date de fin est introuvable');
    if ($validator->is_valid())
        $end_rec = $_PUT['end_rec'];

    $event = array(
        "start" => $_PUT['start'],
        "end" => $_PUT['end'],
        "id" => $_PUT['id'],
        "day" => $_PUT['day'],
        "start_rec" => $_PUT['start_rec'],
        "end_rec" => $end_rec,
        "commentary" => $_PUT['commentary']
    );

    $conflicts = $fullcalendar->lookForConflicts($event, $_PUT['user_token']);

    if (count($conflicts) > 0) 
        parseJson::error('Il y a des conflits de disponibilités', $conflicts)->printJson();

    $return = $fullcalendar->updateEventOnWeek($event);
    if($return === false){
        parseJson::error('Erreur lors de l\'édition du créneau')->printJson();
    }
    parseJson::success('Le créneau a bien été édité')->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('id', "l'id de la disponibilité est invalide", "L'id de la disponibilité est introuvable");
    $validator->is_num('all', "La durée de suppression est invalide", "La durée de suppression est introuvable");
    $validator->is_date_bo('date', "La date de suppression est invalide", "La date de suppression est introuvable");
    if (!$validator->is_valid())
        parseJson::error('Les informations pour supprimer la disponibilité sont incorrectes', $validator->getErrors())->printJson();

    if ($_DELETE['all'] == 1) {
        if (!$fullcalendar->deleteEventById($_DELETE['id']))
            parseJson::error('Erreur lors de la suppression du créneau')->printJson();
    } else {
        try{
            $event = $fullcalendar->getEventById($_DELETE['id']);
            $event_start = new DateTime($event->start_rec);
            $event_end = new DateTime($event->end_rec);

            if ($event_start->diff($event_end, true)->format('%a') <= 7 && !is_null($event->end_rec)) {
                if (!$fullcalendar->deleteEventById($_DELETE['id']))
                    parseJson::error('Erreur lors de la suppression du créneau')->printJson();
            } else if ($event_start->diff(new DateTime($_DELETE['date']))->format('%a') <= 7) {
                $event->start_rec = date("Y-m-d", strtotime("+7 days", strtotime($event->start_rec)));
                if (!$fullcalendar->updateEvent((array)$event))
                    parseJson::error('Erreur lors de la modification du créneau', $e)->printJson();
            } elseif ($event_end->diff(new DateTime($_DELETE['date']))->format('%a') <= 7  && !is_null($event->end_rec)) {
                $event->end_rec = date("Y-m-d", strtotime("-7 days", strtotime($event->end_rec)));
                if (!$fullcalendar->updateEvent((array)$event))
                    parseJson::error('Erreur lors de la modification du créneau', $e)->printJson();
            } else {
                $users = $fullcalendar->getUsersByEventId($_DELETE['id']);
                $category_position = App::getListManagement()->getPositionById($event->l_category_event_id);
                $newDispo_data = (array)$event;
                $newDispo_data["start_rec"] = date("Y-m-d", strtotime("+1 days", strtotime($_DELETE['date'])));
                $newDispo_data["category"] = $category_position;
                $newDispo_id = $fullcalendar->addNewEvent($newDispo_data);
                foreach ($users as $key => $value) {
                    $fullcalendar->addEvent($value->user_token, $newDispo_id);
                }
                $event->end_rec = date("Y-m-d", strtotime("-1 days", strtotime($_DELETE['date'])));
                if (!$fullcalendar->updateEvent((array)$event)){
                    $fullcalendar->deleteEventById($newDispo_id);
                    parseJson::error('Erreur lors de la modification du créneau', $e)->printJson();
                }
                    
            }
        }catch(Exception $e) {
            parseJson::error('Erreur lors de la suppression du créneau', $e)->printJson();
        }
    }

    parseJson::success('Le créneau a bien été supprimer', $_DELETE['id'])->printJson();
}
