<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();

    $validator = New Validator($_PUT);

    $validator->isToken("user_token", "L'utilisateur n'est pas valide", "L'utilisateur est introuvable");
    $validator->no_symbol("password", "Le mot de passe est invalide", "Le mot de passe est introuvable");

    if (!$validator->is_valid()) {
        $json = parseJson::error(null, $validator->getErrors());
        $json->printJson();
    }

    $password = my_crypt::genHash($_PUT['password']);
    $ret = App::getUser()->updatePassword($password, $_PUT['user_token']);
    if ($ret)
        $json = parseJson::success("Le mot de passe a bien été réinitialisé :)", ["message_color" => "success"]);
    else
        $json = parseJson::error("Le mot de passe n'a pas été réinitialisé :/", ["message_color" => "danger"]);
    $json->printJson();
}


