<?php 

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_id('address_id', 'l\'id de l\'adresse est invalides', 'l\'id de l\'adresse est introuvables');

    if(!$validator->is_valid())
        parseJson::error($validator->getError('address_id'))->printJson();

    $address = App::getAddress()->getAddressbyId($_GET['address_id']);

    if (!$address)
        parseJson::error("Impossible de récupérer les informations sur l'adresse")->printJson();

    parseJson::success('Les informations on bien été récupéré', $address)->printJson();
}