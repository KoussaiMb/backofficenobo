<?php 

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_array('tokens', 'Les tokens sont invalides', 'les tokens sont introuvables');

    if(!$validator->is_valid())
        parseJson::error($validator->getError('tokens'))->printJson();

    $tokens = json_decode($_GET['tokens']);
    $infos = [];

    foreach ($tokens as $key => $value) {
        $user_info = App::getUser()->getUserByToken($value);

        if (!$user_info) 
            parseJson::error("Impossible de récupérer les informations des clients")->printJson();

        if ($user_info->who != "customer")
            array_push($infos, $user_info);
    }

    
    
    parseJson::success('Les informations on bien été récupéré', $infos)->printJson();
}