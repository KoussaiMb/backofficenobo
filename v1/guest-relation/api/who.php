<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();

    $validator = New Validator($_PUT);

    $validator->isToken("user_token", "L'utilisateur n'est pas valide", "L'utilisateur est introuvable");
    $validator->no_symbol("who", "Le type d'utilisateur est invalide", "Le type d'utilisateur est introuvable");

    if (!$validator->is_valid()) {
        parseJson::error("Il manque des informations", $validator->getErrors())->printJson();
    }

    if(!App::getUser()->updateUserWho($_PUT['user_token'], $_PUT['who']))
        parseJson::error("Erreur lors du changement de type d'utilisateur")->printJson();

    parseJson::success("Le changement de type d'utilisateur à bien été effectué")->printJson();

}
