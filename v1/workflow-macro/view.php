<?php
require('../../inc/bootstrap.php');
require('../../inc/header_bo.php');

$reception_steps = App::getWorkflow()->getStepsByName("reception");

?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Workflow Macro
    </div>
    <div class="panel-body">
        <div class="flex-container row">
            <div class="col-sm-2">
                <div class="form-group check-filter">
                    <input type="checkbox" class="form-check-input" name="call_request_1" id="call_request_1" checked>
                    <label for="call_request_1" class="form-check-label">Rendez-vous planifié <span class="around-margin glyphicon glyphicon-stop" style="color: #ff751a; font-size: 1.5em"></span></label>
                </div>
                <div class="form-group check-filter">
                     <input type="checkbox" class="form-check-input" name="call_request_2" id="call_request_2" checked>
                     <label for="call_request_2" class="form-check-label">Relance <span class="around-margin glyphicon glyphicon-stop" style="color: #ff884d; font-size: 1.5em"></span></label>                   
                </div>
                <div class="form-group check-filter">
                    <input type="checkbox" class="form-check-input" name="alerte" id="alerte" checked>
                    <label for="alerte" class="form-check-label">Alerte <span class="around-margin glyphicon glyphicon-stop" style="color: #ff3333; font-size: 1.5em"></span></label>
                </div>               
            </div>
            <div class="col-sm-4 text-center">
                <button type="button" class="btn btn-outline-primary btn-filter reception" data-workflow="reception">Réception</button>
                <div class="flex-step text-center">
                    <?php
                        foreach ($reception_steps as $key => $value) {
                            ?>
                                <div class="step-circle" data-val="<?= $value->step ?>"><?= $value->step ?><span><?= $value->step_name ?></span></div>
                            <?php
                        }
                    ?>
                </div>
            </div>
            <div class="col-sm-4 text-center">
                <button type="button" class="btn btn-outline-primary btn-filter guest-relation" data-workflow="guest-relation">Guest relation</button>
                <div class="flex-step text-center">
                    <div class="step-circle" data-val="5">5<span>indisponible</span></div>
                    <div class="step-circle" data-val="6">6<span>indisponible</span></div>
                    <div class="step-circle" data-val="7">7<span>indisponible</span></div>
                    <div class="step-circle" data-val="8">8<span>indisponible</span></div>
                </div>
            </div>
            <div class="col-sm-2 form-group">
                <label for="order_id">Trier par: </label>
                <select class="form-control" name="order_id" id="order_id">
                    <option value="0">Date d'entrée décroissant</option>
                    <option value="1">Date d'entrée croissant</option>
                    <option value="2">Par etape décroissante</option>
                    <option value="3">Par etape croissant</option>
                    <option value="4">Dernier appel décroissant</option>
                    <option value="5">Dernier appel croissant</option>
                </select>
            </div>
        </div>
        <hr style="margin-top: 5px; margin-bottom: 5px;">
        <div class="flex-container row" style="padding-left: 5px;padding-right: 5px" id="customers">

        </div>
    </div>
</div>


<style>
.check-filter{
    width: 100%;
    margin: 0;
    font-size: 0.9em;
}

.btn-filter{
    width: 100%;
    margin-bottom: 10px;
}

.step-circle{
    border: 1px solid lightblue;
    width: 30px;
    height: 30px;
    padding-top: 6px;
    border-radius: 50%;
    text-align: center;
    cursor: pointer;
}

.step-circle.active{
    background: #dff0d8;
    background-image: linear-gradient(to bottom,#dff0d8 0,#d0e9c6 100%);
}

.step-circle span {
  position: absolute;
  width:140px;
  color: #3c763d;
  background: #dff0d8;
  background-image: linear-gradient(to bottom,#dff0d8 0,#d0e9c6 100%);
  border: 1px solid ##d6e9c6;
  line-height: 30px;
  text-align: center;
  visibility: hidden;
  border-radius: 6px;
}
.step-circle span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -8px;
  width: 0; height: 0;
  border-top: 8px solid #dff0d8;
  border-right: 8px solid transparent;
  border-left: 8px solid transparent;
}
.step-circle:hover span {
  visibility: visible;
  bottom: 30px;
  margin-left: -76px;
  z-index: 999;
}

.customer-thumb {
    text-align: left;
    display: block;
    padding: 0;
    margin-bottom: 10px;
    border-radius: 0;
    cursor: pointer;
    border: lightgrey solid 1px;
    padding-top: 5px;
    line-height: 17px;
    font-weight: normal;
    font-size: 0.8em;
    margin: 1px;
}

.customer-thumb p {
    margin-bottom: 0px;
}

.btn.guest-relation{
    background-color: #ccffcc;
    color: black;
}

.btn.reception{
    background-color: #ccddff;
    color: black;
}

.guest-relation .customer-thumb {
    background-color: #ccffcc;
    color: black;
}

.reception .customer-thumb {
    background-color: #ccddff;
    color: black;
}

.alerte .customer-thumb {
    background-color: #ff3333;
    color: white;
}

.call_request_2 .customer-thumb {
    background-color: #ff884d;
    color: white;
}

.call_request_1 .customer-thumb{
    background-color: #ff751a;
    color: white;
} 

.pending .customer-thumb{
    background-color: lightgrey;
    color: black;
} 

.flex-step {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-align-content: stretch;
    -ms-flex-line-pack: stretch;
    align-content: stretch;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
}

.btn.filter-disabled {
    background-color: lightgrey;
    color: black;
    cursor: pointer;
}

.step-thumb{
    position: absolute;
    left: 2px;
    top: 2px;
    padding-left: 3px;
    padding-right: 3px;
    font-size: 1.2em;
    background-color: lightgrey;
    color: black;
}

.info-thumb{
    position: absolute;
    right: 2px;
    bottom: 2px;
    padding-left: 4px;
    padding-right: 4px;
    font-size: 1.5em;
    background-color: green;
    color: white;
    border: none;
}

.surface-thumb{
    position: absolute;
    right: 2px;
    top: 2px;
    padding-left: 4px;
    padding-right: 4px;
    font-size: 1.2em;
    background-color: white;
    color: black;
    border: none;
}
</style>
<script src="/js/app.js"></script>
<script src="/js/reception.js"></script>
<script src="/js/workflow.js"></script>
<?php
require('../../inc/footer_bo.php');
