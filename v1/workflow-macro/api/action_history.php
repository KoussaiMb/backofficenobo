<?php

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_id('user_token', 'Le token est invalide', 'Le token est manquant');
    if (!$validator->is_valid()) 
        parseJson::error($validator->getErrorString('user_token'))->printJson();
    
    $history = App::getCustomerActions()->getActionHistoryByToToken($_GET['user_token']);

    if ($history === false) 
        parseJson::error('Erreur lors de la récupération de l\'historique')->printJson();

    parseJson::success('L\'historique a bien été récuperé', $history)->printJson();
}