<?php require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);
    $filters = [
        "w.date_list_in DESC",
        "w.date_list_in ASC",
        "wf.step DESC",
        "wf.step ASC",
        "last_call DESC",
        "last_call ASC"
    ];

    $validator->is_range('filter', 'Le filtre est incorrect', [-1, count($filters)]);
    if (!$validator->is_valid())
        parseJson::error($validator->getError('filter'))->printJson();

    $customers = App::getWorkflow()->getAllCustomersInWaitingList($filters[$_GET['filter']]);
    if (!$customers)
        parseJson::error("Erreur lors de la récupération de la liste des clients")->printJson();

    parseJson::success('La liste des clients à bien été récupéré', $customers)->printJson();
} elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('w_id', 'L\'id est invalide', 'L\'id est manquant');
    $validator->is_num('alerte', 'L\'alerte est invalide', 'L\'alerte est manquant');
    if (!$validator->is_valid())
        parseJson::error("Les informations sont incorrectes", $validator->getErrors())->printJson();

    
    if (!App::getWorkflow()->updateWaitingListAlerte($_PUT['w_id'], $_PUT['alerte']))
        parseJson::error("Erreur lors de la modification du client")->printJson();

    parseJson::success('La modification à bien été effectuée')->printJson();
}
