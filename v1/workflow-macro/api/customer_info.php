<?php

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_id('w_list_id', 'L\'id est invalide', 'L\'id est manquant');
    if (!$validator->is_valid()) 
        parseJson::error($validator->getErrorString('user_token'))->printJson();

    $customer_info = App::getWorkflow()->getCustomerByWaitingId($_GET['w_list_id']);
    if ($customer_info == false) 
        parseJson::error('Erreur lors de la récupération des informations du client')->printJson();

    //can be null
    $last_call = App::getCustomerActions()->getLastActionByUserIdAndType($customer_info->user_id, 1);
    
    $history = App::getCustomerActions()->getActionHistoryByToToken($customer_info->user_token);
    if ($history === false) 
        parseJson::error('Erreur lors de la récupération de l\'historique')->printJson();

    if ($customer_info->recurrence_id) {
        $customer_info->recurrence = App::getListManagement()->getFieldById($customer_info->recurrence_id);
    }

    if ($customer_info->l_acquisition_id) {
        $customer_info->acquisition = App::getListManagement()->getFieldById($customer_info->l_acquisition_id);
    }

    $customer_info->last_call = $last_call;
    $customer_info->actionHistory = $history;

    parseJson::success('Les informations du client ont bien été récuperé', $customer_info)->printJson();
}