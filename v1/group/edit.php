<?php
require('../../inc/bootstrap.php');

if(App::getAuth()->who() != 'admin')
    App::redirect('/');

$manageGroups = App::getManageGroup();
$primary_groups = $manageGroups->getGroupsWithChildrens();
$users_alone = $manageGroups->usersWithoutGroups();
$users_grouped = $manageGroups->usersWithGroup();
$users = array_merge($users_alone, $users_grouped);
$menus = App::getMenuManager()->getAllMenus();

if (isset($_POST['target_id']) && isset($_POST['target_type'])) {
    $target_id = $_POST['target_id'];
    $target_type = $_POST['target_type'];
} else {
    $target_id = '0';
    $target_type = '';
}

function printGroups($groups, $childCount, $target_type, $target_id) {
    foreach ($groups as $k => $v): ?>
        <div class="col-xs-12 user-group <?= ($target_type == "group" && $target_id == $v->id) ? 'group-selected' : '' ?> ">
            <input type="hidden" name="group_id" value="<?= $v->id ?>">
            <?= str_repeat("—", $childCount) ?> <?= $v->name ?>
            <button class="spanButton pull-right btn-select" name="action" value="select_group"><span class="glyphicon glyphicon-pencil"></span></button>
        </div>
        <?php
        if (isset($v->childrens))
            printGroups($v->childrens, $childCount + 1, $target_type, $target_id);
        ?>
    <?php endforeach;
}

require('../../inc/header_bo.php');
?>
<link type='text/css' rel='stylesheet' href='/css/group.css'/>
<div class="col-xs-12">
    <?php include('../../inc/print_flash_helper.php'); ?>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        Permissions des groupes
    </div>
    <div class="panel-body">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-xs text-center">
                    Groupes
                </div>
                <div class="panel-body noSpace view-container outer-users">
                    <?php printGroups($primary_groups, 0, $target_type, $target_id); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-xs text-center">
                    Utilisateurs
                </div>
                <div class="panel-body noSpace view-container outer-users">
                    <?php foreach ($users as $k): ?>
                        <div class="col-xs-12 user-group">
                            <input type="hidden" name="user_token" value="<?= $k->user_token; ?>">
                            <?= $k->firstname ?> <?=$k->lastname; ?>
                            <button class="spanButton pull-right btn-select" name="action" value="select_user"><span class="glyphicon glyphicon-pencil"></span></button>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-xs text-center">
                    Menus
                </div>
                <div class="panel-body view-container outer-users" style="height: 70px">
                    <select class="form-control input-sm groupSelect" name="menu_id" id="menu">
                        <?php foreach ($menus as $k => $v): ?>
                            <option value="<?= $v->id ?>"><?= $v->menu_name ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-xs text-center">
                    Permissions
                </div>
                <div class="panel-body noSpace view-container outer-users" id="options" style="height: 290px"></div>
                <?php if (App::getAuth()->who() == "admin") : ?>
                    <button type="button" name="add_option" class="btn btn-success col-xs-12" onclick="$('#modal_option').modal()">Ajouter une option</button>
                    <div id="modal_option" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                    <h4 class="modal-title">Ajouter une option</h4>
                                </div>
                                <div id="modal_option_Body" class="modal-body">
                                    <label for="option_name">Nom de l'option</label>
                                    <input type="text" name="option_name" id="option_name" value="">
                                    <label for="option_value">Valeur de l'option</label>
                                    <input type="text" name="option_value" id="option_value" value="">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    <button type="button" class="btn btn-success" data-href="#" data-toggle="modal" onclick="addOption()">Ajouter l'option</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script src="/js/app.js"></script>
<script src="/js/rights.js"></script>
<script>
    let targetId = <?php echo json_encode($target_id); ?>;
    let targetType = <?php echo json_encode($target_type); ?>;
    let menu = $('#menu');

    jQuery(document).ready(function() {
        loadOptionByMenu(menu.val());
        menu.on('change', function(){loadOptionByMenu(menu.val());});
    });

    $(document).on('change', 'input[type=checkbox]', function () {
        if ($(this).prop("checked") && $(this).hasClass('disabled') && targetType === "group") {
            bo_notify("Vous ne pouvez pas changer cette permission car elle dépend du parent", "danger");
            loadOptionByMenu(menu.val());
            return;
        }

        changeOption(Number($(this).data('id')), $(this).prop("checked"));

        let parent = $(this).parent();
        parent.children().each(function() {
            $(this).removeClass('disabled');
        });
    });

    $(document).on('click', '.btn-select', function () {
        let parent = $(this).parent();
        let type = $(this).val();

        if (type === "select_group") {
            targetType = "group";
            targetId = parent.find('[name=group_id]').val()
        } else if (type === "select_user") {
            targetType = "user";
            targetId = parent.find('[name=user_token]').val();
        }

        $( ".user-group" ).each(function() {
            $(this).removeClass('group-selected');
        });
        parent.addClass('group-selected');

        loadOptionByMenu(menu.val());
    });

    $(document).on('click', '.remove-right', function () {
        let optionId = Number($(this).val());
        removeUserRight(optionId);
    });

</script>
<?php
require_once("../../inc/footer_bo.php");
