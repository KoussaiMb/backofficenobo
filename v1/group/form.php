<?php
include_once('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/');

$db = App::getDB();

if (!isset($_POST['action']))
    App::redirect('view');


if($_POST['action'] == 'add') {
    $validator = new Validator($_POST);

}
elseif ($_POST['action'] == 'edit') {

}
elseif ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);
    $validator->is_digit('id');
}
Session::getInstance()->delete('formData');
App::redirect('view');
