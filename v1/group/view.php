<?php
require('../../inc/bootstrap.php');

if(App::getAuth()->who() != 'admin'){
    App::redirect('/');
}

$manageGroups = App::getManageGroup();
$groups = $manageGroups->groupsAsOption();
$users_alone = $manageGroups->usersWithoutGroups();
$users_grouped = $manageGroups->usersWithGroup();
//$features = $manageGroups->getFeatures();

if (empty($groups))
    Session::getInstance()->setFlash('info', 'Aucun groupe n\'a été trouvé');

require('../../inc/header_bo.php');
?>
<div class="col-xs-12">
    <?php include('../../inc/print_flash_helper.php'); ?>
</div>
<!-- Groups -->
<div class="col-xs-12 noSpace">
      <div class="panel panel-primary">
          <div class="panel-heading panel-heading-xs text-center">
              Groupes
          </div>
          <div class="panel-body">
              <div class="col-xs-12 text-center noSpace">
                <div class="col-sm-6">
                  <select class="form-control input-sm groupSelect" name="group_id" id="group">
                    <?php echo $groups ?>
                  </select>
                </div>
                <div class="col-sm-6">
                  <div class="btn-group">
                      <button type="button" class="btn btn-default" onclick="alert('WIP')"><span class="glyphicon glyphicon-eye-open"></span></button>
                      <button type="button" class="btn btn-warning groupButton hidden" name="action" value="put"><span class="glyphicon glyphicon-pencil"></span></button>
                      <button type="button" class="btn btn-success groupButton" name="action" value="post"><span class="glyphicon glyphicon-plus"></span></button>
                      <button type="button" class="btn btn-danger groupButton" name="action" value="delete"><span class="glyphicon glyphicon-remove"></span></button>
                      <button type="button" class="btn btn-default" onclick="editGroup()">Edition des droits</button>
                  </div>
                </div>
              </div>
          </div>
    </div>
</div>
<!-- Users -->
<div class="col-xs-12 noSpace">
    <div class="col-xs-12 col-sm-6 noSpace">
      <div class="panel panel-default">
        <div class="panel-heading noSpace panel-heading-xs text-center">
          Utilisateurs
        </div>
        <div style="padding: 15px">

        <div class="panel panel-default">
            <div class="panel-heading panel-heading-xs text-center">
                Sans groupe
            </div>
            <div class="panel-body noSpace view-container outer-users">
                <?php foreach ($users_alone as $k): ?>
                    <div class="col-xs-12 user-group">
                        <input type="hidden" name="user_token" value="<?= $k->user_token; ?>">
                        <input type="hidden" name="firstname" value="<?= $k->firstname; ?>">
                        <input type="hidden" name="lastname" value="<?= $k->lastname; ?>">
                        <?= $k->firstname ?> <?=$k->lastname; ?>
                        <button class="spanButton pull-right" name="action" value="add"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="panel panel-default" style="margin: 0">
            <div class="panel-heading panel-heading-xs text-center">
                Dans un groupe
            </div>
            <div class="panel-body view-container search-user text-center">
                <input type="hidden" name="user_token">
                <input type="hidden" class="firstname" name="firstname">
                <input type="hidden" class="lastname" name="lastname">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Rechercher un utilisateur..." id="usersearch">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div><!-- /input-group -->
                <button class="btn btn-success SpaceTop" name="action" value="add" id="addUserFromGroup">Ajouter l'utilisateur</button>
            </div>
        </div>
      </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6" style="padding-left: 8px; padding-right: 0">
        <div class="panel panel-warning">
            <div class="panel-heading panel-heading-xs text-center">
                Utilisateurs dans le groupe
            </div>
            <div class="panel-body noSpace view-container inner-users group_view">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="formModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form method='post' action='form' id="formGroup">
                    <label for="name">Nom du groupe</label>
                    <input type='text' class='form-control' name='groupName' id="groupName" required>
                    <label for="description">Description du groupe</label>
                    <input type='text' class='form-control' name='description' id="description" required>
                    <label for="parent_id">Groupe parent</label>
                    <select name="parent_id" class="groupSelect form-control" id="parent_id">
                        <?php echo $groups; ?>
                    </select>
                    <div id="deleteChildren">
                        <label>Supprimer toute la branche ?</label>
                        <input type="checkbox" name="delete_children">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" name="group" value="" id="submit">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    .view-container {
        overflow-y: auto;
    }
    .table-rights>tbody>tr>td:first-child {
        overflow-x: hidden;
        text-align: left;
    }
    .table-rights>thead>tr>th {
        background-color: whitesmoke;
    }
    .table-fixed {
        width: 100%;
    }
    .table-fixed>tbody>tr>td{
        float: left;
    }
    .table-fixed>thead>tr {
        padding-right: 15px;
    }
    .table-fixed>thead>tr>th{
        float: left;
    }
    .table-fixed>tbody{
        overflow-y: scroll;
        width: 100%;
    }
    .table-fixed>thead, .table-fixed>tbody,
    .table-fixed tr, .table-fixed td, .table-fixed th{
        display:block;
    }
    .table-rights {
        text-align: center;
    }
    .table-xs {
        margin: 0;
        padding: 0;
    }
    .table-xs>tbody>tr>td, .table-xs>thead>tr>th {
        width: 12%;
        padding: 2px 5px;
    }
    .table-xs>tbody>tr>td:first-child,
    .table-xs>thead>tr>th:first-child {
        width: 52%;
    }
    .user-group:nth-child(odd) {
        background-color: lightgrey;
    }
    .user-group {
        padding: 3px 15px;
    }
    .spanButton {
        background-color: inherit;
        border: none;
    }
</style>
<script src="/js/app.js"></script>
<script src="/js/group.js"></script>
<script>
    jQuery(document).ready(function() {
        let modal = $('#formModal');
        let formGroup = $('#formGroup');
        let submitBtn = $('#submit');
        let user_grouped = $('#usersearch_hidden');
        let group_sel = $('#group');
        let groupInfo = initGroupInfo();
        let editGroupButton = $('button[name="action"][value="put"]');

        group_sel.on('change', function () {
            if ($(this).val() !== '0') {
                getGroupInfo($(this).val(), groupInfo);
                editGroupButton.removeClass('hidden');
            } else {
                editGroupButton.addClass('hidden');
            }
            printGroupInnerUsers($('.inner-users'), $(this).val());
        }).change();

        $('.groupButton').on('click', function () {
            let btnVal = $(this).val();
            let group_id = $('#group').val();
            //third methods row is about what to lock TODO:Make objects
            let methods = {
                'post' : ['ajouter', 'success', []],
                'put' : ['editer', 'primary', ['parent_id']],//TODO: remove parent_id here when rights are implemented
                'delete': ['supprimer', 'danger', ['groupName', 'description', 'parent_id']]
            };

            if (btnVal === 'delete')
                $('#deleteChildren').prop('checked', false).show();
            else
                $('#deleteChildren').prop('checked', false).hide();

            lockForm(formGroup, methods[btnVal][2]);
            changeBtn(submitBtn, btnVal, methods[btnVal][0], methods[btnVal][1]);
            fillForm(formGroup, btnVal !== 'post' ? groupInfo : initGroupInfo());
            if (group_id !== 0 || group_id === 0 && btnVal === 'post')
                modal.modal('show');
        });
        submitBtn.on('click', function () {
            let callType = $(this).val();
            let calls = ['post', 'put', 'delete'];
            let data = {
                name: formGroup.find('[name=groupName]').val(),
                description: formGroup.find('[name=description]').val(),
                parent_id: formGroup.find('[name=parent_id]').val(),
                group_id: $('#group').val(),
                delete_children: (formGroup.find('[name=delete_children]').prop('checked') ? 1 : 0)
            };
            if (calls.includes(callType)) {
                modal.modal('hide');
                ajaxGroup(data, callType);
            }
        });
        ac_userWithoutGroup($('#usersearch'), $('.search-user'));
        $(".view-container").css('height', $(window).height() * 0.33);
        $(".outer-users, .inner-users, #addUserFromGroup").on('click', function (event) {
            if ($(event.target).is(':button') && group_sel.val() !== 0){
                let parent = $(event.target).parent();
                let data = {
                    user_token: parent.find('[name=user_token]').val(),
                    firstname: parent.find('[name=firstname]').val(),
                    lastname: parent.find('[name=lastname]').val(),
                    group_id: group_sel.val()
                };
                let callType =
                    $(event.target).find('span').hasClass('glyphicon-plus')
                    || $(this).val() === 'add' ? "post" : "delete";

                ajaxUsers(data, callType);
            }
        });
    });
</script>
<?php
require_once("../../inc/footer_bo.php");
