<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_id('group_id', 'Le groupe est invalide', 'Le groupe est introuvable');
    if (!$validator){
        $json = parseJson::error('Impossible de récupérer les droits du groupe', $validator->getErrors());
        $json->printJson();
    }
    try {
        $output = App::getManageGroup()->getGroupRights($_GET['group_id']);
        $json = parseJson::success(null, $output);
    } catch (Exception $e) {
        $json = parseJson::error('Impossible de récupérer les droits du groupe', $e->getMessage());
    } finally {
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('group_id', 'Le groupe est invalide', 'Le groupe est introuvable');
    if (!$validator){
        $json = parseJson::error('Impossible de mettre à jour les droits du groupe', $validator->getErrors());
        $json->printJson();
    }
    try {
        $_PUT['edited_by'] = App::getAuth()->user()->id;
        $output = App::getManageGroup()->updateGroupRights($_PUT);
        $json = parseJson::success('Les droit du groupe ont été mis à jour', $output);
    } catch(Exception $e) {
        $json = parseJson::error('Impossible de mettre à jour les droits du groupe', $e->getMessage());
    } finally {
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est manquant');
    if(!$validator->is_valid()) {
        $json = parseJson::error('Impossible de supprimer les droits du groupe', $validator->getErrors());
        $json->printJson();
    }
    $output = App::getManageGroup()->removeGroupRights($_DELETE);
    if ($output)
        $json = parseJson::success('Les droits du groupe ont été entièrement supprimé', $output);
    else
        $json = parseJson::error('Impossible de supprimer les droits du groupe');
    $json->printJson();
}