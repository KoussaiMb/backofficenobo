<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    if (isset($_GET['group_id']))
        $validator->is_id('group_id', 'Le groupe est invalide');
    $validator->in_array('option', ['select', 'group_id'] , 'L\'option est invalide');
    if(!$validator->is_valid()) {
        $json = parseJson::error(null, $validator->getErrors());
        $json->printJson();
    }
    else if (!isset($_GET['option'])) {
        $output = App::getManageGroup()->getSortedGroup();
    } else if ($_GET['option'] == 'select') {
        $output = App::getManageGroup()->groupsAsOption();
    } else if ($_GET['option'] == 'group_id' && isset($_GET['group_id'])) {
        $output = App::getManageGroup()->getGroup($_GET['group_id']);
    }
    if (!empty($output))
        $json = parseJson::success(null, $output);
    else
        $json = parseJson::error('Impossible de récupérer les groupes');
    $json->printJson();
}else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);

    $validator->no_symbol('name', 'Le nom est invalide', 'Le nom du groupe est manquant');
    $validator->no_symbol('description', 'La description est invalide', 'La description est manquante');
    $validator->is_id('parent_id', 'Le parent est introuvable');
    if(!$validator->is_valid()) {
        $json = parseJson::error('Le groupe n\'a pas été ajouté', $validator->getErrors());
        $json->printJson();
    }
    $ret = App::getManageGroup()->addGroup($_POST);
    if ($ret)
        $json = parseJson::success('Le groupe a bien été ajouté', ['group_id' => $ret]);
    else
        $json = parseJson::error('Le groupe n\'a pas été ajouté');
    $json->printJson();
}else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->no_symbol('name', 'Le nom est invalide', 'Le nom du groupe est manquant');
    $validator->no_symbol('description', 'La description est invalide', 'La description est manquante');
    $validator->is_id('group_id', 'Le groupe est invalide', 'Le groupe est manquant');
    $validator->is_id('parent_id', 'Le parent est introuvable');
    if(!$validator->is_valid()) {
        $json = parseJson::error('Le groupe n\'a pas été modifié', $validator->getErrors());
        $json->printJson();
    }
    $ret = App::getManageGroup()->updateGroup($_PUT);
    if ($ret === false)
        $json = parseJson::error('Le groupe n\'a pas été modifié', $ret);
    else if ($ret === null)
        $json = parseJson::error('On ne peut pas encore changer de groupe parent', $ret);
    else
        $json = parseJson::success('Le groupe a bien été modifié', $ret);
    $json->printJson();
}else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('group_id', 'Le groupe est invalide', 'Le groupe est manquant');
    $validator->is_num('delete_children', 'l\'option est invalide', 'L`\'option est manquante');
    if ($validator->is_valid())
        $validator->is_range('delete_children', 'l\'option est invalide', [0, 1]);
    if(!$validator->is_valid()) {
        $json = parseJson::error('Le groupe n\'a pas été supprimé', $validator->getErrors());
        $json->printJson();
    }
    $ret = App::getManageGroup()->deleteGroup($_DELETE);
    if ($ret && $_DELETE['delete_children'] == 1)
        $json = parseJson::success('Le groupe et sa branche ont été supprimés');
    else if ($ret && $_DELETE['delete_children'] == 0)
        $json = parseJson::success('Le groupe a bien été supprimé');
    else
        $json = parseJson::error('Le groupe n\'a pas été supprimé');
    $json->printJson();
}