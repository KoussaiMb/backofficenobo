<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = new Validator($_GET);

    $validator->is_alpha('type', 'Le type est invalide', 'Le type est introuvable');
    $validator->is_id('menu_id', 'Le menu est invalide', 'Le menu est introuvable');
    if ($_GET['type'] == 'group')
        $validator->is_id('target_id', 'Le groupe est invalide', 'Le groupe est introuvable');
    else if ($_GET['type'] == "user")
        $validator->isToken('target_id', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    else
        $validator->throwException('type', 'Impossible de récupérer des droits pour autre chose que des groupes ou des utilisateurs');
    if (!$validator->is_valid())
        parseJson::error('Impossible de récupérer les options du menu', $validator->getErrors())->printJson();

    if ($_GET['type'] == "group")
        $options = App::getManageGroup()->getRightsByGroupId($_GET['target_id'], $_GET['menu_id']);
    else
        $options = App::getManageGroup()->getRightsByUserToken($_GET['target_id'], $_GET['menu_id']);
    if ($options === false)
        parseJson::error()->printJson();
    parseJson::success("Récupération des options réussie", $options)->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);

    $validator->is_id('menu_id', 'Le menu est invalide', 'Le menu est introuvable');
    $validator->is_specialchars_alphanum('option_name', 'Nom d\'option est invalide', 'Nom d\'option est introuvable');
    $validator->is_alpha('option_value', 'L\'option est invalide', 'L\'option est introuvable');
    if (!$validator){
        $json = parseJson::error('Impossible de récupérer les valeurs d\'options', $validator->getErrors());
        $json->printJson();
    }

    $option_id = App::getMenuManager()->addOption($_POST['option_name'], $_POST['option_value'], $_POST['menu_id']);
    $ManageGroup = App::getManageGroup();
    $groups = $ManageGroup->getGroups();

    foreach ($groups as $k) {
        $ManageGroup->addRightByGroupId($k['id'], $option_id);
    }

    $json = parseJson::success("Récupérations des options réussi", $option_id);
    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_alpha('type', 'Le type est invalide', 'Le type est introuvable');
    $validator->is_id('option_id', 'L\'option est invalide', 'L\'option est introuvable');
    $validator->is_num('activated', 'L\'option est invalide', 'L\'option est introuvable');
    if ($_PUT['type'] == 'group')
        $validator->is_id('target_id', 'Le groupe est invalide', 'Le groupe est introuvable');
    else if ($_PUT['type'] == "user")
        $validator->isToken('target_id', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    else
        $validator->throwException('type', 'Impossible de modifier les droits pour autre chose que des groupe ou des utilisateurs');
    if (!$validator->is_valid())
        parseJson::error('Impossible de récupérer les options du menu', $validator->getErrors())->printJson();

    if ($_PUT['type'] == "group")
        $ret = App::getManageGroup()->updateRightByGroupId($_PUT['target_id'],$_PUT['option_id'],$_PUT['activated'] == 1 ? true : false);
    else
        $ret = App::getManageGroup()->updateRightByUserToken($_PUT['target_id'],$_PUT['option_id'],$_PUT['activated'] == 1 ? true : false);

    if ($ret)
        $json = parseJson::success("Changement réussi");
    else
        $json = parseJson::error("Une erreur c'est produite!");

    $json->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_alpha('type', 'Le type est invalide', 'Le type est introuvable');
    $validator->is_id('option_id', 'L\'option est invalide', 'L\'option est introuvable');
    if ($_DELETE['type'] == 'group')
        $validator->is_id('target_id', 'Le groupe est invalide', 'Le groupe est introuvable');
    else if ($_DELETE['type'] == "user")
        $validator->isToken('target_id', 'L\'utilisateur est invalide', 'L\'utilisateur est introuvable');
    else
        $validator->throwException('type', 'Impossible de supprimer les droits pour autre chose que des groupes ou des utilisateurs');
    if (!$validator->is_valid())
        parseJson::error('Impossible de récupérer les options du menu', $validator->getErrors())->printJson();

    if ($_DELETE['type'] == "group")
        $ret = App::getManageGroup()->deleteRightByGroupId($_DELETE['target_id'],$_DELETE['option_id']);
    else
        $ret = App::getManageGroup()->deleteRightByUserToken($_DELETE['target_id'],$_DELETE['option_id']);
    if ($ret)
        $json = parseJson::success("Suppression réussi");
    else
        $json = parseJson::error("Une erreur c'est produite !");
    $json->printJson();
}
