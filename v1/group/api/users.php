<?php
require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['group_id']) && preg_match("/^[0-9]+$/", $_GET['group_id']))
        $output = App::getManageGroup()->getUsersInGroup($_GET['group_id']);
    else if (!isset($_GET['option']))
        $output = [];
    else if ($_GET['option'] == 'ac_with_group')
        $output = App::getManageGroup()->acUsersWithGroup();
    else if ($_GET['option'] == 'without_group')
        $output = App::getManageGroup()->usersWithoutGroups();
    if (isset($output))
        $json = parseJson::success(null, $output);
    else
        $json = parseJson::error('Impossible de récupérer les groupes');
    $json->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est manquant');
    $validator->is_id('group_id', 'Le groupe est invalide', 'Le groupe est manquant');
    if(!$validator->is_valid()) {
        $json = parseJson::error('Impossible d\'ajouter l\'utilisateur au groupe', $validator->getErrors());
        $json->printJson();
    }
    if (isset($_POST['user_token']) && isset($_POST['group_id']))
        $output = App::getManageGroup()->addUserInGroup($_POST);
    if (!empty($output))
        $json = parseJson::success('L\'utilisateur a été ajouté au groupe', $output);
    else
        $json = parseJson::error('Impossible d\'ajouter l\'utilisateur au groupe');
    $json->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est manquant');
    if(!$validator->is_valid()) {
        $json = parseJson::error('Impossible de supprimer l\'utilisateur du groupe', $validator->getErrors());
        $json->printJson();
    }
    $output = App::getManageGroup()->removeUserFromGroup($_DELETE);
    if ($output)
        $json = parseJson::success('L\'utilisateur a été supprimé du groupe', $output);
    else
        $json = parseJson::error('Impossible de supprimer l\'utilisateur du groupe');
    $json->printJson();
}
