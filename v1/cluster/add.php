<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('cluster');
$governessAvailable = App::getCluster()->getAllGovernessAvailable();
$clusterLocation = App::getListManagement()->getList('clusterLocation');

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        <a href="view">Les Business units</a> / créer une business unit
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <form method="post" action="form">
                <div class="form-group col-sm-12">
                    <label for="name">Nom de la Bu</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="Les marsupilamis" value="<?= isset($data) ? $data['name'] : null; ?>" id="name" required>
                </div>
                <div class="form-group col-sm-12">
                    <label for="user_id">Choisissez une gouvernante</label>
                    <select name="user_id" class="form-control input-sm" id="user_id" required>
                        <option value=""></option>
                        <?php foreach ($governessAvailable as $k): ?>
                            <option value="<?= $k->id; ?>"><?= $k->firstname . ' ' . $k->lastname; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-sm-12">
                    <label for="l_clusterLocation_id">Ville dans laquelle sera la Bu</label>
                    <select name="l_clusterLocation_id" class="form-control input-sm" id="l_clusterLocation_id" required>
                        <option value=""></option>
                        <?php foreach ($clusterLocation as $k): ?>
                            <option value="<?= $k->id; ?>"><?= $k->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-sm-12">
                    <label for="description">Ajoutez une description si vous le souhaitez</label>
                    <textarea class="form-control input-sm" name="description" id="description" maxlength="100" rows="2"><?= isset($data) ? $data['description'] : null; ?></textarea>
                </div>
                <div class="col-sm-12 text-center">
                    <a href="view"><button type="button" class="btn btn-default btn-sm">Revenir en arrière</button></a>
                    <button type="submit" class="btn btn-success btn-sm" name="action" value="add">Créer la business unit</button>
                </div>
            </form>
        </div>
    </div>
</div>
