<?php
require('../../inc/bootstrap.php');

$clusterClass = App::getCluster();

if (!isset($_GET['id']) || !preg_match("/^[0-9]+/", $_GET['id']))
    App::setFlashAndRedirect("warning", "Veuillez indiquer le cluster à éditer", "view");

$cluster = $clusterClass->getClusterById($_GET['id']);
$governessAvailable = App::getCluster()->getAllGovernessAvailable();
$clusterLocation = App::getListManagement()->getList('clusterLocation');
$providerAutocomplete = $clusterClass->providerNotInCluster();
$provider_in_cluster = $clusterClass->getProviderCluster($_GET['id']);

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        <a href="view">Les business units</a> / éditer une Bu
        <form class="pull-right" method="post" action="form">
            <input class="hidden" name="cluster_id" value="<?= $_GET['id']; ?>">
            <button type="submit" class="btn-inherit btn-inline-delete" name="action" value="delete" onclick="return confirm('Supprimer la Bu ?')">Supprimer la Bu</button>
        </form>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <form method="post" action="form">
                <input class="hidden" name="cluster_id" value="<?= $_GET['id']; ?>">
                <div class="form-group col-sm-12">
                    <label for="name">Nom de la Bu</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="Les marsupilamis" value="<?= $cluster->name; ?>" id="name" required>
                </div>
                <div class="form-group col-sm-12">
                    <label for="user_id">Choisissez une gouvernante</label>
                    <select name="user_id" class="form-control input-sm" id="user_id" required>
                        <option value="<?= $cluster->user_id; ?>" selected><?= $cluster->firstname . ' ' . $cluster->lastname ?></option>
                        <?php foreach ($governessAvailable as $k): ?>
                            <option value="<?= $k->id; ?>"><?= $k->firstname . ' ' . $k->lastname; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-sm-12">
                    <label for="l_clusterLocation_id">Ville dans laquelle sera la Bu</label>
                    <select name="l_clusterLocation_id" class="form-control input-sm" id="l_clusterLocation_id" required>
                        <?php foreach ($clusterLocation as $k): ?>
                            <option value="<?= $k->id; ?>" <?= $cluster->l_clusterLocation_id == $k->id ? "selected" : "" ?>><?= $k->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-sm-12">
                    <label for="description">Ajoutez une description si vous le souhaitez</label>
                    <textarea class="form-control input-sm" name="description" id="description" maxlength="100" rows="2"><?= $cluster->description; ?></textarea>
                </div>
                <div class="form-group col-sm-12">
                    <label for="providersearch">Ajoutez des prestataires à la business unit</label>
                    <input type="hidden" name="id" id="provider_token">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="prenom, nom" id="providersearch">
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="button" id="addProvider"><span class="glyphicon glyphicon-plus"></span></button>
                        </span>
                    </div>
                    <div id="provider-list">
                        <?php foreach ($provider_in_cluster as $k => $v): ?>
                            <div class="provider-div col-sm-6">
                                <input type="hidden" name="provider_<?= $k; ?>" value="<?= $v->user_token; ?>">
                                <p>
                                    <?= $v->firstname . ' ' . $v->lastname; ?>
                                    <span class='glyphicon glyphicon-remove pull-right'></span>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <a href="view"><button type="button" class="btn btn-default btn-sm">Revenir en arrière</button></a>
                    <button type="submit" class="btn btn-success btn-sm" name="action" value="edit">Editer la Business unit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .provider-list {
        padding-top: 5px
    }
    .provider-div > p {
        border: 1px solid black;
        border-radius: 5px;
        padding: 3px;
        margin-top: 3px;
        margin-bottom: 0;
    }
    .provider-div > p > span:hover {
        cursor: pointer;
    }
    .provider-div > p > span {
        padding-top: 2px;
        color: red;
    }
</style>
<script src="../../js/cluster.js"></script>
<script>
    jQuery(document).ready(function(){
        var providersearch = $('#providersearch');
        var providerList = $('#provider-list');
        var autoCompleteProvider = <?php if (!empty($providerAutocomplete)){echo $providerAutocomplete;} else {echo "[]";}?>;
        var user = null;

        providersearch.devbridgeAutocomplete({
            lookup: autoCompleteProvider,
            maxHeight: 200,
            onSelect: function (userData) {
               user = userData;
            },
            onSearchStart: function () {
                user = null;
            },
            onSearchError: function () {
                user = null;
            }
        });

        providersearch.keypress(function (e) {
            var key = e.which;

            if(key == 13)
            {
                addProviderInList();
                return false;
            }
        });

        $('#addProvider').on('click', function () {
            addProviderInList();
        });

        function addProviderInList() {
            if (user !== null) {
                var nth = $('.provider-div').length;

                insertProviderDiv(providerList, user, nth);
                autoCompleteProvider = deleteProviderFromList(autoCompleteProvider, user);
                providersearch.devbridgeAutocomplete().setOptions({lookup: autoCompleteProvider});
                providersearch.val("");
                user = null;
            }
        }

        providerList.on("click", function (e) {
            if ($(e.target).is('.glyphicon-remove')){
                var divSelected = $(e.target).parents(".provider-div");
                var userSelected = {
                    value : divSelected.find("p").text(),
                    user_token : divSelected.find(":input").val(),
                    name : divSelected.find("p").text()
                };

                autoCompleteProvider.push(userSelected);
                divSelected.remove();
                updateProviderListNth(providerList);
            }
        })
    });
</script>