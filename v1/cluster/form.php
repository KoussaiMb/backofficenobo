<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');

$db = App::getDB();

if ($_POST['action'] == 'edit') {
    $validator = new Validator($_POST);
    $provider_list = [];
    $i = 0;

    $validator->no_symbol('name', 'Le nom de la business unit est invalide', 'Veuillez indiquer un nom de business Unit');
    $validator->is_id('user_id', 'Le/la gouvernant(e) est invalide', 'Veuillez indiquer un/une gourvenat(e)');
    $validator->is_id('cluster_id', 'Le cluster est invalide', 'Veuillez indiquer le cluster à éditer');
    $validator->is_id('l_clusterLocation_id', 'Le lieu de la business unit est invalide', 'Veuillez indiquer une ville pour la business unit');
    $validator->no_symbol('description', 'La description est invalide');
    if ($validator->is_valid())
        $validator->in_db_list('l_clusterLocation_id', App::getDB(), 'Le lieu de la business unit est introuvable');
    while (isset($_POST['provider_' . $i])) {
        $validator->isToken('provider_' . $i, 'Un prestatair est invalide', "Un prestataire est introuvable");
        $provider_list[] = $_POST['provider_' . $i++];
    }
    $_POST['provider_nb'] = $i;
    $_POST['provider_list'] = "('" . implode($provider_list, "','") . "')";

    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), 'view');
    $ret = App::getCluster()->editCluster($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le cluster n\'a pas pu être édité', 'edit?id=' . $_POST['cluster_id']);
    $ret = App::getCluster()->editProviderInCluster($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('warning', 'Le cluster a été modifié mais les prestataires n\'ont pu être modifiés', 'edit?id=' . $_POST['cluster_id']);
    App::setFlashAndRedirect('success', "Le cluster a bien été modifié", "view");
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);

    $validator->is_id('cluster_id', 'Le cluster est invalide', 'Veuillez indiquer le cluster à supprimer');
    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), 'view');
    $ret = App::getCluster()->deleteCluster($_POST['cluster_id']);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le cluster n\'a pas pu être supprimé', 'edit?id=' . $_POST['cluster_id']);
    App::setFlashAndRedirect('success', "Le cluster a bien été supprimé", "view");
}
else if($_POST['action'] == 'add') {
    $validator = new Validator($_POST);

    $validator->no_symbol('name', 'Le nom de la business unit est invalide', 'Veuillez indiquer un nom de business Unit');
    $validator->is_id('user_id', 'Le/la gouvernant(e) est invalide', 'Veuillez indiquer un/une gourvenat(e)');
    $validator->is_id('user_id', 'Le/la gouvernant(e) est invalide', 'Veuillez indiquer un/une gourvenat(e)');
    $validator->is_id('l_clusterLocation_id', 'Le lieu de la business unit est invalide', 'Veuillez indiquer une ville pour la business unit');
    $validator->no_symbol('description', 'La description est invalide');
    if ($validator->is_valid())
        $validator->in_db_list('l_clusterLocation_id', App::getDB(), 'Le lieu de la business unit est introuvable');
    if (!$validator->is_valid()) {
        Session::getInstance()->write('cluster', $validator->getData());
        App::array_to_flash($validator->getErrors(), 'add');
    }
    $ret = App::getCluster()->addCluster($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le cluster n\'a pas pu être ajouté', 'add');
    Session::getInstance()->delete('cluster');
    App::setFlashAndRedirect('success', "Le cluster a bien été créé", "view");
}
App::redirect('view');