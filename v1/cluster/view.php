<?php
require('../../inc/bootstrap.php');

$clusterClass = App::getCluster();

$clusterLocation = $clusterClass->getCities();
$clusterList = $clusterClass->getActiveClusters();
$max_provider_in_bu = App::getConf()->getConfByName("max_provider_in_bu")->value;

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Les Business units
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <div class="col-sm-4">
            <label for="clusterLocation">Choisir la ville</label>
            <select name="clusterCity" class="form-control input-sm" id="clusterLocation">
                <option value=""></option>
                <?php foreach ($clusterLocation as $k => $v): ?>
                    <option value="<?= $v->id; ?>"><?= $v->city; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-sm-8 text-center" style="padding-top: 22px">
            <a href="add"><button type="button" class="btn btn-success pull-right btn-sm" name="action" value="add">Créer un nouveau cluster</button></a>
        </div>
        <div class="col-xs-12 view-container-nobo">
            <?php foreach ($clusterList as $k => $v): ?>
                <div class="col-xs-4 cluster-view">
                    <div class="col-xs-12">
                        <input type="hidden" name="id" value="<?= $v->id; ?>">
                        <input type="hidden" name="l_clusterLocation_id" value="<?= $v->l_clusterLocation_id; ?>">
                        <div class="col-xs-12">
                            <p style="color: #204d74;"><?= $v->name ?><span class="pull-right"><?= "(" . $v->provider_nb . "/" . $max_provider_in_bu . ")"; ?></span></p>
                        </div>
                        <div class="col-xs-12">
                            <p class="noMargeBottom"><span class="glyphicon glyphicon-user"></span><?= " " . $v->firstname . " " . $v->lastname; ?></p>
                        </div>
                        <div class="col-xs-6">
                            <p><span class="glyphicon glyphicon-map-marker"></span><?= " " . $v->city ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<style>
    .cluster-view {
        padding: 20px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .cluster-view > div:hover {
        border: 1px solid grey;

    }
    .cluster-view > div {
        background-color: #c2c2d6;
        border-radius: 3px;
        padding-top: 10px;
        height: 80px;
        cursor: pointer;
    }
    .cluster-view > div div:first-child {
        color: ;
    }
    .cluster-view > div div:last-child {
        color: #e60073;
    }
</style>
<script>
jQuery(document).ready(function() {
    var allCluster = $('.cluster-view > div');
    var allDiv = $('.cluster-view');

    $(".view-container-nobo").css('max-height', $(window).height() * 0.7);
    allCluster.on('click', function () {
        window.location = 'edit?id=' + $(this).find("input[name='id']").val();
    });
    $('#clusterLocation').on('change', function () {
        var cluster_location = $(this).val();
        console.log(cluster_location);

        if (cluster_location == 0)
            allDiv.show();
        else {
            allDiv.hide();
            $("input[name='l_clusterLocation_id'][value='"+ cluster_location +"']").parentsUntil("div[class='cluster-view']").show();
        }
    })
});
</script>
