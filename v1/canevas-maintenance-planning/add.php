<?php
require('../../inc/bootstrap.php');
require('../../inc/header_bo.php');

if ($rights['add'] == 0)
    App::redirect_js('.');


$db = App::getDB();
//$m_planning = ();
$tasks = $db->query("SELECT * FROM `maintenance` WHERE `deleted` = 0 AND `address_id` = ?", [$_GET['id']]);
/*
if (!isset($_GET['id']))
    die();
$tasks = $db->query("SELECT * FROM `maintenance` WHERE `deleted` = 0 AND `address_id` = ?", [$_GET['id']]);
*/
?>

<div class="panel panel-success">
    <div class="panel-heading">
        Création du nouveau planning d'entretien
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
        </div>
        <?php //foreach($m_planning as $k => $v): ?>
        <div class="col-sm-12 text-center">
            <table class="table table-bordered table-maintenance">
                <thead>
                    <tr>
                        <th class="col-xs-3">
                            PL 1
                        </th>
                        <th class="col-xs-3">
                            PL 2
                        </th>
                        <th class="col-xs-3">
                            PL 3
                        </th>
                        <th class="col-xs-3">
                            PL 4
                        </th>
                    </tr>
                </thead>
                <tbody class="table-section-rec">
                    <tr>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                    </tr>
                </tbody>
                <tbody class="table-section-per">
                    <tr>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                    </tr>
                </tbody>
                <tbody class="table-section-ponc">
                    <tr>
                        <th>
                            <div class="task-box">
                                <img class="img-responsive" src="/img/nobo/logo/maintenance/repassage.svg">
                                <span class="task-separator">|</span>
                                <span class="task-name">repassage</span>
                                <span class="task-separator">|</span>
                                <span class="task-time">2h</span>
                            </div>
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php// endforeach; ?>
    </div>
</div>
<style>
    .table-maintenance > thead th{
        text-align: center;
    }
    .table-section-per .task-box {
        background-color: #ffeecc;
    }
    .table-section-ponc .task-box {
        background-color: #ffb3b3;
    }
    .table-section-rec .task-box {
        background-color: #c2f0f0;
    }
    .task-box {
        border-radius: 4px;
        padding-left: 10px;
        padding-right: 10px;

        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .task-box img {
        height: 36px;
    }
    .task-separator {
        color: rgba(0, 0, 0, 0.2);
    }
    .table-maintenance tbody > tr > th > div:first-child {
        margin-top: inherit;
    }
    .table-maintenance tbody > tr > th > div {
        margin-top: 0.6em;
    }
</style>
<script>
    jQuery(document).ready(function() {
        $('.pickatime').timepicker({
            'timeFormat': 'H:i',
            'step' : 30,
            'maxTime' : '08:00'
        });
    });
</script>
<?php
require('../../inc/footer_bo.php');