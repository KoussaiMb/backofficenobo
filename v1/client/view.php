<?php
require('../../inc/bootstrap.php');

Session::getInstance()->upLog('client_checked');
$db = App::getDB();
$cus = App::getCustomer();
$autocompleteClient = $cus->getAutocompleteName();
$waiting_list = App::getUser()->getWaitingList();
$customers = $cus->customerNotInWaitingList();
require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Rechercher un client:
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="col-sm-10">
                <form class="form-inline">
                    <label class="col-sm-2 col-sm-offset-3" for="clientsearch">Rechercher un client</label>
                    <input type="hidden" name="id" id="clientsearch_hidden">
                    <input type="text" class="form-control" id="clientsearch" placeholder="prenom, nom">
                    <button type="button" class="btn btn-default" onclick="window.location = 'edit?user_token=' + $('#clientsearch_hidden').val()">Fiche client</button>
                </form>
            </div>
            <div class="col-sm-2">
                <a href="add"><button type="button" class="btn btn-success" name="action" value="add"><span class="glyphicon glyphicon-plus"></span> Ajouter un client</button></a>
            </div>
        </div>
        <!-- Clients non validés -->
        <div class="col-sm-6 SpaceTop">
            <h4>Liste d'attente</h4>
            <div class="table-responsive col-sm-10 col-sm-offset-1">
                <table id="table_client" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>email</th>
                        <th>téléphone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($waiting_list as $key => $v):?>
                        <tr class="clickable-row" data-href="<?= 'edit?user_token=' . $v->user_token;?>">
                            <td><?= $v->firstname . ' ' . $v->lastname ?></td>
                            <td><?= $v->email ?></td>
                            <td><?= $v->phone ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Clients validés-->
        <div class="col-sm-6 SpaceTop">
            <h4>Clients</h4>
            <div class="table-responsive col-sm-10 col-sm-offset-1">
                <table id="table_client_validated" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>email</th>
                        <th>téléphone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($customers as $key => $v): ?>
                        <tr class="clickable-row" data-href="<?= 'edit?user_token=' . $v->user_token;?>">
                            <td><?= $v->firstname . ' ' . $v->lastname; ?></td>
                            <td><?= $v->email; ?></td>
                            <td><?= $v->phone; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function(){
    var clientsearch = $('#clientsearch');

    clientsearch.devbridgeAutocomplete({
        lookup: <?php if (!empty($autocompleteClient)){echo $autocompleteClient;} else {echo "[]";}?>,
        maxHeight: 200,
        onSelect: function (suggestion) {
            $('#clientsearch_hidden').val(suggestion.data);
        }
    });
    clientsearch.keypress(function (e) {
        var key = e.which;

        if(key == 13)
        {
            window.location = 'edit?user_token=' + $('#clientsearch_hidden').val();
            return false;
        }
    });
});
</script>
<?php
require('../../inc/footer_bo.php');