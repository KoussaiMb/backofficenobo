<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');
$db = App::getDB();

if ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    if (empty($_POST['phone']) && empty($_POST['email']))
        $validator->throwException('email ou tel', 'Un email ou un numéro de téléphone est obligatoire');
    if (!empty($_POST['email']))
        $validator->is_email('email', 'Email invalide');
    if (!empty($_POST['email']) && $validator->is_valid() == true)
        $validator->is_uniq($db, 'user', 'email', 'email', 'Email déjà utilisé');
    $validator->no_symbol('lastname', 'Le nom n\'est pas valide');
    $validator->no_symbol('firstname', 'Le prénom n\'est pas valide');
    if (!empty($_POST['phone']))
        $validator->is_phone('phone', 'Mauvais format du téléphone');
    $validator->is_id('gender_id', 'Problème au niveau du genre');
    $validator->is_id('waiting_list', 'Voulez-vous ajouter le client à la liste d\'attente ?');
    Session::getInstance()->write('add-client', $validator->getData());
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect('add');
    }
    $user_token = App::getUser()->addUser($_POST, 'customer');
    if (!$user_token)
        App::setFlashAndRedirect('danger', "Une erreur est survenue lors de l'ajout du client", "add");

    //Check if add to waiting list
    if ($_POST['waiting_list'] == 1) {
        //creating new address for this client
        $userClass = App::getUser();
        $addressClass = App::getAddress();
        $user = $userClass->getUserByToken($user_token);
        $address_token = $addressClass->addAddress(["home" => 1, "user_id" => $user->id]);

        if ($address_token) {
            $address = $addressClass->getAddressByToken($address_token);
            $user_info = $userClass->addWaitingList($address->id);
        }
    }

    Session::getInstance()->delete('add-client');
    if (isset($user_info) && $user_info === false)
        App::setFlashAndRedirect('warning', "Votre client a été créé mais n'a pas pu être ajouté à la liste d'attente", "edit?user_token=" . $user_token);
    App::setFlashAndRedirect('success', "Veuillez éditer le client que vous avez créé", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'stop_nobo'){
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);
    $validator->isToken('user_token', 'Le client n\'est pas valide', 'Le client n\'est pas renseigné');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getError('user_token'), "edit?user_token=" . $user_token);
    if ($_POST['select_reason'] != 'autre') {
        $_POST['reason'] = NULL;
    }
    else {
        $_POST['select_reason'] = NULL;
    }
    $_POST['date'] = date('Y-m-d', strtotime($_POST['date']));
    $ret = $db->query_log("UPDATE agenda SET deleted = 1, deleted_at = NOW() WHERE start_rec >= ? AND user_id = (SELECT id FROM user WHERE user_token = ?)", [$_POST['date'], $user_token]);
    if ($ret === false)
        App::setFlashAndRedirect('danger', "Une erreur a eu lieu lors de la modification des missions veuillez contactez un administrateur", "edit?user_token=" . $user_token);
    $ret = $db->query_log("UPDATE agenda SET end_rec = ? WHERE start_rec < ? AND (end_rec >= ? OR end_rec IS NULL) AND user_id = (SELECT id FROM user WHERE user_token = ?)", [$_POST['date'], $_POST['date'], $_POST['date'], $user_token]);
    if ($ret === false)
        App::setFlashAndRedirect('danger', "Une erreur a eu lieu lors de la modification des missions veuillez contactez un administrateur", "edit?user_token=" . $user_token);
    App::getCustomer()->AddStopNobo($_POST);
    App::setFlashAndRedirect('success', "L'arrêt du client a bien été pris en compte", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'restart_nobo'){
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);
    $validator->isToken('user_token', 'Le client n\'est pas valide', 'Le client n\'est pas renseigné');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getError('user_token'), "edit?user_token=" . $user_token);
    $id = $_POST['stop_nobo_id'];
    App::getCustomer()->RemoveStopNoboById($id);
    App::setFlashAndRedirect('success', "La reprise du client a bien été prise en compte", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'delete')
{
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le client n\'est pas valide', 'Le client n\'est pas renseigné');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getError('user_token'), "edit?user_token=" . $user_token);
    if (App::getUser()->deleteUserByToken($user_token))
        App::setFlashAndRedirect('warning', "Le client a bien été supprimé", "view");
    App::setFlashAndRedirect('danger', "Le client n'a pas pu être supprimé :(", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'stop_nobo')
{
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le client n\'est pas valide', 'Le client n\'est pas renseigné');
    if (!$validator->is_valid())
        App::setFlashAndRedirect('danger', $validator->getError('user_token'), "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'delete_photo')
{
    $user_token = $_POST['user_token'];
    $id = App::getUser()->getUserByToken($user_token);
    $ret = App::getUser()->deletePhotoById($id);

    if (!empty($ret))
        Session::getInstance()->setFlash('success', "La photo de profil du client a été supprimée");
    else
        Session::getInstance()->setFlash('danger', "La photo de profil du client n'a pas été supprimée");
    App::redirect("edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'reload_password')
{
    $user_token = $_POST['user_token'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le client n\'est pas valide', 'Le client est introuvable');
    $validator->is_email('email', 'L\'email n\'est pas valide', 'L\'email est introuvable');
    $client = App::getUser()->getUserByToken($_POST['user_token']);
    if ($validator->is_valid() && $_POST['email'] != $client->email)
        $validator->throwException('email', 'L\'email de ne correspond pas au client');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect("edit?user_token=" . $user_token);
    }
    if (App::getAuth()->forget($_POST['email']))
        App::setFlashAndRedirect('success', "Un nouveau mot de passe a été envoyé au client", "edit?user_token=" . $user_token);
    App::setFlashAndRedirect('danger', "Le nouveau mot de passe n'a pas été envoyé", "edit?user_token=" . $user_token);
}
else if ($_POST['action'] == 'edit')
{
    $user_token = $_POST['user_token'];
    $email = $_POST['email'];
    $validator = new Validator($_POST);

    $validator->isToken('user_token', 'Le token est invalide', 'Le token est manquant');
    $validator->is_id('gender_id', 'Le genre n\'est pas valide', 'Le genre n\'est pas défini');
    $validator->is_date_bo('stop_nobo', 'La date n\'est pas valide');
    $validator->is_phone('phone', 'Mauvais format du téléphone');
    $validator->no_symbol('lastname', 'Le nom n\'est pas valide');
    $validator->no_symbol('firstname', 'Le prénom n\'est pas valide');
    $validator->is_date_bo('birthdate', 'Format date de naissance invalide');
    $validator->is_email('email', 'Email invalide');
    $validator->is_num('childNb', 'Nombre d\'enfant invalide');
    $validator->is_id('maritalStatus', 'Etat civil invalide');
    $validator->is_id('customer_package_id', 'Affiliation du client invalide');
    $validator->is_id('l_acquisition_id', 'Source d\'acquisition invalide', 'Source d\'acquisition manquante');
    $validator->is_id('l_payment_mean_id', 'Moyen de paiement invalide');
    $client = App::getUser()->getUserByToken($user_token);
    if ($client == null)
        $validator->throwException('system', 'impossible de vérifier l\'email');
    $validator_email = new Validator(['email' => $_POST['email']]);
    $validator_email->is_uniq($db, 'user', 'email', 'email', 'existe déjà');
    if ($validator->is_valid() && $client->email != $email && !$validator_email->is_valid())
        $validator->throwException('email', 'l\'adresse email entrée existe déjà');
    if (!$validator->is_valid()){
        App::array_to_flash($validator->getErrors());
        App::redirect("edit?user_token=" . $user_token);
    }
    //Création du dossier contenant les photos du customer
    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/img/user/customer/')) {
        $success_create = mkdir($_SERVER['DOCUMENT_ROOT'] . '/img/user/customer/', 0757, true);
        if(!$success_create){
            App::setFlashAndRedirect("danger", "Un problème est survenu lors de la création du dossier", "edit?user_token=" . $user_token);
        }
    }
    //Upload de l'image si elle existe
    if ($_FILES['photo_profile']['error'] !== 4) {
        $img_name = parseStr::cleanImgName($_FILES['photo_profile']['name']);
        $_POST['photo_profile'] = $validator->is_logo_uploaded($_FILES['photo_profile'], $img_name, '/img/user/customer/', 1000000, array('png', 'jpg'));
        if ($validator->is_valid() === false)
            App::array_to_flash($validator->getErrors(), "edit?user_token=" . $user_token);
        if(!empty($_POST['photo_profile']))
            unlink($_POST['photo_profile']);
    }
    $user_instance = App::getUser();
    $user = $user_instance->getUserByToken($_POST['user_token']);
    if ($user === false)
        App::setFlashAndRedirect('danger', "L'utilisateur est invalide", "edit?user_token=" . $user_token);
    if (!empty($_POST['stop_nobo']))
        App::getCustomer()->stop_nobo($_POST['user_token'], $_POST['stop_nobo']);
    if ($_POST['l_payment_mean_id'] != null && $user_instance->canHaveThisPaymentMean($_POST['l_payment_mean_id'], $user->id) === false)
        Session::getInstance()->setFlash('danger payment-mean-error', 'Le client ne peut avoir ce moyen de paiement');
    else if ($user_instance->updatePaymentMean($_POST['l_payment_mean_id'], $user->id) === false)
        Session::getInstance()->setFlash('danger payment-mean-error', 'Impossible de mettre à jour le moyen de paiement');
    if (!$user_instance->updateUserAndCustomer($_POST))
        App::setFlashAndRedirect('danger', "Le client n'a pas pu être modifié", "edit?user_token=" . $user_token);
    App::setFlashAndRedirect('success', "Le client a bien été modifié", "edit?user_token=" . $user_token);
} else if ($_POST['action'] == 'edit_address') {
    $validator = new Validator($_POST);

    $validator->isToken('address_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->isToken('user_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->is_num('roomNb', 'Le nombre de chambre doit être un chiffre');
    $validator->is_num('waterNb', 'Le nombre de toilette doit être un chiffre');
    $validator->is_num('surface', 'La surface doit être un nombre');
    $validator->is_num('have_ironing', 'La surface doit être un nombre');
    $validator->is_num('have_key', 'La surface doit être un nombre');
    $validator->is_num('childNb', 'Le nombre d\'enfant doit être un nombre');
    $validator->is_num('peopleHome', 'Le nombre de personne présent doit être un nombre');
    $validator->is_id('l_pet_id', 'Animaux invalides', 'Animaux manquants');
    $validator->is_id('cluster_id', 'Cluster invalide');
    $validator->is_id('home', 'Est-ce la résidence du client ? invalide', 'Est-ce la résidence du client ? manquant');
    $validator->is_alphanum('lockbox_code', 'Le code de la lockbox est invalide');
    $validator->no_symbol('lockbox_localization', 'L\'emplacement de la lockbox comporte des erreurs');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" .$_POST['user_token']. "&address_token=" .$_POST['address_token']. "&menu_id=menu2");
    }
    $address = App::getAddress();
    $user = App::getUser()->getUserByToken($_POST['user_token']);
    $customer_address = $address->getCustomerAddressByAddressToken($_POST['address_token']);
    $_POST['user_id'] = $user->id;
    if ($_POST['home'] == 1)
        $ret = $address->setAllAddressWithoutHome($user->id);
    if (isset($ret) && $ret === false)
        App::setFlashAndRedirect('danger', "Impossible de mettre le domicile en tant que domicile principal", '/v1/client/edit?user_token=' .$_POST['user_token']. "&menu_id=menu2");
    if ($address->updateAddressCluster($_POST['cluster_id'], $_POST['address_token']) === false)
        Session::getInstance()->setFlash('warning', 'Le cluster n\'a pas été modifié');
    if ($customer_address === false)
        Session::getInstance()->setFlash('warning', 'Impossible de créer des données pour la lockbox');
    else if ($customer_address === null && $address->newCustomerAddress($_POST) === false)
        Session::getInstance()->setFlash('warning', 'Impossible de créer des données pour la lockbox');
    else if ($address->updateLockbox($_POST) === false)
        Session::getInstance()->setFlash('warning', 'Les informations de la lockbox n\'ont pas été enregistrées');
    if ($address->updateAddressFromCustomer($_POST) === false)
        App::setFlashAndRedirect('danger', 'L\'adresse n\'a pas pu être modifiée',  "/v1/client/edit?user_token=" .$_POST['user_token']. "&address_token=" .$_POST['address_token']. "&menu_id=menu2");
    App::setFlashAndRedirect('info', "L'adresse a bien été modifiée", "/v1/client/edit?user_token=" .$_POST['user_token']. "&address_token=" .$_POST['address_token']. "&menu_id=menu2");
} else if ($_POST['action'] == 'delete_address') {
    $validator = new Validator($_POST);

    $validator->isToken('address_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    $validator->isToken('user_token', 'L\'adresse n\'est pas valide', 'L\'adresse est introuvable');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" .$_POST['user_token']. "&address_token=" .$_POST['address_token']. "&menu_id=menu2");
    }
    if (App::getAddress()->deleteAddressByToken($_POST['address_token']))
        App::setFlashAndRedirect('warning', "L'adresse a été supprimée", "/v1/client/edit?id=" .$_POST['user_token']. "&menu_id=menu2");
    App::setFlashAndRedirect('danger', "L'adresse n'a pas pu être supprimée", "/v1/client/edit?id=" .$_POST['user_token']. "&address_token=" .$_POST['address_token']. "&menu_id=menu2");
} else if ($_POST['action'] == 'add_address') {
    $validator = new Validator($_POST);

    $validator->is_id('home', 'Est-ce son domicile ?', 'Est-ce son domicile ?');
    $validator->isToken('user_token', 'Le token est invalide', 'Le token est manquant');
    $validator->is_num('radio_add_wlist', 'Impossible de récupérer l\'ajout à la waiting list');
    if ($validator->is_valid() == false) {
        App::array_to_flash($validator->getErrors());
        App::redirect("/v1/client/edit?user_token=" .$_POST['user_token']. "&menu_id=menu2");
    }
    $redirection_wo_addres_token = "/v1/client/edit?user_token=" .$_POST['user_token']. "&menu_id=menu2";
    $address = App::getAddress();
    $user = App::getUser()->getUserByToken($_POST['user_token']);
    $_POST['user_id'] = $user->id;
    if ($_POST['home'] == 1 && $address->setAllAddressWithoutHome($user->id) == false)
        App::setFlashAndRedirect('danger', "Impossible de mettre le domicile en tant ", $redirection_wo_addres_token);
    $address_token = $address->addAddress($_POST);
    if (!$address_token)
        App::setFlashAndRedirect('danger', 'L\'adresse n\'a pas été créée', $redirection_wo_addres_token);

    if ($_POST['radio_add_wlist'] == 1){
        $address_id = $address->getAddressByToken($address_token);
        if (!$address_id->id)
            App::setFlashAndRedirect('danger', "Impossible d'ajouter l'utilisateur à la liste d'attente", $redirection_wo_addres_token);
        $user_info = App::getUser()->addWaitingList($address_id->id);
    }
    Session::getInstance()->delete('address-data');
    if (isset($user_info) && $user_info == false)
        App::setFlashAndRedirect('warning', "L'adresse a bien été créée mais n'a pas été rajoutée à la liste d'attente", $redirection_wo_addres_token . "&address_token=" . $address_token);
    App::setFlashAndRedirect('success', "L'adresse a bien été créée", $redirection_wo_addres_token . "&address_token=" . $address_token);
}
App::redirect("view");
