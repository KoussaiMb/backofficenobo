<?php
require('../../inc/bootstrap.php');

setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');

if (!empty($_GET['user_token']) && preg_match('/^[a-fA-F0-9]{20}$/', $_GET['user_token']))
    $user = App::getUser()->getClientByToken($_GET['user_token']);
if (empty($user))
    App::setFlashAndRedirect('danger', 'Client inconnu au bataillon', 'view');

$customer_instance = App::getCustomer();
$stop_nobo = $customer_instance->getStopNoboByUserId($user->user_id);
$reason_list = App::getListManagement()->getList("stop_reason");
$address = App::getUser()->getAddressesByUserToken($_GET['user_token']);
$address_count = count($address);
$distinct_rooms = App::getCardex()->getDistinctListElements('roomsList');
$distinct_products = App::getCardex()->getDistinctListElements('productsList');
$allMetro =  App::getAutocompleteMetro();
$moment = App::getConf()->getAll();
$customer_info = $customer_instance->getCustomerByUserId($user->user_id);
$pet_list = App::getListManagement()->getList('pet');
$acquisition_list = App::getListManagement()->getList('source_acquisition');
$payment_mean_list = App::getListManagement()->getList('payment_mean');
$cluster = App::getCluster()->getActiveClusters();
$customer_packages = $customer_instance->getCustomerPackagesAndCurrentPackage($customer_info->customer_package_id);
$user_id = App::getUser()->getUserByToken($_GET['user_token']);
$users = App::getUser()->getUserById($user->user_id);
$cardex = App::getCardex()->getAll();
$pe = App::getMaintenancePlanningType()->getAll();
$materialsNobles = App::getListManagement()->getList('noble_materials');
if (isset($_POST['menu_id'])){
    $menu_id = substr($_POST['menu_id'], -1);
} else if (isset($_GET['menu_id'])) {
    $menu_id = substr($_GET['menu_id'], -1);
} else {
    $menu_id = '1';
}
require('../../inc/header_bo.php');
?>
    <link type='text/css' rel='stylesheet' href="/css/lib/stripe_button.css">
    <!--Modal_confirm_start-->
    <!--
    <div id="modal_stop_nobo" class="modal fade">
        <div class="modal-dialog modal-small">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                    <div style="font-size: 15px; font-weight: bold;">Attention toutes les missions après la date choisie seront supprimer êtes-vous sur ?</div>
                </div>
                <div class="modal-footer">
                    <form method="post" action="form" class="form_confirm">
                        <input type="text" name="user_token" value="<?= $user->user_token; ?>">
                        <div class="col-sm-12 stop_nobo_info">
                            <label class="col-xs-12 text-left nobo_stop_label" ><h5> Date : </h5><input name="date" type="text" class="form-control input-sm date_picker nobo_stop" data-provide="datepicker" required></label>
                        </div>
                        <div class="form-group col-sm-12" style="margin-top: 2em;" id="confirm">
                            <button name="action" value="stop_nobo" class="btn btn-primary col-xs-4 col-xs-offset-4 btn-sm">Valider</button>
                        </div>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    TODO: Check if needed -->
    <!-- fin modal confirm_end -->

    <div class="panel panel-success col-xs-10 noPadding">
        <div class="panel-heading">
            <div style="min-height: 30px">
                <div class="col-sm-3">
                    <img src="<?= !empty($user->photo_profile) ? $user->photo_profile :'/img/user/avatar_femme.jpg'; ?>" alt="Photo de profil" class="img-circle" style="border: 2px solid #39D2B4;" width="30px" height="30px">
                    <input type="hidden" name="user_token" id="user_token" value="<?= $_GET['user_token'];?>"/>
                    <div class="btn-group btn-drop-user">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $user->firstname . ' ' . $user->lastname; ?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <form method="post" action="form" class="text-right">
                                <input type="hidden" name="user_token" value="<?= $user->user_token; ?>">
                                <li><button type="button" name="action" value="reload_password" class="btn btn-sm" id="reload_password">Réinitialiser le mot de passe manuellement</button></li>
                                <li><button type="button" name="action" value="send_reset_token" class="btn btn-sm" id="send_reset_token">Réinitialiser le mot de passe par le client</button></li>
                                <li><button name="action" value="delete_photo" class="btn btn-sm">Supprimer la photo de profil</button></li>
                                <li role="separator" class="divider"></li>
                                <?php if (empty($stop_nobo)): ?>
                                    <li><button type="button" class="btn btn-sm text-danger" name="action" value="stop_nobo" onclick="return add_stop_nobo('<?= $user->user_token; ?>')">Le client arrête Nobo</button></li>
                                <?php else: ?>
                                    <input type="hidden" name="stop_nobo_id" value="<?= $stop_nobo->id; ?>">
                                    <li><button class="btn btn-sm text-info" name="action" value="restart_nobo">Le client reprend Nobo (arrêt le : <?php echo strftime('%A %d %B %Y', strtotime($stop_nobo->date_stop)); ?>)</button></li>
                                <?php endif; ?>
                                <li><button class="btn btn-sm text-danger" name="action" value="delete" onclick="return confirm('Ceci supprimera de manière définitive l\'utilisateur sélectionné')">Supprimer le client</button></li>
                            </form>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-9">
                    <ul class="nav nav-pills" id='tabs_client' style="display: flex; justify-content: space-around;">
                        <input type="hidden" value="<?= $menu_id; ?>" id="get_menu_id" disabled>
                        <li id="tab1"><a data-toggle="tab" href="#menu1">Profil</a></li>
                        <li id="tab2"><a data-toggle="tab" href="#menu2">Adresse</a></li>
                        <li id="tab3"><a data-toggle="tab" href="#menu3">Planning d'entretien</a></li>
                        <!--
                        <li id="tab4"><a data-toggle="tab" href="#menu4">Transactions</a></li>
                        <li id="tab5"><a data-toggle="tab" href="#menu5">Vacances</a></li>
                        <li id="tab6"><a data-toggle="tab" href="#menu6">Disponibilités</a></li>
                        -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <?php include('../../inc/print_flash_helper.php'); ?>
                <!--Profil-->
                <div id="menu1" class="tab-pane fade">
                    <form action="form" id="form_profile" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="menu_id" value="menu1">
                        <input type="hidden" name="user_token" value="<?= $user->user_token;?>"/>
                        <div class="col-sm-12">
                            <div class="col-sm-6" style="margin-top: 23px">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                                        <select class="form-control input-sm" name="gender_id">
                                            <?php $list = App::getListManagement()->getList('gender'); ?>
                                            <?php foreach ($list as $k => $v): ?>
                                                <option value="<?= $v->id;?>" <?= $v->id == $user->gender_id ? 'selected=1' : '';?>><?= $v->field; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input type="text" class="form-control input-sm" name="firstname" value="<?= $user->firstname; ?>" placeholder="Prénom" required/>
                                        <input type="text" class="form-control input-sm" name="lastname" value="<?= $user->lastname; ?>" placeholder="Nom" required/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-sm" name="phone" value="<?= $user->phone; ?>" placeholder="Téléphone" required/>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control pickadate input-sm" name="birthdate" value="<?= $user->birthdate; ?>" placeholder="Date de naissance" id="birthdate"/>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                    <div class="input-group">
                                        <input type="hidden" name="photo_profile" value="<?= $user->photo_profile ?>">
                                        <input type="file" name="photo_profile" style="display: none">
                                        <button type="button" class="btn btn-default btn-sm" style="width:100%" onclick="$(this).prev().trigger('click')">Upload une photo</button>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                        <input type="text" class="form-control input-sm" name="email" value="<?= $user->email; ?>" placeholder="email" required/>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-education"></span></div>
                                        <label class="sr-only" for="job"></label>
                                        <select class="form-control input-sm" name="job_id" id="job">
                                            <?php $list = App::getListManagement()->getList('job');?>
                                            <?php foreach($list as $k => $v):?>
                                                <option value="<?= $v->id; ?>" <?= $user->job_id == $v->id ? 'selected' : ''; ?>><?= $v->field; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-addon"><span>STOP NOBO</span></div>
                                        <input name="stop_nobo" type="text" onkeydown="return false" value="<?= !empty($user->stop_nobo)? $user->stop_nobo: ""; ?>" class="form-control input-sm date_stop_nobo" id="stop_nobo" data-provide="datepicker">
                                        <div style="cursor: pointer;" class="input-group-addon empty_stop_nobo"><span class="text-danger glyphicon glyphicon-remove"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="margin-top: 23px; text-align: center">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-span">Crédits</span>
                                        <label class="sr-only" for="credit"></label>
                                        <input type="number" class="form-control input-sm" name="credit" value="<?= empty($user->credit) ? 0 : $user->credit; ?>" id='credit'/>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-span">Affiliation</span>
                                        <label class="sr-only" for="customer_package_id"></label>
                                        <select name="customer_package_id" class="form-control input-sm" id="customer_package_id">
                                            <option value="">Aucun programme d'affiliation choisi</option>
                                            <?php foreach ($customer_packages as $package): ?>
                                                <option value="<?= $package->id; ?>" <?= $package->id == $customer_info->customer_package_id ? 'selected' :  ''; ?>><?= $package->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-span">Paiement</span>
                                        <label class="sr-only" for="l_payment_mean_id"></label>
                                        <select name="l_payment_mean_id" class="form-control input-sm" id="l_payment_mean_id">
                                            <option value="" <?= empty($user->l_payment_mean_id) ? 'selected' : ''; ?>>Aucun moyen de paiement</option>
                                            <?php foreach ($payment_mean_list as $payment_mean): ?>
                                                <option value="<?= $payment_mean->id; ?>" <?= $payment_mean->id == $user->l_payment_mean_id ? 'selected' :  ''; ?>><?= $payment_mean->field; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-shopping-cart"></span></div>
                                        <label class="sr-only" for="l_acquisition_id">Acquisition</label>
                                        <select name="l_acquisition_id" class="form-control input-sm" id="l_acquisition_id">
                                            <?php foreach ($acquisition_list as $source): ?>
                                                <option value="<?= $source->id; ?>" <?= $source->id == $customer_info->l_acquisition_id ? 'selected' :  ''; ?>><?= $source->field; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <hr class="col-sm-11 hrBo">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-span">Etat civil</span>
                                        <label class="sr-only" for="maritalStatus"></label>
                                        <select class="form-control input-sm" name="maritalStatus" id="maritalStatus">
                                            <?php $list = App::getListManagement()->getList('maritalStatus'); ?>
                                            <?php foreach ($list as $k => $v): ?>
                                                <option value="<?= $v->id;?>" <?= $v->id == $user->maritalStatus_id ? 'selected' : '';?>><?= $v->field; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-span">Enfants</span>
                                        <input type="number" class="form-control input-sm" min="0" step="1" max="10" name="childNb" value="<?= $user->childNb; ?>" placeholder="Nombre d'enfants">
                                    </div>
                                </div>
                                <?php if (!empty($user->password_asked_at)): ?>
                                    <div class="col-sm-12">
                                        <div class="col-xs-12 alert alert-info">Un password temporaire a été généré le <?= $user->password_asked_at; ?></div>
                                    </div>
                                <?php elseif(!empty($user->reset_asked_at)): ?>
                                    <div class="col-sm-12">
                                        <div class="col-xs-12 alert alert-info">Un token temporaire a été envoyé le <?= $user->reset_asked_at; ?></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <hr class="col-sm-11 hrBo">
                            <div class="col-xs-12 text-center">
                                <input type="hidden" name="tab" value="profile"/>
                                <button class="btn btn-info btn-sm" name="action" value="edit">Sauvegarder les changements</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--address-->
                <div id="menu2" class="tab-pane fade">
                    <?php if (!empty($address)):?>
                        <div class="col-sm-6 col-sm-offset-3 text-center">
                            <select class="form-control input-sm" id="address_token">
                                <?php foreach ($address as $k => $v):?>
                                    <option value="<?= $v->address_token; ?>" <?= isset($_GET['address_token']) && $_GET['address_token'] == $v->address_token ? 'selected' : '';?>><?= $v->address; ?> (<?php echo $v->pending == 1 ? 'non validée': 'validée'; ?>)</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3"></div>
                        <?php foreach ($address as $key => $v): ?>
                            <div class="HideMe" id="addr_<?= $v->address_token; ?>">
                                <form id="form_<?= $v->address_token; ?>" method="post" action="form">
                                    <div class="col-sm-6 col-sm-offset-3 text-center">
                                        <div class="input-group">
                                            <span class="input-group-addon input-group-span">Bu</span>
                                            <label class="sr-only" for="cluster_id"></label>
                                            <select class="form-control input-sm" name="cluster_id">
                                                <?php foreach ($cluster as $k => $bu): ?>
                                                    <option value="0">Aucune Bu assignée</option>
                                                    <option value="<?= $bu->id;?>" <?= $bu->id == $v->cluster_id ? 'selected' : '';?>><?= $bu->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="who" value="client">
                                    <input type="hidden" name="user_token" value="<?= $_GET['user_token']; ?>">
                                    <input type="hidden" name="address_token" value="<?= $v->address_token; ?>">
                                    <div class="col-sm-6" style="margin-top: 23px">
                                        <input type="hidden" name="lat" value="<?= $v->lat;?>" id="lat_<?= $v->address_token; ?>">
                                        <input type="hidden" name="lng" value="<?= $v->lng;?>" id="lng_<?= $v->address_token; ?>">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-home"></span>
                                            </div>
                                            <input type="text" class="form-control input-sm" id="autocomplete_<?= $v->address_token; ?>">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-sm" onclick="$('<?= '#security_' . $v->address_token; ?>').trigger('click')">Remove security</button>
                                            </div>
                                        </div>
                                        <br/>
                                        <input type="hidden" name="street_number" value="" id="street_number_<?= $v->address_token; ?>" disabled>
                                        <input type="hidden" name="route" value="" id="route_<?= $v->address_token; ?>" disabled>
                                        <input type="text" class="form-control input-sm" name="address" value="<?= $v->address; ?>" id="address_<?= $v->address_token; ?>" readonly>
                                        <input type="text" class="form-control input-sm" name="address_ext" value="<?= $v->address_ext; ?>" id="administrative_area_level_1_<?= $v->address_token; ?>" readonly>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-sm" name="zipcode" value="<?= $v->zipcode; ?>" id="postal_code_<?= $v->address_token; ?>" readonly>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-sm" name="city" value="<?= $v->city; ?>" id="locality_<?= $v->address_token; ?>" readonly>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-sm" name="country" value="<?= $v->country; ?>" id="country_<?= $v->address_token; ?>" readonly>
                                        </div>
                                        <input type="hidden" id="security_<?= $v->address_token; ?>" onclick="$(this).prevUntil('br').attr('readonly', function(_, attr){return !attr});$(this).prevUntil('br').children().attr('readonly', function(_, attr){return !attr});">
                                        <hr class="col-sm-11 hrBo">
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                                                <input type="text" class="form-control input-sm" name="surface" value="<?= $v->surface;?>" placeholder="surface">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-bed"></span></div>
                                                <input type="text" class="form-control input-sm" name="roomNb" value="<?= empty($v->roomNb) ? 0 : $v->roomNb;?>" placeholder="Chambres">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-tint"></span></div>
                                                <input type="text" class="form-control input-sm" name="waterNb" value="<?= empty($v->waterNb) ? 0 : $v->waterNb;?>" placeholder="Salle d'O">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-span">Présence</span>
                                                <input type="number" class="form-control input-sm" min="0" step="1" name="peopleHome" value="<?= empty($v->peopleHome) ? 0 : $v->peopleHome;?>" placeholder="Nombre de personnes au domicile">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-span">Enfants</span>
                                                <input type="number" class="form-control input-sm" min="0" step="1" max="10" name="childNb" value="<?= empty($v->childNb) ? 0 : $v->childNb;?>" placeholder="Nombre d'enfants">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></div>
                                                <label class="sr-only" for="l_pet_id">Animaux</label>
                                                <select name="l_pet_id" class="form-control input-sm" id="l_pet_id">
                                                    <?php foreach ($pet_list as $pet): ?>
                                                        <option value="<?= $pet->id; ?>" <?= $pet->id == $v->l_pet_id ? 'selected' :  ''; ?>><?= $pet->field; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="checkbox">
                                                <label for="have_ironing">
                                                    <input type="hidden" name="have_ironing" value="0">
                                                    <input type="checkbox" name="have_ironing" id="have_ironing" value="1" <?= empty($v->have_ironing) ? '' : 'checked';?>>
                                                    Besoin de repassage ?
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label for="have_key">
                                                    <input type="hidden" name="have_key" value="0">
                                                    <input type="checkbox" name="have_key" id="have_key" value="1" <?= empty($v->have_key) ? '' : 'checked';?>>
                                                    Double des clés ?
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12  col-sm-6 SpaceTop text-center">
                                        <?php $main_provider = isset($v->mainProvider) ? App::getUser()->getUserById($v->mainProvider) : null; ?>
                                        <!-- l'inclure dans le call adresse avec le cas où mainprovider est null -->
                                        <?php if (!empty($main_provider)): ?>
                                            <div class="input-group">
                                                <div class="input-group-addon input-group-span"><span>Prestataire</span></div>
                                                <input type="text" class="form-control input-sm" name="main_provider" data-href="/v1/provider/edit?=<?= $main_provider->user_token ?>" value="<?= $main_provider; ?>" id="main_provider_<?= $v->address_token; ?>" onclick="window.document.location = $(this).data("href");" disabled>
                                            </div>
                                        <?php endif; ?>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Bâtiment</span></div>
                                            <input type="text" class="form-control input-sm" name="batiment" value="<?= $v->batiment; ?>" id="batiment_<?= $v->address_token; ?>" placeholder="Bâtiment">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Digicode</span></div>
                                            <input type="text" class="form-control input-sm" name="digicode" value="<?= $v->digicode; ?>" id="digicode_<?= $v->address_token; ?>" placeholder="Digicode">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Digicode n°2</span></div>
                                            <input type="text" class="form-control input-sm" name="digicode_2" value="<?= $v->digicode_2; ?>" id="digicode_2_<?= $v->address_token; ?>" placeholder="Deuxième digicode">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Code lockbox</span></div>
                                            <input type="text" class="form-control input-sm" name="lockbox_code" value="<?= $v->lockbox_code; ?>" id="lockbox_code_<?= $v->address_token; ?>" placeholder="Code de la lockbox">
                                            <div class="input-group-btn"><button type="button" class="btn btn-default btn-sm" onclick="$('#lockbox_code_<?= $v->address_token; ?>').val(random_code(4))"><span class="glyphicon glyphicon-repeat"></span></button></div>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Lieu lockbox</span></div>
                                            <input type="text" class="form-control input-sm" name="lockbox_localization" value="<?= $v->lockbox_localization; ?>" id="lockbox_localization_<?= $v->address_token; ?>" placeholder="Emplacement de la lockbox">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span class="">Interphone</span></div>
                                            <input type="text" class="form-control input-sm" name="doorBell" value="<?= $v->doorBell; ?>" placeholder="Interphone" maxlength="20">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Porte</span></div>
                                            <input type="text" class="form-control input-sm" name="door" value="<?= $v->door; ?>" placeholder="Porte" maxlength="20">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Etage</span></div>
                                            <input type="number" class="form-control input-sm" name="floor" value="<?= empty($v->floor) ? 0 : $v->floor; ?>" placeholder="Etage" max="99">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-addon input-group-span"><span>Métro</span></div>
                                            <input type="text" class="form-control input-sm MetroAutoComplete" value="<?= $v->subway_station; ?>" name="subway_station" placeholder="Ligne, arret de métro">
                                        </div>
                                        <input type="hidden" value="<?= $v->subway_id; ?>" name="subway_id" class="form-control input-sm MetroLineAutoComplete">
                                        <textarea class="form-control input-sm" name="description" maxlength="150" style="resize: none" placeholder="Description"><?= $v->description;; ?></textarea>
                                        <div class="row col-sm-12">
                                            <label for="">Ceci est la résidence du client ?</label>
                                            <div>
                                                <label class="radio-inline">
                                                    <input name="home" value="1" type="radio" <?= $v->home === '1' ? 'checked' : ''; ?>/>Oui
                                                </label>
                                                <label class="radio-inline">
                                                    <input name="home" value="0" type="radio" <?= $v->home === '1' ? '' : 'checked'; ?>/>Non
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                    <hr class="col-sm-11 hrBo">
                                    <div class="col-xs-12 text-center">
                                        <input type="hidden" name="tab" value="address"/>
                                        <button type="reset" class="btn btn-warning btn-sm">Reset</button>
                                        <button class="btn btn-info btn-sm" name="action" value="edit_address">Sauvegarder les changements</button>
                                        <button class="btn btn-danger btn-sm" name="action" value="delete_address" onclick="confirm('Ceci supprimera de manière définitive l\'adresse sélectionnée')"><span class="glyphicon glyphicon-remove"></span> Supprimer</button>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal_address"><span class="glyphicon glyphicon-plus"></span> Ajouter</button>
                                    </div>
                                </form>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-sm-6 col-sm-offset-3">
                            <button type="button" class="btn btn-success col-xs-12" data-toggle="modal" data-target="#modal_address"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    <?php endif; ?>
                    <!-- début modal address -->
                    <div id="modal_address" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                    <h4 class="modal-title">Ajouter une adresse</h4>
                                </div>
                                <form method="post" action="form">
                                    <div class="modal-body">
                                        <div class="row col-sm-10 col-sm-offset-1">
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                                                <input type="text" class="form-control" id="autocomplete_home" required>
                                                <span class="input-group-btn"><button type="button" class="btn btn-default" onclick="$('#trigger').trigger('click');">Remove security</button></span>
                                            </div>
                                            <br/>
                                            <input type="hidden" class="form-control" name="street_number" id="street_number_home" disabled>
                                            <input type="hidden" class="form-control" name="route" id="route_home" disabled>
                                            <input type="text" class="form-control" name="address" id="address_home" readonly>
                                            <input type="text" class="form-control" name="address_ext" id="administrative_area_level_1_home" readonly>
                                            <input type="text" class="form-control" name="zipcode" id="postal_code_home" readonly>
                                            <input type="text" class="form-control" name="city" id="locality_home" readonly>
                                            <input type="text" class="form-control" name="country" id="country_home" readonly>
                                            <input type="hidden" id="trigger" onclick="$(this).prevUntil('button').attr('readonly', function(_, attr){return !attr});">
                                            <div class="row col-sm-12">
                                                <label for="home_yes">Ceci est la résidence du client ?</label>
                                                <div>
                                                    <label class="radio-inline">
                                                        <input name="home" id="home_yes" value="1" type="radio"/>Oui
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input name="home" id="home_no" value="0" type="radio" checked/>Non
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row col-sm-12">
                                                <label for="radio_wlst_yes">Ajouter le client en liste d'attente ?</label>
                                                <div>
                                                    <label class="radio-inline">
                                                        <input name="radio_add_wlist" id="radio_wlist_yes" value="1" type="radio"/>Oui
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input name="radio_add_wlist" id="radio_wlist_no" value="0" type="radio" checked/>Non
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="user_token" value="<?= $_GET['user_token']; ?>">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                        <button type="submit" class="btn btn-danger" name="action" value="add_address">Enregister une nouvelle adresse</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- fin modal address -->
                </div>
                <!--Planning d'entretien-->
                <div id="menu3" class="tab-pane fade">
                    <?php if (!empty($address)): ?>
                        <div class="col-sm-6 col-sm-offset-3 text-center" style="margin-bottom: 1em">
                            <input type="hidden" name="address_mp_id" value="<?= $address[0]->id ?>" id="address_mp_id">
                            <?php if ($address_count > 0): ?>
                                <select class="form-control input-sm" id="addr_mp" name="addr_mp">
                                    <?php foreach ($address as $k => $v):?>
                                        <option value="<?= $v->address_token; ?>"  <?= !empty($_GET['address_token']) && $_GET['address_token'] == $v->address_token ? 'selected' : '';?>><?= $v->address; ?> (<?php echo $v->pending == 1 ? 'non validée': 'validée'; ?>)</option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-6" id="mp_info"></div>
                        <div class="col-xs-6">
                            <div class="row">
                                <div id="productLocalization"></div>
                            </div>
                            <div class="row SpaceTop">
                                <div id="nobleMaterials"></div>
                            </div>
                            <div class="row SpaceTop">
                                <div id="trashLocalization"></div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-warning text-center" role="alert">
                            <strong>Il n'y pas d'adresse définie pour ce client</strong>
                        </div>
                    <?php endif; ?>
                </div>
                <!--Transactions-->
                <?php
                $curr = App::getCompany()->getCompanyConf();
                if(isset($curr) && !empty($curr)) {
                    $companyId = $curr->value;
                } ?>

                <div id="menu4" class="tab-pane fade">
                    <div class="col-sm-6 col-sm-offset-3 text-center" style="margin-bottom: 1em">
                        <div class="col-xs-12">
                            <div id="hidden_form_container" style="display:none;"></div>
                            <?php if(isset($companyId)): ?>
                                <div class="col-sm-3">
                                    <div class="form-group form-inline">
                                        <select class="form-control" name="yearpicker" id="yearpicker"></select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <input type="hidden" id="tax_company_id" name="tax_company_id" value="<?=$companyId?>">
                                    <input type="hidden" id="tax_user_token" name="tax_user_token" value="<?= $_GET['user_token'] ?>">
                                    <button class="btn btn-default" id="tax_credit_btn" name="action" value="downloadCredit"> Télécharger le crédit d'impôt</button>
                                </div>
                            <?php else: ?>
                                <p>Veuillez configurer une entreprise pour générer le crédit d'impôt</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!--Vacances-->
                <div id="menu5" class="tab-pane fade">
                    <!--Modal_alert-->
                    <div id="modal_alert" class="modal fade">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                    <div style="font-size: 15px; font-weight: bold;">Attention un ou plusieurs conflict(s) ont lieux, voulez-vous continuer?</div>
                                </div>
                                <div class="modal-body">
                                </div>
                                <div class="modal-footer">
                                    <button id="btn-refuse-holidays-conflict" type="button" class="btn btn-danger">Non</button>
                                    <button id="btn-accept-holidays-conflict" type="button" class="btn btn-success">Oui</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fin modal alert-->
                    <?php if (!empty($address)): ?>
                        <div class="col-sm-6 col-sm-offset-3 text-center" style="margin-bottom: 1em">
                            <select class="form-control input-sm" id="holiday_address">
                                <?php foreach ($address as $k => $v):?>
                                    <option value="<?= $v->address_token; ?>" <?= !empty($_GET['address_token']) && $_GET['address_token'] == $v->address_token ? 'selected' : '';?>><?= $v->address; ?> (<?php echo $v->pending == 1 ? 'non validée': 'validée'; ?>)</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif ?>
                    <div class="col-sm-8">
                        <label class="col-xs-6">Début des vacances<input name="date" type="text" onkeydown="return false" class="form-control input-sm date_picker" id="start_date" data-provide="datepicker" required></label>
                        <label class="col-xs-6">Fin des vacances<input name="date" type="text" onkeydown="return false" class="form-control input-sm date_picker" id="end_date" data-provide="datepicker" required></label>
                    </div>
                    <div class="col-sm-4">
                        <button style="margin-top: 13px;" type="button" class="btn btn-success col-xs-12" id="add_holiday" name="action"><span class="glyphicon glyphicon-plus"></span> Ajouter</button>
                    </div>
                    <div style="margin-top: 10px;" class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading panel-heading-xs">Vacances du client :</div>
                            <div class="panel-body panel-body-holidays"></div>
                        </div>
                    </div>
                </div>

                <!-- Disponibilité -->
                <div id="menu6" class="tab-pane fade">
                    <div class="col-sm-6 col-sm-offset-3 text-center" style="margin-bottom: 1em">
                        <select class="form-control input-sm" id="address_dispo" name="address_select">
                            <?php foreach ($address as $k => $v):?>
                                <option value="<?= $v->address_token; ?>" <?= !empty($_GET['address_token']) && $_GET['address_token'] == $v->address_token ? 'selected' : '';?>><?= $v->address; ?> (<?php echo $v->pending == 1 ? 'non validée': 'validée'; ?>)</option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <!--calendar_Dispo-->
                    <div class="col-sm-12">
                        <div id="calendar"></div>
                    </div>
                    <!--Modal_Dispo-->
                    <div id="modal_dispo" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                                    <h4 id="modal_dispo_Title" class="modal-title"></h4>
                                </div>
                                <div id="modal_dispo_Body" class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    <button type="button" class="btn btn-danger" data-href="#" data-toggle="modal" id="confirm-delete-dispo">Supprimer le créneau de disponibilité</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-2">
        <?php include_once ('../../inc/customer/customer_action_infos.php'); ?>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="sendModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">confirmez-vous l'envoi de l'email a :  </h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-success" name="action" value="visualiser" data-dismiss="modal"><span class="glyphicon glyphicon-eye-open"></span></button>
                    <button type="button" class="btn btn-success" name="action" value="download_" data-dismiss="modal"><span class="glyphicon glyphicon-cloud-download"></span></button>
                    <button type="button" class="btn btn-success" name="action" value="send" data-dismiss="modal"><span class="glyphicon glyphicon-send"></span></button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal_confirm_start-->
    <div id="modal_stop_nobo" class="modal fade">
        <div class="modal-dialog modal-small">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                    <div style="font-size: 15px; font-weight: bold;">Attention toutes les missions après la date choisie seront supprimer êtes-vous sur ?</div>
                </div>
                <div class="modal-footer">
                    <form method="post" action="form" class="form_confirm">
                        <input type="hidden" name="user_token" value="<?= $user->user_token; ?>">
                        <div class="col-sm-12 stop_nobo_info">
                            <label class="col-xs-12 text-left nobo_stop_label" ><h5> Date : </h5><input name="date" type="text" class="form-control input-sm date_picker nobo_stop" data-provide="datepicker" required></label>
                        </div>
                        <div class="form-group col-sm-12" style="margin-top: 2em;" id="confirm">
                            <button name="action" value="stop_nobo" class="btn btn-primary col-xs-4 col-xs-offset-4 btn-sm">Valider</button>
                        </div>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <!-- fin modal confirm_end -->
    <style type="text/css">
        .cell{
            border: 1px solid black;
            padding: 5px;
        }
        .toggle-cell {
            background-color: #e60000;
        }
        .selected {
            background-color: green;
        }
        .ui-datepicker-calendar {
            display: none;
        }
        .fc-day.fc-today{
            background: #FFF !important;
            border: none !important;
            border-top: 1px solid #ddd !important;
            font-weight: bold;
        }
        .fc-day-header.fc-today{
            background: red !important;
        }
        .invalid_event {
            background: repeating-linear-gradient(
                    60deg,
                    red,
                    red 5px,
                    #ff9999 5px,
                    #ff9999 10px
            );
        }
        .border-left{
            border-left: 2px solid grey;
        }
        .border-top-description{
            border-top: 1px solid darkseagreen;
        }
        .border-bottom-description{
            border-bottom: 1px solid darkseagreen;
            margin-bottom: 2px;
        }
        .padding{
            padding: 2px ;
        }
        .marginBottom{
            margin-bottom:4px;
        }
        .big-margin{
            margin : 20px;
        }
        .text-bold-title {
            padding-top: 6px;
            font-size: 15px;
            font-weight: bold;
        }
        .text-description {
            overflow-y: auto;
            font-size: 14px;
        }
        .pac-container {
            z-index: 99999;
        }
    </style>
    <script src="/js/lib/bootstrap-notify.min.js"></script>
    <script src="/js/lib/bootbox.min.js"></script>
    <script src="/js/edit_client.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/taxation.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyBeMne5-vmGGRYsdcCxDb-PYs3McbjVmCg&libraries=places&callback=initialize" async defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
    <script type="text/javascript">
        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        $('.img-circle').magnificPopup({type: 'image'});

        function fillInAddr_id()
        {
            let address_token = $('#address_token').val();
            let id =
                ($("#modal_address").data('bs.modal') || {}).isShown ?
                    'home' :
                    address_token.substr(address_token.indexOf('_') + 1);
            let place = autocomplete.getPlace();

            for (let component in componentForm) {
                document.getElementById(component + '_' + id).value = '';
            }
            for (let i = 0; i < place.address_components.length; i++) {
                let addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + '_' + id).value = val;
                }
            }
            $('#lat_' + id).val(place.geometry.location.lat());
            $('#lng_' + id).val(place.geometry.location.lng());
            $('#address_' + id).val($('#street_number_' + id).val() + ' ' + $('#route_' + id).val());
        }

        function initialize() {
            let address_token = $('#address_token').val();

            if (address_token) {
                $('.current').hide().removeClass('current');
                $('#addr_' + address_token).show().addClass('current');
                initGoogleAutocomplete_addr('autocomplete_' + address_token);
            }
        }

        jQuery(document).ready(function() {

            let address_modal = $('#modal_address');


            /***INIT DATEPICKER***/
            $('.nobo_stop').datepicker({
                language: 'fr',
                format: 'dd-mm-yyyy'
            });

            /***ADD_STOP_NOBO_SELECT_MODAL***/
            var list = <?php echo isset($reason_list) ? json_encode($reason_list) : "[]";?>;
            $('.stop_nobo_info').append(create_select_reason_stop_nobo(list));
            $('.select_reason_stop').change(function(){
                let reason = $(this).val();
                if (reason === 'autre'){
                    $('.stop_nobo_info').append('<label class="col-xs-12 text-left other_reason" ><textarea name="reason" class="form-control" rows="4" id="comment" required></textarea></label>');
                }
                else if ($('.other_reason')[0]){
                    $('.other_reason').remove();
                }
            });
            /***MENU***/
            let menu_id = $('#get_menu_id').val();

            $('#tab' + menu_id).addClass('active');
            $('#menu' + menu_id).addClass('in active');

            /***INFOS CLIENTS***/
            var token = '<?php echo $_GET['user_token'] ?>';
            var address;
            var allMetro = <?php echo isset($allMetro) ? json_encode($allMetro) : "[]";?>;
            if (allMetro) {
                $('.MetroAutoComplete').devbridgeAutocomplete({
                    lookup: allMetro,
                    maxHeight: 200,
                    onSelect: function (suggestion) {
                        $(this).prev().val(suggestion.value);
                        $(".MetroLineAutoComplete").val(suggestion.data.id);
                    },
                    onInvalidateSelection: function () {
                        $(".MetroLineAutoComplete").val(null);
                    }
                });
            } else {
                bo_notify('Impossible de charger les stations de métro', 'metro');
            }

            $('.date_picker').datepicker({
                startDate: '+0d',
                language: 'fr',
                format: 'dd-mm-yyyy'
            });

            $('.empty_stop_nobo').on('click', function () {
                $('#stop_nobo').val("");
            });

            //start Autocomplete address manager

            address_modal.on('hidden.bs.modal', function () {
                initialize();
            });
            address_modal.on('shown.bs.modal', function () {
                initGoogleAutocomplete_addr('autocomplete_home');
                $('#autocomplete_home').focus();
            });

            $('#address_token').change(function () {
                initialize();
            });

            // end Autocomplete address manager

            $('#addr_mp').on('change', function () {
                let address_token = $('#addr_mp').val();
                let dataString = 'address_token='+ address_token;
                $('.div_mp').hide();
                $('#div_mp_' + $(this).val()).show();

                ajax_call_html('/v1/client/inc/maintenance_planning.php', 'GET', dataString,
                    function(r) {$('#mp_info').html(r);},
                    function () {bo_notify('Les plannings d\'entretien ont un problème, prévenir un admin')}
                );
                ajax_call_html('/v1/client/inc/product_localization.php', 'GET', dataString,
                    function(r) {$('#productLocalization').html(r);},
                    function () {bo_notify('La localisation des produits a un problème, prévenir un admin')}
                );
                ajax_call_html('/v1/client/inc/noble_materials.php', 'GET', dataString,
                    function(r) {$('#nobleMaterials').html(r);},
                    function () {bo_notify('Les matériaux nobles ont un problème, prévenir un admin')}
                );
                ajax_call_html('/v1/client/inc/trash_localization.php', 'GET', dataString,
                    function(r) {$('#trashLocalization').html(r);},
                    function () {bo_notify('La localisation du local poubelle a un problème, prévenir un admin')}
                );
            }).trigger('change');

            /***Reload password***/
            $("#reload_password").on("click", function () {
                bootbox.prompt({
                    size: "small",
                    title: "Réinitialiser le mot de passe manuellement",
                    onEscape: true,
                    backdrop: true,
                    callback: function(result){
                        if (result !== null) {
                            ajax_call(
                                "api/password",
                                "PUT",
                                {password : result, user_token : token},
                                function (ret) {
                                    bo_notify(ret.message, ret.data.message_color)
                                }, function () {
                                    bo_notify("Un problème est survenu", "danger")
                                }
                            )
                        }
                    }
                })
            });
            /***Sent email with new token to reload password***/
            $("#send_reset_token").on("click", function () {
                bootbox.confirm({
                    size: "medium",
                    message: "Êtes-vous sur de vouloir envoyer un mail pour réinitialiser le mot de passe du client ?",
                    onEscape: true,
                    backdrop: true,
                    callback: function(result){
                        if (result === true) {
                            ajax_call(
                                "api/ask_password",
                                "PUT",
                                {user_token : token},
                                function (ret) {
                                    bo_notify(ret.message, ret.data.message_color)
                                }, function () {
                                    bo_notify("Un problème est survenu", "danger")
                                }
                            )
                        }
                    }
                })
            });
            /***CALENDRIER***/

            var calendar_s = $('#calendar');

            function displayCustomerDispo(customer_token) {
                calendar_s.fullCalendar('destroy');
                calendar_dispo_customer(customer_token);
                calendar_s.fullCalendar('render');
            }

            $('#tab7').on('click', function () {
                let loading = 250;

                $(this).unbind("click");
                setTimeout(function () {
                    displayCustomerDispo(token);
                }, loading);

            });

            $('#address_dispo').on('change', function () {
                displayCustomerDispo(token);
            });

            /***CARDEX***/

            $('.action-infos').css('height', $(document).height() * 0.66);
        });

        function random_code(digit_number) {
            let random_code = "";

            while (digit_number--)
                random_code += random_digit();
            return random_code;
        }

        function random_digit() {
            return Math.floor(Math.random() * 10);
        }
        $('.remove').on('click', function () {
            alert($("input[name=mp_template_id]").val());
        });

        $(function(){
            $('#addMore').on('click', function() {
                var data = $("#tb tr:eq(1)").clone(true).appendTo("#tb");
                data.find("input").val('');
            });
            $(document).on('click', '.remove', function() {
                var trIndex = $(this).closest("tr").index();
                if(trIndex>1) {
                    $(this).closest("tr").remove();
                }
            });
        });
    </script>
<?php
require('../../inc/footer_bo.php');
