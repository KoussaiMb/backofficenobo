<?php

require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $validator = new Validator($_POST);

    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'addresse est manquante');
    $validator->is_id('lr_product_id', 'Le produit est invalide', 'Le produit est manquant');
    $validator->is_id('lr_room_id', 'La pièce est invalide', 'La pièce est manquante');
    $validator->no_symbol('description', "La description est invalide");
    if ($validator->is_valid() === false)
        parseJson::error('Les données pour ajouter une loclisation de produit sont erronnées', $validator->getErrors())->printJson();
    $product_localization_id = App::getCardex()->insertProductLocalization($_POST);
    if ($product_localization_id === false)
        parseJson::error('Une erreur est survenue lors de l\'ajout de la localisation de produit')->printJson();
    parseJson::success('La localisation du produit a bien été ajoutée', ['product_localization_id' => $product_localization_id])->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->no_symbol('product_localization_id', "La localisation du produit est invalide", 'La localisation du produit  est manquante');
    if ($validator->is_valid() === false)
        parseJson::error('Les données pour supprimer une loclisation de produit sont erronnées', $validator->getErrors())->printJson();
    $ret = App::getCardex()->deleteProductLocalizationById($_DELETE['product_localization_id']);
    if ($ret === false)
        parseJson::error('Une erreur est survenue lors de la suppression de la localisation de produit')->printJson();
    parseJson::success('La localisation du produit a bien été supprimée')->printJson();
}
$json = new parseJson(403, 'Accès interdit', null);
$json->printJson();