<?php

require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $validator = new Validator($_POST);

    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'addresse est manquante');
    $validator->is_id('l_noble_materials_id', 'Le produit est invalide', 'Le produit est manquant');
    $validator->is_id('lr_room_id', 'La pièce est invalide', 'La pièce est manquante');
    $validator->no_symbol('comment', "Le commentaire est invalide");
    if ($validator->is_valid() === false)
        parseJson::error('Les données pour ajouter une un matériau noble sont erronnées', $validator->getErrors())->printJson();
    $mp_noble_materials_id = App::getMaintenancePlanningType()->addNobleMaterials($_POST);
    if ($mp_noble_materials_id === false)
        parseJson::error('Une erreur est survenue lors de l\'ajout du matériau noble')->printJson();
    parseJson::success('Le matériau noble a bien été ajoutée', ['mp_noble_materials_id' => $mp_noble_materials_id])->printJson();
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->no_symbol('mp_noble_materials_id', "Le matériau noble est invalide", 'La localisation du produit  est manquante');
    if ($validator->is_valid() === false)
        parseJson::error('Les données pour supprimer un matériau noble sont erronnées', $validator->getErrors())->printJson();
    $ret = App::getMaintenancePlanningType()->deleteNobleMaterials($_DELETE['mp_noble_materials_id']);
    if ($ret === false)
        parseJson::error('Une erreur est survenue lors de la suppression du matériau noble')->printJson();
    parseJson::success('Le matériau noble a bien été supprimé')->printJson();
}
$json = new parseJson(403, 'Accès interdit', null);
$json->printJson();