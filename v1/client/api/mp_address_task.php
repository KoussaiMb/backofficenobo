<?php
include_once('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $validator = new Validator($_POST);
    $validator->is_id('mp_address_id', 'L\'adresse est invalide', 'L\'adresse est introuvable');
    $validator->is_id('mp_unit_id', 'Le type d\'entretien est invalide', 'Le type d\'entretien est introuvable');
    $validator->is_id('l_rollingTask_id', 'La tâche roulante est invalide', 'La tâche roulante est introuvable');
    if ($validator->is_valid() === false)
        parseJson::error(
            "Les données pour ajouter une tâche roulante sont invalides",
            $validator->getErrors()
        )->printJson();
    $mp_address_task_id = App::getMaintenancePlanningType()->addAddressRollingTask($_POST);
    if ($mp_address_task_id === false)
        parseJson::error("Une erreur s'est produit lors de l'ajout d'une tâche roulante au client")->printJson();
    parseJson::success("La tâche roulante a bien été ajoutée au client", ['mp_address_task_id' => $mp_address_task_id])->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
    $_DELETE = App::getRequest();
    $validator = new Validator($_DELETE);

    $validator->is_id('mp_address_task_id', 'La tâche roulante est invalide', 'La tâche roulante est introuvable');
    if ($validator->is_valid() === false)
        parseJson::error("Les données pour modifier le planning d'entretien du client sont erronnées", $validator->getErrors())->printJson();
    $mp_address = App::getMaintenancePlanningType()->deleteAddressRollingTask($_DELETE['mp_address_task_id']);
    if ($mp_address === false)
        parseJson::error("Une erreur s'est produit lors de la suppression de la tâche roulante")->printJson();
    parseJson::success("La tâche roulante a bien été supprimée au client")->printJson();
}