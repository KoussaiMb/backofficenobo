<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();

    $validator = New Validator($_PUT);

    $validator->isToken("user_token", "L'utilisateur n'est pas valide", "L'utilisateur est introuvable");

    if (!$validator->is_valid()) {
        $json = parseJson::error(null, $validator->getErrors());
        $json->printJson();
    }

    $reset_token = my_crypt::genToken(60);
    $ret = App::getUser()->updateResetToken($_PUT['user_token'], $reset_token);
    if (!$ret)
        parseJson::error("Un problème est intervenu lors de l'attribution d'un 
        jeton pour changer son mot de passe au client", ["message_color" => "danger"])->printJson();

    $user = App::getUser()->getUserByToken($_PUT['user_token']);
    if ($user)
        $ret = App::getMail()->reset_password($user);
    if (!$ret)
        parseJson::error("Une erreur est survenu dans l'envoi du mail au client", ["message_color" => "danger"])->printJson();
    else
        parseJson::success("Un email avec un jeton pour réinitialiser son mot de passe 
        a été envoyé au client", ["message_color" => "success"])->printJson();
}


