<?php
include_once('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $validator = new Validator($_POST);
    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'adresse est introuvable');
    $validator->is_id('mp_template_id', 'Le planning d\'entretien type n\'est pas valide', 'Le planning d\'entretien type est introuvable');
    $validator->no_symbol('comment', 'Le commentaire n\'est pas valide');
    if ($validator->is_valid() === false)
        parseJson::error(
            "Les données pour ajouter le planning d'entretien du client sont erronnées",
            $validator->getErrors()
        )->printJson();
    $mp_address_id = App::getMaintenancePlanningType()->addMpAddress(
        $_POST['address_id'],
        $_POST['mp_template_id'],
        $_POST['comment']
    );
    if ($mp_address_id === false)
        parseJson::error("Une erreur s'est produit lors de l'ajout du planning d'entretien au client")->printJson();
    parseJson::success("Le planning d'entretien a bien été ajouté au client", ['mp_address_id' => $mp_address_id])->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT')
{
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);
    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'adresse est introuvable');
    $validator->is_id('mp_template_id', 'Le planning d\'entretien type n\'est pas valide', 'Le planning d\'entretien type est introuvable');
    $validator->is_id('mp_address_id', 'Le planning d\'entretien du client n\'est pas valide', 'Le planning d\'entretien du client est introuvable');
    $validator->no_symbol('comment', 'Le commentaire n\'est pas valide');
    if ($validator->is_valid() === false)
        parseJson::error("Les données pour modifier le planning d'entretien du client sont erronnées", $validator->getErrors())->printJson();
    $mp_address = App::getMaintenancePlanningType()->editMpAddress(
        $_PUT['address_id'],
        $_PUT['mp_template_id'],
        $_PUT['comment'],
        $_PUT['mp_address_id']
    );
    if ($mp_address === false)
        parseJson::error("Une erreur s'est produit lors de la modification du planning d'entretien au client")->printJson();
    parseJson::success("Le planning d'entretien a bien été modifié au client", ['mp_address_id' => $_PUT['mp_address_id']])->printJson();
}