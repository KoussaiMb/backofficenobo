<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 13/09/2017
 * Time: 17:00
 */

require_once ('../../../inc/bootstrap.php');

$fullcalendar = App::getFullCalendar();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $weekday = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    $json_data = [];
    $validator = App::get_instance_validator($_GET);
    $validator->isToken('user_token', 'Le client est invalide', 'Le client est introuvable');
    $validator->isToken('address_token', 'L\'addresse est invalide', 'L\'addresse est introuvable');
    if (!$validator->is_valid())
        parseJson::error('Les informations pour récupérer les disponibilités sont corompues', $validator->getErrors())->printJson();

    $dispoclient = $fullcalendar->getDispoClient($_GET);
    if ($dispoclient === false)
        parseJson::error('Erreur lors de la récupération des disponibilitées client')->printJson();

    if (count($dispoclient) == 0) {
        for ($i = 1; $i <= 5; $i++) {
            $event = array (
                "token" => $_GET['user_token'],
                "day" => $i,
                "start" => "08:00:00",
                "end" => "19:00:00",
                "address_token" => $_GET['address_token']
            );

            $dispo_id = $fullcalendar->addDispoClient($event);
            if ($dispo_id === 0)
                parseJson::error('Erreur lors de l\'ajout de la disponibilité client')->printJson();
            $start_time = explode(":", $event["start"]);
            $end_time = explode(":", $event["end"]);
            $nextday = date('Y-m-d H:i:s', strtotime('next '.$weekday[$i], strtotime('previous sunday')));
            $startTime = date('Y-m-d H:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
            $endTime = date('Y-m-d H:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
            array_push($json_data, array("start" => $startTime, "end" => $endTime, "id" => $dispo_id));
        }
    } else {
        foreach ($dispoclient as $key => $value){
            $start_time = explode(":", $value->start);
            $end_time = explode(":", $value->end);
            $nextday = date('Y-m-d H:i:s', strtotime('next '.$weekday[$value->day], strtotime('previous sunday')));
            $startTime = date('Y-m-d H:i:s',strtotime('+'.$start_time[0].' hours'.'+'.$start_time[1].' minutes',strtotime($nextday))); //debut prestation
            $endTime = date('Y-m-d H:i:s',strtotime('+'.$end_time[0].' hours'.'+'.$end_time[1].' minutes',strtotime($nextday))); // fin prestation
            array_push($json_data, array("start" => $startTime, "end" => $endTime, "id" => $value->id));
        }
    }
    parseJson::success('Les disponibilitées ont bien été récuperées', $json_data)->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['token']) && isset($_POST['day']) && isset($_POST['start']) && isset($_POST['end']) && isset($_POST['address_token'])) {
        $event = array (
            "token" => json_decode($_POST['token']),
            "day" => json_decode($_POST['day']),
            "start" => json_decode($_POST['start']),
            "end" => json_decode($_POST['end']),
            "address_token" => json_decode($_POST['address_token'])
        );

        if (($data = $fullcalendar->addDispoClient($event)) === false) {
            $json = parseJson::error('Erreur lors de l\'ajout de la disponibilité client');
            $json->printJson();
        }

        $json = parseJson::success('votre disponibilité a bien été ajouté', $data);
        $json->printJson();
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $_PUT = App::getRequest();
    if (isset($_PUT['start']) && isset($_PUT['end']) && isset($_PUT['id']) && isset($_PUT['day'])) {
        $event = array(
            "start" => json_decode($_PUT['start']),
            "end" => json_decode($_PUT['end']),
            "id" => json_decode($_PUT['id']),
            "day" => json_decode($_PUT['day'])
        );

        if (($fullcalendar->updateDispoClient($event)) === false) {
            $json = parseJson::error('Erreur lors de la modification des disponibilitées client');
            $json->printJson();
        } else {
            $json = parseJson::success('Vos modifications des disponibilitées client ont bien été prisent en comptes');
            $json->printJson();
        }
    }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $_DELETE = App::getRequest();
    if (isset($_DELETE['id']) && isset($_DELETE['token'])) {

        $id = json_decode($_DELETE['id']);
        $token = json_decode($_DELETE['token']);
        if (($fullcalendar->delDispoClient($id, $token)) === false) {
            $json = parseJson::error('Erreur lors de la suppression des disponibilitées client');
            $json->printJson();
        } else{
            $json = parseJson::success('La suppression de la disponibilité a bien été prise en compte');
            $json->printJson();
        }
    }
}
