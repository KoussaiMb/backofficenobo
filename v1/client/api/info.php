<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 09/08/2018
 * Time: 17:24
 */

require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = New Validator($_GET);

    $validator->is_id("customer_id", "Le client n'est pas valide", "Le client est manquant");
    if (!$validator->is_valid())
        parseJson::error("Les données pour récupérer le client son erronnées", $validator->getErrors())->printJson();
    $customer = App::getCustomer()->getCustomerById($_GET['customer_id']);
    if ($customer === false)
        parseJson::success("Impossible de récupérer les informations du client")->printJson();
    parseJson::success("Les informations du client ont été récupéré", $customer)->printJson();
}


