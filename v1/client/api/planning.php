<?php
require('../../../inc/bootstrap.php');

$type = 'planning';

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    $validator = App::get_instance_validator($_POST);
    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'adresse est manquante');
    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est manquant');
    $validator->isToken('address_token', 'L\'adresse est invalide', 'L\'adresse est manquante');
    $validator->is_alpha('action', 'L\'action est invalide', 'L\'action est manquante');
    $validator->is_alpha('firstname', 'Le Prénom est invalide', 'Le Prénom est manquante');
    $validator->is_alpha('lastname', 'Le Nom est invalide', 'Le Nom est manquante');
    $validator->is_email('email', 'Le format de l\'action est invalide', 'L\'action est manquante');
    if (!$validator->is_valid()){
        $json = parseJson::error('L\'un des champs est invalide ou manquant',$validator->getErrors());
        $json->printJson();
    }

    $pdf = App::get_instance_generatePdf($_POST, $type);
    $mail_info['pdf'] = $pdf->generate_Pdf();
    if (empty($mail_info['pdf'])){
        $json = parseJson::error('Le PDF n\'a pas été généré ...!');
        $json->printJson();
    }
    $mail_info['data_user'] = array('email' => $_POST['email'], 'firstname' => $_POST['firstname'], 'lastname' => $_POST['lastname']);

    $mail = App::getMail();
    $response = $mail->sendPlanning($mail_info);
    if (!$response){
        $json = parseJson::error('Le Mail n\'a pas été envoyé');
    } else {
        $json = parseJson::success("Le mail a bien été envoyé");
    }
    $json->printJson();
}
else if($_SERVER['REQUEST_METHOD'] === 'GET') {
    $validator = App::get_instance_validator($_GET);
    $validator->is_id('address_id', 'L\'adresse est invalide', 'L\'adresse est manquante');
    $validator->isToken('user_token', 'L\'utilisateur est invalide', 'L\'utilisateur est manquant');
    $validator->isToken('address_token', 'L\'adresse est invalide', 'L\'adresse est manquante');
    $validator->is_alpha('action', 'Le format de l\'action est invalide', 'L\'action est manquante');
    if (! $validator->is_valid()){
        App::array_to_flash($validator->getErrors());
        App::redirect("edit?user_token=" . $_GET['user_token']);
    }

    $pdf = App::get_instance_generatePdf($_GET, $type);
    $response['pdf'] = $pdf->generate_Pdf();
    if ($response['pdf']){
        App::array_to_flash($response['pdf']);
        App::redirect("edit?user_token=" . $_GET['user_token']);
    }
}



