<?php

require('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
    $_PUT = App::getRequest();
    $validator = new Validator($_PUT);

    $validator->is_id('customer_address_id', 'L\'addresse ou le client est invalide', 'L\'addresse ou le client est introuvable');
    $validator->no_symbol('trash_localization', 'La localisation des poubelle est invalide');
    if ($validator->is_valid() === false)
        parseJson::error('Les données pour ajouter une loclisation de poubelle sont erronnées', $validator->getErrors())->printJson();
    $ret = App::getAddress()->editTrashLocalization($_PUT);
    if ($ret === false)
        parseJson::error('Une erreur est survenue lors de l\'ajout de la localisation du local poubelle')->printJson();
    parseJson::success('La localisation du local poubelle a bien été ajoutée')->printJson();
}
$json = new parseJson(403, 'Accès interdit', null);
$json->printJson();