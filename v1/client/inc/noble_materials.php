<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 08/08/2018
 * Time: 14:49
 */
require('../../../inc/bootstrap.php');

$errMessage = 'Impossible d\'afficher les matériaux nobles';
$errType = 'danger';

if (empty($_GET['address_token']))
    App::setHtmlAndExit($errMessage, $errType);
$address_token = $_GET['address_token'];
$user = App::getAddress()->getUserByAddressToken($address_token);
$address = App::getAddress()->getAddressByToken($address_token);
$noble_materials = App::getMaintenancePlanningType()->getNobleMaterialsByAddressId($address->id);
if ($noble_materials === false || $address === false || $user === false)
    App::setHtmlAndExit($errMessage, $errType);
$nm_list = App::getListManagement()->getList('noble_materials');
$room_list = App::getCardex()->getDistinctListElements('roomsList');
?>

<div class="row">
    <div class="clo-xs-12 text-center" id="nm-container">
        <label>Matériaux nobles</label>
        <button class="btn btn-success btn-xs" id="openNmModal">Ajouter</button>
        <?php foreach ($noble_materials as $nm): ?>
        <div class="col-xs-12 noble-materials-view" data-id="<?= $nm->id; ?>">
            <div class="col-xs-4">
                <p><?= $nm->nm_value; ?></p>
            </div>
            <div class="col-xs-4">
                <img class="nm-img" src="<?= $nm->lr_url; ?>">
                <p><?= $nm->lr_value; ?></p>
            </div>
            <div class="col-xs-4">
                <p class="noMarge"><?= $nm->comment; ?></p>
                <span class="mn-delete-icon glyphicon glyphicon-trash"></span>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- modal add noble material -->
<div id="modal_nm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                <h4 class="modal-title">Ajout d'un matériau noble</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select class="form-control" name="lr_room_id" id="nm_lr_room_id">
                        <option class="bg-grey" value="" disabled selected>Le lieu</option>
                        <?php foreach ($room_list as $room): ?>
                        <option value="<?= $room->id; ?>" data-url="<?= $room->url; ?>"><?= $room->value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="l_noble_materials_id" id="l_noble_materials_id">
                        <option class="bg-grey" value="" disabled selected>La matériau</option>
                        <?php foreach ($nm_list as $nm): ?>
                            <option value="<?= $nm->id; ?>"><?= $nm->field; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="description" placeholder="Commentaire (ex: Vinaigre aigre douce)" id="nm_comment">
                </div>
            </div>
            <div class="modal-footer">
                <a href="/v1/list/noble-material/edit" class="btn-link pull-left" target="_blank">Liste des matériaux</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-success" id="add-nm-btn">Ajouter</button>
            </div>
        </div>
    </div>
</div>
<!-- modal add noble material -->
<style>
    .bg-grey {
        background-color: rgba(1, 1, 1, 0.1);
    }
    .nm-img {
        height: 24px;
        width: 24px;
    }
    .noble-materials-view>div:last-child>p {
        text-overflow: ellipsis;
    }
    .noble-materials-view>div:first-child,
    .noble-materials-view>div:last-child {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .mn-delete-icon {
        cursor: pointer;
        position: absolute;
        top: 16px;
        right:0;
        color: red;
    }
    .noble-materials-view {
        padding-top: 3px;
        height: 50px;
        border: 1px solid rgba(1, 1, 1, 0.2);
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
    }
</style>
<script src="/js/maintenance.js"></script>
<script>
    $(document).ready(function(){
        let address_id = <?= $address->id; ?>;
        let nm_container = $('#nm-container');
        let modal_nm = $('#modal_nm');

        $('#openNmModal').on('click', function () {
            $('.bg-grey').prop('selected', 'true');
            $("input[name='comment']").val(null);
            modal_nm.modal();
        });

        $(document).on('click', '.mn-delete-icon', function () {
            if (confirm('Supprimer le matériau noble ?')) {
                let nm_view = $(this).closest('.noble-materials-view');
                let nm_delete = deleteNobleMaterial(nm_view.data('id'));

                nm_delete.done(function (res) {
                    if (res.code !== '200')
                        bo_notify(res.message, 'danger');
                    else {
                        nm_view.remove();
                    }
                })
            }
        });

        $('#add-nm-btn').on('click', function () {
            let data = {
                address_id : address_id,
                lr_room_id : $('#nm_lr_room_id').val(),
                l_noble_materials_id : $('#l_noble_materials_id').val(),
                comment : $('#nm_comment').val()
            };
            let pl_add = addNobleMaterial(data);

            pl_add.done(function (res) {
                if (res.code !== '200')
                    bo_notify(res.message, 'danger');
                else {
                    let nm_view = createNobleMaterial(res.data.mp_noble_materials_id);
                    nm_container.append(nm_view);
                }
                modal_nm.modal('hide');
            })
        });
    });
</script>
