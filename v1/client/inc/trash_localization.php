<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 08/08/2018
 * Time: 14:49
 */
require('../../../inc/bootstrap.php');

$errMessage = 'Impossible d\'afficher la localisation du local poubelle';
$errType = 'danger';
$address_instance = App::getAddress();
if (empty($_GET['address_token']))
    App::setHtmlAndExit($errMessage, $errType);
$address_token = $_GET['address_token'];
$address = $address_instance->getAddressByToken($address_token);
$customer = $address_instance->getCustomerByAddressId($address->id);
if ($address === false || $customer === false)
    App::setHtmlAndExit($errMessage, $errType);
if ($address->customer_address_id === null)
    $address->customer_address_id = $address_instance->insertEmptyCustomerAddress($address->id, $customer->id);
if ($address->customer_address_id === false)
    App::setHtmlAndExit($errMessage, $errType);
?>

<div class="row">
    <div class="clo-xs-12 text-center" id="nm-container">
        <label>Local poubelle</label>
        <button class="btn btn-success btn-xs" id="editTrashLocalization">Ajouter</button>
    </div>
    <div class="col-xs-12">
        <input type="text" class="form-control" placeholder="ex: dans la cave" name="trash_localization" id="trash_localization" value="<?= $address->trash_localization; ?>">
    </div>
</div>
<style>
</style>
<script src="/js/maintenance.js"></script>
<script>
    $(document).ready(function(){
        let customer_address_id = <?= $address->customer_address_id; ?>;

        $('#editTrashLocalization').on('click', function () {
            let data = {
                customer_address_id: customer_address_id,
                trash_localization: $('#trash_localization').val()
            };
            let ret = editTrashLocalization(data);

            ret.done(function (result) {
                if (result.code === '200')
                    bo_notify(result.message, 'success');
                else
                    bo_notify(result.message, 'danger');
            })
        })
    });

    function editTrashLocalization(data) {
        return ajax_call("/v1/client/api/trash_localization", "PUT", data, null, function () {
            ajax_log('api/trash_localization en PUT', '/v1/client/inc/trash_localization', 'editTrashLocalization')
        });
    }
</script>
