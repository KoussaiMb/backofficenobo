<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 08/08/2018
 * Time: 14:49
 */
require('../../../inc/bootstrap.php');

$errMessage = 'Impossible d\'afficher la localisation des produits';
$errType = 'danger';

if (empty($_GET['address_token']))
    App::setHtmlAndExit($errMessage, $errType);
$address_token = $_GET['address_token'];
$user = App::getAddress()->getUserByAddressToken($address_token);
$address = App::getAddress()->getAddressByToken($address_token);
$product_localization = App::getCardex()->getProductLocalizationInfoByAddressToken($address_token);
if ($product_localization === false || $address === false || $user === false)
    App::setHtmlAndExit($errMessage, $errType);
$product_list = App::getCardex()->getDistinctListElements('productsList');
$room_list = App::getCardex()->getDistinctListElements('roomsList');
?>

<div class="row">
    <div class="clo-xs-12 text-center" id="product-localization-container">
        <label>Localisation des produits</label>
        <button class="btn btn-success btn-xs" id="openPlModal">Ajouter</button>
        <?php foreach ($product_localization as $pl): ?>
        <div class="col-xs-12 product-localization-view" data-id="<?= $pl->id; ?>">
            <div class="col-xs-4">
                <img class="pl-img" src="<?= $pl->lp_url; ?>">
                <p><?= $pl->lp_value; ?></p>
            </div>
            <div class="col-xs-4">
                <img class="pl-img" src="<?= $pl->lr_url; ?>">
                <p><?= $pl->lr_value; ?></p>
            </div>
            <div class="col-xs-4">
                <p class="noMarge"><?= $pl->description; ?></p>
                <span class="pl-delete-icon glyphicon glyphicon-trash"></span>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<!-- modal add product localization -->
<div id="modal_pl" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                <h4 class="modal-title">Ajout d'une localisation de produit</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select class="form-control" name="lr_room_id" id="lr_room_id">
                        <option class="bg-grey" value="" disabled selected>Le lieu</option>
                        <?php foreach ($room_list as $room): ?>
                        <option value="<?= $room->id; ?>" data-url="<?= $room->url; ?>"><?= $room->value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="lr_product_id" id="lr_product_id">
                        <option class="bg-grey" value="" disabled selected>Le produit</option>
                        <?php foreach ($product_list as $product): ?>
                            <option value="<?= $product->id; ?>" data-url="<?= $product->url; ?>"><?= $product->value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="description" placeholder="Description (ex: sous l'évier)" id="pl_description">
                </div>
            </div>
            <div class="modal-footer">
                <a href="/v1/settings/edit?menu_id=5" class="btn-link pull-left" target="_blank">Liste des produits</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-success" id="add-pl-btn">Ajouter</button>
            </div>
        </div>
    </div>
</div>
<!-- modal add product localization -->
<style>
    .bg-grey {
        background-color: rgba(1, 1, 1, 0.1);
    }
    .pl-img {
        height: 24px;
        width: 24px;
    }
    .product-localization-view>div:last-child>p {
        text-overflow: ellipsis;
    }
    .product-localization-view>div:last-child {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .pl-delete-icon {
        cursor: pointer;
        position: absolute;
        top: 16px;
        right:0;
        color: red;
    }
    .product-localization-view {
        padding-top: 3px;
        height: 50px;
        border: 1px solid rgba(1, 1, 1, 0.2);
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-direction: row;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        -webkit-justify-content: space-between;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-align-content: stretch;
        -ms-flex-line-pack: stretch;
        align-content: stretch;
        -webkit-align-items: stretch;
        -ms-flex-align: stretch;
        align-items: stretch;
    }
</style>
<script src="/js/cardex.js"></script>
<script>
    $(document).ready(function(){
        let address_id = <?= $address->id; ?>;
        let pl_container = $('#product-localization-container');
        let modal_pl = $('#modal_pl');

        $('#openPlModal').on('click', function () {
            $('.bg-grey').prop('selected', 'true');
            $("input[name='description']").val(null);
            modal_pl.modal();
        });

        $(document).on('click', '.pl-delete-icon', function () {
            if (confirm('Supprimer la localisation du produit ?')) {
                let pl_view = $(this).closest('.product-localization-view');
                let pl_delete = deleteProductLocalization(pl_view.data('id'));

                pl_delete.done(function (res) {
                    if (res.code !== '200')
                        bo_notify(res.message, 'danger');
                    else {
                        pl_view.remove();
                    }
                })
            }
        });

        $('#add-pl-btn').on('click', function () {
            let data = {
                address_id : address_id,
                lr_room_id : $('#lr_room_id').val(),
                lr_product_id : $('#lr_product_id').val(),
                description : $('#pl_description').val()
            };
            let pl_add = addProductLocalization(data);

            pl_add.done(function (res) {
                if (res.code !== '200')
                    bo_notify(res.message, 'danger');
                else {
                    let pl_view = createProductLocalization(res.data.product_localization_id);
                    pl_container.append(pl_view);
                }
                modal_pl.modal('hide');
            })
        });
    });
</script>
