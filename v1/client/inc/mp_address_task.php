<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 16/08/2018
 * Time: 16:45
 */
require('../../../inc/bootstrap.php');

$errMessage = 'Impossible d\'afficher les tâches';
$errType = 'danger';

if (empty($_GET['mp_address_id']) || empty($_GET['mp_template_id']))
    App::setHtmlAndExit($errMessage, $errType);
$pe = App::getMaintenancePlanningType()->getMpUnitByTemplateId($_GET['mp_template_id']);
if ($pe === false)
    App::setHtmlAndExit($errMessage, $errType);
$rollingTasks = App::getListManagement()->getList('rollingTask');
?>

<div class="col-sm-12 text-center" style="margin-top: 10px">
    <button class="btn btn-primary btn-sm" type="button" id="editClientMp">Editer le planning d'entretien</button>
</div>
<!-- modal edit mp -->
<div id="modal_edit_mp" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">Fermer</span></button>
                <h4 class="modal-title">Edition du planning d'entretien</h4>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <?php foreach($pe as $unit): ?>
                        <div class="col-xs-3 pe-client-view text-center" data-id="<?= $unit->id; ?>">
                            <div>
                                <h4><?= $unit->maintenanceType; ?></h4>
                            </div>
                            <div>
                                <span class='glyphicon glyphicon-minus'></span>
                            </div>
                            <div class="client-task-container text-left">
                                <?php $unit_task_name = explode(',', $unit->rolling_task_names); ?>
                                <?php foreach ($unit_task_name as $task_name): ?>
                                    <div class="mp-task">
                                        <p><?= $task_name; ?></p>
                                    </div>
                                <?php endforeach; ?>
                                <?php $client_task = App::getMaintenancePlanningType()->getClientOwnTask($_GET['mp_address_id'], $unit->id); ?>
                                <?php foreach ($client_task as $task): ?>
                                    <div class="mp-task-client" data-id="<?= $task->id; ?>">
                                        <p><?= $task->name; ?><span class="btn-link pull-right delete-task-client">Supprimer</span></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="input-group">
                                <select class="form-control input-xs" name="rollingTask">
                                    <?php foreach ($rollingTasks as $task): ?>
                                        <option value="<?= $task->id; ?>"><?= $task->field; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-xs btn-success" name="add-client-task">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
<!-- modal edit mp -->
<style>
    .pe-client-view {
        border-radius: 3px;
        background-color: rgba(0, 0, 0, 0.05);
        padding: 5px;
        border: 2px solid white;
    }
    .client-task-view>div>span {
        position: relative;
        right: 0;
    }
    .client-task-view .hidden-task {
        color: lightgray;
    }
    .btn-link {
        cursor: pointer;
    }
</style>
<script>
    jQuery(document).ready(function() {
        let modal_edit_mp = $('#modal_edit_mp');
        let mp_address_id = <?= $_GET['mp_address_id']; ?>;

        $('#editClientMp').on('click', function () {
            modal_edit_mp.modal('show');
        });

        $('button[name="add-client-task"]').on('click', function () {
            let select_rt = $(this).closest('.input-group').find('select option:selected');
            let add_location = $(this).closest('.pe-client-view').find('.client-task-container');
            let data = {
                mp_address_id: mp_address_id,
                l_rollingTask_id: select_rt.val(),
                mp_unit_id: $(this).closest('.pe-client-view').data('id')
            };
            let ret = addAddressRollingTask(data);

            ret.done(function (res) {
                if (res.code === '200') {
                    let new_client_task = createRollingTaskClient(
                        res.data.mp_address_task_id,
                        select_rt.text()
                    );

                    add_location.append(new_client_task);
                }
                else {
                    bo_notify(res.message, 'danger');
                    modal_edit_mp.modal('hide');
                }
            });
        });

        modal_edit_mp.on('click', '.delete-task-client', function () {
            let task_to_delete = $(this).closest('.mp-task-client');
            let ret = deleteAddressRollingTask({mp_address_task_id: task_to_delete.data('id')});

            ret.done(function (res) {
                if (res.code === '200')
                    task_to_delete.remove();
                else {
                    bo_notify(res.message, 'danger');
                    modal_edit_mp.modal('hide');
                }
            });
        });
    });
</script>