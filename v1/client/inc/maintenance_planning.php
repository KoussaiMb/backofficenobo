<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 08/08/2018
 * Time: 14:49
 */
require('../../../inc/bootstrap.php');

$errMessage = 'Impossible d\'afficher les plannings d\'entretien';
$errType = 'danger';
$mp_instance = App::getMaintenancePlanningType();

if (empty($_GET['address_token']))
    App::setHtmlAndExit($errMessage, $errType);
$address_token = $_GET['address_token'];
$user = App::getAddress()->getUserByAddressToken($address_token);
$address = App::getAddress()->getAddressByToken($address_token);
$pe_list = $mp_instance->getAllComparedByAddressToken($address_token);
if ($pe_list === false || $address === false || $user === false)
    App::setHtmlAndExit($errMessage, $errType);
$mp_address = $mp_instance->getMpAddressByAddressId($address->id);
if ($mp_address === false)
    App::setHtmlAndExit($errMessage, $errType);
$pe = [];
if (isset($mp_address->mp_template_id))
    $pe = $mp_instance->getMpUnitByTemplateId($mp_address->mp_template_id);
if ($pe === false)
    App::setHtmlAndExit($errMessage, $errType);
$rollingTasks = App::getListManagement()->getList('rollingTask');
?>
<input type="hidden" name="address_id" value="<?= $address->id ?>" id="address_mp_id">
<input type="hidden" name="user_token" value="<?= $user->user_token; ?>" id="user_token">
<input type="hidden" name="mp_address_id" value="<?= empty($mp_address) ? null : $mp_address->mp_address_id; ?>">
<div class="col-xs-12 text-center">
    <input type="hidden" name="mp_address_id" id="mp_address_id">
    <label for="mp_template_id">Liste des PE</label>
    <div class="input-group col-md-12">
        <div class="input-group">
              <select required class="form-control input-sm" id="mp_template_id" name="mp_template_id">
                <option>Selectionner</option>
                <?php foreach ($pe_list as $key => $value): ?>
                    <option
                            class="alert alert-<?= $value->matching == 1 ? 'success' : 'danger'; ?>"
                            value="<?= $value->id ?>"
                        <?= $mp_address !== null && $value->id == $mp_address->mp_template_id ? 'selected' : ''; ?>
                    >
                        <?= $value->name; ?>
                        <?= $value->matching == 1 ?
                            '<span class="match">(match)</span>':
                            '<span class="noMatch">(no match '.$value->matching_percentage.'%)</span>';
                        ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="input-group-btn">
                <button type="submit" class="btn btn-success btn-sm" name="action" value="<?= $mp_address === null ? 'addMpAddress' : 'editMpAddress'; ?>" id="mp_client_action">Valider</button>
            </div>
        </div>
        <textarea style="resize: none;" class="form-control" rows="1" id="mp_comment" name="comment" placeholder="Commentaire"><?= empty($mp_address) ? null : $mp_address->comment; ?></textarea><br>
    </div>
</div>
<div id="edit-client-own-task"></div>
<script src="/js/maintenance.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
<script>
    jQuery(document).ready(function() {
        let address_id = <?= $address->id; ?>;
        let mp_address_id = <?= empty($mp_address) ? 'undefined' : $mp_address->mp_address_id; ?>;
        let mp_template_id = <?= empty($mp_address) ? 'undefined' : $mp_address->mp_template_id; ?>;
        let ajax_actions = {
            addMpAddress: function (e) {
                return addMpAddress(e)
            },
            editMpAddress: function (e) {
                return editMpAddress(e)
            }
        };

        if (mp_address_id !== undefined && mp_template_id !== undefined)
            generateClientMp();

        $('#mp_client_action').on('click', function () {
            let ret;
            let valid = true;
            let data = {
                address_id : address_id,
                mp_template_id : $('#mp_template_id').val(),
                comment: $('#mp_comment').val(),
                mp_address_id: mp_address_id
            };

            if (mp_template_id !== undefined && data.mp_template_id !== mp_template_id)
                valid = confirm('Voulez-vous vraiment changer le planninge d\'entretien ?');
            if (valid === true) {
                let action = $(this).val();

                ret = ajax_actions[action](data);
                ret.done(function (res) {
                    if (res.code !== '200')
                        bo_notify(res.message, 'danger');
                    else {
                        bo_notify(res.message, 'success');
                        mp_template_id = $('#mp_template_id').val();
                        mp_address_id = res.data.mp_address_id;
                        generateClientMp();
                    }
                })
            }
        });

        function generateClientMp() {
            let dataString = {mp_address_id: mp_address_id, mp_template_id: mp_template_id};
            console.log(dataString);

            ajax_call_html('/v1/client/inc/mp_address_task.php', 'GET', dataString,
                function(r) {$('#edit-client-own-task').html(r);},
                function () {bo_notify('Le planing d\'entretien a un problème, prévenir un admin')}
            );
        }
    });
</script>