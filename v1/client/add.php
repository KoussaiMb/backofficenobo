<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('add-client');
require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Création d'un nouveau client
        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <div class="alert alert-info col-sm-12" role="alert">
                    <p><strong>Tous</strong> les champs sont requis</p>
                </div>
            </div>
            <div class="col-sm-6">
                <form method="post" action="form">
                    <div class="input-group">
                        <div class="input-group-addon"><span>Genre</span></div>
                        <select class="form-control" name="gender_id">
                            <?php $list = App::getListManagement()->getList('gender'); ?>
                            <?php foreach ($list as $k => $v): ?>
                                <option value="<?= $v->id;?>" <?= isset($data['gender_id']) && $data['gender_id'] == $v->id ? "selected=1" : ""; ?>><?= $v->field; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                        <input type="text" class="form-control" name="email" value="<?= htmlspecialchars(isset($data['email']) ? $data['email'] : null); ?>" placeholder="email"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" name="firstname" value="<?=  htmlspecialchars(isset($data['firstname']) ? $data['firstname'] : null); ?>" placeholder="Prénom"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" name="lastname" value="<?=  htmlspecialchars(isset($data['lastname']) ? $data['lastname'] : null); ?>" placeholder="Nom"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                        <input type="text" class="form-control" name="phone" value="<?=  htmlspecialchars(isset($data['phone']) ? $data['phone'] : null); ?>" placeholder="Téléphone"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                        <input type="text" class="form-control" name="password" placeholder="Mot de passe" id="password" value="<?=  htmlspecialchars(isset($data['password']) ? $data['password'] : null);?>"/>
                        <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="$('#password').val(rand());">Random</button></span>
                    </div>
                    <div class="input-group" style="margin: 5px 0">
                        <label for="waiting-list">Ajouter à la liste d'attente</label>
                        <input type="hidden" name="waiting_list" value="0"/>
                        <input type="checkbox" name="waiting_list" value="1" <?= isset($data['waiting_list']) && $data['waiting_list'] == 1 ? 'checked' : ''; ?>/>
                    </div>
                    <button type="submit" class="btn btn-success col-sm-6 col-sm-offset-3" name="action" value="add">Créer un nouveau client</button>
                </form>
            </div>
            <div class="col-sm-3">
                <?php require('../../inc/print_flash_create.php'); ?>
            </div>
        </div>
    </div>
<?php
require('../../inc/footer_bo.php');
