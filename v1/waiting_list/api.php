<?php
require_once ('../../inc/bootstrap.php');
$db = App::getDB();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $validator = new Validator($_POST);
    $validator->isToken('address_token', 'Le token est invalide', 'Le token est manquant');
    if (!$validator->is_valid()) {
        $json = parseJson::error($validator->getErrorString('address_token'));
        $json->printJson();
    }
    $address = App::getUser()->getAddressNotInWaitingListByAddressToken($_POST['address_token']);
     if (!$address)
        $json = parseJson::error('L\'adresse est déjà dans la liste d\'attente');
    else
        $json = parseJson::success(null, App::getUser()->addWaitingList($address->id));
    $json->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    parse_str(file_get_contents('php://input'), $delete);
    $validator = new Validator($delete);
    $validator->is_id('id', 'Les informations sont invalides', 'Les informations sont manquantes');
    if (!$validator->is_valid())
        $json = parseJson::error($validator->getErrorString('id'));
    else
        $json = parseJson::success(null, App::getUser()->deleteWaitingListById($delete['id']));
    $json->printJson();
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    parse_str(file_get_contents('php://input'), $put);
    $validator = new Validator($put);
    $validator->is_id('id', 'Les informations sont invalides', 'Les informations sont manquantes');
    if (!$validator->is_valid())
        $json = parseJson::error($validator->getErrorString('id'));
    else
        $json = parseJson::success(null, App::getUser()->acceptWaitingListById($put['id']));
    $json->printJson();
}