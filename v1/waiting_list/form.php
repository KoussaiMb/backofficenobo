<?php
require('../../inc/bootstrap.php');

if(empty($_POST) || !isset($_POST['action']))
    App::redirect('view');

if($_POST['action'] == 'delete'){
    $validator = new Validator($_POST);
    $validator->is_id('waiting_id', "L'utilisateur sélectionné est invalide","Utilisateur inconnu");
    $validator->isToken('user_token', "L'utilisateur sélectionné est invalide","Utilisateur inconnu");
    if(!$validator->is_valid()){
        App::array_to_flash($validator->getErrors());
        App::redirect('view');
    }
    $response = App::getUser()->deleteUserFromWaitlist($_POST['waiting_id']);
    if (!$response)
        App::setFlashAndRedirect('danger', 'Le client n\'a pu être supprimé de la liste d\'attente', 'view');
    $ret = App::getUser()->validateUserByToken($_POST['user_token']);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Le client n\'a pu être mise à jour', 'view');
    App::setFlashAndRedirect('success', 'Le client a bien été supprimé de la liste d\'attente', 'view');
}
App::redirect('view');