<?php
require('../../inc/bootstrap.php');

$userClass = App::getUser();
$waiters = $userClass->getWaitingList();

if ($waiters == null)
    Session::getInstance()->setFlash('danger', 'Aucune personne présente dans la liste d\'attente');

$allClient = App::getCustomer()->getAutocompleteName();
$allAddress = App::getAddress()->getAutocompAddresses();

if (!$allClient || !$allAddress)
    Session::getInstance()->setFlash('danger', "impossible de charger les clients existants");

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading">
        Liste d'attente - (<span id="nb-person"><?= count($waiters); ?></span> personnes)
    </div>
    <div class="panel-body">
        <div class="col-xs-12">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="col-xs-12 col-sm-8 table-view">
                <table id="table_liste_attente" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Adresse</th>
                        <th>téléphone</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php foreach ($waiters as $waiter):?>
                        <tr>
                            <input type="hidden" name="waiting_list_id" value="<?=$waiter->id?>">
                            <input type="hidden" name="address_id" value="<?=$waiter->address_id?>">
                            <td><?= $waiter->lastname; ?> <?= $waiter->firstname; ?></td>
                            <td><?= $waiter->address; ?> (<?= $waiter->zipcode; ?>)</td>
                            <td><?= $waiter->phone; ?></td>
                            <td><?= date('d/m/Y H:i', strtotime($waiter->date_list_in)); ?></td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/v1/client/edit?user_token=<?= $waiter->user_token; ?>">Fiche client</a></li>
                                        <li><a class="validButton">Valider</a></li>
                                        <li><a class="deleteButton">Supprimer</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="col-xs-12 col-sm-4 text-center">
                <div class="col-xs-12">
                    <?php if ($allClient && $allAddress): ?>
                    <div class="form-group">
                        <label for="client">Client</label>
                        <input type="text" class="form-control" id="client">
                    </div>
                    <div class="form-group">
                        <label for="address">Adresse</label>
                        <select name="address_token" class="form-control" id="address" required>
                        </select>
                    </div>
                    <div class="form-group col-xs-12" style="margin-top: 2em;">
                        <button class="btn btn-success" id="addWaitingList">Ajouter un utilisateur en liste d'attente</button>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .validButton, .deleteButton {
        cursor: pointer;
    }
    .deleteButton {
        color: red!important;
    }
    .validButton {
        color: green!important;
    }
</style>
<script src="/js/app.js"></script>
<script>
    var address = $('#address');
    var allClient = <?php if (isset($allClient)){echo $allClient;} else {echo "[]";}?>;
    var allAddress = <?php if (isset($allAddress)){echo json_encode($allAddress);} else {echo "[]";}?>;

    function filterAddress(user_token) {
        address.find('option').remove().end();
        $.each(allAddress, function (i, item) {
            if (user_token == item.user_token) {
                address.append($('<option>', {
                    value: item.address_token,
                    text: item.address
                }));
            }
        });
    }

    function constructWaitingListRow(waiter) {
        var input = "";
        input += "<tr>";
        input += "<input type='hidden' name='waiting_list_id' value='" +waiter.waiting_list_id+ "'>";
        input += "<input type='hidden' name='address_id' value='" +waiter.address_id+ "'>";
        input += "<td>" +waiter.lastname+ " " +waiter.firstname+ "</td>";
        input += "<td>" +waiter.address+ " (" +waiter.zipcode+ ")</td>";
        input += "<td>" +waiter.phone+ "</td>";
        input += "<td>" +mysqlDatetime_to_date(waiter.date_list_in)+ "</td>";
        input += "<td class='text-right'>";
        input += "<div class='btn-group'>";
        input += "<button type='button' class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        input += "<span class='caret'></span>";
        input += "</button>";
        input += "<ul class='dropdown-menu'>";
        input += "<li><a href='/v1/client/edit?user_token=" +waiter.user_token+ "'>Fiche client</a></li>";
        input += "<li><a class='validButton'>Valider</a></li>";
        input += "<li><a class='deleteButton'>Supprimer</a></li>";
        input += "</ul>";
        input += "</div>";
        input += "</td>";
        input += "</tr>";
        return input;
    }

    function addWaitingList(address_token, table, person) {
        $.ajax({
            url: 'api',
            data: 'address_token=' + address_token,
            dataType: 'json',
            type: "POST",
            success: function (res) {
                if (res.code == 200) {
                    table.append(constructWaitingListRow(res.data));
                    person.text(parseInt(person.text()) + 1);
                    bo_notify('L\'adresse a bien été ajoutée à la liste d\'attente', 'success');
                } else {
                    bo_notify(res['message'], 'danger');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                bo_notify('Erreur inconnue, contacter un admin', 'danger');
            }
        })
    }

    function deleteWaitingList(id, div, person) {
        $.ajax({
            url: 'api',
            data: 'id=' + id,
            dataType: 'json',
            type: "DELETE",
            success: function (res) {
                if (res['code'] == 200) {
                    div.remove();
                    person.text(parseInt(person.text()) - 1);
                    bo_notify("L'adresse a été supprimée", 'success');
                } else {
                    bo_notify('La suppression a échoué', 'danger');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                bo_notify('Erreur inconnue, contacter un admin', 'danger');
            }
        })
    }

    function acceptWaitingList(id, div, person) {
        $.ajax({
            url: 'api',
            data: 'id=' + id,
            dataType: 'json',
            type: "PUT",
            success: function (res) {
                if (res['code'] == 200) {
                    div.remove();
                    person.text(parseInt(person.text()) - 1);
                    bo_notify("L'adresse a été validée", 'success');
                } else {
                    bo_notify("La validation a échoué", 'danger');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                bo_notify('Erreur inconnue, contacter un admin', 'danger');
            }
        })
    }

    jQuery(document).ready(function() {
        var table = $('#table_liste_attente');
        var person = $('#nb-person');

        if (allClient && allAddress) {
            $('#client').devbridgeAutocomplete({
                lookup: allClient,
                maxHeight: 200,
                onSelect: function (suggestion) {
                    filterAddress(suggestion.data);
                }
            });
        }
        $('#addWaitingList').on('click', function () {
            if (address.length)
                addWaitingList(address.val(), table, person);
        });
        table.on('click', '.validButton', function () {
            var div = $(this).closest('tr');
            var id = div.find("input[name='waiting_list_id']").val();
            acceptWaitingList(id, div, person);
        });
        table.on('click', '.deleteButton', function () {
            var div = $(this).closest('tr');
            var id = div.find("input[name='waiting_list_id']").val();
            deleteWaitingList(id, div, person);
        });
        table.css('max-height', $(window).height() * 0.7);
    });
</script>
<style>
    .add-user-waiting-list button{
        margin-top: -2px;
    }
    .add-user-waiting-list {
        padding: 5px 5px 5px 5px;
        border-radius: 8px;
    }
</style>
<?php
require('../../inc/footer_bo.php');
