<?php
require('../../inc/bootstrap.php');


$items = App::getItem()->getItems();
$categories = App::getItem()->getCategories();

if (!$items){
    Session::getInstance()->setFlash('info', 'Aucun item trouvé');
}

if (!$categories){
    Session::getInstance()->setFlash('info', 'Aucune catégorie trouvée');
}

require('../../inc/header_bo.php');
?>

    <div class="panel panel-success">
        <div class="panel-heading">
            Gestion des items
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-sm-3">
                <label for="itemCategory">Catégorie</label>
                <select name="itemCategory" class="form-control" id="itemCategory">
                    <option value="0">Toutes</option>
                    <?php foreach ($categories as $k => $v): ?>
                        <option value="<?= $v->id; ?>"><?= $v->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-5 text-center" style="padding-top: 22px">
                <button type="button" class="btn btn-info" name="action" value="add" id="addCategoryButton">Ajouter une catégorie</button>
                <a href="add"><button type="button" class="btn btn-success" name="action" value="add">Ajouter un item</button></a>
            </div>
            <div class="col-xs-12 view-container">
                <?php foreach ($items as $k => $v): ?>
                    <div class="col-xs-4 item-view">
                        <div class="col-xs-12">
                            <input type="hidden" name="item_id" value="<?= $v->id;?>">
                            <input type="hidden" name="category_id" value="<?= $v->item_category_id; ?>">
                            <div class="col-xs-12">
                                <p style="color: #204d74;"><?= 'Nom : ' . $v->name ?></p>
                                <p style="color: #204d74;"><?= 'Prix : ' . $v->price?></p>
                                <?php if($v->description){ ?>
                                    <p style="color: #204d74;"><?= 'Description : ' . $v->description?></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <style>
        .view-container {
            overflow-y: scroll;
            margin-top: 2em;
            border: 2px solid #DBEED4;
            border-radius: 3px;
        }
        .item-view {
            padding: 20px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .item-view > div:hover {
            border: 1px solid grey;

        }
        .item-view > div {
            background-color: #c2c2d6;
            border-radius: 3px;
            padding-top: 10px;
            height: 110px;
            cursor: pointer;
        }
        .item-view > div div:first-child {
            color: ;
        }
        .item-view > div div:last-child {
            color: #e60073;
        }
    </style>

    <script src="/js/item.js"></script>
    <script>

        /* Displaying only the items of the selected category */
        $(document).ready(function() {

            var allItems = $('.item-view > div');
            var allDiv = $('.item-view');

            $(".view-container").css('max-height', $(window).height() * 0.7);

            allItems.on('click', function () {
                window.location = 'edit?id=' + $(this).find("input[name='item_id']").val();
            });

            $('#itemCategory').on('change', function () {
                itemId = $(this).val();
                if (itemId == 0){
                    allDiv.show();
                }
                else {
                    allDiv.hide();
                    $("input[name='category_id'][value='"+ itemId +"']").parentsUntil("div[class='item-view']").show();
                }
            });
        });
    </script>
<?php
require('../../inc/footer_bo.php');