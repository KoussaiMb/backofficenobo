<?php
require('../../inc/bootstrap.php');

if (empty($_POST['action']))
    App::redirect('view');
$db = App::getDB();

if ($_POST['action'] == 'edit') {

    $validator = new Validator($_POST);

    $validator->is_id('itemCategories', "La catégorie est invalide", "Veuillez entrer une catégorie");
    $validator->no_symbol('name', 'Le nom entré est invalide', 'Veuillez entrer un nom');
    $validator->no_symbol('description', 'La description entrée est invalide', 'Veuillez entrer une description');
    $validator->is_decimal('price', 'Le prix entré est invalide', 'Veuillez renseigner un prix');
    $validator->is_decimal('vat', 'La TVA entrée est invalide', 'Veuillez renseigner la TVA');
    $validator->is_alphanum('ref', 'La référence entrée est invalide', 'Veuillez renseigner la référence');
    if ($validator->is_valid())
        $validator->is_uniqButHimself($db, 'item', 'reference_name', 'ref', $_POST['item_id'],'La référence renseignée n\'est pas unique');
    $validator->is_num('stock', 'Le stock entré est invalide');
    $validator->is_num('stock_alert', 'L\'alerte pour le stock est invalide');
    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), 'edit?id='.$_POST['item_id']);

    $ret = App::getItem()->editItem($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'L\'item n\'a pas été modifié', 'edit?id='.$_POST['item_id']);
    App::setFlashAndRedirect('success', "L'item a été modifié", "view");
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);

    $validator->is_id('item_id', "L'id n'est pas valide", "L'id n'est pas valide");
    if (!$validator->is_valid())
        App::array_to_flash($validator->getErrors(), 'edit?id='.$_POST['item_id']);
    $ret = App::getItem()->deleteItem($_POST['item_id']);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la suppression de l\'item', "edit?id=" . $_POST['item_id']);
    App::setFlashAndRedirect('success', 'L\'item a été supprimé avec succès', 'view');
}
else if($_POST['action'] == 'add') {

    $validator = new Validator($_POST);

    $validator->is_id('itemCategories', "La catégorie est invalide", "Veuillez entrer une catégorie");
    $validator->no_symbol('name', 'Le nom entré est invalide', 'Veuillez entrer un nom');
    $validator->no_symbol('description', 'La description entrée est invalide', 'Veuillez entrer une description');
    $validator->is_decimal('price', 'Le prix entré est invalide', 'Veuillez renseigner un prix');
    $validator->is_decimal('vat', 'La TVA entrée est invalide', 'Veuillez renseigner la TVA');
    $validator->is_alphanum('ref', 'La référence entrée est invalide', 'Veuillez renseigner la référence');
    if ($validator->is_valid())
        $validator->is_uniq($db, 'item', 'reference_name', 'ref', 'La référence renseignée n\'est pas unique');
    $validator->is_num('stock', 'Le stock entré est invalide');
    $validator->is_num('stock_alert', 'L\'alerte pour le stock est invalide');

    if (!$validator->is_valid()) {
        Session::getInstance()->write('item', $validator->getData());
        App::array_to_flash($validator->getErrors(), 'add');
    }

    $ret = App::getItem()->insertItem($_POST);
    if ($ret === false)
        App::setFlashAndRedirect('danger', 'Le nouvel item n\'a pas été ajouté', 'add');
    Session::getInstance()->delete('item');
    App::setFlashAndRedirect('success', "Le nouvel item a été ajouté", "view");
}
App::redirect('view');