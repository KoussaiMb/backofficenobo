<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('item');

require('../../inc/header_bo.php');
?>

    <div class="panel panel-success">
        <div class="panel-heading">
            Création d'un nouvel item
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-sm-6">
                <form method="post" action="form">
                    <div class="form-group">
                        <div class="input-group">
                            <?php $categories = App::getItem()->getCategories(); ?>
                            <label for="itemCategory">Catégorie</label>
                            <select class="form-control input-sm" name="itemCategories"  id="itemCategory">
                                <?php foreach ($categories as $k => $v): ?>
                                    <option value="<?= $v->id; ?>" <?=  isset($data) && $data['itemCategories'] == $v->id ?  : 'selected'; ?>><?= $v->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-6" style="padding-left: 0">
                        <label for="name">Nom</label>
                        <input type="text" class="form-control input-sm" name="name" placeholder="Un nom" value="<?= isset($data) ? $data['name'] : null; ?>" id="name" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="ref">Référence</label>
                        <input type="text" class="form-control input-sm" name="ref" value="<?= isset($data) ? $data['ref'] : null; ?>" placeholder="10ADVBGFV" id="ref">
                    </div>
                    <i>Attention, un nom de référence commençant par "inde" correspond à un autoentrepreneur.</i><br><br>
                    <div class="form-group" style="padding-right: 0">
                        <label for="description">Description</label>
                        <input type="text" class="form-control input-sm" name="description" placeholder="Une description" value="<?=  isset($data) ? $data['description'] : null; ?>" id="description" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-left: 0">
                        <label for="price">Prix TTC</label>
                        <input type="text" class="form-control input-sm" name="price" value="<?=  isset($data) ? $data['price'] : null; ?>" placeholder="28" id="price" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-right: 0">
                        <label for="vat">TVA</label>
                        <input type="text" class="form-control input-sm" name="vat" value="<?= isset($data) ? $data['vat'] : null; ?>" placeholder="10" id="vat" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-left: 0">
                        <label for="stock">Stock</label>
                        <input type="text" class="form-control input-sm" name="stock" value="<?= isset($data) ? $data['stock'] : null; ?>" placeholder="10" id="stock">
                    </div>
                    <div class="form-group col-sm-6" style="padding-right: 0">
                        <label for="stock_alert">Stock limite avant alerte</label>
                        <input type="text" class="form-control input-sm" name="stock_alert" value="<?= isset($data) ? $data['stock_alert'] : null; ?>" placeholder="2" id="stock_alert">
                    </div>
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-success" name="action" value="add">Ajouter l'item</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function() {
            $('button[name=show]').on('click', function () {
                $('.showField').addClass('hidden');
                $('#' + $(this).val()).closest('div').removeClass('hidden');
            })
        })
    </script>
<?php
require('../../inc/footer_bo.php');