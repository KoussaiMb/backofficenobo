<?php
require_once ('../../../inc/bootstrap.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $validator = new Validator($_POST);

    if($_POST['parent_id'] == -1){
        $_POST['parent_id'] = null;
    }
    $validator->no_symbol('name', 'Le nom est invalide', 'Le nom n\'a pas été renseigné');

    if(!$validator->is_valid()){
        $json = parseJson::error('Insertion de la nouvelle catégorie impossible');
        $json->printJson();
    }

    $ret = App::getItem()->insertCategory($_POST['name'], $_POST['parent_id']);
    if(!$ret){
        $json = parseJson::error('Impossible d\'insérer la catégorie');
        $json->printJson();
    }

    $json = parseJson::success('La nouvelle catégorie a été insérée avec succès');
    $json->printJson();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $categories = App::getItem()->getCategories();
    if(!$categories){
        $json = parseJson::error('Impossible de récupérer les catégories');
        $json->printJson();
    }

    $json = parseJson::success(null, $categories);
    $json->printJson();

}
?>