<?php
require('../../inc/bootstrap.php');

if (empty($_GET['id'])){
    App::setFlashAndRedirect('warning', "Choisissez un item valide", "view");
}

$item = App::getItem()->getItemById($_GET['id']);

if (empty($item)){
    App::setFlashAndRedirect('info', 'Impossible de récupérer l\'item demandé', 'view');
}

require('../../inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Edition d'un item
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php include('../../inc/print_flash_helper.php'); ?>
            </div>
            <div class="col-sm-6">
                <form method="post" action="form">
                    <input class="hidden" name="item_id" value="<?= $item->id; ?>" id="promoId">
                    <div class="form-group">
                        <div class="input-group">
                            <?php $categories = App::getItem()->getCategories(); ?>
                            <select class="form-control input-sm" name="itemCategories"  id="itemCategory">
                                <?php foreach ($categories as $k => $v): ?>
                                    <option value="<?= $v->id; ?>" <?=  $item->item_category_id == $v->id ? 'selected' : null; ?>><?= $v->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-6" style="padding-left: 0">
                        <label for="name">Nom</label>
                        <input type="text" class="form-control input-sm" name="name" placeholder="Un nom" value="<?= isset($item->name) ? $item->name : null; ?>" id="name" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-right: 0">
                        <label for="description">Description</label>
                        <input type="text" class="form-control input-sm" name="description" placeholder="Une description" value="<?= isset($item->description) ? $item->description : null; ?>" id="description" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-left: 0">
                        <label for="price">Prix TTC</label>
                        <input type="text" class="form-control input-sm" name="price" value="<?=  isset($item->price) ? $item->price : null; ?>" placeholder="28" id="price" required>
                    </div>
                    <div class="form-group col-sm-6" style="padding-right: 0">
                        <label for="vat">TVA</label>
                        <input type="text" class="form-control input-sm" name="vat" value="<?= isset($item->vat) ? $item->vat : null; ?>" placeholder="10" id="vat" required>
                    </div>
                    <div class="form-group">
                        <label for="ref">Référence</label>
                        <input type="text" class="form-control input-sm" name="ref" value="<?= isset($item->reference_name) ? $item->reference_name : null; ?>" placeholder="10ADVBGFV" id="ref">
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="text" class="form-control input-sm" name="stock" value="<?= isset($item->stock) ? $item->stock : null; ?>" placeholder="10" id="stock">
                    </div>
                    <div class="form-group">
                        <label for="stock_alert">Stock limite avant alerte</label>
                        <input type="text" class="form-control input-sm" name="stock_alert" value="<?= isset($item->stock_alert) ? $item->stock_alert : null; ?>" placeholder="2" id="stock_alert">
                    </div>
                    <div class="SpaceTop" style="text-align: center;">
                        <button type="submit" class="btn btn-success" name="action" value="edit">Sauvegarder les modifications</button>
                        <button type="submit" class="btn btn-danger" name="action" value="delete">Supprimer cet item</button>
                        <a href="view"><button type="button" class="btn btn-warning" name="action" value="view">Revernir aux items</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
require('../../inc/footer_bo.php');