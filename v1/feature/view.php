<?php
require('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/');

$features = App::getManageGroup()->getFeatures();

if (empty($features))
    Session::getInstance()->setFlash('danger', 'Aucune feature trouvée');

require('../../inc/header_bo.php');
?>

    <div class="panel panel-danger">
        <div class="panel-heading panel-heading-xs">
            Gestion des features
        </div>
        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
                <h3>Les features<a href="add"><button class="btn btn-success btn-xs pull-right">ajouter</button></a></h3>
                <?php if (!empty($features)): ?>
                <?php foreach($features as $k => $v): ?>
                <div class="col-xs-12 feature-row">
                    <form method="post" action="form">
                        <input type="hidden" name="id" value="<?= $v->id; ?>">
                        <div><?= $v->name; ?>
                            <span class="feature-description"> - <?= $v->description ?></span>
                            <span class="feature-url">[<?= $v->url ?>]</span>
                            <button type="submit" name="action" value="delete" onclick="return confirm('Supprimer la feature ?')" class="pull-right glyphicon glyphicon-remove"></button>
                        </div>
                    </form>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<script>
    jQuery(document).ready(function() {
        $('.feature-row span.glyphicon-remove').on('click', function () {
            bootbox.confirm(
                "This is the default confirm!",
                function(result){
                    console.log('This was logged in the callback: ' + result);
                });
        })
    })
</script>
<?php include_once ('../../inc/footer_bo.php'); ?>