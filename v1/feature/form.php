<?php
require('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/');
if (!isset($_POST['action']))
    App::redirect('view');

if ($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    $validator->no_symbol('name', 'Le nom de la feature semble invalide', 'Il manque le nom de la feature');
    $validator->no_symbol('description', 'La description semble invalide', 'Il manque une description');
    $validator->no_symbol('url', 'Le dossier est invalide', 'Il manque le dossier');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('add');
    }
    $_POST['created_by'] = App::getAuth()->user()->id;
    $ret = App::getManageGroup()->addFeature($_POST);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la création de la feature', 'add');
    App::setFlashAndRedirect('success', 'La feature a bien été créée', 'add');
}
else if ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'La feature semble invalide', 'Selectionnez une feature à supprimer');
    if (!$validator->is_valid()) {
        App::array_to_flash($validator->getErrors());
        App::redirect('view');
    }
    $ret = App::getManageGroup()->deleteFeature($_POST['id']);
    if (!$ret)
        App::setFlashAndRedirect('danger', 'Un problème est survenu lors de la suppression de la feature', 'view');
    App::setFlashAndRedirect('success', 'La feature a bien été supprimée', 'view');
}
App::redirect('view');