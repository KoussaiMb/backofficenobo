<?php
require('../../inc/bootstrap.php');

if (App::getAuth()->who() != 'admin')
    App::redirect('/');

$dirWithoutFeature = App::getManageGroup()->getDirWithoutFeature();

require('../../inc/header_bo.php');
?>

<div class="panel panel-danger">
    <div class="panel-heading">
        <a href="view" style="color: white">Gérer les features</a> / Ajouter une feature
    </div>
    <div class="panel-body">
        <?php include('../../inc/print_flash_helper.php'); ?>
        <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
            <form method="post" action="form">
                <div class="form-group">
                    <label for="url">Dossier</label>
                    <select class="form-control" name="url" id="url">
                        <option>Sélectionnez un dossier</option>
                        <?php foreach ($dirWithoutFeature as $v): ?>
                            <option value="<?= $v; ?>"><?= $v; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Nom de la feature</label>
                    <input type="text" maxlength="30" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" maxlength="60" name="description" class="form-control" id="description" required>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success" name="action" value="add">Ajouter la feature</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        $('#url').on('change', function () {
            $('#name').prop('disabled', true);
            $('#description').prop('disabled', true);
            $('form button').prop('disabled', true);
        }).change();
    }
</script>
<?php include_once ('../../inc/footer_bo.php'); ?>