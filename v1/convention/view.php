<?php
require('../../inc/bootstrap.php');

$convention = App::getConf()->getAllConvention();
if ($convention === null)
    App::setFlashAndRedirect('danger', 'un problème est survenu', '/');

require('../../inc/header_bo.php');
?>
<div class="panel panel-primary">
    <div class="panel-heading panel-heading-xs">
        Gestion des conventions
    </div>
    <div class="panel-body">
        <?php include('../../inc/print_flash_helper.php'); ?>
        <div class="col-xs-12 text-center">
            <form class="form-inline">
                <select class="form-control" name="convention" id="convention">
                    <option value="">Choisissez une convention</option>
                    <?php foreach ($convention as $k => $v): ?>
                        <option value="<?= $v->id; ?>"><?= $v->name; ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="button" class="btn btn-primary" onclick="window.location = 'edit?id=' + $(this).prev().val()">Editer la convention</button>
                <a href="add"><button type="button" class="btn btn-success">Ajouter une concention</button></a>
            </form>
        </div>
        <div class="col-xs-12 SpaceTop" id="showConvention"></div>
    </div>
</div>
<style>
    .inputVue > div {
        padding: 5px;
        border-bottom: 2px solid rgba(0.2, 0.2, 0.2, 0.2);
    }
</style>
<script>
    function appendVue(divId, content, title) {
        divId.children().remove();
        divId.append(title).append(content);
    }
    function constructVueTitle(text) {
        return "<h3>" +text+ "</h3>";
    }
    function constructVueInput(keyTab, valueTab) {
        return "<div class='inputVue'><div class='col-xs-6 col-sm-3'>" +keyTab+ "</div><div class='col-xs-6 col-sm-9'>" +valueTab+ "</div></div>"
    }
    function constructVueContent(keyTab, valueTab) {
        var len = keyTab.length;
        var i = -1;
        var ret = "";

        while (++i < len) {
            ret += constructVueInput(keyTab[i], valueTab);
        }
        return ret;
    }
    jQuery(document).ready(function() {
        $('#convention').on('change', function () {
            var elId = $(this).val();
            if (elId) {
                $.ajax({
                    url: 'api',
                    type: 'GET',
                    dataTypes: 'json',
                    data: {
                        id: elId
                    },
                    success: function (ans) {
                        var convention = JSON.parse(ans);
                        var vueTitle = constructVueTitle(convention.name);
                        var inputTest = "";
                        inputTest += constructVueInput('Durée de la pause', convention.breakDuration);
                        inputTest += constructVueInput('Durée entre les pauses', convention.intervalBreak);
                        inputTest += constructVueInput('Rappel médecin du travail', convention.medecinCheck);
                        inputTest += constructVueInput('Durée du déjeuner', convention.breakfastDuration);
                        inputTest += constructVueInput('Heures par semaine', convention.averageHoursWeek);
                        inputTest += constructVueInput('Max heures par semaine', convention.maxHoursWeek);
                        inputTest += constructVueInput('BreakHeaven', convention.breakHeaven);
                        inputTest += constructVueInput('Jours de congés gagnés par mois', convention.daysOffPerMonth);

                        appendVue($('#showConvention'), inputTest, vueTitle)
                    }
                })
            }
        })
    })
</script>
<?php
require_once("../../inc/footer_bo.php");