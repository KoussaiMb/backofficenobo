<?php
require('../../inc/bootstrap.php');

$method = 'GET';

if ($method == 'GET') {
    $convention = App::getConf()->getConventionById($_GET['id']);

    $convention->medecinCheck = parseStr::date_to_format($convention->medecinCheck, 'Y') . ' ans et ' . parseStr::date_to_format($convention->medecinCheck, 'm') . ' mois';
    $convention->daysOffPerMonth = parseStr::date_to_format($convention->daysOffPerMonth, 'd') . ' jours et ' . parseStr::date_to_format($convention->daysOffPerMonth, 'H') . ' heures';
    $convention->intervalBreak = parseStr::timeToTime($convention->intervalBreak);
    $convention->breakDuration = parseStr::timeToTime($convention->breakDuration);
    $convention->breakfastDuration = parseStr::timeToTime($convention->breakfastDuration);
    echo json_encode($convention);
}