<?php
require('../../inc/bootstrap.php');

$data = Session::getInstance()->read('formData');

require('../../inc/header_bo.php');
?>
<div class="panel panel-success">
    <div class="panel-heading panel-heading-xs">
        Ajouter une convention
    </div>
    <div class="panel-body">
        <?php include('../../inc/print_flash_helper.php'); ?>
        <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
            <form method="post" action="form">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="name">Nom de la convention</label>
                    <input type="text" class="form-control input-sm" name="name" value="<?= htmlspecialchars(isset($data->name) ? $data->name : null); ?>" placeholder="contrat 35h" required/>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="reference">Référence</label>
                    <input type="text" class="form-control input-sm" name="reference" value="<?=  htmlspecialchars(isset($data->reference) ? $data->reference : null); ?>" placeholder="lien"/>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="breakDuration">Durée des pauses</label>
                    <input type="text" class="form-control input-sm pickatime FiveStepTime" name="breakDuration" value="<?= htmlspecialchars(isset($data->breakDuration) ? $data->breakDuration : null); ?>" placeholder="click" required/>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="intervalBreak">Durée entre les pauses</label>
                    <input type="text" class="form-control input-sm pickatime" name="intervalBreak" value="<?=  htmlspecialchars(isset($data->intervalBreak) ? $data->intervalBreak : null); ?>" placeholder="click" required/>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="breakfastDuration">Durée du déjeuner</label>
                    <input type="text" class="form-control input-sm pickatime FiveStepTime" name="breakfastDuration" value="<?=  htmlspecialchars(isset($data->breakfastDuration) ? $data->breakfastDuration : null); ?>" placeholder="click" required/>
                </div>
                <label class="col-sm-12" for="medecinCheck_year">Rappel médecine du travail</label>
                <div class="form-group col-xs-12 col-sm-6">
                    <input type="number" class="form-control input-sm" name="medecinCheck_year" value="<?=  htmlspecialchars(isset($data->medecinCheck_year) ? $data->medecinCheck_year : 0); ?>" required/>
                    <label class="add-prev">Années</label>
                </div>
                <div class="form-group col-xs-12 col-sm-6">
                    <input type="number" class="form-control input-sm" name="medecinCheck_month" value="<?=  htmlspecialchars(isset($data->medecinCheck_month) ? $data->medecinCheck_month : 0); ?>" max="12" required/>
                    <label class="add-prev">Mois</label>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="averageHoursWeek">Heures par semaine</label>
                    <select class="form-control input-sm" name="averageHoursWeek" required>
                        <?php echo App::genClock(100, $data->averageHoursWeek); ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="maxHoursWeek">Heures par semaine maximum</label>
                    <select class="form-control input-sm" name="maxHoursWeek" required>
                        <?php echo App::genClock(100, $data->maxHoursWeek) ?>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-4">
                    <label for="breakHeaven">Heures avant rentabilité</label>
                    <select class="form-control input-sm" name="breakHeaven" required>
                        <?php echo App::genClock(100, $data->breakHeaven) ?>
                    </select>
                </div>
                <label class="col-sm-12">Nombre de jours et d'heures de congés gagnés par mois</label>
                <div class="form-group col-sm-6">
                    <input type="number" class="form-control input-sm" name="daysOffPerMonth_day" value="<?=  htmlspecialchars(isset($data->daysOffPerMonth_day) ? $data->daysOffPerMonth_day : 0); ?>" id="daysOffPerMonth_day" min="0" max="10" step="1"/>
                    <label for="daysOffPerMonth_day" class="add-prev sr-only">Jours</label>
                </div>
                <div class="form-group col-sm-6">
                    <input type="number" class="form-control input-sm" name="daysOffPerMonth_hour" value="<?=  htmlspecialchars(isset($data->daysOffPerMonth_hour) ? $data->daysOffPerMonth_hour : 0); ?>" id="daysOffPerMonth_hour" min="0" max="24" step="1"/>
                    <label for="daysOffPerMonth_hour" class="add-prev sr-only">Heures</label>
                </div>
                <div class="col-xs-12 form-group text-center">
                    <button type="submit" class="btn btn-success" name="action" value="add">Créer une nouvelle convention</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src='/js/app.js'></script>
<script>
    jQuery(document).ready(function() {
        $('.pickatime').timepicker({
            'timeFormat': 'H:i',
            'step': 30
        });
        $('.FiveStepTime').timepicker({
            'timeFormat': 'H:i',
            'step': 5,
            'minTime': '00:00',
            'maxTime': '03:00'
        });
    })
</script>
<?php
require_once("../../inc/footer_bo.php");