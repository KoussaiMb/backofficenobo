<?php
include_once('../../inc/bootstrap.php');

$db = App::getDB();

if (!isset($_POST['action']))
    App::redirect('view');

Session::getInstance()->update('formData', (object)$_POST);

if($_POST['action'] == 'add') {
    $validator = new Validator($_POST);
    $validator->is_time('breakDuration', 'La durée de la pause n\'est pas valide', 'La durée de la pause est nécessaire');
    $validator->is_time('intervalBreak', 'La durée entre les pauses n\'est pas valide', 'La durée entre les pauses est nécessaire');
    $validator->is_time('breakfastDuration', 'La durée de la pause déjeuner n\'est pas valide', 'La durée de la pause déjeuner est nécessaire');
    $validator->is_num('medecinCheck_year', 'Le rappel du médecin n\'est pas valide', 'Le rappel du médecin est nécessaire');
    $validator->is_num('medecinCheck_month', 'Le rappel du médecin n\'est pas valide', 'Le rappel du médecin est nécessaire');
    $validator->is_timeFormat('averageHoursWeek', 'Le nombre d\'heure par semaine n\'est pas valide', 'Le nombre d\'heure par semaine est nécessaire');
    $validator->is_timeFormat('maxHoursWeek', 'Le nombre d\'heure max par semaine n\'est pas valide', 'Le nombre d\'heure par semaine max est nécessaire');
    $validator->is_timeFormat('breakHeaven', 'Le breakheaven n\'est pas valide', 'Le breakheaven est nécessaire');
    $validator->is_num('daysOffPerMonth_day', 'Le nombre de jour de congés n\'est pas valide', 'Le nombre de jour de congés est nécessaire');
    $validator->is_num('daysOffPerMonth_hour', 'Le nombre d\'heure de congés n\'est pas valide', 'Le nombre d\'heure de congés est nécessaire');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('add');
    }
    $medecinCheck = strtotime($_POST['medecinCheck_month'] . ' months ' .  $_POST['medecinCheck_year'] . ' years') - time();
    $_POST['medecinCheck'] = date('Y-m-d H:i:s', $medecinCheck);
    $daysOffPerMonth = strtotime($_POST['daysOffPerMonth_day'] . ' day ' .  $_POST['daysOffPerMonth_hour'] . ' hour') - time();
    $_POST['daysOffPerMonth'] = date('Y-m-d H:i:s', $daysOffPerMonth);
    $conf = App::getConf();
    $id = $conf->addConvention($_POST);
    if ($id === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la création de la convention', 'add');
    Session::getInstance()->delete('formData');
    App::setFlashAndRedirect('success', 'La convention a bien été créée', 'edit?id=' . $id);
}
elseif ($_POST['action'] == 'edit') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'La convention n\'est pas valide', 'Aucune convention sélectionnée');
    $validator->is_time('breakDuration', 'La durée de la pause n\'est pas valide', 'La durée de la pause est nécessaire');
    $validator->is_time('intervalBreak', 'La durée entre les pauses n\'est pas valide', 'La durée entre les pauses est nécessaire');
    $validator->is_time('breakfastDuration', 'La durée de la pause déjeuner n\'est pas valide', 'La durée de la pause déjeuner est nécessaire');
    $validator->is_num('medecinCheck_year', 'Le rappel du médecin n\'est pas valide', 'Le rappel du médecin est nécessaire');
    $validator->is_num('medecinCheck_month', 'Le rappel du médecin n\'est pas valide', 'Le rappel du médecin est nécessaire');
    $validator->is_timeFormat('averageHoursWeek', 'Le nombre d\'heure par semaine n\'est pas valide', 'Le nombre d\'heure par semaine est nécessaire');
    $validator->is_timeFormat('maxHoursWeek', 'Le nombre d\'heure max par semaine n\'est pas valide', 'Le nombre d\'heure par semaine max est nécessaire');
    $validator->is_timeFormat('breakHeaven', 'Le breakheaven n\'est pas valide', 'Le breakheaven est nécessaire');
    $validator->is_num('daysOffPerMonth_day', 'Le nombre de jour de congés n\'est pas valide', 'Le nombre de jour de congés est nécessaire');
    $validator->is_num('daysOffPerMonth_hour', 'Le nombre d\'heure de congés n\'est pas valide', 'Le nombre d\'heure de congés est nécessaire');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('edit?id=' . $_POST['id']);
    }
    $medecinCheck = strtotime($_POST['medecinCheck_month'] . ' months ' .  $_POST['medecinCheck_year'] . ' years') - time();
    $_POST['medecinCheck'] = date('Y-m-d H:i:s', $medecinCheck);
    $daysOffPerMonth = strtotime($_POST['daysOffPerMonth_day'] . ' day ' .  $_POST['daysOffPerMonth_hour'] . ' hour') - time();
    $_POST['daysOffPerMonth'] = date('Y-m-d H:i:s', $daysOffPerMonth);
    $conf = App::getConf();
    $id = $conf->editConvention($_POST);
    if ($id === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la mise à jour de la convention', 'edit?id=' . $_POST['id']);
    Session::getInstance()->delete('formData');
    App::setFlashAndRedirect('success', 'La convention a bien été mise à jour', 'edit?id=' . $_POST['id']);
}
elseif ($_POST['action'] == 'delete') {
    $validator = new Validator($_POST);
    $validator->is_id('id', 'La convention n\'est pas valide', 'Aucune convention sélectionnée');
    if ($validator->is_valid() === false){
        App::array_to_flash($validator->getErrors());
        App::redirect('edit?id=' . $_POST['id']);
    }
    $conf = App::getConf();
    $id = $conf->delConvention($_POST);
    if ($id === null)
        App::setFlashAndRedirect('danger', 'Echec lors de la suppression de la convention', 'edit?id=' . $_POST['id']);
    Session::getInstance()->delete('formData');
    App::setFlashAndRedirect('success', 'La convention a bien été supprimée', 'view');
}
App::redirect('view');
