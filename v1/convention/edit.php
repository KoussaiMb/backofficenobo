<?php
require('../../inc/bootstrap.php');

if (empty($_GET['id']))
    App::setFlashAndRedirect('danger', 'Impossible de trouver la convention à éditer', 'view');
$convention = App::getConf()->getConventionById($_GET['id']);

require('../../inc/header_bo.php');
?>
    <div class="panel panel-primary">
        <div class="panel-heading panel-heading-xs">
            Editer une convention
        </div>
        <div class="panel-body">
            <?php include('../../inc/print_flash_helper.php'); ?>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
                <form method="post" action="form">
                    <div class="form-group col-xs-12 col-sm-8">
                        <label for="name">Nom de la convention</label>
                        <input type="text" class="form-control input-sm" name="name" value="<?= htmlspecialchars($convention->name); ?>" placeholder="contrat 35h" required/>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="reference">Référence</label>
                        <input type="text" class="form-control input-sm" name="reference" value="<?=  htmlspecialchars($convention->reference); ?>" placeholder="lien"/>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="breakDuration">Durée des pauses</label>
                        <input type="text" class="form-control input-sm pickatime FiveStepTime" name="breakDuration" value="<?= parseStr::time_without_seconds($convention->breakDuration); ?>" placeholder="click" required/>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="intervalBreak">Durée entre les pauses</label>
                        <input type="text" class="form-control input-sm pickatime" name="intervalBreak" value="<?=  parseStr::time_without_seconds($convention->intervalBreak); ?>" placeholder="click" required/>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="breakfastDuration">Durée du déjeuner</label>
                        <input type="text" class="form-control input-sm pickatime FiveStepTime" name="breakfastDuration" value="<?=  parseStr::time_without_seconds($convention->breakfastDuration); ?>" placeholder="click" required/>
                    </div>
                    <label class="col-sm-12" for="medecinCheck_year">Rappel médecine du travail</label>
                    <div class="form-group col-xs-12 col-sm-6">
                        <input type="number" class="form-control input-sm" name="medecinCheck_year" value="<?=  parseStr::date_to_format($convention->medecinCheck, 'Y'); ?>" required/>
                        <label class="add-prev">Années</label>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <input type="number" class="form-control input-sm" name="medecinCheck_month" value="<?=  parseStr::date_to_format($convention->medecinCheck, 'm'); ?>" max="12" required/>
                        <label class="add-prev">Mois</label>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="averageHoursWeek">Heures par semaine</label>
                        <select class="form-control input-sm" name="averageHoursWeek" required>
                            <?php echo App::genClock(100, $convention->averageHoursWeek); ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="maxHoursWeek">Heures par semaine maximum</label>
                        <select class="form-control input-sm" name="maxHoursWeek" required>
                            <?php echo App::genClock(100, $convention->maxHoursWeek) ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4">
                        <label for="breakHeaven">Heures avant rentabilité</label>
                        <select class="form-control input-sm" name="breakHeaven" required>
                            <?php echo App::genClock(100, $convention->breakHeaven) ?>
                        </select>
                    </div>
                    <label class="col-sm-12">Nombre de jours et d'heures de congés gagnés par mois</label>
                    <div class="form-group col-sm-6">
                        <input type="number" class="form-control input-sm" name="daysOffPerMonth_day" value="<?=  parseStr::date_to_format($convention->daysOffPerMonth, 'd'); ?>" id="daysOffPerMonth_day" min="0" max="10" step="1"/>
                        <label for="daysOffPerMonth_day" class="add-prev sr-only">Jours</label>
                    </div>
                    <div class="form-group col-sm-6">
                        <input type="number" class="form-control input-sm" name="daysOffPerMonth_hour" value="<?= parseStr::date_to_format($convention->daysOffPerMonth, 'H');?>" id="daysOffPerMonth_hour" min="0" max="24" step="1"/>
                        <label for="daysOffPerMonth_hour" class="add-prev sr-only">Heures</label>
                    </div>
                    <div class="col-xs-12 form-group text-center">
                        <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                        <a href="view"><button type="button" class="btn btn-default">Gestion des conventions</button></a>
                        <button type="submit" class="btn btn-primary" name="action" value="edit">Mettre à jour</button>
                        <button type="submit" class="btn btn-danger" name="action" value="delete" onclick="return confirm('Ceci supprimera définitivement la convention');">Supprimer la convention</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<style>

</style>
<script>
    jQuery(document).ready(function() {

    })
</script>
<?php
require_once("../../inc/footer_bo.php");