<?php
require('../inc/bootstrap_public.php');

if (!empty($_POST)) {
    if (App::getAuth()->login($_POST)){
        Session::getInstance()->setFlash('success', "Vous êtes bien connecté");
        $user = Session::getInstance()->read('auth');
        Session::getInstance()->write('just_log', 'true');
        App::redirect('/index');
    } else {
        Session::getInstance()->setFlash('danger', "Identifiants incorrects");
    }
}
App::redirect('/login');
