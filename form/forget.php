<?php
require('../inc/bootstrap_public.php');
$method = $_SERVER['REQUEST_METHOD'];

//TODO:tout refaire

if (!empty($_POST)) {
    $validator = new Validator($_POST);
    $db = App::getDB();
    $validator->exist($db, 'email', 'id', 'id', "Email existe déjà", "Pas d'email");
    if (!$validator->date_isok($db)){
        Session::getInstance()->setFlash('danger', 'Vous ne pouvez pas demander plus d\'un mot de passe tous les 24h');
        App::redirect('../forget');
    }
    elseif (App::getAuth()->forget($_POST['email'])){
        Session::getInstance()->setFlash('success', "Un email pour générer un nouveau mot de passe vous a été envoyé");
    }
    else {
        Session::getInstance()->setErr();
    }
    App::redirect('../login');
}else{
    //Session::getInstance()->setFlash('danger', 'Attention, votre ancien mot de passe ne sera plus valide');
    App::redirect("../index");
}
