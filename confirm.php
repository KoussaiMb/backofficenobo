<?php
require_once('inc/bootstrap_public.php');

$db = App::getDB();

$validator = new Validator($_GET);
if ($validator->can_confirm($db) && App::getAuth()->confirm($_GET['id'], $_GET['token'])){
    Session::getInstance()->setFlash('success',"Votre compte a bien été validé");
    App::redirect('index');
}else{
    Session::getInstance()->setFlash('danger',"Ce token n'est plus valide");
    App::redirect('login');
}

