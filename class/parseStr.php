<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 06/12/2016
 * Time: 12:32
 */
class parseStr
{
    static function DatetimeToDate($Datetime, $format = null){
        return substr($Datetime, 8, 2) . '/' . substr($Datetime, 5, 2) . '/' . substr($Datetime, 0, 4);
    }
    static function DatetimeToTime($Datetime, $format = null){
        return substr($Datetime, 11, 2) . 'h' . substr($Datetime, 14, 2);
    }
    static function GetIntFromFloat($float){
        return substr($float, 0, strpos($float, '.'));
    }
    static function GetDecFromFloat($float){
        return substr($float, strpos($float, '.') + 1);
    }
    static function DatetimeToBeginIn($Datetime){
        $now = new DateTime();
        $to = new DateTime($Datetime);
        return ($now->diff($to)->format(('%d jours %h heures et %i minutes')));
    }
    static function FloatToTime($float){
        $int = self::GetIntFromFloat($float);
        $dec = self::GetDecFromFloat($float) * 6;
        if ($dec)
        {
            if ($dec == 0)
                return $int . 'h';
            if ($dec < 10)
                return $int . 'h' . '0' . $dec;
        }
        return $int . 'h';
    }
    static function timeToTime($time){
        if ($time[0] == 0)
            return substr($time, 1, 1) . 'h' . substr($time, 3, 2);
        return substr($time, 0, 2) . 'h' . substr($time, 3, 2);
    }
    static function parseDateTime ($date, $time){
        $hour = $time . ":00";
        $test = explode('/', $date);
        return '20' . $test[2]. '-' . $test[1] . '-' . $test[0] . " ". $hour;
    }
    static function GenRate($rate, $StarNumber = 5)
    {
        $i = 0;
        $int = self::GetIntFromFloat($rate);
        $dec = self::GetDecFromFloat($rate);
        $dec = strlen($dec) == 1 ? $dec * 10 : $dec;
        while ($i++ < $int) {
            echo "<span class=\"fa fa-star rate-on\"></span>";
        }
        if ($dec >= 75) {
            echo "<span class=\"fa fa-star rate-on\"></span>";
        }
        elseif($dec >= 25)
            echo "<span class=\"fa fa-star rate-half\"></span>";
        while (++$i < $StarNumber)
            echo "<span class=\"fa fa-star rate-off\"></span>";
    }
    static function GenPhotoName($SourceName, $token){
        $now = (new DateTime())->format('YmdHis');
        $ext = substr(strrchr($SourceName,'.'), 0);
        return $token . $now .$ext;
    }
    static function date_to_str($date, $format = 'y-m-d'){
        $mois = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
        $year_pos = strpos($format, 'y') / 2;
        $month_pos = strpos($format, 'm') / 2;
        $day_pos = strpos($format, 'd') / 2;
        $symbol = $format[1];
        $date_temp = explode($symbol, $date);
        return $date_temp[$day_pos] . ' ' .  $mois[intval($date_temp[$month_pos])  - 1] . ' ' . $date_temp[$year_pos];
    }
    static function date_to_format($date, $format) {
        return date($format, strtotime($date)) - date($format, 0);
    }
    static function date_format($date, $format) {
        return date($format, strtotime($date));
    }
    static function time_without_seconds($time) {
        return substr($time, 0, 5);
    }
    static function after_trailing_slash($str) {
        if (strstr($str, '/'))
            return substr($str, strrpos($str, '/') + 1);
        return substr($str, strrpos($str, '\\') + 1);
    }
    static function after_rchar($str, $needle, $n = 1) {
        $max = strlen($str);
        $count = 0;

        if ($n < 1)
            return $str;
        while ($max--) {
            if ($str[$max] == $needle)
                $count++;
            if ($count == $n)
                return substr($str, $max + 1);
        }
        return $str;
    }
    static function compare_time_strict($time1, $time2){
        if (strtotime($time1) > strtotime($time2)){
            return true;
        }
        return false;
    }

    static function compare_time_strict_equal($time1, $time2){
        if (strtotime($time1) >= strtotime($time2)){
            return true;
        }
        return false;
    }

    static function compare_time_equal($time1, $time2){
        if (strtotime($time1) == strtotime($time2)){
            return true;
        }
        return false;
    }

    static function sum_the_time($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time)
        {
            list($hour,$minute,$second) = explode(':', $time);
            $seconds += $hour*3600;
            $seconds += $minute*60;
            $seconds += $second;
        }
        $hours = floor($seconds/3600);
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= $minutes*60;
        if($seconds <= 9)
        {
            $seconds = "0".$seconds;
        }
        if($minutes <= 9)
        {
            $minutes = "0".$minutes;
        }
        if($hours <= 9)
        {
            $hours = "0".$hours;
        }
        if($hours > 24)
        {
            $hours = "24";
        }
        if($hours >= 24 && $minutes > 0)
        {
            $minutes = 0;
        }
        return "{$hours}:{$minutes}:{$seconds}";
    }

    //convert HH:mm to minute
    static function convert_time_to_minute($time) {
        $total_minute = explode(":", $time);
        $total_minute = ($total_minute[0] * 60 + $total_minute[1]);

        return $total_minute;
    }

    static function sum_the_time_no_restrict($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time)
        {
            list($hour,$minute,$second) = explode(':', $time);
            $seconds += $hour*3600;
            $seconds += $minute*60;
            $seconds += $second;
        }
        $hours = floor($seconds/3600);
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= $minutes*60;
        if($seconds <= 9)
        {
            $seconds = "0".$seconds;
        }
        if($minutes <= 9)
        {
            $minutes = "0".$minutes;
        }
        if($hours <= 9)
        {
            $hours = "0".$hours;
        }
        return "{$hours}:{$minutes}:{$seconds}";
    }

    static function test_sub_time($time1, $time2)
    {
        $date1 = date_create("1980-10-10 " . $time1);
        $date2 = date_create("1980-10-10 " . $time2);

        $interval = date_diff($date1, $date2);

        return $interval->format("H:i:s");
    }

    static function subtract_the_time($time1, $time2) {
        $seconds = 0;

        list($hour,$minute,$second) = explode(':', $time1);
        $seconds += $hour*3600;
        $seconds += $minute*60;
        $seconds += $second;

        list($hour,$minute,$second) = explode(':', $time2);
        $seconds -= $hour*3600;
        $seconds -= $minute*60;
        $seconds -= $second;

        $hours = floor($seconds/3600);
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= $minutes*60;
        if($seconds <= 9)
        {
            $seconds = "0".$seconds;
        }
        if($minutes <= 9)
        {
            $minutes = "0".$minutes;
        }
        if($hours <= 9)
        {
            $hours = "0".$hours;
        }
        if($hours > 24)
        {
            $hours = "24";
        }
        if($hours >= 24 && $minutes > 0)
        {
            $minutes = 0;
        }
        return "{$hours}:{$minutes}:{$seconds}";
    }

    static function divide_time ($time, $divider){
        $total_minute = parseStr::convert_time_to_minute($time) / $divider;
        $result = floor($total_minute / 60).':'.($total_minute -   floor($total_minute / 60) * 60) . ':00';
        return $result;
    }

    static function count_total_time($time_tab){
        $total_time = '00:00:00';
        foreach ($time_tab as $key => $time) {
            $total_time = parseStr::sum_the_time_no_restrict($total_time, $time);
        }
        return $total_time;
    }

    static function get_date_diff($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);
    }

    static function cleanString($string)
    {
        $string = strtolower($string);
        $string = preg_replace("/[^a-z0-9_'\s-]/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", " ", $string);
        return $string;
    }

    static function cleanSymbols($string)
    {

        $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y');

        return strtr( $string, $unwanted_array);
    }

    static function convert_time_to_decimal($time)
    {

        $expl_time = explode(':', $time);
        $hours = $expl_time[0];
        $minutes = $expl_time[1];

        return $hours + round($minutes / 60, 2);
    }

    static function check_if_in_interval($interval_hours_set, $hours_set_to_check){
        if (self::compare_time_strict_equal($hours_set_to_check['start'], $interval_hours_set['start'])
            && self::compare_time_strict_equal($interval_hours_set['end'], $hours_set_to_check['end']))
            return true;
        else
            return false;
    }

    static function check_if_cover_interval($interval_hours_set, $hours_set_to_check){
        if (self::compare_time_strict_equal($interval_hours_set['start'], $hours_set_to_check['start'])
            && self::compare_time_strict_equal($hours_set_to_check['end'], $interval_hours_set['end']))
            return true;
        else
            return false;
    }

    static function check_if_cover_start_interval($interval_hours_set, $hours_set_to_check){
        if (self::compare_time_strict($interval_hours_set['start'], $hours_set_to_check['start'])
            && self::compare_time_strict($hours_set_to_check['end'], $interval_hours_set['start'])
            && self::compare_time_strict($interval_hours_set['end'], $hours_set_to_check['end']))
            return true;
        else
            return false;
    }

    static function check_if_cover_end_interval($interval_hours_set, $hours_set_to_check){
        if (parseStr::compare_time_strict($hours_set_to_check['end'], $interval_hours_set['end'])
            && self::compare_time_strict($interval_hours_set['end'], $hours_set_to_check['start'])
            && self::compare_time_strict($hours_set_to_check['start'], $interval_hours_set['start']))
            return true;
        else
            return false;
    }

    static function in_interval($interval_hours_set, $hours_set_to_check){
        if (self::check_if_in_interval($interval_hours_set, $hours_set_to_check) === true)
            return true;
        else if (self::check_if_cover_interval($interval_hours_set, $hours_set_to_check) === true)
            return true;
        else if (self::check_if_cover_start_interval($interval_hours_set, $hours_set_to_check) === true)
            return true;
        else if (self::check_if_cover_end_interval($interval_hours_set, $hours_set_to_check) === true)
            return true;
        else
            return false;
    }

    static function get_last_week_day($date, $day){
        $day_after_date = strtotime('+1 day', strtotime($date));
        $last_day = date('Y-m-d', strtotime('last ' . $day, $day_after_date));

        return $last_day;
    }

    static function get_next_week_day($date, $day){
        $day_before_date = strtotime('-1 day', strtotime($date));
        $next_day = date('Y-m-d', strtotime('next ' . $day, $day_before_date));

        return $next_day;
    }

    static function cleanImgName($name) {
        $trim_symbols = self::cleanSymbols($name);

        return strtolower(str_replace(' ', '-', $trim_symbols));
    }

    static function phoneReadable($phone_number) {
        $readable_number = "";
        $j = 0;
        $len = strlen($phone_number);

        if ($len != '10' && ($len != '12' || $phone_number[0] != '+'))
           return null;
        if ($phone_number[0] === '+')
            $spaces = [3, 1, 2, 2, 2];
        else
            $spaces = [2, 2, 2, 2];
        foreach ($spaces as $space) {
            while ($space-- > 0)
                $readable_number .= $phone_number[$j++];
            $readable_number .= ' ';
        }
        $readable_number .= $phone_number[$j] . $phone_number[$j + 1];
        return $readable_number;
    }
}