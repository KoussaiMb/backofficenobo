<?php
require_once __DIR__.'/../vendor/autoload.php';
use \Mailjet\Resources;
use \Mailjet\Client;

class mjMail
{
    private $mailjet;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
        $mailjet_keys = $this->getMailjetKeys();
        $this->mailjet = new Client($mailjet_keys['mailjet_public_key'], $mailjet_keys['mailjet_private_key']);
    }

    private function getMailjetKeys()
    {
        $out = [];

        $res = $this->db->query_log("SELECT * FROM `conf` WHERE `name` = \"mailjet_public_key\" OR `name` = \"mailjet_private_key\"")->fetchAll();
        foreach ($res as $r) {
            if ($r->name == "mailjet_public_key")
                $out['mailjet_public_key'] = $r->value;
            else
                $out['mailjet_private_key'] = $r->value;
        }
        return $out;
    }

    public function newCustomer($status, $data)
    {
        $name = $data->firstname . ' ' . $data->lastname;
        $rec = App::getListManagement()->getFieldById($data->recurrence_id);
        $date = 'Le' . parseStr::date_to_str($data->date) . ' à ' . $data->hour;
        $surface = $data->surface;
        $workTime = $data->workTime;
        $price = $data->price;
        $phone = $data->phone;
        $address = $data->address;
        $body = [
            'Recipients' => [['Email' => "contact@nobo.life"]],
            'FromEmail' => "service-client@nobo.life",
            'FromName' => "Nobo",
            'Subject' => "Dring ! Un nouveau client",
            'Text-part' => "
                Un nouveau client est arrivé, status du paiement: $status
                $name
                $rec
                $date
                $surface
                $price
                ",
            'Html-part' => "
                <h3>Une nouveau client est arrivé</h3>
                <h4>Status du paiement: $status</h4>
                <br/> $name
                <br/> $rec, $workTime par prestation
                <br/> $date
                <br/> $surface m²
                <br/> $price euros
                <br/> $phone
                <br/> $address
                "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function welcomeCustomer($recap, $subForm)
    {
        $date = parseStr::date_to_str($recap->date);
        $email = $subForm->email;
        $body = [
            'Recipients' => [['Email' => "$email"]],
            'FromEmail' => "service-client@nobo.life",
            'FromName' => "Nobo",
            'Subject' => "Bienvenue chez Nobo",
            'Text-part' => "
                Bonjour,
    
                Votre compte client vient d'être créé et toute l'équipe Nobo vous souhaite la bienvenue !
                Votre réservation pour le $date a bien été prise en compte.
                
                Un majordome vous contactera pour préciser les détails concernant l'accès à votre domicile dans les jours à venir.
                
                Vous souhaitant une agréable journée,
                Nobo.
            ",
            'Html-part' => "
                Bonjour,
                <br/><br/>
                Votre compte client vient d'être créé, Nobo vous souhaite la bienvenue !
                <br/><br/>
                Votre réservation pour le $date a bien été prise en compte.
                <br/><br/>
                Un gouvernant vous contactera pour préciser les détails concernant l'accès à votre domicile dans les jours à venir.
                <br/><br/>
                Vous souhaitant une agréable journée,
                <br/>
                Nobo.
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function forget($email, $token)
    {
        $lien = $_SERVER['HTTP_HOST'] . "/forget?token=" . $token;
        $body = [
            'Recipients' => [['Email' => "$email"]],
            'FromEmail' => "service-client@nobo.life",
            'FromName' => "Nobo",
            'Subject' => "Votre nouveau mot de passe",
            'Text-part' => "
                Bonjour,
    
                Nous venons de recevoir une demande pour renouveller votre mot de passe, veuillez suivre le lien ci-dessous.
                
                $lien
                
                Si vous n'avez fait aucune demande, veuillez nous contacter:
                contact@nobo.life
                
                L'équipe Nobo
            ",
            'Html-part' => "
                Bonjour,
                <br/><br/>
                Nous venons de recevoir une demande pour renouveller votre mot de passe, veuillez suivre le lien ci-dessous.
                <br/><br/>
                $lien
                <br/><br/>
                Si vous n'avez fait aucune demande, veuillez nous contacter:
                <br/>
                contact@nobo.life
                <br/><br/>
                Nobo.
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function reset_password($user)
    {
        $lien = $_SERVER['HTTP_URL_NOBO'] . "/mon-espace/register?token=" . $user->reset_token;
        $body = [
            'Recipients' => [['Email' => "$user->email"]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Accédez à votre espace privé",
            'Text-part' => "
                Bonjour $user->firstname,
    
                Vous recevez ce mail suite à votre demande de mot de passe pour votre espace client Nobo.
                Générez votre mot de passe en un clic avec le lien suivant :
                
                $lien
                
                Bonne journée,
                Nobo Paris.
            ",
            'Html-part' => "
                Bonjour $user->firstname,
                <br/><br/>
                Vous recevez ce mail suite à votre demande de mot de passe pour votre espace client Nobo.<br>
                Générez votre mot de passe en un clic avec le lien suivant :
                <br/><br/>
                $lien
                <br/><br/>
                Bonne journée,<br>
                l'équipe Nobo
            "
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        return isset($response);
    }

    public function transactionYearErratum($client, $year, $pdf)
    {
        $client = (object)$client;
        $body = [
            'Recipients' => [['Email' => $client->email]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "[ERRATUM]Nobo - Attestation fiscale $year",
            'Text-part' => "
            Bonjour $client->firstname,
            
            Suite à un problème technique, votre précédente attestation fiscale comportait les mauvais intervenants.
            
            Veuillez trouver ci-joint l'attestation fiscale à prendre en compte pour l'année $year.
            
            Toutes les informations nécessaires y sont présentes.
            
            Nous restons à votre disposition pour tout renseignement,
            
            Bien cordialement,
            Nobo paris
                ",
            'Html-part' => "
            <p>Bonjour $client->firstname,</p>
            <p>Suite à un problème technique, votre précédente attestation fiscale comportait les mauvais intervenants.</p>
            <p>Veuillez trouver ci-joint l'attestation fiscale à prendre en compte pour l'année $year.</p>
            <p>Toutes les informations nécessaires y sont présentes.</p>
            <p>Nous restons à disposition pour tout renseignement,</p>
            <p>Bien cordialement,<br>Nobo paris</p>
                ",
            "Attachments" => [[
                "ContentType" => "application/x-pdf",
                "Filename" => "Nobo - Attestations fiscale $year - $client->firstname $client->lastname.pdf",
                "content" => base64_encode($pdf)
            ]]
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        if ($response->success())
            ;
        else
            $this->mailjetError($client);
    }

    public function transactionYear($client, $year, $pdf)
    {
        $client = (object)$client;
        $body = [
            'Recipients' => [['Email' => $client->email]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Nobo - Attestation fiscale $year",
            'Text-part' => "
            Bonjour $client->firstname,
 
            Veuillez trouver ci-joint votre attestation fiscale pour l'année $year.
            
            Toutes les informations nécessaires y sont présentes.
            
            Nous restons à votre disposition pour tout renseignement,
            
            Bien cordialement,
            Nobo paris
                ",
            'Html-part' => "
            <p>Bonjour $client->firstname,</p>
            <p>Veuillez trouver ci-joint votre attestation fiscale pour l'année $year.</p>
            <p>Toutes les informations nécessaires y sont présentes.</p>
            <p>Nous restons à disposition pour tout renseignement,</p>
            <p>Bien cordialement,<br>Nobo paris</p>
                ",
            "Attachments" => [[
                "ContentType" => "application/x-pdf",
                "Filename" => "Nobo - Attestations fiscale $year - $client->firstname $client->lastname.pdf",
                "content" => base64_encode($pdf)
            ]]
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
        if ($response->success())
            ;
        else
            $this->mailjetError($client);
    }

    public function sendPlanning($data)
    {
        $client = (object)$data['data_user'];
        $pdf = $data['pdf'];
        $body = [
            'Recipients' => [['Email' => $client->email]],
            'FromEmail' => $_SERVER['HTTP_MAIL_SENDER'],
            'FromName' => "Nobo",
            'Subject' => "Nobo - Planning d'entretien",
            'Text-part' => "
                Bonjour  $client->firstname,

                Nous avons le plaisir de vous transmettre votre planning d'entretien ci-joint.

                En espérant avoir répondu à vos attentes,
                Nobo
                ",
            'Html-part' => "
                <p>
                Bonjour  $client->firstname,
                <br><br>
                Nous avons le plaisir de vous transmettre votre planning d'entretien ci-joint.
                <br><br>
                En espérant avoir répondu à vos attentes,
                <br>Nobo
                ",
            "Attachments" => [[
                "ContentType" => "application/x-pdf",
                "Filename" => "toto - $client->firstname $client->lastname.pdf",
                "content" => base64_encode($pdf)
            ]]
        ];
        try{
            $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
            if ($response->success())
                return true;
            else
                $this->mailjetError($client->firstname."_".$client->lastname."_".$client->email."_RESPONSE");
        }catch (Exception $e){
            $this->mailjetError($client->firstname."_".$client->lastname."_".$client->email."_EXCEPTION");
            return false;
        }
    }

    private function mailjetError($params) {
        $this->db->query_log("INSERT INTO log_mailjet (params, date_create)
                              VALUES( ?, NOW())", [$params]);
    }
}
