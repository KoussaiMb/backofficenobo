<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 06/08/2018
 * Time: 16:23
 */

class relatedServices
{
    private $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getAllServices() {
        $ret = $this->db->query_log("SELECT 
                                                rs.*,
                                                l.field as rs_category
                                                FROM 
                                                `related_services` rs
                                                INNER JOIN `list` l ON l.`id` = rs.`l_rs_category_id` 
                                                WHERE rs.`deleted` = 0");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getAllServicesRequested() {
        $ret = $this->db->query_log("SELECT 
                                                c.id as customer_id,
                                                rsq.id as rs_request_id,
                                                CONCAT(u.firstname, ' ', u.lastname) as customer_name,
                                                DATE_FORMAT(rsq.requested_at, '%d %M %Y') as requested_at,
                                                rs.name as service_name,
                                                IF(rsq.processed = 1, 'processed', 0) as processed,
                                                IF(rsq.issued = 1, 'issued', 0) as issued,
                                                IF(rsq.canceled = 1, 'canceled', 0) as canceled,
                                                IF(rsq.processed = 0 AND rsq.canceled = 0 AND rsq.issued = 0, 'waiting', 0) as waiting
                                                FROM 
                                                `rs_request` rsq
                                                INNER JOIN `customer` c ON c.`id` = rsq.`customer_id` 
                                                INNER JOIN `user` u ON u.`id` = c.`user_id` 
                                                INNER JOIN `related_services` rs ON rs.`id` = rsq.`rs_id` 
                                                WHERE 
                                                u.deleted = '0'
                                                ORDER BY requested_at ASC");
        if ($ret === false)
            return false;
        return $ret->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addService($data) {
        $ret = $this->db->query_log("INSERT INTO
                                                 `related_services`
                                                 SET 
                                                 `l_rs_category_id` = ?,
                                                 `name` = ?,
                                                 `website` = ?,
                                                 `description` = ?
                                                 ", [
                                                     $data['l_rs_category_id'],
                                                     $data['name'],
                                                     $data['website'],
                                                     $data['description']
            ]
        );
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function getServiceById($rs_id) {
        $ret = $this->db->query_log("SELECT 
                                                rs.*,
                                                l.field as rs_category
                                                FROM 
                                                `related_services` rs
                                                INNER JOIN `list` l ON l.`id` = rs.`l_rs_category_id` 
                                                WHERE rs.`deleted` = 0 AND rs.`id` = ?", [$rs_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function editService($data) {
        $ret = $this->db->query_log("UPDATE
                                                 `related_services`
                                                 SET 
                                                 `l_rs_category_id` = ?,
                                                 `name` = ?,
                                                 `website` = ?,
                                                 `description` = ?
                                                 WHERE
                                                 `id` = ?
                                                 ", [
                $data['l_rs_category_id'],
                $data['name'],
                $data['website'],
                $data['description'],
                $data['id']
            ]
        );
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function deleteService($data) {
        $ret = $this->db->query_log("DELETE FROM `related_services` WHERE `id` = ?", [$data['id']]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function updateRequest($data) {
        $user = App::getAuth()->user();
        $by = empty($user) ? null : $user->id;

        $ret = $this->db->query_log("UPDATE 
                                          `rs_request` 
                                          SET 
                                          `processed` = ?, 
                                          `canceled` = ?,
                                          `cancel_comment` = ?,
                                          `processed_at` = NOW(),
                                          `processed_by` = ?
                                          WHERE
                                          `id` = ?
                                          ",
            [
                $data['processed'],
                $data['canceled'],
                empty($data['cancel_comment']) ? null : $data['cancel_comment'],
                $by,
                $data['rs_request_id']
            ]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }
}