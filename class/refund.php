<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 25/06/2018
 * Time: 11:46
 */

class refund
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->query_log("SELECT * FROM `refund` WHERE `deleted` = 0")->fetchAll();
    }

    public function addRefund($id_mission, $amount, $id_listRefund, $transaction_ext_ref, $user_id )
    {
        $this->db->query_log("INSERT INTO `refund`(`id_mission`, `amount`, `l_refundReason_id` , `transaction_ext_ref`, `user_id`) values(?, ?, ?, ?, ?)",
            [
                $id_mission,
                $amount,
                $id_listRefund,
                $transaction_ext_ref,
                $user_id
            ]);
        return $this->db->lastInsertId();
    }

    public function getAllRefund()
    {
        $ret = $this->db->query_log("SELECT * from `refund` r, `user` u WHERE r.user_id = u.id AND u.who = 'customer'");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }
}
