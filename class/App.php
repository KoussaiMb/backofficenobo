<?php

class App
{
    static $db = null;
    static $list = null;

    static function getDB($database = 'nobo_new'){
        if (!self::$db){
            self::$db = new DB($_SERVER['HTTP_MYSQL_HOST'], $_SERVER['HTTP_MYSQL_DBNAME'], $_SERVER['HTTP_MYSQL_USERNAME'], $_SERVER['HTTP_MYSQL_PASSWORD']);
        }
        return self::$db;
    }

    static function getAuth(){
        return new Auth(Session::getInstance(), self::getDB(), [
            'restrictMsg' => "Sorry, la page à laquelle vous tentez d'accéder n'est pas disponible",
        ]);
    }

    static function getTask() {
        return new task(self::getDB());
    }

    static function getTaskTwo() {
        return new taskTwo(self::getDB());
    }

    static function getPlanning()
    {
        return new planning(self::getDB());
    }

    static function getRefund()
    {
        return new refund(self::getDB());
    }

    static function getSurvey() {
        return new survey(self::getDB());
    }

    static function getMission() {
        return new mission(self::getDB());
    }

    static function getAlert() {
        return new alert(self::getDB(), self::getAuth()->user()->id);
    }

    static function getPromoCode() {
        return new promocode(self::getDB());
    }

    static function getCustomer() {
        return new customer(self::getDB());
    }

    static function getProvider() {
        return new provider(self::getDB());
    }

    static function getAddress() {
        return new address(self::getDB());
    }

    static function getCluster() {
        return new cluster(self::getDB());
    }

    static function getUser() {
        return new user(self::getDB());
    }

    static function getRelatedServices() {
        return new relatedServices(self::getDB());
    }

    static function getConf() {
        return new conf(self::getDB());
    }

    static function getManageGroup(){
        return new manageGroup(self::getDB());
    }

    static function getFullCalendar(){
        return new fullCalendar(self::getDB());
    }

    static function getMaintenancePlanning($address_token){
        $validator = new Validator(['address_token' => $address_token]);

        $validator->isToken('address_token', 'L\'addresse est invalide', 'L\'addresse est introuvable');
        if ($validator->is_valid())
            $address = App::getAddress()->getAddressByToken($address_token);

        if (!$validator->is_valid() || empty($address)){
            return null;
        }
        return new maintenancePlanning(self::getDB(), $address->id);
    }

    static function getMaintenancePlanningType(){
        return new maintenancePlanningType(self::getDB());
    }

    static function getProviderEditPlanning() {
        return new providerEditPlanning(self::getDB(), self::getFullCalendar());
    }

    static function addAjaxLog($function, $error, $user_id, $file) {
        try {
            self::getDB()->query("INSERT INTO `log_bo`(`function`, `error`, `user_id`, `file`, `date_create`) VALUES(?, ?, ?, ?, NOW())",[
                $function,
                $error,
                $user_id,
                $file
            ]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    static function getManageRights(){
        return new ManageRights(self::getDB());
    }

    static function addLog($function, $error, $user_id, $file) {
        try {
            self::getDB()->query("INSERT INTO `log_bo`(`function`, `error`, `user_id`, `file`, `date_create`) VALUES(?, ?, ?, ?, NOW())",[
                $function,
                $error,
                $user_id,
                $file
            ]);
        } catch (PDOException $e) {
            self::setFlashAndRedirect('danger', 'critical error, can\'t log errors', '/');
        }
    }

    static function logPDOException(PDOException $e, $deep, $file = null, $function = null)
    {
        $values = [];
        $values[] = $function === null ? debug_backtrace()[$deep]['function'] : $function;
        $values[] = $e->getMessage();
        $values[] = self::getAuth()->user()->id;
        if ($file === null)
            $file = debug_backtrace()[$deep]['file'];
        $needle =  strchr($file, '/') ? '/' : '\\';
        $values[] = parseStr::after_rchar($file, $needle, 3);

        try{
            self::getDB()->query("INSERT INTO `log_bo`(`function`, `error`, `user_id`, `file`, `date_create`) VALUES(?, ?, ?, ?, NOW())",
                $values);
        } catch (PDOException $e){
            Session::getInstance()->setFlash("danger", "erreur critique , contacter un admin");
        }
    }

    static function redirect($url){
        header("Location: $url");
        exit(0);
    }

    static function fail_redirect($url){
        Session::getInstance()->setFlash('danger', 'Une erreur inconnue est survenue, contactez un administrateur');
        header("Location: $url");
        exit(0);
    }

    static function redirect_js($url){
        echo "<script>location ='". $url . "';</script>";
        exit(0);
    }

    static function console_log($data){
        echo "<script>console.log(". $data . ");</script>";
    }
    static function object_to_array($obj) {
        $arr= null;
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? App::object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    static function array_without_null($arr){
        $new_arr = null;
        foreach ($arr as $k => $v){
            if ($v){
                $new_arr = [$k => $v];
            }
        }
        return $new_arr;
    }

    static function getListManagement(){
        return new ListManagement(self::getDB());
    }

    static function getCompany(){
        return new company(self::getDB());
    }

    static function getMail(){
        return new mjMail(self::getDB());
    }

    static function getCardex(){
        return new cardex(self::getDB());
    }

    static function getItem(){
        return new item(self::getDB());
    }
    static function getGenerationInvoice()
    {
        return new temp_generation_facture(self::getDB());
    }

    static function getTransaction(){
        return new transaction(self::getDB());
    }

    static function get_instance_generatePdf($data,$type){
        return new generatePdf($data, $type);
    }

    static function get_instance_planningHTML($data){
        return new planningHTML($data);
    }

    static function get_instance_validator($data){
        return new Validator($data);
    }
    static function getWorkflow(){
        return new Workflow(self::getDB());
    }

    static function getReception(){
        return new reception(self::getDB());
    }

    static function getDispo($provider_id){
        $dispo = self::getDB()->query("SELECT *, D.id FROM DISPO D WHERE D.user_id = ? AND D.start > NOW() ORDER BY D.start ASC", [$provider_id])->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($dispo);
    }

    static function getPrestation(){
        return new prestation(self::getDB());
    }

    static function getCustomerActions(){
      return new CustomerActions(self::getDB());
    }

    static function getMenuManager(){
        return new MenuManager(self::getDB());
    }

    static function getSearchAlgorithm(){
        try {
            $google_key_matrix = self::getConf()->getConfByName('google_key_matrix');
            $instance_distance_matrix = new DistanceMatrix('json', $google_key_matrix->value);
            $dayConf = self::getDayConf();
            return new search_algorithm(self::getDB(), $dayConf , $instance_distance_matrix);
        } catch (Exception $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), self::getAuth()->user()->id, __FILE__ . ':' . __LINE__);
            return null;
        }
    }

    static function setHtmlAndExit($message, $type) {
        echo "<div class='col-xs-12 text-center'><p class='alert alert-" .$type. "'>" .$message. "</p></div>";
        exit();
    }

    static function setFlashAndRedirect($flashType, $flashMessage, $redirection) {
        Session::getInstance()->setFlash($flashType, $flashMessage);
        self::redirect($redirection);
    }

    static function redirectPrev() {
        echo "<script>history.go(-1);</script>";
        exit();
    }

    static function setFlashRedirectPrev($flashType, $flashMessage) {
        Session::getInstance()->setFlash($flashType, $flashMessage);
        self::redirectPrev();
    }

    static function upSuccess() {
        Session::getInstance()->setFlash('success', 'Les modifications ont été prises en compte.');
    }

    static function upFail() {
        Session::getInstance()->setFlash('danger', 'Les modifications n\'ont pas été prises en compte.');
    }

    static function unkownError() {
        Session::getInstance()->setFlash('danger', 'Une erreur inconnue est survenue, veuillez nous excuser.');
    }

    static function criticalError(){
        Session::getInstance()->setFlash('info', 'le site est en maintenance, réessayez plus tard.');
        self::redirect('/');
    }

    static function array_to_flash($array, $redirect_url = null)
    {
        foreach ($array as $k => $v) {
            Session::getInstance()->setFlash('danger ' . $k, $v);
        }
        if ($redirect_url)
            self::redirect($redirect_url);
    }

    static function createPath($path, $code = 0644)
    {
        if (!file_exists($path))
            return mkdir($path, $code, true);
        return 1;
    }

    static function get_position_in_array($needle, $haystack) {
        $i = -1;
        $max = count($haystack);

        while (++$i < $max)
        {
            if (strcmp($needle, $haystack[$i]) == 0)
                return $i;
        }
        return -1;
    }

    static function getRequest(){
        $query_str = file_get_contents("php://input");
        $array = array();
        parse_str($query_str,$array);
        return $array;
    }

    static function genClock($clockHours, $selected) {
        $max = $clockHours * 2;
        $output = "";

        for ( $i = 0; $i <= $max; $i++) {

            if ($i % 2 == 0)
                $minutes = '00';
            else
                $minutes = '30';
            if ($i < 20)
                $heures = '0' . (int)($i / 2);
            else
                $heures = (int)($i / 2);
            $res = $heures . ":" . $minutes . ":" . "00";
            if ($res == $selected)
                $output .= "<option value='" . $res . "' selected>" . $res . "</option>";
            else
                $output .= "<option value='" . $res . "'>" . $res . "</option>";
        }
        return $output;
    }

    static function getAllLinePic(){
        try {
            return self::getDB()->query("SELECT * FROM img_metro")->fetchAll();
        }	catch(Exception $e) {
            App::logPDOException($e, 1);
            return -1;
        }
    }

    static function getAutocompleteMetro() {
        try {
            $autocomplete = [];

            $ret = self::getDB()->query("SELECT DISTINCT arret, ligne, id FROM `metro_paris`")->fetchAll();
            if (!$ret)
                return false;
            foreach ($ret as $key => $value) {
                array_push($autocomplete, [
                    'value' => $value->arret,
                    'data' => array(
                        "ligne" => $value->ligne,
                        "id" => $value->id
                    )
                ]);
            }
            return $autocomplete;
        } catch (PDOException $e) {
            return null;
        }
    }

    static function getDayConf(){
        $conf = self::getConf()->getAll();
        $sorted_conf = [

            ["start" => $conf['sundayStart'], "end" => $conf['sundayEnd']],
            ["start" => $conf['mondayStart'], "end" => $conf['mondayEnd']],
            ["start" => $conf['tuesdayStart'], "end" => $conf['tuesdayEnd']],
            ["start" => $conf['wednesdayStart'], "end" => $conf['wednesdayEnd']],
            ["start" => $conf['thursdayStart'], "end" => $conf['thursdayEnd']],
            ["start" => $conf['fridayStart'], "end" => $conf['fridayEnd']],
            ["start" => $conf['saturdayStart'], "end" => $conf['saturdayEnd']]
        ];
        return $sorted_conf;
    }

    static function getSplittedUrl($url) {
        $url = str_replace('\\', '/', $url);

        if (preg_match("/(\/v\d\/)/", $url, $matches, PREG_OFFSET_CAPTURE))
            $url = substr($url, $matches[0][1]);

        return explode('/', $url);
    }

    static function getEnumFromTableAndColumn($table, $column) {
        $ret = self::getDB()->query_log("
SELECT COLUMN_TYPE as enum
FROM information_schema.`COLUMNS`
WHERE TABLE_NAME = ?
AND COLUMN_NAME = ?
", [$table, $column]);
        if ($ret === false)
            return false;
        $fetch_enum = $ret->fetch();
        if ($fetch_enum === false)
            return false;
        $enum_string = $fetch_enum->enum;
        $enum_exploded = explode('\'', $enum_string);
        $enum = array_filter($enum_exploded, function ($input) {return $input & 1;}, ARRAY_FILTER_USE_KEY);
        return (array_values(array_filter($enum)));
    }
}
