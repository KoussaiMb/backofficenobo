<?php

class search_algorithm
{

    public $db;
    public $distance;
    public $conf;

    public function __construct($db, $dayConf, DistanceMatrix $instance_distance_matrix)
    {
        $this->db = $db;
        $this->conf = $dayConf;
        $this->distance = $instance_distance_matrix;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// FUNCTION UTILS /////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $duration_tab
     *
     */
    private function sort_duration(&$duration_tab){
        rsort($duration_tab);
        foreach ($duration_tab as $key => $duration) {
            $duration_tab[$key] = $duration.':00';
        }
    }

    /**
     * @param $data                 @var array[
     *                                           @param end_slot => @var time,
     *                                           @param start_slot => @var time,
     *                                           @param duration => @var time,
     *                                           @param end_min => @var time,
     *                                           @param frequency => @var object,
     *                                           @param start_max => @var time,
     *                                           @param day => @var int between 0 and 6,
     *                                           @param address_token => @var string,
     *                                           @param total_duration => @var time
     *                                        ]
     *
     * @param $extend               @var string
     * @param $var_sql              @var empty_array
     */

    private function get_extend_and_sql_data($data, &$extend, &$var_sql){
        /* recurrent */
        $extend_type = "";
        $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        $weeknb = ((bool)!(date('W', strtotime(date('Y-m-d', strtotime('next '. $weekDay[$data['day']] , strtotime('-1 day', strtotime($data['start_week'])))))) % 2) + 1);
        $extend_type = "AND (type = 0 OR type = ". $weeknb ." OR type = 3)" ;
        $extend = "AND (
                        CASE WHEN (
                            SELECT
                            IF (COUNT(*) = 0, 1, 0)
                            FROM
                                agenda
                                WHERE
                                agenda.pending = 0 
                                AND agenda.deleted = 0
                                AND (((agenda.start_rec <= ? AND (agenda.end_rec >= ? OR ISNULL(agenda.end_rec))) OR (agenda.start_rec < ? AND (agenda.end_rec > ? OR ISNULL(agenda.end_rec))) OR ((? <= agenda.start_rec AND ? > agenda.start_rec) OR (? <= agenda.end_rec AND (? > agenda.end_rec OR ISNULL(agenda.end_rec)))))  OR (agenda.start_rec = ? AND agenda.end_rec = ?)) 
                                AND agenda.day = ? 
                                ". $extend_type ."
                                AND agenda.provider_id = provider.id
                                AND agenda.address_id = (
                                    SELECT
                                       address.id
                                       FROM
                                           address
                                       WHERE
                                        token = ?
                                                                                                        )
                                ) = 0 THEN FALSE ELSE TRUE
                        END)";
        $var_sql = [
            0,
            $data['duration'],
            $data['day'],
            $data['start_max'],
            $data['end_min'],
            $data['start_week'],
            $data['start_week'],
            $data['end_week'],
            $data['end_week'],
            $data['start_week'],
            $data['end_week'],
            $data['start_week'],
            $data['end_week'],
            $data['start_week'],
            $data['end_week'],
            $data['day'],
            $data['address_token']
        ];
    }

    /**
     * time = "HH:mm:ss"
     *
     * @param $hours_start       @var time
     * @param $hours_end         @var time
     * @param $day               @var int between 0 and 6
     * @param $duration          @var time
     * @return bool
     */

    private function check_if_conf(&$hours_start, &$hours_end, $day, $duration){
        if (parseStr::compare_time_strict($hours_end, $this->conf[$day]['end'].':00')) {
            $hours_end = $this->conf[$day]['end'] . ':00';
        }
        if (parseStr::compare_time_strict($this->conf[$day]['start'].':00', $hours_start)) {
            $hours_start = $this->conf[$day]['start'] . ':00';
        }
        if (parseStr::compare_time_strict_equal(parseStr::subtract_the_time($hours_end, $hours_start), $duration)) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * time = "HH:mm:ss"
     *
     * @param $duration_tab @var array [
     *                     @param day @var array[
     *                           @param slot @var array [
     *                                                          @param duration @var time,
     *                                                          @param travel_before @var time,
     *                                                          @param travel_after @var time,
     *                                                          @param start_slot @var time,
     *                                                          @param end_slot @var time,
     *                                                          @param start_max @var time,
     *                                                          @param end_min @var time,
     *                                                          @param day @var int between 0 and 6
     *                                                   ]
     *                                          ]
     *                                  ]
     *
     *
     * @param $slot_fdc         @var array[
     *                                     @param day @var array[
     *                                                           @param user_id @var int,
     *                                                           @param day @var int between 0 and 6,
     *                                                           @param duration @var time,
     *                                                           @param start_slot @var time,
     *                                                           @param end_slot @var time,
     *                                                           @param start_max @var time,
     *                                                           @param end_min @var time,
     *                                                           @param name @var string,
     *                                                           @param user_token @var string
     *                                                          ]
     *                                    ]
     *
     *
     * @param $valid_fdc        @var empty_array
     */

    private function fill_business_hours($duration_tab, $slot_fdc, &$valid_fdc){
        foreach ($duration_tab as $index_day => $day){
            foreach ($day as $slot_tab_index => $slot_tab) {
                foreach ($slot_tab as $slot_index => $slot) {
                    $valid_fdc[1][$slot_fdc[0]->user_token][] = array(
                        'dow' => array($slot['day']),
                        'start' => $slot['start_slot'],
                        'end' => $slot['end_slot'],
                        'duration' => $slot['duration']
                    );
                }
            }
        }
    }

    /**
     * time = "HH:mm:ss"
     *
     * @param $data @var array[
     *                          @param user_id @val int,
     *                          @param start @val time,
     *                          @param end @val time
     *                        ]
     * @param $type @var int
     * @return array
     */

    /* this function get all the mission between an interval of date and return a array of mission or null if empty */
    private function getMissionWeek($data, $type) {
        $extend = "";
        $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        $day_before_start_week = strtotime('-1 day', strtotime($data['start_week']));
        $next_day = strtotime('next '. $weekDay[$data['day']] , $day_before_start_week);
        $weeknb = ((bool)!(date('W', strtotime(date('Y-m-d', $next_day))) % 2) + 1);

        try {
            $extend = "AND (type = 0 OR type = ". $weeknb ." OR type = 3)";
            $sql = "select * from agenda where pending = 0 AND deleted = 0 AND provider_id = (SELECT id FROM provider WHERE user_id = ?) AND day = ? ". $extend ." 
            AND 
            (
                    (`start` <= ? AND `end` >= ?)
                OR 
                    (`start` < ? AND `end` > ?)
                OR 
                    ((? <= `start` AND ? > `start`) OR (? <= `end` AND ? > `end`))
            ) 
            AND 
            (
                (
                        (`start_rec` <= ? AND (`end_rec` >= ? OR ISNULL(end_rec)))
                    OR 
                        (`start_rec` < ? AND (`end_rec` > ? OR ISNULL(end_rec)))
                    OR
                        (
                                (? <= `start_rec` AND ? > `start_rec`)
                            OR 
                                (? <= `end_rec` AND (? > `end_rec` OR ISNULL(end_rec)))
                        )
                )
                OR 
                    (`start_rec` = ? AND `end_rec` = ?)
            ) 
            ORDER BY start ASC";
            return $this->db->query($sql, [$data['user_id'], $data['day'], $data['start'], $data['start'], $data['end'], $data['end'], $data['start'], $data['end'], $data['start'], $data['end'], $data['start_week'], $data['start_week'], $data['end_week'], $data['end_week'], $data['start_week'], $data['end_week'], $data['start_week'], $data['end_week'], $data['start_week'], $data['end_week']])->fetchAll();
        } catch (PDOException $e) {
            App::logPDOException($e, 1);
            return -1;
        }
    }


    /**
     *
     * time = "HH:mm:ss"
     *
     * @param $hours1 @var time
     * @param $hours2 @var time
     * @param $slot_to_check @var array[
     *                                   @param start_slot @var time
     *                                   @param end_slot @var time
     *                                   @param duration @var time
     *                                 ]
     * @param $slot_available_table @var array[
     *                                          @param slot_valid @var array
     *                                        ]
     * @return bool
     */

    /* this function clone the slot into the available slot table if the condition required are satisfied */
    private function check_if_slot_is_ok($hours1, $hours2, $slot_to_check, &$slot_available_table){
        if ($this->getTypeDiffTime($hours1, $hours2, $slot_to_check->duration) == true) {
            $slot_to_check->start_slot = $hours2;
            $slot_to_check->end_slot = $hours1;
            $slot_available_table[] = clone $slot_to_check;
            return true;
        } else
            return false;
    }


    /**
     * @param $date1 @var time
     * @param $date2 @var time
     * @param $duration @var time
     * @return bool
     */
    /* this function get 2 hours @date1 and @date2 and a duration and compare if @date2 is before @date1 and
    if the duration fit in the interval between the 2 hours and return a boolean */
    private function getTypeDiffTime($date1, $date2, $duration){
        $format_date_1 = new DateTime($date1);
        $format_date_2 = new DateTime($date2);
        $interval = $format_date_1->diff($format_date_2);
        if (($interval->format('%R')) == '-') {
            if (strtotime($interval->format('%H:%I:%S')) >= strtotime($duration))
                return true;
        }
        else
            return false;
        return false;
    }


    /**
     * @param $client_dispo_tab @var array[
     *                                      @param dispo @var array[
     *                                                               @param start @var time
     *                                                               @param end @var time
     *                                                               @param day @var time
     *                                                             ]
     *                                    ]
     * @param $sorted_duration @var array[
     *                                      @param duration @var time
     *                                   ]
     * @return array
     */

    /* this function get all the client disponibility and all the sorted duration and format them to an array
    of slot disponibility that can be processed after */
    private function fill_param_table($client_dispo_tab, $sorted_duration){
        $tab = [];
        foreach ($client_dispo_tab as $index_day => $client_dispo_day) {
            foreach ($client_dispo_day as $index_slot => $client_dispo) {
                foreach ($sorted_duration as $index_duration => $duration) {
                    if ($this->check_if_conf($client_dispo['start'], $client_dispo['end'], $client_dispo['day'], $duration)) {
                        $start_max = parseStr::subtract_the_time($client_dispo['end'], $duration);
                        $end_min = parseStr::sum_the_time($client_dispo['start'], $duration);
                        array_push($tab, array(
                            "day" => $client_dispo['day'],
                            "start_slot" => $client_dispo['start'],
                            "end_slot" => $client_dispo['end'],
                            "duration" => $duration,
                            "start_max" => $start_max,
                            "end_min" => $end_min,
                        ));
                    }
                }
            }
        }
        return $tab;
    }


    /**
     * @param $slot_available_table @var array[
     *                                          @param slot_valid @var array
     *                                        ]
     * @return array
     */

    /* this function get all slot unsort and sort them by provider id and return an array */
    private function group_slot_by_fdc($slot_available){
        $slot_by_fdc = [];
        foreach ($slot_available as $key => $slot) {
            $slot_by_fdc[$slot->user_id][] = $slot;
        }
        return $slot_by_fdc;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////://///////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @param $slot_to_check
     * @return array
     */

    private function init_data_slot($slot_to_check, $data){
        $data_Planning_Slot = [
            'user_id' => $slot_to_check->user_id,
            'day' => $slot_to_check->day,
            'start' => $slot_to_check->start_slot,
            'end' => $slot_to_check->end_slot,
            "start_week" => $data['start_week'],
            "end_week" => $data['end_week']
        ];

        return $data_Planning_Slot;
    }

    /* this function get the travel time between 2 addresses by calling google API distance matrix
            https://developers.google.com/maps/documentation/distance-matrix/
    */
    private function get_time_travel($id_origin, $id_destination, $departure_time) {
        $address_origin = App::getAddress()->getAddressById($id_origin);
        $address_destination = App::getAddress()->getAddressById($id_destination);

        try {
            $this->distance->setOptions(
                array(
                    "departure_time" => strtotime($departure_time),
                    "mode" => 'transit',
                    "origins" => $address_origin->lat.','.$address_origin->lng,
                    "destinations" => $address_destination->lat.','.$address_destination->lng
                ));
            if ($this->distance->request()) {
                return $this->distance->rows;
            }
        } catch (Exception $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), App::getAuth()->user()->id, __FILE__ . ':' . __LINE__ );
        }
        $json = parseJson::error('Erreur lors de la récupération des temps de trajets');
        $json->printJson();
    }

    /* this function calculate travel_time and check if the mission are still good with the right start and end time */
    public function calculate_travel_time(&$slot_available) {
        foreach ($slot_available as $index => $slot){
            /* we check if we have a travel after the mission we want to add */
            //TODO:Implementation to handle error is wobbly, do it well please
            if (!is_null($slot->mission_after_address_id)) {
                $departure_time = parseStr::subtract_the_time($slot->departure_time, '00:30:00');
                $travel_after = $this->get_time_travel($slot->customer_slot_address_id, $slot->mission_after_address_id, $departure_time);
                $travel_after = isset($travel_after[0]->elements[0]->duration->value) ? $travel_after[0]->elements[0]->duration->value : 0;
            }
            else {
                $travel_after = 0;
            }
            /* we check if we have a travel before the mission we want to add */
            //TODO:Implementation to handle error is wobbly, do it well please

            if (!is_null($slot->mission_before_address_id)) {
                $departure_time = $slot->departure_time;
                $travel_before = $this->get_time_travel($slot->mission_before_address_id, $slot->customer_slot_address_id, $departure_time);
                $travel_before = isset($travel_before[0]->elements[0]->duration->value) ? $travel_before[0]->elements[0]->duration->value : 0;
            }
            else {
                $travel_before = 0;
            }

            /* we initiate the new start and end for the event, to prevent slot bigger than the mission ex : 4H slot for 2H30 wanted */
            $new_start = parseStr::sum_the_time_no_restrict($slot_available[$index]->start_slot, gmdate("H:i:s", $travel_before));
            $new_end = parseStr::subtract_the_time($slot_available[$index]->end_slot, gmdate("H:i:s", $travel_after));

            /* we check if with the new start and end the slot can always contain the duration if not we unset this slot */
            if ($this->getTypeDiffTime($new_end, $new_start, $slot_available[$index]->duration) === false){
                unset($slot_available[$index]);
            }
            else {
                $slot_available[$index]->travel_after = $travel_after;
                $slot_available[$index]->travel_before = $travel_before;
                $slot_available[$index]->start_slot = $new_start;
                $slot_available[$index]->end_slot = $new_end;
            }
        }
    }

    private function set_calcul_travel_data($id_before, $id_after, &$slot_to_check, $departure_time){
        $slot_to_check->mission_before_address_id = $id_before;
        $slot_to_check->mission_after_address_id = $id_after;
        $slot_to_check->departure_time = $departure_time;
    }

    /**
     * @param $mission_array @var array[
     *                                   @param start @var time
     *                                   @param end @var time
     *                                   @param duration @var time
     *                                 ]
     * @param $slot_to_check @var array[
     *                                   @param start_slot @var time
     *                                   @param end_slot @var time
     *                                   @param duration @var time
     *                                   @param start_max @var time
     *                                   @param day @var int between 0 and 6
     *                                 ]
     * @param $slot_available_table @var array[
     *                                          @param valid_slot @var array
     *                                        ]
     * @param $max_end @var time
     * @return bool
     */
    private function check_slot_validity($mission_array, $slot_to_check, &$slot_available_table, $max_end){

        /* on recupere le nombre de missions */
        $nb_case = count($mission_array) - 1;

        /* si il n'y a aucune missions */
        if($nb_case < 0) {
            /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
            $this->set_calcul_travel_data(null, null, $slot_to_check, '08:00:00');
            $slot_available_table[] = $slot_to_check;
            return true;
        }
        /* si il y a plusieurs mission pour le slot */
        else if ($nb_case > 0){
            $i = 0;

            /*  on cheque le premier creneaux entre la premiere mission est le debut du slot */
            /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
            $this->set_calcul_travel_data(null, $mission_array[0]->address_id, $slot_to_check, $mission_array[0]->start);
            $this->check_if_slot_is_ok($mission_array[0]->start, $slot_to_check->start_slot, $slot_to_check, $slot_available_table);

            /* on parcours toutes les missions et on check si la durée du slot rentre entre deux missions est suffisante */

            while ($i < $nb_case) {

                /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
                $this->set_calcul_travel_data($mission_array[$i]->address_id, $mission_array[$i + 1]->address_id, $slot_to_check, $mission_array[$i]->end);

                $this->check_if_slot_is_ok($mission_array[$i + 1]->start, $mission_array[$i]->end, $slot_to_check, $slot_available_table);
                if (parseStr::compare_time_strict_equal($mission_array[$i + 1]->start, $mission_array[$i]->start) && parseStr::compare_time_strict_equal($mission_array[$i]->end, $mission_array[$i + 1]->end)){
                    $nb_case--;
                    unset($mission_array[$i + 1]);
                    $mission_array = array_values($mission_array);
                } else {
                    $i++;
                }
            }

            /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
            $this->set_calcul_travel_data($mission_array[$i]->address_id, null, $slot_to_check, $mission_array[$i]->end);

            /* on check si la durée du slot rentre entre la fin de la derniere mission et le slot max, et si la mission fini au slot max on prend en compte le slot max + durée de la missions */
            $this->check_if_slot_is_ok($max_end, $mission_array[$i]->end, $slot_to_check, $slot_available_table);

            /* si la fin de la derniere missions == le start max (on verifie que la durée finale ne dépasse pas une valeur arbitraire */
            if (parseStr::compare_time_equal($mission_array[$i]->end, $slot_to_check->start_max)){
                if (!parseStr::compare_time_strict_equal($max_end, $this->conf[$slot_to_check->day]['end'])){

                    /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
                    $this->set_calcul_travel_data($mission_array[$i]->address_id, null, $slot_to_check, $mission_array[$i]->end);

                    $slot_to_check->start_slot = $slot_to_check->start_max;
                    $slot_to_check->end_slot = $max_end;
                    $slot_available_table[] = $slot_to_check;
                }
            }
        }


        /* si il y a une mission seulement */
        else {
            /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
            $this->set_calcul_travel_data( null, $mission_array[0]->address_id, $slot_to_check, $mission_array[0]->start);

            /* on verifie le debut de la missions avec le debut du slot si la durée fit */
            $this->check_if_slot_is_ok($mission_array[0]->start, $slot_to_check->start_slot, $slot_to_check, $slot_available_table);

            /* on initialise les id des adresses correspondant au mission avant et apres le creneaux a verifier */
            $this->set_calcul_travel_data($mission_array[0]->address_id, null, $slot_to_check, $mission_array[0]->end);

            /* on verifie la fin de la missions avec la fin du slot si la durée fit */
            $this->check_if_slot_is_ok($max_end, $mission_array[0]->end, $slot_to_check, $slot_available_table);
        }
    }

    /**
     * @param $data @var array[
     *                          @param start_slot @var time
     *                          @param end_slot @var time
     *                          @param duration @var time
     *                          @param start_max @var time
     *                          @param end_min @var time
     *                          @param frequency @var object[
     *                                                          @param frequency @var string
     *                                                          @param frequencyValue @var int
     *                                                          @param frequencyAuto @var string
     *                                                     ]
     *                          @param nb_duration @var int
     *                          @param total_duration @var time
     *                          @param day @var int between 0 and 6
     *                          @param address_token @var string
     *                        ]
     * @return int
     */
    private function get_fdc_dispo($data){
        $extend = "";
        $var_sql = [];
        $this->get_extend_and_sql_data($data, $extend, $var_sql);
        try {
            $fdc_tab = $this->db->query("SELECT
                                        provider.user_id,
                                        `day`,
                                        CAST('" . $data['duration'] . "' AS TIME) AS duration,
                                        IF(dispo.start >= '". $data['start_slot'] ."', dispo.start, '". $data['start_slot'] ."') AS start_slot,
                                        IF(dispo.end <= '". $data['end_slot'] ."', dispo.end, '". $data['end_slot'] ."') AS end_slot,
                                        CAST(SUBTIME(IF(dispo.end <= '". $data['end_slot'] ."', dispo.end, '". $data['end_slot'] ."'), '". $data['duration'] ."') AS TIME) AS start_max,
                                        CAST(ADDTIME(IF(dispo.start >= '". $data['start_slot'] ."', dispo.start, '". $data['start_slot'] ."'), '". $data['duration'] ."') AS TIME) AS end_min,
                                        CONCAT(CONCAT(U.firstname, ' '),U.lastname) AS name,
                                        U.user_token AS user_token,
                                        (SELECT A.`id` FROM `address` A WHERE A.`token` = '" . $data['address_token'] ."') AS customer_slot_address_id
                                    FROM
                                        provider
                                    INNER JOIN user U ON
                                        U.id = provider.user_id
                                    INNER JOIN dispo ON provider.user_id = dispo.user_id
                                    WHERE
                                            provider.deleted = ?
                                                    AND TIMEDIFF(dispo.end, dispo.start) >= ? 
                                                        AND dispo.day = ? AND dispo.start <= ? 
                                                            AND dispo.end >= ?
                                                                " . $extend
                , $var_sql)->fetchAll();
        } catch (PDOException $e) {
            App::logPDOException($e, 1);
            return -1;
        }

        return $fdc_tab;
    }

    /**
     * @param $data @var array[
     *                          @param start_slot @var time
     *                          @param end_slot @var time
     *                          @param duration @var time
     *                          @param start_max @var time
     *                          @param end_min @var time
     *                          @param frequency @var object[
     *                                                          @param frequency @var string
     *                                                          @param frequencyValue @var int
     *                                                          @param frequencyAuto @var string
     *                                                     ]
     *                          @param nb_duration @var int
     *                          @param total_duration @var time
     *                          @param day @var int between 0 and 6
     *                          @param address_token @var string
     *                        ]
     * @param $slot_available
     * @return int
     */

    private function get_valid_slot_array($data, &$slot_available, $fdc_dispo_tab){

        /* on verifie pour chaque slots de dispo fdc si il a une missions qui empéche , et si oui, on verifie si on a la duration qui rentre dans le slot restant */

        foreach ($fdc_dispo_tab as $index_slot => $slot_to_check) {
            if (parseStr::compare_time_strict_equal(parseStr::subtract_the_time($slot_to_check->end_slot, $slot_to_check->start_slot), $slot_to_check->duration)) {
                if ($this->check_if_conf($slot_to_check->start_slot, $slot_to_check->end_slot, $slot_to_check->day, $slot_to_check->duration)) {

                    $data_Planning_Slot = $this->init_data_slot($slot_to_check, $data);
                    if (($mission_array = $this->getMissionWeek($data_Planning_Slot, $data['type'])) == -1)
                        return -1;
                    $this->check_slot_validity($mission_array, clone $slot_to_check, $slot_available, $slot_to_check->end_slot);

                }
            }
        }
    }

    /**
     * @param $data
     * @param $total_duration
     * @param $nb_duration
     * @param $slot_available
     * @return int
     */
    /* this function fill slot_available with slot of provider available for the specific disponibility */
    private function find_valid_provider($data, $total_duration, $nb_duration, &$slot_available){
        if ($data['type'] == 1 || $data['type'] == 2)
            $total_duration = parseStr::divide_time($total_duration, 2);

        $slot_table = $this->fill_param_table($data['dispo_client_tab'], $data['duration']);

        foreach ($slot_table as $index => $slot) {
            $slot_data = [
                "end_slot" => $slot['end_slot'],
                "start_slot" => $slot['start_slot'],
                "duration" => $slot['duration'],
                "end_min" => $slot['end_min'],
                "frequency" => $data['frequency'],
                "nb_duration" => $nb_duration,
                "total_duration" => $total_duration,
                "day" => $slot['day'],
                "start_max" => $slot['start_max'],
                "address_token" => $data['address_token'],
                "type" => $data['type'],
                "start_week" => $data['date'],
                "end_week" => date('Y-m-d', strtotime( $data['date'] . " +7 days"))
            ];
            $fdc_dispo_tab = $this->get_fdc_dispo($slot_data);
            if ($fdc_dispo_tab == -1)
                return -1;
            if ($this->get_valid_slot_array($slot_data, $slot_available, $fdc_dispo_tab) == -1)
                return -1;
        }
    }

    /* this function modify the xx:xx:xx-xx:xx:xx index format into a number for the fits algorithm*/
    private function change_index_event_array($duration_tab){
        $new_index_slot = 0;
        $tmp_tab = [];
        foreach ($duration_tab as $index_day => $day){
            /* we remove the duplicates values due to the same hours slot */
            $day = array_unique($day, SORT_REGULAR);
            foreach ($day as $slot_tab_index => $slot_tab){
                foreach ($slot_tab as $index_slot => $slot){
                    $tmp_tab[$index_day][$new_index_slot][$index_slot] = $slot;
                }
                $new_index_slot++;
            }
        }

        return $tmp_tab;
    }

    private function fill_calendar_event_array($duration, $item){
        return array(
            "duration" => $duration,
            "travel_before" => $item->travel_before,
            "travel_after" => $item->travel_after,
            "start_slot" => $item->start_slot,
            "end_slot" => $item->end_slot,
            "day" => $item->day
        );
    }

    /**
     * @param $data
     * @param $slot_by_fdc
     * @param $nb_duration
     * @return array
     */
    private function construct_calendar_event_array($data, $slot_by_fdc, $nb_duration){
        $valid_fdc = [];

        /* format the available slot by day and hours first sort */
        foreach ($slot_by_fdc as $key => $slot_fdc) {
            $duration_tab = [];
            /* we try to fit all duration */
            foreach ($data['duration'] as $index => $duration) {
                $ok = 0;

                foreach ($slot_fdc as $index_table => $item) {
                    if ($duration == $item->duration) {
                        $ok = 1;

                        $duration_tab[$item->day][$item->start_slot.'-'.$item->end_slot][$index] = $this->fill_calendar_event_array($duration, $item);
                    }
                }
                /* if one of the duration dont fit, exit we cant use this provider*/
                if (!$ok) {
                    break;
                }
            }
            /* if we can fit all duration and we can fit those duration in different day then we add the provider to the final tab */
            if ($ok && count($duration_tab) >= $nb_duration) {
                /* last format to put the right index */
                $tmp_tab = $this->change_index_event_array($duration_tab);

                $this->fill_business_hours($tmp_tab, $slot_fdc, $valid_fdc);

                if ($tmp_tab) {
                    $valid_fdc[0][$slot_fdc[0]->name . '/' . $slot_fdc[0]->user_token] = $tmp_tab;
                }
            }
        }

        return $valid_fdc;
    }

    /* format best_fits to events in array (fullcalendar format) */
    private function format_best_fits($best_fits, $data){
        $rgb_tab = ["rgb(255,255,0)", "rgb(0,255,255)", "rgb(255,0,255)", "rgb(125,0,255)",
            "rgb(0,255,0)", "rgb(0,0,255)", "rgb(255, 0, 125)"];
        $weekDay = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $array_best_fits = [];

        foreach ($best_fits as $index_slot => $slot) {
            foreach ($slot as $index_fit => $fit) {
                $start = $fit['start_slot'];
                $end = parseStr::sum_the_time_no_restrict($fit['start_slot'], $fit['duration']);

                if ($fit['travel_after'] != 0){
                    $start = parseStr::subtract_the_time($fit['end_slot'], $fit['duration']);
                    $end = $fit['end_slot'];
                }
                if ($fit['travel_before'] != 0){
                    $start = $fit['start_slot'];
                    $end = parseStr::sum_the_time_no_restrict($fit['start_slot'], $fit['duration']);
                }
                $next_day = parseStr::get_next_week_day($data['date'], $weekDay[$fit['day']]);
                $start = $next_day . ' ' . $start;
                $end = $next_day . ' ' . $end;

                $tmp = [
                    "rendering" => "background",
                    "start" => $start,
                    "end" => $end,
                    "color" => $rgb_tab[$index_fit],
                    "allday" => false
                ];
                $array_best_fits[$index_slot][] = $tmp;
            }
        }
        return $array_best_fits;
    }

    /**
     * @param $data @var array[
     *                          @param frequency @var object[
     *                                                          @param frequency @var string
     *                                                          @param frequencyValue @var int
     *                                                          @param frequencyAuto @var string
     *                                                      ]
     *                          @param duration @var time
     *                          @param address_token @var string
     *                          @param user_token @var string
     *                          @param type @var int
     *                          @param date @var string
     *                          @param dispo_client_tab @var array[
     *                                                              @param day @var array[
     *                                                                                      @param start @var time
     *                                                                                      @param end @var time
     *                                                                                      @param day @var int between 0 and 6
     *                                                                                   ]
     *                                                            ]
     *                         ]
     * @return array
     */

    public function search_provider($data){
        $slot_available = [];
        $nb_duration = count($data['duration']);
        /* get all the dispo and order them by user_id and day  */

        $this->sort_duration($data['duration']);
        $total_duration = parseStr::count_total_time($data['duration']);

        if (($this->find_valid_provider($data, $total_duration, $nb_duration, $slot_available)) == -1)
            return -1;

        /* we calculate the travel time before and after the potential future mission for each slot */
        $this->calculate_travel_time($slot_available);

        /* we group each slot by provider */
        $slot_by_fdc = $this->group_slot_by_fdc($slot_available);

        /* we format the result of valid provider for determine the best affection possible */
        $valid_fdc = $this->construct_calendar_event_array($data, $slot_by_fdc, $nb_duration);

        /* we check if the number of provider available is greater than 0 to prevent the process on a empty array */
        if (sizeof($valid_fdc) != 0) {
            $best_fits = $this->computeBestAffectation($valid_fdc[0], $nb_duration);
            $best_fits = $this->format_best_fits($best_fits, $data);
            $valid_fdc[2] = $best_fits;
        }

        return $valid_fdc;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////COMPUTE BEST FIST PART//////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    /* This function contains all the steps to return the best fits for each housemaid */
    private function computeBestAffectation($fdc_data, $nb_duration)
    {
        $all_fdc_matrix = $this->formatTravelDurationData($fdc_data);

        $all_best_fits = array();
        foreach($all_fdc_matrix as $fdc_name => $fdc_matrix){

            $hungarian_formatted_mat = $this->formatHungarianMatrix($fdc_matrix);
            $best_curr_fit = $this->customHungarian($hungarian_formatted_mat);
            $processed_best_fit = $this->processHungarianResult($best_curr_fit, $nb_duration);

            $all_best_fits[$fdc_name] = $this->gatherBestSlots($processed_best_fit, $fdc_matrix);
        }

        return $all_best_fits;
    }

    /* This function computes the value used by the hungarian algorithm */
    /* For now, the travel time is used as reference value */
    /* Later on, this value could be changed for a %fit per slot or any integer */
    /* We are interested in the minimum travel time for now, so we minimify the matrix */
    /* If later we need the maximum value (ex : best fit %), the algorithm needs an extra step */
    /* see : https://stackoverflow.com/questions/17520691/can-i-use-the-hungarian-algorithm-to-find-max-cost */
    /* see : https://en.wikipedia.org/wiki/Hungarian_algorithm */
    private function formatTravelDurationData($fdc_data)
    {
        foreach($fdc_data as $fdc_name => $fdc_datum){
            foreach ($fdc_datum as $day_nb => $day){
                foreach($day as $slot_nb => $slot){
                    foreach($slot as $duration_nb => $duration){
                        $before = $duration['travel_before'] ? $duration['travel_before'] : 0;
                        $after = $duration['travel_after'] ? $duration['travel_after'] : 0;

                        $mission_travel_time = $before + $after;
                        $fdc_data[$fdc_name][$day_nb][$slot_nb][$duration_nb]['slot_cost'] = $mission_travel_time;
                    }
                }
            }
        }
        return $fdc_data;
    }

    /* This function returns all the slots data for the corresponding fits obtained with the hungarian algorithm */
    private function gatherBestSlots($fits, $fdc_matrix)
    {
        $best_slots = array();

        foreach($fits as $fit){
            if($this->getBestFdcSlot($fit, $fdc_matrix)){
                $best_slots[] = $this->getBestFdcSlot($fit, $fdc_matrix);
            }
        }

        return $best_slots;
    }

    /* This function returns the slot data for the corresponding given fit */
    private function getBestFdcSlot($fit, $fdc_matrix)
    {
        foreach($fdc_matrix as $all_slots){
            foreach ($all_slots as $slot_id => $slot){
                if($slot_id == $fit[1]){
                    foreach ($slot as $duration_id => $duration){
                        if($duration_id == $fit[0]){
                            return $duration;
                        }
                    }
                }
            }
        }
        return false;
    }

    /* Function used to initialize the matrix with travel durations */
    private function formatHungarianMatrix($fdc_matrix)
    {
        $hung_matrix = $this->initializeMatrix($fdc_matrix);
        foreach ($fdc_matrix as $day_matrix){

            if(sizeof($day_matrix) > 1){
                $day_matrix = $this->processSameDaySlots($day_matrix);
            }

            foreach($day_matrix as $slot_id => $curr_slot){
                foreach ($curr_slot as $duration_id => $duration){
                    $hung_matrix[$slot_id][$duration_id] = $duration['slot_cost'];
                }
            }
        }
        return $hung_matrix;
    }

    /* The hungarian algorithm solves an affectation problem. It will return the best column for each row to minimize the distance and travel time */
    /* Since it is forbidden to have two mission in the same day but there can be more than 1 column for each day */
    /* Here we anticipate the algorithm behavior so that only one mission per day can be put */
    private function processSameDaySlots($day_matrix)
    {
        $arr_values = array();
        foreach ($day_matrix as $day => $slot){
            foreach ($slot as $duration_id => $duration){
                $arr_values[$duration_id][$day] = $duration;
            }
        }

        $min_cost = 999999;
        $min_slot = null;
        foreach ($arr_values as $key => $slot){
            if(sizeof($slot) < 2){
                continue;
            }
            foreach ($slot as $sl_key => $dur){
                if($dur['slot_cost'] <= $min_cost){
                    $min_cost = $dur['slot_cost'];
                    $min_slot = $sl_key;
                }
            }
        }

        foreach ($day_matrix as $day => $slot){
            foreach ($slot as $duration_id => $duration){
                if($day == $min_slot){
                    continue;
                }
                $day_matrix[$day][$duration_id]['slot_cost'] = 999999;
            }
        }
        return $day_matrix;
    }

    /* This function initialized a matrix of size mission*slots with 999999 (INF) to be used by the hungarian algorithm */
    /* The values will be overwritten with travel times, the remaining values at 999999 won't be taken as best fit */
    private function initializeMatrix($fdc_matrix)
    {
        $max_slot_id = 0;
        $max_duration_id = 0;
        $initializedMatrix = array();

        foreach ($fdc_matrix as $day_matrix) {
            foreach ($day_matrix as $slot_id => $curr_slot) {
                foreach ($curr_slot as $duration_id => $duration) {
                    $max_slot_id = ($slot_id > $max_slot_id) ? $slot_id : $max_slot_id;
                    $max_duration_id = ($duration_id > $max_duration_id) ? $duration_id : $max_duration_id;
                }
            }
        }

        for($i = 0; $i <= $max_slot_id; $i++){
            for($j = 0; $j <= $max_duration_id; $j++){
                $initializedMatrix[$i][$j] = 999999;
            }
        }
        return $initializedMatrix;
    }

    /* This function is used to return only the fits for the asked mission */
    /* Since the hungarian algorithm uses a squared matrix, some temporary rows/columns were added previously */
    private function processHungarianResult($hung_matrix, $mission_nb)
    {
        $best_fit = array();
        for($i = 0; $i < $mission_nb; $i++){
            $fit_union = array();
            $fit_union[0] = $i;
            $fit_union[1] = $hung_matrix[$i];

            $best_fit[] = $fit_union;
        }
        return $best_fit;
    }

    /* The custom hungarian algorithm is used to return the best fit in the calendar for each given mission */
    /* The parameter is a matrix which contains each available slot as column and each asked mission as row */
    /* The complexity of this algorithm is O(n^3) */
    /* Useful online solver for testing : http://www.hungarianalgorithm.com/solve.php */
    private function customHungarian($matrix)
    {
        $h = count($matrix);
        $w = count($matrix[0]);

        if ($h < $w) {
            for ($i = $h; $i < $w; ++$i) {
                $matrix[$i] = array_fill(0, $w, 999999);
            }
        } elseif ($w < $h) {
            foreach ($matrix as &$row) {
                for ($i = $w; $i < $h; ++$i) {
                    $row[$i] = 999999;
                }
            }
        }
        $h = $w = max($h, $w);
        $u   = array_fill(0, $h, 0);
        $v   = array_fill(0, $w, 0);
        $ind = array_fill(0, $w, -1);

        foreach (range(0, $h - 1) as $i) {
            $links   = array_fill(0, $w, -1);
            $mins    = array_fill(0, $w, 999999);
            $visited = array_fill(0, $w, false);
            $markedI = $i;
            $markedJ = -1;
            $j       = 0;
            while (true) {
                $j = -1;
                foreach (range(0, $h - 1) as $j1) {
                    if (!$visited[$j1]) {
                        $cur = $matrix[$markedI][$j1] - $u[$markedI] - $v[$j1];
                        if ($cur < $mins[$j1]) {
                            $mins[$j1]  = $cur;
                            $links[$j1] = $markedJ;
                        }
                        if ($j == -1 || $mins[$j1] < $mins[$j]) {
                            $j = $j1;
                        }
                    }
                }
                $delta = $mins[$j];
                foreach (range(0, $w - 1) as $j1) {
                    if ($visited[$j1]) {
                        $u[$ind[$j1]] += $delta;
                        $v[$j1] -= $delta;
                    } else {
                        $mins[$j1] -= $delta;
                    }
                }
                $u[$i] += $delta;
                $visited[$j] = true;
                $markedJ = $j;
                $markedI = $ind[$j];
                if ($markedI == -1) {
                    break;
                }
            }

            while (true) {
                if ($links[$j] != -1) {
                    $ind[$j] = $ind[$links[$j]];
                    $j       = $links[$j];
                } else {
                    break;
                }
            }
            $ind[$j] = $i;
        }
        $result = array();
        foreach (range(0, $w - 1) as $j) {
            $result[$j] = $ind[$j];
        }
        return $result;
    }

    /* Unused but can be useful in case we need to look for a given key inside a multidimensional array */
    function multiKeyExists($arr, $key)
    {
        if (array_key_exists($key, $arr)) {
            return true;
        }

        foreach ($arr as $element) {
            if (is_array($element)) {
                if ($this->multiKeyExists($element, $key)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* Unsued but can be useful to initialize a matrix for testing the hungarian algorithm */
    public function initMatrix($column, $row)
    {
        $matrix = array();
        for ($i = 0; $i < $row; $i++) {
            for ($j = 0; $j < $column; $j++) {
                $matrix[$i][$j]= rand(5, 40);
            }
        }
        return $matrix;
    }

}