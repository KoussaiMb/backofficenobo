<?php


class cardex
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function insertListReference($listname, $url, $value)
    {
        $this->db->query_log("INSERT INTO list_reference(`list_name`, `url`, `value`) VALUES (?, ?, ?) ", [$listname, $url, $value]);
        return $this->db->lastInsertId();
    }

    public function deleteFromListReferenceById($id)
    {
        $rowcount = $this->db->query_log("DELETE FROM list_reference WHERE id = ?", [$id]);
        if ($rowcount)
            return $rowcount->rowCount();
        return null;
    }

    public function getAllItemsOfListByName($listname)
    {
        return $this->db->query_log("SELECT * FROM `list_reference` WHERE list_name = ?", [$listname])->fetchAll();
    }

    public function getProductLocalizationInfoByAddressToken($address_token)
    {
        $ret = $this->db->query('
SELECT 
LR.`url` as lr_url, LR.`value` as lr_value, 
LP.`url` as lp_url, LP.`value` as lp_value, 
P.`description`, P.`id`
FROM `product_localization` P
INNER JOIN `list_reference` LR ON LR.`id` = P.`lr_room_id`
INNER JOIN `list_reference` LP ON LP.`id` = P.`lr_product_id`
WHERE P.`address_id` = (SELECT A.`id` FROM `address` A WHERE A.`token` = ?)'
            ,[$address_token])->fetchAll();
        if ($ret === false)
            return false;
        return $ret;
    }

    public function getDescriptionByAddressTokenForGeneratePdf($address_id)
    {
        return $this->db->query("SELECT L.`value` as room_name, L.`url` as room_url, C.`description` as room_description
    FROM `cardex` C
    INNER JOIN `list_reference` L ON (C.`list_reference_id` = L.`id`)
    WHERE L.`list_name` != 'productsList' AND C.`address_id` = ? ;", [$address_id])->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getDistinctListElements($listname)
    {
        return $this->db->query_log("SELECT DISTINCT id, url, `value` FROM list_reference WHERE list_name = ?", [$listname])->fetchAll();
    }


    public function updateListReferenceById($id, $url, $val)
    {
        $ret = $this->db->query("UPDATE `list_reference` SET url = ?, `value` = ? WHERE id = ?", [$url, $val, $id])->rowCount();
        if ($ret == false)
            return 0;
        return 1;
    }

    public function deleteCardexById($id)
    {
        $ret = $this->db->query("DELETE FROM `mp_cardex` WHERE `id` = ?", [$id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function insertProductLocalization($data)
    {
        $ret = $this->db->query_log("INSERT INTO `product_localization` 
                                  (`address_id`, `lr_room_id`, lr_product_id, `description`)
                                  VALUES (?, ?, ?, ?)",
            [
                $data['address_id'],
                $data['lr_room_id'],
                $data['lr_product_id'],
                $data['description']
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function deleteProductLocalizationById($product_localization_id)
    {
        $ret = $this->db->query_log("DELETE FROM product_localization WHERE id = ?", [$product_localization_id]);
        if ($ret == false)
            return false;
        return $ret->rowCount();
    }


    public function deleteAllTasksByRoomId($roomId)
    {
        $ret = $this->db->query_log("DELETE FROM `cardex` WHERE `list_reference_id` = ?", [$roomId])->rowCount();
        if ($ret == false)
            return 0;
        return 1;
    }

    public function deleteProductLocalizationByProductId($productId)
    {
        $ret = $this->db->query_log("DELETE FROM product_localization WHERE lr_product_id = ?", [$productId]);
        if ($ret)
            return $ret->rowCount();
        return null;
    }

    public function deleteProductLocalizationByRoomId($productId)
    {
        $ret = $this->db->query_log("DELETE FROM product_localization WHERE lr_room_id = ?", [$productId]);
        if ($ret)
            return $ret->rowCount();
        return null;
    }

    /*
    * TODO:del this comment
    * new cardex functions
    */

    /**
     * @param $name
     * @param $l_maintenance_type_id
     * @return mixed
     */
    public function addCardex($l_maintenance_type_id)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_cardex` SET `l_maintenanceType_id` = ?", [$l_maintenance_type_id]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    /**
     * @return array|bool
     *
     * COUNT is forcing the check to return an empty array if there is no cardex
     */
    public function getAll()
    {
        $ret = $this->db->query_log("SELECT 
                                        c.*, 
                                        l.field as maintenance_type,                                        
                                        (SELECT COUNT(*) FROM `mp_cardex_task` mpc WHERE mpc.mp_cardex_id = c.id) as task_nb
                                        FROM `mp_cardex` c 
                                        INNER JOIN `list` l ON c.l_maintenanceType_id = l.id
                                        WHERE l.deleted = 0 and c.deleted = 0");
        if ($ret === false)
            return false;
        $all_cardex = $ret->fetchAll();
        if ($all_cardex[0]->id === null)
            return array();
        return $all_cardex;
    }

    public function getCardexById($cardex_id)
    {
        $ret = $this->db->query_log("SELECT 
                                        c.*, 
                                        l.`field` as maintenance_type,
                                        COUNT(mpc.id) as task_nb
                                    FROM `mp_cardex` c
                                    INNER JOIN `list` l ON c.`l_maintenanceType_id` = l.`id`
                                    LEFT JOIN `mp_cardex_task` mpc ON c.id = mpc.mp_cardex_id 
                                    WHERE l.deleted = 0
                                    AND c.`id` = ?", [$cardex_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public  function  getCardexTaskByCardexId($cardex_id)
    {
        $ret = $this->db->query_log("SELECT 
                                        mpc.id, mpc.color_hex,
                                        task.field as task, task.id as task_id,
                                        room.value as room, room.id as room_id, room.url as room_img,
                                        ponderation.field as ponderation, ponderation.id as ponderation_id
                                        FROM `mp_cardex_task` mpc
                                        INNER JOIN `list` task ON task.id = mpc.l_task_id
                                        INNER JOIN `list_reference` room ON room.id = mpc.lr_room_id
                                        INNER JOIN `list` ponderation ON ponderation.id = mpc.l_ponderation_id
                                        WHERE mp_cardex_id = ? ORDER BY room ASC", [$cardex_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function deleteCardexTask($cardex_task_id) {
        $ret =  $this->db->query("DELETE FROM `mp_cardex_task` WHERE `id` = ?", [$cardex_task_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function addCardexTask($data) {
        $ret = $this->db->query_log("
                                    INSERT INTO 
                                    `mp_cardex_task`
                                    SET 
                                    `color_hex` = ?,
                                    `l_ponderation_id` = ?,
                                    `lr_room_id` = ?,
                                    `l_task_id` = ?,
                                    `mp_cardex_id` = ?
                                    ", [
                                        $data['color_hex'],
                                        $data['l_ponderation_id'],
                                        $data['lr_room_id'],
                                        $data['l_task_id'],
                                        $data['cardex_id']
        ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function getMaintenanceTypeNotUsed() {
        $ret = $this->db->query_log("SELECT
                                         l.`id`,
                                         l.`field`
                                         FROM `list` l
                                         LEFT JOIN `mp_cardex` c ON l.`id` = c.`l_maintenanceType_id` 
                                         WHERE
                                         `listName` = 'maintenanceType'
                                         AND 
                                         c.deleted IS NULL");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }
}
