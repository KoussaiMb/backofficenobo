<?php

class reception
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getFormCustomerInfoByWaitingListId($waiting_list_id)
    {
        return $this->db->query_log("SELECT u.id as user_id, u.firstname, u.lastname, u.email, u.user_token, u.phone, u.job_id, u.maritalStatus_id, u.gender_id, a.*
                                          FROM `waiting_list` w, address a, `user` u, customer c
                                          WHERE w.address_id = a.id
                                          AND a.user_id = u.id
                                          AND c.id = w.customer_id
                                          AND w.id = ?", [$waiting_list_id])->fetch();
    }

    public function getMissionDurationByAddressId($w_id)
    {
        return $this->db->query_log("SELECT md.*, a.recurrence_id FROM `mission_duration` md, waiting_list w, address a
                                          WHERE w.address_id = md.address_id
                                          AND a.id = w.address_id
                                          AND w.id = ? ", [$w_id])->fetchAll();
    }

    public function updateRecurrenceType($w_id, $rec_id, $nbHours)
    {
        if ($rec_id == null)
            $rec_id = null;
        if ($nbHours == null)
            $nbHours = null;

        $ret = $this->db->query_log("UPDATE 
                                        `address` a 
                                    INNER JOIN waiting_list w ON a.id = w.address_id 
                                    SET 
                                        a.`recurrence_id` = ?, 
                                        a.`nbHours` = ? 
                                    WHERE 
                                        w.`id` = ?", [$rec_id, $nbHours, $w_id]);
        if ($ret == false)
            return false;
        return $ret->rowCount();
    }

    public function updateAcquisitionByCustomerId($customer_id, $acq_id)
    {
        if ($acq_id == null)
            $acq_id = null;
        $ret = $this->db->query_log("UPDATE `customer` SET `l_acquisition_id` = ? WHERE `id` = ?", [$acq_id, $customer_id]);
        if($ret === false){
            return 0;
        }
        return 1;
    }

    public function updateUserByToken($data)
    {
        $user_values = [
            empty($data['email']) ? null : $data['email'],
            empty($data['lastname']) ? null : $data['lastname'],
            empty($data['firstname']) ? null : $data['firstname'],
            empty($data['phone']) ? null : $data['phone'],
            empty($data['maritalStatus']) ? null : $data['maritalStatus'],
            empty($data['gender_id']) ? null : $data['gender_id'],
            $data['user_token']
        ];

        $ret =  $this->db->query_log("UPDATE `user` SET `email` = ?, `lastname` = ?, `firstname` = ?, `phone` = ?, `maritalStatus_id` = ?, `gender_id` = ? WHERE `user_token` = ?", $user_values);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function updateAddressInfoById($data){
        $address_values = [
            empty($data['surface']) ? 0 : $data['surface'],
            empty($data['roomNb']) ? 0 : $data['roomNb'],
            empty($data['waterNb']) ? 0 : $data['waterNb'],
            empty($data['wcNb']) ? 0 : $data['wcNb'],
            empty($data['l_pet_id']) ? 0 : $data['l_pet_id'],
            empty($data['childNb']) ? 0 : $data['childNb'],
            empty($data['have_key']) ? 0 : $data['have_key'],
            empty($data['childNb']) ? 0 : $data['childNb'],
            empty($data['peopleHome']) ? 0 : $data['peopleHome'],
            $data['address_id']
        ];

        $ret =  $this->db->query_log("UPDATE `address`
                                        SET
                                            `surface` = ?,
                                            `roomNb` = ?,
                                            `waterNb` = ?,
                                            `wcNb` = ?,
                                            `l_pet_id` = ?,
                                            `childNb` = ?,
                                            `have_key` = ?,
                                            `childNb` = ?,
                                            `peopleHome` = ?
                                        WHERE
                                            `id` = ?",
                                        $address_values);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateAddressById($data)
    {
        $address_values = [
            empty($data['address']) ? null : $data['address'],
            empty($data['lat']) ? null : $data['lat'],
            empty($data['lng']) ? null : $data['lng'],
            empty($data['country']) ? null : $data['country'],
            empty($data['zipcode']) ? null : $data['zipcode'],
            empty($data['city']) ? null : $data['city'],
            empty($data['have_ironing']) ? 0 : $data['have_ironing'],
            $data['address_id']
        ];

        $ret =  $this->db->query_log("UPDATE `address`
                                        SET
                                            `address` = ?,
                                            `lat` = ?,
                                            `lng` = ?,
                                            `country` = ?,
                                            `zipcode` = ?,
                                            `city` = ?,
                                            `have_ironing` = ?
                                        WHERE
                                            `id` = ?",
                                        $address_values);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function updateBuildingAccessInfo($data)
    {
        $access_values = [
            empty($data['batiment']) ? null : $data['batiment'],
            empty($data['digicode']) ? null : $data['digicode'],
            empty($data['digicode_2']) ? null : $data['digicode_2'],
            empty($data['doorBell']) ? null : $data['doorBell'],
            empty($data['door']) ? null : $data['door'],
            empty($data['floor']) ? null : $data['floor'],
            $data['address_id']
        ];
        $ret = $this->db->query_log("UPDATE `address` SET `batiment` = ?, `digicode` = ?, `digicode_2` = ?, `doorBell` = ?, `door` = ?, `floor` = ? WHERE `id` = ?", $access_values);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function insertDuration($address_id, $duration, $locked)
    {
        $this->db->query_log("INSERT INTO `mission_duration` (`address_id`, `duration`, `locked`) VALUES (?, ?, ?)",
            [$address_id, $duration, $locked]);
        return $this->db->lastInsertId();
    }

    public function deleteMissionsByWaitingListId($w_id)
    {
        $ret =  $this->db->query_log("DELETE m FROM mission_duration m
                                        INNER JOIN waiting_list w ON w.address_id = m.address_id
                                        WHERE w.id = ?",[$w_id]);
        if($ret == false){
            return 0;
        }
        return true;
    }

}
