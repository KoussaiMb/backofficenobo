<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 10/11/2016
 * Time: 15:51
 */
class parseJson
{
    private $code;
    private $message = null;
    private $data = null;

    static function success($msg = "Everything worked successfully", $data = null){
        return new parseJson('200', $msg, $data);
    }

    static function     error($msg = "Your request is weird !", $data = null){
        return new parseJson('400', $msg, $data);
    }

    public function __construct($code, $message, $data)
    {
        $this->code = $code;
        if ($message !== null )
            $this->message = $message;
        if ($data !== null)
            $this->data = $data;
    }

    public function printJson(){
        echo json_encode([
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data
        ], JSON_PRETTY_PRINT);
        exit();
    }
}