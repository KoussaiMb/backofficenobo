<?php



class fullCalendar
{

    //////////////////////////////// CHECK PART FOR prestation ///////////////////////////////
    /// ///
    ///
    ///

    private $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getAgendaInInterval($data){
        $extend = "";
        $var = [
            $data['user_token'],
            $data['end'],
            $data['end'],
            $data['end']
        ];
        if (($data['type'] == 1)){
            $week_nb = ((bool)!(date('W', strtotime($data['start_date'])) % 2) + 1);
            $extend = "AND (type = 0 OR type = 3 OR type = " . $week_nb . ")";
        }
        return $this->db->query_log("SELECT ag.*, a.address, a.subway_id,
                                  a.batiment AS batiment, a.digicode AS digicode_1, a.digicode_2 AS digicode_2,
                                  a.floor AS etage, a.door AS porte, a.doorBell AS interphone,
                                  u.firstname, u.lastname, u.user_token
                            FROM agenda ag
                            INNER JOIN address a ON a.id = ag.address_id
                            INNER JOIN `user` u ON u.id = ag.user_id
                            WHERE ag.pending = 0 AND ag.deleted = 0
                            AND ag.provider_id =  (
                                   SELECT id FROM provider p WHERE p.user_id =(
                                      SELECT id FROM `user` WHERE user_token = ?
                                  )
                              )
                            AND (start_rec >= ? OR (start_rec <= ? AND (end_rec > ? OR ISNULL(end_rec))))" . $extend, $var)->fetchAll();
    }


    public function getAgendaConflictInInterval($data){
        $extend = "";
        if (($data['type'] == 1)){
            $week_nb = ((bool)!(date('W', strtotime($data['date_start'])) % 2) + 1);
            $extend = "AND (type = 0 OR type = 3 OR type = " . $week_nb . ")";
        }
        //WARNING: MAYBE ERASE PREVIOUS CONDITION
        if (empty($data['date_end'])) {
            $req = $this->db->query_log("SELECT AG.*, A.address, A.subway_id, A.batiment AS batiment,
                                          A.digicode AS digicode_1, A.digicode_2 AS digicode_2,
                                          A.floor AS etage, A.door AS porte, A.doorBell AS interphone,
                                          U.firstname, U.lastname, U.user_token
                                        FROM agenda AG
                                        INNER JOIN address A ON A.id = AG.address_id
                                        INNER JOIN `user` U ON U.id = AG.user_id
                                        WHERE AG.day = ?
                                        AND AG.pending = 0
                                        AND AG.deleted = 0
                                        AND AG.provider_id = (
                                            SELECT id FROM provider WHERE provider.user_id = (
                                                SELECT id FROM `user` WHERE user_token = ?
                                            )
                                        )
                                        AND (
                                            ((`start` < ? AND `end` > ?)
                                                OR
                                                  (`start` < ? AND `end` > ?)
                                                OR
                                                  ((? < `start` AND ? > `start`)
                                                      OR
                                                        (? < `end` AND ? > `end`)
                                                  )
                                              )
                                            OR
                                              (`start` = ? AND `end` = ?)
                                        )
                                        AND
                                          start_rec >= ? " . $extend . " ORDER BY `start` ASC",
                                    [$data['dayNumeric'], $data['user_token'],
                                        $data['hours_start'], $data['hours_start'],
                                        $data['hours_end'], $data['hours_end'], $data['hours_start'],
                                        $data['hours_end'], $data['hours_start'], $data['hours_end'],
                                        $data['hours_start'], $data['hours_end'], $data['date_start']]);
        }
        else {
            $req = $this->db->query_log("SELECT AG.*, A.address, A.subway_id, A.batiment AS batiment,
                                            A.digicode AS digicode_1, A.digicode_2 AS digicode_2,
                                            A.floor AS etage, A.door AS porte, A.doorBell AS interphone,
                                            U.firstname, U.lastname, U.user_token
                                        FROM agenda AG
                                        INNER JOIN address A ON A.id = AG.address_id
                                        INNER JOIN `user` U ON U.id = AG.user_id
                                        WHERE AG.day = ? 
                                        AND AG.pending = 0 
                                        AND AG.deleted = 0
                                        AND AG.provider_id = (
                                            SELECT id FROM provider WHERE provider.user_id = (
                                                SELECT id FROM `user` WHERE user_token = ?
                                            )
                                        )
                                        AND (
                                                (
                                                    (`start` < ? AND `end` > ?)
                                                  OR
                                                    (`start` < ? AND `end` > ?)
                                                  OR
                                                    (
                                                          (? < `start` AND ? > `start`)
                                                        OR
                                                          (? < `end` AND ? > `end`)
                                                    )
                                                )
                                            OR
                                              (`start` = ? AND `end` = ?)
                                        )
                                            AND
                                              end_rec <= ?
                                            AND
                                              start_rec >= ? " . $extend . " ORDER BY start ASC",
                                [$data['dayNumeric'], $data['user_token'],
                                    $data['hours_start'], $data['hours_start'], $data['hours_end'],
                                    $data['hours_end'], $data['hours_start'], $data['hours_end'],
                                    $data['hours_start'], $data['hours_end'], $data['hours_start'],
                                    $data['hours_end'], $data['date_end'], $data['date_start']]);
        }
        if ($req !== false)
            return $req->fetchAll();
        else{
            return false;
        }
    }

    public function getAgendaConflict($data){
            if (empty($data['end_date']))
                return $this->db->query_log("SELECT * FROM agenda
                                        WHERE agenda.deleted = 0
                                        AND agenda.day = ? AND id != ?
                                        AND (end_rec IS NULL OR end_rec > ?)
                                        AND provider_id =
                                        (
                                            SELECT id FROM provider WHERE user_id =(SELECT id FROM `user` WHERE user_token = ?)
                                        )
                                        AND
                                        (
                                            (
                                                (`start` < ? AND `end` > ?)
                                              OR
                                                (`start` < ? AND `end` > ?)
                                              OR
                                                (
                                                  (? < `start` AND ? > `start`) OR (? < `end` AND ? > `end`)
                                                )
                                            )
                                          OR
                                            (`start` = ? AND `end` = ?)
                                        )
                                        ORDER BY START ASC", [$data['day'], $data['agenda_id'], $data['start_date'], $data['user_token'] , $data['start'], $data['start'], $data['end'], $data['end'], $data['start'], $data['end'], $data['start'], $data['end'], $data['start'], $data['end']])->fetchAll();
            else
                return $this->db->query_log("SELECT * FROM agenda
                                        WHERE agenda.deleted = 0 AND agenda.day = ?
                                        AND id != ? AND (end_rec IS NULL OR end_rec > ?)
                                        AND end_rec < ? AND provider_id =
                                        (
                                            SELECT id FROM provider WHERE user_id =(SELECT id FROM `user` WHERE user_token = ?)
                                        )
                                        AND
                                        (
                                            (
                                                (`start` < ? AND `end` > ?)
                                              OR
                                                (`start` < ? AND `end` > ?)
                                              OR
                                                (
                                                  (? < `start` AND ? > `start`) OR (? < `end` AND ? > `end`)
                                                )
                                            )
                                          OR
                                            (`start` = ? AND `end` = ?)
                                        )
                                        ORDER BY START ASC", [$data['day'], $data['agenda_id'], $data['start_date'] , $data['end_date'], $data['user_token'] , $data['start'], $data['start'], $data['end'], $data['end'], $data['start'], $data['end'], $data['start'], $data['end'], $data['start'], $data['end']])->fetchAll();
    }

    public function getAgendaById($id){
            $ret = $this->db->query_log("SELECT * FROM agenda WHERE id = ?", [$id]);
            if ($ret === false)
                return false;
            return $ret->fetch();
    }

    public function getAllSlotByUserToken($user_token){
            return $this->db->query_log("
                                SELECT
                                    agenda.*,
                                    agenda.id as agenda_id,
                                    address.token as address_token,
                                    address.address,
                                    address.subway_id,
                                    address.batiment AS batiment,
                                    address.digicode AS digicode_1,
                                    address.digicode_2 AS digicode_2,
                                    address.floor AS etage,
                                    address.door AS porte,
                                    address.doorBell AS interphone,
                                    `user`.firstname,
                                    `user`.lastname,
                                    `user`.user_token
                                FROM
                                  agenda
                                INNER JOIN address ON address.id = agenda.address_id
                                INNER JOIN `user` ON `user`.id = agenda.user_id
                                WHERE agenda.pending = 0
                                AND agenda.deleted = 0
                                AND agenda.provider_id =
                                (
                                  SELECT id FROM provider WHERE provider.user_id =(SELECT id FROM `user` WHERE user_token = ?)
                                )", [$user_token])->fetchAll();
    }

    public function getMetroById($data){
            return $this->db->query_log("SELECT * FROM metro_paris WHERE id = ?", [$data['id']])->fetchAll();
    }


    public function add_vacation_by_address($data){
            $this->db->query_log("INSERT INTO `holidays_address`(`address_id`,`start`,`end`,`deleted`)
                                    VALUES((SELECT A.`id` FROM `address` A WHERE A.`token` = ?), ?, ?, ?)",
                                        [$data['address_token'], $data['start'], $data['end'], 0]);
            return $this->db->lastInsertId();
    }

    public function remove_vacation_by_id($data){
            return $this->db->query_log("DELETE FROM holidays_address WHERE id = ?", [$data['id']]);
    }


    public function get_vacation_by_address($address_token){
        $req = $this->db->query_log('SELECT DATE_FORMAT(`start`, \'%Y-%m-%d\') AS `start`, DATE_FORMAT(`end`, \'%Y-%m-%d\') AS `end`, `id`
        FROM `holidays_address`
        WHERE `address_id` = (SELECT A.`id` FROM `address` A WHERE A.`token` = ?)
        ORDER BY `start` ASC;', [$address_token]);
        if ($req === false)
            return false;
        return $req->fetchAll();
        }

    public function addAgenda($data){
            $ret = $this->db->query_log("INSERT INTO `agenda`(
                              `provider_id`, `user_id`, `address_id`, `title`,
                              `day`, `start`, `end`, `duration`, `type`, `locked`,
                              `pending`, `start_rec`, `end_rec`
                            )
                            VALUES(
                                (SELECT `id` FROM `provider` WHERE `user_id` = (SELECT `id` FROM `user` WHERE `user_token` = ?)),
                                (SELECT `id` FROM `user` WHERE `user_token` = ?),
                                (SELECT `id` FROM `address` WHERE `token` = ?),
                                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                            )", [$data['user_token'], $data['customer_token'], $data['address_token'], $data['title'], $data['day'], $data['start'], $data['end'], $data['duration'], $data['type'], $data['locked'], $data['pending'], $data['start_rec'], $data['end_rec']]);
            if ($ret === false)
                return false;
            return $this->db->lastInsertId();
    }

    public function delAgendaEvent($agenda_id){
        $ret = $this->db->query_log("DELETE FROM `agenda` WHERE `id` = ?", [$agenda_id]);
        if ($ret === false)
            return false;
        return true;
        /*
            return $this->db->query_log("UPDATE `agenda` SET deleted = 1, deleted_at = NOW()
                            WHERE `id` = ?", [$id]);
        */
    }

    public function UpdateAgendaEventPending($data){
            return $this->db->query_log("UPDATE `agenda` SET `pending` = 0, `start` = ?, `end` = ?, `day` = ?, `start_rec` = ?, `end_rec` = ?,
                                      provider_id =(
                                        SELECT id FROM provider WHERE provider.user_id = (
                                          SELECT id FROM `user` WHERE user_token = ?
                                        )
                                      ), agenda.type = ?
                                  WHERE `id` = ?",
                        [$data['start'], $data['end'], $data['day'], $data['start_rec'], $data['end_rec'], $data['user_token'], $data['type'], $data['event_id']]);
    }

    public function PendingAgendaEvent($id){ //check missionID en amont
            return $this->db->query_log("UPDATE agenda SET pending = 1 WHERE `id` = ?", [$id]);
    }


    public function getAllPendingAgenda(){
            $ret = $this->db->query_log("SELECT AG.`type`, AG.`id`, AG.`locked`, AG.`start_rec`, AG.`end_rec`, A.`token` as address_token,
                                          AG.`day`, AG.`provider_id`, U.`user_token`, U.`firstname`, U.`lastname`, AG.`address_id`,
                                          AG.`duration`, AG.`start`, AG.`end`
                                    FROM `agenda` AG
                                    INNER JOIN `customer` C ON AG.`user_id` = C.`user_id`
                                    INNER JOIN `user` U ON U.`id` = C.`user_id`
                                    INNER JOIN `address` A ON AG.`address_id` = A.`id`
                                    WHERE AG.`pending` = 1");
            if ($ret === false)
                return false;
            return $ret->fetchAll();
    }

    public function getAgendaByDay($data)
    {
        if ($data['edit'] == 'single' && ($data['type'] == 1 || $data['type'] == 2)){
            $weeknb = ((bool)!(date('W', strtotime($data['date'])) % 2) + 1);
            $req = $this->db->query_log("SELECT IP.*, A.address, `user`.firstname, `user`.lastname,
                                        A.batiment AS batiment, A.digicode AS digicode_1, A.digicode_2 AS digicode_2,
                                        A.floor AS etage, A.door AS porte, A.doorBell AS interphone, A.subway_id
                                    FROM agenda IP
                                    INNER JOIN address A ON A.id = IP.address_id
                                    INNER JOIN `user` ON `user`.id = IP.user_id
                                    WHERE
                                        IP.provider_id =(
                                            SELECT id FROM provider WHERE provider.user_id =(
                                                SELECT id FROM `user` WHERE user_token = ?
                                            )
                                        )
                                    AND IP.pending = 0 AND IP.deleted = 0 AND IP.id != ? AND IP.day = ?
                                    AND(IP.type = 0 OR IP.type = ". $weeknb .")
                                    AND (end_rec IS NULL OR end_rec > ?)", [$data['user_token'], $data['id'], $data['day'], $data['end_date']]);
            if ($req !== false)
                return $req->fetchAll();
            else{
                return false;
            }
        }
        else if ($data['edit'] == 'single' && ($data['type'] == 0)){
            $req = $this->db->query_log("SELECT IP.*, A.address, `user`.firstname, `user`.lastname,
                                          A.batiment AS batiment, A.digicode AS digicode_1, A.digicode_2 AS digicode_2,
                                          A.floor AS etage, A.door AS porte, A.doorBell AS interphone, A.subway_id
                                    FROM agenda IP
                                    INNER JOIN address A ON A.id = IP.address_id
                                    INNER JOIN `user` ON `user`.id = IP.user_id
                                    WHERE
                                        IP.provider_id =
                                        (
                                            SELECT id FROM provider WHERE provider.user_id =
                                            (
                                                SELECT id FROM `user` WHERE user_token = ?
                                            )
                                        )
                                    AND IP.pending = 0 AND IP.deleted = 0 AND IP.id != ? AND IP.day = ?
                                    AND (end_rec IS NULL OR end_rec >= ?)
                                    AND start_rec <= ?", [$data['user_token'], $data['id'], $data['day'], $data['end_date'], $data['end_date']]);
            if ($req !== false)
                return $req->fetchAll();
            else {
                return false;
            }
        }
        else {
            $req = $this->db->query_log("SELECT IP.*, A.address, `user`.firstname, `user`.lastname,
                                          A.batiment AS batiment, A.digicode AS digicode_1, A.digicode_2 AS digicode_2,
                                          A.floor AS etage, A.door AS porte, A.doorBell AS interphone, A.subway_id
                                    FROM agenda IP
                                    INNER JOIN address A ON A.id = IP.address_id
                                    INNER JOIN `user` ON `user`.id = IP.user_id
                                    WHERE IP.provider_id =
                                    (
                                        SELECT id FROM provider WHERE provider.user_id =
                                        (
                                            SELECT id FROM `user` WHERE user_token = ?
                                        )
                                    )
                                    AND IP.pending = 0 AND IP.deleted = 0 AND IP.id != ? AND IP.day = ?
                                    AND (end_rec IS NULL OR end_rec > ?)", [$data['user_token'], $data['id'], $data['day'], $data['end_date']]);
            if ($req !== false)
                return $req->fetchAll();
            else {
                return false;
            }
        }
    }

    public function editAgenda($data){
            return $this->db->query_log("UPDATE agenda
                            SET `start` = ?, `end` = ?, `day` = ?, `start_rec` = ?, `end_rec` = ?
                            WHERE id = ?", [$data['start'], $data['end'], $data['day'], $data['start_rec'], $data['end_rec'], $data['id']]);
    }


    public function getDispoProvider($data)
    {
        $req = $this->db->query_log("SELECT * FROM dispo
                                WHERE user_id =(SELECT id FROM `user` WHERE user_token = ?)
                                ORDER BY `day` ASC", [$data['user_token']]);
        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getDispoClient($data)
    {
        $resultat = $this->db->query_log("SELECT * FROM dispo_client
                                        WHERE user_id =(SELECT id FROM `user` WHERE user_token = ?)
                                        AND address_id =(SELECT id FROM address WHERE token = ?)
                                        ORDER BY `id` ASC", [$data['user_token'], $data['address_token']])->fetchAll();
        return $resultat;
    }

    public function addDispoProvider($event){
        $ret =  $this->db->query_log("INSERT INTO  dispo(`user_id`, `end`, `start`, `day`)
                                        VALUES((SELECT id FROM `user` WHERE user_token = ?), ?, ?, ?)",
                                            [$event['token'], $event['end'], $event['start'], $event['day']]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function addEventForAddress($address_id, $event_id)
    {
        $ret = $this->db->query_log("INSERT INTO event_address (`address_id`, `event_id`)
                                    VALUES (?,?)", [$address_id, $event_id]);
        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }

    public function addEvent($user_token, $event_id){
        $ret = $this->db->query_log("INSERT INTO event_user_client (`user_id`, `event_id`)
                                    VALUES ((SELECT id FROM `user` WHERE user_token = ?),?)", [$user_token, $event_id]);
        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }

    public function addNewEvent($event){
        $ret = $this->db->query_log("INSERT INTO  event(
                                        `l_category_event_id`,
                                        `end`,
                                        `start`,
                                        `day`,
                                        `start_rec`,
                                        `end_rec`,
                                        `title`,
                                        `available`,
                                        `commentary`
                                    )
                                    VALUES (
                                        (SELECT id FROM `list` WHERE position = ? AND listName = 'event_categories'), 
                                        ?, ?, ?, ?, ?, ?, ?, ?
                                        )",
                                    [
                                        $event['category'],
                                        $event['end'],
                                        $event['start'],
                                        $event['day'],
                                        $event['start_rec'],
                                        $event['end_rec'],
                                        empty($event['title']) ? null : $event['title'],
                                        empty($event['available']) ? 0 : $event['available'],
                                        empty($event['commentary']) ? '' : $event['commentary']
                                    ]);
        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }


    public function userHasEvent($user_token, $event_id) {
        $req = $this->db->query_log("SELECT * FROM event_user_client e
                                     INNER JOIN user u ON(e.user_id = u.id)
                                     WHERE u.user_token = ?
                                     AND e.event_id = ?",
                                        [$user_token, $event_id])->fetchAll();
        return count($req) > 0;
    }

    public function addressHasEvent($address_id, $event_id) {
        $req = $this->db->query_log("SELECT * FROM event_address e
                                     WHERE e.address_id = ?
                                     AND e.event_id = ?",
                                        [$address_id, $event_id])->fetchAll();
        return count($req) > 0;
    }

    public function getEventById($event_id)
    {
        return $this->db->query_log("SELECT * FROM `event` WHERE `id` = ?", [$event_id])->fetch();
    }

    public function getEventsByUser($user_token)
    {
        $req = $this->db->query_log("SELECT e.id, e.start, e.end, e.day, r.user_id,
                                            r.event_id, e.l_category_event_id,
                                            e.start_rec, e.end_rec, e.available
                                    FROM event_user_client r
                                    INNER JOIN event e ON(r.event_id = e.id)
                                    INNER JOIN user u ON(r.user_id = u.id)
                                    WHERE u.user_token = ?
                                    AND e.deleted = 0
                                    ORDER BY `day` ASC", [$user_token]);
        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getEventsByUserByCategory($user_token, $category, $available = 0)
    {
        $req = $this->db->query_log("SELECT r.user_id, r.event_id, u.user_token, e.*   FROM event_user_client r
                                     INNER JOIN event e ON(r.event_id = e.id)
                                     INNER JOIN user u ON(r.user_id = u.id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     WHERE l.position = ?
                                     AND l.listName = 'event_categories'
                                     AND u.user_token = ?
                                     AND e.deleted = 0
                                     AND e.available = ?
                                     ORDER BY `day` ASC", [$category,$user_token, $available]);

        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getFirstEventByUserByCategory($user_token, $category, $available = 0)
    {
        $req = $this->db->query_log("SELECT r.user_id, r.event_id, u.user_token, e.*   FROM event_user_client r
                                     INNER JOIN event e ON(r.event_id = e.id)
                                     INNER JOIN user u ON(r.user_id = u.id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     WHERE l.position = ?
                                     AND l.listName = 'event_categories'
                                     AND u.user_token = ?
                                     AND e.deleted = 0
                                     AND e.available = ?
                                     ORDER BY `day` ASC", [$category,$user_token, $available]);

        if ($req === false)
            return false;
        return $req->fetch();
    }

    public function getEventsByCategoryByDay($category, $dayDate, $available = 0)
    {
        $day = strftime("%w", strtotime($dayDate));
        $req = $this->db->query_log("SELECT e.*, r.user_id, u.firstname, u.lastname, u.user_token, ? event_date FROM event e
                                     INNER JOIN event_user_client r ON(e.id = r.event_id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     INNER JOIN user u ON(r.user_id = u.id)
                                     WHERE l.position = ?
                                     AND e.available = ?
                                     AND l.listName = 'event_categories'
                                     AND e.deleted = 0
                                     AND u.who = 'guestrelation'
                                     AND e.day = ?
                                     AND e.start_rec <= ?
                                     AND (e.end_rec IS NULL OR e.end_rec >= ?)
                                     ORDER BY `day` ASC", [$dayDate, $category,$available, $day, $dayDate, $dayDate]);

        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getEventsByCategoryFromDay($category, $dayDate, $available = 0)
    {
        $req = $this->db->query_log("SELECT e.*, r.user_id, u.firstname, u.lastname, u.user_token, ? event_date FROM event e
                                     INNER JOIN event_user_client r ON(e.id = r.event_id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     INNER JOIN user u ON(r.user_id = u.id)
                                     WHERE l.position = ?
                                     AND e.available = ?
                                     AND l.listName = 'event_categories'
                                     AND e.deleted = 0
                                     AND u.who = 'guestrelation'
                                     AND (e.end_rec IS NULL OR e.end_rec >= ?)
                                     ORDER BY `start`, `start_rec`, `day` ASC", [$dayDate, $category,$available, $dayDate]);

        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getEventsByUserByWeek($user_token, $start, $end)
    {
        $req = $this->db->query_log("SELECT e.*, r.user_id, u.firstname, u.lastname, u.user_token FROM event e
                                     INNER JOIN event_user_client r ON(e.id = r.event_id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     INNER JOIN user u ON(r.user_id = u.id)
                                     WHERE l.listName = 'event_categories'
                                     AND e.deleted = 0
                                     AND u.who = 'guestrelation'
                                     AND u.user_token = ?
                                     AND (e.end_rec IS NULL OR e.end_rec >= ?)
                                     AND e.start_rec < ?
                                     ORDER BY `start`, `start_rec`, `day` ASC", [$user_token, $start, $end]);

        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getEventsByAddressByCategory($address_id, $category, $available = 0)
    {
        $req = $this->db->query_log("SELECT ea.address_id, ea.event_id, e.*   FROM event_address ea
                                     INNER JOIN event e ON(ea.event_id = e.id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     WHERE l.position = ?
                                     AND l.listName = 'event_categories'
                                     AND ea.address_id = ?
                                     AND e.deleted = 0
                                     AND e.available = ?
                                     ORDER BY `day` ASC", [$category, $address_id, $available]);

        if ($req === false)
            return false;
        return $req->fetchAll();
    }

    public function getFirstEventByAddressByCategory($address_id, $category, $available = 0)
    {
        $req = $this->db->query_log("SELECT ea.address_id, ea.event_id, e.*   FROM event_address ea
                                     INNER JOIN event e ON(ea.event_id = e.id)
                                     INNER JOIN list l ON(e.l_category_event_id = l.id)
                                     WHERE l.position = ?
                                     AND l.listName = 'event_categories'
                                     AND ea.address_id = ?
                                     AND e.deleted = 0
                                     AND e.available = ?
                                     ORDER BY `day` ASC", [$category, $address_id, $available]);

        if ($req === false)
            return false;
        return $req->fetch();
    }

    public function getUsersByEventId($event_id, $who = "%")
    {
        $ret = $this->db->query_log("SELECT u.* FROM `event_user_client` eu
                                    INNER JOIN `user` u ON(eu.user_id = u.id)
                                    WHERE eu.event_id = ?
                                    AND u.who LIKE ?", [$event_id, $who]);

        if ($ret === false) {
            return false;
        }
        return $ret->fetchAll();
    }

    public function getUserTokensByEventId($event_id, $who = "%")   
    {
        $users = $this->getUsersByEventId($event_id, $who);
        $tokens = [];

        if (!$users)
            return false;

        foreach ($users as $key => $value) {
            array_push($tokens, $value->user_token);
        }

        return $tokens;
    }

    public function getFirstUsersByEventId($event_id, $who = "%")
    {
        $ret = $this->db->query_log("SELECT u.* FROM `event_user_client` eu
                                    INNER JOIN `user` u ON(eu.user_id = u.id)
                                    WHERE eu.event_id = ?
                                    AND u.who LIKE ?", [$event_id, $who]);

        if ($ret === false) {
            return false;
        }
        return $ret->fetch();
    }

    public function getAddressByEventId($event_id)
    {
        $ret = $this->db->query_log("SELECT a.* FROM `event_address` ea
                                    INNER JOIN `address` a ON(ea.address_id = a.id)
                                    WHERE ea.event_id = ?", [$event_id]);

        if ($ret === false) {
            return false;
        }
        return $ret->fetchAll();
    }

    public function getFirstAddressByEventId($event_id)
    {
        $ret = $this->db->query_log("SELECT a.* FROM `event_address` ea
                                    INNER JOIN `address` a ON(ea.address_id = a.id)
                                    WHERE ea.event_id = ?", [$event_id]);

        if ($ret === false) {
            return false;
        }
        return $ret->fetch();
    }

    public function addDispoClient($event){
        $ret = $this->db->query_log("INSERT INTO dispo_client(`user_id`, `address_id`, `end`, `start`, `day`)
                                          VALUES(
                                                  (SELECT U.`id` FROM `user` U WHERE U.`user_token` = ?),
                                                  (SELECT A.`id` FROM `address` A WHERE A.`token` = ?),
                                                  ?, ?, ?
                                                )", [$event['token'], $event['address_token'], $event['end'], $event['start'], $event['day']]);
        if ($ret === false)
            return 0;
        return $this->db->lastInsertId();
    }

    public function updateDispoProvider($event){
        return $this->db->query_log("UPDATE dispo SET `start` = ?, `end` = ?, `day` = ?
                                WHERE id = ?", [$event['start'], $event['end'], $event['day'], $event['id']]);
    }

    public function updateEvent($event){
        return $this->db->query_log("UPDATE
                                        event
                                    SET
                                        `start` = ?,
                                        `end` = ?,
                                        `day` = ?,
                                        `start_rec` = ?,
                                        `end_rec` = ?
                                    WHERE
                                        id = ?",
                                    [
                                        $event['start'],
                                        $event['end'],
                                        $event['day'],
                                        $event['start_rec'],
                                        $event['end_rec'],
                                        $event['id']
                                    ]);
    }

    public function updateEventOnWeek($event){
        return $this->db->query_log("UPDATE
                                        `event`
                                    SET
                                        `start` = ?,
                                        `end` = ?,
                                        `day` = ?,
                                        `start_rec` = ?,
                                        `end_rec` = ?,
                                        `commentary` = ?
                                    WHERE
                                        `id` = ?",
                                    [
                                        $event['start'],
                                        $event['end'],
                                        $event['day'],
                                        $event['start_rec'],
                                        $event['end_rec'],
                                        $event['commentary'],
                                        $event['id']
                                    ]);
    }

    public function updateEventAvailability($event_id, $available)
    {
        $ret = $this->db->query_log("UPDATE `event` SET `available` = ? WHERE `id` = ?", [$available, $event_id]);

        if ($ret === false) {
            return 0;
        }

        return 1;
    }

    public function updateEventTitle($event_id, $title)
    {
        $ret = $this->db->query_log("UPDATE `event` SET `title` = ? WHERE `id` = ?", [$title, $event_id]);

        if ($ret === false) {
            return 0;
        }

        return 1;
    }

    public function updateEventCommentary($event_id, $commentary)
    {
        $ret = $this->db->query_log("UPDATE `event` SET `commentary` = ? WHERE `id` = ?", [$commentary, $event_id]);

        if ($ret === false) {
            return 0;
        }

        return 1;
    }


    public function updateDispoClient($event){
            return $this->db->query_log("UPDATE dispo_client SET `start` = ?, `end` = ?, `day` = ?
                            WHERE id = ?", [$event['start'], $event['end'], $event['day'], $event['id']]);
    }

    public function PendingAgendaByDay($day, $token){
        return $this->db->query_log("UPDATE agenda SET agenda.pending = 1
                                WHERE `day` = ? AND provider_id =(
                                        SELECT id FROM provider WHERE user_id =(
                                            SELECT id FROM `user` WHERE user_token = ?
                                        )
                                )", [$day, $token]);
    }

    public function deleteEventById($id){
        $ret = $this->db->query_log("UPDATE `event` SET deleted = 1 WHERE `id` = ?", [$id]);

        if ($ret == false) {
            return 0;
        }
        return 1;
    }

    public function deleteEventForUser($event_id, $user_id)
    {
        $ret = $this->db->query_log("DELETE FROM `event_user_client` WHERE `event_id` = ? AND `user_id` = ?", [$event_id, $user_id]);

        if ($ret == false) {
            return 0;
        }
        return 1;
    }

    public function deleteEventForAddress($event_id, $address_id)
    {
        $ret = $this->db->query_log("DELETE FROM `event_address` WHERE `event_id` = ? AND `address_id` = ?", [$event_id, $address_id]);

        if ($ret == false) {
            return 0;
        }
        return 1;
    }

    public function lookForConflicts($event, $user_token, $category = 0)
    {
        $events = $this->db->query_log("
            SELECT e.*
            FROM `event` e
            INNER JOIN `list` l ON (e.`l_category_event_id` = l.`id`)
            INNER JOIN `event_user_client` eu ON (e.`id` = eu.`event_id`)
            INNER JOIN `user` u ON (eu.`user_id` = u.`id`)
            WHERE 
                e.`id` != ?
                AND (? IS NULL OR e.`start_rec` < ?)
                AND (e.`end_rec` IS NULL OR e.`end_rec` > ?)
                AND e.`start` < ?
                AND e.`end` >= ?
                AND e.`day` = ?
                AND (? = 0 OR l.position = ?)
                AND u.`user_token` = ?
                AND e.`deleted` = 0
        ",[
            $event['id'],
            $event['end_rec'],
            $event['end_rec'],
            $event['start_rec'],
            $event['end'],
            $event['start'],
            $event['day'],
            $category,
            $category,
            $user_token
        ]);

        if (!$events) {
            return false;
        }
        return $events->fetchAll();
    }

    public function DelAgendaByDay($day, $token){
        return $this->db->query_log("UPDATE agenda SET agenda.deleted = 1, agenda.deleted_at = NOW()
                                WHERE agenda.day = ? AND provider_id =(
                                        SELECT id FROM provider WHERE user_id =(
                                            SELECT id FROM `user` WHERE user_token = ?
                                        )
                                )", [$day, $token]);
    }

    public function delDispoProvider($data){ //option1 = Pending or Del // option2 = Select Day Or Select All
        if ($data['del_option'] == "pending") {
            if ($this->PendingAgendaByDay($data['day'], $data['provider_token']) === false)
                return false;
        }
        else if ($data['del_option'] == "delete") {
            if ($this->DelAgendaByDay($data['day'], $data['provider_token']) === false)
                return false;
        }
        return $this->db->query_log("DELETE FROM dispo
                          WHERE `id` = ? AND user_id = (
                                SELECT id FROM `user` WHERE user_token = ?
                          )", [$data['id'], $data['provider_token']]);
    }


    public function delDispoClient($event_id, $token){ //check missionID en amont
            return $this->db->query_log("DELETE FROM dispo_client WHERE `id` = ? AND user_id = (
                                SELECT id FROM `user`
                                WHERE user_token = ?)", [$event_id, $token]);
    }
}
