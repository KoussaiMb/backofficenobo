<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 06/10/2017
 * Time: 15:37
 */
//TODO: Alerte sur le groupe et tous ses fils
//TODO: Annonces générales
//TODO: Rendre visible les interactions avec les alertes
class alert
{
    private $db;
    private $creator;

    public function __construct($db, $creator)
    {
        $this->db = $db;
        $this->creator = $creator;
    }

    public function getAlertByPriority($priority = 'critic', $from = null, $to = null) {
        $ext = "";

        if ($from && $to)
            $ext .= ' LIMIT ' . $from . ',' . $to;
        $ret = $this->db->query_log("
            SELECT A.*, (SELECT L.`field` FROM `list` L WHERE L.`id` = A.`priority_id`) as priority FROM `alert` A WHERE
           A.`priority_id` = (SELECT L.`id` FROM `list` L WHERE L.`field` = ?) ORDER BY `created_at` DESC
            " . $ext, [$priority]);
        return $ret->fetchAll();
    }

    public function getLastAlertsFromUser($user_id = null, $from = null, $to = null) {
        $ext = "";

        if ($from && $to)
            $ext .= ' LIMIT ' . $from . ',' . $to;
        $ret = $this->db->query_log("
            SELECT A.*, (SELECT L.`field` FROM `list` L WHERE L.`id` = A.`priority_id`) as priority FROM `alert` A WHERE
            A.`created_by` = ? ORDER BY A.`created_at` DESC
            " . $ext, [$user_id]);
        return $ret->fetchAll();
    }

    public function getLastAlertsNotFromUser($user_id = null, $from = null, $to = null) {
        $ext = "";

        if ($from && $to)
            $ext .= ' LIMIT ' . $from . ',' . $to;
        $ret = $this->db->query_log("
            SELECT A.*, (SELECT L.`field` FROM `list` L WHERE L.`id` = A.`priority_id`) as priority FROM `alert` A WHERE
            A.`created_by` != ? ORDER BY A.`created_at` DESC
            " . $ext, [$user_id]);
        return $ret->fetchAll();
    }

    private function addUserAlertByToken($user_token, $alert_id) {
        $this->db->query("
                  INSERT INTO `alert_user` SET
                  `user_id` = (SELECT id FROM `user` WHERE `user_token` = ?), `alert_id` = ?", [$user_token, $alert_id]);
    }

    private function addUserAlertById($user_id, $alert_id) {
        $this->db->query("INSERT INTO `alert_user` SET `user_id` = ?, `alert_id` = ?", [$user_id, $alert_id]);
    }

    private function deleteAlert($alert_id) {
        $this->db->query("DELETE FROM `alert` WHERE `id` = ?", [$alert_id]);
    }

    public function deleteUserAlert($alert_id) {
        $this->db->query("DELETE FROM `alert_user` WHERE `alert_id` = ?", [$alert_id]);
    }

    private function finishAlert($alert_id) {
        $this->db->query("UPDATE `alert` SET `finished` = 1 WHERE `id` = ?", [$alert_id]);
    }

    public function finishUserAlert($alert_id) {
        $this->db->query("UPDATE `alert_user` SET `finished` = 1 WHERE `alert_id` = ?", [$alert_id]);
    }

    public function deleteAlerts($data) {
        try {
            $this->db->beginTransaction();
            $this->deleteAlert($data['id']);
            $this->deleteUserAlert($data['id']);
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 2);
            return 0;
        }
    }

    public function setAlertsAsFinished($data) {
        try {
            $this->db->beginTransaction();
            $this->finishAlert($data['id']);
            $this->finishUserAlert($data['id']);
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 2);
            return 0;
        }
    }

    public function createCriticAlert($title, $description, $finished_at = null, $url = null)
    {
        $priority = App::getListManagement()->getIdByListAndName('priority', 'critic');

        if (!isset($priority->id))
            return false;
        $data['priority_id'] = $priority->id;
        $data['description'] = $description;
        $data['title'] = $title;
        $data['url'] = $url;
        $data['finished_at'] = $finished_at;
        return $this->addAlert($data);

    }

    public function createLowAlert($title, $description, $url = null, $finished_at = null)
    {
        $priority = App::getListManagement()->getIdByListAndName('priority', 'low');

        if (!isset($priority->id))
            return false;
        $data['priority_id'] = $priority->id;
        $data['description'] = $description;
        $data['title'] = $title;
        $data['url'] = $url;
        $data['finished_at'] = $finished_at;
        return $this->addAlert($data);
    }

    public function createHighAlert($title, $description, $url = null, $finished_at = null)
    {
        $priority = App::getListManagement()->getIdByListAndName('priority', 'high');

        if (!isset($priority->id))
            return false;
        $data['priority_id'] = $priority->id;
        $data['description'] = $description;
        $data['title'] = $title;
        $data['url'] = $url;
        $data['finished_at'] = $finished_at;
        return $this->addAlert($data);
    }

    /**
     * @param $data array
     * @param $data['title'] string
     * @param $data['priority'] string (low, high, critic)
     * @param $data['description'] string
     * @param $data['url'] string (to redirect)
     * @param $data['finished_at'] datetime
     * @return mixed
     */
    public function quickAdd($data)
    {
        $priority = App::getListManagement()->getIdByListAndName('priority', $data['priority']);

        if (!$priority)
            return false;
        $ret = $this->db->query_log("
              INSERT INTO `alert` SET
              `title` = ?,
              `priority_id` = ?,
              `created_by` = ?,
              `description` = ?,
              `url` = ?,
              `finished_at` = ?,
              `created_at` = NOW()", [
            $data['title'],
            $priority,
            $this->creator,
            $data['description'],
            $data['url'],
            $data['finished_at']
        ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function addAlert($data)
    {
        $ret = $this->db->query_log("
              INSERT INTO `alert` SET
              `title` = ?,
              `priority_id` = ?,
              `created_by` = ?,
              `description` = ?,
              `url` = ?,
              `finished_at` = ?,
              `created_at` = NOW()", [
            $data['title'],
            $data['priority_id'],
            $this->creator,
            $data['description'],
            $data['url'],
            $data['finished_at']
        ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function addAlertFromBo($data)
    {
        $alert_id = $this->addAlert($data);
        if (empty($alert_id))
            return 0;
        $users = [];
        //TODO:Make 1 call
        foreach($data['groups'] as $k => $group_id) {
            $users_in_group = App::getManageGroup()->getUsersInGroup($group_id);
            foreach ($users_in_group as $user)
                $users[] = $user->id;
        }
        if (!empty($data['groups']) && empty($users))
            return 0;
        try {
            $this->db->beginTransaction();
            foreach ($data['users'] as $k => $user_token) {
                $this->addUserAlertByToken($user_token, $alert_id);
            }
            foreach ($users as $k => $user_id) {
                $this->addUserAlertById($user_id, $alert_id);
            }
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 2);
            return 0;
        }
    }

    static function createAlertDiv($alert)
    {
        $toShow = "<div class='alert-nobo row border-priority-". $alert->priority;
        $toShow .= $alert->finished == 0 ? " alert-clickable'>" : "'>";
        $toShow .= "<input type='hidden' class='alert_id' value='". $alert->id ."''>";
        $toShow .= "<input type='hidden' class='alert_priority' value=". $alert->priority .">";
        $toShow .= "<div class='col-sm-8'>";
        $toShow .= "<p class='alert-title'>" . htmlspecialchars($alert->title) . "</p>";
        $toShow .= "<p class='alert-description'>". htmlspecialchars($alert->description) ."</p>";
        $toShow .= "</div>";
        $toShow .= "<div class='col-sm-4 text-center'>";
        if ($alert->finished == 0){
            $toShow .= "<p class='label label-default bg-priority-". $alert->priority ."'>";
            $toShow .= $alert->priority ."</p>";
        } else {
            $toShow .= "<p class='label label-default'>terminé</p>";
        }
        $toShow .= "<p class='alert-date'>". parseStr::date_format($alert->created_at, 'H\hs d M Y') ."</p>";
        $toShow .= "</div>";
        $toShow .= "</div>";

        echo $toShow;
    }
}