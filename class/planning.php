<?php
/**
 * Created by PhpStorm.
 * User: bienvenue
 * Date: 28/11/2017
 * Time: 15:21
 */

class planning
{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    private function sort_path_img_by_line(){
        $sort_img_path = [];

        $unsort_img_path = App::getAllLinePic();
        foreach ($unsort_img_path as $i => $img_path){
            $sort_img_path[$img_path->line][] = $img_path;
        }
        return $sort_img_path;
    }

    private function create_transport_icon_array($value, $sort_img_path,fullCalendar $fullcalendar, &$transport_icon, &$transport){
        $data_transport = [
            "id" => $value->subway_id
        ];
        $transport = $fullcalendar->getMetroById($data_transport);
        if ($transport === false) {
            $json = parseJson::error('Erreur lors de la récupération de l\'arret de métro');
            $json->printJson();
        }

        $transport_type = [
            "T" => ["img" => ' <img src="' . $sort_img_path["T"][0]->path . '" height="18px" width="18px" />', "nb_line" => 0],
            "R" => ["img" => ' <img src="' . $sort_img_path["R"][0]->path . '" height="18px" width="18px" />', "nb_line" => 0],
            "L" => ["img" => ' <img src="' . $sort_img_path["L"][0]->path . '" height="18px" width="18px" />', "nb_line" => 0],
            "M" => ["img" => ' <img src="' . $sort_img_path["M"][0]->path . '" height="18px" width="18px" />', "nb_line" => 0]
        ];
        $subway_line = explode("/", $transport[0]->ligne);
        $transport_icon = "";
        foreach ($subway_line as $i => $line) {
            $transport_type[substr(trim($line), 0, 1)]["img"] .= ' <img src="' . $sort_img_path[trim($line)][0]->path . '" height="18px" width="18px" />';
            $transport_type[substr(trim($line), 0, 1)]["nb_line"] += 1;
        }

        foreach ($transport_type as $j => $type) {
            if ($type["nb_line"] > 0)
                $transport_icon .= $type["img"] . '</br>';
        }
    }

    private function create_event_array($slot, $color_array, $transport, $transport_icon, $start, $end, $editable){
        return array(
            "agenda_id" => empty($slot->agenda_id) ? NULL : $slot->agenda_id,
            "start" => empty($start) ? NULL : $start,
            "editable" => $editable,
            "end" => empty($end) ? NULL : $end,
            "addresses" => empty($slot->address) ? "" : $slot->address,
            "color" => $color_array[$slot->type],
            "customer_id" => empty($slot->user_id) ? NULL : $slot->user_id,
            "lastname" => empty($slot->lastname) ? "" : $slot->lastname,
            "firstname" => empty($slot->firstname) ? "" : $slot->firstname,
            "customer_token" => empty($slot->user_token) ? NULL : $slot->user_token,
            "allDay" => false,
            "type" => $slot->type,
            "station" => empty($transport[0]->arret) ? "" : $transport[0]->arret,
            "line" => empty($transport[0]->ligne) ? "" : $transport[0]->ligne,
            "transport_icon" => empty($transport_icon) ? "" : $transport_icon,
            "batiment" => empty($slot->batiment) ? "" : $slot->batiment,
            "digicode_1" => empty($slot->digicode_1) ? "" : $slot->digicode_1,
            "digicode_2" => empty($slot->digicode_2) ? "" : $slot->digicode_2,
            "porte" => empty($slot->porte) ? "" : $slot->porte,
            "etage" => empty($slot->etage) ? "" : $slot->etage,
            "interphone" => empty($slot->interphone) ? "" : $slot->interphone,
            "lock" => $slot->locked,
            "slot_id" => $slot->id,
            "rendering" => "event",
            "event_type" => "agenda"
        );
    }

    private function check_week_type($slot, $week){
        if (($week % 2 == 1 && $slot->type == 1) || ($week % 2 == 0 && $slot->type == 2) || ($slot->type == 0 || $slot->type == 3))
            return true;
        else
            return false;
    }

    private function generate_week_event(&$data, &$event_tab){

        $color_array = ["rgb(133, 133, 235)", "rgb(154, 192, 205)", "rgb(205, 154, 154)", "rgb(0, 10, 255)"];
        $weekDay = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

        /**
         * create the events
         */
        $Allslot = $data['fullcalendar']->getAllSlotByUserToken($data['user_token']);
        if ($Allslot === false){
            return false;
        }
        $sort_img_path = $this->sort_path_img_by_line();
        foreach($Allslot as $index => $slot){
            $transport = "";
            $transport_icon = "";
            if (!empty($slot->subway_id)) {
                $this->create_transport_icon_array($slot, $sort_img_path, $data['fullcalendar'], $transport_icon, $transport);
            }
            $start_time = explode(":", $slot->start);
            $end_time = explode(":", $slot->end);
            $day_before_start_date = strtotime('-1 day', strtotime($data['start_date']));
            $next_day = date('Y-m-d', strtotime('next '. $weekDay[$slot->day] , $day_before_start_date));
            $week = date("W", strtotime($next_day));
            if ((parseStr::compare_time_strict_equal($next_day, $slot->start_rec) || empty($slot->start_rec)) && (parseStr::compare_time_strict_equal($slot->end_rec, $next_day) || empty($slot->end_rec)) && $this->check_week_type($slot, $week)) {
                $start = date('Y-m-d H:i:s', strtotime('+' . $start_time[0] . ' hours' . '+' . $start_time[1] . ' minutes', strtotime($next_day)));
                $end = date('Y-m-d H:i:s', strtotime('+' . $end_time[0] . ' hours' . '+' . $end_time[1] . ' minutes', strtotime($next_day)));
                array_push($event_tab, $this->create_event_array($slot, $color_array, $transport, $transport_icon, $start, $end, $data['editable']));
            }
        }
        return true;
    }

    public function merge_mission_agenda($agenda_tab, $mission_tab){
        foreach ($agenda_tab as $agenda_index => $agenda) {
            if ($agenda['rendering'] != 'event')
                continue;
            foreach ($mission_tab as $mission) {
                if ($agenda['agenda_id'] === $mission['agenda_id']) {
                    unset($agenda_tab[$agenda_index]);
                    break;
                }
            }
        }
        return array_merge($agenda_tab, $mission_tab);
    }

    private function generate_month_event(&$data, &$event_tab){
        $week = 0;
        $week_start = $data['month_start'];
        $week_end = date('Y-m-d', strtotime($data['month_start'] .'+ 7 days'));
        while ($week < $data['diff'] / 7){
            $week_start = $week > 0 ? date('Y-m-d', strtotime($week_start .'+ 7 days')) : $week_start;
            $week_end = $week > 0 ? date('Y-m-d', strtotime($week_end .'+ 7 days')) : $week_end;
            $data_generate_week = [
                "week_start" => $week_start,
                "week_end" => $week_end,
                "fullcalendar" => $data['fullcalendar'],
                "db" => $data['db'],
                "user_token" => $data['user_token']
            ];
            if ($this->generate_week_event($data_generate_week, $event_tab) === false)
                return false;
            $week++;
        }
        return true;
    }

    public function generate_calendar_event(&$data, &$event_tab){
        $diff = parseStr::get_date_diff($data['start'], $data['end'], "%a");

        if ($diff <= 7) {
            $data_generate_week = [
                "week_start" => $data['start'],
                "week_end" => $data['end'],
                "fullcalendar" => $data['fullcalendar'],
                "user_token" => $data['user_token'],
                "editable" => $data['editable'],
                "start_date" => $data['start_date']
            ];
            if ($this->generate_week_event($data_generate_week, $event_tab) === false)
                return false;
        }
        else {
            $data_generate_month = [
                "month_start" => $data['start'],
                "month_end" => $data['end'],
                "fullcalendar" => $data['fullcalendar'],
                "user_token" => $data['user_token'],
                "diff" => $diff,
                "editable" => $data['editable'],
                "start_date" => $data['start_date']
            ];
            if ($this->generate_month_event($data_generate_month, $event_tab) === false)
                return false;
        }
    }
}