<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 13/12/2016
 * Time: 10:35
 */
class ListManagement
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getList($listName)
    {
        return $this->db->query_log("SELECT * FROM `list` WHERE `listName` = ? AND `deleted` = 0 ORDER BY `position` ASC", [$listName])->fetchAll();
    }

    public function getListReference($listName)
    {
        return $this->db->query_log("SELECT * FROM `list_reference` WHERE `list_name` = ? ORDER BY `value` ASC", [$listName])->fetchAll();
    }

    public function inList($listName, $fieldId)
    {
        return $this->db->query_log("SELECT 1 as exist FROM `list` WHERE `listName` = ? AND `deleted` = 0 AND `id` = ?", [$listName, $fieldId])->fetch()->exist;
    }

    public function inDeletedList($listName, $fieldId)
    {
        return $this->db->query_log("SELECT 1 as exist FROM `list` WHERE `listName` = ? AND `deleted` = 1 AND `id` = ?", [$listName, $fieldId])->fetch()->exist;
    }

    public function getFieldById($id)
    {
        return $this->db->query_log("SELECT `field` FROM `list` WHERE `id` = ?", [$id])->fetch()->field;
    }

    public function getPositionById($id)
    {
        return $this->db->query_log("SELECT `position` FROM `list` WHERE `id` = ?", [$id])->fetch()->position;
    }

    public function getIdByListAndName($listName, $name)
    {
        return $this->db->query_log("SELECT `id` FROM `list` WHERE `name` = ? AND `listName` = ?", [$name, $listName])->fetch()->position;
    }

    public function getIdByPosition($listName, $position)
    {
        return $this->db->query_log("SELECT `id` FROM `list` WHERE `listName` = ? AND `position` = ? LIMIT 0,1", [$listName, $position])->fetch()->id;
    }

    public function getAllList()
    {
        return $this->db->query_log("SELECT `listName` FROM `list` WHERE `deleted` = 0 GROUP BY `listName`")->fetchAll();
    }

    public function addInput($listName, $field, $position = null)
    {
        try {
            $nbRow = $this->db->query("SELECT COUNT(position) FROM `list` WHERE `listName` = ? AND `deleted` = 0", [$listName])->fetch();
            $nbRow = array_values((array)$nbRow)[0];
        } catch (PDOException $e) {
            $nbRow = 0;
        }
        if ($position == null || $position && $position > $nbRow) {
            $position = $nbRow + 1;
        } else {
            $this->upPosition($listName, $position, 1);
        }
        try {
            $this->db->query("INSERT INTO list(`position`, `listName`, `field`) values(?, ?, ?)", [$position, $listName, $field]);
            return 1;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function getDeletedList($listName)
    {
        return $this->db->query_log("SELECT * FROM `list` WHERE `listName` = ? AND `deleted` = 1 ORDER BY `position`", [$listName])->fetchAll();
    }

    public function getAllDeletedList()
    {
        return $this->db->query_log("SELECT `listName` FROM `list` WHERE `deleted` = 0 GROUP BY `listName`")->fetchAll();
    }

    public function callbackInput($listName, $id, $position = null)
    {
        if ($position) {
            $ret = $this->db->query_log("UPDATE `list` SET `position` = ? WHERE `id` = ?", [$position, $id]);
            if ($ret === false)
                return false;
        } else {
            $position = $this->db->query_log("SELECT `position` FROM `list` WHERE `id` = ?", [$id])->fetch()->position;
            if (!$position)
                return false;
        }
        $ret1 = $this->upPosition($listName, $position, 1);
        $ret2 = $this->db->query_log("UPDATE `list` SET `deleted` = 0 WHERE `id` = ?", [$id]);
        if ($ret1 === false || $ret2 === false)
            return false;
        return true;
    }

    public function delForm($listName)
    {
        $ret = $this->db->query_log("DELETE FROM `list` WHERE `listName` = ?", [$listName]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function delInput($id)
    {
        $record = $this->db->query("SELECT `position`, `listName` FROM `list` WHERE `id` = ?", [$id])->fetch();
        try {
            $nbRow = $this->db->query("SELECT COUNT(position) FROM `list` WHERE `listName` = ?", [$record->listName])->fetch();
            $nbRow = array_values((array)$nbRow)[0];
        } catch (PDOException $e) {
            $nbRow = 0;
        }
        $ret = $this->db->query_log("UPDATE `list` SET `deleted` = 1 WHERE `id` = ?", [$id]);
        if ($record->position <= $nbRow)
            $this->upPosition($record->listName, $record->position + 1, -1);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function upInput($id, $listName, $field, $value, $position = null)
    {
        $nbRow = $this->db->nbRow('list', $listName);
        if ($position && $position > $nbRow) {
            $position = $nbRow + 1;
        } else {
            $this->upPosition($listName, $position, 1);
        }
        $this->db->query_log("UPDATE `list` SET `position` = ?, `listName` = ?, `field` = ?, `value` = ? WHERE `id` = ?", [$position, $listName, $field, $value, $id]);
    }

    private function upPosition($listName, $position, $sens)
    {
        $ret = $this->db->query_log("UPDATE `list` SET `position` = `position` + ? WHERE `listName`= ? AND `position` >= ? AND `deleted` = 0", [$sens, $listName, $position]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }


    public function addToRefList($value, $url)
    {

        try {
            $this->db->query("INSERT INTO list_reference(`list_name`, `value`, `url`) values(?, ?, ?)", ['roomsList', $value, $url]);
            return $this->db->lastInsertId();
        } catch (PDOException $e) {
            return false;
        }
    }

    public function getRoomById($room_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `list_reference` WHERE `id` = ?", [$room_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function upRoom($listname, $value, $url, $id)
    {
        $ret = $this->db->query_log("UPDATE `list_reference` SET `list_name` = ? , `value` = ?  ,  `url` = ?  WHERE  `id` = ? ", [$listname, $value, $url, $id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function deleteFromListReferenceById($id)
    {
        $ret = $this->db->query_log("DELETE FROM `list_reference` WHERE id = ?", [$id]);
        if ($ret === false)
            return 0;
        return 1;
    }

    public function getListById($list_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `list` WHERE `id` = ?", [$list_id]);
        if ($ret === false)
            return false;
        return $ret->fetch()->field;
    }

    public function getUrlImageById($list_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `list_reference` WHERE `id` = ?", [$list_id]);
        if ($ret === false)
            return false;
        return $ret->fetch()->url;
    }

}


