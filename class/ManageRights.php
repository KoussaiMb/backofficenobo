<?php

class ManageRights
{

    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * @param $user_id int
     * @param $url string
     * @param $action string
     * @return bool
     *
     * Checking, if file is api nor form
     */
    public function canAccessUrl($user_id, $url, $action = null) {
        if ($url[0] === '/' || $url[0] === '\\')
            $url = substr($url, 1);
        $url = str_replace('.php', '', $url);
        $splittedUrl = App::getSplittedUrl($url);

        //Other than a simple menu
        if (empty($splittedUrl) || count($splittedUrl) < 3) {
            return true;
        }

        $menu = App::getMenuManager()->getMenuByUrl($splittedUrl[0] . '/' . $splittedUrl[1] . '%');

        if (!empty($menu) && $this->canViewMenu($user_id, $menu->id)) {
            //is API?

            if (preg_grep("/api/i", $splittedUrl)) {
                $methods = [
                    "GET" => 'view',
                    "POST" => 'add',
                    "PUT" => 'edit',
                    "DELETE" => 'delete'
                ];

                if (!array_key_exists($_SERVER['REQUEST_METHOD'], $methods) ||
                    !$this->canUseOption($user_id, $methods[$_SERVER['REQUEST_METHOD']], $menu->id)) {
                    $json = parseJson::error('Vous n\'avez pas le droit');
                    $json->printJson();
                }

                $action = $methods[$_SERVER['REQUEST_METHOD']];
            } else if (preg_grep("/form/i", $splittedUrl) && $_SERVER['REQUEST_METHOD'] === 'POST') {
                $action = empty($_POST['action']) ? null : $_POST['action'];
            } else if ($action === null) {
                $action = end($splittedUrl);
            }
            return $this->canUseOption($user_id, $action, $menu->id);
        }
        return false;
    }

    public function canViewMenu($user_id, $menu_id) {
        $user_right = $this->db->query_log("SELECT activated
                                            FROM `user_rights` u, `menu_bo_options` mo
                                            WHERE u.option_id = mo.id
                                            AND mo.option_value = 'view'
                                            AND u.id = ?
                                            AND mo.menu_bo_id = ?
                                            AND mo.deleted = 0",
                                            [$user_id, $menu_id])->fetch();
        if (!empty($user_right)) {
            if ($user_right->activated) {
                return true;
            } else {
                return false;
            }
        }

        if ($this->hasGroup($user_id)) {
            $group_rights = $this->db->query_log("SELECT activated
                                                FROM `group_rights` g, `menu_bo_options` mo
                                                WHERE g.option_id = mo.id
                                                AND mo.option_value = 'view'
                                                AND g.group_id = ?
                                                AND mo.menu_bo_id = ?
                                                AND mo.deleted = 0",
                                                [$this->getGroupByUserId($user_id)->group_id, $menu_id])->fetch();
            if (!empty($group_rights)) {
                if ($group_rights->activated) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function canUseOption($user_id, $option, $menu_id) {
        $user_right = $this->db->query_log("SELECT activated
                                            FROM `user_rights` u, `menu_bo_options` mo
                                            WHERE u.option_id = mo.id
                                            AND mo.option_value = ?
                                            AND u.id = ?
                                            AND mo.menu_bo_id = ?
                                            AND mo.deleted = 0",
                                            [$option, $user_id, $menu_id])->fetch();
        if (!empty($user_right)) {
            return $user_right->activated;
        }

        if ($this->hasGroup($user_id)) {
            $group_rights = $this->db->query_log("SELECT activated
                                                FROM `group_rights` g, `menu_bo_options` mo
                                                WHERE g.option_id = mo.id
                                                AND mo.option_value = ?
                                                AND g.group_id = ?
                                                AND mo.menu_bo_id = ?
                                                AND mo.deleted = 0",
                [$option, $this->getGroupByUserId($user_id)->group_id, $menu_id])->fetch();
            return !empty($group_rights) && $group_rights->activated;
        }
    }

    public function getAllRightsByUserId($user_id) {
        $rights = [];

        if ($this->hasGroup($user_id)) {
            $group_rights = $this->db->query_log("SELECT *
                                                FROM `group_rights` gr, `menu_bo_options` mbo, `menu_bo` mb
                                                WHERE gr.option_id = mbo.id
                                                AND gr.group_id = ?
                                                AND mbo.menu_bo_id = mb.id
                                                AND mbo.deleted = 0",
                                                [$this->getGroupByUserId($user_id)->group_id])->fetchAll();


            foreach ($group_rights as $k => $v) {
                $v->is_group = true;
                $rights[$v->url][$v->option_value] = $v->activated;
            }
        }

        $user_rights = $this->db->query_log("SELECT u.option_id, mo.option_name, mo.option_value, u.activated, mb.url
                                            FROM `user_rights` u, `menu_bo_options` mo, `menu_bo` mb
                                            WHERE u.option_id = mo.id
                                            AND u.id = ?
                                            AND mo.deleted = 0",
                                            [$user_id])->fetchAll();


        foreach ($user_rights as $k => $v) {
            $v->is_group = false;
            $rights[$v->url][$v->option_value] = $v->activated;
        }

        return $rights;
    }

    public function getAllMenuRightsByUserId($user_id) {
        $menu_rights = [];

        $rights = $this->getAllRightsByUserId($user_id);

        foreach ($rights as $k => $v)
            $menu_rights[$k] = isset($v['view']) && $v['view'] == 1;

        return $menu_rights;
    }

    public function getGroupByUserId($user_id) {
        $ret = $this->db->query_log("SELECT group_id FROM `user` WHERE id = ?", [$user_id]);

        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getGroupByUserToken($user_token) {
        $ret = $this->db->query_log("SELECT group_id FROM `user` WHERE user_token = ?", [$user_token]);

        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function hasGroup($user_id) {
        $group = $this->getGroupByUserId($user_id);
        return $group !== false && $group->group_id > 0;
    }
}
