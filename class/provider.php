<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 20/07/2018
 * Time: 22:42
 */
class provider
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getProviderByUserId($user_id) {
        $ret = $this->db->query_log("SELECT * FROM `provider` WHERE `user_id` = ?", [$user_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getContractsAndCurrentContract($contract) {
        $contracts = [];

        $ret = App::getEnumFromTableAndColumn('provider', 'contract');
        if ($ret === false)
            return false;
        foreach ($ret as $k)
            $contracts[] = ['name' => $k, 'selected' => strcmp($k, $contract) == 0 ? 'selected' : ''];
        return $contracts;
    }
}