<?php

require __DIR__ . '/../vendor/autoload.php';

class InvoiceGenerator
{

    private $companyInfo;
    private $invoiceInfo;
    private $client;
    private $missionsInfo;

    public function __construct($client, $companyInfo, $invoiceInfo, $missionsInfo, $action)
    {
        $this->client = $client;
        $this->companyInfo = $companyInfo;
        $this->invoiceInfo = $invoiceInfo;
        $this->missionsInfo = $missionsInfo;

        $this->generatePdf($action);
    }

    /* Generating the formatted HTML with invoice infos to use html2pdf */
    private function generateHtml()
    {

        /* Every data after ob_start will be put into a buffer */
        ob_start();

        ?>
        <style type="text/css">
            table {
                width: 100%;
                color: black;
                font-family: helvetica;
                line-height: 5mm;
                border-collapse: collapse;
            }
            h2  { margin: 0; padding: 0; }
            p { margin: 5px; }

            .border th {
                border: 1px solid #000;
                color: black;
                background: #FFFFFF;
                padding: 3px;
                font-weight: bold;
                font-size: 13px;
                text-align: center; }
            .border td {
                border: 1px solid #000;
                padding: 5px 10px;
                text-align: center;
            }
            .no-border {
                border-right: 1px solid #000;
                border-left: none;
                border-top: none;
                border-bottom: none;
            }
            .space { padding-top: 20px; }

            .10p { width: 10%; } .15p { width: 15%; }
            .20p { width: 20%; } .25p { width: 25%; }
            .35p { width: 35%; } .40p { width: 40%; }
            .50p { width: 50%; } .60p { width: 60%; }
            .75p { width: 75%; }

        </style>

        <page backtop="10mm" backleft="10mm" backright="10mm" backbottom="10mm" footer="page;">
            <body>
            <table style="margin-top: 10px;">
                <tr>
                    <td class="75p" style="max-width: 350px; max-height: 120px;"><img src=<?= $this->companyInfo['companyLogo'] ;?>></td>
                    <td class="25p"><?= $this->invoiceInfo['num'] . '<br/>Emise le ' . date('d/m/Y', strtotime($this->invoiceInfo['date'])); ?></td>
                </tr>
            </table>

            <table style="vertical-align: top; margin-top: 30px;">
                <tr>
                    <td class="75p">
                        <strong><?php echo $this->companyInfo['name']; ?></strong><br/><br/>
                        <?= $this->companyInfo['siret']; ?><br/>
                        <?= $this->companyInfo['address']; ?><br/>
                        <?= $this->companyInfo['zipcity']; ?><br/><br/>
                    </td>
                    <td class="25p">
                        <strong><?php echo $this->client['firstname'] . " " . $this->client['lastname']; ?></strong><br/>
                        <?php echo $this->client['address']; ?><br/>
                        <?php echo $this->client['zipcity']; ?><br/>
                    </td>
                </tr>
            </table>

            <table style="margin-top: 30px;" class="border">
                <thead>
                <tr>
                    <th class="25p">Designation</th>
                    <th class="10p">Date</th>
                    <th class="15p">Prix unitaire HT</th>
                    <th class="10p">Quantité</th>
                    <th class="10p">Total HT</th>
                    <th class="10p">TVA</th>
                    <th class="10p">Prix TTC</th>
                </tr>
                </thead>
                <tbody>
                <?php
                     $cnt = $this->missionsInfo['mission_nb'];
                     $total_invoice_price = 0;
                     $reduction_total = 0;
                     $tva_total = 0;
                     for($i = 0; $i < $cnt; $i++) {
                        $ti = $this->missionsInfo[$i];
                    ?>
                    <tr>
                        <td><?= $ti['description']; ?></td>
                        <td><?= date('d/m/Y', strtotime($ti['datePrestation'])); ?></td>

                        <?php
                            $total_cart_price = $ti['unitPricePrestation']*$ti['quantity'];
                            $hour_cart_tva = $this->computeVat($ti['tva'], $ti['unitPricePrestation'], $ti['auto']);
                            $unit_price_ht = $ti['unitPricePrestation']-$hour_cart_tva;
                            $htPrice = $unit_price_ht * $ti['quantity'];
                            $cart_tva = $hour_cart_tva*$ti['quantity'];
                        ?>

                        <td><?= round($unit_price_ht,2) . '€'; ?></td>
                        <td><?= $ti['quantity']; ?></td>

                        <?php

                            if ($ti['discountType'] == "euro") {
                                    $reduction_total += $ti['discount'];
                            } else if ($ti['discountType'] == "percentage") {
                                    $reduction_total += $total_cart_price * ($ti["discount"] / 100);
                            }

                            if ($ti['promoType'] == "euro") {
                                $reduction_total += $ti['promo'];
                            } else if ($ti['promoType'] == "percentage") {
                                $reduction_total += $total_cart_price * ($ti["promo"] / 100);
                            }

                            $tva_total += $cart_tva;
                            $total_invoice_price += $total_cart_price;
                        ?>

                        <td><?= round($htPrice, 2) . "€"; ?></td>
                        <td><?= round($cart_tva, 2) . '€'; ?></td>
                        <td><?= round($total_cart_price, 2) . '€'; ?></td>
                    </tr>

             <?php
                }
                ?>
                <tr>
                    <td colspan="7" class="space"></td>
                </tr>

                <tr>
                    <td colspan="4" class="no-border"></td>
                    <td class="border" colspan="2">Total TTC</td>
                    <td><?php echo round($total_invoice_price,2) . '€'; ?></td>
                </tr>
                <tr>
                    <td colspan="4" class="no-border"></td>
                    <td class="border" colspan="2">Dont TVA</td>
                    <td><?=  round($tva_total,2)  . '€'?></td>
                </tr>
                <tr>
                    <td colspan="4" class="no-border"></td>
                    <td class="border" colspan="2">Réduction</td>
                    <td><?php
                            echo round($reduction_total, 2) . '€';
                        ?></td>
                </tr>
                <tr>
                    <td colspan="4" class="no-border"></td>
                    <td class="border" colspan="2">Total TTC après réduction</td>
                    <td><?=  round($total_invoice_price - $reduction_total,2)  . '€'?></td>
                </tr>
                </tbody>
            </table>
            </body>
        </page>

<?php
        /* Returning the content of the buffer, so all the html */
        return ob_get_clean();
    }

    public function generatePdf($action)
    {

        $content = $this->generateHtml();
        //. date('dmY', strtotime($this->invoiceInfo['date']))
        $pdf_name = $this->client['firstname'].'_'.$this->client['lastname'].'_'.$this->companyInfo['name'] . $this->invoiceInfo['num'] . '.pdf';
        $pdf = new HTML2PDF("p", "A4", "fr");
        $pdf->pdf->SetTitle($pdf_name);
        $pdf->writeHTML($content);

        if ($action == 'download') {
            try {
                $pdf->Output($pdf_name, 'D');
            } catch (HTML2PDF_exception $e) {
                die($e);
            }
        }else{
            try {
                $pdf->Output($pdf_name);
            } catch (HTML2PDF_exception $e) {
                die($e);
            }
        }
    }


    private function computeVat($vat, $unitPrice, $auto)
    {
        if($auto){
            $hour_comission = $unitPrice - $this->companyInfo['autoSalary'];
            return $hour_comission*($vat/100);
        }else{
            return $unitPrice*($vat/100);
        }
    }
}