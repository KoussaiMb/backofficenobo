<?php

class Auth
{
    private $session;
    private $db;
    private $options = [
        'restrictMsg' => "Sorry, la page à laquelle vous tentez d'accéder n'est pas disponible",
    ];

    public function __construct($session, $db, $options = [])
    {
        $this->options = array_merge($this->options, $options);
        $this->db = $db;
        $this->session = $session;
    }

    public function connect($user)
    {
        $this->session->write('auth', $user);
        try {
            $this->db->query("UPDATE `user` SET `lastLog` = NOW() WHERE id = ?", [$user->id]);
        } catch (PDOException $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
        }
    }

    /*
     * TODO:DEPRICATED
    public function write_restrict($reload_password = false)
    {
        if ($this->who() == 'client' && $this->accesslvl() < count($this->rights['client']) || $this->who() == 'provider' && $this->accesslvl() < count($this->rights['provider'])) {
            $this->session->write('restrict', $this->rights[$this->who()][$this->accesslvl()] . ".php");
        }
        if ($reload_password) {
            $this->session->write('reload_password', 'private/_OLD_reload_password.php');
        }
    }
    */

    public function register($data)
    {
        $token = my_crypt::genToken(64);
        //$salt = TODO: SALT IT
        $password = my_crypt::genHash($data['password']);
        $this->db->query("INSERT INTO `user` SET `email` = ?, `password` = ?, `phone` = ?, `firstname` = ?, `lastname` = ?, `confirm_token` = ?, `who` = ?", [
            $data['email'],
            $password,
            $data['phone'],
            $data['firstname'],
            $data['lastname'],
            $token,
            $data['who']
        ]);
        $user_id = $this->db->lastInsertId();
        $mail = new sendMail($data['email']);
        $mail->confirm($user_id, $token);
    }

    /*
     * TODO:deprecated
     * */
    public function reload_password($data)
    {
        $data = json_decode($data);
        try {
            $this->db->query("UPDATE `user` SET `password` = ?, `password_temp` = NULL WHERE `id` = ?", [
                my_crypt::genHash($data['password']),
                $data['id']
            ]);
            return json_encode(['Status' => 'SUCCESS',]);
        } catch (PDOException $e) {
            return json_encode([
                'Status' => 'ERROR',
                'message' => $e->getMessage()
            ]);
        }
    }
    /**
     * @param $data
     * @return bool
     */
    public function login($data)
    {
        if (empty($data['password']) || empty($data['email'])) {
            return false;
        }
        try {
            $user = $this->db->query("SELECT * FROM `user` WHERE `email` = ? AND `who` IN  ('bo', 'admin', 'governess', 'guestrelation')", [$data['email']])->fetch();
        } catch (PDOException $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
            return false;
        }
        if ($user && password_verify($data['password'], $user->password)) {
            $this->connect($user);
            return true;
        }
        return false;
    }

    public function logout()
    {
        $this->session->delAll();
    }

    /**
     * @param $user_id
     * @param $token
     * @return bool
     */
    public function confirm($user_id, $token)
    {
        try {
            $user = $this->db->query("SELECT * FROM `user` WHERE id=?", [$user_id])->fetch();
            if ($user && $user->confirm_token == $token) {
                $this->db->query("UPDATE `user` SET confirm_token = NULL, confirmed_at =  NOW(), pending = 0, accesslevel = 0 WHERE `id` = ?", [$user_id]);
                $this->connect($user);
                if ($user->who == 'customer') {
                    $this->db->query("INSERT INTO `customer` SET `user_id` = ?", [$user->id]);
                } elseif ($user->who == 'provider') {
                    $this->db->query("INSERT INTO `provider` SET `user_id` = ?", [$user->id]);
                }
                return true;
            }
        } catch (PDOException $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
            return false;
        }
        return false;
    }

    public function user()
    {
        if (!$this->session || !$this->session->read('auth')) {
            return null;
        }
        return $this->session->read('auth');
    }

    /*
     * 0 : client
     * 1 : provider
     * 2 : bo
     * 3 : admin
     */
    public function who()
    {
        $auth = $this->session->read('auth');
        if (!$auth)
            return null;
        return $auth->who;
    }

    public function accesslvl()
    {
        $auth = $this->session->read('auth');
        if (!$auth)
            return null;
        return $auth->accesslevel;
    }

    public function forget($email)
    {
        try {
            $token = my_crypt::genToken(60);
            $this->db->query("UPDATE `user` SET reset_token = ?, reset_asked_at = NOW() WHERE `email` = ?", [$token, $email]);
            return App::getMail()->forget($email, $token);
        } catch (PDOException $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
            return false;
        }
    }

    /**
     * @param $user_id
     * @param $token
     * @return bool
     */

    public function reset($user_id, $token)
    {
        try {
            $user = $this->db->query("SELECT * FROM `user` WHERE id=?", [$user_id])->fetch();
        } catch (PDOException $e) {
            App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
            return false;
        }
        if ($user && $user->reset_token == $token) {
            $password = my_crypt::genToken(20);
            $hashpassword = my_crypt::genHash($password);
            try {
                $this->db->query("UPDATE `user` SET reset_token = NULL, `password` = ?, password_temp = 1 WHERE `id` = ?", [$hashpassword, $user_id]);
            } catch (PDOException $e) {
                App::addLog(__FUNCTION__, $e->getMessage(), $this->session->read('user')->id, parseStr::after_trailing_slash(__FILE__));
                return false;
            }
            $mail = new sendMail($user->email);
            $mail->reset($password);
            return true;
        }
        return false;
    }

     //TODO: deprecated

    public function customRestrict($list_user, $allow = true){
        if (!$this->session->read('auth')) {
            $this->session->setFLash('danger', $this->options['restrictMsg']);
            App::redirect('../login');
        }
        $who = $this->who();
        $accesslevel = $this->accesslvl();
        if ($allow == true)
            $this->isAllowed($list_user, $who, $accesslevel);
        if ($allow == true)
            $this->isDisallowed($list_user, $who, $accesslevel);
        App::redirect('../login');
    }

    //TODO: deprecated

    private function isAllowed ($list_user, $who, $accesslevel) {
        foreach($list_user as $k => $v) {
            if ($k == $who && (int)$accesslevel >= (int)$v) {
                return true;
            }
        }
        return false;
    }
    //TODO: deprecated

    private function isDisallowed ($list_user, $who, $accesslevel) {
        foreach($list_user as $k => $v) {
            if ($k == $who && (int)$accesslevel <= (int)$v) {
                return false;
            }
        }
        return true;
    }

    public function restrict()
    {
        if (!$this->session->read('auth')) {
            $this->session->setFLash('danger', $this->options['restrictMsg']);
            App::redirect('../login');
        } elseif ($this->session->read('just_log')) {
            $this->session->delete('just_log');
            $this->restrict_strong();
        }
    }
    public function restrict_strong()
    {
        if ($this->session->read('restrict')) {
            App::redirect($this->session->read('restrict'));
            exit(0);
        }
    }

    public function restrict_user($who, $AccessLevel)
    {
        if ($this->who() != $who || $this->accesslvl() < $AccessLevel){
            Session::getInstance()->setFlash('danger', 'La page que vous tentez d\afficher n\'est pas disponible');
            App::redirect('../login');
            exit(0);
        }
    }
}