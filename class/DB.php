<?php

class DB
{
    private $db;

    public function __construct($host, $database, $username, $password)
    {
        try {
            $this->db = new PDO("mysql:host=$host;dbname=$database", $username, $password,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
                    //PDO::ATTR_EMULATE_PREPARES => false //TODO:check if it's necessary
                ));
        }catch(PDOException $e){
            //TODO: Put in logs
            $message = $e->getMessage();
            die("<h1>Impossible de se connecter à la base de donnée : $message</h1>");//TODO: Remove
        }
    }

    public function query_log($sql, $params = false, $deep = 2){
        if ($params) {
            try {
                $req = $this->db->prepare($sql);
                $req->execute($params);
            } catch (PDOException $e){
                App::logPDOException($e, $deep);
                $req = false;
            }
        } else {
            try {
                $req = $this->db->query($sql);
            } catch (PDOException $e) {
                App::logPDOException($e, $deep);
                $req = false;
            }
        }
        return $req;
    }

    public function query($sql, $params = false){
        if ($params) {
                $req = $this->db->prepare($sql);
                $req->execute($params);
        } else {
                $req = $this->db->query($sql);
        }
        return $req;
    }

    public function getRow($row, $table, $k, $v){
        return $this->query("SELECT `$row` FROM `$table` WHERE `$k`= ?", [$v])->fetch();
    }
    public function lastInsertId(){
        return $this->db->lastInsertId();
    }
    public function nbRow($table, $field = false, $option = false){
        if (!$field){
            return $this->db->query("SELECT COUNT(*) as count FROM $table");
        } elseif (!$option) {
            return $this->db->query("SELECT COUNT($field) as count  FROM $table");
        } else {
            return $this->db->query("SELECT COUNT($field) as count FROM $table WHERE $field = ?", [$option]);
        }
    }
    public function rollBack(){
        return $this->db->rollBack();
    }
    public function beginTransaction(){
        $this->db->beginTransaction();
    }
    public function commit(){
        $this->db->commit();
    }
}