<?php
class planningHTML
{
    private $address_id;
    private $address_token;
    private $user_token;
    private $data;

    function __construct($data)
    {
        $this->address_id = $data['address_id'];
        $this->address_token = $data['address_token'];
        $this->user_token = $data['user_token'];
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////bring back of data////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function getData()
    {
        $this->data['data_user'] = $this->get_Data_User();
        $this->data['data_planning'] = $this->get_Data_Planning();
        $this->data['data_cardex'] = $this->get_Data_Cardex();
    }




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////bring back of planning data////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function get_Data_User(){
        return ((ARRAY)(App::getUser()->getUserCostumerForMailCardexByUserToken($this->user_token)));
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////bring back of planning data////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function get_Data_Planning()
    {
        $maintenance = App::getMaintenancePlanning($this->address_token);
        $data_informated = $maintenance->getMP();
        return $this->formatDataPlanning($data_informated);
    }

    private function formatDataPlanning($array)
    {
        $data_formated = array();
        for ($j = 0; $j < $array[0]->maintenance_len; $j++)
            array_push($data_formated, $this->get_dataByPosition($array, $j + 1));
        return $data_formated;
    }

    private function get_dataByPosition($array, $position)
    {
        $data = array();
        foreach ($array as $task) {
            if ($task->position == $position)
                array_push($data, [
                    'task_name' => $task->name,
                    'task_type' => $task->type_task,
                    'task_time' => $task->task_time,
                    'task_logo' => $task->logo
                ]);
        }
        return $data;
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////bring back of cardex data//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function get_Data_Cardex()
    {
        $cardex = App::getCardex();
        $cardex_data['room_list'] = $cardex->getDescriptionByAddressTokenForGeneratePdf($this->address_id);
        $this->formatDataCardex($cardex_data['room_list']);
        $cardex_data['product_list'] = $cardex->getProductLocalizationInfoByAddressToken($this->address_token);
        return $cardex_data;
    }

    private function formatDataCardex(&$cardex_data)
    {
        $tmp = array();
        foreach ($cardex_data as $key => $value) {
            $cardex_data[$key]['room_description'] = (ARRAY)$cardex_data[$key]['room_description'];
        }
        for ($i = 0; $i < count($cardex_data) - 1; $i++) {
            for ($j = $i + 1; $j < count($cardex_data); $j++) {
                if (($cardex_data[$j]['room_name'] == $cardex_data[$i]['room_name']) and ($cardex_data[$i]['room_name'] != 'verified') and ($cardex_data[$j]['room_name'] != 'verified')) {
                    $cardex_data[$i]['room_description'][count($cardex_data[$i]['room_description'])] = $cardex_data[$j]['room_description'][0];
                    $cardex_data[$j]['room_name'] = 'verified';
                }
            }
        }
        for ($i = 0; $i < count($cardex_data); $i++):
            $cardex_data[$i]['nbr_description'] = count($cardex_data[$i]['room_description']);
        endfor;
        $i = 0;
        foreach ($cardex_data as $element) {
            if ($element['room_name'] != 'verified') {
                $tmp[$i] = $element;
                $i++;
            }
        }
        for ($i = 0; $i < count($tmp) - 1; $i++):
            for ($j = $i + 1; $j < count($tmp); $j++):
                if (count($tmp[$i]['room_description']) > count($tmp[$j]['room_description'])):
                    $permut = $tmp[$i];
                    $tmp[$i] = $tmp[$j];
                    $tmp[$j] = $permut;
                endif;
            endfor;
        endfor;
        $cardex_data = $tmp;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////Generating of the HTML/////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate(){
        $this->getData();
        ob_start();
        ?>

        <style type="text/css">
            table {
                width: 100%;
                color: black;
                font-size: 7pt;
                font-family: helvetica;
                background-color: #ffffff;
            }
            span {
                font-style: inherit;
                font-weight: bold;
            }
            .header {
                /*width: 797px;
                width: 900px;*/
                width: 1150px;
                margin-left: -21px;
                margin-top: -21px;
                background-color: #344860;
            }
            .header td {
                text-align: left;
                height: 40px;
                color: #ffffff;
                font-size: 12pt;
            }
            .noborder {
                margin-top: 40px;
                background-color: #344860;
                color: #ffffff;
            }
            .noborder td {
                height: 15px;
                text-align: left;
                font-style: italic;
                border: none;
            }
            .100p {  width: 100%; }
            .99p { width: 99%; } .97p { width: 97%; }
            .90p { width: 90%; } .10p { width: 10%; }
            .80p { width: 80%; } .20p { width: 20%; }
            .50p { width: 50%; } .30p {  width: 30%; }
            .25p { width: 25%; } .9.5p { width: 9.5%; }
            .4p { width: 4%; } .2p { width: 2%; }
            .1.5p { width: 1.5%; } .0.5p { width: 0.5%; }
            .border {
                border: solid 1px black;
            }
            .border td, th {
                text-align: center;
                vertical-align: middle;
            }
            .title {
                background-color: #344860;
                color: #ffffff;
                text-align: center;
            }
            .border_bottom {
                border-bottom: 1px solid lightgrey;
            }
            .cardex_border {
                margin-top: 70px;
                border: solid 1px black;
            }
            .cardex_border th {
                width: 100%;
                height: 10px;
                text-align: center;
                margin-top: -25px;
            }
            .room_td {
                border: solid 1px black;
                vertical-align: top;
            }
            .room_td td {
                vertical-align: middle;
                text-align: left
            }
            .room_td img {
                float: left;
                width: 30px;
                height: 25px;
                margin-right: 20px;
            }
            ul {
                margin-left: -15px;
            }
            li {
                border-bottom: solid 1px gray;
            }
            h6, h5 {
                margin: 0;
                color: #ffffff;
                text-align: center;
            }
        </style>


        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////--COVER PAGE--////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <page backimg="" backleft="0mm" backright="" backtop="0mm" backbottom="-21mm" backimgx="0" backimgy="0" orientation="P">
            <table style="width: 797px;height: 100%; margin-left: -21px;margin-top: -21px;background-color: #344860">
                <tr><td class="" style="width: 100%; height: 50%"></td></tr>
                <tr>
                    <td class="" style="width: 100%; height: 50%; font-size: 45pt;font-style: italic; color: #ffffff;text-align: center; vertical-align: text-top">Planning d'Entretien</td>
                </tr>
            </table>
        </page>


        <!--//////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////--PLANNING--///////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <page footer="date;heure;page;form">
            <table class="header">
                <tr>
                    <td class="10p">The Header</td>
                    <td class="90p">&nbsp;&nbsp;&nbsp;Alexandre Jenne- Planning d'Entretien</td>
                </tr>
            </table>

            <!--costumer info-->
            <table class="noborder">
                <tr>
                    <td class="50p">
                        <?php if ($this->data['data_user']['field'] == 'Homme') echo "<span>Mr.</span>"; else echo "<span>Mme.</span>";
                        echo " " . $this->data['data_user']['firstname'] . " " . $this->data['data_user']['lastname'] ?>
                    </td>
                    <td class="10p" style="text-align: center"><span>Accés :</span></td>
                    <td class="20p"><span>Batiment :</span><?php echo " " . $this->data['data_user']['batiment']; ?>
                    </td>
                    <td class="20p"><span>Etage :</span><?php echo " " . $this->data['data_user']['floor']; ?></td>
                </tr>
                <tr>
                    <td><span>Téléphone :</span><?php echo " " . $this->data['data_user']['phone']; ?></td>
                    <td></td>
                    <td><span>Digicode_1 :</span><?php echo " " . $this->data['data_user']['digicode']; ?></td>
                    <td><span>Digicode_2 :</span><?php echo " " . $this->data['data_user']['digicode_2']; ?></td>
                </tr>
                <tr>
                    <td><span>Email :</span><?php echo " " . $this->data['data_user']['email']; ?> </td>
                    <td></td>
                    <td colspan="2"><span>interphone :</span><?php echo " " . $this->data['data_user']['doorbell']; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Address :</span><?php echo " " . $this->data['data_user']['address'] . "," . " " . $this->data['data_user']['zipcode'] . "-" . $this->data['data_user']['city'] . " " . $this->data['data_user']['address_ext'] . " " . $this->data['data_user']['country'] . "."; ?>
                    </td>
                    <td></td>
                    <td colspan="2"><span>Porte :</span><?php echo " " . $this->data['data_user']['door']; ?></td>
                </tr>
            </table>

            <!--planning table-->
            <?php if (!empty($this->data['data_planning'])): ?>
                <div>
                    <table class="border" style=" margin-top: 50px;">
                        <thead>
                        <tr>
                            <th class="100p title" style="height: 25px;" colspan="3">
                                <h6>TACHES</h6>
                            </th>
                        </tr>
                        </thead>
                        <?php foreach ($this->data['data_planning'] as $key => $prestation): ?>
                            <?php if (!empty($prestation)):
                                $nbr_task = count($prestation);
                                $config = $this->get_spacing_planning($nbr_task);
                                ?>
                                <tr>
                                    <td class="1.5p"></td>
                                    <td class="97p">
                                        <table style="margin-top: 10px;">
                                            <tr>
                                                <td class="0.5p"></td>
                                                <td class="99p">
                                                    <table class="border" style="margin-top: 10px;">
                                                        <tr>
                                                            <td class="title" style="height: 20px;">
                                                                <h6>N° <?= $key + 1 ?></h6>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="">
                                                                <table>
                                                                    <tr>

                                                                        <?php if ($config['spacing'] != 0): ?>
                                                                            <td class="" style="width: <?= $config['spacing']; ?>px;"></td>
                                                                        <?php endif; ?>

                                                                        <?php foreach ($prestation as $key => $task):
                                                                            $logo = null;
                                                                            if (substr($task['task_logo'], -3) != "svg")
                                                                                $logo = $task['task_logo'];
                                                                            ?>
                                                                            <td>
                                                                                <table style=" width: 70px;">
                                                                                    <tr>
                                                                                        <td class="border_bottom" style="width: 100%;height: 40px;">

                                                                                            <?php if (isset($logo)): ?>
                                                                                                <img class=""
                                                                                                     src="<?= __DIR__ . "/.." . $logo ?>"
                                                                                                     style="width: 45px; height: 35px">
                                                                                            <?php endif; ?>

                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="border_bottom" style="height: 15px;"><?php echo $task['task_name'] ?></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>

                                                                            <?php if ($key + 1 < count($prestation)): ?>
                                                                            <td style="width: <?= $config['task_spacing'] ?>px;margin: 0"></td>
                                                                             <?php endif; ?>

                                                                        <?php endforeach; ?>

                                                                        <?php if ($config['spacing'] != 0): ?>
                                                                            <td class="" style="width: <?= $config['spacing'] ?>px;"></td>
                                                                        <?php endif; ?>

                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="0.5p"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="1.5p"></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
            <?php endif; ?>

        </page>



        <!--///////////////////////////////////////////////////////////////////////////////////////////////////
               //////////////////////////////////////////////--CARDEX--///////////////////////////////////////////////
               ////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <?php
        if (isset($this->data['data_cardex']) and (isset($this->data['data_cardex']['room_list'][0]['room_name']) or isset($this->data['data_cardex']['product_list'][0]['lp_value']))):
            $config_cardex = $this->getConfigCardex($this->data['data_cardex']['room_list']);
            $nb_rows = $config_cardex['nb_rows'];
            switch ($config_cardex['configuration']) {
                case 0: {
                    $class[0] = $class[1] = $class[2] = $class[3] = "25p";
                }
                    break;
                case 2: {
                    $class[0] = $class[3] = "25p";
                    $class[1] = $class[2] = "0p";
                }
                    break;
                case 1: {
                    $class[0] = $class[1] = $class[3] = "25p";
                    $class[2] = "0p";
                }
                    break;
            }
            if (!isset($this->data['data_cardex']['product_list'][0]['lp_value']))
                $class[3] = '0p';
            ?>
            <page footer="date;page;form" style="text-align:center" backcolor="">
                <table class="header">
                    <tr>
                        <td class="10p">The Header</td>
                        <td class="90p">&nbsp;&nbsp;Alexandre Jenne- Particularités</td>
                    </tr>
                </table>
                <table class="cardex_border"  cellspacing="15">
                    <tr style="background-color: #344860;color: #ffffff;">
                        <th  colspan="4"><h4>description des taches par piece</h4></th>
                    </tr>
                    <?php
                    for ($i = 0; $i < $nb_rows; $i++):?>
                        <?php
                        $row = $config_cardex['matrix'][$i];
                        $row_nbr[$i] = $row['height'];
                        if ($i != 0)
                            $row_nbr[$i] = $row_nbr[$i - 1] + $row['height'];
                        ?>
                        <tr>
                            <?php
                            for ($j = 0; $j < 3; $j++):
                                if (isset($row[$j]) and $class[$j] != '0p'):
                                    $room = $this->data['data_cardex']['room_list'][$row[$j]];
                                    $logo = null;
                                    if (substr($room['room_url'], -3) != "svg")
                                        $logo = $room['room_url'];
                                    ?>
                                    <td class="<?= $class[$i]; ?> room_td"
                                        style="height: <?= $row['height'] ?>;border: none">
                                        <table style="border: solid 1px #000000">
                                            <tr style="background-color: #344860;">
                                                <td class="100p" style="height: 25px;color: #ffffff">
                                                    <?php if (isset($logo)): ?>
                                                        <img class="" src="<?= __DIR__ . "/.." . $logo ?>">
                                                    <?php endif; ?>
                                                    <h6 style="text-align: start; margin: 10px 0 0 0"><?php echo $room['room_name']; ?></h6>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 100%">
                                                    <?php foreach ($room['room_description'] as $key => $description): ?>
                                                        <ul class="100p">
                                                            <li><?php echo $description; ?></li>
                                                        </ul>
                                                    <?php endforeach; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <?php if ($i == 0): ?>
                                <?php if ($class[3] != '0p'): ?>
                                    <?php
                                    $rowspan = $this->get_rowspan($config_cardex['matrix']);
                                    ?>
                                    <td class="<?= $class[3]; ?>" style="border: solid 1px black; vertical-align: top"
                                        rowspan="<?= $rowspan ?>">
                                        <table style="height: 100%; vertical-align: middle">
                                            <tr style="background-color: #344860; color: #ffffff;">
                                                <td class="100p" style="height: 30px; vertical-align: middle">
                                                    <h6>
                                                        Localisation Des Produits
                                                    </h6>
                                                </td>
                                            </tr>
                                            <?php foreach ($this->data['data_cardex']['product_list'] as $product): ?>
                                                <?php
                                                $logo=null;
                                                if (substr($product['lp_url'], -3) != "svg")
                                                    $logo = $product['lp_url'];
                                                ?>
                                                <tr >
                                                    <td style="padding-top: 10px">
                                                        <table class="room_td" style="border: solid 1px black">
                                                            <tr style="background-color: green;">
                                                                <td class=""  style="width: 98.5%; height: 25px; color: #ffffff">
                                                                    <?php if (isset($logo)): ?>
                                                                        <img class="" src="<?= __DIR__ . "/.." . $logo ?>">
                                                                    <?php endif; ?>
                                                                    <h6 style="text-align: start; margin-top: 10px;padding-left: 50px"><?php echo $product['lp_value'];?></h6>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border-bottom: solid 1px gray; height: 20px; text-align: center"><?php echo $product['lr_value']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 20px; text-align: center"><?php echo $product['description']; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </td>
                                <?php endif; ?>
                            <?php endif;?>
                        </tr>
                    <?php endfor; ?>
                </table>
            </page>
        <?php endif;?>

        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////--END--////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

        <page backimg="" backleft="0mm" backright="" backtop="0mm" backbottom="-21mm" backimgx="0" backimgy="0">
            <table style="width: 797px;height: 100%; margin-left: -21px;margin-top: -21px;background-color: #344860">
                <tr><td class="" style="width: 100%; height: 50%"></td></tr>
                <tr>
                    <td class="" style="width: 100%; height: 50%; font-size: 45pt;font-style: italic; color: #ffffff;text-align: center; vertical-align: text-top">Planning d'Entretien</td>
                </tr>
            </table>
        </page>


        <?php
        try {
            $ret = ob_get_clean();
            return $ret;
        } catch (Exception $e) {
            return ("Error " . $e->getCode() . " at: " . $e->getLine() . " " . $e->getMessage() . "<br/>" . $e->getFile());
        }

    }

    private function get_spacing_planning($nbr_task){
        $task_spacing = 15;
        $spacing=((700-((72*$nbr_task)+($nbr_task*20)))/2)+3;

        switch ($nbr_task){
            case 2:{
                $task_spacing = 80;
                $spacing=((700 - ((72 * $nbr_task) + ($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 6;
            }
                break;
            case 3:{
                $task_spacing = 70;
                $spacing=(( 700 - (( 72 * $nbr_task) + ($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 12;
            }
                break;
            case 4:{
                $task_spacing = 60;
                $spacing=((700 - ((72 * $nbr_task)+($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 18;
            }
                break;
            case 5:{
                $task_spacing = 50;
                $spacing=((700 - ((72 * $nbr_task)+($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 21;
            }
                break;
            case 6:{
                $task_spacing = 30;
                $spacing = ((700 - ((72 * $nbr_task) + ($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 24;

            }
                break;
            case 7:{
                $task_spacing = 15;
                $spacing = ((700 - ((72 * $nbr_task) + ($task_spacing * ($nbr_task - 1) + ($nbr_task * 20)))) / 2) + 30;

            }
                break;
        }
        $planning_config['spacing'] = $spacing;
        $planning_config['task_spacing'] = $task_spacing;
        return $planning_config;
    }

    private function get_rowspan($data){
        $height = 0;

        foreach($data as $key => $value):
            $height += $value['height'];
            if($height > (865-((($key+2)*15)+60)))
                return $key;
        endforeach;
        return (count($data));
    }

    private function getConfigCardex($room_list){
        $count=(count($room_list) / 3);
        if(($count-floor($count)) > 0)
            $config_cardex['nb_rows'] = floor($count) + 1;
        else
            $config_cardex['nb_rows'] = floor($count);

        $config_cardex['configuration'] = 0;

        for($i=0; $i < $config_cardex['nb_rows']; $i++):
            $config_cardex['matrix'][$i] = array();

        endfor;

        $j = 0; $i = 0;
        foreach($room_list as $key => $value){
            $config_cardex['matrix'][$i][$j] = $key;
            $j++;
            if($j > 2){
                $j=0; $i++;
            }

        }

        for($i=0; $i < $config_cardex['nb_rows']; $i++):
            $max_height = $room_list[$config_cardex['matrix'][$i][0]]['nbr_description'];
            for ($j=0; $j < count($config_cardex['matrix'][$i]); $j++):
                $index_room = $config_cardex['matrix'][$i][$j];
                if (!is_array($index_room)):
                    if ($max_height < $room_list[$index_room]['nbr_description'])
                        $max_height = $room_list[$config_cardex['matrix'][$i][$j]]['nbr_description'];
                endif;
            endfor;
            $config_cardex['matrix'][$i]['height'] = ($max_height * 20) + 50;
        endfor;
        return $config_cardex;
    }

}