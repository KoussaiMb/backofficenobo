<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 09/12/2017
 * Time: 19:48
 */
class promocode
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getPromoCodes()
    {
        return $this->db->query_log("SELECT * FROM `promocode` WHERE `deleted` = 0 ORDER BY `name`")->fetchAll();
    }

    public function getPromocodeByCode($code)
    {
        return $this->db->query_log("SELECT * FROM `promocode` WHERE `deleted` = 0 AND `code` = ?",[$code])->fetch();
    }

    public function getPromoCodeById($promoCode_id)
    {
        return $this->db->query_log("
SELECT P.*, U.firstname, U.lastname 
FROM promocode P, `user` U 
WHERE P.created_by = U.id 
AND P.id = ?", [$promoCode_id])->fetch();
    }

    public function addPromoCode($data)
    {
        $ret = $this->db->query_log('
INSERT INTO `promocode` 
SET `name` = ?, 
    `code` = ?, 
    `percentage` = ?, 
    `euro` = ?, 
    `start` = ?, 
    `end` = ?, 
    `number` = ?, 
    `promocodeGroup_id` = ?, 
    `created_by` = ?',
            [
                $data['name'],
                $data['code'],
                $data['percentage'],
                $data['euro'],
                $data['start'],
                $data['end'],
                $data['number'],
                $data['promocodeGroup_id'],
                $data['created_by']
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function deletePromoCode($promoCode_id)
    {
        $ret = $this->db->query_log("UPDATE `promocode` SET `deleted` = 1 WHERE `id` = ?", [$promoCode_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function editPromoCode($data)
    {
        $ret = $this->db->query('UPDATE `promocode` SET `code` = ?, `name` = ?, `percentage` = ?, `euro` = ?, `start` = ?, `end` = ?, `number` = ?, `promocodeGroup_id` = ? WHERE `id` = ?',
            [
                $data['code'],
                $data['name'],
                $data['percentage'],
                $data['euro'],
                $data['start'],
                $data['end'],
                $data['number'],
                $data['promocodeGroup_id'],
                $data['id']
            ]);
        if ($ret === false)
            return $ret;
        return $ret->rowCount();
    }

    public function getAutocompletePromocode()
    {
        try {
            $autocomplete = [];

            $ret = $this->db->query_log("SELECT * FROM `promocode` 
                                          WHERE `deleted` = 0 ")->fetchAll();
            if (!$ret){
                return false;
            }
            foreach ($ret as $key => $value) {
                array_push($autocomplete, [
                    'value' => $value->code,
                    'data' => $value->id
                ]);
            }
            return json_encode($autocomplete);
        } catch (PDOException $e) {
            return null;
        }
    }
}