<?php
/**
 * Created by PhpStorm.
 * User: Jugurta
 * Date: 01/04/2018
 * Time: 19:46
 */
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../class/App.php';
require_once __DIR__ . '/../class/planningHtml.php';

class generatePdf
{
    private $form_user;
    private $type;
    private $html;

    function __construct($form_user, $type = "planning")
    {
        $this->type = $type;
        $this->form_user = $form_user;
    }

    public function generateHTML()
    {
        $planning = App::get_instance_planningHTML($this->form_user);
        $this->html = $planning->generate();
    }

    public function generate_Pdf()
    {
        try {
            $this->generateHTML();
            ob_get_clean();

            $pdf_name = $this->form_user['firstname'] . '_' . $this->form_user['lastname'] . '.pdf';
            $html2pdf = new HTML2PDF("P", "A4", "fr");//error 1
            $html2pdf->pdf->SetTitle($pdf_name);
            $html2pdf->pdf->SetMyFooter(true, true, true, true);
            $html2pdf->setTestTdInOnePage(false);
            $html2pdf->writeHTML($this->html);

            if ($this->form_user['action'] == 'send') {
                $pdf = $html2pdf->Output('','S');
                return ($pdf);
            } else {
                $html2pdf->Output($pdf_name);
            }
        } catch (Exception $e) {
            return $e;
        }
    }
}