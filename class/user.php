<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 13/07/2017
 * Time: 14:21
 */
class user
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function autocompleteAll() {
        $autocomplete = [];

        $ret = $this->db->query_log("SELECT user_token, firstname, lastname FROM `user` WHERE `deleted` = 0")->fetchAll();
        if ($ret == false)
            return false;
        foreach ($ret as $key => $value) {
            array_push($autocomplete, [
                'value' => $value->firstname . ' ' . $value->lastname,
                'data' => [
                    'user_token' => $value->user_token,
                    'firstname' => $value->firstname,
                    'lastname' => $value->lastname
                ],
            ]);
        }
        return json_encode($autocomplete);
    }

    public function getAllClientNotInWaitingList() {
        return $this->db->query("SELECT * FROM `user` u WHERE u.`deleted` = 0 AND u.`who` = 'customer' AND u.pending = 0")->fetchAll();
    }

    public function getClientByToken($user_token)
    {
        return $this->db->query_log("SELECT * FROM `user` u INNER JOIN `customer` c ON u.id = c.user_id WHERE u.`user_token` = ? AND u.`deleted` = 0 AND u.`who` = 'customer'", [$user_token])->fetch();
    }

    public function getClientById($id)
    {
        return $this->db->query_log("SELECT * FROM `user` u INNER JOIN `customer` c ON u.id = c.user_id WHERE u.`id` = ? AND u.`deleted` = 0 AND u.`who` = 'customer'", [$id])->fetch();
    }

    public function getProviderByToken($user_token)
    {
        return $this->db->query_log("SELECT *, p.id as provider_id FROM `user` u INNER JOIN `provider` p ON u.id = p.user_id WHERE u.`user_token` = ? AND u.`deleted` = 0 AND u.`who` = 'provider'", [$user_token])->fetch();
    }

    public function getProviderById($id)
    {
        return $this->db->query_log("SELECT * FROM `user` u INNER JOIN `provider` p ON u.id = p.user_id WHERE u.`id` = ? AND u.`deleted` = 0 AND u.`who` = 'provider'", [$id])->fetch();
    }

    /*
     * User functions
     */

    public function updatePassword($password, $user_token) {
        $ret = $this->db->query_log("UPDATE `user` SET `password` = ? WHERE `user_token` = ?", [$password, $user_token]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function updateResetToken($user_token, $reset_token) {
        $ret = $this->db->query_log("UPDATE `user` SET `reset_token` = ?, `reset_asked_at` = NOW() WHERE `user_token` = ?", [$reset_token, $user_token]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function canHaveThisPaymentMean($l_payment_mean_id, $user_id) {
        $field = App::getListManagement()->getFieldById($l_payment_mean_id);

        if ($field === false)
            return false;
        if (strpos(strtolower($field),'stripe') !== false)
            $payment_account = $this->hasPaymentAccount($user_id);
        if (!isset($payment_account) || $payment_account->paymentAccount !== null)
            return true;
        return false;
    }

    public function hasPaymentAccount($user__id) {
        $ret = $this->db->query_log('SELECT `paymentAccount` FROM `user` WHERE `id` = ?', [$user__id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function updatePaymentMean($l_payment_mean_id, $user_id) {
        if ($l_payment_mean_id == null)
            $l_payment_mean_id = null;
        $ret = $this->db->query_log('UPDATE `user` SET `l_payment_mean_id` = ? WHERE `id` = ?', [$l_payment_mean_id, $user_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function addUser($data, $who)
    {
        $arrayWho = ['customer', 'provider', 'guestrelation'];

        if (!in_array($who, $arrayWho))
            return 0;
        $password = my_crypt::genHash($data['password']);
        $user_token = my_crypt::genStandardToken();
        $who_token = my_crypt::genStandardToken();
        try {
            $this->db->beginTransaction();
            $this->db->query("INSERT INTO `user` SET `email` = ?, `firstname` = ?, `lastname` = ?, `who` = ?, `pending` = 0, `password` = ?, `gender_id`= ?, user_token = ?, phone = ?, subscribed_at = NOW()", [
                $data['email'],
                $data['firstname'],
                $data['lastname'],
                $who,
                $password,
                $data['gender_id'],
                $user_token,
                $data['phone']
            ]);
            $id = $this->db->lastInsertId();
            if ($who == 'provider') {
                $convention = $this->db->query("SELECT * FROM `convention` WHERE `id` = ?", [$data['convention_id']])->fetch();
                if (empty($convention))
                    throw new Exception('Impossible de récupérer la conventtion');
                $this->db->query("INSERT INTO `provider` SET `user_id` = ?, `provider_token` = ?, `convention_id` = ?, `hoursLeft` = ?", [$id, $who_token, $data['convention_id'], $convention->averageHoursWeek]);
            } else if ($who == 'customer') {
                $this->db->query("INSERT INTO `customer` SET `user_id` = ?, `customer_token` = ?", [$id, $who_token]);
            }
            $this->db->commit();
            return $user_token;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
        } catch (Exception $e) {
            $this->db->rollBack();
            App::logException($e, 1);
        }
        return 0;
    }

    public function addBoUser($data)
    {
        $admin = App::getAuth()->user()->id;
        $password = my_crypt::genHash($data['password']);
        $user_token = my_crypt::genStandardToken();
        $ret = $this->db->query_log("INSERT INTO `user` SET
`email` = ?,
`password` = ?,
`firstname` = ?,
`lastname` = ?,
`user_token` = ?,
`who` = 'bo',
`accesslevel` = 1,
`pending` = 0,
`password_temp` = 1,
`validated_by` = ?,
`group_id` = ?,
`validated_at` = NOW(),
`subscribed_at` = NOW()",
            [
                $data['email'],
                $password,
                $data['firstname'],
                $data['lastname'],
                $user_token,
                $admin,
                $data['group']
            ]);
        if (!$ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function getUserByToken($user_token)
    {
        return $this->db->query_log("SELECT * FROM `user` WHERE `user_token` = ? AND `deleted` = 0", [$user_token])->fetch();
    }


    public function getUserCostumerForMailCardexByUserToken($user_token){
        return $this->db->query_log("SELECT U.`firstname`, U.`lastname`, U.`email`, U.`phone`, U.`photo_profile`, U.`subscribed_at`,
                                     A.`address`, A.`address_ext`, A.`city`, A.`country`, A.`zipcode`, A.`batiment`, A.`digicode`, A.`digicode_2`, A.`floor`, A.`door`, A.`doorbell`,
                                     L.`field`
                                     FROM `user` U
                                     INNER JOIN `list` L ON (U.`gender_id`=L.`id`)
                                     INNER JOIN `address` A ON (U.`id`=A.`user_id`)
                                     WHERE U.`user_token` = ?
                                     AND U.`deleted` = 0",
                                     [$user_token])->fetch();
    }

    public function getUserById($id)
    {
        return $this->db->query_log("SELECT * FROM `user` WHERE `id` = ? AND `deleted` = 0", [$id])->fetch();
    }

    public function getUserTokenFromAddressToken($address_token)
    {
        $ret = $this->db->query_log("SELECT U.`user_token`
                                     FROM `address` A
                                     INNER JOIN `user` U ON (U.`id` = A.`user_id`)
                                     WHERE A.`token` = ?",
                                     [$address_token])->fetch();
        if ($ret != false && $ret != null)
            return $ret->user_token;
        return null;
    }

    /*
     * Address functions
     */

    public function getAddressByToken($address_token)
    {
        $ret = $this->db->query_log("SELECT A.*, A.`token` as address_token, M.`arret` as subway_station, M.`ligne` as subway_line,
                                     NOT(IFNULL((SELECT W.`accepted` FROM `waiting_list` W WHERE A.`id` = W.`address_id` AND W.`deleted` = 0), 1)) as pending
                                     FROM `address` A, `metro_paris` M
                                     WHERE A.`subway_id` = M.`id`
                                     AND A.`token` = ?
                                     AND A.`deleted` = 0",
                                     [$address_token]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getAddressesByUserToken($user_token)
    {
         $ret = $this->db->query_log("SELECT A.*, A.`token` as address_token, M.`arret` as subway_station, M.`ligne` as subway_line, AC.cluster_id, AC.customer_id,
                                     NOT(IFNULL((SELECT W.`accepted` FROM `waiting_list` W WHERE A.`id` = W.`address_id` AND W.`deleted` = 0), 1)) as pending,
                                     ACUS.lockbox_code, ACUS.lockbox_localization
                                     FROM `address` A
                                     LEFT JOIN `metro_paris` M ON (M.`id` = A.`subway_id`)
                                     LEFT JOIN `address_cluster` AC ON (AC.`address_id` = A.`id`)
                                     LEFT JOIN `customer_address` ACUS ON (ACUS.`address_id` = A.`id`)
                                     INNER JOIN `user` U ON (U.`id` = A.`user_id`)
                                     WHERE A.`deleted` = 0
                                     AND U.`user_token`= ?",
                                     [$user_token]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getAddressById($address_id)
    {
         $ret = $this->db->query_log("
SELECT 
A.*, A.`token` as address_token, 
M.`arret` as subway_station, M.`ligne` as subway_line,
IF(W.`accepted` = 1, 0, 1) as pending
FROM `address` A
LEFT JOIN `metro_paris` M ON A.`subway_id` = M.`id`
LEFT JOIN `waiting_list` W ON A.ID = W.address_id
WHERE A.`id` = ? AND A.`deleted` = 0
AND (W.`deleted` = 0 OR W.`deleted` IS NULL)",
             [$address_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getAddressHomeByUserToken($user_token)
    {
        return $this->db->query_log("SELECT A.*, A.`token` as address_token
                                     FROM `address` A
                                     INNER JOIN `user` U ON U.`id` = A.`user_id`
                                     WHERE A.`deleted` = 0
                                     AND A.`home` = 1
                                     AND U.`user_token` = ?
                                     LIMIT 0,1",
                                     [$user_token])->fetch();
    }

    public function getAddressHomeById($id)
    {
        return $this->db->query_log("SELECT A.*, A.`token` as address_token, M.`arret` as subway_station, M.`ligne` as subway_line
                                     FROM `address` A
                                     LEFT JOIN `metro_paris` M ON(M.`id` = A.`subway_id`)
                                     WHERE A.`deleted` = 0 AND A.`home` = 1 AND A.`user_id` = ? LIMIT 0,1", [$id])->fetch();
    }

    public function getCustomerByUserId($user_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `customer` WHERE `user_id` = ?", [$user_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    private function transformPhotoToDelete($photo_url)
    {
        $pos = strrpos ($photo_url , '/') + 1;
        return substr($photo_url, 0, $pos) . "OLD-" . substr($photo_url, $pos);
    }

    public function deletePhotoByToken($user_token)
    {
        try {
            $photo_url = $this->getUserByToken($user_token);
            if (empty($photo_url) || !file_exists($photo_url))
                throw new Exception('Previous request failed');
            $delete_url = $this->transformPhotoToDelete($photo_url);
            if (!rename($photo_url, $delete_url))
                throw new Exception('Previous request failed');
            $ret = $this->db->query("UPDATE `user` SET `photo_profile` = null WHERE `user_token` = ?", [$user_token])->fetch();
            return $ret->rowCount();
        } catch (PDOException $e) {
            return 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function deletePhotoById($id)
    {
        try {
            $photo_url = $this->getUserById($id);
            if (empty($photo_url) || !file_exists($photo_url))
                throw new Exception('Previous request failed');
            $delete_url = $this->transformPhotoToDelete($photo_url);
            if (!rename($photo_url, $delete_url))
                throw new Exception('Previous request failed');
            $ret = $this->db->query("UPDATE `user` SET `photo_profile` = null WHERE `id` = ?", [$id])->fetch();
            return $ret->rowCount();
        } catch (PDOException $e) {
            return 0;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function updateUserFromCustomerByToken($data)
    {
        $ret =  $this->db->query_log("UPDATE `user` SET `email` = ?, `phone` = ?, `lastname` = ?, `firstname` = ?, `birthdate` = ?, `job_id` = ?, `photo_profile` = ?, `gender_id` = ?, `maritalStatus_id` = ? WHERE `user_token` = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['job_id'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['maritalStatus'],
                $data['user_token']
            ]);
        if($ret != false)
            return $ret->rowCount();
        return 0;
    }

    public function updateUserFromCustomerById($data)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `email` = ?, `phone` = ?, `lastname` = ?, `firstname` = ?, `birthdate` = ?, `job_id` = ?, `photo_profile` = ?, `gender_id` = ?, `maritalStatus_id` = ? WHERE `id` = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['job_id'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['maritalStatus'],
                $data['id']
            ]);
        if ($ret == false)
            return $ret->rowCount();
        return $ret;
    }

    public function updateUserFromProviderById($data)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `email` = ?, `phone` = ?, `lastname` = ?, `firstname` = ?, `birthdate` = ?, `photo_profile` = ?, `gender_id` = ? WHERE `id` = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['id']
            ]);
        if ($ret == false)
            return $ret->rowCount();
        return $ret;
    }

    public function updateUserFromProviderByToken($data)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `email` = ?, `phone` = ?, `lastname` = ?, `firstname` = ?, `birthdate` = ?, `photo_profile` = ?, `gender_id` = ? WHERE `id` = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['id']
            ]);
        if ($ret == false)
            return $ret->rowCount();
        return $ret;
    }

    public function deleteUserByToken($user_token)
    {
        //Select user by token
        $user = $this->db->query_log("SELECT * FROM `user` WHERE `user_token` = ?", [$user_token])->fetch();
        if ($user == false)
            return 0;
        return $this->deleteUser($user);
    }

    public function deleteUserById($user_id) {
        $user = $this->db->query_log("SELECT * FROM `user` WHERE `id` = ?", [$user_id])->fetch();
        if ($user == false)
            return 0;
        return $this->deleteUser($user);
    }

    /*
         * process COMMON 01/05/2018
         *
         * UPDATE TABLE user: deleted 1
         * DELETE TABLE user_rights: user_token
         * INSERT INTO TABLE stop_nobo
         *
         * process deletion bo 01/05/2018
         *
         * process deletion governess 01/05/2018
         *
         * UPDATE TABLE governess : deleted 1
         * UPDATE TABLE cluster: user_id = NULL
         *
         * process deletion provider 01/05/2018:
         *
         * UPDATE TABLE provider: deleted 1, cluster_id NULL
         * UPDATE TABLE agenda: pending 1, user_id NULL
         * UPDATE TABLE address: main_provider NULL
         *
         * process deletion customer 01/05/2018:
         *
         * UPDATE TABLE customer: deleted 1
         * UPDATE waiting_list: deleted 1
         * DELETE TABLE agenda
         * DELETE TABLE dispo_client
         *
    */
    private function deleteUser($user)
    {
        try {
            $this->db->beginTransaction();

            if ($user->who == 'customer') {
                $this->db->query("UPDATE `waiting_list` SET `deleted` = 1 WHERE customer_id = (SELECT `id` FROM `customer` WHERE `user_id` = " . $user->id . ")");
                $this->db->query("DELETE FROM `agenda` WHERE `user_id` = ?", [$user->id]);
                $this->db->query("DELETE FROM `dispo_client` WHERE `user_id` = ?", [$user->id]);
                $this->db->query("UPDATE `customer` SET `deleted` = 1 WHERE `user_id` = " . $user->id);
            } else if ($user->who == 'provider') {
                $this->db->query("UPDATE `agenda` SET `pending` = 1, `provider_id` = NULL WHERE `user_id` = (SELECT `id` FROM `provider` WHERE `user_id` = " . $user->id . ")");
                $this->db->query("UPDATE `address` SET `main_provider` = NULL WHERE `main_provider` = (SELECT `id` FROM `provider` WHERE `user_id` = " . $user->id . ")");
                $this->db->query("UPDATE `provider` SET `deleted` = 1, `cluster_id` = NULL WHERE `user_id` = " . $user->id);
            } else if ($user->who == 'governess') {
                $this->db->query("UPDATE `governess` SET `deleted` = 1 WHERE `user_id` = " . $user->id);
                $this->db->query("UPDATE `cluster` SET `governess_id` = NULL WHERE `governess_id` = (SELECT `id` FROM `governess` WHERE `user_id` = " . $user->id);
            }
            $this->db->query("UPDATE `user` SET `deleted` = 1, `deleted_at` = NOW() WHERE `id` = " . $user->id);
            $this->db->query("DELETE FROM `user_rights` WHERE `user_token` = ?", [$user->user_token]);
            $this->db->query("INSERT INTO `stop_nobo` SET `date_stop` = NOW(), `user_id` = " . $user->id);

            $this->db->commit();
        } catch (PDOException $e) {
            App::logPDOException($e, 1);
            return 0;
        }
        return 1;
    }

    public function validateUserByToken($user_token)
    {
        $ppl_doing = App::getAuth()->user()->id;
        $ret = $this->db->query_log("UPDATE `user` SET `pending` = 0, `validated_at` = NOW(), `validated_by` = ? WHERE `user_token` = ?", [$ppl_doing, $user_token]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function unvalidateUser($user_id)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `pending` = 1 WHERE `id` = ?", [$user_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateUserAndCustomer($data)
    {

        $data_user = [
            'U.email' => $data['email'],
            'U.phone' => $data['phone'],
            'U.lastname' => $data['lastname'],
            'U.firstname' => $data['firstname'],
            'U.birthdate' => empty($data['birthdate']) ? null : $data['birthdate'],
            'U.job_id' => $data['job_id'],
            'U.photo_profile' => $data['photo_profile'],
            'U.gender_id' => $data['gender_id'],
            'U.maritalStatus_id' => $data['maritalStatus'],
            'C.credit' => $data['credit'],
            'C.customer_package_id' => empty($data['customer_package_id']) ? null : $data['customer_package_id'],
            'C.l_acquisition_id' => $data['l_acquisition_id'],
            'U.childNb' => empty($data['childNb']) ? null : $data['childNb'],
            'C.stop_nobo' => empty($data['stop_nobo']) ? null : $data['stop_nobo']
        ];

        $keys = implode(' = ?, ', array_keys($data_user));
        $values = array_values($data_user);

        $ret = $this->db->query_log("
UPDATE `user` U INNER JOIN `customer` C ON U.id = C.user_id SET "
            .$keys. " = ? WHERE U.user_token = '" .$data['user_token'] ."'", $values);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateUserAndProvider($data)
    {
        $ret = $this->db->query_log("UPDATE `user` U INNER JOIN `provider` P ON U.id = P.user_id SET U.email = ?, U.phone = ?, U.lastname = ?, U.firstname = ?, U.birthdate = ?, U.photo_profile = ?, U.gender_id = ?, U.maritalStatus_id = ?, P.contract = ?, P.convention_id = ?, U.childNb = ? WHERE U.user_token = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['maritalStatus'],
                empty($data['contract']) ? null : $data['contract'],
                $data['convention_id'],
                empty($data['childNb']) ? null : $data['childNb'],
                $data['user_token']
            ]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateUser($data)
    {
        $ret = $this->db->query_log("UPDATE `user` U SET U.email = ?, U.phone = ?, U.lastname = ?, U.firstname = ?, U.birthdate = ?, U.photo_profile = ?, U.gender_id = ?, U.maritalStatus_id = ?, U.childNb = ? WHERE U.user_token = ?",
            [
                $data['email'],
                $data['phone'],
                $data['lastname'],
                $data['firstname'],
                empty($data['birthdate']) ? null : $data['birthdate'],
                $data['photo_profile'],
                $data['gender_id'],
                $data['maritalStatus'],
                empty($data['childNb']) ? null : $data['childNb'],
                $data['user_token']
            ]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function getAutocompleteProvider() {
        $autocomplete = [];

        $ret = $this->db->query_log("SELECT user_token, firstname, lastname FROM `user` WHERE `deleted` = 0 AND `who` = 'provider' ORDER BY firstname ASC, lastname ASC")->fetchAll();
        if ($ret == false)
            return false;
        foreach ($ret as $key => $value) {
            array_push($autocomplete, [
                'value' => $value->firstname . ' ' . $value->lastname,
                'data' => $value->user_token
            ]);
        }
        return json_encode($autocomplete);
    }

    public function getAutoComplete($who) {
        $autocomplete = [];

        $ret = $this->db->query_log("SELECT user_token, firstname, lastname FROM `user` WHERE `deleted` = 0 AND (? = '' OR `who` = ?) ORDER BY firstname ASC, lastname ASC", [$who, $who])->fetchAll();
        if ($ret == false)
            return false;
        foreach ($ret as $key => $value) {
            array_push($autocomplete, [
                'value' => $value->firstname . ' ' . $value->lastname,
                'data' => $value->user_token
            ]);
        }
        return json_encode($autocomplete);
    }

    public function getProvidersHired()
    {
        return $this->db->query_log("SELECT user_token, firstname, lastname, email, phone, pending FROM `user`
                                      WHERE `deleted` = 0 AND `who` = 'provider' AND `accesslevel` > 0
                                      ORDER BY firstname ASC, lastname ASC")->fetchAll();
    }

    public function getHired($who)
    {
        return $this->db->query_log("SELECT user_token, firstname, lastname, email, phone, pending FROM `user`
                                      WHERE `deleted` = 0 AND `who` = ? AND `accesslevel` > 0
                                      ORDER BY firstname ASC, lastname ASC", [$who])->fetchAll();
    }

    public function getProvidersNotHired()
    {
        return $this->db->query_log("SELECT user_token, firstname, lastname, email, phone, pending FROM `user`
                                      WHERE `deleted` = 0 AND `who` = 'provider' AND `accesslevel` = 0
                                      ORDER BY firstname ASC, lastname ASC")->fetchAll();
    }

    public function getNotHired($who)
    {
        return $this->db->query_log("SELECT user_token, firstname, lastname, email, phone, pending FROM `user`
                                      WHERE `deleted` = 0 AND `who` = ? AND `accesslevel` = 0
                                      ORDER BY firstname ASC, lastname ASC", [$who])->fetchAll();
    }

    public function getWaitingList(){
        return $this->db->query_log("
            SELECT W.*, U.`lastname`, U.`firstname`, U.`user_token`, U.`phone`, U.`email`, A.`address`, A.`zipcode`, A.`token` as address_token
            FROM `waiting_list` W, `user` U, `address` A
            WHERE W.`address_id` = A.`id`
             AND A.`user_id` = U.`id`
             AND W.`deleted` = 0
             AND W.`accepted`= 0
             AND U.`deleted` = 0
             ORDER BY W.`date_list_in`")->fetchAll();
    }

    /*
     * OLD FUNCTION , DELETED WHILE MERGING
    public function getWaitingListInfo()
    {
        return $this->db->query_log("SELECT w.*, u.lastname, u.firstname, u.user_token FROM `waiting_list` w
                                      INNER JOIN customer c ON w.customer_id = c.id
                                      INNER JOIN `user` u ON u.id = c.user_id
                                      WHERE w.deleted = 0 AND u.deleted = 0 ORDER BY w.`date_subscribed` ")->fetchAll();
    }*/


    public function deleteWaitingListById($waiting_list_id)
    {
        $ret = $this->db->query_log("UPDATE `waiting_list` SET `deleted` = 1, `date_list_out` = NOW() WHERE `id` = ?; ", [$waiting_list_id]);
        if ($ret)
            return $ret->rowCount();
        return null;
    }

    public function acceptWaitingListById($waiting_list_id)
    {
        $ret = $this->db->query_log("UPDATE `waiting_list` SET `accepted` = 1, `date_list_out` = NOW() WHERE `id` = ?; ", [$waiting_list_id]);
        if ($ret)
            return $ret->rowCount();
        return null;
    }

    public function getUserInMissionByProvider($user_token)
    {
        return $this->db->query("SELECT DISTINCT user_id, user_token FROM agenda INNER JOIN user ON user.id = agenda.user_id WHERE provider_id = (SELECT id FROM provider WHERE provider.user_id = (SELECT id FROM user WHERE user_token = ?))", [$user_token])->fetchAll();
    }

    public function addWaitingList($address_id)
    {
        $user_info = $this->db->query_log("
        SELECT U.`lastname`, U.`firstname`, U.`user_token`, U.`phone`, A.`address`, A.`zipcode`, A.`token` as address_token, C.`id` as c_id
        FROM `address` A, `user` U, `customer` C
        WHERE A.`id` = ? AND C.`user_id` =  U.`id` AND A.`user_id` = U.`id`", [$address_id])->fetch();
        if (!$user_info)
            return null;
        $date_list_in = date_create()->format('Y-m-d H:i:s');
        $this->db->query_log("INSERT INTO `waiting_list` SET `address_id` = ?, `customer_id` = ?, `pending` = 1", [$address_id, $user_info->c_id]);
        $id = $this->db->lastInsertId();
        if (!$id)
            return null;
        $user_info->waiting_list_id = $this->db->lastInsertId();
        $user_info->date_list_in = NULL;
        return $user_info;
    }

    public function getAddressNotInWaitingListByAddressToken($address_token) {
        return $this->db->query_log("SELECT A.*, A.`token` as address_token, M.`arret` as subway_station, M.`ligne` as subway_line
                                     FROM `address` A
                                     LEFT JOIN `metro_paris` M ON (M.`id` = A.`subway_id`)
                                     WHERE A.`token` = ?
                                     AND NOT EXISTS (SELECT W.* FROM `waiting_list` W WHERE W.`address_id` = A.`id`)",
                                     [$address_token])->fetch();
    }

    public function getUsersByWho($who) {
        return $this->db->query_log("SELECT * FROM `user`
                                    WHERE who = ?
                                    AND deleted = 0", [$who])->fetchAll();
    }

    public function updateUserWho($user_token, $who)
    {
        return $this->db->query_log("UPDATE `user` SET `who` = ? WHERE `user_token` = ?", [$who, $user_token]);
    }

    public function getSubscriber() {
        $ret = $this->db->query_log("SELECT * FROM `subscription` WHERE `deleted` = 0 AND `user_id` IS NULL AND `payed` = 0 AND `firstname` IS NOT NULL");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function deleteSubscriber($subscriber_id) {
        $ret = $this->db->query_log("UPDATE `subscription` SET `deleted` = 1 WHERE `id` = ?", [$subscriber_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function subscriberToWaitingList($subscriber_id) {
            $password = my_crypt::genHash(my_crypt::genStandardToken());
            $user_token = my_crypt::genStandardToken();
            $customer_token = my_crypt::genStandardToken();
            $address_token = my_crypt::genStandardToken();

            $ret = $this->db->query_log("
SET @subscriber_id = ?;
INSERT INTO `user` (`email`, `firstname`, `lastname`, `who`, `pending`, `password`, `user_token`, `phone`, `subscribed_at`)
SELECT `email`, `firstname`, `lastname`, 'customer', '0', ?, ?, `phone`, `date` FROM `subscription`
WHERE `id` = @subscriber_id;
SET @user_id = LAST_INSERT_ID();
UPDATE `subscription` SET `user_id` = @user_id WHERE `id` = @subscriber_id;
INSERT INTO `customer` (`user_id`, `customer_token`)
VALUES(@user_id, ?);
SET @customer_id = LAST_INSERT_ID();
INSERT INTO `address` (`user_id`, `token`, `address`, `zipcode`, `surface`, `recurrence_id`)
SELECT @user_id, ?, `address`, `zipcode`, `surface`, `recurrence_id` FROM `subscription`
WHERE `id` = @subscriber_id;
SET @address_id = LAST_INSERT_ID();
INSERT INTO `waiting_list` (`address_id`, `customer_id`, `workflow_id`, `date_list_in`)
VALUES(@address_id, @customer_id, null, NOW());
", [
                $subscriber_id,
                $password,
                $user_token,
                $customer_token,
                $address_token
            ]);

            if ($ret === false)
                return false;
            return $this->db->lastInsertId();
    }
}