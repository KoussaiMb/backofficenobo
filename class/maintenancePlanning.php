<?php

class maintenancePlanning
{
    private $db;
    private $address_id;

    public function __construct($db, $address_id)
    {
        $this->db = $db;
        $this->address_id = $address_id;
    }

    public function getMP()
    {
         return $this->db->query_log("SELECT M.`id`, M.`type_task`, M.`position`, M.`task_id`, M.`task_time`, T.`logo`, T.`name`, A.`maintenance_len`
                                      FROM maintenance M
                                      INNER JOIN `address` A ON (M.`address_id` = A.`id`)
                                      INNER JOIN `task` T ON (T.`id` = M.`task_id`)
                                      WHERE M.`deleted` = 0
                                      AND A.`deleted` = 0
                                      AND T.`deleted` = 0
                                      AND M.`address_id` = ?
                                      ORDER BY (M.`position`)", [$this->address_id])->fetchAll();
    }

    /*
    **  this function is to add a new maintenance planning from task elements formated this way:
    **  [task_"element.position"_"element.type_task"_"element.id" = "element.time"]
    */

    public function updateMaintenancePlanning($data)
    {
        if (!$data)
            $this->deleteAll();
        $filteredData = $this->filterData($data);
        if (!$filteredData)
            return -1;
        $parsedData = $this->parseData($filteredData);
        if (!$parsedData)
            return -1;
        $splitData = $this->splitData($parsedData);
        $oldMP = $this->getMP();
        if (count($splitData['update']) != count($oldMP)) {
            $toDelete = $this->compareOldAndNew($splitData['update'], $oldMP);
            if ($this->delete($toDelete) == -1)
                return -1;
        }
        if ($splitData['update'] && $this->update($splitData['update']) == -1)
            return -1;
        if ($splitData['insert'] && $this->insert($splitData['insert']) == -1)
            return -1;
        if ($data['maintenance_len'] != $oldMP[0]->maintenance_len)
            $ret = $this->updatePlLen($data['maintenance_len']);
        if (isset($ret) && $ret == -1)
            return -1;
        return 1;
    }

    private function compareOldAndNew($newMP, $oldMP)
    {
        $toDelete = [];
        $max = count($newMP);

        foreach ($oldMP as $k => $v) {
            $i = 0;
            while ($i < $max) {
                if ($v->id == $newMP[$i]['id'])
                    break;
                $i++;
            }
            if ($i == $max)
                array_push($toDelete, $v->id);
        }
        return $toDelete;
    }

    private function splitData($data)
    {
        $splitData = ['insert' => [], 'update' => []];

        foreach ($data as $k => $v) {
            if ($v['id'] == 0)
                array_push($splitData['insert'], $v);
            else
                array_push($splitData['update'], $v);
        }
        return $splitData;
    }

    private function delete($data)
    {
        try {
            $this->db->beginTransaction();
            foreach ($data as $k) {
                $this->db->query("UPDATE `maintenance` SET `deleted` = 1 WHERE `id` = ?",
                    [$k]);
            }
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            return -1;
        }
    }

    private function insert($dataParsed)
    {
        $toRemove = 'id';
        $sql = $this->makeSqlInsert($toRemove, $dataParsed);
        $ret = $this->db->query_log("INSERT INTO `maintenance`(`address_id`, `type_task`, `task_time`, `task_id`, `position`) VALUES" . $sql);
        return empty($ret) ? $ret : $this->db->lastInsertId();
    }

    private function makeSqlInsert($toRemove, $data)
    {
        $values = [];

        foreach ($data as $k => $v) {
            unset($v[$toRemove]);
            $values[] = '(\'' . implode('\',\'', $v) . '\')';
        }
        $values = implode(',', $values);
        return $values;
    }

    private function update($dataParsed)
    {
        try {
            $this->db->beginTransaction();
            foreach ($dataParsed as $k => $v) {
                $this->db->query("UPDATE `maintenance` SET `position` = ?, `type_task` = ?, `task_time` = ?, `task_id` = ? WHERE `id` = ?",
                    [$v['position'], $v['type_task'], $v['task_time'], $v['task_id'], $v['id']]);
            }
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            return -1;
        }
    }

    private function deleteAll()
    {
        $ret = $this->db->query_log("UPDATE `maintenance` SET `deleted` = 1 WHERE `address_id` = ?", [$this->address_id]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    public function createMaintenancePlanning($data)
    {
        $filteredData = $this->filterData($data);
        if (!$filteredData)
            return false;
        $parsedData = $this->parseData($filteredData);
        if (!$parsedData)
            return false;
        if ($this->updatePlLen($data['maintenance_len']) === false)
            return false;
        return $this->insert($parsedData);
    }

    private function updatePlLen($maintenance_len)
    {
        $ret = $this->db->query_log("UPDATE `address` SET `maintenance_len` = ? WHERE `id` = ?", [$maintenance_len, $this->address_id]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    private function parseData($data)
    {
        $parsedData = [];

        foreach ($data as $k => $v) {
            $task = explode('_', $k);
            if (!$task)
                return null;
            array_push($parsedData, [
                'addr_id' => $this->address_id,
                'type_task' => $task[2],
                'task_time' => $v,
                'task_id' => $task[3],
                'position' => $task[1],
                'id' => $task[4]
            ]);
        }
        return $parsedData;
    }

    private function filterData($data)
    {
        $filteredData = [];

        foreach ($data as $k => $v) {
            if (preg_match('/^task_[\d]+_[\d]+_[\d]+_[\d]+$/', $k))
                $filteredData[$k] = $v;
        }
        return $filteredData;
    }
}