<?php

require __DIR__ . '/../vendor/autoload.php';

class TaxCreditGenerator
{

    private $companyInfo;
    private $client;
    private $carts;
    private $transactions;
    private $pdf;

    public function __construct($companyInfo, $client, $carts, $action)
    {

        $this->companyInfo = $companyInfo;
        $this->client = $client;
        $this->carts = $carts;
        $this->generatePdf($action);
    }

    /* Generating the html to convert for the pdf */
    private function generateHtml()
    {
        /* Initializing the buffer */
        ob_start();

        ?>
        <style type="text/css">
            table {
                width: 100%;
                color: black;
                font-family: helvetica;
                line-height: 5mm;
                border-collapse: collapse;
            }
            h2  { margin: 0; padding: 0; }
            p { margin: 5px; }

            .border th {
                border: 1px solid #000;
                color: black;
                background: #FFFFFF;
                padding: 3px;
                font-weight: bold;
                font-size: 13px;
                text-align: center; }

            .border td {
                border: 1px solid #000;
                padding: 5px 10px;
                text-align: center;
            }

            .text-center{
                text-align: center;
            }

            .10p { width: 10%; } .15p { width: 15%; }
            .20p { width: 20%; } .25p { width: 25%; }
            .35p { width: 35%; } .75p { width: 75%; }
        </style>

        <page backtop="10mm" backleft="0mm" backright="10mm" backbottom="10mm">
            <body>
            <!-- En-tête -->
            <table style="margin-top: 10px;">
                <tr>
                    <td class="75p">
                        <strong><?= $this->companyInfo['name']; ?></strong><br/><br/>
                        <?= $this->companyInfo['siret']; ?><br/>
                        <?= $this->companyInfo['sap']; ?><br/>
                        <?= $this->companyInfo['address']; ?><br/>
                        <?= $this->companyInfo['zipcity']; ?><br/>
                    </td>
                    <td class="25p">
                        <strong><?= $this->client['firstname'] . ' ' . $this->client['lastname']; ?></strong><br/>
                        <?= $this->client['address']; ?><br/>
                        <?= $this->client['zipcity']; ?><br/>
                    </td>
                </tr>
            </table>
            <br>
            <!-- Titre -->
            <p class="text-center">
                <b>ATTESTATION FISCALE ANNUELLE</b>
            </p>
            <br>
            <br>
            <!-- Message -->
            <p>
                Je soussigné <?= $this->companyInfo['chief'] ?>, Président de la société <?= $this->companyInfo['name'] ?>, commercialisée sous le nom <?= $this->companyInfo['realName'] ?>,
                certifie que <?= $this->client['firstname'] . ' ' . $this->client['lastname']; ?>, résidant au <?= $this->client['address']; ?> <?= $this->client['zipcity']; ?>,
                a bénéficié de service à la personne : entretien du domicile.
            </p>
            <?php
                $total = 0;
                foreach ($this->carts as $c)
                    $total += $c['unit_price'] * $c['quantity'];
            ?>

            <p>
                En <?= $this->client['year'] ?>, le montant des factures effectivement
                acquittées représente une somme totale de <b><?= $total ?> €</b> réglée par carte bancaire.
            </p>

            <p>
                <i>
                    Informations à renseigner en page 1 du formulaire 2042-RICI dans les cases 7DB,
                    7DF ou 7DD selon votre situation.
                </i>
            </p>
            <!-- Tableau des prestations -->
            <?php $total = 0; ?>
            <table style="width: 80%; margin: 30px; auto" class="border">
                <thead>
                <tr>
                    <th class="20p">N°SIREN</th>
                    <th class="30p">Intervenant</th>
                    <th class="15p">Date</th>
                    <th class="15p">Durée</th>
                    <th class="20p">Prix</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($this->carts as $c): ?>
                <tr>
                    <td><?= (empty($c['payroll_identity']) || !isset($c['payroll_identity']) || ($c['payroll_identity'] === 'null')) ? ' ' : $c['payroll_identity'] ?></td>
                    <td><?= $c['firstname']. ' ' . $c['lastname'] ?></td>
                    <td><?= date('d-m-y', strtotime($c['date'])) ?></td>
                    <?php $quantity = explode('.', $c['quantity']); ?>
                    <td><?= $quantity[0] . "h" . substr((isset($quantity[1]) && $quantity[1] != "00" ? $quantity[1] * 6 : "00"), 0 , 2); ?></td>
                    <td><?= $c['unit_price'] * $c['quantity'] . '€'?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <!-- Bas de page -->
            <p>
                Fait pour valoir ce que de droit,<br>
                Le <?= date('d-m-Y') ?><br><br>
                <?= $this->companyInfo['chief'] ?>, Président de <?= $this->companyInfo['name']; ?><br>
            </p>
            <img src="<?= $this->companyInfo['chiefSignature'] ?>">
            <!--<page_footer></page_footer>-->
            </body>
        </page>

        <?php
        /* Returning the content of the buffer */
        return ob_get_clean();
    }

/*
 *  <?php $vat_value = $this->computeVat($c['vat'], $c['unit_price'], $c['auto']) * $c['quantity'] ; ?>
 *  <td><?php round($vat_value, 2) . '€' ?></td>
 */


public function generatePdf($action)
    {
        $content = $this->generateHtml();
        $pdf = new HTML2PDF("p", "A4", "fr");
        $pdf->pdf->SetTitle('CreditImpot_'. $this->companyInfo['name'] .'_'. $this->client['lastname'] .'_'. $this->client['firstname'] .'_'.$this->client['year'] .'.pdf');
        $pdf->writeHTML($content);

        try {
            if ($action == 'download')
                $pdf->Output('CreditImpot_'. $this->companyInfo['name'] .'_'. $this->client['lastname'] .'_'. $this->client['firstname'] .'_'.$this->client['year'] .'.pdf', 'D');
            else if ($action == "generate"){
                $this->pdf = $pdf->Output('CreditImpot_' . $this->companyInfo['name'] . '_' . $this->client['lastname'] . '_' . $this->client['firstname'] . '_' . $this->client['year'] . '.pdf', 'S');
            }
            else
                $pdf->Output('CreditImpot_' . $this->companyInfo['name'] . '_' . $this->client['lastname'] . '_' . $this->client['firstname'] . '_' . $this->client['year'] . '.pdf');
            } catch (HTML2PDF_exception $e) {
                die($e);
        }
    }

    function get_pdf() {
        if (isset($this->pdf))
            return $this->pdf;
        return null;
    }

    function convert_time_to_decimal($time)
    {

        $expl_time = explode(':', $time);
        $hours = $expl_time[0];
        $minutes = $expl_time[1];

        return $hours + round($minutes / 60, 2);
    }

    /*
     * Computes the vat depending on the status of the provider
     */
    private function computeVat($vat, $unitPrice, $auto)
    {
        if($auto){
            $hour_comission = $unitPrice - $this->companyInfo['autoSalary'];
            return $hour_comission*($vat/100);
        }else{
            return $unitPrice*($vat/100);
        }
    }
}