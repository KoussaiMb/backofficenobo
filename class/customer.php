<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 29/06/2017
 * Time: 13:04
 */
class customer
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $ret = $this->db->query_log("SELECT * FROM `user` WHERE `deleted` = 0 AND `who` = 'customer'");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function customerNotInWaitingList()
    {
        return $this->db->query_log("SELECT U.user_token, U.firstname, U.lastname, U.email, U.phone
                                      FROM `user` U, customer Cus
                                      WHERE U.id = Cus.user_id
                                      AND Cus.id NOT IN
                                                (SELECT w.`customer_id`
                                                  FROM `waiting_list` w
                                                  WHERE w.`deleted` = 0
                                                  AND w.`accepted` = 0)
                                      AND U.deleted = 0
                                      AND U.who = 'customer'
                                      ORDER BY U.firstname ASC, U.lastname ASC")->fetchAll();
    }

    public function getAutocompleteName()
    {
        try {
            $autoComplete = [];

            $ret = $this->db->query_log("SELECT user_token, firstname, lastname, phone
                                          FROM `user`
                                          WHERE `deleted` = 0
                                          AND `who` = 'customer'
                                          ORDER BY firstname ASC, lastname ASC")->fetchAll();
            if (!$ret)
                return false;
            foreach ($ret as $key => $value) {
                if ($value->firstname != null || $value->lastname != null)
                    array_push($autoComplete, ['value' => $value->firstname . ' ' . $value->lastname, 'data' => $value->user_token]);
                if ($value->phone != null)
                    array_push($autoComplete, ['value' => $value->phone, 'data' => $value->user_token]);
            }
            return json_encode($autoComplete);
        } catch (PDOException $e) {
            return null;
        }
    }

    public function getAutocompleteNameWithPaymentAccount()
    {
        try {
            $autocomplete = [];

            $ret = $this->db->query_log("SELECT user_token, firstname, lastname
                                          FROM `user`
                                          WHERE `deleted` = 0
                                          AND `who` = 'customer'
                                          AND paymentAccount IS NOT NULL
                                          ORDER BY firstname ASC, lastname ASC")->fetchAll();
            if (!$ret)
                return false;
            foreach ($ret as $key => $value) {
                array_push($autocomplete, [
                    'value' => $value->firstname . ' ' . $value->lastname,
                    'data' => $value->user_token
                ]);
            }
            return json_encode($autocomplete);
        } catch (PDOException $e) {
            return null;
        }
    }

    public function stop_nobo($user_token, $date)
    {
        try {
            $this->db->query("UPDATE mission SET deleted = 1 WHERE mission.end > ? AND user_id = (SELECT id FROM user WHERE user_token = ?)", [$date, $user_token]);
        } catch (PDOException $e) {
            App::logPDOException($e, 1);
            return -1;
        }
    }

    public function getUserByCustomerId($customer_id)
    {
        return $this->db->query_log("SELECT * FROM `user` u
                                      INNER JOIN customer c ON c.user_id = u.id
                                      WHERE c.id = ?", [$customer_id])->fetch();
    }

    public function deleteUserByCustomerId($customer_id)
    {
        $ret = $this->db->query_log("UPDATE `user` u INNER JOIN `customer` c SET u.`deleted` = 1 WHERE c.`user_id` = u.`id` AND c.id = ?", [$customer_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function deleteCustomerById($customer_id)
    {
        $ret = $this->db->query_log("UPDATE `customer` c SET `deleted` = 1 WHERE c.`id` = ?", [$customer_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function deleteCustomerDetailsByCustomerId($customer_id)
    {
        $ret =  $this->db->query_log("DELETE FROM `customer_details` WHERE `customer_id` = ?",[$customer_id]);
        if($ret == false){
            return 0;
        }
        return true;
    }

    public function AddStopNobo($data){
        $this->db->query_log("INSERT INTO stop_nobo (`user_id`, `date_stop`) VALUES ((SELECT id FROM user WHERE user_token = ?), ?)", [$data['user_token'], $data['date']]);
    }

    public function getStopNoboByUserToken($user_token){
        return $this->db->query_log("SELECT * FROM stop_nobo WHERE user_id = (SELECT id FROM `user` WHERE user_token = ?) AND `come_back` IS NULL", [$user_token])->fetch();
    }

    public function getStopNoboByUserId($user_id){
        return $this->db->query_log("SELECT * FROM `stop_nobo` WHERE `user_id` = ? AND `come_back` IS NULL", [$user_id])->fetch();
    }

    public function removeStopNoboById($id){
        return $this->db->query_log("UPDATE stop_nobo SET `come_back` = NOW() WHERE id = ?", [$id]);
    }

    public function hasPaymentCard($user_token){
        $cards = $this->db->query_log("SELECT COUNT(*) FROM `payment_card` p, `user` u WHERE p.user_id = u.id AND user_token = ?", [$user_token])->fetchColumn();

        return $cards > 0;
    }

    public function hasPaymentAccount($user_token){
        $cards = $this->db->query_log("SELECT COUNT(*) FROM `user` u WHERE user_token = ? AND paymentAccount IS NOT NULL", [$user_token])->fetchColumn();

        return $cards > 0;
    }

    public function insertStopRaison($stop_id, $l_stop_reason_id, $comment)
    {
        $ret = $this->db->query_log("INSERT INTO `stop_reason` (`stop_id`, `l_stop_reason_id`, `comment`)
                                    VALUES (?, ?, ?)", [$stop_id, $l_stop_reason_id, $comment]);

        if ($ret == false) {
            return 0;
        }

        return $this->db->lastInsertId();
    }

    /*
        CALL DURATION
     */
    public function getCallDurationByToCustomerId($customer_id) {
        return $this->db->query_log("SELECT `call_duration` FROM `customer` WHERE `id` = ?", [$customer_id])->fetch()->call_duration;
    }

    public function updateCallDurationByCustomerId($customer_id, $duration) {
        $ret = $this->db->query_log("UPDATE `customer` SET `call_duration` = ? WHERE `id` = ?", [$duration, $customer_id]);

        if ($ret == false) {
            return 0;
        }

        return 1;
    }

    public function usedPromocode($customer_id) {
        $ret = $this->db->query_log("SELECT 
                                        l.* 
                                    FROM 
                                        `log_form_order` l
                                    INNER JOIN `user` u ON (l.user_id = u.id)
                                    INNER JOIN `customer` c ON (u.id = c.user_id)
                                    WHERE 
                                        c.id = ?",
                                    [$customer_id]);
        
        return $ret ? ($ret->fetchColumn() > 0) : false;
    }

    public function getCustomerByUserId($user_id) {
        $ret = $this->db->query_log("
                                      SELECT c.*, u.user_token
                                      FROM `customer` c 
                                      INNER JOIN `user` u ON c.`user_id` = u.`id`
                                      WHERE c.`user_id` = ?",
                                        [$user_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getCustomerById($customer_id) {
        $ret = $this->db->query_log("
                                      SELECT c.*, 
                                      u.user_token, u.id as user_id, u.firstname, u.lastname, u.phone, u.email,
                                      a.address, a.zipcode, a.city
                                      FROM `customer` c 
                                      INNER JOIN `user` u ON c.`user_id` = u.`id`
                                      LEFT JOIN `address` a ON a.`user_id` = u.`id`
                                      WHERE c.`id` = ?",
                                        [$customer_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getCustomerPackages() {
        $ret = $this->db->query_log("SELECT * FROM `customer_package` WHERE `active` = 1");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getCustomerPackagesAndCurrentPackage($customer_package_id) {
        $packages = $this->getCustomerPackages();
         if ($packages === false)
            return false;
        if ($customer_package_id === null)
            return $packages;
        $ret = $this->db->query_log('SELECT * FROM `customer_package` WHERE `id` = 1', [$customer_package_id]);
        if ($ret === false)
            return false;
        $customer_package = $ret->fetch();
        if ($customer_package->active == 0)
            array_push($packages, $customer_package);
        return $packages;
    }

    public function updateCustomerCluster($cluster_id, $customer_id) {
        $ret = $this->db->query_log("
UPDATE `customer` C 
INNER JOIN `customer_cluster` CC ON C.id = CC.customer_id 
SET CC.cluster_id = ? WHERE C.id = ?", [$cluster_id, $customer_id]);
        if ($ret == false)
            return 0;
        return 1;
    }
}
