<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 08/11/2016
 * Time: 16:19
 */
class debug
{
    static function data_dump($data){
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
    static function data_r($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}