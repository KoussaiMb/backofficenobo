<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 04/11/2016
 * Time: 14:41
 */
class Session
{
    static $instance;
    private $msg = [
        'unkownErr' => 'Une erreur inconnue est survenue, veuillez nous contacter ou réessayer ultérieurement',

    ];

    static function getInstance(){
        if (!self::$instance) {
            self::$instance = new Session();
        }
        return self::$instance;
    }
    public function __construct()
    {
        session_start();
    }
    public function delAll(){
        session_unset();
    }
    public function setFlash($key, $message){
        $_SESSION['flash'][$key] = $message;
    }
    public function setErr(){
        Session::getInstance()->setFlash('danger', $this->msg['unkownErr']);
    }
    public function hasFlash(){
        return isset($_SESSION['flash']);
    }
    public function getFlash(){
        $value = $_SESSION['flash'];
        unset ($_SESSION['flash']);
        return $value;
    }
    public function write($key, $value){
        $_SESSION[$key] = $value;
    }
    public function read($key){
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }
    public function delete($key){
        unset($_SESSION[$key]);
    }
    public function update($key, $value){
        self::delete($key);
        self::write($key, $value);
    }
    public function upLog($key){
        $var = New DateTime;
        $_SESSION[$key] = $var->format('Y-m-d H:i:s');
    }
    public function lifeTime($lifetime = 1800){
        $time = time();

        //If session is unactive for more than 30 minutes, destroy it
        if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY'] > $lifetime)) {
            session_unset();
            session_destroy();
            header("Location: /login");
        }
        $_SESSION['LAST_ACTIVITY'] = $time;

        //If session is live more than 30 minutes, regenerate it
        if (!isset($_SESSION['CREATED'])) {
            $_SESSION['CREATED'] = $time;
        } else if ($time - $_SESSION['CREATED'] > $lifetime) {
            session_regenerate_id(true);
            $_SESSION['CREATED'] = $time;
        }
    }
}