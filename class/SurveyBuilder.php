<?php

class SurveyBuilder
{
    private $surveyClass;
    private $surveyName;
    private $questions;
    private $last_position;

    private $user_id;

    public function __construct($surveyName, $user_id = null)
    {

        $this->surveyName = $surveyName;
        $this->user_id = $user_id;

        $this->surveyClass = App::getSurvey();

        $this->questions = $this->surveyClass->getQuestionsBySurveyName($this->surveyName);
        $this->last_position = $this->surveyClass->getLastPositionInSurvey($this->surveyName)->max_pos;
    }

    /**
     * @return string The complete HTML displayed in the survey feature to manage it
     */
    public function getEditCompleteHtml()
    {
        $input = '<div class="col-sm-12 question_div">';
        $it_quest = sizeof($this->questions);
        for($i = 0; $i < $it_quest; $i++){
            $input .= $this->getEditQuestionHtml($this->questions[$i]);
        }
        $input .= '</div>';

        return $input;
    }

    /**
     * @return string The complete HTML displayed to the user so he can answer it
     */
    public function getCompleteSurveyHtml()
    {

        $input = '<div class="col-sm-12">';
        $it_quest = sizeof($this->questions);
        for($i = 0; $i < $it_quest; $i++){
            $input .= $this->getQuestionHtml($this->questions[$i]);
        }
        $input .= '</div>';

        return $input;
    }

    /**
     * @param $question A row of the table question with its type and text
     * @return string The HTML of the question displayed to the user
     */
    private function getQuestionHtml($question)
    {

        $question_type = App::getListManagement()->getFieldById($question->l_input_type_id);
        $offered_answers = $this->surveyClass->getOfferedAnswersByQuestionId($question->id);

        $input = '<div class="col-sm-12" style="margin:5px; padding: 5px; border-bottom: 1px solid lightgrey;">';
        $input.= '<div class="col-sm-5" style="font-size:15px">';
        $input.= $question->question_text;
        $input.= '</div>';
        $input.= '<div class="col-sm-4" id="div_answer_'. $question->id .'">';
        $input.= $this->buildAnswerHtml($question_type, $offered_answers, $question);
        $input.= '</div>';
        $input.= '</div>';

        return $input;
    }

    /**
     * @param $question A row of the table question with its type and text
     * @return string The HTML of the question displayed in the survey feature to manage it
     */
    private function getEditQuestionHtml($question)
    {

        $question_type = App::getListManagement()->getFieldById($question->l_input_type_id);
        $offered_answers = $this->surveyClass->getOfferedAnswersByQuestionId($question->id);

        $input = '<div class="col-sm-12" style="margin:5px; padding: 5px; border-bottom: 1px solid lightgrey;" id="div_edit_question_'. $question->id .'">';
        $input.= '<div class="col-sm-4" style="font-size:15px">';
        $input.= $question->question_text;
        $input.= '</div>';
        $input.= '<div class="col-sm-3" id="div_answer_'. $question->id .'">';
        $input.= $this->buildAnswerHtml($question_type, $offered_answers, $question);
        $input.= '</div>';
        $input.= '<div class="col-sm-1">';
        $input.= '<button type="button" id="btn_remove_'. $question->id .'" class="btn btn-danger remove-btn" data-rmv="'. $question->id .'"><span class="glyphicon glyphicon-minus"></span>';
        $input.= '</div>';
        $input.= '<div class="col-sm-1" id="btn_up_' . $question->id . '">';
        $input.= '<button type="button" class="btn btn-default up-btn" data-up="'. $question->id .'"><span class="glyphicon glyphicon-arrow-up"></span>';
        $input.= '</div>';
        $input.= '<div class="col-sm-1 btn-down" id="btn_down_' . $question->id . '">';
        $input.= '<button type="button" class="btn btn-default down-btn" data-down="' . $question->id . '"><span class="glyphicon glyphicon-arrow-down"></span>';
        $input.= '</div>';
        $input.= '<div class="col-sm-1" id="div_edit_question_'. $question->id .'">';
        $input.= '<button type="button" class="btn btn-default edit-btn" data-qid="'. $question->id .'"><span class="glyphicon glyphicon-pencil"></span>';
        $input.= '</div>';


        $input.= '</div>';

        return $input;
    }

    /**
     * @param $type The type of the question
     * @param $answers The answers associated to the question
     * @param $question
     * @return string The HTML for the given question with the answers associated
     */
    private function buildAnswerHtml($type, $answers, $question)
    {
            if($type == 'text' || $type == 'number' || $type == 'date'){
                return $this->buildSingleTypeHtml($type, $question);
            }else if($type == 'select'){
                return $this->buildSelectTypeHtml($answers, $question);
            }else if($type == 'radio'){
                return $this->buildRadioTypeHtml($answers, $question);
            }else if($type == 'checkbox'){
                return $this->buildCheckboxTypeHtml($answers, $question);
            }
            return '';
    }

    /**
     * @param $type The type of the question (text, number or date)
     * @param $q The question
     * @return string The HTML of the question with an input text, number or date for the answer
     */
    private function buildSingleTypeHtml($type, $q)
    {

        $u_answer = $this->getUserAnswer($q);
        if(isset($u_answer->other_text)){
            $u_answer = $u_answer->other_text;
        }

        if(!empty($u_answer)) {
            return '<input type="' . $type . '" class="form-control" name="answer_' . $q->id . '" value="'.$u_answer.'">';
        }else{
            return '<input type="' . $type . '" class="form-control" name="answer_' . $q->id . '" value="">';
        }
    }

    /**
     * @param $answers The offered answers to the question
     * @param $q
     * @return string The HTML of the question with the select to answer it
     */
    private function buildSelectTypeHtml($answers, $q)
    {

        $u_answer = $this->getUserAnswer($q);

        $other_text = empty($u_answer->other_text) ? "" : $u_answer->other_text;
        $html = '';
        $html .= '<select class="form-control select_answer" name="answer_'.$q->id.'" data-other="'. $other_text .'" data-val="'.$q->id.'">';

        $it_answers = sizeof($answers);
        for($i = 0; $i < $it_answers; $i++){
            if(!empty($u_answer) && ($u_answer == $answers[$i]->answer_text)){
                $html .= '<option value="'.$answers[$i]->id.'" selected>'.$answers[$i]->answer_text.'</option>';
            }else{
                $html .= '<option value="'.$answers[$i]->id.'">'.$answers[$i]->answer_text.'</option>';
            }
        }
        $html .= '</select>';

        return $html;
    }

    /**
     * @param $answers The offered answers to the question
     * @param $q
     * @return string The HTML of the question with the radio buttons to answer it
     */
    private function buildRadioTypeHtml($answers, $q)
    {

        $u_answer = $this->getUserAnswer($q);

        $html = '';
        $it_answers = sizeof($answers);
        for($i = 0; $i < $it_answers; $i++){
            if(isset($u_answer) && ($u_answer == $answers[$i]->answer_text)) {
                $html .= '<input type="radio" id="radio_' . $answers[$i]->id . '" name="answer_' . $q->id . '" value="' . $answers[$i]->id . '" checked>';
                $html .= '<label for="radio_' . $answers[$i]->id . '">' . $answers[$i]->answer_text . '</label><br>';
            }else{
                $html .= '<input type="radio" id="radio_' . $answers[$i]->id . '" name="answer_' . $q->id . '" value="' . $answers[$i]->id . '">';
                $html .= '<label for="radio_' . $answers[$i]->id . '">' . $answers[$i]->answer_text . '</label><br>';
            }
        }
        return $html;
    }

    /**
     * @param $answers The offered answers to the question
     * @param $q
     * @return string The HTML of the question with the checkboxes to answer it
     */
    private function buildCheckboxTypeHtml($answers, $q)
    {
        $u_answer = $this->getUserAnswer($q);

        $html = '';

        if(is_array($u_answer)){
            $checked_answers = $this->getCheckedAnswers($u_answer);
        }

        $it_answers = sizeof($answers);
        for($i = 0; $i < $it_answers; $i++){
            if((isset($checked_answers) && in_array($answers[$i]->id, $checked_answers)) || ($u_answer && ($u_answer == $answers[$i]->answer_text))) {
                $html .= '<input type="checkbox" id="check_' . $answers[$i]->id . '" name="answer_' . $q->id . '[]" value="' . $answers[$i]->id . '" checked>';
                $html .= '<label for="check_' . $answers[$i]->id . '">' . $answers[$i]->answer_text . '</label><br>';
            }else{
                $html .= '<input type="checkbox" id="check_' . $answers[$i]->id . '" name="answer_' . $q->id . '[]" value="' . $answers[$i]->id . '">';
                $html .= '<label for="check_' . $answers[$i]->id . '">' . $answers[$i]->answer_text . '</label><br>';
            }
        }

        return $html;
    }

    /**
     * @return mixed The name of the survey
     */
    public function getSurveyName()
    {
        return $this->surveyName;
    }

    /**
     * @param $answers The answers checked by the user
     * @return array Array containing all the ids of the checked answers
     */
    private function getCheckedAnswers($answers)
    {
        $checked = array();
        foreach ($answers as $ans){
            if(isset($ans->offered_answer_id)){
                $checked[] = $ans->offered_answer_id;
            }
        }
        return $checked;
    }

    /**
     * @return array|int The values for each threashold of this survey
     */
    private function getSurveyThreshold(){

        $thresholds_refusal = App::getSurvey()->getThresholdBySurveyName($this->surveyName, 'refusal');
        $thresholds_waiting = App::getSurvey()->getThresholdBySurveyName($this->surveyName, 'waiting');
        $thresholds_accept = App::getSurvey()->getThresholdBySurveyName($this->surveyName, 'acceptance');

        if($thresholds_refusal === false || $thresholds_waiting === false || $thresholds_accept === false){
            return 0;
        }
        $thresholds = ["refusal" => $thresholds_refusal, "waiting" => $thresholds_waiting, "acceptance" => $thresholds_accept];
        return $thresholds;
    }

    /**
     * @param $question A row of the table question
     * @return int  The id or an array of ids of the answers of the user
     */
    private function getUserAnswer($question)
    {
        $question_type = App::getListManagement()->getFieldById($question->l_input_type_id);
        if(!empty($question->field_ref_id)){
            $field = $this->surveyClass->getTableAndColumnByRefId($question->field_ref_id);
            if(!$field){
                return 0;
            }
            $answer_result = $this->surveyClass->getUserAnswerByUserAndField($this->user_id, $field->table_name, $field->field_name);
            if(!$answer_result){
                return 0;
            }
            $answer = $answer_result->answer;

            if($answer == 'Autre' || $answer == 'Texte'){
                $answer_text = $this->surveyClass->getAnswerByQuestionAndUserId($question->id, $this->user_id);
                if(!$answer_text){
                    return 0;
                }
                return $answer_text;
            }else if($answer == 'Multiple'){
                $answer_text = $this->surveyClass->getAnswersByQuestionAndUserId($question->id, $this->user_id);
                if(!$answer_text){
                    return 0;
                }
                return $answer_text;
            }
        }else if($question_type == 'checkbox') {
            $answers = $this->surveyClass->getAnswersByQuestionAndUserId($question->id, $this->user_id);
            return $answers;
        }else{
            $answer_text = $this->surveyClass->getAnswerByQuestionAndUserId($question->id, $this->user_id);
            if(!$answer_text){
                return 0;
            }
            return $answer_text->other_text;
        }
        return $answer;
    }
}