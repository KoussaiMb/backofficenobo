<?php


class prestation
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getCartById($cartId)
    {
            return $this->db->query_log("SELECT c.id, c.price, c.quantity, c.price_final, c.discount_percentage, c.discount_euro, i.reference_name, i.vat , c.promocode_id, CAST(t.date_create as DATE) as date_create, c.cart_count, i.`name`, ica.name as category_name
                                      FROM cart c, item i, item_category ica, `transaction` t
                                      WHERE c.item_id = i.id AND ica.id = i.item_category_id AND c.transaction_id = t.id
                                      AND c.id = ? ", [$cartId])->fetch();
    }

    public function getCartByAddressTokenAndDate($address_token, $date1, $year1)
    {
        return $this->db->query_log("SELECT C.`id`, C.`price`, C.`quantity`, C.`price_final`, C.`discount_percentage`, C.`discount_euro`,
                                     I.`vat`, I.`name`, I.`reference_name`, C.`promocode_id`, C.`cart_count`, IC.`name` as category_name, CAST(T.`date_creat` as DATE) as date_create
                                      FROM cart C, item I, `address` A, `user` U, `transaction` T, `item_category` IC
                                      WHERE C.`item_id` = I.`id`
                                      AND C.`transaction_id` = T.`id`
                                      AND IC.`id` = I.`item_category_id`
                                      AND U.`id` = T.`user_id` = A.`user_id`
                                      AND A.`token` = ?
                                      AND MONTH(date_create) = ?
                                      AND YEAR(date_create) = ?", [$address_token, $date1, $year1])->fetchAll();
    }


    public function getCartByAddressTokenAndDateAndItemType($address_token, $month1, $year1, $type)
    {
        return $this->db->query_log("SELECT c.id, c.price, c.quantity, c.discount_percentage, c.discount_euro, c.price_final, i.vat, i.`name`, i.reference_name, c.promocode_id, c.cart_count, ica.name as category_name, CAST(t.date_create as DATE) as date_create
                                      FROM cart c, item i, `user` u, address a, `transaction` t, `item_category` ica 
                                      WHERE c.item_id = i.id AND c.transaction_id = t.id AND u.id = t.user_id AND u.id = a.user_id AND ica.id = i.item_category_id
                                      AND a.token = ? AND ica.`name` = ? AND MONTH(date_create) = ? AND YEAR(date_create) = ?", [$address_token, $type, $month1, $year1])->fetchAll();
    }

    public function getAllCartsInfoByUserToken($user_token)
    {
        return $this->db->query_log("SELECT DISTINCT c.id, t.date_create, u.firstname, u.lastname, a.address, a.zipcode, a.city, i.`name`, ica.name as category_name
                                      FROM cart c, item i, `list` l, `user` u, address a, `transaction` t, item_category ica 
                                      WHERE c.item_id = i.id AND i.item_category_id = ica.id AND c.transaction_id = t.id AND t.user_id = u.id AND a.user_id = u.id 
                                      AND a.home = 1 AND u.user_token = ? ORDER BY date_create DESC", [$user_token])->fetchAll();
    }

    public function getAllCartsByTransToken($trans_id)
    {
        return $this->db->query_log("SELECT c.id, t.date_create, u.firstname, u.lastname, a.address, a.zipcode, a.city, i.`name`, ica.name as category_name
                                      FROM cart c, item i, `list` l, `user` u, address a, `transaction` t, item_category ica 
                                      WHERE c.item_id = i.id AND i.item_category_id = ica.id AND c.transaction_id = t.id AND t.user_id = u.id AND a.user_id = u.id 
                                      AND a.home = 1 AND t.id = ? ORDER BY date_create DESC", [$trans_id])->fetchAll();
    }

    public function getAdditionalTasksByCartId($cartId)
    {
            return $this->db->query_log("SELECT a.task_time, t.name, a.mission_id 
                                      FROM additional_task a, task t, mission m 
                                      WHERE a.task_id = t.id
                                      AND a.mission_id = m.id
                                      AND m.cart_id = ? ", [$cartId])->fetchAll();
    }

    public function getMissionByCartId($cartId)
    {
        return $this->db->query_log("SELECT *, c.`id` as c_id, m.`id` as m_id FROM mission m, cart c
                                      WHERE m.cart_id = c.id AND m.cart_id = ? ", [$cartId])->fetch();
    }


    public function getUserInfoByToken($user_token)
    {
        return $this->db->query_log("SELECT u.firstname, u.lastname, CAST(u.subscribed_at as DATE) as sub_date 
                                      FROM `user` u
                                      WHERE u.user_token = ?", [$user_token])->fetch();
    }

    /* Getting all missions of the year to build the user tax credit */
    public function getAllTransactionPriceByUserTokenAndYear($userToken, $year)
    {
        return $this->db->query_log("SELECT SUM(T.`price`) as total_price
                                     FROM `transaction` T, `user` U , `address` A
                                     WHERE U.`id` = T.`user_id`
                                     AND U.`user_token` = ?
                                     AND A.`user_id` = U.`id`
                                     AND A.`home` = 1
                                     AND YEAR(T.`date_create`) = ?",
                                     [$userToken, $year])->fetchAll();
    }

    public function getAllTransactionByUserTokenAndYear($userToken, $year)
    {
        return $this->db->query_log("SELECT T.`price`, CAST(T.`date_create` as DATE) as date_create
                                     FROM `transaction` T, `user` U , `address` A
                                      WHERE U.`id` = T.`user_id`
                                      AND U.`user_token` = ?
                                      AND A.`user_id` = U.`id`
                                      AND A.`home` = 1
                                      AND YEAR(T.`date_create`) = ?", [$userToken, $year])->fetchAll();
    }

    public function getProviderById($provider_id)
    {
        return $this->db->query_log("SELECT * FROM `provider` WHERE deleted = 0 AND id = ?", [$provider_id])->fetch();
    }

    public function getCartsByUserTokenTypeAndYear($user_token, $type, $year)
    {
        return $this->db->query_log("SELECT us.firstname, us.lastname, p.payroll_identity, c.price_final, i.vat, i.reference_name, c.quantity, m.`start`, c.price
                                      FROM cart c, item i, `user` u, `transaction` t, item_category ica, `user` us, provider p, mission m
                                      WHERE c.item_id = i.id AND i.item_category_id = ica.id AND c.transaction_id = t.id AND u.id = t.user_id
                                      AND p.user_id = us.id AND m.provider_id = p.id AND m.cart_id = c.id
                                      AND u.user_token = ? AND ica.`name` = ? AND YEAR(t.date_create) = ? ORDER BY t.date_create ASC", [$user_token, $type, $year])->fetchAll();
    }
}