<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 29/06/2017
 * Time: 10:34
 */
class task
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->query_log("SELECT T.*, T.`l_ponderation_id` as ponderation_id FROM `task` T WHERE T.`deleted` = 0 ORDER BY T.`name` ASC")->fetchAll();
    }

    public function editTask($task, $logo_url)
    {
        $ret = $this->db->query_log("UPDATE `task` SET `name` = ?, `logo` = ?, `default_time` = ?, `is_rec` = ?, `is_per` = ?, `is_opt` = ?, `ponderation_id` = ?, 
        `is_rolling` = ?,   WHERE `id` = ?",
            [
                $task['name'],
                $logo_url,
                $task['default_time'],
                $task['is_rec'],
                $task['is_per'],
                $task['is_opt'],
                $task['ponderation_id'],
                $task['is_rolling'],
                $task['id']
            ]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    public function addTask($task, $logo_url)
    {
        $this->db->query_log("INSERT INTO `task`(`name`, `logo`, `default_time`, `is_rec`, `is_per`, `is_opt`, `ponderation_id`, `is_rolling`) values(?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $task['name'],
                $logo_url,
                $task['default_time'],
                $task['is_rec'],
                $task['is_per'],
                $task['is_opt'],
                $task['ponderation_id'],
                $task['is_rolling']
            ]);
        return $this->db->lastInsertId();
    }

    public function deleteTask($task) {
        try {
            $this->db->beginTransaction();
            $this->db->query("UPDATE `task` SET `deleted` = 1 WHERE `id` = ?", [$task['id']]);
            $this->db->query("UPDATE `maintenance` SET `deleted` = 1 WHERE `task_id` = ?", [$task['id']]);
            $this->db->commit();
            return true;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            return null;
        }
    }

    public function getTaskDeletedFromName($taskName) {
        return $this->db->query_log("SELECT T.*, T.`l_ponderation_id` as ponderation_id from `task` T WHERE T.`name` = ? AND T.`deleted` = 1 LIMIT 0,1", [$taskName])->fetch();
    }

    public function callback($task) {
        $ret = $this->db->query_log("UPDATE `task` SET `deleted` = 0 WHERE `id` = ?", [$task['id']]);
        return empty($ret) ? $ret : $ret->count();
    }

    public function checkRollingTask($task)
    {
          return $this->db->query_log("SELECT * FROM `task`
           WHERE `is_rolling` = 1 AND  `id` = ?"  , [$task['id']])->fetch();
    }

    public function getTaskByMpTemplate($mptempalte_id)
    {
        $ret = $this->db->query_log("SELECT l_rollingTask_id FROM mp_unit_task WHERE mp_unit_id in (SELECT mp_unit.id FROM mp_unit WHERE mp_unit.mp_template_id = ? )", [$mptempalte_id]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }
}