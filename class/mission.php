<?php
/**
 * Created by PhpStorm.
 * User: jb
 * Date: 08/06/2018
 * Time: 20:09
 */

class mission
{
    private $db;
    private $agenda;
    private $mission;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function add_mission($mission_data)
    {
        $mission = [
            'start' => $mission_data->start,
            'end' => $mission_data->end,
            'work_time' => $mission_data->duration,
            'address_id' => $mission_data->address_id,
            'provider_id' => $mission_data->provider_id,
            'mission_token' => $mission_data->mission_token,
            'user_id' => $mission_data->user_id,
            'governess_id' => $mission_data->governess_id,
            'real_start' => $mission_data->start,
            'real_end' => $mission_data->end,
            'real_work_time' => $mission_data->duration,
            'agenda_id' => $mission_data->id
        ];

        $keys = implode(',', array_keys($mission));
        $values = implode('\',\'', array_values($mission));
        $values_with_null = str_replace("''", 'null', $values);

        $ret = $this->db->query_log("INSERT INTO `mission` (" . $keys . ") VALUES('" . $values_with_null . "')");
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    /**
     * @param $start
     * @param $end
     * @param int $user_id
     * @param array $option
     * @return bool OR object
     */
    //TODO: switch database method to have only user_id
    public function getAllMissionForCalendar($start, $end, $user_id, array $option)
    {
        $who = isset($option['who']) ? $option['who'] : 'provider';
        $editable = isset($option['editable']) ? $option['editable'] : true;

        $ret = $this->db->query_log("
        SELECT 
        'event' as rendering,
        m.`agenda_id`,
        'mission_validated' as event_type,
        m.start as start, m.end as end,
        u.firstname, u.lastname," .
            strtoupper($editable) . " as editable,
        a.address as addresses,
        c.customer_token,
        c.id as customer_id,
        'mission_validated' as className,
        'rgb(151, 224, 141)' as color
        FROM `mission` m, `user` u, `address` a, `customer` c
        WHERE 
        m.`start` >= ? 
        AND m.`end` < ? 
        AND m.`" . $who . "_id` = ? 
        AND u.id = m.user_id
        AND a.id = m.address_id
        AND c.user_id = u.id
        ", [$start, $end, $user_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll(PDO::FETCH_ASSOC);
    }

    //OLD CREATEMISSION
    public function get_agenda($agenda_id)
    {
        if (empty($this->agenda)) {
            $ret = $this->db->query_log("SELECT * FROM `agenda` WHERE `id` = ?", [$agenda_id]);
            $this->agenda = $ret === false ? null : $ret->fetch();
        }
        return $this->agenda;
    }

    public function get_mission($mission_id = null)
    {
        if ($mission_id !== null) {
            $ret = $this->db->query_log("SELECT * FROM `mission` WHERE `id` = ?", [$mission_id]);
            $this->mission = $ret === false ? null : $ret->fetch(PDO::FETCH_ASSOC);
        }
        return $this->mission;
    }


    public function get_missionByUserID($user_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `mission` m   WHERE m.`user_id` = ? ", [$user_id]);
          if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function is_mission_validated($start_datetime, $agenda_id)
    {
        $ret = $this->db->query_log(
            "SELECT * FROM `mission` WHERE `agenda_id` = ? AND `start` = ?",
            [$agenda_id, $start_datetime])->fetch(PDO::FETCH_ASSOC);
        if ($ret === false)
            return false;
        return $ret->fetch(PDO::FETCH_ASSOC);
    }

    public function start_mission($start, $end)
    {
        $this->construct_mission($start, $end);
        $mission_keys_token = substr(str_repeat('?,', count($this->mission)), 0, -1);
        $mission_keys = implode(',', array_keys($this->mission));
        $mission_values = array_values($this->mission);

        $ret = $this->db->query_log(
            "INSERT INTO `mission`(" . $mission_keys . ") VALUES (" . $mission_keys_token . ")",
            $mission_values
        );

        if ($ret !== false)
            $this->mission['id'] = $this->db->lastInsertId();
        else
            throw new Exception('Impossible de créer une mission');
    }

    private function construct_mission($start, $end)
    {
        $this->mission = [
            'start' => $start,
            'end' => $end,
            'agenda_id' => $this->agenda->id,
            'user_id' => $this->agenda->user_id,
            'provider_id' => $this->agenda->provider_id,
            'work_time' => $this->agenda->duration,
            'mission_token' => my_crypt::genStandardToken(),
            'address_id' => $this->agenda->address_id,
            'real_start' => $start,
            'real_end' => $end,
            'real_work_time' => $this->agenda->duration
        ];
    }

    public function finish_mission($real_end = null)
    {
        if (empty($this->mission))
            throw new Exception('La mission n\'existe pas');
        if (!empty($this->mission->real_end))
            throw new Exception('La mission est déjà terminée');
        if (parseStr::compare_time_strict($this->mission['real_start'], $real_end))
            throw new Exception('La date de fin commence avant la date de début de mission');
        $real_end = $real_end === null ? date_create()->format("Y-m-d H:i:s") : $real_end;
        $real_work_time = parseStr::subtract_the_time($real_end, $this->mission['real_start']);
        $ret = $this->db->query_log(
            "UPDATE `mission` SET `real_end` = ?, `real_work_time` = ?, `finished` = 1 WHERE `id` = ?",
            [$real_end, $real_work_time, $this->mission['id']]
        );
        if (empty($ret))
            throw new Exception('La date de fin commence avant la date de début de mission');
        $this->mission['real_end'] = $real_end;
        $this->mission['real_work_time'] = $real_work_time;
        $this->mission['finished'] = 1;
    }

    public function create_cart_data_from_mission($mission, $discount_type, $discount_value)
    {
        $item = App::getItem()->getItemById(2);

        $quantity = parseStr::convert_time_to_decimal($mission['work_time']);

        $cart_data = [
            'item_id' => $item->id,
            'price' => $item->price,
            'quantity' => $quantity,
            'promocode_id' => NULL,
            'discount_percentage' => NULL,
            'discount_euro' => NULL,
            'price_final' => $quantity * $item->price
        ];

        if ($discount_type == 'promocode') {
            $cart_data['promocode_id'] = $discount_value;
            $promocode = App::getPromoCode()->getPromoCodeById($discount_value);
            if ($promocode) {
                if ($promocode->percentage) {
                    $cart_data['price_final'] -= $cart_data['price_final'] * ($promocode->percentage / 100);
                } else if ($promocode->euro) {
                    $cart_data['price_final'] -= $promocode->euro;
                }
            }
        } else if ($discount_type == 'discount_percentage') {
            $cart_data['discount_percentage'] = $discount_value;
            $cart_data['price_final'] -= $cart_data['price_final'] * ($discount_value / 100);
        } else if ($discount_type == 'discount_euro') {
            $cart_data['discount_euro'] = $discount_value;
            $cart_data['price_final'] -= $discount_value;
        }

        return $cart_data;
    }

    public function update_mission_cart_id($cart_id)
    {
        $ret = $this->db->query_log(
            "UPDATE `mission` SET `cart_id` = ? WHERE `id` = ?",
            [$cart_id, $this->mission['id']]
        );
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }


    public function update_mission_cur_address($address_id)
    {
        $ret = $this->db->query_log(
            "UPDATE `address` SET mission_cur = mission_cur + 1 WHERE `id` = ?",
            [$address_id]
        );
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }
    //END OLD CREATEMISSION

    public function getTransactionByUser($user_id)
    {
        return $this->db->query_log("SELECT *, ca.`price_final` AS price_final_cart FROM `transaction` t, `cart` ca, `item` i, `mission` m WHERE t.`id` = ca.`transaction_id` AND ca.`item_id` = i.`id` AND i.`item_category_id` = 2 AND m.`cart_id` = ca.`id` AND t.`user_id` = ? AND m.`end` <=  now()", [$user_id])->fetchAll();

    }

    public function checkIfMissionPayed($mission_id)
    {
        $ret = $this->db->query_log("SELECT count(*) AS number, `transaction`.price AS price_transaction, `cart`.price_final AS price_final_cart, `cart`.price_final - `transaction`.price AS difference FROM `transaction`, `cart` WHERE `transaction`.id = `cart`.transaction_id and transaction.id = ?", [$mission_id]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function updatePrice($price, $mission_id)
    {
        $ret = $this->db->query_log("UPDATE `transaction` SET `price` =  `price` + ?   WHERE `id_mission` = ?", [$price, $mission_id]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    public function updateMission($mission_id)
    {
        $ret = $this->db->query_log("UPDATE `transaction` SET `payed` = 1   WHERE `id` = ?", [$mission_id]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    public function getPriceDiff($mission_id)
    {
        $ret = $this->db->query_log("select count(*) AS number_row, `cart`.price_final AS price_final , `transaction`.price,  `transaction`.payed, `cart`.price_final - `transaction`.price AS difference from `transaction` , `cart` where `transaction`.id = `cart`.transaction_id and `transaction`.id = ?", [$mission_id]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function  getPriceOfMission($mission_id)
    {
        $ret = $this->db->query_log("SELECT `price_final` FROM `cart` ct  WHERE ct.`id` = ? ", [$mission_id]);
         if($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getMissionByID($mission_id)
    {
        $ret = $this->db->query_log("SELECT * FROM `mission` WHERE `id` = ?", [$mission_id]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function  getMissionAndUser($mission_id)
    {
        $ret = $this->db->query_log("select * from mission m , user u where m.user_id = u.id and u.who = \"customer\" and m.id = ?", [$mission_id]);
        if($ret === false)
            return false;
        return $ret->fetch();
    }

     public function getCartByMission($mission_id)
    {
        $ret = $this->db->query_log("SELECT `price_final` from `cart` where cart.`id` = ?", [$mission_id]);
        if($ret === false)
            return false;
        return $ret->fetch();
    }

    public function addToMissionTransactionPaiement($transaction_id)
    {
        $ret = $this->db->query_log("INSERT INTO `mission_transction_cart`(`id_trantwo`, `id_refund`) 
                                      values((select id from `trantwo` where `idTransc` = ?), null)",
            [$transaction_id,]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function addToMissionTransactionRefund($idTranscation)
    {
        $this->db->query_log("INSERT INTO `mission_transction_cart`(`id_trantwo`, `id_refund`) values(null, (select id from `trantwo` where `idTransc` = ?))",
            [$idTranscation,]);
        return $this->db->lastInsertId();
    }

}
