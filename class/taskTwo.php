<?php
/**
 * Created by PhpStorm.
 * User: koussai
 * Date: 18/06/2018
 * Time: 15:02
 */

class taskTwo
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->query_log("SELECT * FROM `list` WHERE (`listName` = 'rollingTask' OR `listName` = 'task') AND `deleted` = 0 ORDER BY `position` ASC")->fetchAll();
    }

    public function getTasks($is_rolling)
    {
        return $this->db->query_log("SELECT * FROM `list` WHERE `listName` = ? AND `deleted` = 0 ORDER BY `position` ASC", [$is_rolling ? 'rollingTask' : 'task'])->fetchAll();
    }

    public function getTaskById($task_id)
    {
        return $this->db->query_log("SELECT * FROM `list` WHERE `id` = ? AND `deleted` = 0", [$task_id])->fetch();
    }

    public function editTask($id, $name, $is_rolling)
    {
        $ret = $this->db->query_log("UPDATE `list` SET `field` = ?, `listName` = ? WHERE `id` = ?",
            [
                $name,
                $is_rolling ? "rollingTask" : "task",
                $id
            ]);
        return empty($ret) ? $ret : $ret->rowCount();
    }

    public function addTask($name, $is_rolling, $position)
    {
        if($is_rolling == 1){
            $this->db->query_log("INSERT INTO `list`(`listName`, `field`, `position`) values('rollingTask', ?, ? )",
                [
                    $name,
                    $position

                ]);
            return $this->db->lastInsertId();
        }else {
            $this->db->query_log("INSERT INTO `list`(`listName`, `field`, `position`) values('task', ?, ? )",
                [
                    $name,
                    $position
                ]);

            return $this->db->lastInsertId();
        }

    }

    public function deleteTask($data) {
        $ret = $this->db->query_log("UPDATE `list` SET `deleted` = 1 WHERE `id` = ?", [$data['id']]);
        if ($ret === false){
            return false;
        }
        return $ret->rowCount();
    }



}
