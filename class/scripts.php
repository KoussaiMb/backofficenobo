<?php
//TODO: Do something before 2099
/**
 * Created by PhpStorm.
 * User: damie
 * Date: 23/05/2017
 * Time: 10:58
 */
class scripts
{
    private $arr = [];
    private $filesToDel = [];
    private $path;
    private $dueDate;

    public function __construct($path)
    {
        $this->dueDate = 2;
        $this->path = $path;
    }

    public function delFiles()
    {
        $this->getFiles($this->path);
        $this->parseOldFiles();
        if (!$this->delList())
            return -1;
        return count($this->filesToDel);
    }

    private function parseOldFiles()
    {

        foreach ($this->arr as $k => $v)
        {
            $exploded = explode('-', $v['file']);
            $try_year = substr($exploded[1], 0, 4);
            $try_year2 = substr($exploded[1], 4, 4);
            if ($try_year >= 2017 && $try_year <= 2099 && $this->isDeleteMustDo($exploded[1]) ||
                $try_year2 >= 2017 && $try_year2 <= 2099 && $this->isDeleteMustDo($exploded[1], 'dmY'))
                array_push($this->filesToDel, $v['path']);
        }
    }

    private function isDeleteMustDo($str, $format = 'Ymd')
    {
        if ($format == 'dmY')
            $str = substr($str, 4, 4) . substr($str, 2, 2) . substr($str, 0, 2);
        $datetime1 = new DateTime("now");
        $datetime2 = new DateTime($str);
        $interval = $datetime1->diff($datetime2);
        if ($interval->m >= $this->dueDate)
            return true;
        return false;
    }

    private function delList()
    {
        foreach ($this->filesToDel as $k => $v)
        {
            unlink($v);
            if (file_exists($v))
                return false;
        }
        return true;
    }

    private function getFiles($path)
    {
        $dir = scandir($path);
        foreach ($dir as $k => $v){
            if (preg_match("/^(.|..)$/", $v))
                continue;
            $new_path = $path . '/' . $v;
            if (is_dir($new_path))
                $this->getFiles($new_path);
            elseif (preg_match("^OLD-\d{8}", $v)){
                array_push($this->arr, ['path' => $new_path, 'file' => $v]);
            }
        }
    }
}