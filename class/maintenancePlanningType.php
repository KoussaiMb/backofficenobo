<?php

class maintenancePlanningType{

    private $db;
    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function addMpType($data)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_template`(`min_area`, `max_area`, `ironing`,
         `l_recurrence_id`, `childs`, `name`) values(?, ?, ?, ?, ?, ?)",
            [
                $data['min_area'],
                $data['max_area'],
                $data['ironing'] == false ? 0 : 1,
                empty($data['l_recurrence_id']) ? null : $data['l_recurrence_id'],
                $data['childs'] == false ? 0 : 1,
                $data['name']
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function editMpType($data)
    {
        $ret = $this->db->query_log("UPDATE 
                                          `mp_template` 
                                          SET 
                                          `min_area` = ?, 
                                          `max_area` = ?, 
                                          `ironing` = ?, 
                                          `l_recurrence_id` = ?,  
                                          `childs` = ?, 
                                          `name` = ? 
                                          WHERE `id` = ?",
            [
                $data['min_area'],
                $data['max_area'],
                empty($data['ironing']) ? 0 : 1,
                empty($data['l_recurrence_id']) ? null: $data['l_recurrence_id'],
                empty($data['childs']) ? 0 : 1,
                $data['name'],
                $data['mp_template_id']
            ]);
        if ($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function deleteMPType($mp_template_id) {
        $ret = $this->db->query_log("UPDATE `mp_template` SET `deleted` = 1 WHERE `id` = ?", [$mp_template_id]);
        if ($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function deleteMpUnitById($mp_unit_id)
    {
        $ret = $this->db->query("DELETE FROM `mp_unit` WHERE `id` = ?", [$mp_unit_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function getAll()
    {
        $ret = $this->db->query_log("SELECT mp.id, mp.name, mp.min_area, 
                                            mp.max_area, mp.ironing, mp.childs, l.field as recurrence
									  FROM `mp_template` mp
									  LEFT JOIN `list` l ON mp.l_recurrence_id = l.id
									  WHERE mp.`deleted` = 0
									  ORDER BY `name` ASC");
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }


    public function getAllComparedByAddressToken($address_token)
    {
        $ret = $this->db->query_log("SELECT mp.id, mp.name, mp.min_area, mp.max_area,
                                     mp.ironing, mp.childs, mp.l_recurrence_id, l.field, 
                                    (SELECT COUNT(*) FROM mp_unit WHERE mp_template_id = mp.id) as mpu_nb,
                                    IF(
                                    a.surface >= mp.min_area
                                    AND a.surface <= mp.max_area
                                    AND a.have_ironing = mp.ironing
                                    AND mp.childs = IF(a.childNb >= 1, 1, 0)
                                    AND mp.l_recurrence_id = a.recurrence_id, 
                                    'match',
                                    'no match' 
                                    ) as matching,
                                    (
                                    IF (a.surface >= mp.min_area, 20, 0)
                                    + IF (a.surface <= mp.max_area, 20, 0)
                                    + IF (a.have_ironing = mp.ironing, 20, 0)
                                    + IF (mp.childs = IF(a.childNb >= 1, 1, 0), 20, 0)
                                    + IF (mp.l_recurrence_id = a.recurrence_id, 20, 0)
                                    ) as matching_percentage 
                                     FROM `mp_template` mp 
                                     INNER JOIN `list` l ON mp.l_recurrence_id = l.id
                                     INNER JOIN `address` a ON a.token = ?
                                     WHERE mp.`deleted` = 0
                                     ORDER BY `name` ASC", [$address_token]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function insertMpUnitTask($mp_unit_id, $l_rolling_task_id) {
        $ret = $this->db->query_log("INSERT INTO 
                                            `mp_unit_task`
                                            SET 
                                            `mp_unit_id` = ?,
                                            `l_rollingTask_id` = ?", [$mp_unit_id, $l_rolling_task_id]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function insertMpUnit($mp_template_id)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_unit` SET `mp_template_id` = ?", [$mp_template_id]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function insertMPT($mp_unit_id, $l_rollingTask_id, $address_id)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_unit_task`(`mp_unit_id`, `l_rollingTask_id`, `address_id`) values(?,?,?)",
            [
                $mp_unit_id,
                $l_rollingTask_id,
                $address_id
            ]);
        if($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function addNobleMaterials($data)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_noble_materials`(`address_id`, `l_noble_materials_id`, `lr_room_id` , `comment`) values(?, ?, ?, ?)",
            [
                $data['address_id'],
                $data['l_noble_materials_id'],
                $data['lr_room_id'],
                $data['comment']
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function deleteNobleMaterials($mp_noble_materials_id) {
        $ret = $this->db->query_log("DELETE FROM `mp_noble_materials` WHERE `id` = ?", [$mp_noble_materials_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function getMpTemplateById($mp_id)
    {
        $ret = $this->db->query_log("SELECT mp.id, mp.name, mp.min_area, mp.max_area,
									mp.ironing, mp.childs, l.field, mp.l_recurrence_id
									FROM `mp_template` mp
									LEFT JOIN `list` l ON mp.l_recurrence_id = l.id
									WHERE mp.id = ?
									AND mp.deleted = 0", [$mp_id]);

        return $ret == false ? false : $ret->fetch();
    }

    public function getMpUnitTaskByMpUnitId($mp_unit_id)
    {
        $ret = $this->db->query_log("SELECT l.field AS field, mput.l_rollingTask_id, mp.id
                                     FROM mp_unit mp
                                     INNER JOIN mp_unit_task mput ON mput.mp_unit_id = mp.id
                                     INNER JOIN list l ON l.id = mput.l_rollingTask_id
                                     WHERE mp.id = ?", [$mp_unit_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getMpUnitByTemplateId($mp_id)
    {
        $ret = $this->db->query_log("SELECT 
                                        mpu.id, mpu.l_maintenanceType_id, mpu.mp_template_id, 
                                        l.field as maintenanceType,
                                        GROUP_CONCAT(mput.l_rollingTask_id) as `rolling_task_ids`,
                                        GROUP_CONCAT(lrol.`field`) as `rolling_task_names`,
                                        GROUP_CONCAT(mput.`id`) as `mp_unit_task_ids`,
                                        COUNT(mput.`id`) as task_unit_nb
                                        FROM `mp_unit` mpu
                                        LEFT JOIN `list` l ON l.`id` = mpu.`l_maintenanceType_id`
                                        LEFT JOIN `mp_unit_task` mput ON mput.`mp_unit_id` = mpu.`id`
                                        LEFT JOIN `list` lrol ON lrol.`id` = mput.`l_rollingTask_id`
                                        WHERE mpu.`mp_template_id` = ? 
                                        GROUP BY mpu.`id`", [$mp_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getClientOwnTask($mp_address_id, $mp_unit_id)
    {
        $ret = $this->db->query_log("SELECT
                                        l.field as name, mpat.id
                                        FROM `mp_address_task` mpat
                                        INNER JOIN `list` l ON l.`id` = mpat.`l_rollingTask_id`
                                        WHERE mpat.`mp_address_id` = ? AND mpat.`mp_unit_id` = ?
                                        ", [$mp_address_id, $mp_unit_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getAllMaterialsNoble()
    {
        $ret = $this->db->query_log("SELECT mpn.id as id, mpn.comment, l.field as field,
                                     lr.value as lr_value, lr.url as lr_url, ad.address as address,
                                     (SELECT COUNT(*) FROM mp_noble_materials where id = id) as nb
                                     FROM mp_noble_materials mpn 
                                     INNER JOIN list l ON l.id = mpn.l_noble_materials_id 
                                     INNER JOIN list_reference lr ON lr.id = mpn.lr_room_id
                                     INNER JOIN address ad ON ad.id = mpn.address_id");
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function addMpAddress($address_id, $mp_template_id, $comment)
    {
        $ret = $this->db->query_log("INSERT INTO `mp_address`(`address_id`, `mp_template_id`, `comment`) values(?, ?, ?)",
            [
                $address_id,
                $mp_template_id,
                $comment
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function editMpAddress($address_id, $mp_template_id, $comment, $id)
    {
        $ret = $this->db->query_log("UPDATE `mp_address` SET `address_id` = ?, `mp_template_id` = ?, `comment` = ? WHERE `id` = ?",
            [
                $address_id,
                $mp_template_id,
                $comment,
                $id
            ]);
        return $ret === false ? false : $ret->rowCount();
    }

    public function deleteMpUnitTaskById($mp_unit_task_id) {
        $ret = $this->db->query("DELETE FROM `mp_unit_task` WHERE `id` = ?", [$mp_unit_task_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function editMpUnit($mp_unit_id, $l_maintenanceType_id)
    {
        $ret = $this->db->query_log("UPDATE `mp_unit` SET `l_maintenanceType_id` = ?  WHERE `id` = ?",
            [
                $l_maintenanceType_id,
                $mp_unit_id
            ]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function getNobleMaterialsByAddressId($address_id)
    {
        $ret = $this->db->query_log("
SELECT 
nm.id, nm.address_id, nm.comment,
lr.url as lr_url, lr.value as lr_value,
l.id as nm_id, l.field as nm_value
FROM 
mp_noble_materials nm
INNER JOIN `list` l ON l.`id` = nm.`l_noble_materials_id` 
INNER JOIN `list_reference` lr ON lr.`id` = nm.`lr_room_id` 
WHERE address_id = ? ", [$address_id]);
        if($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getMaterialsByAddressId($address_id)
    {
        $ret = $this->db->query_log("
SELECT m.id, m.comment, l.field as name, lr.value as room, lr.url as url
FROM mp_noble_materials m 
INNER JOIN `list` l ON l.id = m.l_noble_materials_id
INNER JOIN `list_reference` lr ON lr.id = m.lr_room_id 
WHERE m.address_id  = ?
", [$address_id ]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }

    public function getMpAddressByAddressId($address_id)
    {
        $ret = $this->db->query_log("SELECT 
                                          mpt.id as mp_template_id, mpt.name, 
                                          mpa.id as mp_address_id, mpa.comment
                                          FROM mp_address mpa
                                          INNER JOIN mp_template mpt ON mpa.mp_template_id = mpt.id 
                                          WHERE address_id = ?
                                         ", [$address_id]);
        if ($ret === false)
            return false;
        $mp_address = $ret->fetch();
        if ($mp_address === false)
            return null;
        return $mp_address;
    }

    /*TODO: doesnt work because of duplicate keys
    public function getMpUnitByTemplateIdWithAddressId($mp_template_id, $mp_address_id)
    {
        $ret = $this->db->query_log("
SELECT 
mpu.id, mpu.l_maintenanceType_id, mpu.mp_template_id, 
l.field as maintenanceType,
GROUP_CONCAT(mput.l_rollingTask_id) as `rolling_task_ids`,
GROUP_CONCAT(lrol.`field` SEPARATOR ';') as `rolling_task_names`,
GROUP_CONCAT(mput.`id` SEPARATOR ';') as `mp_unit_task_ids`,
GROUP_CONCAT(lrolbis.`field` SEPARATOR ';') as `mp_address_task_names`,
GROUP_CONCAT(lrolbis.`id` SEPARATOR ';') as `mp_address_task_ids`
FROM `mp_unit` mpu
LEFT JOIN `list` l ON l.`id` = mpu.`l_maintenanceType_id`
LEFT JOIN `mp_unit_task` mput ON mput.`mp_unit_id` = mpu.`id`
LEFT JOIN `list` lrol ON lrol.`id` = mput.`l_rollingTask_id`
LEFT JOIN `mp_address_task` mpat ON mpat.`mp_unit_id` = mpu.`id` AND mpat.`mp_address_id` = ?
LEFT JOIN `list` lrolbis ON lrolbis.`id` = mpat.`l_rollingTask_id`
WHERE mpu.`mp_template_id` = ?
GROUP BY mpu.id", [$mp_address_id, $mp_template_id]);
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }
*/

    public function addAddressRollingTask($data) {
        $ret = $this->db->query_log("
INSERT INTO `mp_address_task`
SET `mp_address_id` = ?, `mp_unit_id` = ?, `l_rollingTask_id` = ?",
            [
                $data['mp_address_id'],
                $data['mp_unit_id'],
                $data['l_rollingTask_id']
            ]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function deleteAddressRollingTask($mp_address_task_id) {
        $ret = $this->db->query_log("DELETE FROM `mp_address_task` WHERE `id` = ?", [$mp_address_task_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }
}
