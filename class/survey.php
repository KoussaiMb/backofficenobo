<?php


class survey
{
    public $db;

    function __construct($db){
        $this->db = $db;
    }

    public function getTableNames()
    {
        return $this->db->query_log('SELECT `table_name` FROM information_schema.tables WHERE table_schema= ?', [$_SERVER['HTTP_MYSQL_DBNAME']])->fetchAll();
    }

    public function getColumnsInTable($table)
    {
        return $this->db->query_log("SELECT `column_name` FROM information_schema.columns WHERE table_schema= ? AND `table_name` = ?", [$_SERVER['HTTP_MYSQL_DBNAME'], $table])->fetchAll();
    }

    public function getAutocompleteTable()
    {
        $table_autocomplete= [];

        $ret = $this->getTableNames();
        if ($ret == false){
            return false;
        }
        foreach ($ret as $tab) {
            array_push($table_autocomplete, ['value' => $tab->table_name, 'data' => ['table_name' => $tab->table_name],]);
        }
        return json_encode($table_autocomplete);
    }

    function insertFieldReference($table, $field, $name)
    {
        $this->db->query_log("INSERT INTO `field_reference` (`table_name`, `field_name`, `ref_name`) VALUES (?,?,?)",[$table, $field, $name]);
        return $this->db->lastInsertId();
    }

    public function getAllFieldsReferences()
    {
        return $this->db->query_log("SELECT * FROM `field_reference`")->fetchAll();
    }


    public function deleteReferenceById($ref_id)
    {
        $ret =  $this->db->query_log("DELETE FROM field_reference WHERE id = ?",[$ref_id]);
        if($ret == false){
            return 0;
        }
        return true;
    }

    function insertQuestion($input, $ref_field, $name, $text, $position)
    {
        $this->db->query_log("INSERT INTO `question` (`l_input_type_id`,`field_ref_id`,`survey_name`, `question_text`, `position`) VALUES (?,?,?,?,?)",[$input, $ref_field, $name, $text, $position]);
        return $this->db->lastInsertId();
    }

    function updateQuestion($q_id, $input, $ref_field, $text)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `l_input_type_id` = ?,`field_ref_id` = ?,`question_text` = ? WHERE `id` = ?",[$input, $ref_field, $text, $q_id])->rowCount();
        if ($ret === false){
            return 0;
        }
        return 1;
    }

    function insertOfferedAnswer($question_id, $ans_text, $score, $position)
    {
        $this->db->query_log("INSERT INTO `offered_answer` (`question_id`, `answer_text`, `score`, `position`) VALUES (?,?,?,?)",[$question_id, $ans_text, $score, $position]);
        return $this->db->lastInsertId();
    }

    function updateOfferedAnswer($off_id, $ans_text, $score, $position)
    {
        $ret = $this->db->query_log("UPDATE `offered_answer` SET `answer_text` = ?, `score` = ?, `position` = ? WHERE `id` = ? ",[$ans_text, $score, $position, $off_id]);
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function getQuestionsBySurveyName($survey_name)
    {
        return $this->db->query_log("SELECT * FROM `question` WHERE `survey_name` = ? AND `deleted` = 0 ORDER BY `position`", [$survey_name])->fetchAll();
    }

    public function getOfferedAnswersByQuestionId($q_id)
    {
        return $this->db->query_log("SELECT * FROM `offered_answer` WHERE `question_id` = ? ORDER BY `position`", [$q_id])->fetchAll();
    }

    public function insertAnswer($user_id, $q_id, $other, $offered_ans_id = NULL)
    {
        $this->db->query_log("INSERT INTO `answer` (`user_id`, `question_id`, `offered_answer_id`, `other_text`) VALUES (?,?,?,?);",[$user_id, $q_id, $offered_ans_id, $other]);
        return $this->db->lastInsertId();
    }

    public function updateAnswer($user_id, $q_id, $other, $offered_ans_id = NULL)
    {
        $ret = $this->db->query_log("UPDATE `answer` SET `offered_answer_id` = ?, `other_text` = ? WHERE `user_id` = ? AND `question_id` = ?",[$offered_ans_id, $other, $user_id, $q_id])->rowCount();
        if($ret == false){
            return 0;
        }
        return 1;
    }

    public function halfDeleteQuestion($q_id)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `deleted` = 1 WHERE `id` = ?", [$q_id])->rowCount();
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function halfDeleteSurvey($survey_name)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `deleted` = 1 WHERE `survey_name` = ?", [$survey_name])->rowCount();
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function halfDeleteQuestionByRefId($ref_id)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `deleted` = 1 WHERE `field_ref_id` = ?", [$ref_id])->rowCount();
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function getQuestionsByRefId($ref_id)
    {
        return $this->db->query_log("SELECT `id` FROM `question` WHERE `field_ref_id` = ?", [$ref_id])->fetchAll();
    }

    public function getPositionByQuestionId($q_id)
    {
        return $this->db->query_log("SELECT position FROM `question` WHERE `id` = ?", [$q_id])->fetch();
    }

    public function getPositionByOfferedAnswerId($off_id)
    {
        return $this->db->query_log("SELECT position FROM `offered_answer` WHERE `id` = ?", [$off_id])->fetch();
    }

    public function getSurveyByQuestionId($q_id)
    {
        return $this->db->query_log("SELECT survey_name FROM `question` WHERE `id` = ?", [$q_id])->fetch();
    }

    public function getDistinctSurveyNames()
    {
        return $this->db->query_log("SELECT DISTINCT survey_name FROM `question` WHERE `deleted` = 0")->fetchAll();
    }

    public function getLastPositionInSurvey($survey_name)
    {
        return $this->db->query_log("SELECT MAX(position) as max_pos FROM `question` WHERE survey_name = ?",[$survey_name])->fetch();
    }

    public function getLastNotDeletedPositionInSurvey($survey_name)
    {
        return $this->db->query_log("SELECT MAX(position) as max_pos FROM `question` WHERE survey_name = ? AND deleted = 0",[$survey_name])->fetch();
    }

    public function getLastOfferedAnswerByQuestionId($q_id)
    {
        return $this->db->query_log("SELECT MAX(position) as max_pos FROM `offered_answer` WHERE question_id = ?",[$q_id])->fetch();
    }

    public function getQuestionIdByOfferedAnswerId($off_id)
    {
        return $this->db->query_log("SELECT question_id FROM `offered_answer` WHERE id = ?",[$off_id])->fetch();
    }

    public function getQuestionById($q_id)
    {
        return $this->db->query_log("SELECT * FROM `question` WHERE `id` = ?",[$q_id])->fetch();
    }

    public function getQuestionAndTypeById($q_id)
    {
        return $this->db->query_log("SELECT q.*, l.`field` FROM `question` q, `list` l WHERE q.`id` = ? AND l.`id` = q.`l_input_type_id` ",[$q_id])->fetch();
    }

    public function updatePositionByQuestionId($q_id, $pos)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `position` = ? WHERE `id` = ?", [$pos, $q_id])->rowCount();
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function modifyPositionBySurveyName($survey_name, $position, $sens)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `position` = `position` + ? WHERE `survey_name`= ? AND `position` > ?", [$sens, $survey_name, $position])->rowCount();
        if($ret == false){
            return 0;
        }
        return 1;
    }

    public function modifyOfferedAnswerPositionByQuestionId($q_id, $position, $sens)
    {
        $ret = $this->db->query_log("UPDATE `offered_answer` SET `position` = `position` + ? WHERE `question_id`= ? AND `position` > ?", [$sens, $q_id, $position])->rowCount();
        if($ret == false){
            return 0;
        }
        return 1;
    }

    public function getQuestionBySurveyNameAndPosition($surveyName, $pos)
    {
        return $this->db->query_log("SELECT * FROM `question` WHERE `survey_name` = ? AND `position` = ?",[$surveyName, $pos])->fetch();

    }

    public function getScoreByAnswerId($answer_id)
    {
        return $this->db->query_log("SELECT score FROM `offered_answer` oa INNER JOIN `answer` a ON  oa.id = a.offered_answer_id WHERE a.`id` = ?",[$answer_id])->fetch();
    }

    public function getScoreByOfferedAnswerId($off_answer_id)
    {
        return $this->db->query_log("SELECT score FROM `offered_answer` WHERE `id` = ?",[$off_answer_id])->fetch();
    }

    public function updateSurveyName($curr_name, $new_name)
    {
        $ret = $this->db->query_log("UPDATE `question` SET `survey_name` = ? WHERE `survey_name` = ?", [$new_name, $curr_name])->rowCount();
        if ($ret == false){
            return 0;
        }
        return 1;
    }

    public function getAnswersByQuestionAndUserId($q_id, $user_id)
    {
        return $this->db->query_log("SELECT a.`offered_answer_id`, a.`other_text` FROM `answer` a WHERE a.`question_id` = ? AND a.`user_id` = ?",[$q_id, $user_id])->fetchAll();
    }

    public function getAnswerByQuestionAndUserId($q_id, $user_id)
    {
        return $this->db->query_log("SELECT a.`offered_answer_id`, a.`other_text` FROM `answer` a WHERE a.`question_id` = ? AND a.`user_id` = ?",[$q_id, $user_id])->fetch();
    }

    public function deleteAnswersByQuestionAndUserId($q_id, $user_id)
    {
        $ret =  $this->db->query_log("DELETE FROM `answer` WHERE `question_id` = ? AND `user_id` = ?",[$q_id, $user_id]);
        if($ret == false){
            return 0;
        }
        return true;
    }

    public function deleteOfferedAnswer($off_id)
    {
        $ret =  $this->db->query_log("DELETE FROM `offered_answer` WHERE `id` = ?",[$off_id]);
        if($ret == false){
            return 0;
        }
        return true;
    }

    public function insertValueWithTableAndColumn($table, $column, $value, $user_id)
    {
        $this->db->query_log("INSERT INTO `$table` (`user_id`, `$column`, `date_answer`) VALUES (?, ?, ?) 
                                ON DUPLICATE KEY UPDATE `id` = LAST_INSERT_ID(id),  `$column` = ?",[$user_id, $value, date("Y-m-d H:i:s"), $value]);
        return $this->db->lastInsertId();
    }

    public function updateValueWithTableAndColumn($table, $column, $value, $user_id)
    {
        $ret =  $this->db->query_log("UPDATE `$table` SET `$column` = ? WHERE `user_id` = ?",[$value, $user_id]);
        if($ret == false){
            return 0;
        }
        return $ret->rowCount();
    }

    public function getTableAndColumnByRefId($ref_id)
    {
        return $this->db->query_log("SELECT * FROM `field_reference` WHERE `id` = ?",[$ref_id])->fetch();
    }

    public function getOfferedAnswerById($off_id)
    {
        return $this->db->query_log("SELECT * FROM `offered_answer` WHERE `id` = ?",[$off_id])->fetch();
    }

    public function getTypeByQuestionId($q_id)
    {
        return $this->db->query_log("SELECT `field` FROM `question` q INNER JOIN `list` l ON l.`id` = q.`l_input_type_id` 
                                      WHERE q.`id` = ?",[$q_id])->fetch();
    }

    public function getUserAnswerByUserAndField($user_id, $table, $column)
    {
        return $this->db->query_log("SELECT `$column` as answer FROM `$table` WHERE `user_id` = ?",[$user_id])->fetch();
    }

    public function insertSurveyThreshold($survey_name, $score, $quantity, $action){
        $this->db->query_log("INSERT INTO `survey_threshold` (`name`, `score`, `quantity`, `action`) VALUES (?, ?, ?, ?)", [$survey_name, $score, $quantity, $action]);
        return $this->db->lastInsertId();
    }

    public function updateSurveyThreshold($survey_name, $score, $quantity, $action){
        $ret = $this->db->query_log("UPDATE `survey_threshold` SET `quantity` = ? WHERE `name` = ? AND `score` = ? AND `action` = ?", [$quantity, $survey_name, $score, $action]);
        if($ret !== false){
            return 1;
        }
        return 0;
    }

    public function deleteSurveyThreshold($survey_name){
        $ret = $this->db->query_log("DELETE FROM `survey_threshold` WHERE `name` = ?", [$survey_name]);
        if($ret !== false){
            return $ret;
        }
        return 0;
    }

    public function getThresholdBySurveyName($survey_name, $action)
    {
        return $this->db->query_log("SELECT * FROM `survey_threshold` WHERE `name` = ? AND `action` = ? ORDER BY `score` ASC",[$survey_name, $action])->fetchAll();
    }
}