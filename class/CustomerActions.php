<?php

/**
 * file: CustomerActions.php
 * auhtor: Jonathan BRICE
 * date: 05-04-2018
 * DESCription:
 **/

class CustomerActions
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getActions()
    {
        return App::getListManagement()->getList('customerActions');
    }

    public function getActionHistoryByFromId($fromId)
    {
        return $this->db->query_log("SELECT 
                                        ah.id, ah.l_customerActions_id, ah.from_id, 
                                        ah.to_id, ah.action_date, ah.message,
                                        from_u.firstname from_firstname, 
                                        from_u.lastname from_lastname, 
                                        to_u.firstname to_firstname, to_u.lastname to_lastname,
                                        la.field action_name, lr.field call_request_name,
                                        cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                        COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                  FROM `action_history` ah
                                  LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                  LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                  LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                  INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                  INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                  INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE ah.from_id = ?
                                  ORDER BY 
                                    cr.request_time DESC, 
                                    cr.request_date DESC, 
                                    order_date DESC", [$fromId])->fetchAll();
    }

    public function getActionHistoryByToId($toId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                    WHERE ah.to_id = 56
                                    ORDER BY 
                                    cr.request_time DESC, 
                                    cr.request_date DESC, 
                                    order_date DESC", [$toId])->fetchAll();
    }

    public function getActionHistoryByToToken($toToken)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE to_u.user_token = ?
                                  ORDER BY 
                                    cr.request_time DESC, 
                                    cr.request_date DESC, 
                                    order_date DESC", [$toToken])->fetchAll();
    }

    public function getActionHistoryByUserId($userId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE (ah.to_id = ? OR ah.from_id = ?)
                                  ORDER BY 
                                    cr.request_time DESC, 
                                    cr.request_date DESC, 
                                    order_date DESC", [$userId, $userId])->fetchAll();
    }

    public function getLastActionByFromId($fromId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE ah.from_id = ?
                                  ORDER BY order_date DESC
                                  LIMIT 1", [$fromId])->fetch();
    }

    public function getLastActionByToId($toId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE ah.to_id = ?
                                  ORDER BY order_date DESC
                                  LIMIT 1", [$toId])->fetch();
    }

    public function getLastActionByUserId($userId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE (ah.from_id = ? OR ah.to_id = ?)
                                  ORDER BY order_date DESC
                                  LIMIT 1", [$userId, $userId])->fetch();
    }

    public function getLastActionByUserIdAndType($userId, $type)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled,
                                            COALESCE(cr2.done, cr2.canceled, ah.action_date) order_date
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE (ah.from_id = ? OR ah.to_id = ?)
                                  AND la.position = ?
                                  ORDER BY order_date DESC
                                  LIMIT 1", [$userId, $userId, $type])->fetch();
    }

    public function getActionInfoById($actionId)
    {
        return $this->db->query_log("SELECT 
                                            ah.id, ah.l_customerActions_id, ah.from_id, 
                                            ah.to_id, ah.action_date, ah.message,
                                            from_u.firstname from_firstname, 
                                            from_u.lastname from_lastname, 
                                            to_u.firstname to_firstname, to_u.lastname to_lastname,
                                            la.field action_name, lr.field call_request_name,
                                            cr.id request_id,
                                            cr.request_date, cr.request_time, cr2.done, cr2.canceled
                                    FROM `action_history` ah
                                    LEFT JOIN `call_request` cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL )                                     
                                    LEFT JOIN `call_request` cr2 ON (ah.id = cr2.action_history_id AND (cr2.done IS NOT NULL OR cr2.canceled IS NOT NULL) )
                                    LEFT JOIN `list` lr ON (cr.l_call_request_id = lr.id OR cr2.l_call_request_id = lr.id)
                                    INNER JOIN `user` from_u ON (ah.from_id = from_u.id)
                                    INNER JOIN `user` to_u ON (ah.to_id = to_u.id)
                                    INNER JOIN `list` la ON (ah.l_customerActions_id = la.id)
                                  WHERE ah.id = ?", [$actionId])->fetch();
    }

    public function addActionHistory($customerAction, $fromId, $toId, $message)
    {
        $ret = $this->db->query_log("INSERT INTO
                                      `action_history`(`l_customerActions_id`, `from_id`, `to_id`, `action_date`, `message`)
                                      VALUES (?, ?, ?, NOW(), ?)", [$customerAction, $fromId, $toId, $message]);

        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }

    public function editActionHistory($actionId, $message)
    {
        $ret = $this->db->query_log("UPDATE `action_history` SET `message` = ? WHERE `id` = ?", [$message, $actionId]);

        if ($ret == false)
            return 0;
        return $this->getActionInfoById($actionId);
    }

    public function deleteActionHistory($actionId)
    {
        $ret = $this->db->query_log("DELETE FROM `action_history` WHERE `id`=?", [$actionId]);

        if ($ret == false)
            return 0;
        return 1;
    }

    public function addCallRequest($actionId, $call_date, $call_time, $call_request_id)
    {
        $ret = $this->db->query_log("INSERT INTO
                                      `call_request`(`action_history_id`, `request_date`, `request_time`, `l_call_request_id`)
                                      VALUES (?, ?, ?, ?)", [$actionId, $call_date, $call_time, $call_request_id]);

        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }


    public function terminateCallRequestById($call_request_id) 
    {
        $ret = $this->db->query_log("UPDATE `call_request` SET done = NOW() WHERE id = ?", [$call_request_id]);

        return $ret;
    }

    public function cancelCallRequestById($call_request_id) 
    {
        $ret = $this->db->query_log("UPDATE `call_request` SET canceled = NOW() WHERE id = ?", [$call_request_id]);

        return $ret;
    }
}

?>
