<?php
class MenuManager {

    public $db;

    function __construct($db) {
        $this->db = $db;
    }

    function getAllMenus() {
        return $this->db->query_log("SELECT id, url, menu_name, glyphicon, icon_url
                                    FROM `menu_bo`
                                    WHERE deleted = 0")->fetchAll();
    }

    function getMenuById($menu_id) {
        return $this->db->query_log("SELECT id, url, menu_name, glyphicon, icon_url
                                    FROM `menu_bo`
                                    WHERE id = ?
                                    AND deleted = 0", [$menu_id])->fetch();
    }

    function getMenuByName($menu_name) {
        return $this->db->query_log("SELECT id, url, menu_name, glyphicon, icon_url
                                    FROM `menu_bo`
                                    WHERE name = ?
                                    ANd deleted = 0", [$menu_name])->fetchAll();
    }

    function getMenuByUrl($url) {
        return $this->db->query_log("SELECT id, url, menu_name, glyphicon, icon_url
                                    FROM `menu_bo`
                                    WHERE url LIKE ?
                                    AND deleted = 0", [$url])->fetch();
    }

    function addMenu($url, $menu_name, $glyphicon, $icon_url) {
        $ret = $this->db->query_log("INSERT INTO `menu_bo` (url, menu_name, glyphicon, icon_url)
                                    VALUES (?,?,?,?)", [$url, $menu_name, $glyphicon, $icon_url]);
        if ($rest == false) {
            return 0;
        }
        return $this->db->lastInsertId();
    }

    function deleteMenuById($menu_id) {
        $ret = $this->db->query_log("UPDATE `menu_bo` SET deleted = 1 WHERE id = ?", [$menu_id]);
        if ($rest == false) {
            return 0;
        }
        return 1;
    }

    function getOptionsByMenuId($menu_id) {
        return $this->db->query_log("SELECT id, option_name, option_value
                                    FROM `menu_bo_options`
                                    WHERE menu_bo_id = ?
                                    AND deleted = 0",
                                    [$menu_id])->fetchAll();
    }

    function addOption($option_name, $option_value, $menu_id) {
        $ret = $this->db->query_log("INSERT INTO `menu_bo_options`
                                    (`menu_bo_id`, `option_name`, `option_value`)
                                    VALUES
                                    (?, ?, ?)",
                                    [$menu_id, $option_name, $option_value]);
        if ($ret == false)
            return 0;
        return $this->db->lastInsertId();
    }

    function deleteOption($option_id) {
        $ret = $this->db->query_log("UPDATE `menu_bo_options` SET deleted = 1 WHERE id = ?", [$option_id]);
        if ($ret == false) {
            return 0;
        }
        return 1;
    }
}
?>
