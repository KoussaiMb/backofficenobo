<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 02/04/2018
 * Time: 13:20
 */

/*
 **** HOW IT WORKS ****
 * You have a Table Agenda with a start_rec, an end_rec and a day. If the first occur is edited, we apply first link
 * functions. For last occur, last link functions, else, between link functions.
 ****
 */


class providerEditPlanning
{
    private $db;
    private $fullCalendar;

    function __construct(DB $db, fullCalendar $fullCalendar)
    {
        $this->db = $db;
        $this->fullCalendar = $fullCalendar;
    }

    private function init_edit_data($data_mission, $data_common){
        return [
            "start" => $data_mission['start'],
            "end" => $data_mission['end'],
            "start_rec" => $data_mission['start_rec'],
            "end_rec" => $data_mission['end_rec'],
            "day" => $data_mission['day'],
            "id" => $data_common['id']
        ];
    }

    private function init_add_data($data_mission, $data_common){
        return [
            "user_token" => $data_common['user_token'],
            "customer_token" => $data_common['customer_token'],
            "address_token" => $data_common['address_token'],
            "title" => $data_common['title'],
            "day" => $data_mission['day'],
            "start" => $data_mission['start'],
            "end" => $data_mission['end'],
            "duration" => $data_common['duration'],
            "type" => $data_common['type'],
            "locked" => $data_common['locked'],
            "pending" => $data_common['pending'],
            "start_rec" => $data_mission['start_rec'],
            "end_rec" => $data_mission['end_rec']
        ];
    }

    private function check_if_false($ret, $message = 'Erreur lors de l\'édition de la mission', $code = 520) {
        if ($ret === false) {
            $json = new parseJson($code, $message, null);
            $json->printJson();
        }
    }

    private function noMethod() {
        $json = parseJson::error('Aucune méthode trouvée lors de l\'édition de mission');
        $json->printJson();
    }

    /* First link functions - BEGIN */

    public function pending_planning_first_link($data){
        if ($data['edit'] !== 'all' && $data['edit'] !== 'single')
            $this->noMethod();
        if ($data['edit'] == 'single') {
            $data_mission_edit = [
                "start" => $data['start'][1],
                "end" => $data['end'][1],
                "start_rec" => $data['start'][0],
                "end_rec" => $data['end'][0],
                "day" => date("w", (strtotime(date($data['start'][0]))))
            ];
            $data_mission_add = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => empty($data['moved_agenda']->end_rec) ? NULL : $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];

            $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
            $result = $this->fullCalendar->editAgenda($data_edit);
            $this->check_if_false($result);
            $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
        $result = $this->fullCalendar->PendingAgendaEvent($data['data_common']['id']);
        $this->check_if_false($result);
    }


    public function delete_planning_first_link($data){
        if ($data['edit'] !== 'all' && $data['edit'] !== 'single')
            $this->noMethod();
        if ($data['edit'] == 'single') {
            $data_mission_edit = [
                "start" => $data['start'][1],
                "end" => $data['end'][1],
                "start_rec" => $data['start'][0],
                "end_rec" => $data['end'][0],
                "day" => date("w", (strtotime(date($data['start'][0]))))
            ];
            $data_mission_add = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => empty($data['moved_agenda']->end_rec) ? NULL : $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];

            $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
            $result = $this->fullCalendar->editAgenda($data_edit);
            $this->check_if_false($result);
            $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
        $result = $this->fullCalendar->delAgendaEvent($data['data_common']['id']);
        $this->check_if_false($result);
    }

    public function edit_planning_first_link($data){
        if ($data['edit'] !== 'all' && $data['edit'] !== 'single')
            $this->noMethod();
        if ($data['edit'] == 'single') {
            $data_mission_edit = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => empty($data['moved_agenda']->end_rec) ? NULL : $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];
            $data_mission_add = [
                "start" => $data['start'][1],
                "end" => $data['end'][1],
                "start_rec" => $data['start'][0],
                "end_rec" => $data['end'][0],
                "day" => date("w", (strtotime(date($data['start'][0]))))
            ];

            $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
            $result = $this->fullCalendar->editAgenda($data_edit);
            $this->check_if_false($result);
            $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
        else if ($data['edit'] == 'all') {
            $data_mission = [
                "start" => $data['start'][1],
                "end" => $data['end'][1],
                "start_rec" => $data['start'][0],
                "end_rec" => empty($data['moved_agenda']->end_rec) ? NULL : $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['start'][0]))))
            ];

            $data_edit = $this->init_edit_data($data_mission, $data['data_common']);
            $result = $this->fullCalendar->editAgenda($data_edit);
            $this->check_if_false($result);
        }
    }

    /* First link functions - END */
    /* Last link functions - BEGIN */

    public function edit_planning_last_link($data){
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $data['end'][0],
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);
    }

    public function delete_planning_last_link($data){
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $data['end'][0],
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);
        $result = $this->fullCalendar->delAgendaEvent($result->id);
        $this->check_if_false($result);
    }

    public function pending_planning_last_link($data){
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $data['end'][0],
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);
        $result = $this->fullCalendar->PendingAgendaEvent($result->id);
        $this->check_if_false($result);
    }

    /* Last link functions - END */
    /* Between link functions - BEGIN */

    public function edit_planning_between_link($data){
        if ($data['edit'] !== 'single' && $data['edit'] !== 'all')
            $this->noMethod();
        $end_rec = $data['edit'] == 'all' ? $data['moved_agenda']->end_rec : $data['start'][0];
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $end_rec,
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);

        if ($data['edit'] == 'single') {
            $data_mission_add_second = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];

            $data_add = $this->init_add_data($data_mission_add_second, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
    }

    public function delete_planning_between_link($data){
        if ($data['edit'] !== 'single' && $data['edit'] !== 'all')
            $this->noMethod();
        $end_rec = $data['edit'] == 'all' ? $data['moved_agenda']->end_rec : $data['start'][0];
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $end_rec,
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);
        $result = $this->fullCalendar->delAgendaEvent($result->id);
        $this->check_if_false($result);

        if ($data['edit'] == 'single') {
            $data_mission_add_second = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];

            $data_add = $this->init_add_data($data_mission_add_second, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
    }

    public function pending_planning_between_link($data){
        if ($data['edit'] !== 'single' && $data['edit'] !== 'all')
            $this->noMethod();
        $end_rec = $data['edit'] == 'all' ? $data['moved_agenda']->end_rec : $data['start'][0];
        $data_mission_edit = [
            "start" => $data['moved_agenda']->start,
            "end" => $data['moved_agenda']->end,
            "start_rec" => $data['moved_agenda']->start_rec,
            "end_rec" => date('Y-m-d', strtotime('last ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
            "day" => date("w", (strtotime(date($data['previous_date']))))
        ];
        $data_mission_add = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $end_rec,
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];

        $data_edit = $this->init_edit_data($data_mission_edit, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
        $data_add = $this->init_add_data($data_mission_add, $data['data_common']);
        $result = $this->fullCalendar->addAgenda($data_add);
        $this->check_if_false($result);
        $result = $this->fullCalendar->PendingAgendaEvent($result->id);
        $this->check_if_false($result);

        if ($data['edit'] == 'single') {
            $data_mission_add_second = [
                "start" => $data['moved_agenda']->start,
                "end" => $data['moved_agenda']->end,
                "start_rec" => date('Y-m-d', strtotime('next ' . date('l', strtotime($data['previous_date'])), strtotime($data['previous_date']))),
                "end_rec" => $data['moved_agenda']->end_rec,
                "day" => date("w", (strtotime(date($data['previous_date']))))
            ];

            $data_add = $this->init_add_data($data_mission_add_second, $data['data_common']);
            $result = $this->fullCalendar->addAgenda($data_add);
            $this->check_if_false($result);
        }
    }

    /* Between link functions - END */
    /* Alone link functions - BEGIN */

    public function edit_planning_alone_link($data){
        $data_mission = [
            "start" => $data['start'][1],
            "end" => $data['end'][1],
            "start_rec" => $data['start'][0],
            "end_rec" => $data['end'][0],
            "day" => date("w", (strtotime(date($data['start'][0]))))
        ];
        $data_edit = $this->init_edit_data($data_mission, $data['data_common']);
        $result = $this->fullCalendar->editAgenda($data_edit);
        $this->check_if_false($result);
    }

    public function delete_planning_alone_link($data){
        $result = $this->fullCalendar->delAgendaEvent($data['data_common']['id']);
        $this->check_if_false($result);
    }

    public function pending_planning_alone_link($data){
        $result = $this->fullCalendar->PendingAgendaEvent($data['data_common']['id']);
        $this->check_if_false($result);
    }

    /* Alone link functions - END */

    public function check_pos_in_list($data, $action){
        if (parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->start_rec) && !parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->end_rec)){

            if ($action == 'delete'){
                $this->delete_planning_first_link($data);
            }
            else if ($action == 'pending'){
                $this->pending_planning_first_link($data);
            }
            else if ($action == 'edit') {
                $this->edit_planning_first_link($data);
            }
        }
        /**
         * if the slot are the last link in set of slot
         */
        else if (parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->end_rec) && !parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->start_rec)){

            if ($action == 'delete'){
                $this->delete_planning_last_link($data);
            }
            else if ($action == 'pending'){
                $this->pending_planning_last_link($data);
            }
            else if ($action == 'edit') {
                $this->edit_planning_last_link($data);
            }
        }
        /**
         * if the slot are between link in set of slot
         */
        else if (parseStr::compare_time_strict($data['previous_date'], $data['moved_agenda']->start_rec) && (parseStr::compare_time_strict($data['moved_agenda']->end_rec, $data['previous_date']) || empty($data['moved_agenda']->end_rec))){

            if ($action == 'delete'){
                $this->delete_planning_between_link($data);
            }
            else if ($action == 'pending'){
                $this->pending_planning_between_link($data);
            }
            else if ($action == 'edit') {
                $this->edit_planning_between_link($data);
            }
        }
        /**
         * if the slot are the isolate link
         */
        else if (parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->start_rec) && parseStr::compare_time_equal($data['previous_date'], $data['moved_agenda']->end_rec)){

            if ($action == 'delete'){
                $this->delete_planning_alone_link($data);
            }
            else if ($action == 'pending'){
                $this->pending_planning_alone_link($data);
            }
            else if ($action == 'edit') {
                $this->edit_planning_alone_link($data);
            }
        }
    }

    /**
     * @param $data_constraint
     * @param $agenda
     * @param $error_message
     */
    public function check_by_type($data_constraint, $agenda, $error_message){
        if (($data_constraint['type'] == 2 && $agenda->type == 1) || ($data_constraint['type'] == 1 && $agenda->type == 2))
            ;
        else if ($agenda->type == 3 && (((date("W", strtotime($agenda->start_rec)) % 2 == 1) && $data_constraint['type'] == 2) || ((date("W", strtotime($agenda->start_rec)) % 2 == 0) && $data_constraint['type'] == 1)))
            ;
        else
            $this->check_if_false(false, $error_message, 204);
    }

    public function is_same_address($data_constraint){
        $data_agenda = [
            "day" => $data_constraint['day'],
            "user_token" => $data_constraint['user_token'],
            "id" => $data_constraint['id'],
            "end_date" =>  $data_constraint['end'][0],
            "edit" => $data_constraint['edit'],
            "date" => $data_constraint['start'][0],
            "type" => $data_constraint['type']
        ];

        $moved_slot = $this->fullCalendar->getAgendaById($data_constraint['id']);
        $this->check_if_false($moved_slot);
        $day_agenda = $this->fullCalendar->getAgendaByDay($data_agenda);
        $this->check_if_false($day_agenda);

        if (!empty($day_agenda)) {
            foreach ($day_agenda as $index => $agenda) {
                if (($data_constraint['type'] == 0 || $data_constraint['type'] == 3) && ($agenda->address_id == $moved_slot[0]->address_id))
                    $this->check_if_false(false, "Vous ne pouvez pas placer deux mission avec la meme adresse le meme jour", 204);
                else if ($agenda->address_id == $moved_slot[0]->address_id)
                    $this->check_by_type($data_constraint, $agenda, "Vous ne pouvez pas placer deux mission avec la même adresse le meme jour");
             }
        }
    }

    public function is_conflict_overlap($data_constraint){
        $data = [
            "user_token" => $data_constraint['user_token'],
            "start" => $data_constraint['start'][1],
            "end" => $data_constraint['end'][1],
            "day" => $data_constraint['day'],
            "agenda_id" => $data_constraint['id'],
            "start_date" => $data_constraint['end'][0],
            "end_date" => $data_constraint['end_rec']
        ];

        $overlap_agenda = $this->fullCalendar->getAgendaConflict($data);
        $this->check_if_false($overlap_agenda);

        if (!empty($overlap_agenda)) {
            if ($data_constraint['type'] == 0)
                $this->check_if_false(false, "Un créneaux empèche l'édition des mission", 204);
            else if ($data_constraint['type'] == 3)
                return ;
            foreach ($overlap_agenda as $index => $agenda) {
                $this->check_by_type($data_constraint, $agenda, "Un créneaux empèche l'édition des mission");
            }
        }
    }

    public function is_pass_constraint($data_constraint){
        $this->is_same_address($data_constraint);
        $this->is_conflict_overlap($data_constraint);
    }
}