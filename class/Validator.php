<?php
/**
 * Created by PhpStorm.
 * User: damie
 * Date: 04/11/2016
 * Time: 10:42
 */
class Validator
{
    private $data;
    private $errors = [];

    public function __construct($data){
        $this->data = $data;
    }

    /*
     * Private functions
     */

    private function getField($field){
        if (!isset($this->data[$field]))
            return null;
        return $this->data[$field];
    }

    private function is_empty($field){
        return $this->getField($field) == null;
    }

    /*
     * Public functions
     */

    public function is_not_null($field, $errValid, $errEmpty) {
        if ($errEmpty && $this->is_empty($field)) {
            $this->errors[$field] = $errEmpty;
        }
        else if ($errValid && $this->getField($field) == null) {
            $this->errors[$field] = $errValid;
        }
    }

    public function is_uniq($db, $table, $field, $value, $errMsg, $errEmpty = null){
        if ($errEmpty && $this->is_empty($value)){
            $this->errors[$value] = $errEmpty;
        }
        else {
            try {
                $record = $db->query("SELECT 1 FROM $table WHERE $field = ?", [$this->getField($value)])->fetch();
            } catch (PDOException $e) {
                $this->errors[$value] = 'Un problème inconnu est survenu';
            }
            if (!empty($record)) {
                $this->errors[$value] = $errMsg;
            }
        }
    }

    public function is_uniqButHimself($db, $table, $field, $value, $id, $errMsg, $errEmpty = null){
        if ($errEmpty && $this->is_empty($value)){
            $this->errors[$value] = $errEmpty;
        }
        else {
            try {
                $record = $db->query("SELECT 1 FROM $table WHERE $field = ? AND `id` != ?", [$this->getField($value), $id])->fetch();
            } catch (PDOException $e) {
                $this->errors[$value] = 'Un problème inconnu est survenu';
            }
            if (isset($record) && $record) {
                $this->errors[$value] = $errMsg;
            }
        }
    }

    public function is_group_uniq($db, $value, $errMsg, $errEmpty = null){
        if ($errEmpty && $this->is_empty($value)){
            $this->errors[$value] = $errEmpty;
        }
        else {
            try {
                $record = $db->query("SELECT 1 FROM `groupe` WHERE `name` = ? AND `deleted` = 0", [$this->getField($value)])->fetch();
            } catch (PDOException $e) {
                $this->errors[$value] = 'Un problème inconnu est survenu';
            }
            if (isset($record) && $record) {
                $this->errors[$value] = $errMsg;
            }
        }
    }

    public function exist($db, $table, $field, $data, $errMsg, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        else {
            try {
                $record = $db->query("SELECT 1 FROM $table WHERE $field = ?", [$this->getField($data)])->fetch();
            } catch (PDOException $e) {
                $this->errors[$field] = 'Un problème inconnu est survenu';
            }
            if (isset($record) && !$record) {
             $this->errors[$field] = $errMsg;
            }
        }
    }

    public function is_confirmed($db, $field){
        $record = $db->getRow('pending', 'USER', $field, $this->getField($field));
        if ($record && !$record->pending)
            return true;
        return false;
    }

    public function is_color_hex($field, $errMsg, $errEmpty = null){
        if ($errEmpty !== null && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if (preg_match("/^#[a-zA-Z]{6}$/", $this->getField($field)))
            $this->errors[$field] = $errMsg;
    }

    public function date_isok($db){
        $record = $db->getRow('asked_at', 'USER', 'email', $this->getField('email'));
        $d1 = $record->asked_at;
        $d2 = new DateTime("now");
        if ($record->asked_at && date_diff($d1, $d2) < 1)
            return false;
        return true;
    }

    public function can_confirm($db){
        $record = $db->getRow('pending', 'USER', 'id', $this->getField('id'));
        if ($record && $record->pending == 1)
            return true;
        return false;
    }

    public function is_len($field, $min_range = null, $max_range = null, $errValid) {
        $len = strlen($this->getField($field));
        if ($min_range && $len < $min_range)
            $this->errors[$field] = $errValid;
        if ($max_range && $len > $max_range)
            $this->errors[$field] = $errValid;
    }

    public function is_bool($field, $errValid, $errEmpty) {
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if ($this->getField($field) !== false && $this->getField($field) !== true)
            $this->errors[$field] = $errValid;
    }

    public function is_bool_string($field, $errValid, $errEmpty) {
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if ($this->getField($field) !== 'false' && $this->getField($field) !== 'true')
            $this->errors[$field] = $errValid;
    }

    public function is_alpha($field, $errValid, $errEmpty = null, $range_len = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[a-zA-Z \-_]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function no_symbol($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(preg_match("/^[\"]+$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_url($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[a-zA-Z0-9-\/_.]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }
    public function is_special_char_alpha($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[^\d\s][^\"]*[\w \-éèà]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }
    public function isToken($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[a-fA-F0-9]{20}$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }
    public function is_alphanum($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[0-9a-zA-Z ]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_specialchars_alphanum($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field)){
            $this->errors[$field] = $errEmpty;
        }
        else if(!preg_match("/^[0-9a-zA-Z éèàù&_]*$/", $this->getField($field))){
            $this->errors[$field] = $errValid;
        }
    }

    public function is_stringRange($field, $errValid, $errEmpty = null, $range){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if (strlen($this->getField($field)) < $range[0] || $range[1] < strlen($this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function in_list($field, $list, $errValid){
        $i = 0;
        $max = count($list);

        while ($i < $max) {
            if($this->getField($field) == $list[$i]->id){
                $isOk = 1;
                break;
            }
            $i++;
        }
        if (empty($isOk))
            $this->errors[$field] = $errValid;
    }

    public function in_db_list($field, $db, $errValid){
        if ($this->is_empty($field))
            return ;
        $ret = $db->query_log("SELECT `field` FROM `list` WHERE `id` = ?", [$this->getField($field)])->fetch();
        if (!$ret)
            $this->errors[$field] = $errValid;
    }

    public function is_digicode($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[0-9a-zA-Z]*$/", $this->getField($field)) || strlen($this->getField($field)) > 8)
            $this->errors[$field] = $errValid;
    }

    public function is_cp($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[0-9]*$/", $this->getField($field)) || strlen($this->getField($field)) != 5)
            $this->errors[$field] = $errValid;
    }
    public function is_range($field, $errValid = null, $range){
        if ($this->getField($field) < $range[0] || $this->getField($field) > $range[1]){
            if ($errValid == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else
                $this->errors[$field] = $errValid;
        }
    }
    public function is_digit($field, $errValid = null, $errEmpty = null, $nb_digit = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if($nb_digit && strlen($this->getField($field)) > $nb_digit)
            $this->errors[$field] = "Le nombre entré est trop grand";
        else if(!preg_match("/^[0-9]*$/", $this->getField($field))){
            if($errValid == null){
                $this->errors[$field] = 'Le format n\'est pas valide';
            }else {
                $this->errors[$field] = $errValid;
            }
        }
    }

    public function is_num($field, $errValid = null, $errEmpty = null, $nb_digit = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if($nb_digit && strlen($this->getField($field)) > $nb_digit)
            $this->errors[$field] = "Le nombre entré est trop grand";
        else if(!preg_match("/^[0-9]*$/", $this->getField($field))){
            if($errValid == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else
                $this->errors[$field] = $errValid;
        }
    }

    public function is_decimal($field, $errValid = null, $errEmpty = null, $nb_digit = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if($nb_digit && strlen($this->getField($field)) > $nb_digit)
            $this->errors[$field] = "Le nombre entré est trop grand";
        else if(!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/", $this->getField($field))){
            if($errValid == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else
                $this->errors[$field] = $errValid;
        }
    }


    public function is_id($field, $errValid = null, $errEmpty = null, $nb_digit = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if ($nb_digit && strlen($this->getField($field)) > $nb_digit)
            $this->errors[$field] = "Le nombre entré est trop grand";
        else if (!preg_match("/^[0-9]*$/", $this->getField($field))){
            if($errValid == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else
                $this->errors[$field] = $errValid;
        }
    }

    public function in_array($field, $array, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!in_array($this->getField($field), $array))
            $this->errors[$field] = $errValid;
    }

    public function is_pack_duedate($field, $errEmpty = null)
    {
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if (!preg_match("/^[0-9 ]*$/", $this->getField($field))) {
            if ($errEmpty == null)
                $this->errors[$field] = 'Le format n\'est pas valide';
            else if ($this->getField($field) < 1 || $this->getField($field) > 24)
                $this->errors[$field] = 'La date limite du pack doit se situer entre 1 et 24 mois';
        }
    }

    public function is_siret($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^[0-9 ]*$/", $this->getField($field)) && strlen($this->getField($field)) != 14)
            $this->errors[$field] = $errValid;
    }


    //On remplace les lettres accentutées par les non accentuées dans $fichier.
    //Et on récupère le résultat dans fichier
    //En dessous, il y a l'expression régulière qui remplace tout ce qui n'est pas une lettre non accentuées ou un chiffre
    //dans $fichier par un tiret "-" et qui place le résultat dans $fichier.
    public function clean_string($fichier){
        $fichier = strtr($fichier,
            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

        return $fichier;
    }





    public function is_file($file, $destination, $maxsize = false, $extensions = false){
        if (!isset($_FILES[$file]) OR $_FILES[$file]['error'] > 0)
            $this->errors['file'] = "Le document suivant: $file est manquant ou corrompu";
        else if ($maxsize !== false AND $_FILES[$file]['size'] > $maxsize)
            $this->errors['file'] = 'La taille de ' . $_FILES[$file]['name'] . ' est trop importante';
        else if ($extensions !== false AND is_array($extensions) AND !in_array(substr(strrchr($_FILES[$file]['name'],'.'),1), $extensions))
            $this->errors['file'] = 'Veuillez upload un fichier au format pdf';
        else if (!move_uploaded_file($_FILES[$file]['tmp_name'], $destination))
            $this->errors['file'] = 'Le fichier n\'a pas pu être upload';
    }

    public function is_logo($file, $maxsize = 5, $extensions = ['svg', 'png', 'jpg']){
        if (empty($file) || $file['error'] > 0)
            $this->errors['file'] = "Le logo est manquant ou corrompu";
        else if (!isset($maxsize) && $file['size'] > $maxsize)
            $this->errors['file'] = 'La taille du logo ' . $file['name'] . ' est trop importante';
        else if (!isset($extensions) && !in_array(substr(strrchr($file['name'],'.'),1), $extensions))
            $this->errors['file'] = 'Extension du logo incorrect';
    }

    public function is_valid_html2pdf_logo($file, $maxsize = 5, $extensions = ['png', 'jpg']){
        if (empty($file) || $file['error'] > 0) {
            $this->errors['file'] = "Le logo est manquant ou corrompu";
        }
        else if (!isset($maxsize) && $file['size'] > $maxsize){
            $this->errors['file'] = 'La taille du logo ' . $file['name'] . ' est trop importante';
        }
        else if (!isset($extensions) && !in_array(substr(strrchr($file['name'],'.'),1), $extensions)){
            $this->errors['file'] = 'Extension du logo incorrect';
        }
    }

    public function is_logo_uploaded($file, $name, $destination, $maxsize = 20000, $extensions = ['svg', 'png', 'jpg']) {
        if (empty($file) || $file['error'] > 0) {
            $this->errors[$name] = "Le logo est manquant ou corrompu";
            return null;
        }
        $file_extension = substr(strrchr($file['name'], '.'), 1);
        $logo_url = $destination . $name . '.' . $file_extension;

       if (!empty($maxsize) && $file['size'] > $maxsize)
            $this->errors[$name] = 'La taille du logo ' . htmlspecialchars($file['name']) . ' est trop importante (>5ko)';
        else if (empty($file_extension) || !empty($extensions) && !in_array($file_extension, $extensions))
            $this->errors[$name] = 'Extension du logo incorrect';
        else if (empty($logo_url) || !move_uploaded_file($file['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $logo_url))
            $this->errors[$name] = 'Le fichier ' . htmlspecialchars($file['name']) . ' n\'a pas pu être upload';
        if (isset($this->errors[$name]))
            return null;
        return $logo_url;
    }

    public function is_uploaded($file, $destination, $name){
        $file_extension = substr(strrchr($file['name'], '.'), 1);
        $logo_url = $destination . $name . '.' . $file_extension;

        if (empty($file) || !$file['tmp_name'] || !$file['name'] || empty($logo_url))
            $this->errors['file'] = "Le fichier est inexistant";
        else if (!isset($logo_url) || !move_uploaded_file($file['tmp_name'], $logo_url))
            $this->errors['file'] = 'Le fichier ' . htmlspecialchars($file['name']) . ' n\'a pas pu être upload';
        return $logo_url;
    }

    public function is_photo_profile($file, $maxsize = false, $extensions = false){
        if (!isset($_FILES[$file]) OR $_FILES[$file]['error'] > 0)
            $this->errors['file'] = "Le document suivant: $file est manquant ou corrompu";
        else if ($maxsize !== false AND $_FILES[$file]['size'] > $maxsize)
            $this->errors['file'] = 'La taille de ' . $_FILES[$file]['name'] . ' est trop importante';
        else if ($extensions !== false AND is_array($extensions) AND !in_array(substr(strrchr($_FILES[$file]['name'],'.'),1), $extensions))
            $this->errors['file'] = 'Veuillez upload un fichier au format jpg ou png';
    }

    public function is_email($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!filter_var($this->getField($field), FILTER_VALIDATE_EMAIL))
            $this->errors[$field] = $errValid;
    }

    public function is_stripeToken($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/(tok_)([a-zA-Z0-9]*)$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }
    public function is_stripeKey($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/[a-zA-Z0-9_]+$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }
    public function is_pw($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,20}$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_pw_light($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/^(?=.*\d)(?=.*[a-zA-Z]).{4,20}$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_same($field1, $field2, $errMsg){
        if($this->getField($field1) !== $this->getField($field2))
            $this->errors[$field2] = $errMsg;
    }

    public function is_phone($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("#^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$#", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    //(0|(\+33)|(0033))[1-9][0-9]{8}

    public function is_date($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!$this->valid_date($this->getField($field), 'd-m-Y') && !$this->valid_date($this->getField($field), 'd/m/Y') && !$this->valid_date($this->getField($field), 'd,m,Y'))
            $this->errors[$field] = $errValid;
    }

    public function is_date_bo($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(empty($this->getField($field)))
            ;
        else if(!$this->valid_date($this->getField($field), 'Y-m-d'))
            $this->errors[$field] = $errValid;
    }

    private function valid_date($date, $format = 'd-m-Y'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    ///^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
    public function is_time($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if (!preg_match("/^(24:00)|(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_timeFormat($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if (!preg_match("/^[0-9][0-9]:[0-5][0-9]:[0-5][0-9]*$/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function is_time_superior($field_time_1, $field_time_2, $errValid){
        if (!$this->is_empty($field_time_1) && !$this->is_empty($field_time_2))
        {
            $d1 = strtotime($this->getField($field_time_1));
            $d2 = strtotime($this->getField($field_time_2));
            if ($d1 - $d2 > 0)
                $this->errors[$field_time_1] = $errValid;
        }
    }

    public function is_array($field, $errValid, $errEmpty = null){
        if ($errEmpty && $this->is_empty($field))
            $this->errors[$field] = $errEmpty;
        else if(!preg_match("/\[.*\]/", $this->getField($field)))
            $this->errors[$field] = $errValid;
    }

    public function throwException($field, $errMessage)
    {
        $this->errors[$field] = $errMessage;
    }

    public function is_valid(){
        return empty($this->errors);
    }

    public function getErrors(){
        return $this->errors;
    }

    public function getError($field){
        return isset($this->errors[$field]);
    }

    public function getErrorString($field){
        if ($this->getError($field))
            return $this->errors[$field];
        return false;
    }

    public function getData(){
        return $this->data;
    }
}