<?php

class Workflow
{
    private $activeStepNumber;
    private $wfName;
    private $steps;
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

    /**
    * Get steps methods
    **/

    public function getStepsByName($wf_name){
        return $this->db->query_log("SELECT wf.id, wf.name, wf.step, wf.step_name, wf.url
                                    FROM `workflow` wf
                                    WHERE wf.name = ?
                                    ORDER BY wf.step ASC", [$wf_name])->fetchAll();
    }

    public function getStepById($wf_id){
        return $this->db->query_log("SELECT wf.id, wf.name, wf.step, wf.step_name, wf.url
                                    FROM `workflow` wf
                                    WHERE wf.id = ?", [$wf_id])->fetch();
    }

    public function getStepByStepAndName($wf_id, $wf_name){
        return $this->db->query_log("SELECT wf.id, wf.name, wf.step, wf.step_name, wf.url
                                    FROM `workflow` wf
                                    WHERE wf.step = ?
                                    AND wf.name = ?", [$wf_id, $wf_name])->fetch();
    }

    public function getStepByUserId($user_id){
        return $this->db->query_log("SELECT wf.id, wf.name, wf.step, wf.step_name, wf.url
                                    FROM `user` u, `customers` c, `waiting_list` wl, `workflow` wf
                                    WHERE u.id = ?
                                    AND u.id = c.user_id
                                    AND c.id = wl.customer_id
                                    AND wf.id = wl.workflow_id",
                                    [$user_id])->fetch();
    }


    public function getStepByCustomerId($customer_id){
        return $this->db->query_log("SELECT wf.id, wf.name, wf.step, wf.step_name, wf.url FROM `workflow` wf, customer c, waiting_list w
                                      WHERE wf.id = w.workflow_id
                                      AND c.id = w.customer_id
                                      AND c.id = ?", [$customer_id])->fetch();
    }

    public function getWorkflowByStep($wf_step)
    {
        return $this->db->query_log("SELECT wf.`id`, wf.name, wf.step, wf.step_name, wf.url FROM `workflow` wf
                                      WHERE wf.step = ?", [$wf_step])->fetch();
    }

    /**
    *  Get customer methods
    **/

    public function getCustomerByWaitingId($w_id){
        $ret = $this->db->query_log("SELECT c.id, c.user_id, w.workflow_id, c.call_duration,
                                            u.firstname, u.lastname, u.phone, u.email,
                                            u.user_token, u.gender_id, u.maritalStatus_id,
                                            a.address, a.surface, a.roomNb, a.waterNb,
                                            a.zipcode, a.city, a.batiment, a.digicode,
                                            a.digicode_2, a.doorBell, a.door, a.floor,
                                            a.token as address_token, a.recurrence_id, a.nbHours,
                                            a.peopleHome, w.pending, w.address_id,
                                            w.alerte, w.accepted, w.date_list_in,
                                            w.date_list_out, a.id address_id,
                                            a.l_pet_id, a.wcNb, c.l_acquisition_id,
                                            a.have_ironing, a.have_key, a.childNb
                                      FROM `waiting_list` w, `customer` c, `user` u, `address` a
                                      WHERE c.id = w.customer_id
                                      AND c.user_id = u.id
                                      AND w.address_id = a.id
                                      AND w.id = ?",
                                      [$w_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    /**
    * Get waiting list methods
    **/

    public function getAllCustomersInWaitingList($filter = "wf.step DESC")
    {
        return $this->db->query_log("SELECT u.firstname, u.lastname, u.phone, a.address,
                                            a.city, a.zipcode, a.workTime, a.surface,
                                            c.call_duration, w.id, w.date_list_in,
                                            a.recurrence_id, l.field recurrence, w.workflow_id, u.user_token,
                                            u.id user_id, w.pending, w.accepted, wf.step, w.alerte, wf.name workflow_name,
                                            wf.url,
                                            (
                                                SELECT 
                                                    ah.action_date
                                                FROM action_history ah
                                                INNER JOIN list l ON (ah.l_customerActions_id = l.id)
                                                WHERE l.position = 1
                                                AND ah.to_id = u.id
                                                ORDER BY ah.action_date DESC
                                                LIMIT 1
                                            ) last_call,
                                            (
                                                SELECT 
                                                    l.position
                                                FROM action_history ah
                                                INNER JOIN call_request cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL)
                                                INNER JOIN list l ON (cr.l_call_request_id = l.id)
                                                WHERE ah.to_id = u.id
                                                ORDER BY cr.request_time DESC, cr.request_date DESC
                                                LIMIT 1
                                            ) call_request,
                                            (
                                                SELECT 
                                                    CONCAT(cr.request_date, ' ', COALESCE(cr.request_time, '00:00:00'))
                                                FROM action_history ah
                                                INNER JOIN call_request cr ON (ah.id = cr.action_history_id AND cr.done IS NULL AND cr.canceled IS NULL)
                                                INNER JOIN list l ON (cr.l_call_request_id = l.id)
                                                WHERE ah.to_id = u.id
                                                ORDER BY cr.request_time DESC, cr.request_date DESC
                                                LIMIT 1
                                            ) call_request_time
                                    FROM `user` u
                                    INNER JOIN `customer` c ON (u.id = c.user_id)
                                    INNER JOIN `waiting_list` w ON (c.id = w.customer_id)
                                    INNER JOIN `address` a ON (w.address_id = a.id)
                                    LEFT JOIN `workflow` wf ON (w.workflow_id = wf.id)
                                    LEFT JOIN `list` l ON (a.recurrence_id = l.id)
                                    WHERE u.`deleted` = 0 AND w.deleted = 0 
                                    AND w.accepted = 0 AND w.date_list_out IS NULL
                                    ORDER BY -call_request DESC, w.alerte DESC, call_request_time ASC, " .$filter)->fetchAll();
    }

    public function getNewCustomersInWaitingList()
    {
        return $this->db->query_log("SELECT u.firstname, u.lastname, u.phone, a.address,
                                            a.city, a.zipcode, a.workTime, a.surface,
                                            c.call_duration, c.last_call_at, w.id, w.date_list_in,
                                            a.recurrence_id, w.workflow_id, u.user_token,
                                            u.id user_id
                                      FROM `user` u, customer c, address a, waiting_list w
                                      WHERE c.user_id = u.id
                                      AND a.user_id = u.id
                                      AND w.address_id = a.id
                                      AND w.date_list_out IS NULL
                                      AND u.`deleted` = 0 AND w.deleted = 0 AND w.pending=1 AND w.alerte = 0")->fetchAll();
    }

    public function getWaitingCustomersInWaitingList()
    {
        return $this->db->query_log("SELECT a.surface, a.workTime, a.recurrence_id,
                                            a.address, a.zipcode, a.city, u.firstname,
                                            u.lastname, u.phone, c.call_duration, c.last_call_at,
                                            w.workflow_id, w.id, w.customer_id,
                                            w.address_id, w.date_list_in, u.user_token,
                                            u.id user_id
                                      FROM `waiting_list` w, customer c, `user` u, address a
                                      WHERE w.customer_id = c.id
                                      AND c.user_id = u.id
                                      AND w.address_id = a.id
                                      AND a.deleted = 0
                                      AND w.pending = 0
                                      AND w.deleted = 0
                                      AND w.alerte = 0
                                      AND w.date_list_out IS NULL
                                      AND w.workflow_id = 0 ORDER BY w.date_list_in")->fetchAll();
    }

    public function getProgressCustomersInWaitingList()
    {
        return $this->db->query_log("SELECT a.surface, a.workTime, a.recurrence_id, a.address,
                                            a.zipcode, a.city, u.firstname, u.lastname, u.phone,
                                            c.call_duration, c.last_call_at, w.workflow_id, w.id,
                                            w.customer_id, w.address_id, w.date_list_in,
                                            u.user_token, u.id user_id
                                        FROM `waiting_list` w, customer c, `user` u, address a
                                        WHERE w.customer_id = c.id
                                        AND c.user_id = u.id
                                        AND w.address_id = a.id
                                        AND a.deleted = 0
                                        AND w.deleted = 0
                                        AND w.alerte = 0
                                        AND w.date_list_out IS NULL
                                        AND w.workflow_id != 0 ORDER BY w.date_list_in")->fetchAll();
    }

    public function getAlerteCustomersInWaitingList()
    {
        return $this->db->query_log("SELECT a.surface, a.workTime, a.recurrence_id,
                                            a.address, a.zipcode, a.city, u.firstname,
                                            u.lastname, u.phone, c.call_duration, c.last_call_at,
                                            w.workflow_id, w.id, w.customer_id,
                                            w.address_id, w.date_list_in, u.user_token,
                                            u.id user_id
                                        FROM `waiting_list` w, customer c, `user` u, address a
                                        WHERE w.customer_id = c.id
                                        AND c.user_id = u.id
                                        AND w.address_id = a.id
                                        AND a.deleted = 0
                                        AND w.deleted = 0
                                        AND w.alerte = 1
                                        AND w.date_list_out IS NULL
                                        ORDER BY w.date_list_in")->fetchAll();
    }

    /**
    * Update methods
    **/

    public function updateUserStep($user_id, $wf_id){
        $ret = $this->db->query_log("UPDATE `user` u, `customer` c, `waiting_list` wl
                                    SET wl.workflow_id = ?
                                    WHERE u.id = ?
                                    AND u.id = c.user_id
                                    AND c.id = wl.customer_id",
                                    [$wf_id, $user_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListStep($w_list_id, $step){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.workflow_id = ?
                                    WHERE wl.id = ?",
                                    [$step, $w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListDateListIn($w_list_id){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.date_list_in = NOW()
                                    WHERE wl.id = ?",
                                    [$w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListDateListOut($w_list_id){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.date_list_out = NOW()
                                    WHERE wl.id = ?",
                                    [$w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListAccepted($w_list_id, $accepted){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.accepted = ?
                                    WHERE wl.id = ?",
                                    [$accepted, $w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListPending($w_list_id, $pending){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.pending = ?
                                    WHERE wl.id = ?",
                                    [$pending, $w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListAlerte($w_list_id, $alerte){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.alerte = ?
                                    WHERE wl.id = ?",
                                    [$alerte, $w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }

    public function updateWaitingListDelete($w_list_id, $delete){
        $ret = $this->db->query_log("UPDATE `waiting_list` wl
                                    SET wl.delete = ?
                                    WHERE wl.id = ?",
                                    [$delete, $w_list_id]);
        if ($ret == false)
            return 0;
        return 1;
    }
}
