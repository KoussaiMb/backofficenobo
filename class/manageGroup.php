<?php

class manageGroup
{
    private $db;
    private $groups;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getFeatures() {
        return $this->db->query_log('SELECT * FROM `feature` WHERE `deleted` = 0 ORDER BY `name`')->fetchAll();
    }

    private function getDirectories()
    {
        $all_dir = [];
        $version = 1;
        $dir = $_SERVER['DOCUMENT_ROOT'];
        $func = function(&$s, $key, $v){$s = $v . $s;};
        while ($version) {
            $scan = scandir($dir . '/v' . $version);
            $scan_cleaned = array_diff($scan, array('.', '..'));
            array_walk($scan_cleaned, $func, '/v' . $version . '/');
            $all_dir = array_merge($all_dir, $scan_cleaned);
            $version--;
        }
        return $all_dir;
    }

    private function diffFeaturesDirs($features, $dirs)
    {
        $i = -1;

        while (isset($dirs[++$i])) {
            foreach ($features as $feature) {
                if ($dirs[$i] == $feature->url){
                    unset($dirs[$i]);
                    break;
                }
            }
        }
        return $dirs;
    }

    public function getDirWithoutFeature() {
        $features = $this->getFeatures();
        if ($features === false)
            return null;
        $dirs = $this->getDirectories();
        if (!$dirs)
            return null;
        else if ($features == null)
            return $dirs;
        return $this->diffFeaturesDirs($features, $dirs);
    }

    public function addFeature($data) {
        return $this->db->query_log('
            INSERT INTO `feature`
            SET `name` = ?, `description` = ?, `url` = ?, `created_by` = ?, `created_at` = NOW()',
            [$data['name'], $data['description'], $data['url'], $data['created_by']]);
    }

    public function deleteFeature($feature_id) {
        $ret = $this->db->query_log('UPDATE `feature` SET `deleted` = 1 WHERE `id` = ?', [$feature_id]);
        return $ret->rowCount();
    }

    public function getGroups()
    {
        if (empty($this->groups))
            $this->groups = $this->db->query_log("SELECT * FROM `group_nobo` WHERE `deleted` = 0")->fetchall(PDO::FETCH_ASSOC);
        return $this->groups;
    }

    public function getGroupsWithChildrens()
    {
        $groups = $this->db->query_log("SELECT *
                                        FROM `group_nobo`
                                        WHERE `deleted` = 0
                                        AND parent_id IS NULL")->fetchAll();

        foreach ($groups as $k => $v) {
            $v->childrens = $this->getAllChildernByGroupId($v->id);
            $groups[$k] = $v;
        }

        return $groups;
    }

    public function getGroup($group_id)
    {
        return $this->db->query_log("SELECT * FROM `group_nobo` WHERE `deleted` = 0 AND id = ? LIMIT 0,1", [$group_id])->fetch();
    }

    public function getUsersInGroup($group_id)
    {
        return $this->db->query_log("SELECT `firstname`, `lastname`, `user_token` FROM `user` WHERE `deleted` = 0 AND `group_id` = ?", [$group_id])->fetchAll();
    }

    private function getSpecificGroupRights($group_id)
    {
        return $this->db->query_log("SELECT G.* FROM `group_rights` G WHERE G.`group_id` = ?", [$group_id])->fetchAll();
    }

    private function getSpecificUserRights($user_id)
    {
        return $this->db->query_log("SELECT * FROM `user_rights` WHERE `user_id` = ?", [$user_id])->fetchAll();
    }

    /**
     * @param $user_token string
     * @return array
     * @throws Exception
     */

    public function getUserRights($user_token)
    {
        $user = App::getUser()->getUserByToken($user_token);
        if (!$user)
            throw new Exception('L\'utilisateur est introuvable');
        $rights = $this->getSpecificUserRights($user->id);
        if ($rights === false)
            throw new Exception('Impossible de récupérer les droits de l\'utilisateur');
        $features = $this->getFeatures();
        if ($features === false)
            throw new Exception('Impossible de récupérer les features');
        else if ($features == null)
            throw new Exception('Il n\'a pas de features existantes');
        return $this->unionRightsAndFeature($rights, $features);
    }

    private function unionRightsAndFeature($rights, $features) {
        $temp_rights = array();
        $out = array();

        foreach ($rights as $right)
            $temp_rights[$right->feature_id] = $right;
        foreach ($features as $feature) {
            unset($feature->created_by);
            unset($feature->created_at);
            unset($feature->deleted);

            if (isset($temp_rights[$feature->id]))
                $r = $this->createRights((array)$temp_rights[$feature->id]);
            else
                $r = $this->createEmptyRights();
            $out[] = array_merge($r, (array)$feature);
        }
        return $out;
    }

    private function createEmptyRights()
    {
        return $this->createRights(['add' => 0, 'edit' => 0, 'delete' => 0, 'view' => 0]);
    }

    private function createRights($rights)
    {
        return [
            'add' => $rights['add'],
            'edit' => $rights['edit'],
            'view' => $rights['view'],
            'delete' => $rights['delete']
        ];
    }

    /**
     * @param $group_id
     * @return array
     * @throws Exception
     */

    public function getGroupRights($group_id)
    {
        $group = $this->getGroup($group_id);
        if (!$group)
            throw new Exception('Le groupe est introuvable');
        $rights = $this->getSpecificGroupRights($group->id);
        if ($rights === false)
            throw new Exception('Impossible de récupérer les droits du groupe');
        $features = $this->getFeatures();
        if ($features === false)
            throw new Exception('Impossible de récupérer les features');
        return $this->unionRightsAndFeature($rights, $features);
    }

    /**
     * @param array $data ['group_id'] int
     * @param array $data ['method'] string 'feature_id' OR 'feature_name'
     * @param array $data ['edited_by'] int
     * @param array $data ['rights'] array int OR string
     * @param array $data ['rights'][int] is a feature_id
     * @param array $data ['rights'][string] is a feature_name
     * @param array $data ['rights'][int or string][add] boolean
     * @param array $data ['rights'][int or string][view] boolean
     * @param array $data ['rights'][int or string][delete] boolean
     * @param array $data ['rights'][int or string][edit] boolean
     * @return bool|null
     * @throws Exception for everything
     */

    public function updateGroupRights($data)
    {
        $group = $this->getGroup($data['group_id']);
        if ($group == null)
            throw new Exception('Le groupe est introuvable');
        if ($data['method'] == 'feature_name')
            $check = $this->featureNameToFeatureId($data);
        else
            $check = $this->checkFeaturesId($data);
        if ($check === false)
            throw new Exception('Les features sont invalides');
        $check = $this->isParentRightsOk($data['rights'], $group->parent_id);
        if ($check === false)
            throw new Exception('Le groupe parent ne permet pas cette attribution de droits');
        $rights_list = $this->splitFeatureList($data['rights'], $data['group_id'], 'group');
        if ($rights_list == null)
            throw new Exception('Impossible de récupérer les droits du groupe');
        $this->removeEmptyRights($rights_list['add_list']);
        $ret = $this->updateAndAddRights($rights_list, $data['group_id'], $data['edited_by'], 'group');
        if ($ret == null)
            throw new Exception('Impossible de rentrer les informations dans la base de donnée');
        return $ret;
    }

    public function updateUserRights($data)
    {
            $user = App::getUser()->getUserByToken($data['user_token']);
            if ($user == null)
                throw new Exception('Le groupe est introuvable');
            $this->removeEmptyRights($data['rights']);
            if ($data['method'] == 'feature_name')
                $check = $this->featureNameToFeatureId($data);
            else
                $check = $this->checkFeaturesId($data);
            if ($check === false)
                throw new Exception('Les features sont invalides');
            $rights_list = $this->splitFeatureList($data['rights'], $user->id, 'user');
            if ($rights_list == null)
                throw new Exception('Impossible de récupérer les droits du groupe');
            $this->removeEmptyRights($rights_list['add_list']);
            $ret = $this->updateAndAddRights($rights_list, $user->id, $data['edited_by'], 'user');
            if ($ret == null)
                throw new Exception('Impossible de rentrer les informations dans la base de donnée');
            return $ret;
    }

    public function removeUserRights($user_token)
    {
        return $this->db->query_log("DELETE FROM `group_rights` WHERE `user_id` = (SELECT `id` FROM `user` WHERE `user_token` = ? LIMIT 0,1)", [$user_token]);
    }

    public function removeGroupRights($group_id)
    {
        return $group_id;
        //TODO: Remove Group Rights remove sons rights tho
        //return $this->db->query_log("DELETE FROM `group_rights` WHERE `group_id` = ?", [$group_id]);
    }

    private function removeEmptyRights(&$group_rights)
    {
       foreach ($group_rights as $k => $v) {
           if ($v['add'] == 0 && $v['edit'] == 0 && $v['view'] == 0 && $v['delete'] == 0)
               unset($group_rights[$k]);
       }
    }

    private function isParentRightsOk($son_rights, $parent_id)
    {
        if ($parent_id == 0)
            return true;
        $parent = $this->getSpecificGroupRights($parent_id);
        if ($parent == null)
            return false;
        foreach ($parent as $feature){
            if (!isset($son_rights[$feature->feature_id]))
                continue;
            if ($feature->view < $son_rights[$feature->feature_id]['view']
                || $feature->add < $son_rights[$feature->feature_id]['add']
                || $feature->edit < $son_rights[$feature->feature_id]['edit']
                || $feature->delete < $son_rights[$feature->feature_id]['delete']
            )
                return false;
            unset($son_rights[$feature->feature_id]);
        }
        if ($son_rights != [])
            return false;
        return true;
    }

    private function updateAndAddRights($rights_list, $id, $editor, $method)
    {
        $methods = ['group', 'user'];

        if (!in_array($method, $methods))
            return null;

        try {
            $this->db->beginTransaction();
            foreach ($rights_list['update_list'] as $k => $v) {
                $this->db->query('UPDATE `' . $method . '_rights` SET `add` = ?, `view` = ?, `edit` = ?, `delete` = ?, `edited_by` = ?, `edited_at` = NOW() WHERE `feature_id` = ? AND `group_id` = ?',
                    [$v['add'], $v['view'], $v['edit'], $v['delete'], $editor, $k, $id]
                );
            }
            foreach ($rights_list['add_list'] as $k => $v) {
                $this->db->query('INSERT INTO `' . $method . '_rights` SET `add` = ?, `view` = ?, `edit` = ?, `delete` = ?, `group_id` = ?, `feature_id` = ?, `edited_by` = ?, `edited_at` = NOW()',
                    [$v['add'], $v['view'], $v['edit'], $v['delete'], $id, $k, $editor]
                );
            }
            $this->db->commit();
            return true;
        } catch (PDOException $e){
            $this->db->rollBack();
            App::logPDOException($e, 1);
            return null;
        }
    }

    private function splitFeatureList($features, $id, $method)
    {
        $updateList = array();

        if ($method == 'group')
            $rights = $this->getSpecificGroupRights($id);
        else if ($method == 'user')
            $rights = $this->getSpecificUserRights($id);
        if (!isset($rights) || $rights === false)
            return null;
        foreach ($rights as $k) {
            if (isset($features[$k->feature_id])) {
                $updateList[$k->feature_id] = $features[$k->feature_id];
                unset($features[$k->feature_id]);
            }
        }
        return ['update_list' => $updateList, 'add_list' => $features];
    }

    private function featureNameToFeatureId(&$data)
    {
        $new_data['rights'] = array();
        $features = $this->getFeatures();

        if ($features === false)
            return false;
        //Replace feature_name by feature_id
        foreach ($features as $feature) {
            if (isset($data['rights'][$feature->name])){
                $new_data['rights'][$feature->id] = $data['rights'][$feature->name];
                unset($data['rights'][$feature->name]);
            }
        }
        //if something leaves it means they are wrong data => return false
        if ($data['rights'] != null)
            return false;
        $data['rights'] = $new_data['rights'];
        return true;
    }

    private function checkFeaturesId($data)
    {
        $ret = $this->getFeatures();

        if ($ret === false)
            return false;
        //Replace feature_name by feature_id
        foreach ($ret as $k)
            if (isset($data['rights'][$k->id]))
                unset($data['rights'][$k->id]);
        //if something leaves it means they are wrong data => return false
        if ($data['rights'] != null)
            return false;
        return true;
    }

    public function addUserInGroup($data)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `group_id` = ? WHERE `user_token` = ?", [$data['group_id'], $data['user_token']]);
        if ($ret->rowCount() >= 0)
            return $ret->rowCount();
        return null;
    }

    public function removeUserFromGroup($data)
    {
        $ret = $this->db->query_log("UPDATE `user` SET `group_id` = NULL WHERE `user_token` = ?", [$data['user_token']]);
        if ($ret->rowCount() >= 0)
            return $ret->rowCount();
        return null;
    }

    public function groupsAsOption() {
        $output = "<option value='0'></option>";
        $groups = $this->getSortedGroup();

        if ($groups === false)
            return $output;
        else if ($groups == null)
            return $output;

        foreach ($groups as $k => $v) {
            $output .= "<option value='" . $v['id'] . "'>";
            $output .= $v['name'];
            $output .= "</option>";

            $output .= $this->printChildren($v, 1);
        }

        return $output;
    }

    private function printChildren($group, $cat) {
        $out = "";
        $i = -1;

        while (isset($group['children'][++$i])) {
            $out .= $this->printGroup($group['children'][$i], $cat);
            $out .= $this->printChildren($group['children'][$i], $cat + 1);
        }

        return $out;
    }

    private function printGroup($group, $cat) {
        $display = "<option value='" . $group['id'] . "'>";

        for ($j = 0; $j < $cat; $j++)
            $display .= "—";

        $display .= $group['name'] . "</option>";

        return $display;
    }

    public function getSortedGroup()
    {
        $groups = $this->getGroups();
        $output = array();
        $all = array();
        $dangling = array();

        if (empty($groups))
            return $groups;

        // Initialize arrays
        foreach ($groups as $group) {
            $group['children'] = array();
            $id = $group['id'];

            // If this is a top-level node, add it to the output immediately
            if ($group['parent_id'] == 0) {
                $all[$id] = $group;
                $output[] =& $all[$id];

                // If this isn't a top-level node, we have to process it later
            } else {
                $dangling[$id] = $group;
            }
        }
        // Process all 'dangling' nodes
        $added = 0;
        while (count($dangling) > 0 || $added == 1) {
            $added = 0;
            foreach($dangling as $group) {
                $id = $group['id'];
                $pid = $group['parent_id'];

                // If the parent has already been added to the output, it's
                // safe to add this node too
                if (isset($all[$pid])) {
                    $added = 1;
                    $all[$id] = $group;
                    $all[$pid]['children'][] =& $all[$id];
                    unset($dangling[$group['id']]);
                }
            }
        }
        if (count($dangling) != 0)
            return null;
        return $output;
    }

    public function addGroup($data)
    {
        if ($data['parent_id'] != 0) {
            $parent = $this->getGroup($data['parent_id']);
            if (!$parent)
                return null;
        }
        $adder = App::getAuth()->user()->id;
        if (!$adder)
            return null;
        $this->db->query_log("INSERT INTO `group_nobo`(`name`, `parent_id`, `description`, `created_by`, `created_at`) VALUES (?, ?, ?, ?, NOW())", [$data['name'], $data['parent_id'], $data['description'], $adder]);
        $group_id = $this->db->lastInsertId();

        if ($data['parent_id'] == 0) {
            $menus = App::getMenuManager()->getAllMenus();
            foreach ($menus as $k => $v) {
                $menuOptions = App::getMenuManager()->getOptionsByMenuId($v->id);
                foreach ($menuOptions as $k => $v) {
                    $this->addRightByGroupId($group_id, $v->id, 0);
                }
            }
        }
        if ($group_id)
            return $group_id;
        return null;
    }

    /**
     * @param $data array with parent_id and group_id
     * @return bool|null|int
     */
    public function updateGroup($data)
    {
        if ($data['parent_id'] != 0) {
            $parent = $this->getGroup($data['parent_id']);
            if (!$parent)
                return false;
        }
        $old_parent = $this->getGroup($data['group_id']);
        if (!$old_parent)
            return false;
        if ($old_parent->parent_id != $data['parent_id']) {
            return null;
            //TODO: Has to update the whole tree with the new parent rights to accept this condition (new parent_id)
        }
        else
            $ret = $this->db->query_log("UPDATE `group_nobo` SET `name` = ?, `description` = ? WHERE id = ?", [$data['name'], $data['description'], $data['group_id']]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function deleteGroup($data)
    {
        $groups = $this->getBranch($data['group_id']);
        if ($groups == null)
            return 'tti';
        $pid = $groups[0]['parent_id'];
        $old_pid = $data['group_id'];
        if ($data['delete_children'] == 1)
            $groups_to_delete = implode(',', $this->getChildren($groups[0]));
        else
            $groups_to_delete = $data['group_id'];
        if (empty($groups_to_delete))
            return null;
        try {
            $this->db->beginTransaction();
            //Delete elements
            $this->db->query("UPDATE `group_nobo` SET `deleted` = 1 WHERE `id` IN (" . $groups_to_delete . ") OR `id` = ?", [$data['group_id']]);
            //If we don't delete children bind them to the parent id of the deleted element
            if ($data['delete_children'] == 0)
                $this->db->query("UPDATE `group_nobo` SET `parent_id` = ? WHERE `parent_id` = ?", [$pid, $old_pid]);
            //Remove the group from the users
            $this->db->query("UPDATE `user` SET `group_id` = NULL WHERE `group_id` IN (" . $groups_to_delete . ") OR `group_id` = ?", [$data['group_id']]);
            $this->db->commit();
            return true;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            return null;
        }
    }

    public function getBranch($group_id)
    {
        $groups = $this->getGroups();
        $output = array();
        $all = array();
        $dangling = array();

        if (empty($groups))
            return null;

        foreach ($groups as $group) {
            $group['children'] = array();
            $id = $group['id'];

            if ($group['id'] == $group_id) {
                $all[$id] = $group;
                $output[] =& $all[$id];
            } else {
                $dangling[$id] = $group;
            }
        }

        $added = 1;

        while ($added == 1) {
            $added = 0;
            foreach($dangling as $group) {
                $id = $group['id'];
                $pid = $group['parent_id'];

                // If the parent has already been added to the output, it's
                // safe to add this node too
                if (isset($all[$pid])) {
                    $all[$id] = $group;
                    $all[$pid]['children'][] =& $all[$id];
                    unset($dangling[$group['id']]);
                    $added = 1;
                }
            }
        }
        return $output;
    }

    public function getParent($group_id) {
        $group_infos = $this->getGroup($group_id);
        if ($group_infos->parent_id != 0) {
            return $this->getGroup($group_infos->parent_id);
        }
        return false;
    }

    public function getChildrenByGroupId($group_id) {
        return $this->db->query_log("SELECT *
                                    FROM `group_nobo`
                                    WHERE parent_id = ?
                                    AND deleted = 0",
                                    [$group_id])->fetchAll();
    }

    public function getAllChildernByGroupId($group_id) {
        $childrens = $this->getChildrenByGroupId($group_id);

        foreach ($childrens as $k => $v) {
            $v->childrens = $this->getChildrenByGroupId($v->id);
            $childrens[$k] = $v;
        }

        return $childrens;
    }

    public function getChildren($group) {
        $out = [$group['id']];
        $i = -1;

        while (isset($group['children'][++$i]))
            $out = array_merge($out, $this->getChildren($group['children'][$i]));
        return $out;
    }

    public function usersWithoutGroups() {
        return $this->db->query_log("SELECT `firstname`, `lastname`, `user_token` FROM `user` WHERE `deleted` = 0 AND `group_id` IS NULL")->fetchAll();
    }

    public function usersWithGroup() {
        return $this->db->query_log("SELECT `firstname`, `lastname`, `user_token` FROM `user` WHERE `deleted` = 0 AND `group_id` IS NOT NULL")->fetchAll();
    }

    public function acUsersWithGroup() {
        $autocomplete = [];

        $ret = $this->db->query_log("SELECT U.`firstname`, U.`lastname`, U.`user_token`, (SELECT G.`name` FROM `group_nobo` G WHERE G.`id` = U.`group_id`) as group_name FROM `user` U WHERE U.`deleted` = 0 AND U.`group_id` IS NOT NULL")->fetchAll();
        if ($ret === false)
            return null;
        foreach ($ret as $key => $value) {
            array_push($autocomplete, [
                'value' => $value->firstname . ' ' . $value->lastname,
                'user_token' => $value->user_token,
                'group_name' => $value->group_name,
                'firstname' => $value->firstname,
                'lastname' => $value->lastname
            ]);
        }
        return $autocomplete;
    }

    /**
     * New groups rights
     */
    public function getRightsByGroupId($group_id, $menu_id) {
        $rights = [];
        $parent = $this->getParent($group_id);

        if ($parent) {
            $parentRights = $this->getRightsByGroupId($parent->id, $menu_id);
            foreach ($parentRights as $k => $v) {
                $v->is_parent = true;
                $rights[$v->option_id] = $v;
            }
        }

        $selfRights = $this->db->query_log("
                                    SELECT 
                                    m.option_name, 
                                    m.option_value,
                                    IFNULL((SELECT `activated` FROM `group_rights` WHERE `option_id` = m.`id` AND group_id = ?), 0) as activated,
                                    m.id as option_id
                                    FROM `menu_bo_options` m
                                    WHERE
                                    m.menu_bo_id = ?
                                    AND m.deleted = 0
                                    ",
                                    [$group_id, $menu_id])->fetchAll();

        foreach ($selfRights as $k => $v) {
            $v->is_parent = false;
            $rights[$v->option_id] = $v;
        }

        return $rights;
    }

    public function getRightsByUserToken($user_token, $menu_id) {

        $rights = [];
        $ManageRights = App::getManageRights();

        if ($ManageRights->hasGroup($user_token)) {
            $groupRights = $this->getRightsByGroupId($ManageRights->getGroupByUserToken($user_token)->group_id, $menu_id);

            foreach ($groupRights as $k => $v) {
                $v->is_group = true;
                $rights[$v->option_id] = $v;
            }
        }

        $userRights = $this->db->query_log("SELECT 
mbo.id as option_id, 
mbo.option_name, 
mbo.option_value, 
IFNULL(
  (SELECT ur.`activated` FROM `user_rights` ur WHERE ur.`option_id` = mbo.`id` AND ur.user_id = 
    (SELECT u.id FROM `user` u WHERE u.user_token = ?)
  ), 0) as activated
                                            FROM `menu_bo_options` mbo
                                            WHERE
                                            mbo.menu_bo_id = ?
                                            AND mbo.deleted = 0",
            [$user_token, $menu_id])->fetchAll();

        /*
        $userRights = $this->db->query_log("SELECT u.option_id, mo.option_name, mo.option_value, u.activated
                                            FROM `user_rights` u, `menu_bo_options` mo
                                            WHERE u.option_id = mo.id
                                            AND u.user_id = (SELECT `id` FROM `user` WHERE `user_token` = ?)
                                            AND mo.menu_bo_id = ?
                                            AND mo.deleted = 0",
                                            [$user_token, $menu_id])->fetchAll();
*/
        foreach ($userRights as $k => $v) {
            $v->is_group = false;
            $rights[$v->option_id] = $v;
        }

        return $rights;
    }

    public function addRightByGroupId($group_id, $option_id, $activated = 0) {
        $ret = $this->db->query_log("INSERT INTO `group_rights`
                                    (`group_id`, `option_id`, `activated`)
                                    VALUES (?, ?, ?)",
                                    [$group_id, $option_id, $activated]);

        if ($ret == false) {
            return 0;
        }
        return $this->db->lastinsertId();
    }

    public function addRightByUserToken($user_token, $option_id, $activated = 0) {
        $ret = $this->db->query_log("INSERT INTO `user_rights`
                                    (`user_id`, `option_id`, `activated`)
                                    VALUES ((SELECT `id` FROM `user` WHERE `user_token` = ?), ?, ?)",
                                    [$user_token, $option_id, $activated]);

        if ($ret == false) {
            return 0;
        }
        return $this->db->lastinsertId();
    }

    public function updateRightByGroupId($group_id, $option_id, $activated) {
        $testRight = $this->db->query_log("SELECT *
                                            FROM `group_rights`
                                            WHERE option_id = ?
                                            AND group_id = ?",
                                            [$option_id, $group_id])->fetchAll();

        if (count($testRight) == 0) {
            $this->addRightByGroupId($group_id, $option_id);
        }

        $ret = $this->db->query_log("UPDATE `group_rights` g
                                    SET g.activated = ?
                                    WHERE g.group_id = ?
                                    AND g.option_id = ?",
                                    [$activated ? 1 : 0, $group_id, $option_id]);

        if ($ret == false) {
            return 0;
        }

        if ($activated == 0) {
            $childrens = $this->getChildrenByGroupId($group_id);
            foreach ($childrens as $k => $v) {
                $this->deleteRightByGroupId($v->id, $option_id);
            }
        }

        return 1;
    }

    public function updateRightByUserToken($user_token, $option_id, $activated) {

        $testRight = $this->db->query_log("SELECT *
                                            FROM `user_rights`
                                            WHERE option_id = ?
                                            AND user_id = (SELECT `id` FROM `user` WHERE `user_token` = ?)",
                                            [$option_id, $user_token])->fetchAll();

        if (count($testRight) == 0) {
            $this->addRightByUserToken($user_token, $option_id);
        }

        $ret = $this->db->query_log("UPDATE `user_rights` u
                                    SET u.activated = ?
                                    WHERE u.user_id = (SELECT `id` FROM `user` WHERE `user_token` = ?)
                                    AND u.option_id = ?",
                                    [$activated ? 1 : 0, $user_token, $option_id]);

        if ($ret == false) {
            return 0;
        }
        return 1;
    }

    public function deleteRightByGroupId($group_id, $option_id) {
        $parent = $this->getParent($group_id);
        $childrens = $this->getChildrenByGroupId($group_id);

        foreach ($childrens as $k => $v) {
            $this->deleteRightByGroupId($v->id, $option_id);
        }

        if ($parent) {
            $ret = $this->db->query_log("DELETE FROM `group_rights`
                                        WHERE group_id = ?
                                        AND option_id = ?",
                                        [$group_id, $option_id]);

            if ($ret == false) {
                return 0;
            }
            return 1;
        } else {
            return App::getMenuManager()->deleteOption($option_id);
        }
    }

    public function deleteRightByUserToken($user_token, $option_id) {
        $ret = $this->db->query_log("DELETE FROM `user_rights`
                                    WHERE user_id = (SELECT `id` FROM `user` WHERE `user_token` = ?)
                                    AND option_id = ?",
                                    [$user_token, $option_id]);

        if ($ret == false) {
            return 0;
        }
        return 1;
    }
}
