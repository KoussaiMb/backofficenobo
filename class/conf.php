<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 11/08/2017
 * Time: 11:00
 */
class conf
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll() {
        try {
            $conf = $this->db->query("SELECT `name`, `value` FROM `conf`")->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return null;
        }
        $arr = [];
        foreach($conf as $k => $v){
            $arr += [$v['name'] => $v['value']];
        }
        return $arr;
    }

    public function getAllPublicHolidays() {
        try {
            return $this->db->query("SELECT * FROM `public_holiday` WHERE `date` >= NOW()")->fetchAll();
        } catch (PDOException $e) {
            return null;
        }
    }

    public function updateConf($data) {
        try {
            unset($data['action']);
            unset($data['setting']);

            $this->db->beginTransaction();
            foreach($data as $k => $v)
                $this->db->query('UPDATE `conf` SET `value` = ? WHERE `name` = ?', [$v, $k]);
            $this->db->commit();
            return 1;
        }catch(PDOException $e){
            return null;
        }
    }

    /**
     * @param $settings array name/value
     * @return int
     */
    public function editConf($settings) {
        try {
            $this->db->beginTransaction();
            foreach ($settings as $k) {
                $this->db->query("UPDATE `conf` SET `value` = ? WHERE `name` = ?", [$k['value'], $k['name']]);
            }
            $this->db->commit();
            return 1;
        } catch(PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 2);
            return 0;
        }
    }




    public function addPublicHoliday($data) {
        try {
            $this->db->query('INSERT INTO `public_holiday`(`date`, `canWork`, `addWagePercentage`, `addWageBonus`) VALUES(?, ?, ?, ?)', [
                $data['date'],
                $data['canWork'],
                $data['addWagePercentage'],
                $data['addWageBonus']
            ]);
            return $this->db->lastInsertId();
        }catch(PDOException $e){
            return null;
        }
    }

    public function removePublicHoliday($id) {
        try {
            $ret = $this->db->query("DELETE FROM `public_holiday` WHERE id = ?", [$id]);
            return $ret->rowCount();
        }catch(PDOException $e){
            return null;
        }
    }

    public function getAllConvention() {
        try {
            return $this->db->query("SELECT * from `convention` WHERE `deleted` = 0")->fetchAll();
        }catch(PDOException $e){
            return null;
        }
    }

    public function getConfByName($conf_name) {
            return $this->db->query_log("SELECT * from `conf` WHERE `name` = ?", [$conf_name])->fetch();
    }

    public function getConventionById($id) {
        try {
            return $this->db->query("SELECT * from `convention` WHERE `deleted` = 0 AND `id` = ?", [$id])->fetch();
        }catch(PDOException $e){
            return null;
        }
    }

    public function addConvention($data) {
        try {
            $ret = $this->db->query("INSERT INTO `convention`(
`name`,
`breakDuration`, 
`intervalBreak`,
`medecinCheck`,
`breakfastDuration`, 
`averageHoursWeek`,
`maxHoursWeek`,
`breakHeaven`,
`daysOffPerMonth`, 
`reference`
) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
    $data['name'],
    $data['breakDuration'],
    $data['intervalBreak'],
    $data['medecinCheck'],
    $data['breakfastDuration'],
    $data['averageHoursWeek'],
    $data['maxHoursWeek'],
    $data['breakHeaven'],
    $data['daysOffPerMonth'],
    $data['reference']
            ]);
        return $ret->rowCount();
        }catch(PDOException $e){
            return null;
        }
    }

    public function editConvention($data) {
        try {
            $ret = $this->db->query("UPDATE `convention` SET
            `name` = ?,
            `breakDuration` = ?, 
            `intervalBreak` = ?,
            `medecinCheck` = ?,
            `breakfastDuration` = ?, 
            `averageHoursWeek` = ?,
            `maxHoursWeek` = ?,
            `breakHeaven` = ?,
            `daysOffPerMonth` = ?, 
            `reference` = ?",
                [
                    $data['name'],
                    $data['breakDuration'],
                    $data['intervalBreak'],
                    $data['medecinCheck'],
                    $data['breakfastDuration'],
                    $data['averageHoursWeek'],
                    $data['maxHoursWeek'],
                    $data['breakHeaven'],
                    $data['daysOffPerMonth'],
                    $data['reference']
                ]);
            return $ret->rowCount();
        }catch(PDOException $e){
            return null;
        }
    }

    public function deleteConvention($id) {
        try {
            $ret = $this->db->query("UPDATE `convention` SET `deleted` = 1 WHERE `id` = ?", [$id]);
            return $ret->rowCount();
        }catch(PDOException $e){
            return null;
        }
    }
}