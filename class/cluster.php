<?php

class cluster
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    function getCities()
    {
        return $this->db->query_log("SELECT `field` as city, `id` FROM `list` WHERE id IN (SELECT DISTINCT `l_clusterLocation_id` FROM `cluster`)")->fetchAll();
    }

    function getActiveClusters()
    {
        return $this->db->query_log("
SELECT c.*, u.firstname, u.lastname, l.`field` as city, (SELECT COUNT(*) FROM `provider` p WHERE p.cluster_id = c.id) as provider_nb 
FROM `cluster` c, `user` u, `list` l
WHERE c.`deleted` = 0
AND c.`governess_id` = (SELECT `id` FROM `governess` where u.`id` = `user_id`)
AND l.`id` = c.`l_clusterLocation_id`
GROUP BY c.id
")->fetchAll();

    }

    function getClusterById($id)
    {
        return $this->db->query_log("
SELECT c.*, u.id as user_id, u.firstname, u.lastname, l.`field` as city, (SELECT COUNT(*) FROM `provider` p WHERE p.cluster_id = c.id) as provider_nb 
FROM `cluster` c, `user` u, `list` l
WHERE c.`deleted` = 0
AND u.`id` = (SELECT `user_id` FROM `governess` where `id` = c.`governess_id`)
AND l.`id` = c.`l_clusterLocation_id`
AND c.id = ?
GROUP BY c.id
", [$id])->fetch();
    }

    function getProviderCluster($id)
    {
        return $this->db->query_log("SELECT u.`id`, u.`user_token`, u.firstname, u.lastname FROM `provider` p, `user` u WHERE u.id = p.user_id AND p.cluster_id = ?", [$id])->fetchAll();
    }

    public function getAllGoverness()
    {
        return $this->db->query_log("SELECT * FROM `user` u WHERE u.`deleted` = 0 AND u.`who` = 'governess'")->fetchAll();
    }

    public function getGovernessByProviderId($user_id)
    {
        $ret = $this->db->query_log("
SELECT * FROM `governess` 
WHERE `user_id` = (
  SELECT `user_id` FROM `cluster` WHERE `id` = (
    SELECT `cluster_id` FROM `provider` WHERE `id` = ?
    ))", [$user_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function addCluster($data)
    {
        return $this->db->query_log("INSERT INTO `cluster` SET `name` = ?, `governess_id` = (SELECT `id` FROM `governess` WHERE `user_id` = ?), `l_clusterLocation_id` = ?, `description` = ?",
            [$data['name'], $data['user_id'], $data['l_clusterLocation_id'], $data['description']]);
    }

    public function editCluster($data)
    {
        return $this->db->query_log("UPDATE `cluster` SET `name` = ?, `governess_id` = (SELECT `id` FROM `governess` WHERE `user_id` = ?), `l_clusterLocation_id` = ?, `description` = ? WHERE `id` = ?",
            [$data['name'], $data['user_id'], $data['l_clusterLocation_id'], $data['description'], $data['cluster_id']]);
    }

    public function editProviderInCluster($data)
    {
        try {
            $this->db->beginTransaction();
            $this->db->query("UPDATE `provider` SET `cluster_id` = NULL WHERE `cluster_id` = ?", [$data['cluster_id']]);
            $this->db->query("UPDATE `provider` SET `cluster_id` = ? WHERE `user_id` IN (SELECT `id` FROM `user` WHERE `user_token` IN" . $data['provider_list'] . ")", [$data['cluster_id']]);
            $this->db->commit();
            return 1;
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            return 0;
        }
    }

    public function deleteCluster($data)
    {
        return $this->db->query_log("UPDATE `cluster` SET `deleted` = 1 WHERE `id` =  ?", [$data['cluster_id']]);
    }

    public function getAllGovernessAvailable()
    {
        return $this->db->query_log("
SELECT u.*, u.id as user_id FROM `user` u
WHERE u.`deleted` = 0 
AND u.`who` = 'governess' 
AND `id` NOT IN(
  SELECT G.`user_id` FROM `governess` G WHERE G.`id` IN (SELECT C.`governess_id` FROM `cluster` C WHERE C.`deleted` = 0)
)")->fetchAll();
    }

    public function providerNotInCluster() {
        $autocomplete = [];

        $ret = $this->db->query_log("
SELECT u.user_token, u.firstname, u.lastname 
FROM `user` u, `provider` p 
WHERE u.`deleted` = 0 AND u.`who` = 'provider' AND u.id = p.user_id AND (p.cluster_id IS NULL OR p.cluster_id = 0)")->fetchAll();
        if ($ret == false)
            return false;
        foreach ($ret as $key => $value) {
            array_push($autocomplete, [
                'value' => $value->firstname . ' ' . $value->lastname,
                'user_token' => $value->user_token,
                'name' => $value->firstname . ' ' . $value->lastname
            ]);
        }
        return json_encode($autocomplete);
    }
}