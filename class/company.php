<?php


class company
{
    public $db;

    function __construct($db){
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->query_log("SELECT * FROM `company` ORDER BY `corporate_name` ASC")->fetchAll();
    }

    function getCompanyById($id)
    {
        return $this->db->query_log("SELECT * FROM `company` WHERE id = ?", [$id])->fetch();
    }

    public function addCompany($company, $logo_url, $signature_url)
    {

        $this->db->query_log("INSERT INTO `company`(`corporate_name`, `business_name`, `siren`, `siret`, `address`, `zipcode`, `city`, `description`, `ceo_firstname`, `ceo_lastname`, `ceo_signature`, `logo`) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $company['corporate_name'],
                $company['business_name'],
                $company['siren'],
                $company['siret'],
                $company['address'],
                $company['zipcode'],
                $company['city'],
                $company['description'],
                $company['ceo_firstname'],
                $company['ceo_lastname'],
                $signature_url,
                $logo_url
            ]);

        return $this->db->lastInsertId();
    }


    public function editCompany($company, $logo_url, $signature_url)
    {

        $ret = $this->db->query_log("UPDATE `company` SET `corporate_name` = ?, `business_name` = ?, `siren` = ?, `siret` = ?, `address` = ?, `zipcode` = ?, `city` = ?, `description` = ?, `ceo_firstname` = ?, `ceo_lastname` = ?, `ceo_signature` = ?, `logo` = ? WHERE `id` = ?",
            [
                $company['corporate_name'],
                $company['business_name'],
                $company['siren'],
                $company['siret'],
                $company['address'],
                $company['zipcode'],
                $company['city'],
                $company['description'],
                $company['ceo_firstname'],
                $company['ceo_lastname'],
                $signature_url,
                $logo_url,
                $company['id']
            ])->rowCount();
        if($ret == false){
            return 0;
        }
        return 1;
    }

    public function deleteCompany($company)
    {
        $ret = $this->db->query_log("DELETE FROM company WHERE `id` = ?", [$company['id']]);
        if($ret == false){
            return 0;
        }
        return 1;
    }

    public function insertCompanyConf($conf)
    {
        $ret = $this->db->query_log("INSERT INTO `conf` (`name`, `value`) VALUES ('companySetting', ?)", [$conf]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function updateCompanyConf($id, $conf)
    {
        $ret = $this->db->query_log("UPDATE `conf` SET `value` = ?  WHERE id = ?", [$conf, $id]);
        if($ret == false)
            return false;
        return $ret->rowCount();
    }

    public function getCompanyConf()
    {
            return $this->db->query_log("SELECT * FROM `conf` WHERE `name` = 'companySetting'")->fetch();
    }

    public function deleteConf($conf)
    {
        $ret = $this->db->query_log("DELETE FROM conf WHERE `id` = ?", [$conf]);
        if($ret == false){
            return 0;
        }
        return 1;
    }
}