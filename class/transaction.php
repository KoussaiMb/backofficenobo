<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 17/01/2018
 * Time: 17:49
 */
class transaction
{
    /**
     * @var $db DB
     * @var $carts array of cart array [transaction_id, promocode_id, item_id, price, quantity,
     * discount_percentage, discount_euro, price_final, cart_count]
     * @var transaction array [user_id, company_id, title, reference, address, zipcode, city,
     * l_payment_mean_id, transacction_ext_ref, payment_account, price, canceled, payed, date_create]
     * @var item array [type]
     */

    private $db;
    private $carts = [];
    private $transaction;


    /*OLD SYSTEM*/
    private $item;
    private $trans_id;
    private $company_id;
    private $missionInfo;
    private $paymentInfo;
    private $required_transaction_field = ['user_id', 'payment_account', 'company_id'];
    private $required_cart_field = ['item_id', 'price', 'price_final'];

    public function __construct(DB $db) {
        $this->db = $db;
    }

    public function debug() {
        echo "carts";
        debug::data_r($this->carts);
        echo "transaction";
        debug::data_r($this->transaction);
    }

    /**
     * @param array $mission_ids
     * @return bool
     */
    public function createTransactionFromMission(array $mission_ids) {
        try {
            $this->constructCarts($mission_ids);
            $this->constructTransaction(reset($mission_ids));
            $this->carts_are_valid();
            $this->transaction_is_valid();
            $this->createWholeTransaction();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @throws Exception
     */
    public function carts_are_valid()
    {
        $cart_required_field = ['price', 'quantity', 'price_final', 'item_id', 'mission_id'];

        foreach ($this->carts as $k => $cart)
            foreach ($cart_required_field as $v)
               if (!isset($cart[$v]) || $cart[$v] == null)
                    throw new Exception('Un champe requis pour le cart est manquant');
    }

    /**
     * @throws Exception
     */
    public function transaction_is_valid()
    {
        $transaction_required_field = ['user_id', 'company_id', 'title', 'address', 'zipcode', 'city', 'price'];

        foreach ($transaction_required_field as $v)
            if (!isset($this->transaction[$v]) || $this->transaction[$v] == null)
                throw new Exception('Un champ requis pour la transaction est manquant');
    }

    /**
     * @throws Exception
     */
    private function getCompanyId() {
        $company = $this->db->query_log("
SELECT 
comp.`id`
FROM 
`conf` c
LEFT JOIN `company` comp ON comp.`id` = c.`value`
WHERE 
c.`name` = 'companySetting'
");
        if ($company === false)
            throw new Exception('Impossible de récupérer les paramètre de l\'entreprise facturante');
        $company_fetch = $company->fetch();
        if ($company_fetch === false || $company_fetch->id == null)
            throw new Exception('L\'entreprise facturante n\'existe plus');
        return $company_fetch->id;
    }

    /**
     * @throws Exception
     */
    private function isCompanyExist($company_id) {
        $companyExist = $this->db->query_log("SELECT 1 FROM `company` WHERE `id` = ?", [$company_id]);
        if ($companyExist === false || $companyExist->fetch() === false)
            throw new Exception('L\'entreprise facturante n\'existe plus');
    }

    /**
     * @param array $mission_ids
     * @return array
     * @throws Exception
     */
    private function getMissionsInfo(array $mission_ids) {
        $mission_count = count($mission_ids);
        $tokens = str_repeat('?,', $mission_count);
        $tokens_formated = substr($tokens, 0, $mission_count * 2 - 1);
        $missionsInfo = $this->db->query_log("
SELECT
m.work_time, m.id as mission_id,
p.percentage as discount_percentage, p.euro as discount_euro, 
pu.id as promocode_user_id, pu.promocode_id, 
cp.price as customer_price,
prov.contract as provider_contract
FROM `mission` m
INNER JOIN `customer` c ON c.`user_id` = m.`user_id`
INNER JOIN `customer_package` cp ON c.`customer_package_id` = cp.`id`
INNER JOIN `provider` prov ON prov.`id` = m.`provider_id`
LEFT JOIN `promocode_user` pu ON pu.`user_id` = m.`user_id`
LEFT JOIN `promocode` p ON p.`id` = pu.`promocode_id`
WHERE m.`id` IN (" . $tokens_formated . ") AND m.`cart_id` IS NULL AND m.`real_end` IS NOT NULL
AND (pu.`used` < pu.`occurrence` OR pu.`occurrence` IS NULL)
AND (p.`start` <= DATE(m.start) OR p.`start` IS NULL) 
AND (p.`end` >= DATE(m.end) OR p.`end` IS NULL) 
AND (p.`used` < p.`number` OR p.number IS NULL) 
AND (p.deleted = 0 OR p.deleted IS NULL)", $mission_ids);
        if ($missionsInfo === false)
            throw new Exception('Impossible de récupérer les informations des missions');
        $missions = $missionsInfo->fetchAll();
        return $missions;
    }

    /**
     * @param array $mission_ids
     * @throws Exception
     *
     * This function will find the package that the customer subscribed to, then find out the price and apply the
     * discounts.
     */

    private function constructCarts(array $mission_ids) {
        $missions = $this->getMissionsInfo($mission_ids);
        $cart_count = count($missions);
        $i = 0;

        foreach ($missions as $mission) {
            $discount = [];
            if ($mission->discount_percentage != null)
                $discount = ['percentage' => $mission->discount_percentage];
            if ($mission->discount_euro != null)
                $discount = ['euro' => $mission->discount_euro];
            $this->carts[$i]['promocode_id'] = $mission->promocode_id;
            $this->carts[$i]['cart_count'] = $cart_count;
            $this->carts[$i]['discount_percentage'] = $mission->discount_percentage;
            $this->carts[$i]['discount_euro'] = $mission->discount_euro;
            $this->carts[$i]['price'] = $mission->customer_price;
            $this->carts[$i]['quantity'] = $this->worktime_to_quantity($mission->work_time);
            $final_price = $mission->customer_price * $this->carts[$i]['quantity'];
            $this->carts[$i]['price_final'] = $this->applyDiscount($final_price, $discount);
            $item_id = $this->getItemIdFromProviderContract($mission->provider_contract, $mission->customer_price);
            $this->carts[$i]['item_id'] = $item_id === false ? null : $item_id;
            $this->carts[$i]['mission_id'] = $mission->mission_id;
        }
    }

    private function applyDiscount($price, $discount) {
        if (isset($discount['euro']))
            return max($price - $discount['euro'], 0);
        if (isset($discount['percentage']))
            return round($price * $discount['percentage'] / 100);
        return $price;
    }

    /**
     * @param $contract
     * @param $customer_price
     * @return bool|mixed|null
     *
     * we get an Item, but this wont be the reference to export an invoice
     */
    private function getItemIdFromProviderContract($contract, $customer_price) {
        $contract_type = [
            'self employed' => '10',
            'salaried' => '20',
            'extra' => '10'
        ];

        if (!isset($contract_type[$contract]))
            return null;
        $item = $this->db->query_log("SELECT * FROM `item` WHERE `vat` = ? AND `price` = ? AND `type` = 'mission'",
            [$contract_type[$contract], $customer_price]);
        if ($item === false)
            return false;
        $item_fetch = $item->fetch();
        if ($item_fetch === false)
            return false;
        return $item_fetch->id;
    }

    private function worktime_to_quantity($work_time) {
        if ($work_time == null)
            return null;
        $date = new DateTime($work_time);
        return $date->format('G') . '.' . round((int)$date->format('i') * 10 / 6);
    }

    /**
     * @param $mission_id
     * @throws Exception
     */
    private function constructTransaction($mission_id) {
        $company_id = $this->getCompanyId();
        $user_info = $this->getUserInfoFromMissionId($mission_id);

        $this->transaction['user_id'] = $user_info->user_id;
        $this->transaction['company_id'] = $company_id;
        $this->transaction['title'] = $user_info->title;
        $this->transaction['address'] = $user_info->address;
        $this->transaction['zipcode'] = $user_info->zipcode;
        $this->transaction['city'] = $user_info->city;
        $this->transaction['l_payment_mean_id'] = $user_info->l_payment_mean_id;
        $this->transaction['payment_account'] = $user_info->payment_account;
        $this->transaction['price'] = $this->getTotalPriceFromCarts();
    }

    /**
     * @return int
     */
    private function getTotalPriceFromCarts() {
        $final_price = 0;

        foreach ($this->carts as $cart)
            $final_price += $cart['price_final'];
        return $final_price;
    }

    /**
     * @param $mission_id
     * @return mixed
     * @throws Exception
     */
    private function getUserInfoFromMissionId($mission_id) {
        $mission = $this->db->query_log("
SELECT 
m.`user_id`,
CONCAT(u.`firstname`, ' ', u.lastname) as title,
u.l_payment_mean_id,
u.paymentAccount as payment_account,
a.address,
a.zipcode,
a.city
FROM 
`mission` m
INNER JOIN `user` u ON u.`id` = m.`user_id` 
INNER JOIN `address` a ON a.`id` = m.`address_id` 
WHERE
m.`id` = ?",
            [$mission_id]);
        if ($mission === false)
            throw new Exception('Could not get the mission and the payment information');
        $mission_fetch = $mission->fetch();
        if ($mission_fetch === false)
            throw new Exception('The mission does not exist');
        return $mission_fetch;
    }

    /**
     * @throws Exception
     */
    private function createWholeTransaction() {
        try {
            $this->db->beginTransaction();
            $this->insertTransaction();
            $transaction_id = $this->db->lastInsertId();
            foreach ($this->carts as $cart)
                $this->insertCarts($cart, $transaction_id);
            $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            throw new Exception('Impossible de créer une nouvelle transaction');
        }
    }

    private function insertTransaction() {
        $keys = implode(' = ?, ', array_keys($this->transaction));
        $values = array_values($this->transaction);

        $this->db->query("INSERT INTO `transaction` SET " . $keys . " = ?, `date_create` = NOW();".
            " @transaction_id = LAST_INSERT_ID()", $values);
    }

    private function insertCarts($cart, $transaction_id) {
        $mission_id = $cart['mission_id'];
        unset($cart['mission_id']);
        $keys = implode(' = ?, ', array_keys($cart));
        $values = array_values($cart);
        $values[] = $transaction_id;

        $this->db->query("INSERT INTO `cart` SET " . $keys . " = ?, transaction_id = ?", $values);
        $cart_id = $this->db->lastInsertId();
        $this->db->query("UPDATE `mission` SET `cart_id` = ? WHERE `id` = ?", [$cart_id, $mission_id]);
    }

    /*
     * invoice prototype -----------------------------------------------------------------------------------------------
     *
     private function getFinalPriceFromProviderContract($contract, $price, $quantity, $discount) {
        $contract_type = [
            'self employed' => 'priceFromSelfEmployed',
            'salaried' => 'priceFromSalaried',
            'extra' => 'priceFromExtra'
        ];

        if (!isset($contract_type[$contract]))
            return null;
        if ($price == null || $quantity == null || $quantity == 0)
            return null;
        $function = $contract_type[$contract];
        return $this->$function($price, $quantity, $discount);
    }
    private function priceFromSelfEmployed($price, $quantity, $discount) {
        $vat = 0.1;

        $price_wo_vat = $price - $price * $vat;
        $price_after_discount = $this->applyDiscount($price_wo_vat, $discount);
        return $price_after_discount * $quantity;
    }

    private function priceFromSalaried($price, $quantity, $discount) {
        $vat = 0.2;

        return $price * $quantity;
    }

    private function priceFromExtra($price, $quantity, $discount) {
        $vat = 0.1;

        return $price * $quantity;
    }
    *
    * invoice prototype -----------------------------------------------------------------------------------------------
    */

    /*OLD downside*/
    /**
     * @param array $cart_data [required: item_id, price, price_final]
     * @throws Exception
     */
    public function add_cart(array $cart_data) {
        foreach ($this->required_cart_field as $k)
            if (!isset($cart_data[$k]))
                throw new Exception('Il manque le paramètre ' . $k);
        $cart = $this->init_cart();
        foreach ($cart_data as $k => $v)
            $cart[$k] = $v;
        $this->carts[] = $cart;
    }

    private function init_cart() {
        return [
            'item_id' => NULL,
            'price' => NULL,
            'quantity' => 1,
            'promocode_id' => NULL,
            'discount_percentage' => NULL,
            'discount_euro' => NULL,
            'transaction_id' => $this->trans_id,
            'price_final' => NULL,
            'cart_count' => count($this->carts) + 1
        ];
    }

    /**
     * @param array $transaction_data [required: user_id, payment_account, company_id]
     * @throws Exception
     */
    public function set_transaction(array $transaction_data) {
        if (empty($this->carts))
            throw new Exception('Il n\'y a pas de panier dans la facture à générer');
        foreach ($this->required_transaction_field as $k)
            if (!isset($transaction_data[$k]))
                throw new Exception('Il manque le paramètre ' . $k);
        $this->init_transaction();
        foreach ($transaction_data as $k => $v)
            $this->transaction[$k] = $v;
    }

    public function insert_uniq_cart(){
        if (empty($this->transaction))
            throw new Exception('set_transaction() avant de generate_transaction()');
        $cart = $this->carts[0];
        $c = $this->prepare_sql_insert_data($cart);

        $ret = $this->db->query_log("INSERT INTO `cart`(" . $c['keys'] . ") VALUES(" . $c['to_complete'] . ")",
            $c['values']);
        if(!$ret){
            return 0;
        }
        return $this->db->lastInsertId();
    }

    public function update_promocode_cart($promocode_id, $cart_id)
    {
        $ret = $this->db->query_log("UPDATE `cart` SET `promocode_id` = ?, `discount_euro` = NULL , `discount_percentage` = NULL WHERE `id` = ?", [$promocode_id, $cart_id]);
        if($ret === false){
            return false;
        }
        $cart = $this->db->query_log("SELECT * FROM `cart` WHERE `id` = ?", [$cart_id])->fetch();
        if(!$cart){
            return false;
        }
        $promocode = App::getPromoCode()->getPromoCodeById($promocode_id);
        if(!$promocode){
            return false;
        }

        $cart_final_price = $cart->price_final;
        if($promocode->percentage){
            $new_price = $cart_final_price - ($cart_final_price*($promocode->percentage/100));
        }else if($promocode->euro){
            $new_price = $cart_final_price - $promocode->euro;
        }else{
            return false;
        }

        $ret = $this->db->query_log("UPDATE `cart` SET `price_final` = ? WHERE `id` = ?", [$new_price, $cart_id]);
        if($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function update_discount_cart($discount, $type, $cart_id)
    {
        $ret = false;
        if($type == 'euro') {
            $ret = $this->db->query_log("UPDATE `cart` SET `discount_euro` = ?,  `promocode_id` = NULL, `discount_percentage` = NULL, `price_final` = `price_final` - ? WHERE `id` = ?", [$discount, $discount, $cart_id]);
        }else if($type == 'percentage'){
            $ret = $this->db->query_log("UPDATE `cart` SET `discount_percentage` = ?, `promocode_id` = NULL, `discount_euro` = NULL, `price_final` = `price_final` - (`price_final`*(?/100)) WHERE `id` = ?", [$discount, $discount, $cart_id]);
        }
        if($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function remove_all_promo($cart_id)
    {
        $ret = $this->db->query_log("UPDATE `cart` SET `discount_euro` = NULL , `discount_percentage` = NULL, `promocode_id` = NULL , `price_final` = `quantity` * `price` WHERE `id` = ?", [$cart_id]);
        if($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function insert_uniq_transaction(){
        if (empty($this->transaction))
            throw new Exception('set_transaction() avant de generate_transaction()');
        $t = $this->prepare_sql_insert_data($this->transaction);
        $ret = $this->db->query("INSERT INTO `transaction`(" . $t['keys'] . ") VALUES(" . $t['to_complete'] . ")",
            $t['values']);
        if(!$ret){
            return false;
        }
        return $this->db->lastInsertId();
    }

    public function update_uniq_transaction(){
        if (empty($this->transaction))
            throw new Exception('set_transaction() avant de generate_transaction()');
        $t = $this->prepare_sql_update_data($this->transaction);
        $t['values'][] = $this->trans_id;

        $ret = $this->db->query_log("UPDATE `transaction` SET " . $t['query'] . " WHERE id=?", $t['values']);
        if(!$ret){
            return false;
        }
        return $ret->rowCount();
    }

    private function insert_all_carts()
    {
        if(!empty($this->carts)){
            foreach ($this->carts as $cart) {
                $c = $this->prepare_sql_insert_data($cart);
                $this->db->query_log("INSERT INTO `cart`(" . $c['keys'] . ") VALUES(" . $c['to_complete'] . ")",
                    $c['values']);
            }
        }
    }

    public function generate_transaction() {
        if (empty($this->transaction))
            throw new Exception('set_transaction() avant de generate_transaction()');
        try {
            $this->db->beginTransaction();
            foreach ($this->carts as $cart) {
                $c = $this->prepare_sql_insert_data($cart);
                $this->db->query("INSERT INTO `cart`(" . $c['keys'] . ") VALUES(" . $c['to_complete'] . ")",
                    $c['values']);
            }
            $t = $this->prepare_sql_insert_data($this->transaction);
            $this->db->query("INSERT INTO `transaction`(" . $t['keys'] . ") VALUES(" . $t['to_complete'] . ")",
                $t['values']);
            $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            throw new Exception('Impossible de créer la facture');
        }
    }

    private function init_transaction() {
        $this->transaction['date_create'] = date_create()->format('Y-m-d H:i:s');
    }

    private function prepare_sql_insert_data($cart) {
        $out['to_complete'] = substr(str_repeat('?,', count($cart)), 0, -1);
        $out['keys'] = implode(',', array_keys($cart));
        $out['values'] = array_values($cart);
        return $out;
    }

    private function prepare_sql_update_data($transaction) {
        $out['query'] = "";

        $tr_size = count($transaction);

        $req['keys'] = array_keys($transaction);
        $out['values'] = array_values($transaction);

        for ($i = 0; $i < $tr_size; $i++){
            $out['query'] .= $req['keys'][$i] . '=' . '?';
            if ($i < $tr_size - 1){
                $out['query'] .= ',';
            }
        }
        return $out;
    }


    public function get_transaction($trans_id = null) {
        if ($trans_id !== null) {
            $transaction = $this->db->query_log("SELECT * FROM `transaction` WHERE `id` = ?", [$trans_id])->fetch(PDO::FETCH_ASSOC);
            $carts = $this->db->query_log("SELECT * FROM `cart` WHERE `transaction_id` = ?", [$trans_id])->fetchAll(PDO::FETCH_ASSOC);
            $transaction['carts'] = $carts;
            return $transaction;
        }
        return ($this->transaction['carts'][$this->carts]);
    }

    public function is_payed_transaction($cart_id)
    {
        return $this->db->query_log(
            "SELECT payed FROM `transaction` t, `cart` c WHERE t.`id` = c.`transaction_id` AND c.`id` = ?",
            [$cart_id])->fetch(PDO::FETCH_ASSOC);
    }

    public function update_transaction_price_final()
    {
        $price_final = 0;
        foreach ($this->carts as $key => $cart){
            $price_final += $cart['price_final'];
        }
        $ret = $this->db->query_log("UPDATE `transaction` SET `price` = ? WHERE `id` = ?", [$price_final, $this->trans_id]);
        if($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function create_transaction_data($user_id)
    {
        $company = App::getConf()->getConfByName('companySetting');
        if(!$company){
            return 0;
        }
        $company_id = $company->value;

        $user = App::getUser()->getUserById($user_id);
        if(!$user){
            return 0;
        }

        $transaction_data = [
            'user_id' => $user_id,
            'payment_account' => $user->paymentAccount,
            'company_id' => $company_id
        ];
        return $transaction_data;
    }

    public function get_carts() {
        return $this->carts;
    }

    public function getAll()
    {
        return $this->db->query_log("SELECT * FROM `transaction`")->fetchAll();
    }

    public function getCartByMission($id_cart){

        $ret = $this->db->query_log("SELECT * FROM `cart` WHERE `id` = ?", [$id_cart]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getAllMissionTransaction(){

        return $this->db->query_log("SELECT * FROM `mission_transction_cart`")->fetchAll();

    }

    public function getMissionCart($id_mission){

        return $this->db->query_log("SELECT * FROM `mission_transction_cart`,`mission` WHERE mission_transction_cart.id = ?", [$id_mission])->fetch();
    }

    public function getMissionTranscByMissionID($id_mission) {

        return $this->db->query_log("SELECT * FROM `mission_transction_cart` WHERE `id` = ?", [$id_mission])->fetch();
    }
    /*OLD upside*/

    public function getPayementAccountByUserId($user_id) {

        $ret = $this->db->query_log("SELECT DISTINCT `paymentAccount` FROM `user` WHERE `id` = ?", [$user_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function addToTransaction($user_id, $payment_account, $price, $payed, $transaction_ext_ref)
    {
       $ret = $this->db->query_log("INSERT INTO `transaction`(`user_id`, `payment_account`, `price`, `payed`, `date_create`, `transaction_ext_ref`) VALUES (?,?,?,?,now(),?)",
            [
                $user_id,
                $payment_account,
                $price,
                $payed,
                $transaction_ext_ref,
            ]);

        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function getAllPayement() {

        $ret = $this->db->query_log("SELECT * from `transaction` t, `cart` c, `user` u where t.id = c.transaction_id and t.user_id = u.id AND u.who = \"customer\"");
        if ($ret === false)
            return false;
        return $ret->fetchAll();
    }
}