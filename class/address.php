<?php

/**
 * Created by PhpStorm.
 * User: damie
 * Date: 29/06/2017
 * Time: 14:59
 */
class address
{
    public $db;

    function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $ret = $this->db->query("SELECT * FROM `address` WHERE `deleted` = 0");
        return $ret ? $ret->fetchAll() : $ret;
    }

    public function getCustom($fields)
    {
        $selection = implode(', ', $fields);
        $ret = $this->db->query_log("SELECT $selection FROM `address` WHERE `deleted` = 0");
        return $ret ? $ret->fetchAll() : $ret;
    }

    public function updateAddressById($data)
    {
        $ret = $this->db->query_log("UPDATE `address` SET `lat` = ?, `lng` = ?, `address` = ?, `address_ext` = ?, `zipcode` = ?, `city` = ?, `country` = ?, `home` = ? WHERE `id` = ?",
            [
                empty($data['lat']) ? null : $data['lat'],
                empty($data['lng']) ? null : $data['lng'],
                empty($data['address']) ? null : $data['address'],
                empty($data['address_ext']) ? null : $data['address_ext'],
                empty($data['zipcode']) ? null : $data['zipcode'],
                empty($data['city']) ? null : $data['city'],
                empty($data['country']) ? null : $data['country'],
                empty($data['home']) ? 0 : $data['home'],
                $data['address_id']
            ]);
        return $ret ? $ret->rowCount() : $ret;
    }

    public function newCustomerAddress($data) {
        $update = [
            'lockbox_code' => empty($data['lockbox_code']) ? null : $data['lockbox_code'],
            'lockbox_localization' => empty($data['lockbox_localization']) ? null : $data['lockbox_localization'],
        ];

        $keys = implode(' = ?, ', array_keys($update));
        $values = array_values($update);

        $ret = $this->db->query_log("
INSERT INTO `customer_address` SET " .$keys. " = ?, `address_id` = 
(SELECT `id` FROM address WHERE `token` = '" .$data['address_token']. "'),
`customer_id` = (SELECT `id` FROM `customer` WHERE `user_id` = '" .$data['user_id'] . "')" , $values
        );
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function updateLockbox($data) {
        $update = [
            'lockbox_code' => empty($data['lockbox_code']) ? null : $data['lockbox_code'],
            'lockbox_localization' => empty($data['lockbox_localization']) ? null : $data['lockbox_localization'],
        ];

        $keys = implode(' = ?, ', array_keys($update));
        $values = array_values($update);

        $ret = $this->db->query_log("
UPDATE `customer_address` SET " .$keys. " = ? WHERE `address_id` = 
(SELECT `id` FROM address WHERE `token` = '" .$data['address_token']. "')", $values
        );
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function updateAddressFromCustomer($data)
    {
        $address = [
            'lat' => empty($data['lat']) ? null : $data['lat'],
            'lng' => empty($data['lng']) ? null : $data['lng'],
            'address' => empty($data['address']) ? null : $data['address'],
            'address_ext' => empty($data['address_ext']) ? null : $data['address_ext'],
            'zipcode' => empty($data['zipcode']) ? null : $data['zipcode'],
            'batiment' => empty($data['batiment']) ? null : $data['batiment'],
            'city' => empty($data['city']) ? null : $data['city'],
            'country' => empty($data['country']) ? null : $data['country'],
            'digicode' => empty($data['digicode']) ? null : $data['digicode'],
            'digicode_2' => empty($data['digicode_2']) ? null : $data['digicode_2'],
            'doorBell' => empty($data['doorBell']) ? null : $data['doorBell'],
            'door' => empty($data['door']) ? null : $data['door'],
            'floor' => empty($data['floor']) ? null : $data['floor'],
            'description' => empty($data['description']) ? null : $data['description'],
            'surface' => empty($data['surface']) ? null : $data['surface'],
            'roomNb' => empty($data['roomNb']) ? null : $data['roomNb'],
            'waterNb' => empty($data['waterNb']) ? null : $data['waterNb'],
            'childNb' => empty($data['childNb']) ? null : $data['childNb'],
            'peopleHome' => empty($data['peopleHome']) ? null : $data['peopleHome'],
            'l_pet_id' => $data['l_pet_id'],
            'have_ironing' => $data['have_ironing'],
            'have_key' => $data['have_key'],
            'home' => $data['home'],
            'subway_id' => empty($data['subway_id']) ? null :  $data['subway_id']
        ];

        $keys = implode(' = ?, ', array_keys($address));
        $values = array_values($address);

        $ret = $this->db->query_log("UPDATE `address` SET " .$keys. " = ? WHERE `token` = '" .$data['address_token']. "'", $values);

        return $ret ? $ret->rowCount() : $ret;
    }

    public function getUserAddress($user_id)
    {
        $ret = $this->db->query_log("SELECT A.*, A.`token` as address_token, M.`arret` as subway_station, M.`ligne` as subway_line
                                     FROM `address` A
                                     LEFT JOIN `metro_paris` M ON (M.`id` = A.`subway_id`)
                                     WHERE `deleted` = 0 AND `user_id` = ?", [$user_id]);
        return $ret ? $ret->fetchAll() : $ret;
    }

    public function getCustomerAddressByAddressId($address_id) {
        $ret = $this->db->query_log("SELECT * FROM `customer_address` WHERE `address_id` = ?", [$address_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getCustomerAddressByAddressToken($address_token) {
        $ret = $this->db->query_log("
SELECT * FROM `customer_address` WHERE `address_id` = (SELECT `id` FROM `address` WHERE `token` = ?)", [$address_token]);
        if ($ret === false)
            return false;
        $cusAdd = $ret->fetch();
        if ($cusAdd === false)
            return null;
        return $cusAdd;
    }

    public function getAutocompAddresses()
    {
        $ret = $this->db->query_log("SELECT U.`user_token`, A.`address`, A.`token` as address_token FROM `address` A INNER JOIN `user` U ON U.`id` = A.`user_id` WHERE U.`deleted` = 0 AND A.`deleted` = 0");
        return $ret ? $ret->fetchAll() : $ret;
    }

    public function getAddressByToken($address_token)
    {
        $ret = $this->db->query_log("
SELECT 
A.*, A.`token` as address_token, 
AC.`cluster_id`, AC.`customer_id`, AC.`id` as address_cluster_id,
cusA.`trash_localization`, cusA.`id` as customer_address_id
FROM `address` A
LEFT JOIN `address_cluster` AC ON A.`id` = AC.`address_id`  
LEFT JOIN `customer_address` cusA ON A.`id` = cusA.`address_id`  
WHERE A.`deleted` = 0 AND A.`token` = ?", [$address_token]);
        return $ret ? $ret->fetch() : $ret;
    }

    public function getAddressById($address_id)
    {
        $ret = $this->db->query_log("SELECT 
a.*, a.token as `address_token`, 
(SELECT l.`field` FROM `list` l WHERE l.`id` = a.l_pet_id) as pet, 
(SELECT l.`field` FROM `list` l WHERE l.`id` = a.recurrence_id) as rec 
FROM `address` a WHERE a.`deleted` = 0 AND a.`id` = ?", [$address_id]);
        return $ret ? $ret->fetch() : $ret;
    }

    public function addAddress($data)
    {
        $address_token = my_crypt::genStandardToken();
        $this->db->query_log("INSERT INTO `address` SET `token` = ?, `lat` = ?, `lng` = ?, `address` = ?, `address_ext` = ?, `zipcode` = ?, `city` = ?, `country` = ?, `home` = ?, `user_id` = ?",
            [
                $address_token,
                empty($data['lat']) ? null :  $data['lat'],
                empty($data['lng']) ? null :  $data['lng'],
                empty($data['address']) ? null :  $data['address'],
                empty($data['address_ext']) ? null :  $data['address_ext'],
                empty($data['zipcode']) ? null :  $data['zipcode'],
                empty($data['city']) ? null :  $data['city'],
                empty($data['country']) ? null :  $data['country'],
                $data['home'],
                $data['user_id']
            ]);
        return $this->db->lastInsertId() > 0 ? $address_token : 0;
    }

    public function deleteAddressByToken($address_token)
    {
        $address = $this->getAddressByToken($address_token);
        if (empty($address))
            return 0;
        return $this->deleteAddressById($address->id);
    }

    public function deleteAddressById($address_id)
    {
        try {
            $this->db->beginTransaction();
            $this->db->query_log("UPDATE `address` SET `deleted` = 1 WHERE `id` = ?", [$address_id]);
            $this->db->query_log("UPDATE `agenda` SET `deleted` = 1 WHERE `address_id` = ?", [$address_id]);
            $this->db->query_log("UPDATE `waiting_list` SET `deleted` = 1 WHERE `address_id` = ?", [$address_id]);
            $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollBack();
            App::logPDOException($e, 1);
            return 0;
        }
        return 1;
    }

    public function setAllAddressWithoutHome($user_id)
    {
        $ret = $this->db->query_log("UPDATE `address` SET `home` = 0 WHERE `user_id` = ?", [$user_id]);
        return $ret === false ? $ret->rowCount() : $ret;
    }

    public function addressToString($address_id)
    {
        $address_info = $this->getAddressById($address_id);

        return $address_info ? $address_info->address . ', ' . $address_info->city . ', ' . $address_info->zipcode . ', ' . $address_info->country : "Adresse non trouvé";
    }

    public function getAddressByUserId($user_id)
    {
        $ret = $this->db->query_log("SELECT *, token as address_token FROM `address` WHERE user_id = ?", [$user_id]);
        if($ret === false)
            return false;
        return $ret->fetch();
    }

    public function getAddressClusterByAddressId($address_id) {
        $ret = $this->db->query_log("SELECT * FROM address_cluster WHERE address_id = ?", [$address_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function updateAddressCluster($cluster_id, $address_token) {
        $address = $this->getAddressByToken($address_token);

        if ($address === false)
            return false;
        if ($cluster_id == 0)
            $cluster_id = null;
        if ($cluster_id === null && empty($address->address_cluster_id))
            return 1;
        if (!empty($addressCluster))
            $ret = $this->updateAddressClusterByAddressId($cluster_id, $address->address_cluster_id);
        else
            $ret = $this->insertAddressClusterByAddressId($cluster_id, $address->id, $address->user_id);
        return $ret;
    }

    private function updateAddressClusterByAddressId($cluster_id, $address_cluster_id) {
        $ret = $this->db->query_log("UPDATE `address_cluster` AC SET AC.`cluster_id` = ? WHERE AC.`id` = ?", [$cluster_id, $address_cluster_id]);
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    private function insertAddressClusterByAddressId($cluster_id, $address_id, $user_id) {
        $ret = $this->db->query_log("
INSERT INTO `address_cluster`
SET `cluster_id` = ?,
`address_id` = ?,
`customer_id` = (SELECT C.`id` FROM `customer` C WHERE C.`user_id` = ?)
", [$cluster_id, $address_id, $user_id]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    private function deleteAddressClusterByAddressId($address_id) {
        $ret = $this->db->query_log("DELETE FROM `address_cluster` WHERE `address_id` = ?", [$address_id]);
        if ($ret === false)
            return false;
        return 1;
    }


    public function getUserByAddressToken($address_token)
    {
        $ret = $this->db->query_log("SELECT u.*
                                         FROM `user` u  
                                         INNER JOIN `address` a ON u.id = a.user_id 
                                         WHERE a.token = ? ", [$address_token]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }

    public function editTrashLocalization($data) {
        $ret = $this->db->query_log("UPDATE `customer_address` SET `trash_localization` = ? WHERE `id` = ?",
            [
                empty($data['trash_localization']) ? null : $data['trash_localization'],
                $data['customer_address_id']
            ]
        );
        if ($ret === false)
            return false;
        return $ret->rowCount();
    }

    public function insertEmptyCustomerAddress($address_id, $customer_id) {
        $ret = $this->db->query_log("
INSERT INTO `customer_address` SET `address_id` = ?, `customer_id` = ?",
            [$address_id, $customer_id]);
        if ($ret === false)
            return false;
        return $this->db->lastInsertId();
    }

    public function getCustomerByAddressId($address_id) {
        $ret = $this->db->query_log("
        SELECT c.*, c.id as customer_id 
        FROM `customer` c
        WHERE c.user_id = (SELECT user_id FROM address WHERE id = ?)", [$address_id]);
        if ($ret === false)
            return false;
        return $ret->fetch();
    }
}