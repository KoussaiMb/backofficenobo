<?php


class item
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getItems()
    {
        return $this->db->query_log("SELECT * FROM `item` WHERE `deleted` = 0 ORDER BY `name`")->fetchAll();
    }

    public function getItemById($item_id)
    {
        return $this->db->query_log("SELECT * FROM `item` WHERE `id` = ? ", [$item_id])->fetch();
    }

    public function getCategories()
    {
        return $this->db->query_log("SELECT * FROM `item_category` WHERE `deleted` = 0 ORDER BY `name`")->fetchAll();
    }

    public function getCategoryById($category_id)
    {
        return $this->db->query_log("SELECT * FROM `item_category` WHERE `deleted` = 0 AND id = ?",[$category_id])->fetch();
    }

    public function insertItem($data)
    {
        $ret = $this->db->query_log('
                    INSERT INTO `item` 
                    SET `name` = ?, `description` = ?, `item_category_id` = ?, `stock` = ?, `stock_alert` = ?, `price` = ?, `vat` = ?, `reference_name` = ?',
            [
                $data['name'],
                $data['description'],
                $data['itemCategories'],
                $data['stock'],
                $data['stock_alert'],
                $data['price'],
                $data['vat'],
                $data['ref'],
            ]);
        if ($ret === false){
            return false;
        }
        return $this->db->lastInsertId();
    }

    public function editItem($data)
    {
        $ret = $this->db->query_log('
                    UPDATE `item` 
                    SET `name` = ?, `description` = ?, `item_category_id` = ?, `stock` = ?, `stock_alert` = ?, `price` = ?, `vat` = ?, `reference_name` = ? WHERE id = ?',
            [
                $data['name'],
                $data['description'],
                $data['itemCategories'],
                empty($data['stock']) ? null : $data['stock'],
                $data['stock_alert'],
                $data['price'],
                $data['vat'],
                $data['ref'],
                $data['item_id']
            ]);
        if ($ret === false)
            return $ret;
        return $ret->rowCount();
    }

    public function deleteItem($item_id)
    {
        $ret = $this->db->query_log("UPDATE `item` SET `deleted` = 1 WHERE `id` = ?", [$item_id]);
        if ($ret === false){
            return false;
        }
        return $ret->rowCount();
    }

    public function insertCategory($name, $parent_id)
    {
        $ret = $this->db->query_log('INSERT INTO `item_category` SET `name` = ?, `parent_category_id` = ?',[$name, $parent_id]);
        if ($ret === false){
            return false;
        }
        return $this->db->lastInsertId();
    }
}