<?php
require_once ('inc/bootstrap_public.php');
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8" lang="fr">
        <title>Nobo - Bienvenue</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/nobo_bo.css">
    </head>
    <body class="login-body">
    <div class="container login-container">
        <div class="login-wrapper">
            <div class="login-head">
                <img src="img/nobo/logo/nobo-logo-paris-noir.png" alt="logo-nobo-menage-paris">
                <h3>Quelle joie de vous retrouver !</h3>
            </div>
            <hr>
            <div class="login-content">
                <form method="post" action="/form/login">
                    <?php include ("inc/print_flash_helper.php"); ?>
                    <div class="form-group">
                        <label for="email">Entrez votre email:</label>
                        <input type="email" class="form-control" name="email" placeholder="alice@merveille.com" id="email" required/>
                    </div>
                    <div class="form-group">
                        <label for="password">Entrez votre mot de passe:</label>
                        <input type="password" class="form-control" name="password" placeholder="******" id="password" required/>
                    </div>
                    <button type="submit" class="btn btn-primary col-sm-12">Consultez votre compte Nobo</button>
                </form>
            </div>
            <hr>
            <hr>
            <hr>
            <div class="login-foot">
                <p><a href="forget">Vous avez oublié votre mot de passe ?</a></p>
                <p>Vous n'avez pas de compte ? <a href="<?= '/reservation/besoin'; ?>">Faites votre première prestation !</a></p>
            </div>
        </div>
    </div>

    <!-- Jquery -->
    <script src="js/lib/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- js -->
    <script>
        $("input:visible:first").focus();
    </script>
    </body>
</html>
