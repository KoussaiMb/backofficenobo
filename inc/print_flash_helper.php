<?php if(Session::getInstance()->hasFlash()): ?>
    <?php foreach(Session::getInstance()->getFlash() as $type => $message): ?>
        <div class="bg-<?= $type; ?> bg-flash">
            <?= htmlspecialchars($message);?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>