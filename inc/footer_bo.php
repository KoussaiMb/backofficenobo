</div>
<script async defer src="https://apis.google.com/js/api.js"
        onload="this.onload=function(){};handleClientLoad()"
        onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>
<footer class="pull-left footer" style="text-align: center">
    <p class="col-md-12">
    <hr class="divider">
    Copyright &COPY; <?php $date = new DateTime(); echo $date->format('Y')?> <a href="/">Nobo</a>
    </p>
</footer>
</div>
</body>
</html>