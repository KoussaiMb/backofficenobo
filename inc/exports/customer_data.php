<?php
spl_autoload_register('app_autoload');

function app_autoload($class){
    $i = mb_substr_count($_SERVER['PHP_SELF'], '/') - 1;
    $str = "";

    while ($i-- > 0)
        $str .= '../';

    require $str . "class/$class.php";
}
//Redirect 404 on .php
$url = $_SERVER['REQUEST_URI'];
if (strlen($url) > 4 && preg_match('/.php$/', $url))
    App::redirect('/error/404');
//Resctrict from rights
if (App::getAuth()->who() != 'admin' && !empty($menu_rights['v1/promocode/view']))
    App::redirect('/error/403');

$db = App::getDB();

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=customer_data.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
//Get variables
$start = isset($_GET['start']) ? " DATE(u.subscribed_at) >= '" . $_GET['start'] . "' " : "";
$end = isset($_GET['end']) ? " DATE(u.subscribed_at) <= '" . $_GET['end'] . "' " : "";
$conditions = "";
if ($start !== "" && $end !== "")
    $conditions .= " AND" . $start . "AND" . $end;
else if ($start !== "" || $end !== "")
    $conditions .= " AND" . $start . $end;
// output the column headings
$data_needed = array(
    'TOKEN' => 'u.user_token',
    'DATE DE PREMIER CONTACT' => 'u.subscribed_at',
    'DATE DE PREMIERE PRESTA DEMANDEE' => 'DATE(sub.date)',
    'TEMPS ENTRE 1er CONTACT ET 1ère PRESTA' => 'IF(sub.date, DATEDIFF(DATE(sub.date), DATE(u.subscribed_at)), \'Non renseigné\')',
    'HEURE DE LA PRESTA' => 'TIME(sub.date)',
    '# HEURES PROPOSEES FORMULAIRE' => 'sub.workTime',
    '# HEURES CHOISIES FORMULAIRE' => 'sub.workTimeChosen',
    '# HEURES AVEC CALL' => 'a.nbHours',
    '# RECURRENCE' => 'IF(a.recurrence_id IS NOT NULL, (SELECT l.field FROM list l WHERE a.recurrence_id = l.id), \'Non renseigné\')',
    'CRENEAU' => 'IF(TIME(sub.date)>\'12:00:00\', \'matin\', \'après-midi\')',
    'JOUR' => 'DAY(sub.date)',
    'MOIS' => 'MONTH(sub.date)',
    'ANNEE'=> 'YEAR(sub.date)',
    'SOURCE ACQUISITION' => 'IF(c.l_acquisition_id IS NOT NULL, (SELECT l.field FROM list l WHERE c.l_acquisition_id = l.id), \'Non renseigné\')',
    'Genre' => 'IF(u.gender_id IS NOT NULL, (SELECT l.field FROM list l WHERE u.gender_id = l.id), \'Non renseigné\')',
    'Etat civil' => 'IF(u.maritalStatus_id IS NOT NULL, (SELECT l.field FROM list l WHERE u.maritalStatus_id = l.id), \'Non renseigné\')',
    'NOM' => 'u.lastname',
    'PRENOM' => 'u.firstname',
    'EMAIL' => 'u.email',
    'TEL' => 'u.phone',
    'ADRESSE' => 'a.address',
    'CP' => 'a.zipcode',
    'VILLE' => 'a.city',
    'CLÉS' => 'IF(a.have_key, SUBSTR(a.token, 0, 5), 0)',
    'CODE' => 'CONCAT(COALESCE(a.digicode, \'Aucun\'), COALESCE(a.digicode_2, \'\'))',
    'INTERPHONE' => 'a.doorBell',
    'BAT' => 'a.batiment',
    'PORTE' => 'a.door',
    'ETAGE' => 'a.floor',
    'M2' => 'a.surface',
    'CHAMBRES' => 'a.roomNb',
    'SDB' => 'a.waterNb',
    'WC' => 'a.wcNb',
    '# PERSONNES DANS LE DOMICILE' => 'a.peopleHome',
    '# ADULTES' => '(a.peopleHome - a.childNb) as adultNb',
    '# ENFANTS' => 'a.childNb',
    'ANIMAUX' => 'IF(a.l_pet_id IS NOT NULL, (SELECT l.field FROM list l WHERE a.l_pet_id = l.id), \'Non renseigné\')',
    'REPASSAGE' => 'IF(a.have_ironing, \'OUI\', \'NON\')',
    'COMMENTAIRES / DETAILS PRESTA' => 'a.description',
    'OUI/NON' => 'IF((SELECT 1 FROM event_user_client euc, event e WHERE u.id = euc.user_id AND e.id = euc.event_id AND e.deleted = 0 AND e.l_category_event_id = \'83\' LIMIT 1), \'OUI\', \'NON\')'
);
/*
 *
    'DATE' => '',
    'Temps entre 1er contact et 1er RDV Client' => '',
    'NOM du GR' => '',
    'PRENOM du GR' => ''
 */
fputcsv($output, array_keys($data_needed));
$keys = implode(', ', array_values($data_needed));
// fetch the data
$rows = $db->query_log("
SELECT ". $keys ." FROM 
user u
LEFT JOIN address a ON a.user_id = u.id
LEFT JOIN customer c ON c.user_id = u.id
LEFT JOIN subscription sub ON sub.user_id = u.id
WHERE u.who = 'customer'
" . $conditions, false, 1);

// loop over the rows, outputting them
while ($row = $rows->fetch(PDO::FETCH_ASSOC))
    fputcsv($output, $row);