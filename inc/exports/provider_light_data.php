<?php
spl_autoload_register('app_autoload');

function app_autoload($class){
    $i = mb_substr_count($_SERVER['PHP_SELF'], '/') - 1;
    $str = "";

    while ($i-- > 0)
        $str .= '../';

    require $str . "class/$class.php";
}
//Redirect 404 on .php
$url = $_SERVER['REQUEST_URI'];
if (strlen($url) > 4 && preg_match('/.php$/', $url))
    App::redirect('/error/404');
//Resctrict from rights
if (App::getAuth()->who() != 'admin' && !empty($menu_rights['v1/promocode/view']))
    App::redirect('/error/403');

$db = App::getDB();

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=provider_light_data.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
//Get variables
$start = isset($_GET['start']) ? " DATE(u.subscribed_at) >= '" . $_GET['start'] . "' " : "";
$end = isset($_GET['end']) ? " DATE(u.subscribed_at) <= '" . $_GET['end'] . "' " : "";
$conditions = "";
if ($start !== "" && $end !== "")
    $conditions .= " AND" . $start . "AND" . $end;
else if ($start !== "" || $end !== "")
    $conditions .= " AND" . $start . $end;

// output the column headings
$data_needed = array(
    "TOKEN PRESTA" => "p.provider_token",
    "MAIL PRESTA" => "u.email",
    "NOM PRESTA" => "CONCAT(u.firstname, ' ', u.lastname)"
    );

fputcsv($output, array_keys($data_needed));
$keys = implode(', ', array_values($data_needed));

// fetch the data
$rows = $db->query_log("
SELECT ". $keys ." FROM
user u
INNER JOIN provider p ON p.user_id = u.id
" . $conditions, false, 1);

// loop over the rows, outputting them
while ($row = $rows->fetch(PDO::FETCH_ASSOC))
    fputcsv($output, $row);