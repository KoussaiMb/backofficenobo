<?php
spl_autoload_register('app_autoload');

function app_autoload($class){
    $i = mb_substr_count($_SERVER['PHP_SELF'], '/') - 1;
    $str = "";

    while ($i-- > 0)
        $str .= '../';

    require $str . "class/$class.php";
}
//Redirect 404 on .php
$url = $_SERVER['REQUEST_URI'];
if (strlen($url) > 4 && preg_match('/.php$/', $url))
    App::redirect('/error/404');
//Resctrict from rights
if (App::getAuth()->who() != 'admin' && !empty($menu_rights['v1/promocode/view']))
    App::redirect('/error/403');

$db = App::getDB();

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=mission_data.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));
//Get variables
$start = isset($_GET['start']) ? " DATE(m.start) >= '" . $_GET['start'] . "' " : "";
$end = isset($_GET['end']) ? " DATE(m.end) <= '" . $_GET['end'] . "' " : "";
$conditions = "";
if ($start !== "" && $end !== "")
    $conditions .= "WHERE" . $start . "AND" . $end;
else if ($start !== "" || $end !== "")
    $conditions .= "WHERE" . $start . $end;

// output the column headings
$data_needed = array(
    "DATE" => "DATE_FORMAT(m.start, '%d/%m/%Y')",
    "HEURE" => "DATE_FORMAT(m.start, '%Hh%i')",
    "DUREE" => "cast(time_to_sec(m.work_time) / (60 * 60) as decimal(10, 1))",
    "TYPE PRESTA" => "l.field",
    "TOKEN INTERVENANT" => "p.provider_token",
    "TOKEN CLIENT" => "c.customer_token",
    "INTERVENANT" => "CONCAT(pu.firstname, ' ', pu.lastname)",
    "CLIENT" => "CONCAT(cu.firstname, ' ', cu.lastname)",
    "MENAGE" => "cast(time_to_sec(m.work_time) / (60 * 60) as decimal(10, 1))",
    "REPASSAGE" => "0",
    "AUTRE" => "0",
    "NOTE /5" => "m.rate",
    "PAYEE MAJORDOME" => "0"
    );

fputcsv($output, array_keys($data_needed));
$keys = implode(', ', array_values($data_needed));

// fetch the data
$rows = $db->query_log("
SELECT ". $keys ." FROM
mission m
INNER JOIN provider p ON p.id = m.provider_id
INNER JOIN user pu ON pu.id = p.user_id
INNER JOIN user cu ON cu.id = m.user_id
INNER JOIN customer c ON cu.id = c.user_id
INNER JOIN address a ON a.id = m.address_id
LEFT JOIN list l ON l.id = a.recurrence_id
" . $conditions, false, 1);

// loop over the rows, outputting them
while ($row = $rows->fetch(PDO::FETCH_ASSOC))
    fputcsv($output, $row);