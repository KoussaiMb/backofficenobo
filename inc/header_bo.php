<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>Nobo - Backoffice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Nobo CSS -->
    <link type='text/css' rel='stylesheet' href="/css/nobo_bo.css">
    <!-- Bootstrap CSS -->
    <link type='text/css' rel='stylesheet' href="/css/bootstrap.min.css">
    <link type='text/css' rel='stylesheet' href="/css/bootstrap-theme.min.css">
    <!-- Jquery -->
    <script src="/js/lib/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- autocomplete -->
    <script type="text/javascript" charset="utf8" src="/js/lib/autocomplete.min.js"></script>
    <!-- Nobo functions -->
    <script type="text/javascript" charset="utf8" src="/js/nobo_bo.js"></script>
    <script type="text/javascript" charset="utf8" src="/js/app.js"></script>
    <!-- boostrap toggle -->
    <link type='text/css' rel='stylesheet' href="/css/lib/bootstrap-switch.min.css">
    <script type="text/javascript" src="/js/lib/bootstrap-switch.min.js"></script>
    <!-- Datepicker-->
    <link type='text/css' rel='stylesheet' href="/css/lib/bootstrap-datepicker.min.css">
    <script type="text/javascript" src="/js/lib/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/locale/bootstrap-datepicker.fr.min.js"></script>
    <!-- timepicker -->
    <link type='text/css' rel='stylesheet' href="/css/lib/timepicker.css">
    <script type="text/javascript" src="/js/lib/timepicker.min.js"></script>
    <!-- fullcalendar -->
    <link type='text/css' rel='stylesheet' href='/css/lib/fullcalendar.min.css'/>
    <link type='text/css' rel='stylesheet' href='/css/lib/fullcalendar.print.min.css' media='print'/>
    <link type='text/css' rel='stylesheet' href='/css/nobo_fullcalendar.css'/>
    <!--<link type='text/css' rel='stylesheet' href='/css/scheduler_file/scheduler.min.css'/>-->
    <script type="text/javascript" src='/js/lib/moment.min.js'></script>
    <script type="text/javascript" src='/js/lib/fullcalendar.min.js'></script>
    <!--<script type="text/javascript" src='/js/scheduler_file/scheduler.min.js'></script>-->
    <script type="text/javascript" src='/js/locale/allFullCalendar.js'></script>
    <!-- magnific popup -->
    <link type='text/css' rel='stylesheet' href="/css/lib/magnific-popup.css">
    <script type="text/javascript" src="/js/lib/magnific-popup.min.js"></script>
    <script type="text/javascript" src="/js/lib/fr.js"></script>
    <!-- animation for bootstrap-notify -->
    <script type="text/javascript" src="/js/lib/bootstrap-notify.min.js"></script>
    <link type='text/css' rel='stylesheet' href="/css/lib/animate.css">
    <script src="/js/lib/bootbox.min.js"></script>
    <!-- Google calendar api -->
    <script src="/js/google_calendar.js"></script>
</head>
<body>
<script>
    jQuery(document).ready(function() {
        //custom input number
        $(':input[type="number"]').wrap("<div class='input-group'>").parent().append("<div class='input-group-addon number-plus'><span class='glyphicon glyphicon-chevron-up'></span></div><div class='input-group-addon number-minus'><span class='glyphicon glyphicon-chevron-down'></span></div>");
        $('.number-plus').click(function() {
            var el = $(this).prev();
            if (!el.attr('disabled') && !el.attr('readonly')){
                var step = el.attr('step') != null ? el.attr('step') : 1;
                var max = el.attr('max') != null ? el.attr('max') : 50;
                var val = el.val();
                el.val(Math.min(max, el.val() - -step));
                if (val != el.val())
                    el.trigger('change');
            }
        });
        $('.number-minus').click(function() {
            var el = $(this).prev().prev();
            if (!el.attr('disabled') && !el.attr('readonly')) {
                var step = el.attr('step') != null ? el.attr('step') : 1;
                var min = el.attr('min') != null ? el.attr('min') : 0;
                var val = el.val();
                el.val(Math.max(min, el.val() - step));
                if (val != el.val())
                    el.trigger('change');
            }
        });
        //Make the row clickable
        $(".clickable-row").on('click', function () {
            window.document.location = $(this).data("href");
        });
        $('.navbar-toggle-sidebar').on('click', function () {
            $('.navbar-nav').toggleClass('slide-in');
            $('.side-body').toggleClass('body-slide-in');
            $('#search').removeClass('in').addClass('collapse').slideUp(200);
        });
        $('#search-trigger').on('click', function () {
            $('.navbar-nav').removeClass('slide-in');
            $('.side-body').removeClass('body-slide-in');
            $('.search-input').focus();
        });
        $(".pickadate" ).datepicker({
            'language' : "fr",
            'autoclose': 'true',
            'format': "yyyy-mm-dd",
            disableTouchKeyboard: true,
            todayHighlight: true
        });
    });
</script>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
                    MENU
                </button>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <strong><?= App::getAuth()->user()->firstname; ?> <?= App::getAuth()->user()->lastname; ?></strong>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!--
                <form class="navbar-form navbar-left" method="GET" role="search">
                    <div class="form-group">
                        <input type="text" name="q" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                </form>
                -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" onclick="handleSignoutClick()" id="g_logout_btn" style="display: none;">Se déconnecter de Google</a>
                        <a href="#" onclick="handleAuthClick()" id="g_login_btn">Se connecter à Google</a>
                    </li>
                    <li><a href="/" class="glyphicon glyphicon-home"></a></li>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Mon compte
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li class=""><a href="#">Mon profil</a></li>
                            <li class="divider"></li>
                            <li><a href="/logout">Se déconnecter</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
<?php

$menu = App::getMenuManager()->getAllMenus();

?>

    <div class="container-fluid main-container">
        <div class="col-md-2 sidebar">
            <div class="row">
                <div class="absolute-wrapper"> </div>
                <!-- Menu -->
                <div class="side-menu">
                    <nav class="navbar navbar-default" role="navigation">
                        <!-- Main Menu -->
                        <div class="side-menu-container">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="/"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
                                <?php
                                $user_id = App::getAuth()->user()->id;
                                $am_i_admin = App::getAuth()->who() === 'admin';
                                $menu_rights = App::getManageRights()->getAllMenuRightsByUserId($user_id);
                                ?>
                                <li class="panel panel-primary vdropdown" id="">
                                    <a data-toggle="collapse" href="#gestion-client">
                                        <span class="glyphicon glyphicon-user"></span>Gestion client<span class="caret"></span>
                                    </a>
                                    <div id="gestion-client" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <?php
                                                if ($am_i_admin === true || !empty($menu_rights['v1/workflow-macro/view']))
                                                    echo "<li><a href='/v1/workflow-macro/view'>Vue générale</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/client/view']))
                                                    echo "<li><a href='/v1/client/view'>Fiches clients</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/waiting_list/view']))
                                                    echo "<li><a href='/v1/waiting_list/view'>Liste d'attente <span class='label label-warning'>admin</span></a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/maintenante/view']))
                                                    echo "<li><a href='/v1/maintenance/view'>Planning d'entretien <span class='label label-warning'>admin</span></a></li>";
                                                 if ($am_i_admin === true || !empty($menu_rights['v1/cardex/view']))
                                                    echo "<li><a href='/v1/cardex/view'>Cardex <span class='label label-warning'>admin</span></a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/invoice/view']))
                                                    echo "<li><a href='/v1/invoice/view'>Factures <span class='label label-warning'>admin</span></a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/payment/view']))
                                                    echo "<li><a href='/v1/payment/view'>Paiement <span class='label label-warning'>admin</span></a></li>";
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel panel-primary vdropdown" id="">
                                    <a data-toggle="collapse" href="#gestion-operationnelle">
                                        <span class="glyphicon glyphicon-user"></span>Gestion opérationnelle<span class="caret"></span>
                                    </a>
                                    <div id="gestion-operationnelle" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <?php
                                                if ($am_i_admin === true || !empty($menu_rights['v1/mission/add']))
                                                    echo "<li><a href='/v1/mission/add'>Ajout de prestation</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/provider/view']))
                                                    echo "<li><a href='/v1/provider/view'>Fiches prestataires</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/guest-relation/view']))
                                                    echo "<li><a href='/v1/guest-relation/view'>Guest relation <span class='label label-warning'>admin</span></a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/cluster/view']))
                                                    echo "<li><a href='/v1/cluster/view'>BU <span class='label label-warning'>admin</span></a></li>";
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel panel-primary vdropdown" id="">
                                    <a data-toggle="collapse" href="#gestion-marketing">
                                        <span class="glyphicon glyphicon-user"></span>Gestion marketing<span class="caret"></span>
                                    </a>
                                    <div id="gestion-marketing" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <?php
                                                if ($am_i_admin === true || !empty($menu_rights['v1/promocode/view']))
                                                    echo "<li><a href='/v1/promocode/view'>Codes promos</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/subscriber/view']))
                                                    echo "<li><a href='/v1/subscriber/view'>Prospects</a></li>";
                                                if ($am_i_admin === true || !empty($menu_rights['v1/related-services/view']))
                                                    echo "<li><a href='/v1/related-services/view'>Services connexes</a></li>";
                                                 if ($am_i_admin === true || !empty($menu_rights['v1/related-services-request/view']))
                                                    echo "<li><a href='/v1/related-services-request/view'>Requêtes de services</a></li>";
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                if ($am_i_admin === true || !empty($menu_rights['v1/exports/view']))
                                    echo "<li><a href='/v1/exports/view'>Exports <span class='label label-warning'>admin</span></a></li>";
                                ?>
                                <?php if (App::getAuth()->who() == 'admin'): ?>
                                    <li class="panel panel-primary vdropdown" id="dropdown1">
                                        <a data-toggle="collapse" href="#dropdown1-lvl1">
                                            <span class="glyphicon glyphicon-tasks"></span>Backoffice <span class='label label-warning'>admin</span><span class="caret"></span>
                                        </a>
                                        <div id="dropdown1-lvl1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="nav navbar-nav">
                                                    <li><a href="/v1/alert/view">Alertes</a></li>
                                                    <!-- Gestion des droits -->
                                                    <li ><a href="/v1/group/view">Gestion  des groupes</a></li>
                                                    <li><a href="/v1/group/edit">Permissions des groupes</a></li>
                                                    <li><a href="/v1/user/add">Ajouter un utilisateur</a></li>
                                                    <!-- Paramètres admin -->
                                                    <li><a href='/v1/company/view'>Entreprises</a></li>
                                                    <li><a href="/v1/convention/view">Convention</a></li>
                                                    <li><a href="/v1/settings/edit">Paramètres basiques</a></li>
                                                    <li><a href="/v1/settings-admin/edit">Paramètres avancés</a></li>
                                                    <!-- Questionnaire -->
                                                    <li><a href="/v1/survey/view">Questionnaire</a></li>
                                                    <?php if(App::getAuth()->accesslvl() > 9): ?>
                                                        <li><a href="/v1/list/edit">Listes</a></li>
                                                        <li><a href="/v1/item/view">Item</a></li>
                                                        <li><a href="/v1/user-admin/add">Gestion des utilisateurs</a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-10 content">