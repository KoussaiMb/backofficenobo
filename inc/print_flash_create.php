<?php if(Session::getInstance()->hasFlash()): ?>
    <?php foreach(Session::getInstance()->getFlash() as $type => $message): ?>
        <div class="ad bg-<?= $type; ?> ad-<?= $type; ?>" style="padding: 0.5em 0.5em; border-radius: 4px; margin-bottom: 10px">
            <?= htmlspecialchars($message);?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>