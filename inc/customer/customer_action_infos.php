<?php
/**
* file: customer_action_infos.php
* auhtor: Jonathan BRICE
* date: 06-04-2018
* description:
**/


/**
 * $customer_info require:
 *
 * @user_id of the customer
 * @id of the customer
 * @user_token of the customer
 * @call_duratioon of the customer
 */

$validator = App::get_instance_validator((array)$customer_info);

$validator->is_id('user_id', 'Utilisateur invalide', 'Utilisateur manquant');
$validator->is_id('id', 'Client invalide', 'Client manquant');
$validator->isToken('user_token', 'Utilisateur invalide', 'Utilisateur manquant');
$validator->is_timeFormat('call_duration', 'Durée invalide', 'Durée manquante');
if ($validator->is_valid() !== false) {
    $actions = App::getCustomerActions()->getActions();
    $history = App::getCustomerActions()->getActionHistoryByToId($customer_info->user_id);
    $call_requests = App::getListManagement()->getList('call_request');
}
if (!isset($actions) || !isset($history) || !isset($call_requests)):
    echo "<div class='alert alert-warning'>Impossible de générer les actions du client</div>";
else:

$actionNb = sizeof($actions);
$historyNb = sizeof($history);
$call_requestNb = sizeOf($call_requests);

?>
<div class="action-infos">
    <input type="hidden" name="c_id" value="<?= $customer_info->id; ?>" id="c_id">
    <div class="action-title">
        <button type="button" name="call_start" style="color: green;" onclick="startCall()"><span class="glyphicon glyphicon-earphone"></span></button>
        <button type="button" name="call_end" style="color: red;" onclick="stopCall()"><span class="glyphicon glyphicon-phone-alt"></span></button>
        <span id="call_duration" style="margin-left: 10px"><?= $customer_info->call_duration ?></span>
    </div>
    <div class="action-buttons row">
        <div class="col-sm-12" style="padding: 2px">
            <button type="button" class="btn btn-success" onclick="addActionMessage()">Contacter</button>
        </div>        
    </div>
    <ul class="action-history">
    </ul>
    <div class="action-create-message" style="display: none;">
        <h4>Je fais un:</h4>
        <div class="form-group">
            <label for="action_id">Type</label>
            <select name="action_id" id="action_id">
            <?php

                for($i = 0; $i < $actionNb; $i++){
                    echo '<option value="'.$actions[$i]->id.'">'.($actions[$i]->field).'</option>';
                }
            ?>
            </select>
        </div>
        <div class="form-group">
            <label for="call_request_id">Type de rappel</label>
            <select name="call_request_id" id="call_request_id">
            <option value="0">Non planifié</option>
            <?php

                for($i = 0; $i < $call_requestNb; $i++){
                    echo '<option value="'.$call_requests[$i]->id.'">'.($call_requests[$i]->field).'</option>';
                }
            ?>
            </select>
        </div>
        <div class="form-group" id="call_request_date_input">
            <label for="call_request_date">Date</label>
            <input type="date" name="call_request_date" id="call_request_date">
        </div>
        <div class="form-group" id="call_request_time_input">
            <label for="call_request_time">Heure</label>
            <input type="time" name="call_request_time" id="call_request_time">
        </div>
        <div class="form-group">
            <label for="actionMessage">Commentaire</label>
            <textarea name="name" rows="8" cols="80" id="actionMessage"></textarea>
        </div>
        <input type="hidden" name="" value="<?= $customer_info->user_token ?>" id="toToken">
        <input type="hidden" name="" value="<?= App::getAuth()->user()->user_token ?>" id="fromToken">
        <div class="row">
            <button type="button" name="back" class="col-sm-6 btn btn-warning" onclick="cancelActionMessage()">Annuler</button>
            <button type="button" name="sendMessage" class="col-sm-6 btn btn-success" onclick="sendActionMessage()">Valider</button>
        </div>

    </div>
</div>

<style>
    .btn-action-history {
        font-size: 14px!important;
        padding: 2px!important;
        background-color: white!important;
        color: #0b0b0b;
        border: 1px solid #ffb8ae;
    }
    .btn-action-history:hover {
        background-color: #ffb8ae!important;
    }
    .action-title{
        font-size: 1em;
        padding: 10px 15px;
        background-color: rgb(223, 240, 216);
        background-image: linear-gradient(rgb(223, 240, 216) 0px, rgb(208, 233, 198) 100%);
        color: rgb(60, 118, 61);
    }

    .action-infos{
        height: auto;
        border-color: rgb(214, 233, 198);
        border-radius: 4px;
        border-width: 1px;
        border-style: solid;
    }

    .action-buttons{
        margin: 0;
        height: auto;
    }

    .action-buttons button{
        margin: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 1.3em;
        width: 100%;
    }

    .action-history{
        display: block;
        overflow-y: auto;
        min-height: 350px;
        height: 600px;
        padding: 0;
        width: 100%;
    }

    .action-history li{
        padding: 4px 5px 4px 5px;
        text-align: center;
        border-color: rgb(233, 198, 214);
        border-radius: 4px;
        border-width: 1px;
        border-style: solid;
        background-color: rgb(240, 216, 223);
        color: rgb(118, 60, 60);
    }

    .action-history li.active{
        background-color: rgb(220, 196, 203);
    }

    .action-history li.call_request{
        background-color: rgb(220, 50, 50);
    }

    .action-history li.call_request.active{
        background-color: rgb(220, 30, 30);
    }

    .action-history li p{
        padding: 2px 5px 2px 5px;
        text-align: left;
        display: none;
        min-height: 100px;
    }

    .action-history li h6{
        line-height: 0.5px;
        display: none;
        list-style-type: none;
    }

    .action-history li button{
        font-size: 1em;
        color: black;
        list-style-type: none;
        float: right;
        background-color: transparent;
        border: none;
        margin-top: 3px;
        display: none;
    }

    .action-history li h6 hr{
        margin: 0 0 10px 0;
    }

    .action-history li.active p{
        display: inherit;
    }

    .action-history li.active h6{
        display: inherit;
    }

    .action-history li.active button{
        display: inherit;
    }

    .action-message{
        padding: 0;
        margin: 0;
        background-color: white;
        color: black;
    }

    .action-create-message{
        margin: 0;
        background-color: rgb(220, 196, 203);
        width: 100%;
        color: rgb(98, 40, 40);
        padding: 8px 5px 5px 5px;
    }

    .action-create-message h3{
        width: 100%;
        text-align: center;
        margin-top: 10px;
    }

    .action-create-message textarea{
        width: 100%;
        height: auto;
        resize: none;
        margin-bottom: 5px;
        color: black;
    }

    .action-create-message .row{
        margin: 0;
    }

    .action-create-message button{
        padding-top: 5px;
        padding-bottom: 5px;
        color: white;
        font-size: 1.3em;
        margin: 0;
        margin-top: 2px;
    }
</style>
<script src="/js/app.js"></script>
<script src="/js/customer_action_history.js"></script>
<script src="/js/nobo_bo.js"></script>
<script type="text/javascript">
    $(document).on("click", ".action-history li", function(){
        var tmp = this;
        $( ".action-history li" ).each(function() {
            if(tmp != this)
                $(this).removeClass('active');
        });
        $(this).toggleClass("active");
    });

    $(document).ready(function(){
        $('#call_request_id').on('change', function(){
            if ($(this).val() == 0) {
                $("#call_request_date_input").hide();
                $("#call_request_time_input").hide();
            } else {
                $("#call_request_date_input").show();
                $("#call_request_time_input").show();
            }
        }).trigger("change");

        if ($('#toToken').val())
            loadActionHistory($('#toToken').val());
    });
</script>
<?php endif; ?>