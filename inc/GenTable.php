<?php if(isset($table)): ?>
<?php $head = array_keys((array)$table[0]); ?>
<thead>
<tr>
    <?php foreach($head as $key => $col): ?>
        <th><?= $col; ?></th>
    <?php endforeach; ?>
</tr>
</thead>
<tbody>
<?php foreach ($table as $key): ?>
    <?php $row = (array)$key; ?>
        <tr>
            <?php foreach ($row as $k => $v): ?>
                <td><?= $v ?></td>
            <?php endforeach; ?>
        </tr>
<?php endforeach; ?>
</tbody>
<?php endif; ?>