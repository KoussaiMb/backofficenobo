<?php

/* Building the workflow & setting active step as 1 */

// TODO: get workflow step

if(!$customer_info){
    App::setFlashAndRedirect('danger', 'Impossible de récupérer les informations du client', 'dashboard');
}
// TODO: echo step infos

?>
<div class="row col-sm-12 workflow-header">
    <div class="col-sm-2" style="height: 100%">
        <div class="workflow-step">
            <h4 id="w_step">N°<?= $w_step ?></h4>
            <p><?= /*$workflow->getStepById($w_step)->step_name*/ '' ?></p>
        </div>
    </div>
    <div class="col-sm-10 workflow-header-info" id="customer_infos">
        <div class="col-sm-2">
            <p><span class='glyphicon glyphicon-user'></span> <?= $customer_info->firstname ?> <?= $customer_info->lastname ?></p>
            <p><span class='glyphicon glyphicon-earphone'></span> <?= parseStr::phoneReadable($customer_info->phone); ?></p>

        </div>
        <div class="col-sm-6">
            <span class="col-sm-6">
                <p><span class='glyphicon glyphicon-home'></span> <?=$customer_info->address?></p>
                <p><span class=' glyphicon glyphicon-envelope'></span> <?=$customer_info->email?></p>
            </span>
            <span class="col-sm-6">
                <p><span class='glyphicon glyphicon-map-marker'></span> <?=$customer_info->zipcode?> <?=$customer_info->city?></p>
                <p>
                <span class='glyphicon glyphicon-barcode'></span>
                <?php
                    if (App::getCustomer()->usedPromocode($customer_info->id)) {
                        echo "A utilisé un promocode";
                    } else {
                        echo "Pas de promocode";
                    }
                ?>
            </p>
            </span>


        </div>
        <div class="col-sm-4">
            <p>
                <span class='glyphicon glyphicon-euro'></span>
                <?php
                    if (App::getCustomer()->hasPaymentAccount($customer_info->user_token)) {
                        echo "Carte de paiement liée";
                    } else {
                        echo "Pas de carte de paiement";
                    }
                ?>
            </p>
            <p><span class='glyphicon glyphicon-calendar'></span>
                <?=
                parseStr::DatetimeToDate($customer_info->date_list_in)
                ?> à
                <?=
                parseStr::DatetimeToTime($customer_info->date_list_in)
                ?>
            </p>
        </div>
    </div>
</div>
<style>
    .workflow-step{
        background-color: rgb(223, 240, 216);
        vertical-align: middle;
        margin-right: 5px;
        padding: 5px 5px 5px 5px;
        text-align: center;
        height: 100%;
        border-color: rgb(214, 233, 198);
        border-radius: 4px;
        border-width: 1px;
        border-style: solid;
    }

    .workflow-step h4{
        font-size: 1.4em;
    }

    .workflow-step p{
        font-size: 1em;
        line-height: 8px;
    }

    .workflow-header-info{
        padding-top: 5px;
        padding-bottom: 5px;
        background-color: rgb(223, 240, 216);
        font-size: 1em;
        height: 100%;
        vertical-align: middle;
        border-color: rgb(214, 233, 198);
        border-radius: 4px;
        border-width: 1px;
        border-style: solid;
    }

    .workflow-header-info p{
        margin: 0;
        line-height: 18px;
    }

    .workflow-header{
        position: relative;
        margin-bottom: 10px;
        height: 50px;
        color: rgb(60, 118, 61);
    }
</style>
<script type="text/javascript">

</script>
