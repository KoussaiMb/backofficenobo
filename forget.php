<?php
require ('inc/bootstrap_public.php');
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" lang="fr">
    <title>Nobo - Bienvenue</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/public_style.css">
</head>
    <body>
        <div class="container">
            <div class="forget-wrapper col-sm-8">
                <div class="forget-content">
                    <form method="post" action="form/forget" class="form-horizontal">
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label" style="text-align: center">Email:</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" placeholder="johndoe@mail.com" id="email" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary col-sm-12" id="submit">J'ai oublié mon mot de passe</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="forget-foot">
                    <p>Vous vous en rappelez ? <a href="login">Accéder à votre compte Nobo !</a> </p>
                </div>
            </div>
        </div>
        <style>
            body {
                background-color: #ffd699;
            }
            hr {
                border-color: #333333;
            }
            #email {
                width 100%;
            }
            .container {
                display: flex;
                align-items: center;
                margin-top: 30px;
            }
            .forget-wrapper {
                margin: 0 auto;
                background-color: #ffffff;
                border: 1px solid #333333;
                border-radius: 5px;
                padding: 17px;
            }
            .forget-foot {
                text-align: center;
                margin-top: 17px;
            }
        </style>
        <!-- Jquery -->
        <script src="js/lib/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>