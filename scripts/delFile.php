<?php
require ('../inc/bootstrap.php');
$pathToClean = '../upload';
$file = "script_log";
$scripts = new scripts($pathToClean);
$ret = $scripts->delFiles();
$date = new DateTime('now');
if ($ret == -1)
    $log = '[' . $date->format('Y-m-d H:i:s') . ']' . "Une erreur est survenue\n";
else
    $log = '[' . $date->format('Y-m-d H:i:s') . ']' . $ret . " fichiers ont été supprimés\n";
file_put_contents($file, $log, FILE_APPEND | LOCK_EX);