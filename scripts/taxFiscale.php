<?php
die();
/*
 * SCRIPT POUR ENVOYER LES ATTESTATIONS FISCALES
 *
 * Méthode:
 *
 * php taxFiscale.php [année]
 *
 * exmple:
 *
 * php taxFiscale.php 2017
 */

/*
if (strtolower($_SERVER['HTTP_STAGE']) !== 'prod') {
    die("This script can only be executed on the production");
}
*/
/**
 * Load des classes
 */
spl_autoload_register('app_autoload');

function app_autoload($class){
    require "../class/$class.php";
}

/**
 * Chargement des constantes
 */
?>

<html>
<body>
<form method="post">
    <button type="submit" name="action" value="send_all">Envoyer les attestations fiscales</button>
</form>
</body>
</html>

<?php

if (!isset($_POST['action']) || $_POST['action'] !== "send_all") {
    die("Veuillez confirmez l'envoi des mails");
}
//TODO:set the year via POST or do a script
echo "Initialisation...<br>";
$db = App::getDB();
$autoSalary = App::getConf()->getConfByName('autoentrSalary')->value;
$company_conf = App::getCompany()->getCompanyConf();
$company = App::getCompany()->getCompanyById($company_conf->value);
$company_info = constructCompanyInfo($company, $autoSalary);
$type = 'Mission';
$year = 2017;
$action = "generate";

$erratum = [
    "polge.as@gmail.com"
];

$dontSend = [
    "dr.harold.lhermite@gmail.com",
    "jordangozlan5@gmail.com",
    "psoukaloun@yahoo.fr",
    "marleneligonie@gmail.com",
    "antoine.kodsi@gmail.com",
    "heleneprou@contactoffice.net",
    "mona_ayad@hotmail.com",
    "imbertchristiane@hotmail.com",
    "iena@zenstay.com",
    "saint-lambert@zenstay.com",
    "fabrice.marcant@gmail.com",
    "rochefort@zenstay.com",
    "gprwrigley@gmail.com",
    "morgan.ohana@me.com",
    "fpumir@gmail.com",
    "lavinia.bime@gmail.com",
    "cat.monteillard@gmail.com",
    "kenzabalandraud@gmail.com",
    "alan.miny@gmail.com",
    "elena_munoz_aguilar@yahoo.es",
    "marinegimbert@yahoo.fr",
    "agnes.nicodtran@gmail.com",
    "neo@vitality.gg",
    "hdemongeot@hotmail.fr",
    "fischerromain@gmail.com",
    "moranea.380@gmail.com",
    "dr.harold.lhermite@gmail.com",
    "biboucar5@gmail.com",
    "sebastien.croizard@frost.com",
    "delphinebeilin@hotmail.com",
    "Cle8@bluewin.ch",
    "alainber1805@outlook.com",
    "alexis.barrau@viseo.com",
    "montessuy@zenstay.com",
    "aguichardrein@gmail.com",
    "ferry92morice@gmail.com",
    "aurelien_masse@hotmail.com",
    "arthur.katz@gmail.com",
    "clairewurtz@hotmail.com",
    "davenaouri@hotmail.com",
    "amymili@yahoo.fr",
    "champs-elysees@zenstay.com",
    "emma.depoilly@gmail.com",
    "omedouni@hotmail.fr",
    "larrey@zenstay.com",
    "rosaelynd@gmail.com",
    "pbaechlin@hotmail.com",
    "akasha_2202@hotmail.com",
    "pierre_laugeri@yahoo.com",
    "sfourgeaud@noos.fr",
    "ktazi@astorg.com",
    "ugo8544@hotmail.com",
    "p.lechelle@gmail.com",
    "guillaume.gaillard@gmail.com",
    "mlcittanova@gmail.com",
    "mustaphamokass@gmail.com",
    "quentin.reyt@gmail.com",
    "ega.jannaud@gmail.com",
    "gicquel.hel@gmail.com",
    "lara.marx@orange.fr",
    "sblanchi@adhocparis.com",
    "marinedubost@hotmail.com",
    "flowerfabry@gmail.com",
    "laplace.ma@wanadoo.fr",
    "elodie.saux@gmail.com",
    "stephgabriel@gmail.com",
    "virginie_patard@hotmail.com",
    "arda.marion@yahoo.com",
    "antoinettestrodijk@gmail.com",
    "timmerman.jenn@gmail.com",
    "Dearthomas2010@gmail.com",
    "ralphba2003@yahoo.com",
    "jerome.mocq@gmail.com",
    "sarahlucasfrance@gmail.com",
    "laurentdecaux@gmail.com",
    "florence.ligier@hotmail.fr",
    "anais.dourthe@gmail.com",
    "atelierca@live.fr",
    "thomas.baduel@gmail.com",
    "gwegwe3012@hotmail.com",
    "gtaroni@mayerbrown.com",
    "redat@hotmail.com",
    "eric@aromes.com",
    "desvignes.ph@gmail.com",
    "sophiegoudiaby@hotmail.com",
    "atousskha@gmail.com",
    "emmacosso@wanadoo.fr",
    "amina@instagram.com",
    "ln.negrel@gmail.com",
    "jeanpasseron@gmail.com",
    "simonbuicanges@gmail.com",
    "ir.paschalidis@gmail.com",
    "christophe.gatti@gmail.com",
    "franck.trey@gmail.com",
    "valot.emmanuel@gmail.com",
    "sicard.cl@gmail.com",
    "wydra@mona.fr",
    "ferrier.jonathan@gmail.com",
    "bianchi.lydia@gmail.com",
    "francoisevdb@live.be",
    "emilieaugerot@yahoo.fr",
    "acdml@msn.com",
    "annasouakri@hotmail.fr",
    "fbourgois@showmeyourdata.com",
    "cvieville@assemblee-nationale.fr",
    "valerie.dejenlis@gmail.com",
    "sebastien@wattiez.fr",
    "mikaela.ramamonjisoa@gmail.com",
    "mary_bat_and_co@yahoo.fr",
    "aline_braillard@yahoo.fr",
    "louisonlou@gmail.com",
    "drmichaelatlan@gmail.com",
    "pg.ld@outlook.fr",
    "sml.carles@gmail.com",
    "henri.du-boislouveau@gadz.org",
    "jordangozlan5@gmail.com",
    "mrbochet@gmail.com",
    "laurent.gaelle@gmail.com",
    "cecile.chambras@laposte.net",
    "coraimarie@hotmail.fr",
    "thomas.dubouchet@gmail.com",
    "maelle.varene@gmail.com",
    "anatolerucheton@icloud.com",
    "sleridou@gmail.com",
    "mcdomingos92@gmail.com",
    "francoisdrillon@gmail.com",
    "cadieu.cecile75001@gmail.com",
    "julien.mousseau@gadz.org",
    "christabel.hayman@hotmail.com",
    "fashionrelationship.consulting@gmail.com"
];

//Clients this year and payed
$good_client = getClientsPayedAll($db, $year);

foreach ($good_client as $user) {
    try {
        if (in_array($user->email, $dontSend))
            continue;
        echo $user->firstname . " " . $user->lastname;
        echo " - création du client...";
        $client = constructClient($user, $year);
        echo "[OK] récupération des données...";
        $carts = App::getPrestation()->getCartsByUserTokenTypeAndYear($user->user_token, $type, $year);
        $carts_info = constructRows($carts);
        echo "[OK] création du pdf...";
        $taxGenerator = new TaxCreditGenerator($company_info, $client, $carts_info, $action);
        echo "[OK] envoi du mail...";
        if (in_array($user->email, $erratum))
            App::getMail()->transactionYearErratum($client, $year, $taxGenerator->get_pdf());
        else
            App::getMail()->transactionYear($client, $year, $taxGenerator->get_pdf());
        echo "[OK]<br>";
    } catch (Exception $e) {
        echo "[FAILED]<br>";
    }
    sleep(0.5);
}

function constructCompanyInfo($company, $autoSalary)
{
    $company_info = array();

    $company_info['siret'] = "N° SIRET : " . $company->siret;
    $company_info['sap'] = "N° SAP : " . $company->sap;
    $company_info['name'] = $company->corporate_name;
    $company_info['address'] = $company->address;
    $company_info['zipcity'] = $company->zipcode . ", " . $company->city;
    $company_info['realName'] = $company->business_name;
    $company_info['chief'] = $company->ceo_firstname . " " . $company->ceo_lastname;
    $company_info['autoentrSalary'] = $autoSalary;

    try {
        $signature_path = $_SERVER['DOCUMENT_ROOT'] . $company->ceo_signature;
        if (!file_exists($signature_path)) {
            throw new Exception('Le logo de l\'entreprise n\'existe pas, vérifiez son url');
        }
        if(!is_readable($signature_path)){
            throw new Exception('Permission de récupérer le logo de l\'entreprise non accordée');
        }
        $company_info['chiefSignature'] = $signature_path;
    } catch(Exception $e) {
        App::setFlashAndRedirect('danger', $e->getMessage(), 'view');
    }
    return $company_info;
}

function constructClient($user, $year)
{
    $client = array();

    $client['firstname'] = $user->firstname;
    $client['lastname'] = $user->lastname;
    $client['email'] = $user->email;
    $client['address'] = $user->address;
    $client['zipcity'] = $user->zipcode . ', ' . $user->city;
    $client['year'] = $year;

    return $client;
}

function constructRows($carts)
{
    $rows = array();

    foreach ($carts as $cart)
        $rows[] = constructRow($cart);

    return $rows;
}

function constructRow($cart)
{
    $row = array();

    //nom et prénom de la prestataire
    $row['firstname'] = $cart->firstname;
    $row['lastname'] = $cart->lastname;
    $row['payroll_identity'] = $cart->payroll_identity;
    $row['price_final'] = $cart->price_final;
    $row['quantity'] = $cart->quantity;
    $row['date'] = $cart->start;
    //$row['vat'] = $cart->vat;
    $row['unit_price'] = $cart->price;

    if (substr($cart->reference_name, 0, 4) === "inde")
        $row['auto'] = 1;
    else
        $row['auto'] = 0;

    return $row;
}

function getClientsPayedAll($db, $year) {
    try {
        $res = $db->query("SELECT DISTINCT  U.`id`, U.`user_token`, U.`firstname`, U.`lastname`, U.`email`, A.`address`, A.`zipcode`, A.`city`
                           FROM `user` U, `transaction` T, `address` A
                           WHERE T.`user_id` = U.`id`
                           AND U.`id` = A.`user_id`
                           AND U.`id` NOT IN (SELECT DISTINCT U.`id`
                                            FROM `user` U, `transaction` T
                                            WHERE T.`user_id` = U.`id`
                                            AND T.`payed` = 0
                                            AND YEAR(T.`date_create`) = ?
                           )
                           AND YEAR(T.`date_create`) = ?",
                           [$year, $year])->fetchAll();
        return $res;
    }catch(PDOException $e){
        App::logPDOException($e, 1);
        die('Bad request');
    }
}