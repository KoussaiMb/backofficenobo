<?php
//Boostrap classes
spl_autoload_register('app_autoload');

function app_autoload($class){
    $i = mb_substr_count($_SERVER['PHP_SELF'], '/') - 1;
    $str = "";

    while ($i-- > 0)
        $str .= '../';

    require $str . "class/$class.php";
}
//Redirect 404 on .php
$url = $_SERVER['REQUEST_URI'];
if (strlen($url) > 4 && preg_match('/.php$/', $url))
    include("/error/404.php");
//Check if already logged
$auth = App::getAuth();
if (empty($auth) || !empty($auth) && empty($auth->user()))
    App::redirect('/login');

$time = date("H"); //TODO:timezone à intégrer
if ($time >= 22){$message = "bonne fin de soirée";}
elseif ($time >= 18){$message = "bonne soirée";}
elseif ($time >= 14){$message = "bonne après-midi";}
elseif ($time >= 12){$message = "bonne journée";}
elseif ($time >= 10){$message = "bonne fin de matinée";}
elseif ($time >= 5){$message = "bonne matinée";}
else {$message = "bonne nuit";}

App::getAuth()->logout();
Session::getInstance()->setFlash('success', "En vous souhaitant une $message");
App::redirect('/login');