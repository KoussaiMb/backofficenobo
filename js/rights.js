function addOption() {
    let optionName = $('#option_name').val();
    let optionValue = $('#option_value').val();
    let menuId = $('#menu').val();

    if (!anySelected()) {
        bo_notify("Vous n'avez pas sélectionné de groupe ou d'utilisateur");
        return;
    }

    if (!optionName || !optionValue || !menuId
        || optionName.length < 1 || optionValue.length < 1 || menuId.length < 1) {
        bo_notify("Veuillez remplir tous les champs correctement", "danger");
        return;
    }
    $('#modal_option').modal('hide');

    ajax_call(
        'api/options',
        "POST",
        {
            option_name: optionName,
            option_value: optionValue,
            menu_id: menuId
        },
        function (result) {
            if (result.code === '200') {
                $('#options').append(getOptionHtml(result.data, optionName, optionValue, false));
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );
}

function getOptionHtml(id, name, value, activated, disabled) {
    return '<div class="option">'+
        '<div class="col-xs-7 text-center">'+
            name +
        '</div>'+
        '<div class="col-xs-4">'+
            "<div class='checkbox'>" +
              "<label class='checkbox__container' style='padding: 0'>"+
                "<input class='checkbox__toggle "+(disabled ? "disabled" : "" )+"' type='checkbox' "+
                (activated ? "checked" : "" ) +
                " data-id='"+id+"'"+
                " data-name='"+name+"' data-val='"+value+"'"+
                " >"+
                "<span class='checkbox__checker "+(disabled ? "disabled" : "" )+"'></span>"+
                "<span class='checkbox__cross "+(disabled ? "disabled" : "" )+"'></span>"+
                "<span class='checkbox__ok "+(disabled ? "disabled" : "" )+"'></span>"+
              "</label>"+
            "</div>"+
        "</div>"+
        '<button class="spanButton col-xs-1 remove-right" value="'+id+'"><span class="glyphicon glyphicon-remove"></span></button>' +
    "</div>";
}

function loadOptionByMenu(menuId) {
    if (!anySelected()) {
        return;
    }
    $('#options').empty();
    ajax_call(
        'api/options',
        'GET',
        {
            menu_id: menuId,
            target_id: targetId,
            type: targetType
        },
        function (result) {
        if (result.code === '200') {
            for (let i in result.data) {
                let option = result.data[i];
                $('#options')
                    .append(
                        getOptionHtml(
                            option.option_id,
                            option.option_name,
                            option.option_value,
                            option.activated === '1',
                            targetType === 'user' ? option.is_group : option.is_parent
                        )
                    );
            }
        } else {
            bo_notify(result.message, "danger");
        }
    });
}

function changeOption(optionId, activated) {
    if (!anySelected()) {
        bo_notify("Vous n'avez pas sélectionné de groupe ou d'utilisateur");
        return;
    }
    ajax_call(
        "api/options",
        "PUT",
        {
            option_id: optionId,
            activated: activated ? 1 : 0,
            target_id: targetId,
            type: targetType
        },
        function(result) {
            if (result.code === '200') {

            } else {
                bo_notify(result.message, 'danger');
            }
        }
    );
}

function removeUserRight(optionId) {
    if (!anySelected()) {
        bo_notify("Vous n'avez pas sélectionné de groupe ou d'utilisateur");
        return;
    }

    new Promise(function(res, err) {
        bootbox.confirm({
            title: "Confirmation",
            message: "Voulez-vous vraiment supprimer cette option?",
            buttons: {
                confirm: {
                    label: 'Oui',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'Non',
                    className: 'btn-warning'
                }
            },
            callback: function(accepted) {
                if (accepted)
                    res();
            }
        });
    }).then(() => {
        ajax_call(
            "api/options",
            "DELETE",
            {
                option_id: optionId,
                target_id: targetId,
                type: targetType
            },
            function(result) {
                if (result.code == 200) {
                    loadOptionByMenu($('#menu').val());
                } else {
                    bo_notify(result.message, 'danger');
                    console.log(result);
                }
            }
        );
    }).catch(err => {
        bo_notify(err, "danger");
    })
}

function anySelected() {
    return !(!targetId || !targetType);
}
