function editGroup() {
  let group_id = $('#group').val();

  if (group_id && Number(group_id) > 0)
    redirect_with_params('edit', 'POST', {target_id: group_id, target_type: "group"});
  else
    bo_notify("Vous n'avez pas sélectionné de groupe!", "danger");
}

function ajaxGroup(data, callType)
{
    let toSelect;

    return ajax_call('api/group', callType, data, function (ret) {
        if (ret.code === '200') {
            if (callType === 'post')
                toSelect = ret.data.group_id;
            else if (callType === 'put')
                toSelect = $('#group').val();
            else
                toSelect = 0;
            initGroups(toSelect);
        }
        bo_notify(ret.message, ret.code === '200' ? 'success' : 'danger');
    })
}

function initGroups(toSelect)
{
    let groupSelect = $('.groupSelect');

    return ajax_call('api/group', 'GET', {option: 'select'}, function (ret) {
        if (ret.code === '200') {
            groupSelect
                .find('option')
                .remove()
                .end()
                .append(ret.data)
                .val(toSelect)
                .change();
        }
        else {
            bo_notify('un problème est survenu dans la récupération des groupes, contactez un admin 1', 'danger')
        }
    })
}

function getGroupInfo(id, groupInfo)
{
    ajax_call('api/group', 'GET', {option: 'group_id', group_id: id}, function (ret) {
        if (ret.code === '200') {
            groupInfo.groupName = ret.data.name;
            groupInfo.description = ret.data.description;
            groupInfo.parent_id = ret.data.parent_id;
            groupInfo.group_id = ret.data.id;
        }
        else {
            bo_notify('un problème est survenu dans la récupération des groupes, contactez un admin 2', 'danger')
        }
    });
}

function ac_userWithoutGroup(input_search, search_user)
{
    let input_user_token = search_user.find(":input[name=user_token]"),
        input_firstname = search_user.find(":input[name=firstname]"),
        input_lastname = search_user.find(":input[name=lastname]");
    let all_input = search_user.find(":input:hidden");
    let action_button = search_user.find(":button[value=add]");
    let result;

    input_search.devbridgeAutocomplete({
        lookup: function (query, done) {
            if (query.length > 1) {
                ajax_call('api/users', 'GET', {option: 'ac_with_group'}, function (ret) {
                    if (ret.code === '200')
                        result = {suggestions: ret.data};
                    else
                        result = {suggestions: []};
                    done(result);
                }, null);
            }
        },
        maxHeight: 200,
        showNoSuggestionNotice: true,
        onSelect: function (suggestion) {
            input_user_token.val(suggestion.user_token);
            input_firstname.val(suggestion.firstname);
            input_lastname.val(suggestion.lastname);
            action_button.prop('disabled', false);
        },
        onSearchStart: function () {
            all_input.val(null);
            action_button.prop('disabled', true);
        },
        onSearchError: function () {
            all_input.val(null);
            action_button.prop('disabled', true);
        }
    });
}

function createInnerUserDiv(user, sign)
{
    return "<div class='col-xs-12 user-group'>"
        + "<input type='hidden' name='user_token' value='" + user.user_token + "'>"
        + "<input type='hidden' name='firstname' value='" + user.firstname + "'>"
        + "<input type='hidden' name='lastname' value='" + user.lastname + "'>"
        + user.firstname + " " + user.lastname
        + "<button class='spanButton pull-right'><span class='glyphicon glyphicon-"
        + sign
        +"'></span></button>"
        + "</div>";
}

function constructUsersGroup(inner_users, options)
{
    let i = -1;
    let out = "";
    let sign = options.add ? "plus" : "minus";

    while (inner_users[++i])
        out += createInnerUserDiv(inner_users[i], sign);

    return out;
}

function printGroupInnerUsers(inner_users_div, id)
{
    return ajax_call('api/users', 'GET', {group_id: id}, function (ret) {
        if (ret.code === '200') {
            let inner_users = constructUsersGroup(ret.data, "minus");

            inner_users_div.empty().append(inner_users);
        }
        else {
            bo_notify('un problème est survenu dans la récupération des groupes, contactez un admin 3', 'danger')
        }
    })
}

function updateUserDiv(data, callType)
{
    let inner_users = $('.inner-users');
    let outer_users = $('.outer-users');

    if (callType === 'delete'){
        inner_users.find(":input[value='"+data.user_token+"']").parent().remove();
        outer_users.append(createInnerUserDiv(data, "plus"));
    } else if (callType === 'post'){
        outer_users.find(":input[value='"+data.user_token+"']").parent().remove();
        inner_users.append(createInnerUserDiv(data, "minus"));
    }
}

function ajaxUsers(data, callType)
{
    return ajax_call('api/users', callType, data, function (ret) {
        if (ret.code === '200' && callType !== 'get') {
            updateUserDiv(data, callType);
        }
        bo_notify(ret.message, ret.code === '200' ? 'success' : 'danger');
    });
}

function initGroupInfo()
{
    return {
        groupName : '',
        description: '',
        parent_id: '0',
        group_id: '0',
        delete_children: '0',
        users: [],
        rights: []
    };
}
