function updateProviderListNth(providerList) {
    var inputs = providerList.find(':input');

    inputs.each(function (i) {
        $(this).attr('name', 'provider_' + i);
    })
}

function deleteProviderFromList(list, user) {
    return $.grep(list, function(e){
        return e.user_token != user.user_token;
     });
}

function insertProviderDiv(providerList, user, nth) {
    var div = createProviderDiv(user, nth);

    providerList.append(div);
}
function createProviderDiv(user, nth) {
    return  "<div class='provider-div col-sm-6'>"
        +   "<input type='hidden' name='provider_"
        +   nth
        +   "' value='"
        +   user.user_token
        +   "'><p>"
        +   user.name
        +   "<span class='glyphicon glyphicon-remove pull-right'></span></button></p>"
        +   "</div>"
}