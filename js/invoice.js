/* Conditions used for filters */
var conditions = {start_date : "",  item_type : "", address : ""};

/* Hiding and showing rows when filters are used */
function hideRow(index, table_id)
{
    var table = document.getElementById(table_id);
    var tr = table.getElementsByTagName("tr");
    tr[index+1].style.display = "none";
}

function showRow(index, table_id)
{
    var table = document.getElementById(table_id);
    var tr = table.getElementsByTagName("tr");
    tr[index+1].style.display = "";
}

/* Checking correspondance for each row */
function checkCorrespondance(index)
{

    var id_date = 'date' + index;
    var date_input = document.getElementById(id_date).value;

    var id_item_type = 'item_type' + index;
    var item_type_input = document.getElementById(id_item_type).value;

    var id_address = 'address' + index;
    var address_input = document.getElementById(id_address).value;

    var item = this.conditions['item_type'];
    var start_date = this.conditions['start_date'];
    var address = this.conditions['address'];

    var item_month = date_input.split('-')[1];
    var item_year = date_input.split('-')[2];

    var input_start_month = start_date.split('-')[0];
    var input_start_year = start_date.split('-')[1];

    return !((item_type_input.indexOf(item)) === -1
        || ((item_month !== input_start_month || item_year !== input_start_year) && input_start_month !== "")
        || (address_input.indexOf(address)) === -1);
}

/* Checking correspondance for each row */
function filter(table_id)
{
    var it = document.getElementById("mission_nb").value;
    for(var i = 0; i < it; i++){
        if(checkCorrespondance(i)){
            showRow(i, table_id);
        }else{
            hideRow(i, table_id);
        }
    }
}

/* Listener for checkboxes */
$("input[class='checkbox_invoice']").on('change', function(){

    /* Disabling buttons when no invoice are chosen */
    if($("input[class='checkbox_invoice']:checked").length === 0){
        $( "#bill_cart_display" ).prop( "disabled", true);
        $( "#bill_cart_download" ).prop( "disabled", true);
    }else{
        $( "#bill_cart_display" ).prop( "disabled", false);
        $( "#bill_cart_download" ).prop( "disabled", false);
    }

    /* Changing row class when an item is selected*/
    if($(this).is(":checked")){
        $(this).parents("tr").addClass('highlight');
    }else{
        $(this).parents("tr").removeClass('highlight');
    }
});

/* Listener for "All" checkbox */
$('#checkbox_all').change(function (){
    /* Changing status of visible checkboxes when All is checked */
    $('input[class="checkbox_invoice"]:visible').prop('checked', $(this).prop('checked')).change();
});


/*Changing filters when a field is filled */
$('#item_type_select').on('change',function(){
    var value = $(this).find('option:selected').text().trim();
    conditions['item_type'] = (value == 'Tous') ? "" : value;
    filter('table_missions');
}).trigger('change');


$('#customer_address').on('change', function(){
    conditions['address'] = $(this).find('option:selected').text().trim();
    filter('table_missions');
}).trigger('change');


/* Listener for datepicker */
$('#datepicker1').datepicker( {
    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months",
    endDate: "+0m"
}).on('change', function(){
    conditions['start_date'] = $(this).val();

    /* Enabling the display & download of invoices once a month is chosen */
    $( "#display_bill" ).prop( "disabled", false);
    $( "#download_bill" ).prop( "disabled", false);

    filter('table_missions');
});


$('#reinit_client').on('click', function(){
    window.location.href = 'view';
});


/* Rinitialising all buttons and filters */
$('#reinit_filters').on('click', function(){

    $('#datepicker1').val("").trigger('change');

    $("#item_type_select").prop("selectedIndex", 0).trigger('change');
    $("#company_ref").prop("selectedIndex", 0).trigger('change');
    $("#customer_address").prop("selectedIndex", 0).trigger('change');

    $('input[class="checkbox_invoice"]').prop('checked', false).change();

    $( "#display_bill" ).prop( "disabled", true);
    $( "#download_bill" ).prop( "disabled", true);

    $( "#bill_cart_display" ).prop( "disabled", true);
    $( "#bill_cart_download" ).prop( "disabled", true);

    filter('table_missions');
});

/* Buttons handler related to carts invoice download */
$('#bill_cart_display').on('click', function(){
    invoiceGeneration('display', 'CART_REQUEST');
});

$('#bill_cart_download').on('click' ,function(){
    invoiceGeneration('download', 'CART_REQUEST');
});

/* Buttons handler related to months */
$('#display_bill').on('click',function(){
    invoiceGeneration('display', 'FILTER_REQUEST');
});

$('#download_bill').on('click',function(){
    invoiceGeneration('download', 'FILTER_REQUEST');
});

/* Ajax call to get data for building invoice */
function invoiceGeneration(action, request_type)
{
    var user_token = $('#client_token').val();

    var date_full = $('#datepicker1').val();
    var month = "1";
    var year = 2000;
    if(date_full !== ""){
        month = date_full.split('-')[0];
        year = date_full.split('-')[1];
    }

    var item_type_id = $('#item_type_select').find('option:selected').val();
    var company_id = $("#company_ref").find('option:selected').val();
    var address_token = $('#customer_address').find('option:selected').val();

    var checkedBoxes = [];
    var c = document.querySelectorAll('input[name=cart_checkbox]:checked');
    var c_length = c.length;
    for (var i = 0; i < c_length; i++) {
        checkedBoxes.push(c[i].value);
    }

    $.ajax({
        type: 'GET',
        url: 'api/invoice.php',
        data: {
            company: company_id,
            item_type: item_type_id,
            month: month,
            year: year,
            cart_ids: checkedBoxes,
            action: action,
            user_token: user_token,
            request_type: request_type,
            address_token : address_token
        },
        success: function (json){
            var res = JSON.parse(json);
            if(res.code == 200 && res.data){
                sendInvoiceData(res.data, 'form');
            }else{
                bo_notify(res.message, 'danger');
                bo_notify('Impossible de générer la facture', 'danger');
            }
        },
        error: function () {
            bo_notify('Une erreur est survenue', 'danger');
        }
    });
}

/* Function for passing parameters to the next page */
function sendInvoiceData(json, action_url)
{
    $form = $("<form method='POST' action=" + action_url + " target='_blank'></form>");
    for (var name in json) {
        $form.append('<input type="hidden" name="' + name + '" value="' + json[name] + '">');
    }
    $('#hidden_form_container').append($form);
    $form.submit();
}