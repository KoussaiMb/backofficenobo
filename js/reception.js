/*
Updates pending and redirect to home
Used when using "En attente" button
*/
function updateWaitinglistPending(w_id){
    ajax_call(
        'api/manage_steps',
        'PUT',
        {
            w_list_id: w_id
        },
        function(res){
            var res = JSON.parse(json);
            if (res.code == 200){
                window.location = 'dashboard';
            } else {
                bo_notify(res.message, 'danger');
            }
        }
    )
}

/*Modal buttons */
function buildCancelButton()
{
    return  {
        label: "Annuler",
        className: 'btn-warning'
    }
}

function buildDeleteButton(w_id)
{
    return  {
        label: "Supprimer",
        className: 'btn-danger',
        callback: function(){
            openRefuseModal(w_id);
        }
    }
}

/*
Handler when user deletion is validated
If the "Autre" field was selected and no comment was given, asks to fill one
*/
function buildDeleteReasonButton(w_id)
{
    return  {
        label: "Supprimer",
        className: 'btn-danger',
        callback: function(){
            openRefuseModal(w_id);
        }
    }
}

function buildValidateSurveyButton(label, w_id, redirect, action)
{
    return  {
        label: label,
        className: 'btn-success',
        callback: function(){
            changeStep(1);
            bootbox.hideAll();
        }
    }
}


/**
* Dashboard
**/

function acceptNewClient(w_id, cb){
    bootbox.confirm({
        message: 'Voulez vous accepter ce client?',
        buttons:{
            confirm:{
                label: "Oui",
                className: "btn-success"
            },
            cancel: {
                label: "Non",
                className: "btn-danger"
            }
        },
        callback: function(yes){
            if (yes) {
                ajax_call("/v1/reception/api/manage_client", "PUT", {w_list_id: w_id}, function(result){
                    console.log("coucou");
                    if (result.code == 200){
                        cb();
                    } else {
                        bo_notify(result.message, "danger");
                    }
                });
            } else {
                openRefuseModal(w_id);
            }
        }
    });
}

/* Getting which client on which step was clicked */
$(document).on('click', '.thumbnail', function()
{
    // TODO: When in alerte, add accept modal if workflow_id = 0
    var w_id = $(this).data("val");
    var w_phone = $(this).data("phone");
    var clicked_class = $(this).data("class");

    switch (clicked_class){
        case "new-client":
            acceptNewClient(w_id, function(){location.reload();});
            break;
        case "waiting-client":
            ajax_call("api/manage_client", "PUT", {w_list_id: w_id, action: "next"}, function(result){
                if (result.code == 200){
                    redirect_with_params("view", 'POST', {step: 1, w_id: w_id});
                } else {
                    bo_notify(result.message, "danger");
                }
            });
            break;
        default:
            ajax_call("api/manage_client", "GET", {w_list_id: w_id}, function(result){
                if (result.code == 200){
                    if (result.data.pending == 1) {
                        acceptNewClient(w_id, function(){location.reload();});
                    } else if (result.data.step == 0){
                        ajax_call("api/manage_client", "PUT", {w_list_id: w_id, action: "next"}, function(result){
                            if (result.code == 200){
                                redirect_with_params("view", 'POST', {step: 1, w_id: w_id});
                            } else {
                                bo_notify(result.message, "danger");
                            }
                        });
                    } else {
                        redirect_with_params("view", 'POST', {step: result.data.step, w_id: w_id});
                    }
                } else {
                    bo_notify(result.message, "danger");
                }
            });
    }
});

/**
*  SURVEY
*/

$('.select_answer').trigger('change');

/* Build the score modal depending on the calculated score */
/* Scale is still to define */
function buildScoreModal(score, color_scores, thresholds, w_id)
{
    var msg = "";
    var buttons = null;

    var is_accepted = compareScoreAndThreshold(color_scores, thresholds.acceptance, true);
    var is_refused = compareScoreAndThreshold(color_scores, thresholds.refusal, false);

    //Waiting for decision
    if((is_accepted && is_refused) || (!is_accepted && !is_refused)){
        var text = "Veuillez demander l'avis d'un <b>commercial</b> avant de valider. Si aucun n'est disponible, mettre le client <b>en attente</b>.";
        msg = constructScoreModalText(score, text);
        buttons = {
            cancel : buildCancelButton(),
            waiting :   {
                label: "En attente",
                className: 'btn-info',
                callback: function(){
                    updateWaitinglistPending(w_id);
                }
            },
            delete_btn : buildDeleteButton(w_id),
            validate: buildValidateSurveyButton('Valider', w_id, true, 'next')
        };
        //Validated
    }else if(is_accepted && !is_refused){
        msg = constructScoreModalText(score, "Le client va être automatiquement <b>validé</b>.");
        buttons = {
            cancel : buildCancelButton(),
            validate : buildValidateSurveyButton('Valider', w_id, true, 'next')
        };
        //Refused
    }else{
        msg = constructScoreModalText(score, "Le client va être automatiquement <b>supprimé</b>.");
        buttons = {
            cancel : buildCancelButton(),
            delete_btn : buildDeleteButton(w_id)
        };
    }

    return bootbox.dialog({
        closeButton: false,
        backdrop: true,
        title: "Valider le score",
        message: msg,
        buttons: buttons
    });
}

/* Builds the modal containing the score */
function constructScoreModalText(score, message)
{
    var input = "";

    input += '<h5>Le score du client est de : ' + score +  '</h5>';
    input += '<h4>';
    input += message;
    input += '<h4>';

    return input;
}

function compareScoreAndThreshold(color_scores, thresholds_action, accept)
{
    var color_length = color_scores.length;
    var act_length = thresholds_action.length;
    if(color_length !== act_length){
        return false;
    }
    var t_val;
    var i;
    if(accept){
        for(i = 0; i < color_length; i++){
            t_val = typeof (thresholds_action[i].quantity) === 'undefined' ? 0 : thresholds_action[i].quantity;
            if(color_scores[i] < t_val){
                return false;
            }
        }
        return true;
    }else{
        for(i = 0; i < color_length; i++){
            t_val = typeof (thresholds_action[i].quantity) === 'undefined' ? 0 : thresholds_action[i].quantity;
            if(color_scores[i] >= t_val && t_val !== 0){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }
}

////////////////////////////////////////////
////////////////////////////////////////////
// STOP NOBO
////////////////////////////////////////////
////////////////////////////////////////////

function refuseClient() {
    var raisons = [];
    var noReason = true;
    var raisonMessage = $('#raison_other_message').val();
    var w_id = $("#w_id").val();

    $("#refuse_raison :input").each(function() {
        var id = $(this).data("val");
        if (typeof id !== typeof undefined && id !== false) {
            noReason = noReason && !$(this).prop('checked');
            raisons.push({
                id: id,
                checked: $(this).prop('checked'),
                comment: $("#comment_" + id).val()
            });
        }
    });

    if (noReason) {
        bo_notify("Vous avez sélectionné aucune raison!", "danger");
        return;
    }

    $('#modal_refuse').modal('hide');

    ajax_call(
        "api/delete_client",
        "DELETE",
        {
            w_list_id: w_id,
            raisons: JSON.stringify(raisons)
        },
        function (result) {
            if (result.code == 200) {
                bo_notify(result.message, "success");
                $(location).attr('href', '/v1/workflow-macro/view');
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );
}

function openRefuseModal(w_id) {
    $('#modal_refuse').modal();

    if (w_id) {
        $('#modal_refuse').append(`<input type="hidden" name="w_id" id="w_id" value="${w_id}">`);
    }

    $("#refuse_raison textarea").each(function() {
        $(this).hide();
    });

    $("#refuse_raison :input").each(function() {
        $(this).on("change", function() {
            var id = $(this).data("val");
            if ($(this).prop("checked")) {
                $('#comment_'+id).show();
            } else {
                $('#comment_'+id).hide();
            }
        });
    });
}
