////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  10:    createHtml      ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function create_legend(menu){
    $(menu).find('.legend').remove();
    $(menu).prepend("<ul class=\"legend\">\n" +
        "    <li><span class=\"recurent_1\"></span> Récurrent S1</li>\n" +
        "    <li><span class=\"recurent_2\"></span> Récurrent S2</li>\n" +
        "    <li><span class=\"recurent\"></span> Récurrent</li>\n" +
        "    <li><span class=\"ponctuel\"></span> Ponctuel</li>\n" +
        "    <li><span class=\"validated_mission\"></span> Mission validée</li>\n" +
        "    </ul>")
}

function create_info_event(event){
    return "<table style='font-size: 12px; margin-bottom: 5px; text-align: center;' class=\"table table-bordered table-condensed\">\n" +
        "  <thead>\n" +
        "    <tr>\n" +
        "      <th><span class='glyphicon glyphicon-user'></span> Client</th>\n" +
        "      <th><span class='glyphicon glyphicon-home'></span> Adresse</th>\n" +
        "      <th><span class='glyphicon glyphicon-home'></span> Accès Batîment</th>\n" +
        "      <th><span class='glyphicon glyphicon-globe'></span> Arret </th>\n" +
        "      <th><span class='glyphicon glyphicon-road'></span> Correspondance </th>\n" +
        "    </tr>\n" +
        "  </thead>\n" +
        "  <tbody>\n" +
        "    <tr>\n" +
        "      <th scope=\"row\">"+ event.firstname +" "+ event.lastname +"</th>\n" +
        "      <td>"+ event.addresses +"</td>\n" +
        "      <td>" +
        "       <table class=\"table table-bordered table-condensed\">\n" +
        "       <th>Batîment</th>\n" +
        "       <th>Digicode 1</th>\n" +
        "       <th>Digicode 2</th>\n" +
        "       <th>Porte</th>\n" +
        "       <th>Étage</th>\n" +
        "       <th>Interphone</th>\n" +
        "           <tr>\n" +
        "               <td>" + event.batiment + "</td>\n" +
        "               <td>"+ event.digicode_1 + "</td>\n" +
        "               <td>" + event.digicode_2 + "</td>\n" +
        "               <td>" + event.porte + "</td>\n" +
        "               <td>" + event.etage + "</td>\n" +
        "               <td>" + event.interphone + "</td>\n" +
        "           </tr>\n" +
        "        </table>" +
        "      </td>\n" +
        "      <td>"+ event.station +"</td>\n" +
        "      <td>" + event.transport_icon + "</td>\n" +
        "    </tr>\n" +
        "  </tbody>\n" +
        "</table>"
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  10:    End createHtml      ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////CALENDAR DISPONIBILITÉ PROVIDER ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function calendar_dispo_provider(user_token){
    $('#calendar').fullCalendar({
        columnFormat: 'dddd',
        defaultView: 'agendaWeek',
        header: {
            left: '',
            center: '',
            right: ''
        },
        height: 'auto',
        editable: true,
        slotDuration: '00:30:00',
        snapDuration: '00:15:00',
        selectable: true,
        slotEventOverlap: false,
        selectHelper: true,
        selectOverlap: false,
        allDaySlot: false,
        minTime: "08:00",
        maxTime: "22:00",
        businessHours: {
            dow: [ 1, 2, 3, 4 , 5, 6, 0], // Monday - Thursday

            start: '08:00', // a start time (10am in this example)
            end: '22:00' // an end time (6pm in this example)
        },
        eventConstraint: "businessHours",
        selectConstraint: "businessHours",
        events: function(start, end, timezone, callback) {
            let ajax_data = {
                user_token: user_token
            };
            ajax_call("api/provider_dispo", "GET", ajax_data, function(result){
                if (result.code === '200')
                    callback(result.data);
                else
                    bo_notify(result.message, "danger")
            }, function () {
                ajax_log("api/provider_dispo en GET", "edit_provider.js", "ajax_call")
            })
        },
        eventClick: function (event) {
            let start = moment(event.start).format('dddd, H:mm');
            let end = moment(event.end).format('H:mm');
            let modal_dispo_title = $('#modal_dispo_Title');

            modal_dispo_title.html(start + ' à ' + end);
            modal_dispo_title.append("<input id='dispo_id' type='hidden' value='" + event.id + "'> </input>");
            modal_dispo_title.append("<input id='provider_token' type='hidden' value='" + user_token + "'> </input>");
            modal_dispo_title.append("<input id='dispo_day' type='hidden' value='" + moment(event.start).format('d') + "'> </input>");
            $('#modal_dispo').modal();
        },
        select: function(start, end) {
            let the_calendar = $('#calendar');
            let select_duration = moment.utc(end.diff(start)).format("HH:mm:ss");

            if (select_duration < "03:00:00") {
                launch_notify("Une disponibilité ne peux pas être inferieur à 3H", "danger");
                the_calendar.fullCalendar('unselect');
            }
            else {
                let ajax_data = {
                    token: user_token,
                    day: moment(start).day(),
                    start: moment(start).format('HH:mm:ss'),
                    end: moment(end).format('HH:mm:ss')
                };

                ajax_call("api/provider_dispo", "POST", ajax_data, function(result){
                    if (result.code === '200') {
                        the_calendar.fullCalendar('renderEvent',
                            {
                                start: start,
                                id: result.data,
                                end: end,
                                overlap: false
                            },
                            true // make the event "stick"
                        );
                        bo_notify(result.message, "success");
                    }
                    else {
                        bo_notify(result.message, "danger");
                    }
                    the_calendar.fullCalendar('unselect');
                }, function () {
                    ajax_log("api/provider_dispo en POST", "edit_provider.js");
                });
            }
        },
        eventDrop: modifyProviderDispo,
        eventRender: function (event, eventElement){
            $(eventElement).css("text-align", "center");
            $(eventElement).css("opacity", "0.70");
        },
        eventResize: modifyProviderDispo
    });
}

function modifyProviderDispo(event) {
        let ajax_data = {
            id: event.id,
            day: moment(event.start).day(),
            start: moment(event.start).format('HH:mm:ss'),
            end: moment(event.end).format('HH:mm:ss')
        };

    ajax_call("api/provider_dispo", "PUT", ajax_data, function(result){
        if (result.code === '200')
            bo_notify(result.message, "success");
        else
            bo_notify(result.message, "danger");
    }, function () {
        ajax_log("api/provider_dispo en PUT", "edit_provider.js");
    });
}

$('#confirm-delete-dispo-all').on('click', function() {
    $('#modal_dispo').modal('hide');
    let data_bootbox = {
        message: "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir supprimer ce créneaux de disponibilité ?</span>",
        callBackTrue: function callBackTrue(data)
        {
            ajax_call("api/provider_dispo", "DELETE", data.ajax_data, function(result){
                if (result.code === '200') {
                    bo_notify(result.message, "success");
                    $('#calendar').fullCalendar('removeEvents', result.data);
                } else {
                    bo_notify(result.message, "danger");
                }
            }, function () {
                ajax_log("api/provider_dispo en DELETE", "edit_provider.js");
            });
        },
        callBackFalse:function callBackFalse(){
            $('#modal_dispo').modal('show');
        },
        data : {
            true: {
                ajax_data: {
                    id: $('#dispo_id').val(), // get event_id
                    day: $('#dispo_day').val(), // get event_id
                    provider_token: $('#provider_token').val(), // get provider_token
                    del_option: $('input[name=optradio_option]:checked').val() // get del_option : pending or delete
                }
            },
            false: {}
        }
    };

    bootbox_confirm(data_bootbox);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// FIN CALENDAR DISPONIBILITÉ PROVIDER //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////CALENDAR PLANNING PROVIDER ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function calendar_planning_provider(user_token, conf) {
    conf = JSON.parse(conf);
    let ajax_data = {
        user_token: user_token
    };
    ajax_call("api/business_hours", "GET", ajax_data, function(result){
        if (result.code !== '200') {
            bo_notify(result.message, 'warning');
        }
        $('#calendar_planning').fullCalendar({
            height: 'auto',
            columnFormat: 'dddd DD MMM',
            defaultView: 'agendaWeek',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: ''
            },
            selectOverlap: false,
            allDaySlot: false,
            nowIndicator: true,
            slotDuration: '00:30:00',
            snapDuration: '00:05:00',
            eventDurationEditable: true,
            slotEventOverlap: false,
            minTime: "08:00:00",
            maxTime: "22:00:00",
            businessHours: result.data,
            eventConstraint: "businessHours",
            selectConstraint: "businessHours",
            events: function(start, end, timezone, callback) {
                let ajax_data = {
                    user_token: user_token,
                    week_start: JSON.stringify(moment(start).format('YYYY-MM-DD')),
                    week_end: JSON.stringify(moment(end).format('YYYY-MM-DD'))
                };
                ajax_call("api/provider_planning", "GET", ajax_data, function(result){
                    callback(result.data);
                })
            },

            eventOverlap: function() {
                return false;
            },
            viewRender: function(){
                $('#calendar_planning').fullCalendar( 'refetchEvents' );
            },
            eventAfterAllRender: function(){
                create_legend("#menu4");
            },
            eventDrop: function( event, delta, revertFunc) {
                let all_move = "";
                if (event.type !== '3' && event.modified !== '1') {
                    all_move += modalContentAllMission("Déplacer toutes les missions correspondantes ? ");
                }
                //on initialise l'object bootbox_data
                let data_bootbox = {
                    message: "<div style='font-size: 15px; font-weight: bold;'>Modification de mission</div>" +
                    "         " + all_move + "",
                    callBackTrue: function(data)
                    {
                        putEditPlanning(data, event, revertFunc);
                    },
                    callBackFalse: function callBackFalse(){
                        revertFunc();
                    },
                    data : {
                        true: {
                            ajax_data: {
                                id: JSON.stringify(event.slot_id),
                                start: JSON.stringify(event.start),
                                end: JSON.stringify(event.end),
                                user_token: JSON.stringify(user_token),
                                type: JSON.stringify(event.type),
                                previous_date: JSON.stringify(moment(moment(event.start).subtract(moment.duration(delta).asSeconds(), 'seconds')).format("YYYY-MM-DD"))
                            },
                            action: "edit"
                        },
                        false: {}
                    }
                };
                bootbox_confirm(data_bootbox);
                $("[name='move-all-mission']").bootstrapSwitch();
            },
            eventResize: function( event, delta, revertFunc){
                var all_move = "";
                var diff = moment.duration(event.end.diff(event.start)).asSeconds();
                var min_prestation = moment.duration(conf['prestation_min']).asSeconds();
                var max_prestation = moment.duration(conf['prestation_max']).asSeconds();

                if (event.type != 3 && event.modified != 1) {
                    all_move += modalContentAllMission("Voulez vous modifier toutes les missions correspondantes ?");
                }
                if (diff < min_prestation){
                    launch_notify("la durée minimum d'une prestation est de " + conf['prestation_min'], "danger");
                    revertFunc();
                }
                else if (diff > max_prestation){
                    launch_notify("la durée maximale d'une prestation est de " + conf['prestation_max'], "danger");
                    revertFunc();
                }
                else{
                    let data_bootbox = {
                        message: "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir modifier les horaires de cette mission</span>" +
                        "         " + all_move + "",
                        callBackTrue: function(data)
                        {
                            putEditPlanning(data, event);
                        },
                        callBackFalse:function(){
                            revertFunc();
                        },
                        data : {
                            true: {
                                ajax_data: {
                                    id: JSON.stringify(event.slot_id),
                                    start: JSON.stringify(event.start),
                                    end: JSON.stringify(event.end),
                                    user_token: JSON.stringify(user_token),
                                    type: JSON.stringify(event.type),
                                    previous_date: JSON.stringify(moment(moment(event.start).subtract(moment.duration(delta).asSeconds(), 'seconds')).format("YYYY-MM-DD"))
                                },
                                action: "edit"
                            },
                            false: {}
                        }
                    };
                    bootbox_confirm(data_bootbox);
                    $("[name='move-all-mission']").bootstrapSwitch();
                }
            },

            eventDragStart: function (event) {
                let show_holidays = document.getElementsByClassName(event.customer_token);
                $.each(show_holidays, function (index, value){
                    $(value).removeClass("hidden");
                });
            },

            eventDragStop : function(event) {
                let hide_holidays = document.getElementsByClassName(event.customer_token);
                $.each(hide_holidays, function (index, value){
                    $(value).addClass("hidden");
                });
            },

            eventClick: function (event) {
                if (event.event_type === 'agenda') {
                    let modal_planning_title = $('#modal_planning_Title');

                    modal_planning_title.html(create_info_event(event));
                    $('#modal_planning_Body').val(JSON.stringify({
                        id: JSON.stringify(event.slot_id),
                        start: JSON.stringify(event.start),
                        end: JSON.stringify(event.end),
                        user_token: JSON.stringify(user_token),
                        type: JSON.stringify(event.type),
                        previous_date: JSON.stringify(moment(event.start).format("YYYY-MM-DD"))
                    }));
                    modal_planning_title.val(event._id);
                    $('#modal_planning').modal();
                } else if (event.event_type === 'mission_validated') {
                    let modal_mission = $('#modal_mission');

                    modal_mission.find('.modal-body').html('WIP');
                    modal_mission.modal();
                }
            },

            eventRender: function (event, eventElement){
                let lock = event.lock === '0' ? 'unlock': 'lock';
                let event_content = "";

                if (event.firstname !== undefined && event.lastname !== undefined)
                    event_content += "<span class='customer_name'>" + event.firstname + " " + event.lastname + "</span><br>";
                if (event.addresses !== undefined)
                    event_content += "<span class='customer_address'>" + event.addresses + "</span><br/>";
                if (event.transport_icon !== undefined)
                    event_content += event.transport_icon;
                if (event.rendering !== "background")
                    eventElement
                        .find('.fc-content')
                        .append("<span class='pull-right lock_icon'><img src='/img/icon/" + lock + "_icon.svg'></span>")
                        .append(event_content);
            }
        });
    });
}

function putEditPlanning(data, event, revertFunc)
{
    let state = $("#all-mission").bootstrapSwitch('state');
    if (state === true){
        data.ajax_data.edit = JSON.stringify('all');
    }
    else {
        event.modified = 1;
        data.ajax_data.edit = JSON.stringify('single');
        $('#calendar_planning').fullCalendar( 'updateEvent', event );
    }
    ajax_call("api/edit_planning", "PUT", data, function(ret) {
        if (ret.code === '200')
            $('#calendar_planning').fullCalendar( 'refetchEvents' );
        else
            revertFunc();
        launch_notify(ret.message, ret.code === '200' ? 'success' : 'danger');
    }, function () {
        ajax_log("api/edit_planning en PUT", "edit_provider.js");
        revertFunc();
    });
}

function modalContentAllMission(modal_message) {
    return " </br><table style='font-size: 13px; margin-top: 15px; text-align: center;' class=\"table table-bordered table-condensed\">\n" +
        "  <tbody>\n" +
        "    <tr>\n" +
        "      <th scope=\"row\"> "+ modal_message + "</th>\n" +
        "      <th scope=\"row\"> <input id='all-mission' type='checkbox' name='move-all-mission' data-size='small' data-on-text='Oui' data-off-text='Non'></th>\n" +
        "    </tr>\n" +
        "  </tbody>\n" +
        "</table>";
}

$('#confirm-edit-planning').on('click', function() {
    let del_option = $('input[name=optradio_option_planning_1]:checked').val();
    let message = "<span style='font-weight: bold; font-size: 15px;'>";
    let calendar_planning = $('#calendar_planning');
    let modal_title = $('#modal_planning_Title');
    let modal_body = $('#modal_planning_Body');
    let modal_planning = $('#modal_planning');

    if (del_option === "pending")
        message += "Êtes-vous sûr de vouloir mettre cette mission ?";
    else if (del_option === "delete")
        message += "Êtes-vous sûr de vouloir supprimer cette mission ?";
    message += "</span>";

    modal_planning.modal('hide');
    //on initialise l'object bootbox_data
    let data_bootbox = {
        message: message,
        callBackTrue: function callBackTrue(data)
        {
            data.ajax_data.edit = JSON.stringify('single');
            ajax_call("api/edit_planning", "PUT", data, function(result){
                if (result.code === '200') {
                    launch_notify(result.message, "success");
                    calendar_planning.fullCalendar('removeEvents', modal_title.val());
                    calendar_planning.fullCalendar('refetchEvents');
                }
                else{
                    launch_notify(result.message, "danger");
                }
            }/*, function () {
                ajax_log("api/edit_planning en PUT avec le bouton confirm-edit-planning", "edit_provider.js");
            }*/);
        },
        callBackFalse:function callBackFalse(){
            modal_planning.modal('show');
        },
        data : {
            true: {
                ajax_data: JSON.parse(modal_body.val()),
                action: del_option
            },
            false: {}
        }
    };
    bootbox_confirm(data_bootbox);
});


$('#confirm-edit-all-planning').on('click', function() {
    var del_option = $('input[name=optradio_option_planning_1]:checked').val();
    var message = "";
    if (del_option === "pending")
        message += "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir mettre toutes les missions suivantes en attente ?</span>";
    else if (del_option === "delete")
        message += "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir supprimer toutes les missions suivantes ?</span>";
    $('#modal_planning').modal('hide');
    //on initialise l'object bootbox_data
    var data_bootbox = {
        message: message,
        callBackTrue: function callBackTrue(data)
        {
            data.ajax_data.edit = JSON.stringify('all');
            ajax_call("api/edit_planning", "PUT", data, function(result){
                if (result.code === '200') {
                    var calendar_planning = $('#calendar_planning');
                    launch_notify(result.message, "success");
                    calendar_planning.fullCalendar('removeEvents', $('#modal_planning_Title').val());
                    calendar_planning.fullCalendar('refetchEvents');
                }
                else{
                    launch_notify(result.message, "danger");
                }
            } ,function () {
                ajax_log("api/edit_planning en PUT avec le bouton confirm-edit-all-planning", "edit_provider.js");
            });
        },
        callBackFalse:function callBackFalse(){
            $('#modal_planning').modal('show');
        },
        data : {
            true: {
                ajax_data: JSON.parse($('#modal_planning_Body').val()),
                action: del_option
            },
            false: {}
        }
    };

    bootbox_confirm(data_bootbox);
});

/*
    Handler for the button to validate a mission
    Build a the bootbox asking for a promocode or a discount
 */
$('#confirm-validate-mission').on('click', function() {
    $('#modal_planning').modal('hide');
    let data_bootbox = {
        message: buildPromoMessage(),
        callBackTrue: function callBackTrue(data) {
            ajax_call("api/edit_planning", "POST", data, function (result) {
                if (result.code === '200') {
                    launch_notify(result.message, "info");
                    $('#calendar_planning').fullCalendar('refetchEvents');
                }
                else {
                    launch_notify(result.message, "danger");
                }
            }, function () {
                ajax_log("api/edit_planning en POST", "edit_provider.js");
            });
        },
        callBackFalse: function callBackFalse() {
            $('#modal_planning').modal('show');
        },
        data: {
            true: {
                ajax_data: JSON.parse($('#modal_planning_Body').val())
            },
            false: {},
            type_val : {}
        }
    };
    bootbox_add_promo(data_bootbox);
});

/*
    Build the message asking for a promocode or a discount
 */
function buildPromoMessage()
{

    let input = '';

    input += '<br>';
    input += '<p><b>Sélectionnez le type de réduction souhaitée : </b></p>';
    input += '<br>';

    input += '<form action="form" id="form_promo">';

    input += '<div class="col-sm-6">';
    input += '<input class="promo_type_class" type="radio" name="promo_type" value="no_discount" checked><b>Aucune</b><br>';
    input += '<input class="promo_type_class" type="radio" name="promo_type" value="promocode"><b>Code promo</b><br>';
    input += '<input class="promo_type_class" type="radio" name="promo_type" value="discount_euro"><b>Réduction en euro</b><br>';
    input += '<input class="promo_type_class" type="radio" name="promo_type" value="discount_percentage"><b>Réduction en pourcentage</b>';
    input += '</div>';

    input += '<div class="col-sm-3 promo_value_class hidden">';
    input += '<label for="promo_value_id">Entrez la réduction ou le code promo : </label>';
    input += '<input id="promo_value_id" class="form-control" type="text" name="promo_value" value="0">';
    input += '</div>';

    input += '<br>';
    input += '<br>';

    input += '</form>';

    return input;

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////CALENDAR PLANNING PROVIDER ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function launch_notify(message, type) {

    $.notify({
        // options
        icon: 'glyphicon glyphicon-warning-sign',
        message: message
    }, {
        // settings
        element: 'body',
        type: type,
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        spacing: 10,
        z_index: 1031,
        delay: 2000,
        timer: 1000,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}

function bootbox_confirm(data_bootbox){
    bootbox.confirm({
        message: data_bootbox.message,
        buttons: {
            confirm: {
                label: 'Valider',
                className: 'btn-success'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result){
                data_bootbox.callBackTrue(data_bootbox.data.true);
            }
            else{
                data_bootbox.callBackFalse(data_bootbox.data.false);
            }
        }
    });
}

/*
    Bootbox getting the values entered by the user about the promocode & discount
 */
function bootbox_add_promo(data_bootbox)
{
    bootbox.confirm({
        message: data_bootbox.message,
        buttons: {
            confirm: {
                label: 'Valider',
                className: 'btn-success'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result){
                let form_promo_data = $('#form_promo').serialize().split('&');
                let type = form_promo_data[0].split('=')[1];
                let value = form_promo_data[1].split('=')[1];

                data_bootbox.data.true.type_val = [type, value];

                data_bootbox.callBackTrue(data_bootbox.data.true);
            }
            else{
                data_bootbox.callBackFalse(data_bootbox.data.false);
            }
        }
    });
}