var CLIENT_ID = false;
var API_KEY = false;
var AGENDA_ID = false;

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar";

var g_connected = false;

function getGoogleConfigs() {
    return new Promise((res, err) => {
        ajax_call(
            "/api/conf",
            "GET",
            {
                names: JSON.stringify(["GOOGLE_CLIENT_ID", "GOOGLE_API_KEY", "GOOGLE_AGENDA_ID"])
            },
            function (result){
                if (result.code == 200) {
                    CLIENT_ID = result.data.GOOGLE_CLIENT_ID;
                    API_KEY = result.data.GOOGLE_API_KEY;
                    AGENDA_ID = result.data.GOOGLE_AGENDA_ID;
                } else {
                    CLIENT_ID = false;
                    API_KEY = false;
                    AGENDA_ID = false;
                }
                res();
            }
        );
    });
}

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
    getGoogleConfigs().then(() => {
        gapi.client.init({
            apiKey: API_KEY,
            clientId: CLIENT_ID,
            discoveryDocs: DISCOVERY_DOCS,
            scope: SCOPES
        }).then(() => {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        }).catch(e => {
            console.log(e);
        });
    });
}

function updateSigninStatus(isSignedIn) {
    // When signin status changes, this function is called.
    // If the signin status is changed to signedIn, we make an API call.
    if (isSignedIn) {
        $('#g_login_btn').hide();
        $('#g_logout_btn').show();
        g_connected = true;
    } else {
        $('#g_login_btn').show();
        $('#g_logout_btn').hide();
        g_connected = false;
    }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn()
    .then(user => {
        g_connected = true;
        bo_notify("Vous êtes connecté au compte Google: " + user.w3.U3, "success");
    }).catch(err => {
        console.log(err);
    });
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut()
    .then(() => {
        g_connected = false;
        bo_notify("Vous êtes déconecté du compte Google", "success");
    }).catch(err => {
        console.log(err);
    });
}

function getAttendeesInfos(tokens) {
    return new Promise((res, err) => {
        ajax_call(
            "api/user_info",
            "GET",
            {
                tokens: JSON.stringify(tokens)
            },
            function(result) {
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    });
}

function getAddressInfo(address_id) {
    return new Promise((res, err) => {
        if(address_id == 0)
            res(null);

        ajax_call(
            "api/address_info",
            "GET",
            {
                address_id: address_id
            },
            function(result) {
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    });
}

function exportEvent(event) {

    if (!g_connected) {
        bo_notify("Vous n'êtes pas connecté à votre compte google", "danger");
        return;
    }

    if (event.available) {
        bo_notify("Pour le moment, vous pouvez ne pouvez pas exporter de disponibilité", "danger");
        return;
    }

    getAttendeesInfos(event.attendees)
    .then(infos => {
        getAddressInfo(event.address_id)
        .then(address => {

            var commentary = event.commentary;

            if (address) {
                commentary =
                    `Surface: ${address.surface}` +
                    `\nanimaux: ${address.pet}` +
                    `\nrepassage: ${address.have_ironing}` +
                    `\nRécurrence: ${address.rec}` +
                    `\nNombre d'heure: ${address.nbHours}` +
                    `\nBatiment: ${address.batiment}` +
                    `\nDigicode: ${address.digicode}` +
                    `\nDigicode 2: ${address.digicode_2}` +
                    `\nEtage: ${address.floor}` +
                    `\nPorte: ${address.door}` +
                    `\nCommentaire: ${event.commentary}`;
            }

            var g_event = {
                'summary': event.title,
                'location': event.address ? event.address : "Aucune adresse",
                'description': commentary,
                'colorId': event.available ? "1" : "4",
                'start': {
                'dateTime': event.start,
                'timeZone': 'Europe/Paris'
                },
                'end': {
                'dateTime': event.end,
                'timeZone': 'Europe/Paris'
                },
                'recurrence': [
                'RRULE:FREQ=WEEKLY;COUNT=1'
                ],
                'attendees': []
            };
            
            infos.forEach(info => {
                g_event.attendees.push({
                    'displayName': info.firstname + " " + info.lastname,
                    'email': info.email,
                    'comment': "Tel: " + info.phone
                });
            });
            

            var request = gapi.client.calendar.events.insert({
                'calendarId': AGENDA_ID,
                'resource': g_event
            });
        
            request.execute(function(e) {
                if (e.status == "confirmed") {
                    bo_notify("L'évenement à correctement été ajouté", "success");
                } else {
                    bo_notify("Un problème est survenu", "danger");
                }
            });
        });
    }).catch(err => {
        bo_notify("Une erreur est survenue", "danger");
        console.log(err);
    });    
}