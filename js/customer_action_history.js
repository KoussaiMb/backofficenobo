/**
* file: customer_action_history.php
* auhtor: Jonathan BRICE
* date: 06-04-2018
* description:
**/

/**
ACTION HISTORY FUNCTIONS
**/

function addActionMessage(){
    $('.action-history').hide();
    $('.action-create-message').show();
}

function appendActionHistory(actionInfo){
    if (actionInfo.request_date) {
        $('.action-history').append(
            `<li class="call_request" id="action${actionInfo.id}">
                <h4>
                    ${actionInfo.call_request_name} / ${actionInfo.action_name}
                    <br/>
                    <h5> ${actionInfo.request_time ? moment(actionInfo.request_date + " " + actionInfo.request_time).format("DD/MM/YYYY HH:mm") : moment(actionInfo.request_date).format("DD/MM/YYYY")} </h5>
                </h4>
                <h6><hr/>fait le: ${moment(actionInfo.action_date).format("DD/MM/YYYY")} </h6>
                <h6>par: ${actionInfo.from_firstname} ${actionInfo.from_lastname} </h6>
                <button type="button" name="button" onclick="editActionMessageModal(${actionInfo.id})"><span class="glyphicon glyphicon-pencil"></span></button>
                <p class="action-message"  id="actionMessage${actionInfo.id}">${actionInfo.message}</p>
                <div class="row" style="padding-left: 10px; padding-right: 10px; margin:0">
                    <div class="col-sm-6" style="padding: 0"><button type="button" name="back" class="btn btn-warning" style="width: 100%; margin:0" onclick="closeCallRequest(${actionInfo.id},false)">Annuler</button></div>
                    <div class="col-sm-6" style="padding: 0"><button type="button" name="sendMessage" class="btn btn-success" style="width: 100%; margin: 0" onclick="closeCallRequest(${actionInfo.id},true)">Terminer</button></div>
                </div>
            </li>`
        );
    } else if (actionInfo.done || actionInfo.canceled) {
        $('.action-history').append(
            `<li id="action${actionInfo.id}">
                <h4>
                    ${actionInfo.call_request_name} / ${actionInfo.action_name}
                    <br/>
                    <h5> 
                    ${
                        actionInfo.done ? 
                        moment(actionInfo.done).format("DD/MM/YYYY HH:mm") : 
                        moment(actionInfo.canceled).format("DD/MM/YYYY HH:mm")
                    } 
                    </h5>
                </h4>
                <h6><hr/>fait le: ${moment(actionInfo.action_date).format("DD/MM/YYYY")} </h6>
                <h6>par: ${actionInfo.from_firstname} ${actionInfo.from_lastname} </h6>
                <button type="button" name="button" onclick="editActionMessageModal(${actionInfo.id})"><span class="glyphicon glyphicon-pencil"></span></button>
                <p class="action-message"  id="actionMessage${actionInfo.id}">${actionInfo.message}</p>
            </li>`
        );
    } else {
        $('.action-history').append(
            `<li id="action${actionInfo.id}">
                <h4>
                    ${actionInfo.action_name}
                    <br/>
                    <h5> 
                    ${moment(actionInfo.action_date).format("DD/MM/YYYY HH:mm")} 
                    </h5>
                </h4>
                <h6><hr/>par: ${actionInfo.from_firstname} ${actionInfo.from_lastname} </h6>
                <button type="button" name="button" onclick="editActionMessageModal(${actionInfo.id})"><span class="glyphicon glyphicon-pencil"></span></button>
                <p class="action-message"  id="actionMessage${actionInfo.id}">${actionInfo.message}</p>
            </li>`
        );
    }
}

function sendActionMessage(){
    if($('.action-create-message').is(":hidden"))
        return;

    let a_id = $('#action_id').val();
    let message = $('#actionMessage').val();
    let to_token = $('#toToken').val();
    let from_token = $('#fromToken').val();
    let cr_id = $('#call_request_id').val();
    let call_date = $('#call_request_date').val();
    let call_time = $('#call_request_time').val();

    if (!a_id || !message || !to_token || !from_token){
        bo_notify('Il manque une information!', 'danger');
        return;
    }

    if (cr_id === false && call_date !== false) {
        bo_notify('Vous n\'avez pas entré de date pour la demande de rappel', 'danger');
        return;
    }

    let ajax_data = {
        customerAction: a_id,
        from_token: from_token,
        to_token: to_token,
        message: message,
        call_request_id: cr_id,
        call_date: call_date,
        call_time: call_time ? call_time + ":00" : null
    };

    ajax_call("/v1/reception/api/action_history", "POST", ajax_data, function(result){
        if(result.code === '200'){
            bo_notify(result.message, 'success');
            $('.action-history').show();
            $('.action-create-message').hide();
            $('#actionMessage').val('');
            if ($('#toToken').val())
                loadActionHistory($('#toToken').val());
        }else{
            bo_notify(result.message, 'danger');
        }
    });
}

function closeCallRequest(a_id, done) {
    new Promise((res, err) => {
        bootbox.confirm({
            message: "Voulez-vous vraiment " + (done ? "terminer": "annuler") + " cette demande rappel?",
            buttons: {
                confirm: {
                    label: 'Oui',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Non',
                    className: 'btn-danger'
                }
            },
            callback: function (yes) {
                if (yes){
                    res();
                }
            }
        })
    }).then(() => {
        ajax_call(
            "/v1/reception/api/action_history",
            "PUT",
            {
                action_id: a_id,
                done: done ? 1 : 0
            },
            function(result){
                if (result.code === '200') {
                    bo_notify(result.message, "success");
                    if ($('#toToken').val())
                        loadActionHistory($('#toToken').val());
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        )
    }).catch(err => {
        ajax_log(err, "/js/customer_action_history.js");
    });
}

function cancelActionMessage(){
    $('.action-history').show();
    $('.action-create-message').hide();
}

function editActionMessage(ah_id, message){
    let ajax_data = {
        action_id: ah_id,
        message: message
    };

    ajax_call("/v1/reception/api/action_history", "PUT", ajax_data, function(result){
        if (result.code === '200'){
            $('#actionMessage' + ah_id).html(message);
            bo_notify(result.message, 'success');
        } else {
            bo_notify(result.message, 'danger');
        }
    });
}

function editActionMessageModal(ah_id){
    var msg = '<h5>Commentaire:</h5><textarea name="name" rows="8" cols="80" id="actionMessageModal" style="resize: none; width: 99.8%">'+$('#actionMessage'+ah_id).html()+'</textarea>';
    var buttons = {
        cancel : {label: "Annuler", className:"btn-warning"},
        confirm : {label: "Valider", className:"btn-success",
            callback: function(){
                editActionMessage(ah_id, $('#actionMessageModal').val());
            }
        }
    };

    bootbox.dialog({
        closeButton: false,
        backdrop: true,
        title: 'Editer le commentaire',
        message: msg,
        buttons: buttons
    });
}

function loadActionHistory(to_token) {
    $('.action-history').empty();
    ajax_call(
        "/v1/reception/api/action_history",
        "GET",
        {
            to_token: to_token
        },
        function(result){
            if (result.code == 200) {
                result.data.forEach(action => {
                    appendActionHistory(action);
                });
            } else {
                bo_notify(result.message, "danger");
            }
        }
    )
}

var callIntervalId = 0;

function startCall () {
    clearInterval(callIntervalId);
    callIntervalId = setInterval(function() {
        var currentDuration = $("#call_duration").html();
        if (currentDuration) {
            $('#call_duration').html(addTimes(currentDuration, '00:00:01'));
        }
    }, 1000);
}

function stopCall () {
    clearInterval(callIntervalId);

    ajax_call(
        "/v1/reception/api/call_duration",
        "PUT",
        {
            c_id: $("#c_id").val(),
            duration: $("#call_duration").html()
        },
        function (result) {
            if (result.code != 200) {
                bo_notify(result.message, "danger");
            }
        }
    );
}
