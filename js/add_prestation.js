var global_dragged_id;
var global_nb_duration;
var global_type;

function getDates(startDate, stopDate) {
    let dateArray = [];
    let currentDate = moment(startDate);
    let endDate = moment(stopDate);

    while (currentDate <= endDate) {
        dateArray.push( moment(currentDate).format('DD-MM-YYYY') );
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}

function init_datepicker(address_token){
    let ajax_data = {
        address_token: JSON.stringify(address_token)
    };

    return ajax_call('api/holidays', "GET", ajax_data, function(result){
        let disableDate = [];

        $.each(result.data, function(index, value){
            disableDate = disableDate.concat(getDates(moment(value.start), moment(value.end)));
        });
        $('.date_picker').datepicker({
            startDate: '+0d',
            language: 'fr',
            format: 'DD dd MM yyyy',
        });
        sessionStorage.setItem('holiday_date', JSON.stringify(disableDate));
    });
}


$(document).ready(function() {
    $('.date_confirm').on('change', function(){ // check if date_confirm is under 2 day after today
        let date = $(this).val();

        $('.alert_day').remove();
        if (moment(date, 'dddd Do MMMM YYYY') < moment(moment().add(2, 'days').format("YYYY-MM-DD"))){
            $('.form_confirm').prepend("<div class='alert_day col-sm-12 text-center alert alert-warning'>Attention, si vous validez, vous ne respectez pas le délai de 48H</div>");
        }
    });

    $('#btn-accept').on('click', function () {
        core_send_event();
    });

    $('#btn-refuse').on('click', function () {
        $('#modal_search').modal('show');
        $('#modal_alert').modal('hide');
    });
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  0:    add_pending_prévionnel      /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function() {
    $('#tab3').on('click', function () {
        let ajax_data = {
            method: JSON.stringify("getAgenda")
        };
        ajax_call('api/pending', 'GET', ajax_data, function (result) {
            if (result.code === '200') {
                $('.pending_container_recurent').empty();
                for (let i = 0; i < result.data.length; i++) {
                    $('.pending_container_recurent').append(create_pending_item(result.data[i], 'pending_item_recurent'));
                }
                setClickListenerOnPendingItem();
            } else {
                bo_notify(result.message, 'danger');
            }
        });
    });
});

function setClickListenerOnPendingItem() {
    $('.pending_item_recurent').on('click', function () {
        $('#valid_choice').remove();
        let type = $(this).find('.type').attr('value');
        let start_rec = $(this).find('.start_rec').attr('value');
        let end_rec = $(this).find('.end_rec').attr('value');
        let duration = {
            duration: [$(this).find('.time_slot').attr('value')],
            locked: [$(this).find('.locked_value').attr('value')]
        };
        let method = 'edit';
        let frequency = {frequency: "recurent", frequencyValue: type, autoValue: "0"};

        global_nb_duration = 1;
        global_type = frequency.frequency;
        sessionStorage.setItem("duration", JSON.stringify(duration));
        sessionStorage.setItem("event_id", JSON.stringify($(this).find('.event_id').attr('value')));
        sessionStorage.setItem("method", JSON.stringify(method));
        sessionStorage.setItem('address_token', $(this).find('.address_token').attr('value'));
        sessionStorage.setItem('user_token', $(this).find('.user_token').attr('value'));
        let data_bootbox = {
            message: "<span style='font-weight: bold; font-size: 15px;'>Cette mission va du "+ moment(start_rec, "YYYY-MM-DD").format("Do MMMM YYYY") +" au "+ moment(end_rec, "YYYY-MM-DD").format("Do MMMM YYYY") +" voulez vous la modifier ?</span>",
            callBackTrue: function callBackTrue(data)
            {
                confirm_start_rec(1, data.ajax_data.frequency.frequency);
            },
            callBackFalse:function callBackFalse(data)
            {
                $('.date_confirm').val(data.ajax_data.start_rec);
                $('.date_confirm_end').val(data.ajax_data.end_rec);
                valid_select(data.ajax_data.duration);
            },
            data : {
                true: {
                    ajax_data: {
                        frequency: frequency
                    }
                },
                false: {
                    ajax_data: {
                        start_rec: moment(start_rec, "YYYY-MM-DD").format('dddd Do MMMM YYYY'),
                        end_rec: moment(end_rec, "YYYY-MM-DD").format('dddd Do MMMM YYYY'),
                        frequency: frequency,
                        duration: duration
                    }
                }
            }
        };

        bootbox_confirm(data_bootbox);
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  0:   End add_pending_prévionnel      //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  1:    Frequency management      ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function get_frequency(frequency_type){
    let frequency_tab;

    if (frequency_type === 'recurent'){
        let frequencyValue = $(".frequency").val();
        let autoValue = $(".auto_search_recurrent").val();
        frequency_tab = {
            frequency : frequency_type,
            frequencyValue : frequencyValue,
            autoValue : autoValue
        };
    }
    else if(frequency_type === 'ponctuel'){
        frequency_tab = {
            frequency : frequency_type,
            frequencyValue : 3
        };
    }
    return frequency_tab;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////     End  Frequency management          ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// 8 : Init and creation of select duration ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function create_timepicker(element, minTime, maxTime) {
    element.timepicker({
        'minTime': minTime,
        'maxTime': maxTime
    });
}

function create_duration_selection(index) {
    return '<div class="col-xs-6 duration_div_'+ index +'">\n' +
        '       <input name="duration" type="text" class="form-control input-sm duration_slot" id="duration_recurent_'+ index +'" data-time-format="H:i" required>\n' +
        '   </div>\n' +
        '   <div class="col-xs-6 flex_div_'+ index +'">\n' +
        '      <div class="form-group">\n' +
        '           <select class="form-control flex_duration_recurent_'+ index +'" style="height: 30px; padding: 5px 10px; font-size: 12px; line-height: 1.5;" id="select_lock">\n' +
        '                <option value="0">Flexible</option>\n' +
        '                <option value="1">Rigide</option>\n' +
        '           </select>\n' +
        '      </div>\n' +
        '   </div>'
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// End Init and creation of select duration ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////










////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// 2 : Init and creation of draggable duration ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function initiate_draggable_duration(duration_tab, event_id){
    let rgb_tab = ["rgba(255,255,0,0.9)", "rgba(0,255,255,0.7)", "rgba(255,0,255,0.7)","rgba(125,0,255,0.7)", "rgba(0,255,0,0.7)", "rgba(0,0,255,0.7)", "rgba(255, 0, 125,0.7)"];

    $('.duration').remove();
    for (let i = 0; i < duration_tab.duration.length; i++) {
        $('#duration_drag').append(create_draggable_duration(duration_tab.duration[i], i + 1));
        $('.duration_' + (i + 1)).draggable({
            revert: true,      // immediately snap back to original position
            revertDuration: 0, //
            stop: function (event, ui) {
                global_dragged_id = undefined;
            },
            start: function (event, ui) {
                global_dragged_id = $(this).attr('value');
            }
        });
        $('.duration_' + (i + 1)).data('duration', duration_tab.duration[i]);
        $('.duration_' + (i + 1)).data('event', {
            resourceId: duration_tab.duration[i],
            startEditable: true,
            id: 'duration_' + (i + 1),
            event_id: event_id,
            edition: 'duration_event',
            color: rgb_tab[i],
            locked: duration_tab.locked[i]
        });
    }
}

function get_duration(nb_duration, type_frequency){
    let duration = {
        duration: [],
        locked: []
    };

    for (let i = 1; i <= nb_duration; i++) {
        let tmp_duration = $('#duration_' + type_frequency + '_' + i).val();
        let tmp_locked = $('.flex_duration_' + type_frequency + '_' + (i)).val();
        duration.duration.push(tmp_duration);
        duration.locked.push(tmp_locked);
    }
    duration.duration.sort();
    duration.duration.reverse();

    return duration;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// End Init and creation of draggable duration ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// 3: Creation of HTML part ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function create_draggable_duration(duration, i){
    let rgb_tab = ["rgba(255,255,0,0.5)", "rgba(0,255,255,0.5)", "rgba(255,0,255,0.5)","rgba(125,0,255,0.5)", "rgba(0,255,0,0.5)", "rgba(0,0,255,0.5)", "rgba(255, 0, 125,0.5)"];
    return '<div value="' + duration +'" style="font-weight: bold; font-size: 15px; text-align: center; background-color: '+ rgb_tab[i - 1] +'" class="draggable_duration duration col-xs-2 duration_'+ i +'" ><span class="glyphicon glyphicon-time"></span> '+ duration +'</div>';
}

function create_html(data) {
    return "<div value='"+ data[1] +"' style='font-weight: bold; font-size: 15px; text-align: center;' class='col-xs-12 provider_name provider_name_" +data[1]+ "'><span class='glyphicon glyphicon-calendar'></span> " +data[0]+ "</div>";
}


function create_container(data){
    return "<div class='container_provider container_provider_" +data[1]+ "'></div>";
}

function create_validate_button(address_token , customer_token, method){
    return "<form onsubmit=\"return core_check_event();\" id=\"valid_choice\">" +
        "         <input value='"+ address_token +"' class='address_token_validate' type='hidden'>" +
        "         <input value='"+ customer_token +"' class='customer_token_validate' type='hidden'>" +
        "         <input value='" + method +"' name='method' type='hidden'>" +
        " <div class=\"form-group\" style=\"margin-top: 2em;\" id=\"valid_choice_duration\">\n" +
        "              <button type=\"submit\" class=\"btn btn-success col-xs-12 btn-sm\">Confirmer les créneaux</button>\n" +
        "        </div>" +
        "   </form>"
}

function create_pending_item(data, class_name){
    return "<div class='"+ class_name +" pending_item col-xs-12'>" +
        "<table class='table table-condensed table-bordered'>" +
        "<thead>" +
        "      <div type='hidden' class='address_token hidden' value='"+ data.address_token +"'></div>" +
        "      <div type='hidden' class='start_info hidden' value='"+ data.start +"'></div>" +
        "      <div type='hidden' class='user_token hidden' value='"+ data.user_token +"'></div>" +
        "      <div type='hidden' class='time_slot hidden' value='"+ data.duration.substr(0, 5) +"'></div>" +
        "      <div type='hidden' class='locked_value hidden' value='"+ data.locked +"'></div>" +
        "      <div type='hidden' class='event_id hidden' value='"+ data.id +"'></div>" +
        "      <div type='hidden' class='start_rec hidden' value='"+ data.start_rec +"'></div>" +
        "      <div type='hidden' class='end_rec hidden' value='"+ data.end_rec +"'></div>" +
        "      <tr class='type' value='"+ data.type +"'>\n" +
        "        <th style='max-width: 100px;'><span class='glyphicon glyphicon-user'></span>&nbsp&nbsp Prenom : "+ data.firstname +" &nbsp&nbspNom : "+ data.lastname +"</th>\n" +
        "        <th class='warning'><span class='glyphicon glyphicon-time'></span> Durée : "+ data.duration.substr(0, 5) +"</th>\n" +
        "      </tr>\n" +
        "    </thead>" +
        "</table></div>"
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// End Creation of HTML part ///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////















////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////// 4 : Core Valid Mission Choice////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function core_check_event(){
    let data_customer = JSON.parse(sessionStorage.getItem('data_customer'));
    let event = get_event_to_send();
    let format_event = format_to_send_event(event);

    let ajax_data = {
        events: JSON.stringify(format_event),
        frequency: JSON.stringify(data_customer.frequency),
        customer_token: JSON.stringify(data_customer.customer_token),
        address_token: JSON.stringify(data_customer.address_token),
        user_token: JSON.stringify(data_customer.user_token),
        type: JSON.stringify(data_customer.type),
        date_start: JSON.stringify(data_customer.date_start),
        date_end: JSON.stringify(data_customer.date_end)
    };

    function check_holidays() {
        let dfd = jQuery.Deferred();

        ajax_call('api/holidays', 'GET', ajax_data, function(json){
            if (jQuery.isEmptyObject(json.data)){
                dfd.resolve("empty");
            }
            else{
                $('#modal_holidays').find('.modal-body').empty();
                $.each(json.data, function(index, holidays) {
                    $('#modal_holidays').find('.modal-body').append("<h5> - Vacances du "+ moment(holidays.start, "YYYY-MM-DD").format("dddd Do MMMM YYYY") +" au "+ moment(holidays.end, "YYYY-MM-DD").format("dddd Do MMMM YYYY") +"</h5>");
                });
                $('#modal_holidays').modal('show');
                $('#modal_search').modal('hide');
                $('#btn-accept-holidays').on('click', function(){
                    $('#modal_holidays').modal('hide');
                    dfd.resolve("save");
                });
                $('#btn-refuse-holidays').on('click', function(){
                    $('#modal_holidays').modal('hide');
                    dfd.resolve("remove");
                });
                $('#btn-return-holidays').on('click', function(){
                    $('#modal_holidays').modal('hide');
                    $('#modal_search').modal('show');
                    dfd.rejectWith("return");
                });
            }
        });

        return dfd.promise();
    }

    $.when( check_holidays() ).then(
        function( status ) {
            ajax_data.holidays = JSON.stringify(status);
            sessionStorage.setItem('ajax_data', JSON.stringify(ajax_data));
            ajax_call('api/check', 'GET', ajax_data, function (check_ans) {
                if (check_ans.code === '400') {
                    bo_notify(check_ans.message, "danger");
                }
                else if (check_ans.code === '200'){
                    core_send_event();
                } else {
                    let modal_alert = $('#modal_alert');

                    modal_alert.find('.modal-body').empty();
                    $.each(json.data['conflict_message'], function(index, problem) {
                        modal_alert.find('.modal-body').append(problem);
                    });
                    modal_alert.modal('show');
                    $('#modal_search').modal('hide');
                }
            });
        }
    );
    return false;
}

function core_send_event(){
    let data_customer = JSON.parse(sessionStorage.getItem('data_customer'));
    let ajax_data = JSON.parse(sessionStorage.getItem('ajax_data'));
    let method = "";

    if (data_customer.method === 'edit')
        method += 'PUT';
    else if (data_customer.method === 'add')
        method += 'POST';
    if (method !== "") {
        ajax_call('api/prestation', method, ajax_data, function (json) {
            $('#tab3').trigger('click');
            $('#modal_search').modal('hide');
            $('#modal_alert').modal('hide');

            bo_notify(json.message, json.code === '200' ? 'success' : 'danger');
        });
    }
}

function get_event_to_send(){
    return $('.calendar_agenda').not(".hidden").fullCalendar( 'clientEvents', function(events){
        return events.edition === "duration_event";
    });
}


function format_to_send_event(event){
    let format_event = [];

    $.each(event, function(index, value) {
        format_event.push({duration: value.resourceId, start: moment(value.start).format("YYYY-MM-DD HH:mm:ss"), end: moment(value.end).format("YYYY-MM-DD HH:mm:ss"), name: value.id, locked: value.locked, id: value.event_id});
    });
    return format_event;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////// End  Core Block //////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////









////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////// 5 : Core Search Provider /////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function confirm_start_rec(nb_duration, type_frequency){
    let address_token = sessionStorage.getItem('address_token');
    init_datepicker(address_token).done(function (){
        if ($('#select_frequency').val() != 3)
            $('#modal_confirm').modal('show');
        else
            $('#modal_confirm_ponctuel').modal('show');
        global_nb_duration = nb_duration;
        global_type = type_frequency;
    });
    return false;
}

function valid_select(){
    let frequency = get_frequency(global_type);
    let date_start;
    let date_end;

    if (frequency.frequencyValue !== '3') {
        date_start = moment($('.date_confirm').val(), 'dddd Do MMMM YYYY');
        date_end = $('.date_confirm_end').val() == "" ? null : moment($('.date_confirm_end').val(), 'dddd Do MMMM YYYY');
        $('#modal_confirm').modal('hide');
    }
    else {
        date_start = moment($('.date_confirm_ponctual').val(), 'dddd Do MMMM YYYY');
        date_end = moment($('.date_confirm_ponctual').val(), 'dddd Do MMMM YYYY');

        $('#modal_confirm_ponctuel').modal('hide');
    }

    if (date_end != null) {
        if (moment(date_start).isAfter(moment(date_end))) {
            bo_notify("Votre début ne doit pas dépasser la fin", "danger");
            return false;
        }
        else {
            date_start = moment(date_start).format('YYYY-MM-DD');
            date_end = moment(date_end).format('YYYY-MM-DD');
        }
    }
    else{
        date_start = moment(date_start).format('YYYY-MM-DD');
        date_end = null;
    }

    $('#modal_search').addClass("hidden_item");
    let address_token = sessionStorage.getItem('address_token');
    let customer_token = sessionStorage.getItem('user_token');
    let event_id = JSON.parse(sessionStorage.getItem('event_id'));
    let method_feed = JSON.parse(sessionStorage.getItem('method'));
    let method = (method_feed === null) ? 'add' : method_feed;
    let duration_feed = JSON.parse(sessionStorage.getItem('duration'));
    let duration = (duration_feed === null) ? get_duration(global_nb_duration, global_type) : duration_feed;
    let data_customer = {
        frequency: frequency,
        date_start: date_start,
        date_end: date_end,
        customer_token: customer_token,
        address_token: address_token,
        duration: duration,
        method: method,
        event_id: event_id,
        type: frequency.frequencyValue

    };

    sessionStorage.setItem('data_customer', JSON.stringify(data_customer));
    core_get_valid_provider(data_customer);
    $('#valid_choice').remove();

    return false;
}

function core_get_valid_provider(data){
    let ajax_data = {
        date_start: JSON.stringify(data.date_start),
        date_end: JSON.stringify(data.date_end),
        duration: JSON.stringify(data.duration.duration),
        frequency: JSON.stringify(data.frequency),
        address_token: JSON.stringify(data.address_token),
        customer_token: JSON.stringify(data.customer_token),
        type: JSON.stringify(data.type)
    };

    $('body').prepend("<div class='loading'>Loading</div>");
    ajax_call('api/search_provider', 'POST', ajax_data, function (result) {
        process_response(result, data);
    }/*, function () {
        $('.loading').remove();
        bo_notify("Impossible de récupérer les données, contactez un admin", "danger");
    }*/);
}
function reset_screen() {
    window.location = '/v1/mission/add';
}

function process_response(result, data){
    if (result.code === "324") {
        $('.loading').remove();
        bo_notify(result.message, "danger");
    }
    else if (result.code === "400"){
        bo_notify(result.message, "danger");
        setTimeout(reset_screen, 1500);
        return false;
    }
    else {
        function asyncEvent() {
            let dfd = jQuery.Deferred();
            let modal_search = $('#modal_search');

            modal_search.on('shown.bs.modal', function (e) {
                dfd.resolve("shown");
            });

            modal_search.modal('show');

            return dfd.promise();
        }

        $.when( asyncEvent() ).then(
            function( status ) {
                initiate_draggable_duration(data.duration, data.event_id);
                $.when(init_all_calendar(result.data[0], result.data[1], data, result.data[2])).then(
                    function( status ) {
                        $('.loading').remove();
                        $('#modal_search').removeClass("hidden_item")
                    },
                    function( status ) {
                        //console.log("fail");
                    });
            },
            function( status ) {
                //console.log("fail");
            }
        );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////// End Core Block /////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////










////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// 6: Init and creation of all the calendar /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function build_resources(business_hour, duration){
    let resource = [];

    for (let i = 0; i < duration.length; i++) {
        let tmp_tab = {};

        tmp_tab.id = duration[i];
        tmp_tab.title = duration[i];
        tmp_tab.eventBackgroundColor = "rgb(0,0,255)";
        tmp_tab.businessHours = [];
        resource.push(tmp_tab);
        for (let j = 0; j < business_hour.length; j++) {
            if (business_hour[j].duration === duration[i] + ':00'){
                resource[i]["businessHours"].push(business_hour[j]);
            }
        }
    }
    return resource;
}


function init_all_calendar(valid_fdc, business_hour, data, best_fits){
    let dfd = jQuery.Deferred();
    let promises = [];

    $(".container_provider").remove();
    $.each(valid_fdc, function(index, value) {
        let fdc_info = index.split("/");

        $(".provider_list").append(create_container(fdc_info));
        $(".container_provider_" + fdc_info[1]).append(create_html(fdc_info));
        $(".container_provider_" + fdc_info[1]).append('<div class="calendar_agenda" id="calendar_agenda_'+ fdc_info[1] +'"></div>');
        promises.push(create_calendar_agenda_provider(fdc_info[1], business_hour[fdc_info[1]], data, best_fits[index]));
    });
    $('.provider_name').on('click', function(){
        let token = $(this).attr('value');

        get_calendar(token, valid_fdc);
    });
    $.when.apply($, promises).then(function() {
        $('.calendar_agenda').addClass("hidden");
        dfd.resolve("all_created");
    }, function() {
        //console.log("fail");
    });
    return dfd.promise();
}


function get_calendar(token, valid_fdc){
    let data = JSON.parse(sessionStorage.getItem('data_customer'));
    data.user_token = token;

    sessionStorage.setItem('data_customer', JSON.stringify(data));
    $('#valid_choice').remove();
    $.each(valid_fdc, function(index, value) {
        let token_tmp = index.split("/")[1];
        if (token != token_tmp){
            $(".container_provider_" + token_tmp + "").toggleClass("hidden");
        }
    });
    for (let i = 1; i <= global_nb_duration; i++){
        $(".calendar_agenda").fullCalendar( 'removeEvents', 'duration_' + i);
    }
    $('.duration').removeClass("hidden");
    $("#calendar_agenda_" + token).toggleClass("hidden");
    $(".frequency_button_" + token).toggleClass("hidden");
    $("#calendar_agenda_" + token).fullCalendar("render");
}


function create_calendar_agenda_provider(token, business_hour, data, best_fits){
    let dfd = jQuery.Deferred();
    let calendar = "#calendar_agenda_" + token;
    let resource = build_resources(business_hour, data.duration.duration);

    if (data.duration.duration.length > 1) {
        $(calendar).hover(
            function () {
                if (typeof global_dragged_id !== 'undefined') {
                    for (let i = 0; i < resource.length; i++) {
                        if (resource[i].id === global_dragged_id) {
                            $(calendar).fullCalendar('option', {
                                businessHours: resource[i].businessHours
                            });
                            break;
                        }
                    }
                }
            }
        );
    }
    $(calendar).fullCalendar({
        height: 'auto',
        columnFormat: 'ddd DD/MM',
        defaultView: 'agendaWeek',
        header: {
            left: '',
            center: '',
            right: ''
        },
        eventOverlap: function(stillEvent, movingEvent) {
            return (stillEvent.type === 1 || stillEvent.type === 2 || stillEvent.rendering === "background");
        },
        droppable: true,
        nowIndicator: true,
        slotDuration: '00:30:00',
        snapDuration: '00:05:00',
        allDaySlot: false,
        slotEventOverlap: false,
        minTime: "08:00",
        maxTime: "22:00",
        businessHours: business_hour,
        eventConstraint: "businessHours",
        selectConstraint: "businessHours",
        events: function(start, end, timezone, callback) {
            let ajax_data = {
                customer_token: data.customer_token,
                address_token: data.address_token,
                token: token,
                type: JSON.stringify(data.frequency.frequencyValue),
                week_start: JSON.stringify(data.date_start),
                week_end: JSON.stringify(moment(data.date_start, "YYYY-MM-DD").add(7, 'days').format('YYYY-MM-DD')),
                start: JSON.stringify(data.date_start),
                end: JSON.stringify(data.date_end),
                business_hours: JSON.stringify(business_hour)
            };

            ajax_call("api/prestation", "GET", ajax_data, function(result){
                for(let i = 0; i < best_fits.length; i++){
                    result.data.push(best_fits[i]);
                }
                callback(result.data);
            });
        },

        eventAfterAllRender: function( view ) {
            $(calendar).on("click",".fc-bgevent",function(event){
                var event = $(this).data();
            });
            dfd.resolve( "create" );
        },

        eventDragStart: function( event, jsEvent, ui, view ) {
            for (let i = 0; i < resource.length; i++) {
                if (resource[i].id === event.resourceId) {
                    $(calendar).fullCalendar('option', {
                        businessHours: resource[i].businessHours
                    });
                }
            }
        },

        eventAllow: function(dropInfo, draggedEvent){
            let allow = 1;
            let start_dragged = JSON.parse(JSON.stringify(dropInfo.start)).split("T");
            $(calendar).fullCalendar( 'clientEvents', function(events){
                let start = JSON.parse(JSON.stringify(events.start)).split("T");
                if (events.edition === "duration_event" && start_dragged[0] === start[0] && events.id != draggedEvent.id) {
                    allow = 0;
                }
            });

            return (allow === 1);
        },


        drop: function( date, jsEvent, ui, resourceId ) {

            let string_class = ui.helper[0].className;
            let tab_class = string_class.split(' ');

            $('.' + tab_class[3] + '').addClass("hidden");
            if (!$(".draggable_duration").not(".hidden").length) {
                $(".provider_list").append(create_validate_button(data.address_token, data.customer_token, data.method));
            }
        },

        eventRender: function (event, element) {
            $(element).css('color', 'rgb(0,0,0');
            $(element).css('padding-left', "5px");
            $(element).css("font-weight", "bold");
            if (event.rendering !== "background")
                $(element).css("opacity", "0.75");
            else {
                $(element).css("opacity", "0.3");
                $(element).data(event);
            }
            if (event.edition === "duration_event"){
                element.find('.fc-content').append( "<span style='color: red; font-size: 10px; float: right; margin-right: 5px;' class='closeon'>X</span>" );
                element.find(".closeon").click(function() {
                    $(calendar).fullCalendar('removeEvents',event.id);
                    $("." + event.id).removeClass('hidden');
                    if (!$(".draggable_duration").has(".hidden").length) {
                        $('#valid_choice').remove();
                    }
                });
            } else {
                element.find('.fc-content').append("" + event.firstname + " " + event.lastname + "<br/>" + event.addresses + "<br/>" + event.transport_icon);
            }
        }
    });

    let date_format = moment(data.date_start, "YYYY-MM-DD");
    $(calendar).fullCalendar('gotoDate', date_format);
    $(calendar).fullCalendar('option', {
    firstDay: date_format.format('d')
    });

    return dfd.promise();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////   End  Init and creation of all the calendar        ////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////   Calendar Dispo       /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function create_dispo_calendar(){
    var dfd = jQuery.Deferred();
    var address_token = sessionStorage.getItem('address_token');
    var user_token = sessionStorage.getItem('user_token');
    $("#calendar_dispo").fullCalendar({
        height: 'auto',
        columnFormat: 'dddd',
        defaultView: 'agendaWeek',
        header: {
            left: '',
            center: '',
            right: ''
        },
        editable: true,
        eventOverlap: false,
        slotDuration: '00:30:00',
        snapDuration: '00:05:00',
        selectable: true,
        slotEventOverlap: false,
        selectHelper: true,
        selectOverlap: false,
        allDaySlot: false,
        minTime: "08:00",
        maxTime: "22:00",
        businessHours: {
            dow: [ 1, 2, 3, 4 , 5, 6, 0], // Monday - Thursday

            start: '08:00', // a start time (10am in this example)
            end: '22:00', // an end time (6pm in this example)
        },
        eventConstraint: "businessHours",
        selectConstraint: "businessHours",
        events: function(start, end, timezone, callback) {
            let ajax_data = {
                user_token: user_token,
                address_token: address_token
            };
            ajax_call("../client/api/client_dispo", "GET", ajax_data, function(result){
                callback(result.data);
            })
        },
        eventAfterAllRender: function( view ) {
            dfd.resolve( "create" );
        },

        eventClick: function (event, jsEvent, view) {
            let start = moment(event.start).format('HH:mm:ss');
            let end = moment(event.end).format('HH:mm:ss');
            $('#modal_delete_dispo_Title').html(start + ' - ' + end);
            $('#modal_delete_dispo_Body').val(event.id);
            $('#modal_delete_dispo_Title').val(user_token);
            $("#modal_dispo").modal('hide');
            $('#modal_delete_dispo').modal('show');
        },

        select: function(start, end, allDay) {
            let select_duration = moment.utc(end.diff(start)).format("HH:mm:ss");
            if (select_duration < "02:00:00") {
                bo_notify("Une disponibilité ne peux pas être inférieure à 2h", "danger");
                $('#calendar_dispo').fullCalendar('unselect');
            }
            else {
                var ajax_data = {
                    start: JSON.stringify(moment(start).format('HH:mm:ss')),
                    end: JSON.stringify(moment(end).format('HH:mm:ss')),
                    day: JSON.stringify(moment(start).day()),
                    token: JSON.stringify(user_token),
                    address_token: JSON.stringify(address_token)
                };
                ajax_call("../client/api/client_dispo", "POST", ajax_data, function(result){
                    if (result.code == 200) {
                        $('#calendar_dispo').fullCalendar('renderEvent',
                            {
                                start: start,
                                id: result.data,
                                end: end,
                                overlap: false
                            },
                            true // make the event "stick"
                        );
                    }
                    bo_notify(result.message, "success");
                    $('#calendar_dispo').fullCalendar('unselect');
                });
            }
        },




        eventDrop: function(event, delta) {
            var ajax_data = {
                start: JSON.stringify(moment(event.start).format('HH:mm:ss')),
                end: JSON.stringify(moment(event.end).format('HH:mm:ss')),
                id: JSON.stringify(event.id),
                day: JSON.stringify(moment(event.start).day())
            };
            ajax_call("../client/api/client_dispo", "PUT", ajax_data, function(){
                bo_notify(result.message, "success");
            });
        },




        eventResize: function(event) {
            var ajax_data = {
                start: JSON.stringify(moment(event.start).format('HH:mm:ss')),
                end: JSON.stringify(moment(event.end).format('HH:mm:ss')),
                id: JSON.stringify(event.id),
                day: JSON.stringify(moment(event.start).day())
            };
            ajax_call("../client/api/client_dispo", "PUT", ajax_data, function(){
                bo_notify(result.message, "success");
            });
        },

        eventRender: function (event, eventElement){
            $(eventElement).css("opacity", "0.70");
        }

    });
    return dfd.promise();
}

$(document).ready(function() {

    $('#close_dispo').on('click', function (e) {
        $('#modal_dispo').modal('show');
    });

    $('#confirm-delete-dispo').on('click', function (e) {
        $('#modal_delete_dispo').modal('hide');
        var data_bootbox = {
            message: "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir supprimer ce créneaux de disponibilité ?</span>",
            callBackTrue: function callBackTrue(data)
            {
                ajax_call("../client/api/client_dispo", "DELETE", data.ajax_data, function(result){
                    if (result.code == 200) {
                        bo_notify(result.message, "success");
                        $('#calendar_dispo').fullCalendar('removeEvents', JSON.parse(data.ajax_data.id));
                        $('#modal_dispo').modal('show')
                    } else {
                        bo_notify(result.message, "danger");
                    }
                });
            },
            callBackFalse:function callBackFalse(data){
                $('#modal_delete_dispo').modal('show');
            },
            data : {
                true: {
                    ajax_data: {
                        id: JSON.stringify($('#modal_delete_dispo_Body').val()),
                        token: JSON.stringify($('#modal_delete_dispo_Title').val())
                    }
                },
                false: {}
            }
        };

        bootbox_confirm(data_bootbox);
    });

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////   End Calendar Dispo       /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////   Bootbox Confirm      /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function bootbox_confirm(data_bootbox){
    bootbox.confirm({
        size: "large",
        message: data_bootbox.message,
        buttons: {
            confirm: {
                label: 'Oui',
                className: 'btn-success'
            },
            cancel: {
                label: 'Non',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result){
                data_bootbox.callBackTrue(data_bootbox.data.true);
            }
            else{
                data_bootbox.callBackFalse(data_bootbox.data.false);
            }
        }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////   End Bootbox Confirm      /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
