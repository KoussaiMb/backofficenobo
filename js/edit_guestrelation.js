////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  10:    createHtml      ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function create_legend(menu){
    $(menu).find('.legend').remove();
    $(menu).prepend("<ul class=\"legend\">\n" +
    "    <li><span class=\"recurent_1\"></span> Récurrent S1</li>\n" +
    "    <li><span class=\"recurent_2\"></span> Récurrent S2</li>\n" +
    "    <li><span class=\"recurent\"></span> Récurrent</li>\n" +
    "    <li><span class=\"ponctuel\"></span> Ponctuel</li>\n" +
    "    </ul>")
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  10:    End createHtml      ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////CALENDAR DISPONIBILITÉ ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function calendar_dispo_guestrelation(token){
    $('#calendar').fullCalendar({
        columnFormat: 'dddd DD MMM',
        defaultView: 'agendaWeek',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: ''
        },
        height: 'auto',
        editable: true,
        slotDuration: '00:30:00',
        snapDuration: '00:15:00',
        selectable: true,
        slotEventOverlap: false,
        selectHelper: true,
        selectOverlap: false,
        allDaySlot: false,
        minTime: "08:00",
        maxTime: "22:00",
        businessHours: {
            dow: [ 1, 2, 3, 4 , 5, 6, 0], // Monday - Thursday

            start: '08:00', // a start time (10am in this example)
            end: '22:00' // an end time (6pm in this example)
        },
        eventConstraint: "businessHours",
        selectConstraint: "businessHours",
        events: [],
        //When click on event
        eventClick: function (event) {
            let start = moment(event.start).format('H:mm');
            let end = moment(event.end).format('H:mm');
            let event_id = event.id;

            bootbox.dialog({
                title: event.title,
                message: '<div>'+
                            `<h3>${moment(event.start).format('LLLL')}</h3>`+
                            `<p>de ${start} à ${end}</p>`+
                            '<h4>Commentaire:</h4>'+
                            `<p>${event.commentary ? event.commentary : 'Aucun commentaire'}</p>`+
                        '</div>',
                buttons: {
                    cancel: {
                        label: "Fermer",
                        callback: function(){

                        }
                    },
                    export: {
                        label: "Exporter",
                        callback: function(){
                            exportEvent(event);
                        }
                    },
                    edit: {
                        label: "Modifier",
                        className: 'btn-warning',
                        callback: function(){
                            if (event.available) {
                                editDispoEvent(event, token);
                            } else {
                                editEvent(event, token);
                            }
                        }
                    },
                    delete: {
                        label: "Supprimer",
                        className: 'btn-danger',
                        callback: function(){
                            if (event.available) {
                                deleteEvent(event);
                            } else {
                                bo_notify("Vous ne pouvez pas supprimer un premier rendez-vous!", "danger");
                            }
                        }
                    }
                }
            });
        },
        //When add a new event
        select: function(start, end) {
            var the_calendar = $('#calendar');
            var select_duration = moment.utc(end.diff(start)).format("HH:mm:ss");

            if (select_duration < "00:30:00") {
                bo_notify("Une disponibilité ne peux pas être inferieur à 30 minutes", "danger");
                the_calendar.fullCalendar('unselect');
                return;
            }

            if (moment().startOf("day").isAfter(start)) {
                bo_notify("Une disponibilité ne peux pas être avant aujourd'hui", "danger");
                the_calendar.fullCalendar('unselect');
                return;
            }

            new Promise((res, err) => {
                bootbox.dialog({
                    title: 'Ajout d\'une nouvelle disponibilité',
                    message: '<div class="form-group">'+
                    '<label for="dispo_recurrence">Sélectionnez la récurrence:</label>' +
                    '<select class="form-control" id="dispo_recurrence">'+
                    '<option value="1">Juste une fois</option>'+
                    '<option value="2">Pendant plusieurs semaines</option>'+
                    '</select>'+
                    '</div>'+
                    '<div class="form-group" id="date_end_group">'+
                    '<label for="dispo_date_end">Fin de la récurrence:</label>' +
                    '<input type="date" id="dispo_date_end" name="dispo_date_end">'+
                    '</div>'+
                    '<div class="form-group">'+
                    '<label for="commentary">Commentaire:</label>' +
                    '<textarea class="form-control" style="resize: vertical;" id="commentary" name="commentary" rows="3"></textarea>'+
                    '</div>',
                    buttons: {
                        cancel: {
                            label: "Annuler",
                            className: 'btn-danger',
                            callback: function(){

                            }
                        },
                        ok: {
                            label: "Ajouter",
                            className: 'btn-success',
                            callback: function(){
                                var recurrence = $('#dispo_recurrence').val();
                                var end_rec = $('#dispo_date_end').val();
                                var commentary = $('#commentary').val() ? $('#commentary').val() : 'Aucun commentaire';

                                if (recurrence === '1') {
                                    res([moment(end).add('1', 'days').format('YYYY-MM-DD'), commentary]);
                                } else if (recurrence === '2') {
                                    res([end_rec ? end_rec : null, commentary]);
                                } else {
                                    err("Aucune récurrence sélectionné");
                                }
                            }
                        }
                    }
                });
            }).then(([end_rec, commentary]) => {
                var ajax_data = {
                    user_token: token,
                    day: moment(start).day(),
                    start: moment(start).format('HH:mm:ss'),
                    end: moment(end).format('HH:mm:ss'),
                    start_rec: moment(start).subtract('1', 'days').format('YYYY-MM-DD'),
                    end_rec: end_rec,
                    commentary: commentary
                };

                ajax_call("api/guestrelation_dispo", "POST", ajax_data, function(result){
                    if (result.code === '200') {
                        result.data.start = start;
                        result.data.end = end;
                        the_calendar.fullCalendar('renderEvent', result.data, true);
                        bo_notify(result.message, "success");
                    }
                    else {
                        bo_notify(result.message, "danger");
                    }
                    the_calendar.fullCalendar('unselect');
                }, function () {
                    ajax_log("api/guestrelation_dispo en POST", "edit_guestrelation.js");
                });
            }).catch(err => {
                bo_notify("Une erreur est survenue", "danger");
            });
        },
        //When moving an event
        eventDrop: function(event, delta, revertFunc) {
            modifyGuestRelationDispo(event, token, revertFunc);
        },
        //When event is displayed
        eventRender: function (event, eventElement){
            $(eventElement).css("text-align", "center");
            $(eventElement).css("opacity", "0.70");
        },
        //When resize an event
        eventResize: function(event, delta, revertFunc) {
            modifyGuestRelationDispo(event, token, revertFunc);
        },
        //When week changed
        viewRender: function(view, element) {
            var ajax_data = {
                user_token: token,
                start: view.start.format('Y-MM-DD'),
                end: view.end.format('Y-MM-DD')
            };

            ajax_call("api/guestrelation_dispo", "GET", ajax_data, function(result){
                if (result.code === '200') {
                    $('#calendar').fullCalendar('removeEvents');
                    result.data.forEach(event => {
                        $('#calendar').fullCalendar('renderEvent', event, true);
                        $('#calendar').fullCalendar('addEventSource', event);
                    });
                    $('#calendar').fullCalendar('refetchEvents');
                }
                else
                bo_notify(result.message, "danger");
            }, function () {
                ajax_log("api/guestrelation_dispo en GET", "edit_guestrelation.js");
            });
        }
    });
}

function modifyGuestRelationDispo(event, token, revertFunc) {

    if (moment().startOf("day").isAfter(event.start)) {
        bo_notify("Un évenement ne peux pas être avant aujourd'hui", "danger");
        $('#calendar').fullCalendar('unselect');
        revertFunc();
        return;
    }

    let ajax_data = {
        id: event.id,
        day: moment(event.start).day(),
        start: moment(event.start).format('HH:mm:ss'),
        end: moment(event.end).format('HH:mm:ss'),
        start_rec: event.start_rec,
        end_rec: event.end_rec,
        commentary: event.commentary ? event.commentary : "Aucun commentaire",
        user_token: token
    };

    ajax_call("api/guestrelation_dispo", "PUT", ajax_data, function(result){
        if (result.code === '200')
            bo_notify(result.message, "success");
        else{
            revertFunc();
            bo_notify(result.message, "danger");
        }
    }, function () {
        ajax_log("api/guestrelation_dispo en PUT", "edit_guestrelation.js");
    });
}

function deleteEvent(event) {
    new Promise((res, err) => {
        bootbox.prompt({
            title: "Confirmez la suppression de ce créneau",
            inputType: 'select',
            inputOptions: [
                {
                    text: 'Jusqu\'à la fin de la récurrence',
                    value: '1',
                },
                {
                    text: 'Uniquement ce jour',
                    value: '2',
                }
            ],
            callback: function (result) {
                if(result)
                    res(result === '1' ? 1 : 0);
            }
        });
    }).then(all => {
        ajax_call(
            "api/guestrelation_dispo",
            "DELETE",
            {
                id: event.id,
                all: all,
                date: moment(event.start).format('YYYY-MM-DD')
            },
            function(result){
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    $('#calendar').fullCalendar("removeEvents", function(e){
                        return e.id === result.data;
                    });
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        bo_notify("Une erreur est survenue", "danger");
    });
}

function editDispoEvent(event, token) {

    new Promise((res, err) => {
        bootbox.dialog({
            title: 'Modification d\'une nouvelle disponibilité',
            message: '<form>'+
                    '<div class="form-group">'+
                        '<label for="dispo_date_start">Debut de la récurrence:</label>' +
                        `<input type="date" id="dispo_date_start" name="dispo_date_start" value="${event.start_rec}">`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="dispo_date_end">Fin de la récurrence:</label>' +
                        `<input type="date" id="dispo_date_end" value="${event.end_rec}"`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="event_day">Jour:</label>' +
                        '<select class="form-control" id="event_day">'+
                            `<option value="1" ${moment(event.start).day() == '1' ? 'selected' : ''}>Lundi</option>`+
                            `<option value="2" ${moment(event.start).day() == '2' ? 'selected' : ''}>Mardi</option>`+
                            `<option value="3" ${moment(event.start).day() == '3' ? 'selected' : ''}>Mercredi</option>`+
                            `<option value="4" ${moment(event.start).day() == '4' ? 'selected' : ''}>Jeudi</option>`+
                            `<option value="5" ${moment(event.start).day() == '5' ? 'selected' : ''}>Vendredi</option>`+
                            `<option value="6" ${moment(event.start).day() == '6' ? 'selected' : ''}>Samedi</option>`+
                            `<option value="0" ${moment(event.start).day() == '0' ? 'selected' : ''}>Dimanche</option>`+
                        '</select>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="event_start">Heure de début:</label>' +
                        `<input type="time" id="event_start" value="${moment(event.start).format('HH:mm')}"`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="event_end">Heure de fin:</label>' +
                        `<input type="time" id="event_end" value="${moment(event.end).format('HH:mm')}"`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="commentary">Commentaire:</label>' +
                        `<textarea class="form-control" style="resize: vertical;" id="commentary" name="commentary" rows="3" value="${event.commentary}">${event.commentary}</textarea>`+
                    '</div>'+
                    '</form>',
            buttons: {
                cancel: {
                    label: "Annuler",
                    className: 'btn-danger',
                    callback: function(){

                    }
                },
                ok: {
                    label: "Modifier",
                    className: 'btn-success',
                    callback: function(){
                        var start_rec = $('#dispo_date_start').val();
                        var end_rec = $('#dispo_date_end').val();
                        var event_day = $('#event_day').val();
                        var event_start = $('#event_start').val();
                        var event_end = $('#event_end').val();  
                        var commentary = $('#commentary').val() ? $('#commentary').val() : 'Aucun commentaire';

                        if (start_rec && commentary &&
                            event_day && event_start && event_end) {
                            //check correct date
                            if (moment(end_rec).isBefore(start_rec) || moment(end_rec).isSame(start_rec))
                                err("La fin de récurrence ne peut pas être avant le début de récurrence");
                            else if (moment(end_rec + " " + event_end + ":00").isBefore(start_rec + " " + event_start + ":00") 
                            || moment(end_rec + " " + event_end + ":00").isSame(start_rec + " " + event_start + ":00"))
                                err("La fin de récurrence ne peut pas être avant le début de récurrence");
                            else if (moment().isAfter(start_rec + " " + event_start + ":00"))
                                err("Une disponibilité ne peux pas être avant aujourd'hui");
                            else {
                                var event_date = moment(start_rec).startOf("week").startOf("day").add(event_day - 1, 'days');
                            
                                res([start_rec, end_rec, event_date, event_start, event_end, commentary]);
                            }
                        } else {
                            err("Les informations sont incorrectes");
                        }
                    }
                }
            }
        });
    }).then(([start_rec, end_rec, event_date, event_start, event_end, commentary]) => {
        let ajax_data = {
            id: event.id,
            day: moment(event_date).day(),
            start: event_start + ":00",
            end: event_end + ":00",
            start_rec: start_rec,
            end_rec: end_rec,
            commentary: commentary,
            user_token: token
        };

        console.log(ajax_data);

        ajax_call("api/guestrelation_dispo", "PUT", ajax_data, function(result){
            if (result.code === '200') {
                bo_notify(result.message, "success");
                event.start_rec = start_rec;
                event.end_rec = end_rec;
                event.commentary = commentary;
                event.start = moment(event.start).format('YYYY-MM-DD ') + event_start;
                event.end = moment(event.end).format('YYYY-MM-DD ') + event_end;
                event.day = ajax_data.day;
                $('#calendar').fullCalendar("updateEvent", event);
            } else {
                bo_notify(result.message, "danger");
            }
        }, function () {
            ajax_log("api/guestrelation_dispo en PUT", "edit_guestrelation.js");
        });
    }).catch(err => {
        bo_notify(err, "danger");
    });
}

function editEvent(event, token) {

    new Promise((res, err) => {
        bootbox.dialog({
            title: 'Modification d\'une nouvelle disponibilité',
            message: '<div class="form-group">'+
                        '<label for="event_date">Date:</label>' +
                        `<input type="date" id="event_date" name="event_date" value="${moment(event.start).format('YYYY-MM-DD')}">`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="event_start">Heure de début:</label>' +
                        `<input type="time" id="event_start" value="${moment(event.start).format('HH:mm')}"`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="event_end">Heure de fin:</label>' +
                        `<input type="time" id="event_end" value="${moment(event.end).format('HH:mm')}"`+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="commentary">Commentaire:</label>' +
                        `<textarea class="form-control" style="resize: vertical;" id="commentary" name="commentary" rows="3" value="${event.commentary}">${event.commentary}</textarea>`+
                    '</div>',
            buttons: {
                cancel: {
                    label: "Annuler",
                    className: 'btn-danger',
                    callback: function(){

                    }
                },
                ok: {
                    label: "Modifier",
                    className: 'btn-success',
                    callback: function(){
                        var event_date = $('#event_date').val();
                        var event_start = $('#event_start').val();
                        var event_end = $('#event_end').val();                        
                        var commentary = $('#commentary').val() ? $('#commentary').val() : 'Aucun commentaire';

                        if (moment(event_date + " " + event_end + ":00").isBefore(event_date + " " + event_start + ":00")) 
                            err("L'heure de fin ne peut pas être avant l'heure de début");
                        else if (moment().isAfter(event_date + " " + event_start + ":00"))
                            err("Un rendez-vous ne peux pas être avant aujourd'hui");
                        else if (event_date && event_start && event_end && commentary) 
                            res([event_date, event_start, event_end, commentary]);
                        else
                            err("Les informations sont incorrectes");
                        
                    }
                }
            }
        });
    }).then(([event_date, event_start, event_end, commentary]) => {
        let ajax_data = {
            id: event.id,
            day: moment(event_date).day(),
            start: event_start + ":00",
            end: event_end + ":00",
            start_rec: moment(event_date).subtract('1', 'days').format('YYYY-MM-DD'),
            end_rec: moment(event_date).add('1', 'days').format('YYYY-MM-DD'),
            commentary: commentary,
            user_token: token
        };

        ajax_call("api/guestrelation_dispo", "PUT", ajax_data, function(result){
            if (result.code === '200') {
                bo_notify(result.message, "success");
                event.start_rec = ajax_data.start_rec;
                event.end_rec = ajax_data.end_rec;
                event.commentary = commentary;
                event.start = moment(event_date).format('YYYY-MM-DD ') + event_start;
                event.end = moment(event_date).format('YYYY-MM-DD ') + event_end;
                event.day = ajax_data.day;
                $('#calendar').fullCalendar("updateEvent", event);
            } else {
                bo_notify(result.message, "danger");
            }
        }, function () {
            ajax_log("api/guestrelation_dispo en PUT", "edit_guestrelation.js");
        });
    }).catch(err => {
        bo_notify(err, "danger");
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// FIN CALENDAR DISPONIBILITÉ PROVIDER //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
