//Permet d'ajouter un addon devant un input (fonctionne avec les number custome)
jQuery(document).ready(function() {
    $('.add-prev').each(function () {
        $(this).prev().find('input').before("<div class='input-group-addon alpha-addon'>" +$(this).text()+ "</div>");
    });
    $('.pickatime.start').timepicker({
        'timeFormat': 'H:i',
        'step' : 30
    }).on('change', function () {
        if ($(this).val() > $(this).nextAll('input.end:first').val())
            $(this).nextAll('input').val($(this).val());
    });

    $('.pickatime.end').timepicker({
        'timeFormat': 'H:i',
        'step' : 30,
        minTime: '00:30',
        'noneOption' : [{'label': '24:00', 'value': '24:00'}]
    }).on('change', function () {
        if ($(this).val() <= $(this).prevAll('input.start:first').val())
            $(this).prevAll('input').val($(this).val());
    });
});
//Cherche tous les subMenu à la recherche d'un match avec subMenu
function isTextInSubMenu(selectInput, subMenu) {
    var is_found = selectInput.find('div[data-href=' +subMenu+ ']');

    return typeof is_found !== 'undefined';
}
/*Affiche de manière relative les sous-menus, nécessite l'architecture suivante:
 *
 * div[class=subMenu]
 *       div[class=select-side][data-href=toto]
 *       div[class=select-side][data-href=tata]
 * div[class=content-side]
 * div[class=content-side]
 * div[class=content-side]
 *
 */
function showSubMenu(subMenuObject) {
    subMenuObject.addClass('current').siblings().removeClass('current');
    subMenuObject.parent().siblings().hide();
    $('#' +subMenuObject.data('href')).show();
}

function ajax_log(error, file, func){
    let fn = func === undefined ? "ajax_call" : func;

    return $.ajax({
        url: '/api/log',
        data: {
            error : error,
            function : fn,
            file : file
        },
        dataType: 'json',
        type: 'POST',
        success: function(ret) {
            bo_notify(ret.message, ret.code === '200' ? 'info' : 'danger');
        },
        error: function () {
            bo_notify("An error system occured, call an admin", 'danger')
        }
    });
}

function ajax_call(url, type, data, SuccessCallBack, errorCallBack){
    SuccessCallBack = (typeof SuccessCallBack !== 'undefined') ? SuccessCallBack : false;

    if (typeof errorCallBack !== 'undefined')
        return $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: type,
            success: SuccessCallBack,
            error: errorCallBack
        });
    else
        return $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            type: type,
            success: SuccessCallBack,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.url);
                console.log(thrownError);
            }
        })
}

function ajax_call_html(url, type, data, SuccessCallBack, errorCallBack){
    SuccessCallBack = (typeof SuccessCallBack !== 'undefined') ? SuccessCallBack : false;

    if (typeof errorCallBack !== 'undefined')
        return $.ajax({
            url: url,
            data: data,
            dataType: 'html',
            type: type,
            success: SuccessCallBack,
            error: errorCallBack
        });
    else
        return $.ajax({
            url: url,
            data: data,
            dataType: 'html',
            type: type,
            success: SuccessCallBack,
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(xhr.status);
                console.log(xhr.url);
                console.log(thrownError);
            }
        })
}

function addTimes(start, end) {
    var times = [],
        times1 = start.split(':'),
        times2 = end.split(':');

    for (var i = 0; i < 3; i++) {
        times1[i] = (isNaN(parseInt(times1[i]))) ? 0 : parseInt(times1[i]);
        times2[i] = (isNaN(parseInt(times2[i]))) ? 0 : parseInt(times2[i]);
        times[i] = times1[i] + times2[i];
    }

    var seconds = times[2];
    var minutes = times[1];
    var hours = times[0];

    if (seconds >= 60) {
        minutes += Math.floor(seconds / 60);
        seconds = seconds % 60;
    }

    if (minutes >= 60) {
        hours += Math.floor(minutes / 60);
        minutes = minutes % 60;
    }

    if (hours <= 9)
        hours = "0" + hours;
    if (minutes <= 9)
        minutes = "0" + minutes;
    if (seconds <= 9)
        seconds = "0" + seconds;

    return hours + ':' + minutes + ':' + seconds;
}

function timeToTime(time) {
    if(time[0] == 0)
        return time.substring(1, 2) + "h" + time.substring(3, 5);
    return time.substring(0, 2) + "h" + time.substring(3, 5);
}

function redirect_with_params(url, type, data)
{
    $form = $(`<form method='${type}' action='${url}'></form>`);
    for (var name in data) {
      $form.append(`<input type="hidden" name="${name}" value="${data[name]}">`);
    }
    $('body').append($form);
    $form.submit();
}

function bootbox_confirm(data_bootbox){
    bootbox.confirm({
        message: data_bootbox.message,
        buttons: {
            confirm: {
                label: 'Oui',
                className: 'btn-success'
            },
            cancel: {
                label: 'Non',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result){
                data_bootbox.callBackTrue(data_bootbox.data.true);
            }
            else{
                data_bootbox.callBackFalse(data_bootbox.data.false);
            }
        }
    });
}

Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) === -1;
    });
};