for (var i = new Date().getFullYear(); i > 2015; i--){
    $('#yearpicker').append($('<option />').val(i).html(i));
}

$('#tax_credit_btn').on('click', function(){

    var action = 'download';
    var u_token = document.getElementById('tax_user_token').value;
    var company = document.getElementById('tax_company_id').value;

    var elem_select = document.getElementById('yearpicker');
    var year = elem_select.options[elem_select.selectedIndex].text;

    display_tax(u_token, action, company, year);
});

function display_tax(token, action, company, year)
{
    $.ajax({
        url: '../taxation/api/tax.php',
        type: 'GET',
        data: {
            user_token : token,
            action : action,
            company : company,
            year : year
        },
        success: function (json) {
            var res = JSON.parse(json);
            if(res.code == 200 && res.data){
                taxCreditGenerate(res.data);
            }
        }
    });
}

/*
    Redirects all the data gathered to be formatted on the view file and then send to the TaxCreditGenerator
 */
function taxCreditGenerate(data)
{
    $form = $("<form method='POST' action='../taxation/view' target='_blank'></form>");
    for(var name in data){
        $form.append('<input type="hidden" name="' + name + '" value="' + data[name] + '">');
    }
    $('#hidden_form_container').append($form);
    $form.submit();
}