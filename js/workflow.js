jQuery(document).ready(function() {
    var stepContent = $("#step-content");
    if (stepContent) {
        var stepHeight = parseInt(stepContent.css('height'), 10);
        $('.action-history').css('height', (stepHeight - 146) + "px");
    }

    //workflow-macro
    var step_selected = null;
    var workflow_selected = null;

    $('.step-circle').on("click", function(){
        if (step_selected)
                step_selected.removeClass("active");

        if (!step_selected || step_selected.data("val") != $(this).data("val")) {
            step_selected = $(this);
            workflow_selected = null;
            $('.btn-filter').removeClass("filter-disabled");
            step_selected.addClass("active");
            $(".customer-thumb").each(function(){
                if ($(this).parent().data("step") == step_selected.data("val")) {
                    $(this).parent().show();
                } else {
                    $(this).parent().hide();
                }
            });
            $(".check-filter input").trigger("change");
        } else {
            step_selected = null;
            $(".customer-thumb").each(function(){
                $(this).parent().show();
            });
            $(".check-filter input").trigger("change");
        }
    });

    $('.btn-filter').on("click", function(){

        if (!workflow_selected || workflow_selected.data("workflow") != $(this).data("workflow")) {
            workflow_selected = $(this);
            step_selected = null;
            $('.btn-filter').addClass("filter-disabled");
            $('.step-circle').removeClass("active");
            workflow_selected.removeClass("filter-disabled");
            $(".customer-thumb").parent().hide();
            $("."+workflow_selected.data("workflow")).show();
            $(".check-filter input").trigger("change");
        } else {
            workflow_selected = null;
            $('.btn-filter').removeClass("filter-disabled");
            $(".customer-thumb").parent().show();
            $(".check-filter input").trigger("change");
        }
    });

    $(document).on("click", ".info-thumb", function(){
        var w_id = $(this).data('val');
        var step = $(this).parent().parent().data('step');
        var url = $(this).parent().data("url");

        if (w_id) 
            displayCustomerInfo(w_id, step, url, $(this).parent().parent().hasClass("alerte"));
    });

    $(document).on("click", ".customer-thumb", function(e){
        if (!$(e.target).hasClass("info-thumb")) {

            if ($(this).data("pending") == 1) {
                acceptNewClient($(this).data("val"), function(){
                    $('#order_id').trigger("change");
                });
            } else {
                redirect_with_params(
                    $(this).data("url"),
                    "POST",
                    {
                        w_id: $(this).data("val"),
                        step: $(this).parent().data("step")
                    }
                );
            }  
        }
    });

    $(".check-filter input").on("change", function(e){
        var filter_id = $(this).prop("id");
        var checked = $(this).prop('checked');

        if (!filter_id)
            return;

        $("."+filter_id).each(function(){
            if (checked && (!step_selected || $(this).data("step") == step_selected.data("val"))
                && (!workflow_selected || $(this).hasClass(workflow_selected.data("workflow")))) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });

    $('#order_id').on("change", function(){
        ajax_call(
            "api/workflow",
            "GET",
            {
                filter: $(this).val()
            },
            function(result){
                if (result.code == 200) {
                    $("#customers").empty();
                    result.data.forEach(customer_info => {
                        addCustomerThumbnail(customer_info);
                    });
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).trigger("change");
});

/**
 * Refresh step content
 * @param {string} step_url
 * @param {int} step 
 * @param {int} w_id : wainting list id
 */
function loadStep(step_url, step, w_id){
    $("#step-content").empty();
    $("#step-content").load(step_url, {w_id: w_id, step: step}, function(response, status, xhr) {
        if (status == "error") {
            bo_notify('impossible de charger la page', 'danger');
        }
    });
}

/**
 * Go to previous or next step
 * @param {int} dir: +1 / -1 
 */
function changeStep(dir){
    var step = Math.max(0, Number($('#step').val()) + dir);
    var w_id = Number($('#w_id').val());
    var ajax_data = {
        step: step,
        w_id: w_id,
        wf_name: 'reception'
    };

    if (step == 0) {
        sendActionMessage();
        $(location).attr('href', '/v1/workflow-macro/view');
        return;
    }

    ajax_call("api/workflow", "PUT", ajax_data, function(result){
        if (result.code == 200){
            if (!result.data.step_info) {
                sendActionMessage();
                $(location).attr('href', '/v1/workflow-macro/view');
            } else {
                $('#step').val(result.data.step_info.step);
                $('#w_step').html("N°" + result.data.step_info.step);
                loadStep(result.data.step_info.url, result.data.step_info.step, w_id);
                reloadHeader(result.data.customer_info);
            }
        } else {
            bo_notify(result.message, 'danger');
        }
    });

    var call_duration = $("#call_duration").html();

    if (call_duration) {
        ajax_call(
            "/v1/reception/api/call_duration",
            "PUT",
            {
                c_id: $("#c_id").val(),
                duration: $("#call_duration").html()
            },
            function (result) {
                if (result.code != 200) {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }
}

/**
 * Refresh workflow header
 * @param {object} datas:
 *  -> firstname
 *  -> lastname
 *  -> phone
 *  -> address
 *  -> email
 *  -> zipcode
 *  -> city
 *  -> hasCard
 *  -> date_list_in          
 */
function reloadHeader (datas) {
    $('#customer_infos').empty();
    $('#customer_infos').append(
        '<div class="col-sm-2">'+
            `<p><span class='glyphicon glyphicon-user'></span> ${datas.firstname} ${datas.lastname}</p>`+
            `<p><span class='glyphicon glyphicon-earphone'></span> ${datas.phone}</p>`+
        '</div>'
    );
    $('#customer_infos').append(
        '<div class="col-sm-6">'+
            '<span class="col-sm-6">'+
                `<p><span class='glyphicon glyphicon-home'></span> ${datas.address}</p>`+
                `<p><span class=' glyphicon glyphicon-envelope'></span> ${datas.email}</p>`+
            '</span>'+
            '<span class="col-sm-6">'+
                `<p><span class='glyphicon glyphicon-map-marker'></span> ${datas.zipcode} ${datas.city}</p>`+
            '</span>'+
        '</div>'
    );
    $('#customer_infos').append(
        '<div class="col-sm-4">'+
            '<p>'+
                "<span class='glyphicon glyphicon-euro'></span>"+
                (datas.hasCard ? "Carte de paiement liée" : "Pas de carte de paiement") +
            '</p>'+
            "<p><span class='glyphicon glyphicon-calendar'></span>"+
                datas.date_list_in +
            '</p>'+
        '</div>'
    );
}

/**
 * Display a modal with all customer info and action history 
 * @param {int} w_list_id 
 * @param {int} step 
 * @param {string} url 
 * @param {bool} alerte 
 */
function displayCustomerInfo(w_list_id, step, url, alerte) {
    new Promise((res, err) => {
        ajax_call(
            "api/customer_info",
            "GET",
            {
                w_list_id: w_list_id
            },
            function(result) {
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    }).then(customer_info => {
        var history = '<div style="display: block;overflow-y: auto;max-height: 280px">';

        customer_info.actionHistory.forEach(element => {
            var history_date = moment(element.order_date).format("DD/MM/YYYY HH:mm");

            if (element.request_date) {
                history_date = element.request_time ? moment(element.request_date + " " + element.request_time).format("DD/MM/YYYY HH:mm") : moment(element.request_date).format("DD/MM/YYYY");
            }

            history+=(
                `<div>
                    <p>
                        <b>${element.request_date ? element.call_request_name + " / " : ""}${element.action_name} : ${history_date}</b>
                    </p>
                    <p>
                        ${element.message}
                    </p>
                </div>
                <hr style="margin: 0px; background-color: lightgrey; height: 1px; border: 0;"/>`
            );
        });

        bootbox.dialog({
            title: customer_info.firstname + " " + customer_info.lastname,
            message: 
                    `<div class="row"><div class="col-sm-6">`+
                        `<h4>Informations du client:</h4>`+
                        `<p><b>Nom</b>: ${customer_info.lastname}</p>`+
                        `<p><b>Prénom</b>: ${customer_info.firstname}</p>`+
                        `<p><b>Adresse</b>: ${customer_info.address}</p>`+
                        `<p><b>Ville</b>: ${customer_info.city}</p>`+
                        `<p><b>Code postal</b>: ${customer_info.zipcode}</p>`+
                        `<p><b>Surface</b>: ${customer_info.surface}</p>`+
                        `<p><b>Date d'entrée</b>: ${customer_info.date_list_in}</p>`+
                        `<p><b>Récurrence</b>: ${customer_info.recurrence}</p>`+
                        `<p><b>Nombre d'heure par prestation</b>: ${customer_info.nbHours}</p>`+
                        `<p><b>Source d'acquisition</b>: ${customer_info.acquisition ? customer_info.acquisition : "Indéfinie"}</p>`+
                        `<p><b>Durée d'appel</b>: ${customer_info.call_duration}</p>`+
                    `</div>`+
                    `<div class="col-sm-6">`+
                        `<h4>Historique:</h4>`+
                        history+
                    `</div></div>`,
            buttons: {
                close: {
                    label: "Fermer",
                    className: 'btn',
                    callback: function(){
                    }
                },
                alerte: {
                    label: alerte ? "Enlever des alertes" : "Ajouter aux alertes",
                    className: 'btn btn-danger',
                    callback: function(){
                        ajax_call(
                            "api/workflow",
                            "PUT",
                            {
                                w_id: w_list_id,
                                alerte: alerte ? 0 : 1 
                            },
                            function(result){
                                if (result.code == 200) {
                                    bo_notify(result.message, "success");
                                    $('#order_id').trigger("change");
                                } else {
                                    bo_notify(result.message, "danger");
                                }
                            }
                        )
                    }
                },
                ok: {
                    label: "Reprendre",
                    className: 'btn-success',
                    callback: function(){
                        redirect_with_params(
                            url,
                            "POST",
                            {
                                w_id: w_list_id,
                                step: step
                            }
                        );
                    }
                }
            }
        });
    }).catch(err => {
        bo_notify(err, "danger");
    })
}

/**
 * Add a customer thumbnail in customer list on workflow-macro page
 * @param {object} infos 
 *  -> id
 *  -> call_request {int}
 *  -> alerte {bool}
 *  -> workflow_name
 *  -> step
 *  -> firstname
 *  -> lastname
 *  -> date_list_in
 *  -> city
 *  -> zipcode
 *  -> last_call
 *  -> recurrence
 *  -> pending
 *  -> surface
 */
function addCustomerThumbnail(infos) {
    $("#customers").append(
        `<div class="col-sm-3 ${infos.call_request ? "call_request_" + infos.call_request : ""} ${infos.alerte == 1 ? "alerte" : ""} ${infos.workflow_name ? infos.workflow_name : "reception "} ${infos.pending == 1  ? "pending" : " "}" style="padding: 0" data-step="${infos.step ? infos.step : 1}">
            <div class="customer-thumb row" data-val="${infos.id}" data-url="/v1/${infos.workflow_name ? infos.workflow_name : "reception"}/view" data-pending="${infos.pending ? infos.pending : 0}">
                <div class="col-sm-6">
                    <p>
                        <span class="around-margin glyphicon glyphicon-user"></span>
                        ${infos.firstname} ${infos.lastname}
                    </p>
                    <p>
                        <span class="around-margin glyphicon glyphicon-calendar"></span>
                        ${infos.date_list_in ? moment(infos.date_list_in).format('LLL') : "En attente"}
                    </p>
                </div>
                <div class="col-sm-6">
                    <p>
                        <span class="around-margin glyphicon glyphicon-map-marker"></span>
                        ${infos.city ? infos.city : "Inconnue"} ${infos.zipcode ? infos.zipcode : ""}
                    </p>
                    <p>
                        <span class="around-margin glyphicon glyphicon-earphone"></span>
                        ${infos.call_request_time ? moment(infos.call_request_time ).format('LLL') :(
                        infos.last_call ? moment(infos.last_call).format('LLL') : "Aucun appel")}
                    </p>
                </div>
                <div class="col-sm-12">
                    <p>
                        <span class="around-margin glyphicon glyphicon-list"></span>
                        ${infos.recurrence ? infos.recurrence : "Pas de récurrence"}
                    </p>
                </div>
                <div class="step-thumb">${infos.step ? infos.step : 1}</div>
                <div class="info-thumb" data-val="${infos.id}">?</div>
                <div class="surface-thumb">${infos.surface ? infos.surface : 0}</div>
            </div>
        </div>`
    );
}
