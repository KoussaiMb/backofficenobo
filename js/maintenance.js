function addMpUnit(mp_template_id) {
    return ajax_call("api/mpUnit", "POST", {mp_template_id: mp_template_id}, null, function () {
        ajax_log('api/mpUnit en POST', 'maintenance.js', 'addMpUnit')
    });
}

function deleteMpUnit(mp_unit_id) {
    return ajax_call("api/mpUnit", "DELETE", {mp_unit_id: mp_unit_id}, null, function () {
        ajax_log('api/mpUnit en DELETE', 'maintenance.js', 'deleteMpUnit')
    });
}

function editMpUnit(mp_unit_id, l_maintenanceType_id) {
    return ajax_call("api/mpUnit", "PUT",
        {
            mp_unit_id: mp_unit_id,
            l_maintenanceType_id: l_maintenanceType_id
        },
        null, function () {
            ajax_log('api/mpUnit en PUT', 'maintenance.js', 'putMpUnit')
        });
}

function addMpUnitTask(mp_unit_id, l_rollingTask_id) {
    return ajax_call("api/mpUnitTask", "POST",
        {
            mp_unit_id: mp_unit_id,
            l_rollingTask_id: l_rollingTask_id
        }, null, function () {
            ajax_log('api/mpUnitTask en POST', 'maintenance.js', 'addMpUnitTask')
        });
}

function deleteMpUnitTask(mp_unit_task_id) {
    return ajax_call("api/mpUnitTask", "DELETE", {mp_unit_task_id: mp_unit_task_id}, null, function () {
        ajax_log('api/mpUnitTask en DELETE', 'maintenance.js', 'deleteMpUnitTask')
    });
}

function editMpTemplate(data) {
    return ajax_call("api/mpTemplate", "PUT", data, null, function () {
        ajax_log('api/mpTemplate en PUT', 'maintenance.js', 'editMpTemplate')
    });
}

function deleteMpTemplate(mp_template_id) {
    return ajax_call("api/mpTemplate", "PUT",
        {
            mp_template_id: mp_template_id

        }, null, function () {
        ajax_log('api/mpTemplate en DELETE', 'maintenance.js', 'deleteMpTemplate')
    });
}

function addNobleMaterial(data) {
    return ajax_call(
        "/v1/client/api/noble_materials",
        "POST",
        data,
        null
    );
}

function deleteNobleMaterial(mp_noble_materials_id) {
    return ajax_call(
        "/v1/client/api/noble_materials",
        "DELETE",
        {mp_noble_materials_id: mp_noble_materials_id},
        null,
        function () {
            ajax_log('/v1/client/api/noble_materials en DELETE', 'cardex.js', 'deleteNobleMaterial')
        }
    );
}

function createNobleMaterial(pl_id) {
    let nm = $('#l_noble_materials_id option:selected'),
        room = $('#nm_lr_room_id option:selected');
    let noble_material = nm.text(),
        lr_url = room.data('url'),
        lr_value = room.text(),
        comment = $("#nm_comment").val();

    return "<div class=\"col-xs-12 noble-materials-view\" data-id=\""+pl_id+"\">"
        + "<div class=\"col-xs-4\">"
        + "<p>"+noble_material+"</p>"
        + "</div>"
        + "<div class=\"col-xs-4\">"
        + "<img class=\"pl-img\" src=\""+lr_url+"\">"
        + "<p>"+lr_value+"</p>"
        + "</div>"
        + "<div class=\"col-xs-4\">"
        + "<p class=\"noMarge\">"+comment+"</p>"
        + "<span class=\"pl-delete-icon glyphicon glyphicon-trash\"></span>"
        + "</div>"
        + "</div>";
}

function addMpAddress(data) {
    return ajax_call("api/maintenance_planning", "POST", data, null, function () {
            ajax_log('api/maintenance_planning en POST', 'maintenance.js', 'addMpAddress')
        });
}

function editMpAddress(data) {
    return ajax_call("api/maintenance_planning", "PUT", data, null, function () {
            ajax_log('api/maintenance_planning en PUT', 'maintenance.js', 'editMpAddress')
        });
}

function createRollingTaskClient(rollingTask_id, rollingTask) {
    return "<div class=\"mp-task-client\" data-id=\""+rollingTask_id+"\">"
        + "<p>"
        + rollingTask
        + "<span class=\"btn-link pull-right delete-task-client\">Supprimer</span>"
        + "</p>"
        + "</div>";
}

function addAddressRollingTask(data) {
    return ajax_call("api/mp_address_task", "POST", data, null, function () {
        ajax_log('api/mp_address_task en POST', 'maintenance.js', 'addAddressRollingTask')
    });
}

function deleteAddressRollingTask(data) {
    return ajax_call("api/mp_address_task", "DELETE", data, null, function () {
        ajax_log('api/mp_address_task en DELETE', 'maintenance.js', 'deleteAddressRollingTask')
    });
}