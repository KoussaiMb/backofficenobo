/**
 *  App
 */

function rand() {
    return Math.random().toString(36).substr(2);
}

function parseDate(input) {
    var parts = input.match(/(\d+)/g); // parse a date in yyyy-mm-dd format
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
}

/**
 *  Time functions
 */

function addTime(time1, time2) {
    var date1 = new Date("7/13/2010 " + time1);
    var date2 = new Date("7/13/2010 " + time2);
    var dateRef = new Date("7/13/2010");
    var t = diffDate(dateRef, date1) + diffDate(dateRef, date2);
    return msToTime(t);
}

function subTime(time1, time2) {
    var date1 = new Date("7/13/2010 " + time1);
    var date2 = new Date("7/13/2010 " + time2);
    var dateRef = new Date("7/13/2010");
    var t = diffDate(dateRef, date1) - diffDate(dateRef, date2);
    return msToTime(t);
}

function multiplyTime(i, time) {
    var date = new Date("7/13/2010 " + time);
    var dateRef = new Date("7/13/2010");
    var t = i * diffDate(dateRef, date);
    return msToTime(t);
}

function diffDate(date1, date2){
    return Math.abs(date1.getTime() - date2.getTime());
}

function msToTime(ms){
    var h = Math.floor(ms/(1000 * 3600));
    var m = Math.floor(ms/(1000 * 60) % 60);
    return ((h.toString().length == 1 ? '0' + h : h) + ':' + (m.toString().length == 1 ? '0' + m : m));
}

function mysqlDatetime_to_date(date) {
    var res = "";

    res += date.substr(8, 2);
    res += "/";
    res += date.substr(5, 2);
    res += "/";
    res += date.substr(0, 4);
    res += " ";
    res += date.substr(11, 5);

    return res;
}

/**
 *  Notifications
 */

function bo_notify(message, type) {
    $.notify({
        // options
        icon: 'glyphicon glyphicon-warning-sign',
        message: message
    }, {
        // settings
        element: 'body',
        type: type,
        allow_dismiss: true,
        newest_on_top: true,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        //offset: 200,
        //spacing: 10,
        z_index: 1031,
        delay: 1000,
        timer: 2800,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}

/**
 *  Google API functions
 */

function LoadGoogleMap(map_id, Lat, Lng) {
    var myCenter = new google.maps.LatLng(Lat, Lng);
    var mapCanvas = document.getElementById(map_id);
    var mapOptions = {center: myCenter, zoom: 15};
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({position:myCenter});
    marker.setMap(map);
}
function initGoogleAutocomplete_addr(input_id) {
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode'],  componentRestrictions: {country: 'fr'}});
    autocomplete.addListener('place_changed', fillInAddr_id);
}
function initGoogleAutocomplete_home(input_id) {
    autocomplete_home = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode'], componentRestrictions: {country: 'fr'}});
    autocomplete_home.addListener('place_changed', fillInAddr_home);
}

/*function initGoogleAutocomplete(input_id) {
    var id = input_id.substr(input_id.indexOf('_') + 1);
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById(input_id)),
        {types: ['geocode']});
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component+ '_' + id).value = '';
        }
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType + '_' + id).value = val;
            }
        }
        $('#lat_' + id).val(place.geometry.location.lat());
        $('#lng_' + id).val(place.geometry.location.lng());
        $('#Address_' + id).val($('#street_number_' + id).val() + ' ' + $('#route_' + id).val());
    })
}*/
function Googlegeolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}
/**
 * AJAX
 */

function ajax_call_v2(url, type, data, successFunc, errorMessage){
    return $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: type,
        success: successFunc,
        error: function (a, b, c) {
            console.log(b);
            console.log(a);
            console.log(c);

            bo_notify(typeof errorMessage === 'undefined' ? 'Un problème est survenu, contactez un admin' : errorMessage, 'danger');
        }
    });
}

/**
 *  Form Functions
 */

function changeBtn(submitBtn, buttonValue, buttonText, buttonType) {
    submitBtn.val(buttonValue);
    submitBtn.text(buttonText);
    submitBtn.removeClass().addClass('btn btn-' + buttonType)
}

function fillForm(form, data) {
    let keys = Object.keys(data);
    let len = keys.length;

    for (let i = 0; i < len; i++)
        form.find('[name=' +keys[i]+ ']').val(data[keys[i]]);
}

function lockForm(form, selection_to_lock) {
    var str = arrayToNameInputs(selection_to_lock);
    form
        .find(':input')
        .prop('disabled', false)
        .end()
        .find(str)
        .prop('disabled', true);
}

function arrayToNameInputs(array) {
    var i = -1;
    var str = "";

    while (array[++i])
        str += '[name=' + array[i] + '], ';
    if (i > 0)
        return str.substring(0, str.length - 2);
    else
        return str;
}

function propInputs(inputs, propMethod, value)
{
    inputs.prop(propMethod, value);
}
