/* Colors used for scores */
var colors = ["red", "orange", "yellow", "lightgreen", "green"];

/* Calls to change position of a question */
function movePositionAjaxCall(move_id, action)
{
    $.ajax({
        url: 'api/question',
        type: "POST",
        data: {
            question_id : move_id,
            action : action
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200 && res.data){

                var div_move_id = '#div_edit_question_' + res.data.move.id;
                var div_switch_id = '#div_edit_question_' + res.data.switch.id;

                if(action == 'up'){
                    $(div_switch_id).insertBefore($(div_switch_id).prev());
                }else if(action == 'down'){
                    $(div_move_id).insertBefore($(div_move_id).prev());
                }
            }
        },
        error: function () {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
}


/*
 *  All function user to build modal elements
 */
function buildModal(title, msg, buttons)
{
    return bootbox.dialog({
        size : "large",
        closeButton: false,
        backdrop: true,
        title: title,
        message: msg,
        buttons: buttons
    });
}

function buildCancelButton()
{
    return  {
        label: "Annuler",
        className: 'btn-warning',
        callback: function(){
            choice_nb = 1;
        }
    }
}

function buildValidateButton()
{
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var input_val = $('#select_input_id').find('option:selected').text();
            if(input_val == 'select'){
                var prompt_answer = confirm("Voulez-vous ajouter un champs \"Autre\" à la question ?");
                if(prompt_answer){
                    document.getElementById('other_field_bool').value = 1;
                }
            }
            document.getElementById('form_new_question').submit();
            choice_nb = 1;
        }
    }
}


function buildValidateEditButton()
{
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var input_val = $('#select_input_id').find('option:selected').text();
            var other = document.getElementById('other_field_bool').value;
            if(input_val == 'select' && !other){
                var other = confirm("Voulez-vous ajouter un champs \"Autre\" à la question ?");
                if(other){
                    document.getElementById('other_field_bool').value = 1;
                }
            }
            document.getElementById('form_edit_question').submit();
            choice_nb = 1;
        }
    }
}

function buildValidateNewSurvey()
{
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var survey_title = document.getElementById('survey_title_id').value;
            if(survey_title.length < 1){
                var ans = confirm('Merci de renseigner un nom pour le questionnaire.');
                if(ans){
                    return false;
                }
            }else{
                document.getElementById('survey_name_id').value = survey_title;
                $('#addQuestion').trigger('click');
            }
        }
    }
}

function buildValidateEditSurveyName()
{
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var new_name = document.getElementById('survey_new_title_id').value;
            var current_name = $('#select_survey_name').val();

            $.ajax({
                url: 'api/survey',
                type: "EDIT",
                data: {
                    current_name : current_name,
                    new_name : new_name
                },
                success: function (json) {
                    var res = JSON.parse(json);
                    if (res.code == 200){
                        $('#select_survey_name :selected').text(new_name).val(new_name);
                        bo_notify('Le nom du questionnaire a bien été changé', 'success');

                    }else{
                        bo_notify('Impossible de modifier le nom du questionnaire', 'danger');
                    }
                },
                error: function () {
                    bo_notify('Erreur inconnue, contacter un admin', 'danger');
                }
            })
        }
    }
}

/*
* Functions to build the messages to append to modals
*/
function buildNewSurveyMsg()
{
    var input = '';

    input += '<div class="col-sm-10" style="margin-bottom: 10px;">';
    input += '<div class="col-sm-5">';
    input += '<label for="question_text_id">Entrez le nom du nouveau questionnaire :</label>';
    input += '</div>';
    input += '<div class="col-sm-5">';
    input += '<input type="text" class="form-control" id="survey_title_id" name="survey_title" required>';
    input += '</div>';
    input += '</div>';

    return input
}

function buildEditSurveyMsg(name)
{
    var input = '';

    input += '<div class="col-sm-10" style="margin-bottom: 10px;">';
    input += '<div class="col-sm-5">';
    input += '<label for="question_text_id">Entrez le nouveau nom du questionnaire: </label>';
    input += '</div>';
    input += '<div class="col-sm-5">';
    input += '<input type="text" class="form-control" id="survey_new_title_id" name="survey_new_title" value="'+ name +'"required>';
    input += '</div>';
    input += '</div>';

    return input
}

function buildEditQuestionMsg(question, offered_answers, refs, inputs)
{
    var input = '';
    var surveyName = document.getElementById('survey_name_id').value;

    input += '<form action="form" method="POST" id="form_edit_question">';

    input += '<div class="col-sm-12 text-center" id="div_question" style="margin-top: 20px; margin-bottom: 20px">';

    input += '<div class="col-sm-2" id="div_select_input">';
    input += '<label for="select_input_id">Type</label>';
    input += createInputSelect(inputs);
    input += '</div>';

    input += '<div class="col-sm-3">';
    input += '<label for="question_text_id">Question</label>';
    input += '<input type="text" class="form-control" id="question_text_id" name="question_text_name" value="'+ question.question_text +'">';
    input += '</div>';

    input += '<div class="col-sm-3" id="div_already_offered_answers">';

    var it_off_ans = offered_answers.length;
    choice_nb = it_off_ans;
    for(var i = 0; i < it_off_ans; i++){

        var choice = i+1;

        if(offered_answers[i].text == 'Autre'){
            document.getElementById('other_field_bool').value = 1;
            continue;
        }
        var off_id = (offered_answers[i].id == null) ? null : offered_answers[i].id;
        input += '<input type="hidden" id="off_id_'+ choice + '" name="off_id_'+ choice + '" value="'+ off_id +'">';

        input += '<div id="div_offered_answers_'+ choice + '" class="row">';

        input += '<div class="col-sm-5">';
        input += '<label for="choice_'+ choice + '_id">Choix</label>';
        input += '<input type="text" class="form-control" id="choice_'+ choice + '_id" name="choice_'+ choice + '" value="'+ offered_answers[i].answer_text +'">';
        input += '</div>';

        input += '<div class="col-sm-5">';
        input += '<label for="score_'+ choice + '_id">Score</label>';
        input += createScoreColorSelect(choice);
        input += '</div>';

        input += '<div class="col-sm-2" id="div_rmv_btn">';
        input += '<label for="remove_choice_btn"></label>';
        input += '<button type="button" class="btn btn-danger removeQuestionBtn" name="action_remove_choice" data-offid="' + off_id + '" data-offchoice="' + choice + '"><span class="glyphicon glyphicon-minus"></span></button>';
        input += '</div>';

        input += '</div>';

    }
    input += '</div>';

    input += '<div class="col-sm-2" id="div_add_btn">';
    input += '<label for="add_choice_btn"></label>';
    input += '<button type="button" id="add_edit_choice_btn" class="btn btn-success addQuestionBtn" name="action_add_choice" ><span class="glyphicon glyphicon-plus"></span></button>';
    input += '</div>';

    input += '<div class="col-sm-2" id="div_select_ref">';
    input += '<label for="select_ref_id">Reference</label>';
    input += createReferenceSelect(refs);
    input += '</div>';

    input += '<input type="hidden" name="survey_name_edit_q" id="survey_name_id" value="' + surveyName + '">';
    input += '<input type="hidden" name="question_choice_nb" id="question_choice_nb" value="' + choice_nb + '">';
    input += '<input type="hidden" name="other_field_bool" id="other_field_bool" value="0">';
    input += '<input type="hidden" name="question_id" id="question_id" value="'+ question.id +'">';
    input += '<input type="hidden" name="action" value="edit">';

    input += '</div>';
    input += '</form>';

    return input;
}

function buildNewQuestionMessage(inputs, references)
{
    var surveyName = document.getElementById("survey_name_id").value;

    var input = '';

    input += '<form action="form" method="POST" id="form_new_question">';

    input += '<div class="col-sm-12 text-center" id="div_question" style="margin-top: 20px; margin-bottom: 20px">';

    input += '<div class="col-sm-2" id="div_select_input">';
    input += '<label for="select_input_id">Type</label>';
    input += createInputSelect(inputs);
    input += '</div>';

    input += '<div class="col-sm-3">';
    input += '<label for="question_text_id">Question</label>';
    input += '<input type="text" class="form-control" id="question_text_id" name="question_text_name">';
    input += '</div>';

    input += '<div class="col-sm-3" id="div_offered_answers">';
    input += '<div id="div_offered_answers_'+ choice_nb + '" class="row">';
    input += '<div class="col-sm-6">';
    input += '<label for="choice_'+ choice_nb + '_id">Choix</label>';
    input += '<input type="text" class="form-control" id="choice_'+ choice_nb + '_id" name="choice_'+ choice_nb + '">';
    input += '</div>';

    input += '<div class="col-sm-6">';
    input += '<label for="score_'+ choice_nb + '_id">Score</label>';
    input += createScoreColorSelect(choice_nb);
    input += '</div>';
    input += '</div>';
    input += '</div>';

    input += '<div class="col-sm-1" id="div_add_btn">';
    input += '<label for="add_choice_btn"></label>';
    input += '<button type="button" id="add_choice_btn" class="btn btn-success addQuestionBtn" name="action_add_choice" ><span class="glyphicon glyphicon-plus"></span></button>';
    input += '</div>';

    input += '<div class="col-sm-1" id="div_rmv_btn">';
    input += '<label for="remove_choice_btn"></label>';
    input += '<button type="button" id="remove_choice_btn" class="btn btn-danger removeQuestionBtn" name="action_remove_choice" ><span class="glyphicon glyphicon-minus"></span></button>';
    input += '</div>';

    input += '<div class="col-sm-2" id="div_select_ref">';
    input += '<label for="select_ref_id">Reference</label>';
    input += createReferenceSelect(references);
    input += '</div>';

    input += '<input type="hidden" name="survey_name_new_q" id="survey_name_id" value="' + surveyName + '">';
    input += '<input type="hidden" name="question_choice_nb" id="question_choice_nb" value="1">';
    input += '<input type="hidden" name="other_field_bool" id="other_field_bool" value="0">';
    input += '<input type="hidden" name="action" value="add">';

    input += '</div>';
    input += '</form>';

    return input;
}

function buildNewChoice(number)
{
    var input = '';

    input += '<div id="div_offered_answers_'+ number + '" class="row">';

    input += '<div class="col-sm-6">';
    input += '<label for="choice_'+ number + '_id">Choix</label>';
    input += '<input type="text" class="form-control" id="choice_'+ number + '_id" name="choice_'+ number + '" required>';
    input += '</div>';

    input += '<div class="col-sm-6">';
    input += '<label for="score_'+ number + '_id">Score</label>';
    input += createScoreColorSelect(number);
    input += '</div>';

    input += '</div>';

    return input;
}

function buildNewEditChoice(number)
{
    var input = '';

    input += '<div id="div_offered_answers_'+ number + '" class="row">';

    input += '<div class="col-sm-5">';
    input += '<label for="choice_'+ number + '_id">Choix</label>';
    input += '<input type="text" class="form-control" id="choice_'+ number + '_id" name="choice_'+ number + '">';
    input += '</div>';

    input += '<div class="col-sm-5">';
    input += '<label for="score_'+ number + '_id">Score</label>';
    input += createScoreColorSelect(number);
    input += '</div>';

    input += '<div class="col-sm-2" id="div_rmv_btn_'+ number +'">';
    input += '<label for="remove_choice_btn"></label>';
    input += '<button type="button" id="remove_choice_btn_'+ number +'" class="btn btn-danger removeQuestionBtn" name="action_remove_choice" data-offnumber="' + number + '"><span class="glyphicon glyphicon-minus"></span></button>';
    input += '</div>';

    input += '</div>';

    return input;
}


/*
* Functions to build input type & reference select
*/
function createInputSelect(data)
{
    var selectList = document.createElement("select");
    selectList.id = "select_input_id";
    selectList.name = "select_input";
    selectList.className = "form-control select_input";

    var data_length = data.length;
    for (var i = 0; i < data_length; i++) {
        var option = document.createElement("option");
        option.value = data[i].id;
        option.text = data[i].field;
        selectList.appendChild(option);
    }
    return selectList.outerHTML;
}

function createScoreColorSelect(choice)
{
    var selectList = document.createElement("select");
    selectList.id = "select_score_color_" + choice;
    selectList.name = "select_score_color_" + choice;
    selectList.className = "form-control select_color";

    var color_length = colors.length;
    for (var i = 0; i < color_length; i++) {
        var option = document.createElement("option");
        option.value = i;
        option.text = i;
        option.style.backgroundColor = colors[i];
        selectList.appendChild(option);
    }
    return selectList.outerHTML;
}


function createReferenceSelect(data)
{
    var selectList = document.createElement("select");
    selectList.id = "select_ref_id";
    selectList.name = "select_ref";
    selectList.className = "form-control select_reference";

    var option = document.createElement("option");
    option.value = 0;
    option.text = 'Aucune';
    selectList.appendChild(option);

    var data_length = data.length;
    for (var i = 0; i < data_length; i++) {
        var option = document.createElement("option");
        option.value = data[i].id;
        option.text = data[i].ref_name;
        selectList.appendChild(option);
    }
    return selectList.outerHTML;
}

/**
 *  SURVEY BUILDER
 */

var choice_nb = 1;

$('#addQuestion').on('click', function (){

    var list_name = 'inputType';
    $.ajax({
        url: 'api/survey',
        type: "GET",
        data: {
            list_name : list_name
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code !== 400){

                var buttons = {
                    cancel : buildCancelButton(),
                    add : buildValidateButton()
                };
                var title = "Ajouter une question";
                var msg = buildNewQuestionMessage(res.data.inputs, res.data.refs);

                buildModal(title, msg, buttons);
                $('#select_input_id').trigger('change');
            }
            else {
                bo_notify('Impossible de construire le modal', 'danger');
            }
        },
        error: function () {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
});

function buildValidateThresholdButton(survey_name){
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var thresh_data = $('#threshold_form').serialize();
            $.ajax({
                url: 'api/threshold',
                type: "POST",
                data: {
                    thresh_data : thresh_data,
                    survey_name : survey_name
                },
                success: function (json) {
                    var res = JSON.parse(json);
                    if (res.code == 200){
                        bo_notify('Les seuils ont bien été configurés', 'success');
                    }else{
                        bo_notify(res.message, 'danger');
                    }
                },
                error: function () {
                    bo_notify('Erreur inconnue, contacter un admin', 'danger');
                }
            })
        }

    }
}

function buildThresholdMessage(refusal, waiting, acceptance){

    var input = '';
    input += '<p>Pour chaque action, indiquez le nombre minimal nécessaire de chaque couleur pour atteindre le seuil (Refus, Attente, Acceptation)</p>';
    input += '<br>';

    input += '<form id="threshold_form">';

    input += '<div class="col-sm-12" style="margin-bottom: 10px;">';
    input += '<div class="col-sm-4">';
    input += '<div class="col-sm-12 text-center">';
    input += '<p style="font-size: 15px;">Refus</p>';
    input += '</div>';


    var color_length = colors.length;
    var i;
    for(i = 0; i < color_length; i++){

        var curr_ref_val = typeof (refusal[i].quantity) === 'undefined' ? refusal[i] : refusal[i].quantity;

        input += '<div class="col-sm-12">';
        input += '<div class="col-sm-4">';
        input += '<div class = "square" style="background-color: '+colors[i]+'"></div>';
        input += '</div>';
        input += '<div class="col-sm-4">';
        input += '<input type="number" class="form-control" id="refusal_thresh_' + colors[i] + '" name="refusal_thresh[]" value="'+ curr_ref_val  + '">';
        input += '</div>';
        input += '</div>';
    }

    input += '</div>';
    input += '<div class="col-sm-4">';
    input += '<div class="col-sm-12 text-center">';
    input += '<p style="font-size: 15px;">Attente</p>';
    input += '</div>';

    for(i = 0; i < color_length; i++){

        var curr_wait_val = typeof (waiting[i].quantity) === 'undefined' ? waiting[i] : waiting[i].quantity;

        input += '<div class="col-sm-12">';
        input += '<div class="col-sm-4">';
        input += '<div class = "square" style="background-color: '+colors[i]+'"></div>';
        input += '</div>';
        input += '<div class="col-sm-4">';
        input += '<input type="number" class="form-control" id="waiting_thresh_' + colors[i] + '" name="waiting_thresh[]" value="'+ curr_wait_val +'">';
        input += '</div>';
        input += '</div>';
    }

    input += '</div>';
    input += '<div class="col-sm-4">';
    input += '<div class="col-sm-12 text-center">';
    input += '<p style="font-size: 15px;">Acceptation</p>';
    input += '</div>';

    for(i = 0; i < color_length; i++){

        var curr_accept_val = typeof (acceptance[i].quantity) === 'undefined' ? acceptance[i] : acceptance[i].quantity;

        input += '<div class="col-sm-12">';
        input += '<div class="col-sm-4">';
        input += '<div class = "square" style="background-color: '+colors[i]+'"></div>';
        input += '</div>';
        input += '<div class="col-sm-4">';
        input += '<input type="number" class="form-control" id="accept_thresh_' + colors[i] + '" name="accept_thresh[]" value="'+ curr_accept_val +'">';
        input += '</div>';
        input += '</div>';

    }

    input += '</div>';
    input += '</div>';

    input += '</form>'

    return input
}

$('#configure_threshold').on('click', function(){
    var survey_name = $('#select_survey_name').val();
    $.ajax({
        url: 'api/threshold',
        type: "GET",
        data: {
            survey_name : survey_name
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200){
                var msg = buildThresholdMessage(res.data.refusal, res.data.waiting, res.data.acceptance);
                var title = "Modifier les seuils du questionnaire : " + survey_name;
                var buttons = {
                    cancel : buildCancelButton(),
                    add : buildValidateThresholdButton(survey_name)
                };
                buildModal(title, msg, buttons);
            }else{
                bo_notify(res.message, 'danger');
            }
        },
        error: function () {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })


});

$('#select_survey_name').on('change',function(){
    document.location.href = 'view?name=' + $(this).val();
});

$('#edit_survey_btn').on('click',function(){

    var current_name = $('#select_survey_name').val();

    var msg = buildEditSurveyMsg(current_name);
    var title = "Modifier le nom du questionnaire";
    var buttons = {
        cancel : buildCancelButton(),
        add : buildValidateEditSurveyName()
    };
    buildModal(title, msg, buttons);
});


/* Handler to add a choice & score in question modal */
$(document).on('click', '#add_choice_btn', function(){

    $('#div_offered_answers').append(buildNewChoice(++choice_nb));
    document.getElementById('question_choice_nb').value = choice_nb;
});

/* Handler to add a choice & score in edit question modal */
$(document).on('click', '#add_edit_choice_btn', function(){

    $('#div_already_offered_answers').append(buildNewEditChoice(++choice_nb));
    document.getElementById('question_choice_nb').value = choice_nb;
});

/* Handler to remove a choice & score in the edit question modal */
$(document).on('click', '.remove-btn', function(){
    var rmv_id = $(this).attr('data-rmv');
    $.ajax({
        url: 'api/question',
        type: "DELETE",
        data: {
            question_id : rmv_id
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200){
                var question_to_remove = "#div_edit_question_" + rmv_id;
                $(question_to_remove).remove();
                bo_notify('Suppression effectuée avec succès', 'success');
            }
            else {
                bo_notify('Impossible de supprimer la question', 'danger');
            }
        },
        error: function (json) {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
});

/*
 * Handler for the delete survey button
 *  Sets all questions to deleted = 0
*/
$(document).on('click', '#delete_survey_btn', function(){

    var prompt_remove = confirm('Voulez-vous vraiment supprimer le questionnaire ?');
    if(!prompt_remove){
        return;
    }

    var rmv_name = $('#select_survey_name').val();
    $.ajax({
        url: 'api/survey',
        type: "DELETE",
        data: {
            survey_name : rmv_name
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200){
                window.location.href = "view";
                bo_notify('Suppression effectuée avec succès', 'success');
            }
            else {
                bo_notify('Impossible de supprimer le questionnaire', 'danger');
            }
        },
        error: function (json) {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
});

/*
 *  Handlers to move a question to the top or bottom
 *   Switches position of two questions
 */
$(document).on('click', '.up-btn', function(){
    var up_id = $(this).attr('data-up');
    movePositionAjaxCall(up_id, 'down');
});

$(document).on('click', '.down-btn', function(){
    var down_id = $(this).attr('data-down');
    movePositionAjaxCall(down_id, 'up');
});

/*
 * Handler to add a new survey
 * Triggers the button to add a question afterwards : a survey doesn't exist without at least 1 question
 */
$(document).on('click', '#add_survey_btn', function(){

    var msg = buildNewSurveyMsg();
    var title = "Créer un questionnaire";
    var buttons = {
        cancel : buildCancelButton(),
        add : buildValidateNewSurvey()
    };
    buildModal(title, msg, buttons);
});


/*
 * Handler for the type select of an answer
 * Appends an empty field if "Other" is selected
 */
$(document).on('change', '.select_answer', function(){
    var src_txt = $(this).find('option:selected').text();
    var src_id = $(this).attr('data-val');

    if(src_txt == 'Autre'){
        var parent = $(this).closest('div');

        var input = '<div class="col-sm-12" id="other_answer_' + src_id + '">';
        input += '<label for="other_answer_' + src_id + '"></label>';
        input += '<input type="text" class="form-control" id="other_answer_' + src_id + '" name="other_answer_' + src_id + '">';
        input += '</div>';

        parent.append(input);
    }else{
        if(document.getElementById("other_answer_" + src_id)){
            $('#other_answer_' + src_id).remove();
        }
    }
});

/*
* Handler for the edit button of each question
*/
$(document).on('click', '.edit-btn', function(){
    var q_id = $(this).attr('data-qid');
    $.ajax({
        url: 'api/question',
        type: "EDIT",
        data: {
            question_id : q_id,
            list_name : 'inputType'
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200 && res.data){
                var buttons = {
                    cancel : buildCancelButton(),
                    edit : buildValidateEditButton()
                };
                var title = "Editer une question";
                var msg = buildEditQuestionMsg(res.data.question, res.data.offered_answers, res.data.refs, res.data.inputs);
                buildModal(title, msg, buttons);

                $('#select_input_id').val(res.data.question.l_input_type_id);
                $('#select_ref_id').val(res.data.question.field_ref_id);
                $('#select_input_id').trigger('change');

                setScoresSelectedValues(res.data.offered_answers);
            }else {
                bo_notify('Impossible d\'éditer la question', 'danger');
            }
        },
        error: function (json) {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })

});

function setScoresSelectedValues(off_answers){
    var it_answers = off_answers.length;
    for(var i = 0; i < it_answers; i++){
        $('#select_score_color_' + (i+1)).val(off_answers[i].score);
        $('#select_score_color_' + (i+1)).change();
    }
}

/*
* Handler for the type select of a question
* Appends or removes choices & score depending on the type
*/
$(document).on('change', '#select_input_id', function(){
    var src_txt = $(this).find('option:selected').text();
    if(src_txt == "text" || src_txt == "date" || src_txt == "number" ) {
        $('#div_offered_answers').addClass('hidden');
        $('#div_add_btn').addClass('hidden');
        $('#div_rmv_btn').addClass('hidden');
        while(choice_nb > 1){
            $('#remove_choice_btn').click();
        }
    }else{
        $('#div_offered_answers').removeClass('hidden');
        $('#div_add_btn').removeClass('hidden');
        $('#div_rmv_btn').removeClass('hidden');
    }
});

$(document).on('change', '.select_color', function(){
    var selected_val = $(this).val();
    $(this).css("backgroundColor", colors[selected_val]);
}).trigger();

/*
* Handler for the remove choice & score button while creating a question
*/
$(document).on('click', '#remove_choice_btn', function(){

    if(choice_nb > 1){
        document.getElementById('div_offered_answers_' + choice_nb).remove();
        document.getElementById('question_choice_nb').value = --choice_nb;
    }
});

/*
* Handler for the remove choice & score of a button while editing a question
*/
$(document).on('click', '.removeQuestionBtn', function(){

    if($(this).attr('data-offnumber')){
        $('#div_offered_answers_' + $(this).attr('data-offnumber')).remove();
        return;
    }

    var off_id = $(this).attr('data-offid');
    var off_choice = $(this).attr('data-offchoice');

    var prompt_rmv = confirm('Voulez-vous vraiment supprimer ce choix ?');
    if(!prompt_rmv){
        return;
    }

    $.ajax({
        url: 'api/offered_answers',
        type: "DELETE",
        data: {
            rmv_id : off_id
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200){
                $('#div_offered_answers_' + off_choice).remove();
            }else {
                bo_notify(res.message, 'danger');
            }
        },
        error: function (json) {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
});
