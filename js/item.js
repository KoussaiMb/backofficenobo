/*
    Handler for the button used to add a category
 */
$('#addCategoryButton').on('click', function(){

    var buttons = {
        validate : buildValidateButton(),
        cancel : buildCancelButton()
    };
    var title = 'Insérer une nouvelle catégorie';

    $.ajax({
        url: 'api/add_category',
        type: "GET",
        success: function (json) {
            var res = JSON.parse(json);
            var msg = "";
            if (res.code == 200 && res.data){
                msg = buildNewCategoryMessage(res.data);
            }else{
                msg = buildNewCategoryMessage(null);
                bo_notify('La liste des catégories est vide', 'danger');
            }
            buildModal(title, msg, buttons);
        },
        error: function () {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
});

/*
    Call to insert a new category in the database
 */
function addCategory(name, parent)
{
    $.ajax({
        url: 'api/add_category',
        type: "POST",
        data: {
            name : name,
            parent_id : parent
        },
        success: function (json) {
            var res = JSON.parse(json);
            if (res.code == 200){
                window.location = 'view';
                bo_notify('Catégorie insérée avec succès', 'success');
            }else{
                bo_notify('Impossible d\'insérer la catégorie', 'danger');
            }
        },
        error: function () {
            bo_notify('Erreur inconnue, contacter un admin', 'danger');
        }
    })
}

function buildCancelButton()
{
    return  {
        label: "Annuler",
        className: 'btn-warning',
    }
}

function buildValidateButton()
{
    return  {
        label: "Valider",
        className: 'btn-success',
        callback: function(){
            var categoryName = $('#categoryName').val();
            var parentId = $('#select_category_id').val();

            addCategory(categoryName, parentId);
        }
    }
}

function buildModal(title, msg, buttons)
{
    return bootbox.dialog({
        size : "large",
        closeButton: false,
        backdrop: true,
        title: title,
        message: msg,
        buttons: buttons
    });
}

/*
    Message displayed in the bootbox used to add a category
 */
function buildNewCategoryMessage(categories)
{
    var input = '';

    input+= '<div class="col-sm-12">';
    input+= '<div class="col-sm-3">';
    input+= '<label for="categoryName">Nom</label>';
    input += '<input type="text" name="category_name" id="categoryName" class="form-control">';
    input+= '</div>';
    input+= '<div class="col-sm-3">';
    input+= '<label for="select_category_id">Parent</label>';
    input += createCategorySelect(categories);
    input+= '</div>';

    return input;
}

/*
    Creates a select with all the existing categories
    The first option is none
 */
function createCategorySelect(data)
{
    var selectList = document.createElement("select");
    selectList.id = "select_category_id";
    selectList.name = "select_category";
    selectList.className = "form-control";

    var option = document.createElement("option");
    option.value = -1;
    option.text = 'Pas de parent';
    selectList.appendChild(option);

    if(data){
        var data_length = data.length;
        for (var i = 0; i < data_length; i++) {
            option = document.createElement("option");
            option.value = data[i].id;
            option.text = data[i].name;
            selectList.appendChild(option);
        }
    }
    return selectList.outerHTML;
}