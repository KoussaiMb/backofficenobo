//TODO: updateListInModal()

function insertTasks(arrTask, tableName)
{
    arrTask.forEach(function(task) {
        insertTask(task, tableName);
    });
}

function insertTask(task, table)
{
    var pos = task.position - 1;

    table.find('tbody:eq('+task.type_task+') > tr > td:eq('+pos+')').prepend(createTask(task));
}

function createTask(task)
{
    return '<input type="hidden" name="task_' + task.position + '_' +task.type_task+ '_' +task.task_id+ '_' +task.id+ '" value="' +task.task_time.substr(0, 5)+ '">' +
        '<div class="task-box">' +
        '<img class="img-responsive" src="' +task.logo+ '">' +
        '<span class="task-separator">|</span>' +
        '<span class="task-name">' +task.name+ '</span>' +
        '<span class="task-separator">|</span>' +
        '<span class="task-time">' +task.task_time.substr(0, 5)+ '</span>' +
        '</div>';
}

function insertButtonAddTask(table)
{
    table
        .find('tbody td')
        .append('<button type="button" class="btn add-m-task"><span class="add-m-task glyphicon glyphicon-plus"></span></button>');
}

function mp_removeCol(table)
{
    table.find('tr > th:last-child').remove();
    table.find('tr > td:last-child').remove();
}

function mp_addCol(table, pos)
{
    table
        .find('thead tr')
        .append('<th><span>PL ' +pos+ '</span></th>');
    table
        .find('tbody tr')
        .append('<td><button type="button" class="btn add-m-task"><span class="glyphicon glyphicon-plus"></span></button></td>');
}

function updateHead(table)
{
    table.find('thead th').empty().each(function () {
        var pos = $(this).index() + 1;

        $(this).append('<span>PL ' +pos+ '</span>');
    });
}

function deleteTask(taskDiv)
{
    taskDiv.prev().remove();
    taskDiv.remove();
}

function editTask(task, task_time)
{
    task.prev().val(task_time);
    task.find('span[class=task-time]').text(task_time);
}

function getType(buttonClicked)
{
    return buttonClicked.closest('tbody').index() - 1;
}

function getPosition(buttonClicked)
{
    return buttonClicked.closest('td').index() - - 1;
}

function addTask(task, buttonClicked)
{
    var type_task = getType(buttonClicked);
    var position = getPosition(buttonClicked);

    buttonClicked.before('<input type="hidden" name="task_' +position+ '_' +type_task+ '_' +task.id+ '_' +0+ '" value="' + task.time + '">' +
        '<div class="task-box">' +
        '<img class="img-responsive" src="' + task.logo + '">' +
        '<span class="task-separator">|</span>' +
        '<span class="task-name">' + task.name + '</span>' +
        '<span class="task-separator">|</span>' +
        '<span class="task-time">' + task.time + '</span>' +
        '</div>');
}

/**
 * New Maintenance Planning
 */

$(document).ready(function(){
    $("#l_recurrence_id").on("change", function(){
        showMaintenances($("option:selected", this).data("val"));
    }).trigger("change");

    $(".maintenanceType").on("change", function(){
        var maintenance_id = $(this).data("val");
        var mp_type_maintenance_type_id = $("#maintenance_" + maintenance_id).data("val");
        var cardex_id = $(this).val();

        if (!mp_type_maintenance_type_id ||!cardex_id) {
            bo_notify("Impossible de récupérer le type de cardex", "danger");
            return;
        }

        if (cardex_id == 0) {
            removeNewMaintenanceType(maintenance_id, mp_type_maintenance_type_id)
        } else {
            addNewMaintenanceType(maintenance_id, mp_type_maintenance_type_id, cardex_id);
        }

    });
});

function showMaintenances(recurrence_position) {
    var nb_recurrence = getNbMaintenance(recurrence_position);

    $(".maintenance-type").hide();

    for (let i = 1; i <= 5; i++) {
        if (i <= nb_recurrence) {
            $("#maintenance_" + i).show();
        } else {
            removeNewMaintenanceType(i, $("#maintenance_" + i).data("val"));
        }
    }
}

function getNbMaintenance(recurrence_position) {
    switch (recurrence_position) {
        case 5:
            return 2;
        case 6:
            return 3;
        case 7:
            return 4;
        case 8:
            return 5;
        default:
            return 1;
    }
}

function addNewMP() {
    var MP_name = $("#name").val();
    var l_recurrence_id = $("#l_recurrence_id").val();
    var min_area = $("#min_area").val();
    var max_area = $("#max_area").val();
    var ironing = $("#ironing").prop("checked");
    var childs = $("#childs").prop("checked");

    if (MP_name || min_area || max_area) {
        bo_notify("Impossible des récupérer les informations pour créer le cardex", "danger");
        return;
    }


    ajax_call(
        "api/maintenance",
        "POST",
        {
            name: MP_name,
            l_recurrence_id: l_recurrence_id,
            min_area: min_area,
            max_area: max_area,
            ironing: ironing ? 1 : 0,
            childs: childs ? 1 : 0
        },
        function(result) {
            console.log(result);
            if (result.code == 200) {
                window.location.href = "view";
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );
}

function addNewMaintenanceType(maintenance_id, mp_type_maintenance_type_id, cardex_id) {
    var MP_type_id = $("#MP_type_id").val();

    ajax_call(
        "api/cardex",
        mp_type_maintenance_type_id == -1 ? "POST" : "PUT",
        {
            mp_type_maintenance_type_id: mp_type_maintenance_type_id,
            MP_type_id : MP_type_id,
            cardex_id: cardex_id
        }, function(result){
            console.log(result);
            if (result.code == 200) {
                bo_notify(result.message, "success");
                $("#maintenance_" + maintenance_id).data("val", result.data);
            } else {
                bo_notify(result.message, "danger");
            }
        }
    )
}

function removeNewMaintenanceType(maintenance_id, mp_type_maintenance_type_id) {
    if (mp_type_maintenance_type_id == -1)
        return;

    ajax_call(
        "api/cardex",
        "DELETE",
        {
            mp_type_maintenance_type_id: mp_type_maintenance_type_id
        }, function(result){
            console.log(result);
            if (result.code == 200) {
                bo_notify(result.message, "success");
                $("#maintenance_" + maintenance_id).data("val", -1);
            } else {
                bo_notify(result.message, "danger");
            }
        }
    )
}

function addNewTask(maintenance_id) {
    new Promise((res, err) => {
        ajax_call(
            "api/list",
            "GET",
            {
                listName: "rollingTask"
            },
            function(result){
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    }).then(task_list => {
        var selectInputs = [{text: 'Tâches...', value: ''}]

        task_list.forEach(task => {
            selectInputs.push({
                text: task.field,
                value: task.id
            });
        });

        return new Promise((res, err) => {
            bootbox.prompt({
                title: "Sélectionnez la tâche",
                inputType: 'select',
                inputOptions: selectInputs,
                callback: function (result) {
                    if (result) {
                        res(result);
                    }
                }
            })
        });
    }).then(task => {
        ajax_call(
            "api/task",
            "POST",
            {
                l_rollingTask_id: task,
                mp_type_maintenance_type_id: $("#maintenance_" + maintenance_id).data("val")
            }, function(result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    displayNewTask(maintenance_id, result.data);
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        )
    }).catch(err => {
        ajax_log(err, "maintenance_planning.js / addNewTask");
    });
}

function displayNewTask(maintenance_id, task) {
    $("#taskList_" + maintenance_id).prepend(`
        <div class="rolling-task" id="task_${task.id}">
            ${task.field}
            <button class="spanButton pull-right" onclick="removeTask(${task.id})"><span class="glyphicon glyphicon-remove"></span></button>
        </div>
    `);
}

function removeTask(task_id) {

    new Promise((res, err) => {
        bootbox.confirm({
            message: "Voulez-vous vraiment supprimer cette tâche?",
            buttons: {
                confirm: {
                    label: 'Oui',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Non',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result)
                    res();
            }
        });
    }).then(() => {
        ajax_call(
            "api/task",
            "DELETE",
            {
                task_maintenance_type_id: task_id
            }, function(result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    $("#task_" + task_id).remove();
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        ajax_log(err, "maintenance_planning.js / removeTask");
    })
}

function updateMP() {
    var MP_type_id = $("#MP_type_id").val();
    var MP_name = $("#MP_name").val();
    var l_recurrence_id = $("#l_recurrence_id").val();
    var min_area = $("#min_area").val();
    var max_area = $("#max_area").val();
    var ironing = $("#ironing").prop("checked");
    var childs = $("#childs").prop("checked");

    if (!MP_name || !min_area || !max_area) {
        bo_notify("Impossible des récupérer les informations pour créer le cardex", "danger");
        return;
    }

    ajax_call(
        "api/maintenance",
        "PUT",
        {
            id: MP_type_id,
            name: MP_name,
            l_recurrence_id: l_recurrence_id,
            min_area: min_area,
            max_area: max_area,
            ironing: ironing ? 1 : 0,
            childs: childs ? 1 : 0
        },
        function(result) {
            console.log(result);
            if (result.code == 200) {
                bo_notify(result.message, "success");
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );
}

