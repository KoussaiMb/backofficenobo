////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////  10:    End launch_ajax_call      //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function add_holidays(){
    var start = $('#start_date').val();
    var end = $('#end_date').val();
    var address_token = $('#holiday_address').val();
    var ajax_data = {
        address_token: JSON.stringify(address_token),
        start: JSON.stringify(moment(start, 'DD-MM-YYYY').format('YYYY-MM-DD')),
        end: JSON.stringify(moment(end, 'DD-MM-YYYY').format('YYYY-MM-DD'))
    };
    ajax_call("../mission/api/holidays", "POST", ajax_data, function(result){
        if(result.code == 200){
            bo_notify(result.message, 'success');
        }else{
            bo_notify(result.message, 'danger');
        }
        $('#holiday_address').trigger('change');
    });
}


$(document).ready(function() {
    /* Validation button when conflict on holidays*/
    $('#btn-accept-holidays-conflict').on('click', function () {
        $('#modal_alert').modal('hide');
        add_holidays();
    });

    $('#btn-refuse-holidays-conflict').on('click', function () {
        $('#modal_alert').modal('hide');
    });


    $('#tab5').on('click', function(){
        $('#holiday_address').trigger("change");
    });

    $('#holiday_address').on('change', function () {
        var address_token = $('#holiday_address').val();
        var ajax_data = {
            address_token: JSON.stringify(address_token)
        };
        ajax_call("../mission/api/holidays", "GET", ajax_data, function(result){
            $('.panel-body-holidays').empty();
            $.each(result.data, function(index, value){
                $('.panel-body-holidays').append(buildHolidaysPanelMessage(value));
            });
            $('.remove_holiday').on('click', function(){
                var data_bootbox = {
                    message: "Êtes-vous sur de vouloir supprimer cette période de vacances ?",
                    callBackTrue: function callBackTrue(data)
                    {
                        ajax_call("../mission/api/holidays", "DELETE", data.ajax_data, function(result){
                            if(result.code == 200){
                                bo_notify(result.message), 'success';
                            }else{
                                bo_notify(result.message), 'danger';
                            }
                            $('#holiday_address').trigger("change");
                        });
                    },
                    callBackFalse:function callBackFalse(data){
                    },
                    data : {
                        true: {
                            ajax_data: {
                                id: JSON.stringify($(this).val())
                            }
                        },
                        false: {}
                    }
                };
                bootbox_confirm(data_bootbox);
            })
        })
    });

    function buildHolidaysPanelMessage(value){
        var input = "<div class='col-xs-12'>" +
            "<table class='table table-condensed table-bordered'>" +
            "<thead>" +
            "      <tr\n>" +
            "        <th style='text-align: center; padding: 5px;' ><span class='glyphicon glyphicon-time'></span>&nbsp&nbsp Début Vacances : &nbsp&nbsp"+ moment(value.start, 'YYYY-MM-DD').format('DD/MM/YYYY') +"</th>\n" +
            "        <th style='text-align: center; padding: 5px;' > Fin Vacances : &nbsp&nbsp"+ moment(value.end, 'YYYY-MM-DD').format('DD/MM/YYYY') +"</th>\n" +
            "        <th style='text-align: center; padding: 5px;'><button type='button' value='"+ value.id +"' style='padding: 1px 10px 1px 10px;' class='btn btn-danger btn-sm remove_holiday' name='action'><span class='glyphicon glyphicon-remove'></span></th>\n" +
            "      </tr>\n" +
            "</thead>" +
            "</table></div>";
        return input;
    }

    $('#add_holiday').on('click', function(){
        var holliday_start = $('#start_date').val();
        var holliday_end = $('#end_date').val();
        //We check if dates have been filled
        if (holliday_start === '' || holliday_end === ''){
            bo_notify("Veuillez entrer des dates valides", "danger");
        }
        else {
            //We check if start is lower than end
            if (moment(holliday_start, 'DD-MM-YYYY').isAfter(moment(holliday_end, 'DD-MM-YYYY'))) {
                bo_notify("Le début des vacances dois être inférieur a la fin des vacances", "danger");
            }
            else{
                //We check if start is after today
                if (moment(holliday_start, 'DD-MM-YYYY').isBefore(moment(moment().format('DD-MM-YYYY'), 'DD-MM-YYYY'))){
                    bo_notify("Le début des vacances doit être égal ou supérieur au jour actuel", "danger");
                }else{
                    add_holidays();
                }
            }
        }
    });
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////CALENDAR DISPO//////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function calendar_dispo_customer(token){
    let address = $('#address_dispo').val();

    $('#calendar').fullCalendar({
        columnFormat: 'dddd',
        defaultView: 'agendaWeek',
        header: {
            left: '',
            center: '',
            right: ''
        },
        height: 'auto',
        editable: true,
        slotDuration: '00:30:00',
        snapDuration: '00:15:00',
        selectable: true,
        selectHelper: true,
        selectOverlap: false,
        allDaySlot: false,
        minTime: "08:00",
        maxTime: "22:00",
        businessHours: {
            // days of week. an array of zero-based day of week integers (0=Sunday)
            dow: [0, 1, 2, 3, 4, 5, 6], // Monday - Thursday

            start: '08:00', // a start time (10am in this example)
            end: '22:00' // an end time (6pm in this example)
        },
        eventConstraint: "businessHours",
        selectConstraint: "businessHours",
        events: function(start, end, timezone, callback) {
            let ajax_data = {
                user_token : token,
                address_token : address
            };
            ajax_call("api/client_dispo", "GET", ajax_data, function(result){
                if (result.code === '200') {
                    callback(result.data);
                } else {
                    bo_notify(result.message, "danger");
                }
            })
        },

        eventClick: function (event, jsEvent, view) {
            let start = moment(event.start).format('HH:mm:ss');
            let end = moment(event.end).format('HH:mm:ss');
            $('#modal_dispo_Title').html(start + ' - ' + end).val(token);
            $('#modal_dispo_Body').val(event.id);
            $('#modal_dispo').modal();
        },

        select: function(start, end, allDay) {
            let select_duration = moment.utc(end.diff(start)).format("HH:mm:ss");
            if (select_duration < "02:00:00") {
                bo_notify("Une disponibilité ne peux pas être inférieure à 2H", "danger");
                $('#calendar').fullCalendar('unselect');
            }
            else {
                let ajax_data = {
                    start: JSON.stringify(moment(start).format('HH:mm:ss')),
                    end: JSON.stringify(moment(end).format('HH:mm:ss')),
                    day: JSON.stringify(moment(start).day()),
                    token: JSON.stringify(token),
                    address_token: JSON.stringify(address)
                };
                console.log(ajax_data);
                ajax_call("api/client_dispo", "POST", ajax_data, function (result){
                    if (result.code === '200') {
                        $('#calendar').fullCalendar('renderEvent',
                            {
                                start: start,
                                id: result.data,
                                end: end,
                                overlap: false
                            },
                            true // Makes the event "stick"
                        );
                        bo_notify(result.message, "success");
                    }
                    else
                    {
                      bo_notify(result.message, "danger");
                    }

                    $('#calendar').fullCalendar('unselect');
                });
            }
        },

        eventDrop: function(event, delta) {
            var ajax_data = {
                start: JSON.stringify(moment(event.start).format('HH:mm:ss')),
                end: JSON.stringify(moment(event.end).format('HH:mm:ss')),
                id: JSON.stringify(event.id),
                day: JSON.stringify(moment(event.start).day())
            };
            ajax_call("api/client_dispo", "PUT", ajax_data, function (result){
                bo_notify(result.message, "success");
            });
        },

        eventResize: function(event) {
            var ajax_data = {
                start: JSON.stringify(moment(event.start).format('HH:mm:ss')),
                end: JSON.stringify(moment(event.end).format('HH:mm:ss')),
                id: JSON.stringify(event.id),
                day: JSON.stringify(moment(event.start).day())
            };
            ajax_call("api/client_dispo", "PUT", ajax_data, function (result){
                bo_notify(result.message, "success");
            });
        },

        eventRender: function (event, eventElement){
            $(eventElement).css("text-align", "center");
            $(eventElement).css("opacity", "0.70");
        }

    });
}


$('#confirm-delete-dispo').on('click', function(e) {
    $('#modal_dispo').modal('hide');
    var data_bootbox = {
        message: "<span style='font-weight: bold; font-size: 15px;'>Êtes-vous sûr de vouloir supprimer ce créneaux de disponibilité ?</span>",
        callBackTrue: function callBackTrue(data)
        {
            ajax_call("api/client_dispo", "DELETE", data.ajax_data, function(result){
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    $('#calendar').fullCalendar('removeEvents', JSON.parse(data.ajax_data.id));
                } else {
                    bo_notify(result.message, "danger");
                }
            });
        },
        callBackFalse:function callBackFalse(data){
            $('#modal_dispo').modal('show');
        },
        data : {
            true: {
                ajax_data: {
                    token: JSON.stringify($('#modal_dispo_Title').val()), //Getting de mission token
                    id: JSON.stringify($('#modal_dispo_Body').val())
                }
            },
            false: {}
        }
    };
    bootbox_confirm(data_bootbox);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// FIN CALENDAR DISPONIBILITÉ PROVIDER //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// DEBUT STOP NOBO //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function() {

    $('.nobo_stop').change(function(){
        var date = $(this).val();
        $('.alert_past').remove();
        if (moment(date, 'DD-MM-YYYY') < moment()){
            $('.nobo_stop_label').prepend("<div class='alert_past col-sm-12 text-center alert alert-danger'>Attention vous avez selectionner une date passée</div>");
        }
    })
});

function create_select_reason_stop_nobo(list_reason){
    var select = "<div class=\"form-group\">\n" +
        " <label class=\"col-xs-12 text-left\" ><h5> Raison :</h5>\n" +
        "  <select name='select_reason' class=\"form-control select_reason_stop\" id=\"sel1\">";

    $.each(list_reason, function(index, value){
        select += "<option value=" + value.id + ">"+ value.field +"</option>";
    });
    select += "<option>autre</option>" +
        "   </select></label>\n" +
        "</div>";
    return select;
}

function add_stop_nobo(user_token){
    $('#modal_stop_nobo').modal("show");
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// FIN STOP NOBO ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function bootbox_confirm(data_bootbox){
    bootbox.confirm({
        message: data_bootbox.message,
        buttons: {
            confirm: {
                label: 'Oui',
                className: 'btn-success'
            },
            cancel: {
                label: 'Non',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result){
                data_bootbox.callBackTrue(data_bootbox.data.true);
            }
            else{
                data_bootbox.callBackFalse(data_bootbox.data.false);
            }
        }
    });
}
