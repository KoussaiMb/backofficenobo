var lastAppointment = 0;

jQuery(document).ready(function() {
    $("#search_date" ).datepicker({
        'language' : "fr",
        'autoclose': 'true',
        'format': "yyyy-mm-dd",
        disableTouchKeyboard: true,
        todayHighlight: true
    });
    $('#search_date').on("change", function() {
        var date = $("#search_date").val();

        $("#search_date").data("real", date);
        setTimeout(function() {
            $("#search_date").val(moment(date).format('LLLL').replace("00:00", ""));
        }, 1);
        loadNewDispo(date);
    }).trigger('change');

    $(document).on("click", ".dispo-item", function(){

        if ($(this).attr('id') == 'current_appointment') {
            displayAppointment();
            return;
        }

        var e_id = $(this).data("val");
        var e_date = $(this).data("date");
        var w_id = $("#w_id").val();
        var g_token = $(this).data("user");

        if (!(e_id && e_date && w_id && g_token))
            return;

        new Promise((res, err) => {
            var current_id = $('#current_appointment').data("val");

            if (current_id && current_id != 0) {
                bootbox.confirm({
                    message: 'Il existe déjà un premier rendez-vous, voulez-vous vraiment le remplacer?',
                    buttons:{
                        confirm:{
                            label: "Oui",
                            className: "btn-success"
                        },
                        cancel: {
                            label: "Non",
                            className: "btn-danger"
                        }
                    },
                    callback: function(yes){
                        if (yes)
                            res();
                    }
                });
            } else {
                res();
            }
        }).then(() => {
            addAppointment(e_id, e_date, w_id, g_token);
        }).catch(err => {
            console.log(err);
            bo_notify("Une erreur est survenue", "danger");
        });

    });
});

function displayAppointment() {
    var e_id = $('#current_appointment').data("val");

    new Promise((res, err) => {
        ajax_call(
            "api/event_info",
            "GET",
            {
                event_id: e_id
            },
            function(result) {
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        );
    }).then((event_info)=> {
        bootbox.dialog({
            title: event.title,
            message: '<div>'+
                        `<h3>${moment(event_info.start).format('LLLL')}</h3>`+
                        `<p>de ${moment(event_info.start).format('HH:mm')} à ${moment(event_info.end).format('HH:mm')}</p>`+
                        '<h4>Commentaire:</h4>'+
                        `<p>${event_info.commentary ? event_info.commentary : 'Aucun commentaire'}</p>`+
                    '</div>',
            buttons: {
                cancel: {
                    label: "Fermer",
                    callback: function(){

                    }
                },
                edit: {
                    label: "Modifier",
                    className: 'btn-warning',
                    callback: function(){
                        editAppointment(event_info);
                    }
                },
                delete: {
                    label: "Supprimer",
                    className: 'btn-danger',
                    callback: function(){
                        deleteAppointment();
                    }
                }
            }
        });
    }).catch(err => {
        bo_notify(err, 'danger');
    });
}

function editAppointment(event_info) {
    var w_id = $("#w_id").val();

    new Promise((res, err) => {
        bootbox.dialog({
            title: 'Modification du commentaire',
            message: '<div class="form-group">'+
                        '<label for="commentary">Commentaire:</label>' +
                        `<textarea class="form-control" style="resize: vertical;" id="commentary" name="commentary" rows="3" value="${event_info.commentary}">${event_info.commentary}</textarea>`+
                    '</div>',
            buttons: {
                cancel: {
                    label: "Annuler",
                    className: 'btn-danger',
                    callback: function(){

                    }
                },
                ok: {
                    label: "Modifier",
                    className: 'btn-success',
                    callback: function(){                    
                        var commentary = $('#commentary').val() ? $('#commentary').val() : 'Aucun commentaire';

                        if (commentary) 
                            res(commentary);
                        else
                            err("Le commentaire est incorrect");
                    }
                }
            }
        });
    }).then(commentary => {
        ajax_call(
            "api/appointment",
            "PUT",
            {
                event_id: event_info.id,
                commentary: commentary
            },
            function(result){
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        bo_notify(err, "danger");
    });
}

function addAppointment(e_id, e_date, w_id, g_token) {
    new Promise((res, err) => {
        bootbox.prompt({
            title: "Ajouter un commentaire pour ce rendez-vous:",
            inputType: 'textarea',
            callback: function (commentary) {
                var commentary = commentary ? commentary : 'Aucun commentaire';
                res(commentary);
            }
        });
    }).then(commentary => {
        ajax_call(
            "api/appointment",
            "POST",
            {
                event_id: e_id,
                date: e_date,
                w_list_id: w_id,
                guest_token: g_token,
                commentary: commentary
            },
            function (result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    updateCurrentAppointment(result.data);
                    var date = $("#search_date").data("real");
                    loadNewDispo(date);
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        console.log(err);
        bo_notify("Une erreur est survenue", "danger");
    })
}

function deleteAppointment() {
    var e_id = $('#current_appointment').data("val");
    var w_id = $("#w_id").val();

    if (!(e_id && w_id) || (e_id == 0))
        return;

    new Promise((res, err) => {

        bootbox.confirm({
            message: 'Voulez-vous vraiment supprimer ce rendez-vous?',
            buttons:{
                confirm:{
                    label: "Oui",
                    className: "btn-success"
                },
                cancel: {
                    label: "Non",
                    className: "btn-danger"
                }
            },
            callback: function(yes){
                if (yes)
                    res();
            }
        });

    }).then(() => {
        ajax_call(
            "api/appointment",
            "DELETE",
            {
                event_id: e_id,
                w_list_id: w_id
            },
            function (result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    clearCurrentAppointment();
                    var date = $("#search_date").data("real");
                    loadNewDispo(date);
                } else {
                    console.log(result);
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        console.log(err);
        bo_notify("Une erreur est survenue", "danger");
    });

}

function updateCurrentAppointment(event) {
    $('#current_appointment').empty();
    $('#current_appointment').removeClass("empty");
    $('#current_appointment').data("val", event.id);
    $('#current_appointment').data("date", event.event_date);
    $('#current_appointment').append('<div class="dispo-item-corner"></div>');
    $('#current_appointment').append(`<h5><u>${event.firstname} ${event.lastname}</u></h5>`);
    $('#current_appointment').append(`<p>${moment(event.event_date).format('LLLL').replace("00:00", "")} de ${timeToTime(event.start)} à ${timeToTime(event.end)}</p>`);
}

function clearCurrentAppointment () {
    $('#current_appointment').empty();
    $('#current_appointment').addClass("empty");
    $('#current_appointment').data("val", 0);
    $('#current_appointment').append('<div class="dispo-item-corner"></div>');
    $('#current_appointment').append(`<h5>Aucun rendez-vous</h5>`);
}

function appendDispo(dispo) {
    $('.guest-dispo').append(
        '<div class="col-xs-4">'+
            `<div class="col-xs-12 dispo-item" data-val="${dispo.id}" data-date="${dispo.event_date}" data-user="${dispo.user_token}">`+
                `<div class="dispo-item-corner"></div>`+
                `<h5>${dispo.firstname} ${dispo.lastname}</h5>`+
                `<p>${timeToTime(dispo.start)} <span class="glyphicon glyphicon-arrow-right"> </span> ${timeToTime(dispo.end)}</p>`+
            `</div>`+
        `</div>`
    );
}

function appendDay (day) {
    $('.guest-dispo').append(
        `<div class="col-sm-12">`+
            `<h5>${moment(day).format('LLLL').replace("00:00", "")}</h5>`+
            `<hr>`+
        `</div>`
    );
}

function loadNewDispo (date) {
    $('.guest-dispo').empty();
    ajax_call(
        "api/appointment",
        "GET",
        {
            date: date
        },
        function (result) {
            if (result.code == 200) {
                var currentDate = "";
                for (var event in result.data) {
                    if (result.data.hasOwnProperty(event)) {
                        if (currentDate != result.data[event].event_date) {
                            appendDay(result.data[event].event_date);
                            currentDate = result.data[event].event_date;
                        }
                        appendDispo(result.data[event]);
                    }
                }
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );
}

function exportAppointment() {
    var current_appointment = $('#current_appointment').data("val");

    if (current_appointment == 0) {
        bo_notify("Il n'y a pas de rendez-vous établi", "danger");
        return;
    }

    new Promise((res, err) => {
        bootbox.confirm({
            message: 'Exporter le rendez-vous vers Google Agenda?',
            buttons:{
                confirm:{
                    label: "Oui",
                    className: "btn-success"
                },
                cancel: {
                    label: "Non",
                    className: "btn-danger"
                }
            },
            callback: function(yes){
                if (yes)
                    res();
            }
        });
    }).then(() => {
        ajax_call(
            "api/event_info",
            "GET",
            {
                event_id: current_appointment
            },
            function(result){
                if (result.code == 200) {
                    exportEvent(result.data);
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        )
    }).catch(err => {
        console.log(err);
        bo_notify("Une erreur est survenue", "danger");
    });
}