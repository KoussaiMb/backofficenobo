/*
var product_number = 0;
var task_number = 0;


//settings

function constructCardexForm(url, value, id, setting)
{
    var input = '';

    input += "<form method='POST' class='text-center' action='form/cardex' enctype='multipart/form-data'>";
    input += "<div class='form-group'>";
    input += "<input type='text' class='form-control' name='name' value='";
    input += value;
    input += "'>";
    input += "</div>";
    input += "<div class='form-group'>";
    input += "<img class='img-cardex' src='";
    input += url;
    input += "'>";
    input += "</div>";
    input += "<div class='form-group'>";
    input += "<input type='file' name='logo'>";
    input += "</div>";
    input += "<input type='hidden' name='setting' value='";
    input += setting;
    input += "'>";
    input += "<input type='hidden' name='id' value='";
    input += id;
    input += "'>";
    input += "<input type='hidden' name='logo' value='";
    input += url;
    input += "'>";
    input += "<div class='form-group text-center'>";
    input += "<button type='button' class='btn btn-default' id='cancel'>Annuler</button>";
    input += "<button type='submit' class='btn btn-danger' name='action' value='edit'>Valider</button>";
    input += "</div>";
    input += "</form>";

    return input;
}

//CARDEX CLIENT


function buildNewRoomModalMsg(line_nb ,rooms)
{
    var input = '';

    input += '<div class="panel panel-default">';
    input += '<div class="panel panel-body">';
    input += '<form id="productForm">';
    input += buildNewRoomLine(line_nb, rooms);
    input += '</div>';
    input += '</div>';
    input += '</form>';

    return input;
}


function buildNewRoomLine(line_nb, rooms)
{
    var input = '';

    input += '<div class="col-sm-5">';
    input += '<div class="form-group">';

    var it_rooms = rooms.length;
    input += '<select name="r_room_select[]" class="form-control" id="r_room_select' + line_nb + '" required>';
    for (var i = 0; i < it_rooms; i++) {
        input += '<option value="' + rooms[i].id + '">' + rooms[i].value + '</option>';
    }
    input += '</select>';

    input += '</div>';
    input += '</div>';
    input += '<div class="col-sm-5">';
    input += '<div class="form-group">';
    input += '<input type="text" class="form-control r_description" id="r_description' + line_nb + '" name="r_description[]" value="" placeholder="Description">';
    input += '</div>';
    input += '</div>';
    input += '<div class="col-sm-2">';
    input += '<div class="form-group">';
    input += '<button class="btn btn-success" type="button" id="btn_add_room"> <span class="glyphicon glyphicon-plus"></span></button>';
    input += '</div>';
    input += '</div>';
    input += '<div id="room_fields"></div>';

    return input;
}



function buildNewProductModalMsg(line_nb, rooms, products)
{

    var input = '';

    input += '<div class="panel panel-default">';
    input += '<div class="panel panel-body">';
    input += '<form id="productForm">';
    input += buildNewProductLine(line_nb, rooms, products);
    input += '</div>';
    input += '</div>';
    input += '</form>';

    return input;
}


function buildNewProductLine(line_nb, rooms, products)
{
    var input = '';

    input += '<div class="col-sm-3">';
    input += '<div class="form-group">';

    var it_products = products.length;
    input += '<select name="p_product_select[]" class="form-control" id="p_product_select' + line_nb +'" required>';
    for(var i = 0; i < it_products; i++) {
        input += '<option value="' + products[i].id + '">' + products[i].value + '</option>';
    }
    input += '</select>';

    input += '</div>';
    input += '</div>';
    input += '<div class="col-sm-3">';
    input += '<div class="form-group">';

    var it_rooms = rooms.length;
    input += '<select name="p_room_select[]" class="form-control" id="p_room_select' + line_nb +'" required>';
    for(var j = 0; j < it_rooms; j++) {
        input += '<option value="' + rooms[j].id + '">' + rooms[j].value + '</option>';
    }
    input += '</select>';

    input += '</div>';
    input += '</div>';
    input += '<div class="col-sm-4">';
    input += '<div class="form-group">';
    input += '<input type="text" class="form-control p_description" id="p_description' + line_nb +'" name="p_description[]" value="" placeholder="Description">';
    input += '</div>';
    input += '</div>';
    input += '<div class="col-sm-2">';
    input += '<div class="form-group">';
    input += '<button class="btn btn-success" id="btn_add_product" type="button"> <span class="glyphicon glyphicon-plus"></span></button>';
    input += '</div>';
    input += '</div>';
    input += '<div id="product_fields"></div>';

    return input;
}


function load_cardexCustomer(url, element_desc, element_product_localization)
{
    let address_token = $(this).val();

    $.ajax({
        url: url,
        type: 'GET',
        data : {
            address_token : address_token
        },
        success: function (json) {
            let res = JSON.parse(json);
            if (res.code === '200' && res.data){

                if (res.data.descriptions && res.data.distinct_room_references && res.data.list_occurences){
                    element_desc.innerHTML = constructDescription(res.data.descriptions, res.data.distinct_room_references, res.data.list_occurences);
                }

                if (res.data.product_list_references){
                    element_product_localization.innerHTML = constructProductLocalization(res.data.product_list_references);
                }
            }
        },
        error: function () {
            bo_notify('Erreur inconnue', 'danger');
        }
    });
    return address_token;
}


function constructWholeTaskContainer(url, value)
{
    var input = '';

    input += '<div class="col-xs-12 cardex-task">';
    input += '<div class="col-xs-12 padding">';
    input += '<div class="col-xs-3">';
    input += '<img class="img-cardex" src="' + url + '">';
    input += '</div>';
    input += '<div class="col-xs-9 text-bold-title">';
    input += value;
    input += '<span class="pull-right glyphicon glyphicon-menu-down"></span>';
    input += '</div>';
    input += '</div>';
    input += '</div>';

    return input;
}


function constructTaskContainer(description, cardexId)
{
    var input ='';

    input += '<div class="col-xs-12 padding border-bottom-description">';
    input += '<div class="col-xs-10 text-description">';
    input += description;
    input += '</div>';
    input += '<div class="col-xs-2 text-right">';
    input += '<button type="submit" class="btn btn-default btn-xs" onclick="deleteRoom(' + cardexId + ');" name="action" value="delete">';
    input += '<span class="glyphicon glyphicon-minus">';
    input += '</button>' + '</div>' + '</div>';

    return input;
}

function constructDescription(descriptions, distinct_rooms, list_occurences)
{
    var input = '';
    var dr_length = distinct_rooms.length;

    for (var i = 0; i < dr_length; i++) {
        if(containsId(list_occurences, distinct_rooms[i])) {
            input += constructWholeTaskContainer(distinct_rooms[i].url, distinct_rooms[i].value);
            input += '<div class="col-xs-12 overflow-task margin">';
            var descr_length = descriptions.length;
            for (var j = 0; j < descr_length; j++) {
                if (descriptions[j].id == distinct_rooms[i].id) {
                    input += constructTaskContainer(descriptions[j].description, descriptions[j].c_id);
                }
            }
            input += '</div>';
            input += '</div>';
            input += '</div>';
        }
    }
    return input;
}

function constructProductLocalizationContainer(lp_url, lr_value, description, id)
{
    var input = '';

    input += '<div class="col-xs-12 margin padding cardex-product">';
    input += '<div class="col-xs-3">';
    input += '<img class="img-cardex" src="' + lp_url +'">';
    input += '</div>';
    input += '<div class="col-xs-7">';
    input += '<p class="text-bold-title text-center noMargeBottom">';
    input += lr_value;
    input += '</p>';
    input += '<p class="text-description noMargeBottom border-top-description">';
    input += description;
    input += '</p>';
    input += '</div>';
    input += '<div class="col-xs-2">';
    input += '<button type="submit" class="btn btn-default btn-xs" onclick="deleteProduct(' + id + ');" name="action" value="delete">';
    input += '<span class="glyphicon glyphicon-minus">';
    input += '</button>';
    input += '</div>';
    input += '</div>';

    return input;
}

function constructProductLocalization(references)
{
    var input = '';

    var ref_length = references.length;
    for (var i = 0; i < ref_length; i++) {
        input += constructProductLocalizationContainer(references[i].lp_url, references[i].lr_value, references[i].description, references[i].id);
    }
    return input;
}

function containsId(array_to_search, entity)
{
    var array_length = array_to_search.length;
    for(var i = 0; i < array_length; i++){
        if(array_to_search[i].id == entity.id){
            return true;
        }
    }
    return false;
}


function deleteRoom(id)
{
    $.ajax({
        url: 'api/cardex.php',
        type: "DELETE",
        data: {
            id : id,
            type : 'task'
        },
        success: function () {
            $('#address_cardex').trigger('change');
        },
        error: function () {
            bo_notify('Erreur inconnue');
        }
    });
}

function deleteProduct(id)
{
    $.ajax({
        url: 'api/cardex.php',
        type: "DELETE",
        data: {
            id : id,
            type : 'product'
        },
        success: function () {
            $('#address_cardex').trigger('change');
        },
        error: function () {
            bo_notify('Erreur inconnue');
        }
    });
}

function validateRoomForm()
{

    var r_descr = $('input[name="r_description[]"]').map(function() {
        return this.value
    }).get();

    var r_select = $('select[name="r_room_select[]"]').map(function() {
        return this.value
    }).get();

    var e = document.getElementById("address_cardex");
    var tok_address = e.options[e.selectedIndex].value;

    $.ajax({
        url: 'api/cardex',
        type: "POST",
        data: {
            address_token: tok_address,
            description: r_descr,
            select_room: r_select,
            elem_number : task_number
        },
        success: function () {
            bo_notify('Element(s) inséré(s) avec succès', 'success')
        },
        error: function () {
            bo_notify('Une erreur est survenue', 'danger');
        }
    }).done(function(){
        task_number = 0;
        $('#address_cardex').trigger('change');
    });
    bootbox.hideAll();
}


function validateProductForm()
{
    var p_descr = $('input[name="p_description[]"]').map(function() {
        return this.value
    }).get();

    var r_select = $('select[name="p_room_select[]"]').map(function() {
        return this.value
    }).get();

    var p_select = $('select[name="p_product_select[]"]').map(function() {
        return this.value
    }).get();

    var e = document.getElementById("address_cardex");
    var tok_address = e.options[e.selectedIndex].value;

    $.ajax({
        url: 'api/cardex',
        type: "POST",
        data: {
            address_token: tok_address,
            description: p_descr,
            select_room: r_select,
            select_product: p_select,
            elem_number : product_number
        },
        success: function () {
            bo_notify('Element(s) inséré avec succès', 'success')
        },
        error: function () {
            bo_notify('Une erreur est survenue', 'danger');
        }
    }).done(function(){
        product_number = 0;
        $('#address_cardex').trigger('change');
    });
    bootbox.hideAll();
}


function buildCancelButton()
{
    return  {
        label: "Annuler",
        className: 'btn-warning'
    }
}

function buildValidateTaskButton()
{
    return  {
        label: "Valider",
        className: 'btn-success btn-primary btn-task-valid',
        callback: function(){
            validateRoomForm();
        }
    }
}

function buildValidateProductButton()
{
    return  {
        label: "Valider",
        className: 'btn-success btn-primary btn-product-valid',
        callback: function(){
            validateProductForm();
        }
    }
}

function buildModal(title, msg, buttons)
{

    return bootbox.dialog({
        title: title,
        message: msg,
        buttons: buttons,
        onEscape : true
    });
}


//NEW CARDEX

function addNewCardex() {
    var cardex_name = $("#name").val();
    var l_maintenanceType_id = $("#l_maintenanceType_id").val();

    if (!cardex_name || !l_maintenanceType_id) {
        bo_notify("Impossible des récupérer les informations pour créer le cardex");
        retrun;
    }

    ajax_call(
        "api/cardex",
        "POST",
        {
            name: cardex_name,
            l_maintenanceType_id: l_maintenanceType_id
        },
        function(result) {
            if (result.code == 200) {
                window.location.href = "view";
            } else {
                bo_notify(result.message, "danger");
            }
        }
    );

}

function addNewTask(is_rolling, room_id) {
    new Promise((res, err) => {
        ajax_call(
            "api/list",
            "GET",
            {
                listName: is_rolling ? "rollingTask" : "task"
            },
            function(result){
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    }).then(task_list => {
        var selectInputs = [{text: 'Tâches...', value: ''}]

        task_list.forEach(task => {
            selectInputs.push({
                text: task.field,
                value: task.id
            });
        });

        return new Promise((res, err) => {
            bootbox.prompt({
                title: "Sélectionnez la tâche",
                inputType: 'select',
                inputOptions: selectInputs,
                callback: function (result) {
                    if (result) {
                        res(result);
                    }
                }
            })
        });
    }).then(task => {
        ajax_call(
            "api/cardex",
            "PUT",
            {
                lr_room_id: room_id,
                l_task_id: task,
                cardex_id: $("#cardex_id").val()
            }, function(result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    displayNewTask(room_id, result.data);
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        )
    }).catch(err => {
        ajax_log(err, "cardex.js / addNewTask");
    });
}

function addNewRoom() {
    var rooms = [];

    new Promise((res, err) => {
        ajax_call(
            "api/list_reference",
            "GET",
            {
                listName: "roomsList"
            },
            function(result){
                if (result.code == 200) {
                    res(result.data);
                } else {
                    err(result.message);
                }
            }
        )
    }).then(room_list => {
        var selectInputs = [{text: 'Pièces...', value: ''}]

        room_list.forEach(room => {
            rooms[room.id] = room;
            selectInputs.push({
                text: room.value,
                value: room.id
            });
        });

        return new Promise((res, err) => {
            bootbox.prompt({
                title: "Sélectionnez une pièce",
                inputType: 'select',
                inputOptions: selectInputs,
                callback: function (result) {
                    if (result) {
                        res(result);
                    }
                }
            })
        });
    }).then(room_id => {

        if ($("#room_" + room_id).length) {
            bo_notify("La pièce éxiste déjà!", "danger");
        } else {
            displayNewRoom(rooms[room_id]);   
        }
    }).catch(err => {
        ajax_log(err, "cardex.js");
    });
}

function removeTask(cardex_task_id) {

    new Promise((res, err) => {
        bootbox.confirm({
            message: "Voulez-vous vraiment supprimer cette tâche?",
            buttons: {
                confirm: {
                    label: 'Oui',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Non',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result)
                    res();
            }
        });
    }).then(() => {
        ajax_call(
            "api/cardex",
            "DELETE",
            {
                cardex_task_id: cardex_task_id
            }, function(result) {
                if (result.code == 200) {
                    bo_notify(result.message, "success");
                    $("#cardex_task_" + cardex_task_id).remove();
                } else {
                    bo_notify(result.message, "danger");
                }
            }
        );
    }).catch(err => {
        ajax_log(err, "cardex.js / removeTask");
    })


}

function displayNewRoom(room_info) {
    $(".flex-room").append(`
    <div class="room">
        <h3>${room_info.value}</h3>
        <div class="flex-task" id="tasks_${room_info.id}">
            <div class="new-task">
                <button class="spanButton" onclick="addNewTask(false, ${room_info.id})"><span class="glyphicon glyphicon-plus"></span></button>
            </div>
        </div>
    </div>
    `);
}

function displayNewTask(room_id, task) {
    $("#tasks_" + room_id).prepend(`
    <div class="task" id="cardex_task_${task.cardex_task_id}">
        ${task.field}
        <button class="spanButton pull-right" onclick="removeTask(${task.cardex_task_id})"><span class="glyphicon glyphicon-remove"></span></button>
    </div>
    `);
}
*/

function addCardexTask(ajax_data) {
    ajax_call(
        "api/task",
        "POST",
        ajax_data,
        function(result) {
            if (result.code === '200') {
                refreshCardexTasks();
            } else {
                bo_notify(result.message, "danger");
            }
        }, function () {
            ajax_log('api/task en POST', 'cardex.js', 'addTask')
        }
    );
}

function refreshCardexTasks() {
    ajax_call(
        "api/task",
        "GET",
        {cardex_id: $('#cardex_id').val()},
        function(result) {
            if (result.code === '200') {
                appendCardex(result.data);
             } else {
                bo_notify(result.message, "danger");
            }
        }, function () {
            ajax_log('api/task en GET', 'cardex.js', 'refreshCardexTasks')
        }
    );
}

function appendCardex(tasks) {
    let i = 0;
    let room_exist = [];
    let roomContainer = $('#task-cardex-container');

    if (tasks === 'undefined' || tasks.length < 1)
        return;
    roomContainer.find('div').remove();
    while (tasks[i]) {
        let task = tasks[i];

        if (room_exist.indexOf(task.room_id) !== -1) {
            let cardexTask = createCardexTask(task.id, task.task, task.ponderation, task.color_hex);
            roomContainer.find('.overflow-task:last-child').append(cardexTask);
        }
        else {
            let room = createCardexRoom(task.room_img, task.room_id, task.room);
            roomContainer.append(room);
            room_exist.push(task.room_id);
            continue;
        }
        i++;
    }
}

function createCardexTask(cardex_task_id, task_name, ponderation, color) {
    let task ='';

    task += '<div class="col-xs-12 padding border-bottom-description">';
    task += '<div class="col-xs-10 text-description" style="color:'+color+'">';
    task += task_name;
    task += '</div>';
    task += '<div class="col-xs-2 text-right">';
    task += '<button type="submit" class="btn btn-default btn-xs" onclick="deleteCardexTask(' + cardex_task_id + ');" name="action" value="delete">';
    task += '<span class="glyphicon glyphicon-minus">';
    task += '</button>' + '</div>' + '</div>';

    return task;
}

function createCardexRoom(room_img, room_id, room_name) {
    let room = '';

    room += '<div class="col-xs-12 cardex-task">';
    room += '<div class="col-xs-12 padding">';
    room += '<div class="col-xs-3">';
    room += '<img class="img-cardex" src="' +room_img+ '">';
    room += '</div>';
    room += '<div class="col-xs-9 text-bold-title">';
    room += room_name;
    room += '<span class="pull-right glyphicon glyphicon-menu-down"></span>';
    room += '</div>';
    room += '</div>';
    room += '</div>';
    room += '<div class="col-xs-12 overflow-task margin" data-room-id="'+room_id+'">';
    room += '</div>';

    return room;
}

function deleteCardexTask(cardex_task_id) {
    ajax_call(
        "api/task",
        "DELETE",
        {cardex_task_id: cardex_task_id},
        function(result) {
            if (result.code === '200') {
                refreshCardexTasks();
            } else {
                bo_notify(result.message, "danger");
            }
        }, function () {
            ajax_log('api/task en DELETE', 'cardex.js', 'deleteCardexTask')
        }
    );
}

function addProductLocalization(data) {
    return ajax_call(
        "/v1/client/api/product_localization",
        "POST",
        data,
        null
    );
}

function deleteProductLocalization(product_localization_id) {
    return ajax_call(
        "/v1/client/api/product_localization",
        "DELETE",
        {product_localization_id: product_localization_id},
        null,
        function () {
            ajax_log('/v1/client/api/product_localization en DELETE', 'cardex.js', 'deleteProductLocalization')
        }
    );
}

function createProductLocalization(pl_id) {
    let product = $('#lr_product_id option:selected'),
        room = $('#lr_room_id option:selected');
    let lp_url = product.data('url'),
        lp_value = product.text(),
        lr_url = room.data('url'),
        lr_value = room.text(),
        description = $("#pl_description").val();

    return "<div class=\"col-xs-12 product-localization-view\" data-id=\""+pl_id+"\">"
        + "<div class=\"col-xs-4\">"
        + "<img class=\"pl-img\" src=\""+lp_url+"\">"
        + "<p>"+lp_value+"</p>"
        + "</div>"
        + "<div class=\"col-xs-4\">"
        + "<img class=\"pl-img\" src=\""+lr_url+"\">"
        + "<p>"+lr_value+"</p>"
        + "</div>"
        + "<div class=\"col-xs-4\">"
        + "<p class=\"noMarge\">"+description+"</p>"
        + "<span class=\"pl-delete-icon glyphicon glyphicon-trash\"></span>"
        + "</div>"
        + "</div>";
}
