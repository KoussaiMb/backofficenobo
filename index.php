<?php
require_once('inc/bootstrap.php');

$db = App::getDB();
$user = App::getAuth()->user();
try {
    if ($user->who == 'admin') {
        Session::getInstance()->setFlash('info', 'Maître, nous sommes entrain de générer votre écran d\'accueuil');
        $board = null;
    }
    else  {
        Session::getInstance()->setFlash('info', 'Membre du BackOffice, nous sommes entrain de générer votre écran d\'accueuil');
        $board = null;
    }
    $db->beginTransaction();
    $waitingList = $db->query_log("SELECT COUNT(*) AS count FROM `waiting_list` WHERE `deleted` = 0 AND `accepted` = 0", null, 1)->fetch();
    $activeProviders = $db->query_log("SELECT COUNT(*) AS count FROM `provider` WHERE `deleted` = 0", null, 1)->fetch();
    $alerts = $db->query_log("SELECT COUNT(*) AS count FROM `alert` WHERE `finished` = 0", null, 1)->fetch();
    $clients = $db->query_log("SELECT COUNT(*) AS count FROM `customer` WHERE `deleted` = 0", null, 1)->fetch();
    $db->commit();
} catch (PDOException $e){
    Session::getInstance()->setFlash('danger', $e->getMessage());
}
require_once('inc/header_bo.php');
?>
    <div class="panel panel-success">
        <div class="panel-heading panel-heading-xs">
            Informations générales
        </div>
        <div class="panel-body">
            <?php include('inc/print_flash_helper.php'); ?>
            <div class="col-sm-12">
                <?php if (!empty($board)): ?>
                    <div class="col-xs-12 title-main-board">
                        <h2><?= $board->title; ?></h2>
                    </div>
                    <div class="col-xs-12 main-board">
                        <?php foreach($board as $k => $row): ?>
                            <div class="row-board col-xs-12">
                                <div class="col-xs-8">
                                    <div class="title-row-board">
                                        <?= $row->title ?>
                                    </div>
                                    <div class="description-row-board">
                                        <?= $row->description ?>
                                        <span class="pull-right"><?= $row->date_create ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="priority-row-board priority-<?= $row->priority; ?>">
                                        <span class="glyphicon glyphicon-certificate"></span>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <style>
                .priority-row-board {

                }
                .priority-row-board-low {
                }
                .priority-row-board-high {
                }
                .priority-row-board-critic {
                }
                .row-board {
                    border-bottom: 1px solid #669999;
                }
                .title-main-board {
                    color: #669999
                }
                .main-board {
                    padding: 0;
                    overflow-y: scroll;

                }
            </style>
            <div class="col-sm-12">
                <div class="card col-xs-4">
                    <div class="card-success text-center" data-toggle="/v1/client/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-2"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-10">
                            <p>Clients dans la liste d'attente</p>
                            <p><?= $waitingList->count; ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-4">
                    <div class="card-warning text-center" data-toggle="/v1/provider/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-4"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-8">
                            <p>Prestataires actifs</p>
                            <p><?= $activeProviders->count; ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-4">
                    <div class="card-danger text-center" data-toggle="/v1/alerts/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-4"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-8">
                            <p>Alertes actives</p>
                            <p><?= $alerts->count; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card col-xs-4">
                    <div class="card-success text-center" data-toggle="/v1/client/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-4"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-8">
                            <p>Nos clients</p>
                            <p><?= $clients->count; ?></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-4">
                    <div class="card-warning text-center" data-toggle="/v1/provider/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-4"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-8">
                            <p>Autres</p>
                            <p>0</p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-4">
                    <div class="card-danger text-center" data-toggle="/v1/mission/view" onclick="window.location = $(this).attr('data-toggle');">
                        <div class="col-xs-4"><span class="glyphicon glyphicon-user"></span></div>
                        <div class="col-xs-8">
                            <p>Missions</p>
                            <p>0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .card {
            padding: 0 10px;
            cursor: pointer;
        }
        .card > div > div:first-child {
            font-size: 4.2em;
        }
        .card > div > div {
            display: inline-block;;
            padding: 5px;
        }
        .card > div >div:first-child {
            font-size: 4.2em;
        }
        .card > div > div > p:first-child {
            text-transform: uppercase;
        }
        .card > div > div > p:last-child {
            font-size: 2em;
            font-weight: bold;
            color: lawngreen;
        }
        .card-danger{
            border: 3px solid #f44336;
            color: #f44336;
        }
        .card-danger > div > p:first-child {
            color: #e57373;
        }

        .card-danger div:first-child {
            background-color: #ffcdd2;
        }
        .card-danger:hover > div:last-child{
            background-color: #ffebee;
        }
        .card-success{
            border: 3px solid #4caf50;
            color: #4caf50;
        }
        .card-success > div > p:first-child {
            color: #81c784;
        }
        .card-success div:first-child {
            background-color: #c8e6c9;
        }
        .card-success:hover > div:last-child{
            background-color: #e8f5e9;
        }
        .card-warning{
            border: 3px solid #ffc107;
            color: #ffc107;
        }
        .card-warning > div > p:first-child {
            color: #ffd54f;
        }
        .card-warning div:first-child {
            background-color: #ffecb3;
        }
        .card-warning:hover > div:last-child{
            background-color: #fff8e1;
        }
    </style>
    <script>
        jQuery(document).ready(function() {
            $(".main-board").css('max-height', $(window).height() * 0.4);
        });
    </script>
<?php

require('inc/footer_bo.php');
